<?php 
$timestamp = $testdetails->test_time;
$time = $timestamp;
$parsed = date_parse($time);
$diff = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
$hours = floor($diff / 3600) . ' : ';
$diff = $diff % 3600;
$minutes = floor($diff / 60) . ' : ';
$diff = $diff % 60;
$seconds = $diff;
$total_min  =  $minutes*60;
?>

<div class="container">
<div class="graybg">
<div class="testcss">
    <img src="<?php echo base_url();?>themes/front/images/logo.png"  class="logo" style="margin-top:0;" alt="" />
    <h3 class="testTitle">
       <?php echo $testdetails->test_name; ?>
    </h3>

</div>
    <form method="post" name="form1" id="formsubmit" class="myform" action="<?php echo base_url();?>practicetest/submitquestions">
    <input type="hidden" name="total_questions" value="<?php echo $quetionscount; ?>" />
    <input type="hidden" value="<?php echo  $testdetails->test_id; ?>" id="test_id" name="test_id">
    <input type="hidden" value="<?php echo  $testdetails->test_time; ?>" id="test_time" name="test_time">
    <div class="leftboxq col-xs-18 col-sm-8 col-md-8 col-lg-8">
    <p class="txt7">Questions</p>
    <ul class="pogo pull-left nav nav-tabs" role="tablist" id="pempTx">
    <?php
    $j=1;
    $x=1;
    foreach($testquestions as $value) { 
    ?>
    <li class="<?php if($x==$j) echo "active";?>">
    <a data-toggle="tab" role="tab" href="#tb<?php echo $value->q_id;?>">
            <?php if(strlen($j) == 1) {  echo '0'.$j; } else { echo $j; }?>
    </a>
    </li>
    <?php $j++;} ?>
    </ul>

</div>
<div class="leftboxq col-xs-18 col-sm-4 col-md-4 col-lg-4">
<div class="timer">
  <div id="CountDownTimer" data-timer="<?php echo $total_min;?>" style="width: 238px; height: 120px;"></div>
</div>
</div>
<div class="clear-fix"></div>
<div class="testsection tab-content">
<?php
$i=1;
$y=1;
foreach($testquestions as $value) {
?>
<div class="tab-pane <?php if($i==$y) echo "active";?>" id="tb<?php echo $value->q_id;?>">
<div class="col-xs-18 col-sm-8 col-md-8 col-lg-8">

<div class="clear-fix">

	<ul class="pagination-content">
	<li><?php
        if($value->passage_id !='') {
        ?>
        <?php
        echo $passage[$value->q_id]->passage;
        ?>
        <?php } else { 
         echo $value->question_name;
        } ?>

	</li>
	</ul>
</div>

</div>
<div class="kalps col-xs-18 col-sm-4 col-md-4 col-lg-4">
	<?php
        if($value->passage_id !='') {
        ?>
	<div class="que">
		<h4>Question</h4>
		<?php echo str_replace("<p><br /></p>", '',$value->question_name);?>
	</div>
     <?php } ?>
	<div class="ops">
		<h4>Options</h4>
                <?php if($value->option1 !='') { ?>
		<p><input class="answerValue"  type="radio" name="opt_<?php echo $value->q_id;?>" value="1" /><?php echo str_replace("<p>", '',$value->option1);?> </p>
                <?php } if($value->option2 !='') {?>
                <p><input class="answerValue"  type="radio" name="opt_<?php echo $value->q_id;?>" value="2" /><?php echo str_replace("<p>", '', $value->option2);?></p>
                <?php } if($value->option3 !='')  { ?>
                <p><input class="answerValue"  type="radio" name="opt_<?php echo $value->q_id;?>" value="3" /><?php echo str_replace("<p>", '', $value->option3);?></p>
                <?php } if($value->option4 !='')  { ?>
		<p><input class="answerValue" type="radio" name="opt_<?php echo $value->q_id;?>" value="4" /><?php echo str_replace("<p>", '', $value->option4);?></p>
                <?php } if($value->option5 !='')  { ?>
                <p><input class="answerValue"  type="radio" name="opt_<?php echo $value->q_id;?>" value="5" /><?php echo str_replace("<p>", '', $value->option5);?></p>
                <?php } ?>
                <input type="hidden" value="" name="checked[]" class="radioCheck" />
                <input type="hidden" name="question_id[]" value="<?php echo $value->q_id; ?>" />
	</div>
	<div class="que">
		<p class="pull-right pagin"><a href="#" class="pv">Previous</a> <a href="#" class="nx">Next</a></p>
	</div>
</div>
</div>

<?php $i++;} ?>
</div>

  <button type="submit" name="finish" class="btn btn-warning result pull-right">Result</button>
</form>

  <script type="text/javascript">
 var hour = "<?php echo floor($hours); ?>";
 var min = "<?php echo floor($minutes); ?>";
 var sec = "<?php echo floor($seconds); ?>";

function countdown() {
 
 if(sec <= 0 && min > 0) {
  sec = 59;
  min -= 1;
 }
 else if(hour <=0 && min <= 0 && sec <= 0) {
  hour = 0;
  min = 0;
  sec = 0;
   $("#CountDownTimer").hide();
  alert("your test time is completed & Submit now");
  document.form1.submit();
  return false;
  
 }
 else {
  sec -= 1;
 }
 
 if(min <= 0 && hour > 0) {
  min = 59;
  hour -= 1;
 }
 
 var pat = /^[0-9]{1}$/;
 sec = (pat.test(sec) === true) ? '0'+sec : sec;
 min = (pat.test(min) === true) ? '0'+min : min;
 hour = (pat.test(hour) === true) ? '0'+hour : hour;
 
 //document.getElementById('strclock').innerHTML = "Time Left  "+hour+":"+min+":"+sec;
 //document.getElementById('timeLeft').value = hour+":"+min+":"+sec;
 setTimeout("countdown()",1000);
 }
 countdown();
</script>