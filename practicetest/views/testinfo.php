<style>
    .ui-progressbar {
        text-align: right !important;
    }
</style>
<div class="container">
		<div class="container-fluid" id="kolcon">
		<div class="row">
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-16">
                    
		  <?php  
                  if($quetionscount == '0') {
                  $percentage = 0;  
                  }else {
                  $percentage  =  100 / 3;
                  }
                  ?>
		  <h3 class="hdtxt"><strong>Practice</strong> Test </h3>
			<div class="listingnames">
				<div class="graylinebox">
				<div class="iconbox2">
				<h4 class="orange"><span class="glyphicon glyphicon-bell"></span> Test Details:</h4>
				<!--<table class="borderxs">
					<tr><td><span class="a1"></span></td> <td><strong>Total Number of questions: </strong></td><td><strong><?php echo $quetionscount;?></strong></td></tr>
					<tr><td><span class="a2"></span></td> <td><strong>Time Allotted: </strong></td> <td><strong><?php echo $testdetails->test_time;?> minutes</strong></td></tr>
				</table>-->
                
                <table class="table">
					<tr><td><b>Test Name</b></td> <td><?php echo $testdetails->test_name;?></td></tr>
					<tr><td><b>Number Of Question</b></td> <td><?php echo $quetionscount;?></td></tr>
                    <tr><td><b>Time( minutes )</b></td> <td><?php echo $testdetails->test_time;?> minutes</td></tr>
                    <tr><td colspan="2" class="gray">You can mark/unmark the question for revision.</td></tr>
                    <tr><td colspan="2" class="gray">No mark will be deducted for unanswered question.</td></tr>
				</table>
				<ul>
					<li><span class="a3"></span> <p>No negative marks.</p></li>
					<li><span class="a4"></span> <p>Each questions carry 1 marks.</p></li>
				</ul>
				</div>
				<!--<div class="sandbox">
					<blockquote>
						<ol>
						<li>Click submit button to submit your answer.</li>
						<li>Test will be submitted automatically expired.</li>
						<li>Don`t refresh the page.</li>
						</ol>
					</blockquote>
				</div>-->
                                    
                                    <div style="margin:20px 0;">
                                        <div class="progress">
				<div id="progressbar">
                                <div class="progress-label">
                                     <?php for($p=1;$p<=$quetionscount;$p++){?>
                                      <div style="width: <?php echo $percentage . '%'?>" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="custom-progress-label progress-bar progress-bar-info progress-bar-striped">
				       <span class="sr-only"><?php echo $percentage . '%'?> Complete</span>
				      </div>
                                      <?php } ?>
                                  
                                </div>                
                            </div>
                               </div></div>     
                                    
				<div style="position:relative;width:100%;height: 36px; display:none;" id="abcd">
				<a class="btn btn-success" href="<?php echo base_url();?>practicetest/practiceteststart/<?php echo $testdetails->test_id;?>/<?php echo $testdetails->topic_id;?>"  style="position:absolute;left:40%;width:150px;" >TAKE A TEST</a>
				</div>
				</div>	
				
				  
			</div> 
		 </div>
		 <div class="col-lg-3 col-sm-3 col-md-3 col-xs-16 rightpane">
         
		<div class="panel panel-default">
		  <div class="panel-heading text1"><span class="glyphicon glyphicon-th"></span> Challenger of the Day</div>
			<div class="panel-body">
				<div class="m4_content_block">
					<div title="SUMIT" id="winner_photo">
                                            <img alt="SUMIT" src="<?php echo base_url();?>public/profileimages/<?php echo $c->member_image;?>" class="img-rounded"></div>
					<div id="winner_info">
						<div class="txt3"><?php echo $c->member_name;?></div>
						<div class="small">India</div>
						<div><strong>Andhra Pradesh</strong></div>
                                                <div class="time_box">Time: <span>
                                       <?php 
                                        $date1 = "10:00:00";
                                        $date2 = $c->time;
                                        $seconds = ($date2 - strtotime($date1))-19800;
                                        echo date("H:i:s", $seconds);
                                        ?>
                                                    </span></div>
					</div>
					<div id="challangeroftheday_points">
					<div class="points">Points</div><div class="number"><?php echo $c->points; ?></div><div class="clear"></div>
						</div>
					<div class="clear"></div>
				</div>
		 </div>
		 </div>
		 <div class="panel panel-default">
		  <div class="panel-heading text1"><span class="glyphicon glyphicon-file"></span> Placement Questions</div>
			<div class="panel-body">
			<ul class="rightcommentslist">
				<li>
				<div class="m4_content_box">
				 <p>A man travels from X to Y at a average speed og 9Km/Hr and from Y to Z at 7Km/Hr. What is the average speed?</p>
				 <h3 class="bwname">Asked By: TCS</h3>
				 <span class="m4email">Unsolved</span>
				 </div>
				</li>
				<li>
				<li>
				<div class="m4_content_box">
				 <p>The eaverage of two natural number is 10. randomly picked onenumber is 3, then what is the other number?</p>
				 <h3 class="bwname">Asked By: TCS</h3>
				 <span class="m4email">Unsolved</span>
				 </div>
				</li>
				</li>
			</ul>
		 </div>
		 </div>
		 <div class="panel panel-default">
		  <div class="panel-heading text1"><span class="glyphicon glyphicon-file"></span> User Comments</div>
			<div class="panel-body">
			<ul class="rightcommentslist">
				<li>
				<div class="m4_content_box">
				 <p>I need tcs new pattern papers of last 3 years any body plz send me at my mail :
				 <img class="pull-right img-circle" src="http://www.m4maths.com/userThumb/556501_84633.jpg" data-original-title="" title="" >
				 </p>
				 <h3 class="m4name">Mayuresh Pradhan</h3>
				 <span class="m4email">mayuresh@yahoo.com</span>
				 <span class="m4poseted">11 Hours ago</span>
				 </div>
				</li>
				<li>
				<li>
				<div class="m4_content_box">
				 <p>Sir Can you please send me recent last three or four years Cognizant quetion paper with answers, so I can practice.
				 <img class="pull-right img-circle" src="<?php echo base_url();?>themes/front/images/photo.png" data-original-title="" title="" >
				 </p>
				 <h3 class="m4name">Parrot Parrel</h3>
				 <span class="m4email">pp2589@yahoo.com</span>
				 <span class="m4poseted">01 days 13 Hours ago</span>
				 </div>
				</li>
				</li>
			</ul>
		 </div>
		 </div>
		 </div>
	  </div>
	
		</div>
    </div>


<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
.custom-progress-label {
    float: left;text-align: center;line-height: 27px;
}
.ui-progressbar {
position: relative;
}
.progress-label {
position: absolute;
/*left: 50%;
top: 4px;*/
width: 100%;
font-weight: bold;
color: #F07C00;
}
.ui-progressbar .ui-progressbar-value {
    margin: -1px;
    height: 100%;
    transition: width 0.5s;
    -webkit-transition: width 0.5s;
}
</style>
<script>
$(function() {    
    var progressbar = $( "#progressbar" ),
    progressLabel = $( ".progress-label" );
    progressbar.progressbar({
        value: false,
        change: function() {
            progressLabel.text( progressbar.progressbar( "value" ) + "%" );
        },
        complete: function() {
            //progressLabel.text( "Complete!" );
             $("#abcd").fadeIn();
            //$("#start-test").fadeIn();
        }
    });

    function progress() {
        var percentage = "<?php echo round($percentage); ?>";
        var val = progressbar.progressbar( "value" ) || 0;
        progressbar.progressbar( "value", val + parseInt(percentage) );
        if ( val < 99 ) {
            setTimeout( progress, 2000 );
        }
    }
    setTimeout( progress, 2000 );
});
</script>