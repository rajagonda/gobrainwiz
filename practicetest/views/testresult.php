
<div class="container">
<div class="graybg">
<div class="testcss"><img src="<?php echo base_url();?>themes/front/images/logo.png"  class="logo" style="margin-top:0;" alt="" /> <h3 class="testTitle">Score Card</h3>
<a href="#" class="btnClick" title="download pdf" onClick="alert('Download Score Card?')"><span class="glyphicon glyphicon-save"></span> 
</a> 
    <a class="btnClick" href="<?php echo base_url();?>" style="padding-right:28px;"> 
        <span class="glyphicon glyphicon-home">
            
        </span></a>
</div>

<div class="col-xs-18 col-sm-3 col-md-3 col-lg-3">
<div class="profilepic">
<span class="ppicsNum">
   <?php 
    $per = ($testresult->correct_q/$testresult->total_q)*100;
    echo round($per);
    ?>
    <?php
    $tid = $this->uri->segment(3);    
    ?>
    <span class="ppicsPercent">%</span>
</span>
    </div>
<h5><strong>Summery</strong></h5>
<p><small>Notice your progress how it is. Is it appriciatable?
If yes, then you can download the progress card and share on facebook or gmail.</small>
</p>

</div>
<div class="col-xs-18 col-sm-9 col-md-9 col-lg-9 perchart">
<h3>Your Score Card </h3>
<div class="hoax"><em>You can download your score card as download pdf, so you have to click on right top download icon to download score card.</em> </div>
<table class="table table-condensed table-striped splpaddin">
	<tr>
    <td colspan="5"><h4 class="pull-left"><?php echo $testdetails->test_name;?> </h4><!--<h5 class="pull-right tlenm">Topic Name</h5>--></td>
    </tr>
    <tr>
    <td width="20%"><span class="nup"><?php echo $testresult->total_q;?></span><span class="ndown">Total Ques</span></td>
    <td width="20%"><span class="nup"><?php echo $testresult->notattempetd_q;?></span><span class="ndown">Unattempted</span></td>
    <td width="20%"><span class="nup"><?php echo $testresult->total_q-$testresult->notattempetd_q;?></span><span class="ndown">Attempted</span></td>
    <td width="20%"><span class="nup"><?php echo $testresult->correct_q;?></span><span class="ndown">Correct</span></td>
    <td width="20%"><span class="nup"><?php echo $testresult->correct_q;?></span><span class="ndown">Score</span></td>
    </tr>
   
</table>
<a href="<?php echo base_url();?>practicetest/showanswerkey/<?php echo $tid;?>" class="btn btn-info btn-lg" >CLICK HERE FOR EXPLANATION</a>
</div>
