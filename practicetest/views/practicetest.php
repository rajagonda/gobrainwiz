<script type="text/javascript">
          $(document).ready(function () {
            gettopics('<?php echo $subcatid;?>');
            });
     </script>
     
<div class="container">
		<div class="container-fluid" id="kolcon">
			<div class="row">
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-16">
		<h3 class="hdtxt"><strong>Choose your test from lists</strong></h3>
		  <div class="panel panel-default">
		  <ul class="list-group">
			<li class="list-group-item ntabw3">
				<ul class="nav nav-tabs tabw3" role="tablist" id="tabw3">
                                <?php
                                foreach($subcategories as $value) {
                                ?>
				<li <?php if($value->id == $subcatid) echo "class='active'";?> onclick="gettopics('<?php echo $value->id;?>')"><a href="#ma" role="tab" data-toggle="tab" class="width130px"><?php echo $value->category_name;?></a></li>
                                <?php } ?>
				</ul>
				<div class="tab-content tabw3Content">
				<div class="tab-pane active" id="ma">
				 </div>
					  
                                          
					
				</div>
				
				
				</div>
			</li>
			</ul>
		 </div>
		
		<?php echo $this->load->view('brainwizz/rightpanel'); ?>
	  </div>
	
		</div>
    </div>
<div class="modal fade modalsamp" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Choose your Paper </h4>
      </div>
       <div class="modal-body">
				<div class="list-group lockOrUnlock" id="cholist">
                                    <div id="abcdefg">
				  <a href="<?php echo base_url();?>practicetest/teststart" class="list-group-item list-group-item-success">&rsaquo; <strong>Paper 1</strong></a>
				  <a href="<?php echo base_url();?>practicetest/teststart" class="list-group-item list-group-item-success">&rsaquo; <strong>Paper 2</strong></a>
				  <a href="<?php echo base_url();?>practicetest/teststart" class="list-group-item list-group-item-success">&rsaquo; <strong>Paper 3</strong></a>
				  <a href="#" data-target=".modalsamp2" data-toggle="modal" class="list-group-item list-group-item-danger" data-dismiss="modal">&rsaquo; <strong>Paper 4</strong></a>
				  <a href="<?php echo base_url();?>practicetest/teststart" class="list-group-item list-group-item-success">&rsaquo; <strong>Paper 5</strong></a>
				</div>
                                    </div>
      </div>
    </div>
  </div>
</div>
<div id="myModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Just Login </h4>
      </div>
       <div class="modal-body">
        <!--<img src="images/fbads.png" alt="article1" />-->
		<div class="containerlg">
	<section id="content">
            <span id="loginerror"></span>
		<form name="puzzleloginform" id="puzzleloginform" action="<?php echo base_url(); ?>login/puzzleloginsubmit" method="post">
			<!--<h1>Just Login</h1>-->
			<div>
				<input type="text" placeholder="Username" required="" name="email" id="username" />
			</div>
			<div>
				<input type="password" placeholder="Password" required="" name="password" id="password" />
			</div>
			<div>
			<p class="forgotPara" style="padding-left:22px;"><span class="forgetline" data-dismiss="modal" data-target=".myForgotPass" data-toggle="modal" href="#">Forgot Your Password?</span></p>
			</div>
			<div>
				<input type="submit" value="Log in" />
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="<?php echo base_url();?>register">Register</a>
		</div><!-- button -->
		<div><a target="_blank" href="<?php //echo $login_url;?>"><img src="<?php echo base_url();?>b_assets/images/fb3.png" style="width:100%;" /></a></div>
	</section><!-- content -->
</div><!-- container -->
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Skip</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Like</button>
      </div>-->
    </div>
  </div>
</div>
     
<div id="" class="myForgotPass modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<h4 class="modal-title" id="myModalLabel">Forgot Password. </h4>
      </div>
       <div class="modal-body">
        <div class="containerlg">
              <span id="loginerror11"></span>
		<form name="forgotfrm" method="post" action="#" id="forgotfrm">
			<div>
				<input class="txtbx1 form-control"  name="member_email" type="email" placeholder="Enter your Registered email" required />
			</div>
			<div style="margin-top:10px;">
				<button style="width:100%;" type="submit" class="btn btn-lg btn-primary">Send Mail</button>
			</div>
		</form>
	
</div><!-- container -->
      </div>
      
    </div>
  </div>
</div>

    <div id="testtaken" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		
      </div>
       <div class="modal-body">
        <div class="containerlg">
             <span id="loginerror">
		You Have Already Taken Test!
	</span>
</div><!-- container -->
      </div>
      
    </div>
  </div>
</div> 


<div class="modal fade modalsamp2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Choose your Pack </h4>
      </div>
       <div class="modal-body">
	   <!-- left start-->
				<div class="popupLeft">
                               <table class="plandetails">
                               <tr>
                                 <th><img src="<?php echo base_url();?>themes/front/images/six.png" alt="six" class="pull-left" ><h3>Months Plan</h3></th>
                                 <th><img src="<?php echo base_url();?>themes/front/images/one.png" alt="six" class="pull-left" ><h3>Year Plan</h3></th>
                                </tr>
                                <tr>
                                 <td class="rupee"><img src="<?php echo base_url();?>themes/front/images/rupp.png" alt="rupees" class="pull-left" > <h4>200</h4></td>
                                 <td class="rupee"><img src="<?php echo base_url();?>themes/front/images/rupp.png" alt="rupees" class="pull-left" > <h4>300</h4></td>
                                </tr>
                                <tr>
                                 <td class="txxt">Free Question Download</td>
                                 <td class="txxt">Free Question Download</td>
                                </tr>
                                <tr>
                                 <td class="txxt">Notes & Video Sessions</td>
                                 <td class="txxt">Notes & Video Sessions</td>
                                </tr>
                                <tr>
                                 <td class="bottomf"> <button class="btn btn-success">Register Now</button></td>
                                 <td class="bottomf"> <button class="btn btn-success">Register Now</button></td>
                                </tr>
                               </table>
                               
                               </div>
								
								<!--left is end-->
								<!--right start-->
								<div class="rightSide">
                                            <div id="contactFrm">
                                                <div class="rightSideInner"> Need Help? </div>
												<div class="contactUs">
                                                <span class="contactUsText">Contact Us</span>
												<span class="phoneEmail">022-11111111</span>
                                            <spanclass="phoneEmail eamilIcon">pawan.jaswal@gmail.com</span>
                                            </div>
                                            </div>
                                            
                                            
                                        </div>
										<!-- right is end-->
      </div>
    </div>
  </div>
</div>
<!--modal ends-->
     
<script>
    function gettopics(id) {
        var htmlString = "";
                $('#ma').html('<img src="<?php echo base_url();?>themes/front/images/loading_small.gif" />');
                $.ajax({
                url:'<?php echo base_url();?>practicetest/gettopics/'+id,
                type: 'POST',
                datatype:'application/json',
                async: false,
                success:function(data){           
                htmlString+='<table class="tableshot"><tr><td colspan="3" class="classshadow"><div class="list-group" id="cholist"><div class="list-group-item"> <strong>Name of the topic</strong><p class="pull-right"><span class="test">Test</span></p></div>'
                if($.isEmptyObject(data)){
                htmlString+="<tr><td><h3 style='text-align:center'>No Records<span></span></h3></tr></td>"
                }else {
                $.each(data,function(i) {        
               htmlString+= '<div class="list-group-item list-group-item-warning"><strong>'+data[i]["topic_name"]+'</strong><p class="pull-right"><a onclick="gettopicpapers('+data[i]["topic_id"]+')" data-target=".modalsamp" data-toggle="modal" href="#" class="test"><img src="<?php echo base_url();?>b_assets/images/re-copy_04.png" alt="" /></a> </p></div>' 
               //<a href="<?php echo base_url();?>videos/gettopicsvideo/'+data[i]["topic_id"]+'" class="video"><img src="<?php echo base_url();?>b_assets/images/re-copy_02.png" alt="" /></a>
                }); 
                }
                htmlString+="</div></td></tr></table>";

                $("#ma").html(htmlString);       
                }

                });    
                
    }
    function  gettopicpapers(id) {
    
        var htmlString = "";
                $('#abcdefg').html('<img src="<?php echo base_url();?>themes/front/images/loading_small.gif" />');
                $.ajax({
                url:'<?php echo base_url();?>practicetest/gettopicspapers/'+id,
                type: 'POST',
                datatype:'application/json',
                async: false,
                success:function(data){           
                htmlString+=''
                if($.isEmptyObject(data)){
                htmlString+="No Test Papers"
                }else {
                $.each(data,function(i) {
                if(data[i]['test_show'] == 'y') {         
                htmlString+= '<a onclick="return logincheck('+data[i]["test_id"]+');" href="<?php echo base_url();?>practicetest/teststart/'+data[i]["test_id"]+'" class="list-group-item list-group-item-success"> <strong>'+data[i]["test_name"]+'</strong></a>' 
                }else {
                htmlString+= '<a href="#" data-target=".modalsamp2" data-toggle="modal" class="list-group-item list-group-item-danger" data-dismiss="modal"> <strong>'+data[i]["test_name"]+'</strong></a>' 
                }
                }); 
                }
                htmlString+="";

                $("#abcdefg").html(htmlString);       
                }

                });   
   //$("#abcdefg").html("dfghfdkghdkgh"); 
    }
    function logincheck(id){
    
   <?php if($this->session->userdata('buser_id') != '') {  ?>
           var a;
             $.ajax({
             url:"<?php echo base_url(); ?>practicetest/checktestuser",
             type: "POST",
             async: false,
             data: {'tid' : id},
             success: function(response) {
              a = response;
             if(response == '0') {
             $('#testtaken').modal();
             return false;
             }else {
             return true;
             }
             }          
            });
            if(a == '0') {
                 return false;
            }else {
                return true; 
            }
                
            
            
       <?php   } else {  ?>
       $.ajax({
             url:"<?php echo base_url(); ?>user/getseiion",
             type: "POST",
              async: false,
             //data: $(form).serialize(),
             success: function(response) {
              if(response == '1') {
               $('#myModal').modal();  
                return false;
             }else {
                 return true;
             }
             }          
            });
            return false;
  <?php } ?>
    }
    
</script>

<script type="text/javascript">
$(document).ready(function(){
    

  $("#puzzleform").validate({
       rules: {
         r1: {
            required: true
	  },
          user_answer: {
            required: true,
           
	  },
      	
    },
    
    
});

$("#puzzleloginform").validate({
       rules: {
         email: {
            required: true,
             email:true
	  },
          password: {
            required: true,
           
	  },
      },
      submitHandler: function(form) {
                    $.ajax({
                        url:"<?php echo base_url(); ?>user/puzzleloginsubmit",
                        type: "POST",
                        data: $(form).serialize(),
                        success: function(response) {
                         if(response == '2') {
                        $('#loginerror').html("NO USER ID EXIST WITH THIS MAIL ID!");
                        } else if(response == '1') {
                        $('#loginerror').html("INCORRECT USER ID or PASSWORD, TRY AGAIN!");
                         }else { 
                           window.location.reload(true);
                         }
                        }            
                    });
                    return false;
                }
    
});
});
</script>
	

<script type="text/javascript">
$(document).ready(function(){
    
$("#forgotfrm").validate({
       rules: {
         email: {
            required: true,
             email:true
	  },
          
      },
      submitHandler: function(form) {
     
                 $.ajax({
                        url:"<?php echo base_url(); ?>register/forgotpassword",
                        type: "POST",
                        data: $(form).serialize(),
                        success: function(response) {
                        // alert(response)
                        if(response == '1') {
                        $('#loginerror11').html("NO USER ID EXIST WITH THIS MAIL ID!");
                        }else { 
                          $('#loginerror11').html("The Password has been sent to your email address");
                          //setTimeout(function() { $('#dialog').hide(); }, 10000);
                          //$('#myModal').modal('hide');
                          // window.location.reload(true);
                         }
                        }            
                    });
                    return false;
                }
    
});
});
</script>