<div class="container">
<div class="graybg">
<div class="testcss"><img src="<?php echo base_url();?>themes/front/images/logo.png"  class="logo" style="margin-top:0;" alt="" /> <h3 class="testTitle">Score Card Answer With Review</h3>
<a href="#" class="btnClick" title="download pdf" onClick="alert('Download Score Card?')"><span class="glyphicon glyphicon-save"></span> </a> <a class="btnClick" href="<?php echo base_url();?>" style="padding-right:28px;"> <span class="glyphicon glyphicon-home"></span></a>
</div>
<div class="col-xs-18 col-sm-3 col-md-3 col-lg-3">
<div class="profilepic">
	<span class="ppicsNum">0<span class="ppicsPercent">%</span></span>
</div>
<h5><strong>Questions</strong></h5>
<ul class="listQuestionde nav nav-tabs" role="tablist" id="sdTx">
    <?php
$j=1;
$x=1;
foreach($testresult as $val) {
?><li class="<?php if($x==$j) echo "active";?>">
<a data-toggle="tab" role="tab" href="#scq<?php echo $j;?>">
Q<?php echo $j;?>. 
<?php
if($val->attempet == '1') {
?>
<?php
if($answer[$val->id]->question_answer == $val->answer){
?>
<span class="correct" style="display:inline-block;"></span></a></li>
<?php }else { ?>
<span class="wrong" style="display:inline-block;"></span></a></li>
<?php } ?>

<?php } else { ?>
<span class="notcomplete" style="display:inline-block;"></span></a></li>

<?php } ?>
<?php
$j++;}
?>

</ul>
</div>
<div class="col-xs-18 col-sm-9 col-md-9 col-lg-9 perchart">
<h3>As Your Have Answered</h3>
<div class="testsection tab-content">
    
<?php
$j=1;
$x=1;
foreach($testresult as $value) {
?>
<div class="tab-pane <?php if($x==$j) echo "active";?>" id="scq<?php echo $j;?>">
<div class="col-xs-18 col-sm-8 col-md-8 col-lg-8" style="padding-left:0">
<div class="paragraphtest">
	<ul class="pagination-content pp2p">
	<li>
	<?php
           if($answer[$value->id]->passage_id !='') {
            echo $passage[$value->id]->passage;
            } else { 
            echo $answer[$value->id]->question_name;
            } 
        ?>

	</li>
	
	</ul>
	<div class="explainsection">
	<h3><span class="glyphicon glyphicon-file"></span> Answer Explained</h3>
	<div class="ansbox">
            <?php  echo $answer[$value->id]->question_explanation;?>
        </div>
	
	</div>
</div>

</div>
<div class="kalps col-xs-18 col-sm-4 col-md-4 col-lg-4">
	
	<?php if($answer[$value->id]->passage_id !='') { ?>
	<div class="que">
	<h4>Question</h4>
	<?php  echo $answer[$value->id]->question_name;?> 
       </div>
        <?php } ?>
	<div class="ops">
		<h4>Options</h4>
		  <?php if($answer[$value->id]->option1 !='') { ?>
		<p><input  type="radio" name="opt_<?php echo $answer[$value->id]->q_id;?>" value="1" /><?php echo $answer[$value->id]->option1;?></p>
                <?php } if($answer[$value->id]->option2 !='') {?>
                <p><input  type="radio" name="opt_<?php echo $answer[$value->id]->q_id;?>" value="2" /><?php echo $answer[$value->id]->option2;?></p>
                <?php } if($answer[$value->id]->option3 !='')  { ?>
                <p><input  type="radio" name="opt_<?php echo $answer[$value->id]->q_id;?>" value="3" /><?php echo $answer[$value->id]->option3;?></p>
                <?php } if($answer[$value->id]->option4 !='')  { ?>
		<p><input type="radio" name="opt_<?php echo $answer[$value->id]->q_id;?>" value="4" /><?php echo $answer[$value->id]->option4;?></p>
                <?php } if($answer[$value->id]->option5 !='')  { ?>
                <p><input  type="radio" name="opt_<?php echo $answer[$value->id]->q_id;?>" value="5" /><?php echo $answer[$value->id]->option5;?></p>
                <?php } ?>
	</div>
	<div class="que">
		<p class="pull-right pagin"><a href="#" class="pv">Previous</a> <a href="#" class="nx">Next</a></p>
	</div>
</div>
</div>
<?php
$j++;}
?>
</div>

</div>


  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/TimeCircles.js"></script>
	<script src="js/custom.js"></script>
	<script>
$(document).ready(function() {	
				
		//ScreDetails page
		$('#sdTx a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
		var NumListNum = $('#sdTx').length;
		
		$('.nx').click(function() {
			if($(this).parents('.tab-pane').next().length < NumListNum) {
				}
			else {
			$(this).parents('.tab-pane').removeClass('active');
			$(this).parents('.tab-pane').next().addClass('active');
				$(this).each(function() {
					$('#sdTx').find('.active').removeClass().next().addClass('active');
				});
			}
		});
		$('.pv').click(function() {
			if($(this).parents('.tab-pane').prev().length === 0) {
			}
			else {
		    $(this).parents('.tab-pane').removeClass('active');
			$(this).parents('.tab-pane').prev().addClass('active');
				$(this).each(function() {
					$('#sdTx').find('.active').removeClass().prev().addClass('active');
				});
			}
		});
});
</script>
    
	

	
  </body>
</html>