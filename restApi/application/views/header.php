<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>App Document Version 1.0</title>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/resources/style.css?e99947befd7bf673c6b43ff75e9e0f170c88a60e">

</head>

<body>
<div id="left">
	<div id="menu">
		<a href="<?php echo base_url(); ?>Document_v1" title="Overview"><span>Home</span></a>


		<div id="groups">
				<h3>Service Urls</h3>

				<?php  $url = $this->uri->segment(2);?>
			<ul>
				<li <?php if($url == 'login') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/login">
						Login<span></span>
					</a>
					</li>
				<li>
							
				 <li <?php if($url == 'dashboard') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/dashboard">
					Dashboard
					</a>
				</li>
				<li <?php if($url == 'changePassword') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/chamagePassword">
					Change Password
					</a>
				</li>
				<li <?php if($url == 'weekly') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/weekly">
					Weekly Schedule
					</a>
				</li>
				<li <?php if($url == 'videos') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/videoscat">
					All Videos Categories
					</a>
				</li>
				<li <?php if($url == 'catvideos') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/catvideos">
					Get Category Videos
					</a>
				</li>
				
				<li <?php if($url == 'videoviews') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/videoviews">
					Update Video Views
					</a>
				</li>
				<li <?php if($url == 'enquiry') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/enquiry">
					Enquiry (join Brainwiz)
					</a>
				</li>
				<li <?php if($url == 'notifications') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/notifications">
					Notifications List
					</a>
				</li>
				<li <?php if($url == 'notificationsUpdate') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/notificationsUpdate">
					Notification View Update
					</a>
				</li>
				
				<li <?php if($url == 'checkMobile') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/checkMobile">
					Check Mobile Number
					</a>
				</li>
				<li <?php if($url == 'setPassword') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/setPassword">
					Set Password
					</a>
				</li>

				 <li <?php if($url == 'register') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/register">
					Register
					</a>
				</li>
				 <li <?php if($url == 'practiceTest') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/practiceTest">
					All practiceTest Categories(screen 1)
					</a>
				</li>
				<li <?php if($url == 'practiceTest2') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/practiceTest2">
					All practiceTest topic And testes info(screen 2)
					</a>
				</li>
				<li <?php if($url == 'questions') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/questions">
					All practiceTest topic Questions(screen 3)
					</a>
				</li>
				<li <?php if($url == 'viewSummery') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/viewSummery">
					Test Review Summery (screen 5)
					</a>
				</li>
				
				<li <?php if($url == 'getScore') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/getScore">
					get Score (screen 4)
					</a>
				</li>
				<li <?php if($url == 'saveAnswers') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/saveAnswers">
					Save Test Answers Data
					</a>
				</li>

				
				<!--<li <?php if($url == 'videos') { ?>class="active" <?php } ?>>
					<a href="#">
					
					</a>
				</li>
				<li <?php if($url == 'workmapprofile') { ?>class="active" <?php } ?>>
					<a href="#">
					Check Mobile Number When Brainwiz student register
					</a>
				</li> -->
				
				<!--<li <?php if($url == 'zonemap') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/zonemap">
					Zone Map Details(Ignore This url)
					</a>
				</li>
				<li <?php if($url == 'propertyDetails') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/propertyDetails">
					Property Details
					</a>
				</li>
				<li <?php if($url == 'getZones') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/getZones">
					Get Zones ( For Add New Project)
					</a>
				</li>
				<li <?php if($url == 'addProperty') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/addProperty">
					Add Property
					</a>
				</li>
				<li>
					<a href="#">
					Regular Update
					</a>
				</li>
				<li <?php if($url == 'employeeTrack') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeTrack">
					Employee Tavel Tracking Save
					</a>
				</li>
				<li <?php if($url == 'getEmployees') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/getEmployees">
					get Employee's
					</a>
				</li>
				<li <?php if($url == 'employeeTrackShow') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeTrackShow">
					Employee Travel Tracking View
					</a>
				</li>
				<li <?php if($url == 'employeeProjectworkView') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeProjectworkView">
					Employee Work Tracking View
					</a>
				</li>
				<li <?php if($url == 'teamView') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/teamView">
					Team View
					</a>
				</li>
				<li <?php if($url == 'reports') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/reports">
					Reports
					</a>
				</li>

				<li <?php if($url == 'rating') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/rating">
					Property Rating 
					</a>
				</li>
				
				<li <?php if($url == 'checkout') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/checkout">
					Check Out Process
					</a>
				</li>
				<li <?php if($url == 'logout') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/logout">
						Logout<span></span>
					</a>
				</li>	 --> 	
		</div>

		<hr>


		
	</div>
</div>

<div id="splitter"></div>

<div id="right">
<div id="rightInner">