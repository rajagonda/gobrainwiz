<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
* @emplogin 
* This function will use save and update login information into login tabl
* @param userid integer
* @param imei string
* @return string
*/

if(! function_exists('studentlogin')){

  function studentlogin($userId=NULL,$imei=NULL){   
         $ci =& get_instance();
    if($userId !=NULL){
           $ci->load->model('Custom_model');
             
           //check before login or not
           $res = $ci->Custom_model->checkLogin($userId);   
           if($res){

            $accessToken = $res->accessToken;

            $token = createToken($userId);

            $updateValues = array(                                  
                                  'loginTime' => date('Y-m-d H:i:s'),                                  
                                  'accessToken' => $token                                       
                                   );

            $result = $ci->Custom_model->updateLogin($updateValues,$userId,$accessToken);
             return $token;

           }else {

            $token = createToken($userId);
           
              //insert
             $insertValues = array( 
                                  'emp_id' =>$userId, 
                                  'loginTime' => date('Y-m-d H:i:s'),                                  
                                  'accessToken' => $token                                       
                                   );

             $result = $ci->Custom_model->insertLogin($insertValues);
           
           return $token;

           }        
           
    }else {
      return false;
    }

  }
}
/*
* @createToken
* This function will generte accessToken 
*
* @param userId integer
* @param imei string
*
* @return string
*/
if(! function_exists('createToken')) {
    
    function createToken($userId=NULL){

      if($userId !=NULL){
              $tokenString = $userId.time();
              $accessToken = md5($tokenString);
              return $accessToken;              
      }else {
        return false;
      }
    }
}

/*
* @checkLogin
* This function will check access token and login 
* If time is more than 6hr to update time regenerate access token and return back to resposne
* 
* @param userId integer
* @param accessToken string
* @imei  string
*
* @return true or false
* @return accessToken
*/
if(! function_exists('checkLogin')){
  function checkLogin($userId=NULL,$accessToken=NULL){
        $ci =& get_instance();
    if($userId !=NULL){
           $ci->load->model('Custom_model');

           $res = $ci->Custom_model->checkLogin($userId);           
           if($res){
            // echo $res->accessToken;
            // echo "<br>";
            // echo $accessToken;
            // exit;
             if($res->accessToken == $accessToken){
                  return true;
             }else {
              return false;
             }
           }else {
             return false;
           }
        }

  }
}

if(!function_exists('smssend_new')){
    function smssend_new($mobileNumber = NULL,$msg){
      if($mobileNumber!=null){
        
        $senderid = "BRNWIZ"; 
        $apiurl = "http://sms.teleparishkar.com/SMS_API/sendsms.php";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,  $apiurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=BRNWIZ&password=123456&mobile=".$mobileNumber."&sendername=".$senderid."&message=".$msg."&routetype=1");
        $buffer = curl_exec($ch);            
        curl_close($ch);
        return true;
       
           

      }
    }
}


?>