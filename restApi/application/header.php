<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>App Document Version 1.0</title>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/resources/style.css?e99947befd7bf673c6b43ff75e9e0f170c88a60e">

</head>

<body>
<div id="left">
	<div id="menu">
		<a href="<?php echo base_url(); ?>Document_v1" title="Overview"><span>Home</span></a>


		<div id="groups">
				<h3>Service Urls</h3>

				<?php  $url = $this->uri->segment(2);?>
			<ul>
				<li <?php if($url == 'login') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/login">
						Login<span></span>
					</a>
					</li>
				<li>
							
				 <li <?php if($url == 'dashboard') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/dashboard">
					Dashboard
					</a>
				</li>
				<li <?php if($url == 'changePassword') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/chamagePassword">
					Change Password
					</a>
				</li>
				<li <?php if($url == 'weekly') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/weekly">
					Weekly Schedule
					</a>
				</li>
				<li <?php if($url == 'videos') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/videoscat">
					All Videos Categories
					</a>
				</li>
				<li <?php if($url == 'catvideos') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/catvideos">
					Get Category Videos
					</a>
				</li>
				
				<li <?php if($url == 'videoviews') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/videoviews">
					Update Video Views
					</a>
				</li>
				<!-- <li <?php if($url == 'workmapprofile') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/workmapprofile">
					Work Map And Profile (Ignore This url)
					</a>
				</li>
				<li <?php if($url == 'zonemapcount') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/zonemapcount">
					Zone Map Count
					</a>
				</li>
				<li <?php if($url == 'zonemapproperties') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/zonemapproperties">
					Zone Map Properties
					</a>
				</li> -->
				<!--<li <?php if($url == 'zonemap') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/zonemap">
					Zone Map Details(Ignore This url)
					</a>
				</li>
				<li <?php if($url == 'propertyDetails') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/propertyDetails">
					Property Details
					</a>
				</li>
				<li <?php if($url == 'getZones') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/getZones">
					Get Zones ( For Add New Project)
					</a>
				</li>
				<li <?php if($url == 'addProperty') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/addProperty">
					Add Property
					</a>
				</li>
				<li>
					<a href="#">
					Regular Update
					</a>
				</li>
				<li <?php if($url == 'employeeTrack') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeTrack">
					Employee Tavel Tracking Save
					</a>
				</li>
				<li <?php if($url == 'getEmployees') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/getEmployees">
					get Employee's
					</a>
				</li>
				<li <?php if($url == 'employeeTrackShow') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeTrackShow">
					Employee Travel Tracking View
					</a>
				</li>
				<li <?php if($url == 'employeeProjectworkView') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/employeeProjectworkView">
					Employee Work Tracking View
					</a>
				</li>
				<li <?php if($url == 'teamView') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/teamView">
					Team View
					</a>
				</li>
				<li <?php if($url == 'reports') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/reports">
					Reports
					</a>
				</li>

				<li <?php if($url == 'rating') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/rating">
					Property Rating 
					</a>
				</li>
				
				<li <?php if($url == 'checkout') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/checkout">
					Check Out Process
					</a>
				</li>
				<li <?php if($url == 'logout') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>Document_v1/logout">
						Logout<span></span>
					</a>
				</li>	 --> 	
		</div>

		<hr>


		
	</div>
</div>

<div id="splitter"></div>

<div id="right">
<div id="rightInner">