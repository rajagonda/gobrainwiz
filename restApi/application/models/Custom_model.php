<?php
class Custom_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    } 
    public function checkLogin($userId=NULL){
    	if($userId !=NULL){
             $sql = "SELECT * FROM emp_login WHERE emp_id='$userId' ORDER BY id DESC limit 1";
             $res = $this->db->query($sql);
             if($res->num_rows()>0){
             	return $res->row();
             }else {
             	return false;
             }
    	}else {
    		return false;
    	}
    }
    public function insertLogin($data){
    	$query=$this->db->insert('emp_login', $data);
        if($query){
            return $this->db->insert_id();
        }else {
            return FALSE;  
        }
    	
    }
    public function updateLogin($data, $uid,$accessToken){
        $this->db->where('accessToken', $accessToken);
    	$this->db->where('emp_id', $uid);
    	$this->db->update('emp_login', $data);
    	return TRUE;  
    }
    /*
    * @$tablename : tablename
    *  @params  : array
    * Usage : here we wil get single row result from table with conditions or with out conditions
    * Return : array data
    */
    public function getSingleRow($tablename, $params = array()){
       if(sizeof($params) >0) {
       foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
       }
       $query = $this->db->get($tablename);
       if($query->num_rows() > 0) {
        return $query->row();
       }else {
        return false;
       }    
    }

      

}