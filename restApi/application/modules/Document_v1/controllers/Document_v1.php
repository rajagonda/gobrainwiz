<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Document_v1 extends MX_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();       
      
    }

    public function index() {
        
        $this->load->view('header');
        $this->load->view('welcome');        
        $this->load->view('footer');        
    }
    public function login(){
        $this->load->view('header');
        $this->load->view('login');        
        $this->load->view('footer');           
    }
      public function dashboard(){
        $this->load->view('header');
        $this->load->view('dashboard');        
        $this->load->view('footer');           
    }
     public function chamagePassword(){
        $this->load->view('header');
        $this->load->view('chamagePassword');        
        $this->load->view('footer');           
    }
     public function weekly(){
        $this->load->view('header');
        $this->load->view('weekly');        
        $this->load->view('footer');           
    }
     public function videoscat(){
        $this->load->view('header');
        $this->load->view('videosCategories');        
        $this->load->view('footer');           
    }
     public function catvideos(){
        $this->load->view('header');
        $this->load->view('catvideos');        
        $this->load->view('footer');           
    }
     public function videoviews(){
        $this->load->view('header');
        $this->load->view('videoviews');        
        $this->load->view('footer');           
    }
    public function notifications(){
        $this->load->view('header');
        $this->load->view('notifications');        
        $this->load->view('footer');           
    }
    public function notificationsUpdate(){
        $this->load->view('header');
        $this->load->view('notificationsupdate');        
        $this->load->view('footer');           
    }
    public function enquiry(){
        $this->load->view('header');
        $this->load->view('enquiry');        
        $this->load->view('footer');           
    }
    public function checkMobile(){
        $this->load->view('header');
        $this->load->view('checkMobile');        
        $this->load->view('footer');           
    }
    public function setPassword(){
        $this->load->view('header');
        $this->load->view('setPassword');        
        $this->load->view('footer');           
    }
    public function register(){
        $this->load->view('header');
        $this->load->view('register');        
        $this->load->view('footer');           
    }
    public function practiceTest(){
        $this->load->view('header');
        $this->load->view('practiceTest');        
        $this->load->view('footer');           
    }
    public function practiceTest2(){
        $this->load->view('header');
        $this->load->view('practiceTest2');        
        $this->load->view('footer');           
    }
    public function questions(){
        $this->load->view('header');
        $this->load->view('questions');        
        $this->load->view('footer');           
    }
    public function viewSummery(){
        $this->load->view('header');
        $this->load->view('viewSummery');        
        $this->load->view('footer');           
    }
    public function saveAnswers(){
        $this->load->view('header');
        $this->load->view('saveAnswers');        
        $this->load->view('footer');           
    }
    public function getScore(){
        $this->load->view('header');
        $this->load->view('getScore');        
        $this->load->view('footer');           
    }
    
    

}