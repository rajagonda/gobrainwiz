<div id="content" class="class">
	<h1>login Url Information</h1>


	<div class="description">
	<p>This page will provide login url information</p>
	</div>

	<div class="info">
		<b>Information</b><br>
		
		
		<b>Package:</b> <a href="#">Web</a><a href="#">Services</a><br>

				<b>Category:</b>
				Mobile App<br>
				<b>Mode:</b>
				Testing<br>
				<b>License:</b>
				Go Brainwiz<br>				
				<b>Author:</b>
				Venkata Sudhakar Challagulla<br>
				<b>Version:</b>
				1.0.0<br>
				<b>Type:</b> <a href="#">Post</a><br>
				<b>Url:</b>
				<a href="#"><?php echo base_url();?>App/User/login</a><br>
			<b>Located at</b> <a href="#" title="Go to source code">App</a>
		<br>
	</div>



	<table class="summary methods" id="methods">
	<caption>IN Parameters:</caption>
	
	<tr data-order="put" id="_put">

		<td class="attributes"><code>
			userName
			</code>
		</td>

		<td class="name"><div>
		
		<code>Pass user Id What user will enter in login screen</code>
      </div></td>
	</tr>

	<tr data-order="put" id="_put">

		<td class="attributes"><code>
			password
			</code>
		</td>

		<td class="name"><div>
		
		<code>Pass password What user will enter in login screen</code>
      </div></td>
	</tr>
	<tr data-order="put" id="_put">

		<td class="attributes"><code>
			notification_token
			</code>
		</td>

		<td class="name"><div>
		
		<code>Pass notification_token </code>
      </div></td>
	</tr>

	
	<!-- <tr data-order="put" id="_put">

		<td class="attributes"><code>
			imei
			</code>
		</td>

		<td class="name"><div>
		
		<code>Pass Mobile Unique IMEI Number </code>
      </div></td>
	</tr> -->
	</table>








	<table class="summary constants" id="constants">
	<caption>Out Parameters Summary</caption>
	<tr data-order="HTTP_CONTINUE" id="HTTP_CONTINUE">

		
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>status</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Will give always TRUE OR FALSE</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>message</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Return will give string message for related to call</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>data</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>data parameter will come based on status if status parameter resposne is true  data parameter will come other wise data parameter will not came</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	</table>




	<table class="summary properties" id="properties">
	<caption>Response Information</caption>
	<tr>
	<td>
	<b>Status</b>
	</td>    
	<td>
	<b>Message</b>
	</td>	
	<td>
	<b>Resposne Data</b>
	</td>
	</tr>
	<tr data-order="_start_rtime" id="$_start_rtime">
		<td class="attributes"><code>
			true
		</code></td>

		<td class="name">
				

			<div class="description short">
				<p>Successfully Login</p>
			</div>			
		</td>
		<td class="value">
			<div>
				<a href="#$_start_rtime" class="anchor">#</a>
				<code><span class="php-quote">{
    "status": true,
    "message": "Successfully Get Data",
    "data": {
        "notificationsCount": 17,
        "profileData": {
            "userId": "6002",
            "name": "venkat",
            "mobile": "8686994774",
            "email": "ch.v.sudhakar9@gmail.com",
            "password": "$2a$08$K6029WuxP8SX8tnMMJF5NOW67qJLt5k7Ri8aNzcJUluCD1eP6Sn46",
            "userType": "y",
            "profilepic": "public/profilepics/"
        },
        "accessToken": "27fef4f170aa16c1cf34e5702ec951dc"
    }
}
</span></code>
			</div>
		</td>
	</tr>
	<tr data-order="_start_rtime" id="$_start_rtime">
		<td class="attributes"><code>
			false
		</code></td>

		<td class="name">
				

			<div class="description short">
				<p>Invalid User Id</p>
			</div>			
		</td>
		<td class="value">
			<div>
				<a href="#$_start_rtime" class="anchor">#</a>
				<code><span class="php-quote">
{
    "status": false,
    "code": "400",
    "message": "Invalid Mobile Number"
}
				</span></code>
			</div>
		</td>
	</tr>
	<tr data-order="_start_rtime" id="$_start_rtime">
		<td class="attributes"><code>
			false
		</code></td>

		<td class="name">
				

			<div class="description short">
				<p>Invalid Password</p>
			</div>			
		</td>
		<td class="value">
			<div>
				<a href="#$_start_rtime" class="anchor">#</a>
				<code><span class="php-quote">
{
    "status": false,
    "code": "400",
    "message": "Invalid Password"
}
				</span></code>
			</div>
		</td>
	</tr>

	

	</table>

	<table class="properties" id="properties">
	<caption>Screen Shorts </caption>
 
	<tr >
		<td class="">
			<img src="<?php echo base_url();?>assets/images/login1.png" height="500px" width="800x">
		</td>
	</tr>	
	<tr >
		<td class="">
			<img src="<?php echo base_url();?>assets/images/login2.png" height="500px" width="800x">
		</td>
	</tr>	
	<tr >
		<td class="">
			<img src="<?php echo base_url();?>assets/images/login.png" height="500px" width="800x">
		</td>
	</tr>	 



	</table>

</div>