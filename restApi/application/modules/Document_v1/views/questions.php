<div id="content" class="class">
	<h1>All Questions  List Information</h1>


	<div class="description">
	<p>This page will provide All Questions List  information</p>	
	</div>

	
    <div class="info">
        <b>Information</b><br>
        
        
        <b>Package:</b> <a href="#">Web</a><a href="#">Services</a><br>

                <b>Category:</b>
                Mobile App<br>
                <b>Mode:</b>
                Testing<br>
                <b>License:</b>
                Go Brainwiz<br>             
                <b>Author:</b>
                Venkata Sudhakar Challagulla<br>
                <b>Version:</b>
                1.0.0<br>
                <b>Type:</b> <a href="#">Post</a><br>
                <b>Url:</b>
                <a href="#"><?php echo base_url();?>App/Practicetest/getQuetionsList</a><br>
            <b>Located at</b> <a href="#" title="Go to source code">App</a>
        <br>
    </div>



	 <table class="summary methods" id="methods">
    <caption>IN Parameters:</caption>
    
    <tr data-order="put" id="_put">

        <td class="attributes"><code>
            userId
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass user Id where login url return user id information</code>
      </div></td>
    </tr>

    <tr data-order="put" id="_put">

        <td class="attributes"><code>
            accessToken
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass accessToken where login url return accessToken information</code>
      </div></td>
    </tr> 

     <tr data-order="put" id="_put">

        <td class="attributes"><code>
            topicId
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass topicId </code>
      </div></td>
    </tr>  

     <tr data-order="put" id="_put">

        <td class="attributes"><code>
            testId
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass testId </code>
      </div></td>
    </tr>      
    
    
        </table>







	<table class="summary constants" id="constants">
	<caption>Out Parameters Summary</caption>
	<tr data-order="HTTP_CONTINUE" id="HTTP_CONTINUE">

		
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>status</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Will give always TRUE OR FALSE</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>message</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Return will give string message for related to call</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>data</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>data parameter will come based on status if status parameter resposne is true  data parameter will come other wise data parameter will not came</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	</table>




    <table class="summary properties" id="properties">
    <caption>Response Information</caption>
    <tr>
    <td>
    <b>Status</b>
    </td>    
    <td>
    <b>Message</b>
    </td>   
    <td>
    <b>Resposne Data</b>
    </td>
    </tr>
    <tr data-order="_start_rtime" id="$_start_rtime">
        <td class="attributes"><code>
            true
        </code></td>

        <td class="name">
                

            <div class="description short">
                <p>Successfully Get Data</p>
            </div>          
        </td>
        <td class="value">
            <div>
                <a href="#$_start_rtime" class="anchor">#</a>
                <code><span class="php-quote">
           {
    "status": true,
    "message": "Successfully Get Data",
    "data": [
        {
            "q_id": "1",
            "question_name": "<p>What PHP?</p>\n",
            "option1": "<p>programming language</p>\n",
            "option2": "<p>Personal home page</p>\n",
            "option3": "<p>Server side language</p>\n",
            "option4": "<p>none</p>\n",
            "option5": ""
        },
        {
            "q_id": "2",
            "question_name": "<p>12 persons can make 28 toys in 8 days, in how many days 16 persons make 42 toys?</p>\n",
            "option1": "<p>10 days</p>\n",
            "option2": "<p>8 days</p>\n",
            "option3": "<p>9 days</p>\n",
            "option4": "<p>10 days</p>\n",
            "option5": "<p>None</p>\n"
        },
        {
            "q_id": "3",
            "question_name": "<p><strong>36 men can complete a piece of work in 18 days. In how many days will 27 men complte the same work?</strong></p>\n",
            "option1": "<p>12 days</p>\n",
            "option2": "<p>18 days</p>\n",
            "option3": "<p>22 days</p>\n",
            "option4": "<p>24 days</p>\n",
            "option5": "<p>None</p>\n"
        },
        {
            "q_id": "4",
            "question_name": "<p><strong>Directions: Read the following carefully and answer the questions that follow. </strong></p>\n\n<p>Among A, B, C, D and E, E is taller than D but not as fat as D. C is taller than A but shorter than B. a is fatter than D but not as fat as B. E is thinner than C who is thinner than D. E is shorter than A. In these questions two rankings one related with height and other with figures of persons are given. On the basis of information, these are ranked in descending order as below:&nbsp;</p>\n\n<p><strong>In the above question, who is the tallest?</strong></p>\n",
            "option1": "<p>A</p>\n",
            "option2": "<p>C</p>\n",
            "option3": "<p>B</p>\n",
            "option4": "<p>D</p>\n",
            "option5": "<p>E</p>\n"
        },
        {
            "q_id": "5",
            "question_name": "<p><strong>Find the number of Prime factors in the given expression </strong></p>\n\n<p><strong>12<sup>2 </sup>&times; 5<sup>4</sup> &times; 34<sup>8</sup> &times; 19<sup>10&nbsp; </sup></strong></p>\n",
            "option1": "<p>4</p>\n",
            "option2": "<p>5</p>\n",
            "option3": "<p>6</p>\n",
            "option4": "<p>10</p>\n",
            "option5": "<p>None</p>\n"
        },
                </span></code>
            </div>
        </td>
    </tr>
    <tr data-order="_start_rtime" id="$_start_rtime">
        <td class="attributes"><code>
            false
        </code></td>

        <td class="name">
                

            <div class="description short">
                <p>accessToken Mismatch</p>
            </div>          
        </td>
        <td class="value">
            <div>
                <a href="#$_start_rtime" class="anchor">#</a>
                <code><span class="php-quote">
{
    "status": false,
    "code": "400",
    "message": "accessToken Mismatch"
}
                </span></code>
            </div>
        </td>
    </tr>
    

    </table>

	<table class="properties" id="properties">
	<caption>Screen Shorts </caption>

	<tr >
		<td class="">
			<img src="<?php echo base_url();?>assets/images/questions.png" height="500px" width="800x">
		</td>
	</tr>	

  
	



	</table>

</div>