<div id="content" class="class">
	<h1>Change Password url Information</h1>


	<div class="description">
	<p>This page will provide Change Password Url information</p>	
	</div>

	<div class="info">
        <b>Information</b><br>
        
        
        <b>Package:</b> <a href="#">Web</a><a href="#">Services</a><br>

                <b>Category:</b>
                Mobile App<br>
                <b>Mode:</b>
                Testing<br>
                <b>License:</b>
                Go Brainwiz<br>             
                <b>Author:</b>
                Venkata Sudhakar Challagulla<br>
                <b>Version:</b>
                1.0.0<br>
                <b>Type:</b> <a href="#">Post</a><br>
                <b>Url:</b>
                <a href="#"><?php echo base_url();?>App/User/changePassword</a><br>
            <b>Located at</b> <a href="#" title="Go to source code">App</a>
        <br>
    </div>



	 <table class="summary methods" id="methods">
    <caption>IN Parameters:</caption>
    
    <tr data-order="put" id="_put">

        <td class="attributes"><code>
            userId
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass user Id where login url return user id information</code>
      </div></td>
    </tr>

    <tr data-order="put" id="_put">

        <td class="attributes"><code>
            accessToken
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass accessToken where login url return accessToken information</code>
      </div></td>
    </tr>
  <!--   <tr data-order="put" id="_put">

        <td class="attributes"><code>
            imei
            </code>
        </td>

        <td class="name"><div>
        
        <code>Pass Mobile Unique IMEI Number </code>
      </div></td>
    </tr> -->
    
    </table>



	<table class="summary constants" id="constants">
	<caption>Out Parameters Summary</caption>
	<tr data-order="HTTP_CONTINUE" id="HTTP_CONTINUE">

		
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>status</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Will give always TRUE OR FALSE</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>message</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>Return will give string message for related to call</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	<tr data-order="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE" id="HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE">

		<td class="attributes"><code>data</code></td>
		<td class="name">
			<code>
				<a href="#" title="Go to source code"><b>data parameter will come based on status if status parameter resposne is true  data parameter will come other wise data parameter will not came</b></a>
			</code>

			<div class="description short">
				
			</div>

			<div class="description detailed hidden">
				

			</div>
		</td>
		
	</tr>
	</table>




    <table class="summary properties" id="properties">
    <caption>Response Information</caption>
    <tr>
    <td>
    <b>Status</b>
    </td>    
    <td>
    <b>Message</b>
    </td>   
    <td>
    <b>Resposne Data</b>
    </td>
    </tr>
    <tr data-order="_start_rtime" id="$_start_rtime">
        <td class="attributes"><code>
            true
        </code></td>

        <td class="name">
                

            <div class="description short">
                <p>Successfully Get Data</p>
            </div>          
        </td>
        <td class="value">
            <div>
                <a href="#$_start_rtime" class="anchor">#</a>
                <code><span class="php-quote">
                  {
    "status": true,
    "message": "Successfully Changed Password"
}
                </span></code>
            </div>
        </td>
    </tr>
    <tr data-order="_start_rtime" id="$_start_rtime">
        <td class="attributes"><code>
            false
        </code></td>

        <td class="name">
                

            <div class="description short">
                <p>Invalid User Id OR accessToken</p>
            </div>          
        </td>
        <td class="value">
            <div>
                <a href="#$_start_rtime" class="anchor">#</a>
                <code><span class="php-quote">
{
    "status": false,
    "code": "400",
    "message": "accessToken Mismatch"
}
                </span></code>
            </div>
        </td>
    </tr>
    

    </table>

	<table class="properties" id="properties">
	<caption>Screen Shorts </caption>

	<tr >
		<td class="">
			<img src="<?php echo base_url();?>assets/images/changepasword.png" height="500px" width="800x">
		</td>
	</tr>	
    <tr >
        <td class="">
            <img src="<?php echo base_url();?>assets/images/changepasword1.png" height="500px" width="800x">
        </td>
    </tr>   
	



	</table>

</div>