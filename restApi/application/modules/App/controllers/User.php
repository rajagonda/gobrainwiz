<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class User extends REST_Controller
{

    public function __construct()
    {

     

        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model', 'Custom_model', 'User_model'));
        $this->load->helper('sendmail_helper');
        $this->load->helper('phpass_helper');
        $this->load->helper('common_helper');
        $this->load->library(array('authentication'));
    }

    public function login_post()
    {
        if ($_POST) {

            $userName = $this->input->post('userName');
            $password = $this->input->post('password');
            $notification_token = $this->input->post('notification_token');

            //write internal andriod developer validations
            if ($userName == '' || $userName == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userName Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            if (!(preg_match('/^[0-9]{10}+$/', $userName))) {
                $this->response([
                    'status' => false,
                    'code' => '200',
                    'message' => 'Invalid Mobile Number',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            //write internal andriod developer validations
            if ($password == '' || $password == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'password Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $userDetails = $this->Common_model->getUserDetailsWithMobile($userName);
            if ($userDetails) {

                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                if ($hasher->CheckPassword($password, $userDetails->password)) {

                    $notificationsCount = $this->Common_model->getNotificationsCount($userDetails->userId);

                    if (!$notificationsCount) {
                        $resData['notificationsCount'] = '0';
                    } else {
                        $resData['notificationsCount'] = $notificationsCount;
                    }
                    $resData['profileData'] = $userDetails;

                    $token = studentlogin($userDetails->userId);
                    $resData['accessToken'] = $token;

                    if ($notification_token != $userDetails->notification_token) { //reg or forgot
                        $userData['notification_token'] = $notification_token;
                        $params = array('examuser_id' => $userDetails->userId);
                        $this->Common_model->updateRow('gk_examuserslist', $userData, $params);
                    }
                    
                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Get Data',
                        'data' => $resData,
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Invalid Password',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => true,
                    'code' => '400',
                    'message' => 'Invalid User Name or Mobile Number',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function checkStudent_post()
    {
        if ($_POST) {

            $mobile = $this->input->post('mobile');
            $type = $this->input->post('requestType'); //reg or forgot

            //write internal andriod developer validations
            if ($mobile == '' || $mobile == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'mobile Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($type == 'forgot') {

                $studentRes = $this->Common_model->checkForgotStudent($mobile);

                if ($studentRes) {

                    $otp = rand(8956, 2356);
                    $msg = "$otp is the OTP for your  Forgot Password.Expires after use.Please do not share with anyone.
                        Team
                        BRAINWIZ";

                    // $msg = str_replace(' ','%20',$msg);
                    smssend_new($mobile, $msg);
                    $userData['otp_verify'] = $otp;
                    $params = array('examuser_id' => $studentRes->examuser_id);
                    $this->Common_model->updateRow('gk_examuserslist', $userData, $params);

                    $resData['userId'] = $studentRes->examuser_id;

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Sent Otp',
                        'data' => $resData,
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'You Are Not Register With Us',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {

                $studentRes = $this->Common_model->checkStudent($mobile);

                if ($studentRes) {

                    $otp = rand(8956, 2356);
                    $msg = "$otp is the OTP for your  Registration.Expires after use.Please do not share with anyone.
                        Team
                        BRAINWIZ";

                    // $msg = str_replace(' ','%20',$msg);
                    smssend_new($mobile, $msg);
                    $userData['otp_verify'] = $otp;
                    $params = array('examuser_id' => $studentRes->examuser_id);
                    $this->Common_model->updateRow('gk_examuserslist', $userData, $params);

                    $resData['userId'] = $studentRes->examuser_id;

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Sent Otp',
                        'data' => $resData,
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'You Are Not Register With Us',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function setPassword_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $otp = $this->input->post('otp');
            $newpassword = $this->input->post('newpassword');
            $type = $this->input->post('requestType'); //reg or forgot

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            //write internal andriod developer validations
            if ($newpassword == '' || $newpassword == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'newpassword Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $userDetails = $this->Common_model->checkOtp($userId, $otp);
            if ($userDetails) {

                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);

                $hashed_password = $hasher->HashPassword($newpassword);

                if ($type == 'reg') {
                    $notification_token = $this->input->post('notification_token'); //reg or forgot
                    $userData['notification_token'] = $notification_token;
                }
                $userData['examuser_password'] = $hashed_password;

                $params = array('examuser_id' => $userId);
                $this->Common_model->updateRow('gk_examuserslist', $userData, $params);

                $this->response([
                    'status' => true,
                    'message' => 'Successfully Update Password',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'Invalid OTP',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function register_post()
    {

        
        if ($_POST) {

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $mobile = $this->input->post('mobile');
            $collegeName = $this->input->post('collegeName');
            $referral_code = $this->input->post('referral_code', "");
            $payment_options = $this->input->post('payment_options');

            //write internal andriod developer validations 
            if ($name == '' || $name == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'name Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            // if ($email == '' || $email == null) {
            //     $this->response([
            //         'status' => false,
            //         'code' => '400',
            //         'message' => 'email Filed is Mandatory',
            //     ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            // }
            if ($mobile == '' || $mobile == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'mobile Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            // if ($collegeName == '' || $collegeName == null) {
            //     $this->response([
            //         'status' => false,
            //         'code' => '400',
            //         'message' => 'collegeName Filed is Mandatory',
            //     ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            // }

            $checkStudent = $this->Common_model->getUserDetailsWithMobile($mobile);

            if (!$checkStudent) {

                $registerData['examuser_name'] = $name;
                $registerData['examuser_mobile'] = $mobile;
                $registerData['examuser_email'] = $email;
                $registerData['examuser_cname'] = $collegeName;
                $registerData['examuser_type'] = 'n';
                $registerData['payment_options'] = $payment_options;

                $otp = rand(8956, 2356);
                $msg = "$otp is the OTP for your  Registration.Expires after use.Please do not share with anyone.
                        Team
                        BRAINWIZ";
                smssend_new($mobile, $msg);
                $registerData['otp_verify'] = $otp;

                $userId = $this->Common_model->insertSingle('gk_examuserslist', $registerData);
                $resData['userId'] = $userId;

                if($referral_code != "") {
                    $referredUser = $this->User_model->getExamuserByReferral($referral_code);
                    if (is_object($referredUser)) {
                        $this->User_model->saveNewCashbackReferral([
                            'new_examuser_id' => $userId,
                            'old_examuser_id' => $referredUser->examuser_id,
                            'referral_code' => $referral_code
                        ]);
                    }
                }

                $this->response([
                    'status' => true,
                    'message' => 'Successfully Sent Otp',
                    'data' => $resData,
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'Mobile Number Already Register with us',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function changePassword_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $oldpassword = $this->input->post('oldpassword');
            $newpassword = $this->input->post('newpassword');
            $accessToken = $this->input->post('accessToken');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            //write internal andriod developer validations
            if ($oldpassword == '' || $oldpassword == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'oldpassword Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            //write internal andriod developer validations
            if ($newpassword == '' || $newpassword == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'newpassword Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userDetails = $this->Common_model->getUserDetails($userId);
                if ($userDetails) {

                    $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                    if ($hasher->CheckPassword($oldpassword, $userDetails->examuser_password)) {

                        $hashed_password = $hasher->HashPassword($newpassword);
                        $userData['examuser_password'] = $hashed_password;

                        $params = array('examuser_id' => $userId);
                        $this->Common_model->updateRow('gk_examuserslist', $userData, $params);

                        $this->response([
                            'status' => true,
                            'message' => 'Successfully Changed Password',
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                    } else {
                        $this->response([
                            'status' => false,
                            'code' => '400',
                            'message' => 'Invalid Old Password',
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                    }

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Invalid User',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function profilepic_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $image = $this->input->post('image');
            $accessToken = $this->input->post('accessToken');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            //write internal andriod developer validations
            if ($image == '' || $image == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'image Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userDetails = $this->Common_model->getUserDetails($userId);
                if ($userDetails) {

                    $image = base64_decode($image);
                    $imageName = strtotime("now") . '.jpg';
                    $imgpath = '../upload/profilepics/' . $imageName;
                    file_put_contents($imgpath, $image);

                    $userData['profile_pic'] = $imageName;

                    $params = array('examuser_id' => $userId);
                    $this->Common_model->updateRow('gk_examuserslist', $userData, $params);

                    $resData['profilepic'] = "upload/profilepics/" . $imageName;

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Updated',
                        'data' => $resData,
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Invalid User',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
