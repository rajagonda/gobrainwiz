<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Videos extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model','Videos_model'));       
     }
     public function getAllvideoCategories_post(){
     	if($_POST){

     		$userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             $res = checkLogin($userId,$accessToken);            
              if($res){

            $allVideoCategories= $this->Videos_model->getAllvideoCategories();
     		
     		

			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $allVideoCategories
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	
            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   




     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
      }
     }
     public function getAllvideos_post(){
     	if($_POST){

     		$userId =$this->input->post('userId');
     		$catId =$this->input->post('catId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
             $res = checkLogin($userId,$accessToken);            
              if($res){

            $allVideos= $this->Videos_model->getAllvideos($catId);    		

            if(!$allVideos){
                $data = array();
            }else {
                $data = $allVideos;
            }
     		

			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $data
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	
            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   



     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
     }
     public function saveVideoViews_post(){
     	if($_POST){

     		$userId =$this->input->post('userId');
     		$videoId =$this->input->post('videoId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
             $res = checkLogin($userId,$accessToken);            
              if($res){

            $videoDetails= $this->Videos_model->getVideDetails($videoId);  

            if($videoDetails)  {	

            $existingViews = $videoDetails->views;
            $newviews = $existingViews+1;

            $params = array('id'=>$videoId);
            $viewsData['views'] = $newviews;
            $this->Common_model->updateRow('gk_hvideos',$viewsData,$params);



			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Saved'				
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	

        }else {
            $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'Video Not Found',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

             }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   


     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
     }
 }
 ?>