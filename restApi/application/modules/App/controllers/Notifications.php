<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Notifications extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model'));  
           
    }
    public function getNotifications_post(){
    	if($_POST){

     		$userId =$this->input->post('userId');
        $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


             $res = checkLogin($userId,$accessToken);           
              if($res){

            $notifications= $this->Common_model->getAllNotifications($userId);

            if(!$notifications){
              $data = array();
            }else {
               $notiInfo = array();
              foreach ($notifications as  $value) {
                
                 $row['notification_id'] = $value->notification_id;
                 $row['checked'] = $value->checked;
                 $row['title'] = $value->title;
                 $row['message'] = $value->message;
                 $row['image'] = $value->image;
                 $row['notify_type'] = $value->notify_type;
                 $row['catid'] = $value->catid;
                 $row['url'] = $value->url;
                 $row['timeUpdate'] = $value->timeUpdate;
                 $row['topicid'] = $value->topicid;
                 $row['testId'] = $value->subcatid;


                if($value->notify_type == '2'){
                  $videores = $this->Common_model->getVideoCatName($value->catid);
                  $row['catname'] = $videores->cat_name;
                  //array_push($notiInfo, $row);               
                }
                if($value->notify_type == '3'){
                  $testInfo = $this->Common_model->getTopicAndCatName($value->topicid);
                  $row['topic_name'] = $testInfo->topic_name;  
                  $row['cat_type'] = $testInfo->category_name;  
                 // array_push($notiInfo, $row);
                }
                array_push($notiInfo, $row);
                
                }

              $data = $notiInfo;
            }
            



			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $data
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	

      }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } 

     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }
    public function updateNotification_post(){
    	if($_POST){

     		$userId =$this->input->post('userId');
     		$notificationId =$this->input->post('notification_id');
        $accessToken = $this->input->post('accessToken');


     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            //write internal andriod developer validations 
            if($notificationId == '' || $notificationId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'Notification Id Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
              if($res){

              $notification = $this->Common_model->getNotification($notificationId,$userId);
              if($notification){

            $params = array('notification_id'=>$notificationId,'student_id'=>$userId);
            $notificationsData['checked'] = '1';
            $this->Common_model->updateRow('gk_notifications_log',$notificationsData,$params);



    			$this->response([
    				'status' => TRUE,
    				'message' => 'Successfully Updated Dats'				
    			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	

        }else {
          $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'Notification Id  Not Found',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

        }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   




     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }
}
?>