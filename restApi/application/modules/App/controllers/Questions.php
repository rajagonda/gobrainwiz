<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Questions extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model', 'Custom_model', 'User_model', 'Questions_model'));
        $this->load->helper('sendmail_helper');
        $this->load->helper('phpass_helper');
        $this->load->helper('common_helper');
        $this->load->library(array('authentication'));
    }


    public function getQuestionForUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userid = $this->input->post('userId');

                $data = $this->Questions_model->getRandomQuestionByUserid($userid);
                if (count($data) > 0) {
                    $resultdata = $data;
                } else {
                    $resultdata = array();
                }
                $this->response([
                    'status' => true,
                    'data' => $resultdata,
                    'message' => 'Successfully Get Data',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function getResultFromUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userid = $this->input->post('userId');
                $question = $this->input->post('questionid');
                $answer = $this->input->post('answerid');

                if ($question == '' || $question == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Question Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($answer == '' || $answer == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Answer Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


                $inputData['qr_user_id'] = $userid;
                $inputData['qr_question_id'] = $question;
                $inputData['qr_answer_id'] = $answer;


                $data = $this->Questions_model->addQuestionsResult($inputData);


                if ($data) {
                    $this->response([
                        'status' => true,
                        'message' =>'Successfully Inserted Result',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Operation failed try again',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }


}