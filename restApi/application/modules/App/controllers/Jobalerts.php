<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Jobalerts extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model','Jobalerts_model'));       
     }
     public function getAllJobalerts_post(){
     	if($_POST){

     		$userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             $res = checkLogin($userId,$accessToken);            
              if($res){

            $allJobAlerts= $this->Jobalerts_model->getAllJobalerts();
     		
     		

			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $allJobAlerts
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	
            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   




     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
      }
     }
     
     }
 
 ?>
