<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Weekly extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model'));       
     }
     public function getWeeklySchedule_post(){

     	if($_POST){

     		$userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');


     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             $res = checkLogin($userId,$accessToken);
            // echo $this->db->last_query();exit;
              if($res){

     		$weeklyRes= $this->Common_model->getWeeklySchudle();

     		$resposneData = array();
     		if($weeklyRes){
     			$row['title'] =   $weeklyRes->title;
 				$row["image"] = "upload/weekly/".$weeklyRes->image;
 				$resposneData[] = $row;

     		}
     		

			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $resposneData
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   



     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }


     }
 }
 ?>