<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Login extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Login_model', 'Custom_model'));

    }
    public function login_post()
    {

        if ($_POST) {

            //echo "<pre>"; print_r($_POST);exit;

            $uname = $this->input->post('userName');
            $pass = $this->input->post('userPass');
            $imei = $this->input->post('imei');

            //write internal andriod developer validations
            if ($uname == '' || $uname == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userName Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }

            if ($pass == '' || $pass == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userPass Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }
            if ($imei == '' || $imei == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'imei Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }

            $res = $this->Login_model->getLogin($uname);
            if ($res) {

                if ($res->login_pwd == $pass) {

                    if ($imei == '1234567890' || $imei == '12345678901' || $imei == '862312032788446') {

                        $token = emplogin($res->userId, $res->Imei);

                        $params = array('id' => '1');
                        $checkinDistance = $this->Custom_model->getSingleRow('settings', $params);

                        $checkinDistance = $checkinDistance->checkin_distance;

                        //upate login information

                        $name = $res->fname . $res->lname;
                        $response = array(
                            'userLogin' => 'TRUE',
                            'userId' => $res->userId,
                            'name' => $name,
                            'designation' => $res->designation,
                            'accessToken' => $token,
                            'checkinDistance' => $checkinDistance,
                        );

                        $this->response([
                            'status' => true,
                            'message' => 'Successfully Login',
                            'data' => $response,
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                    } else {
                        if ($res->Imei == $imei) {

                            $token = emplogin($res->userId, $res->Imei);
                            $params = array('id' => '1');
                            $checkinDistance = $this->Custom_model->getSingleRow('settings', $params);

                            $checkinDistance = $checkinDistance->checkin_distance;

                            //upate login information

                            $name = $res->fname . $res->lname;
                            $response = array(
                                'userLogin' => 'TRUE',
                                'userId' => $res->userId,
                                'name' => $name,
                                'designation' => $res->designation,
                                'userdata' => $res,
                                'accessToken' => $token,
                                'checkinDistance' => $checkinDistance,
                            );

                            $this->response([
                                'status' => true,
                                'message' => 'Successfully Login',
                                'data' => $response,
                            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                        } else {
                            $this->response([
                                'status' => false,
                                'message' => 'Mobile App Cannot Be Enable For User',
                            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                        }
                    }

                } else {
                    // Set the response and exit
                    $this->response([
                        'status' => false,
                        'message' => 'Invalid Password',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }

            } else {
                // Set the response and exit
                $this->response([
                    'status' => false,
                    'message' => 'Invalid User Id',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            }

        } else {
            // Set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Bad Request Format',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (400) being the HTTP response code
        }
    }
    public function logout_post()
    {
        if ($_POST) {

            //echo "<pre>"; print_r($_POST);exit;
            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            $imei = $this->input->post('imei');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }
            if (!checkInt(trim($userId))) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed Must And Should Pass Integer Value',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }

            if ($imei == '' || $imei == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'imei Filed is Mandatory',
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
            }

            //update
            $updateValues = array(
                'accessToken' => '',
                'logoutTime' => date('Y-m-d H:i:s'),
            );
            $result = $this->Custom_model->updateLogin($updateValues, $userId, $accessToken);
            $this->response([
                'status' => true,
                'message' => 'Successfully Logout',
            ], REST_Controller::HTTP_OK);

        } else {
            // Set the response and exit
            $this->response([
                'status' => false,
                'message' => 'Bad Request Format',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (400) being the HTTP response code
        }
    }

}
