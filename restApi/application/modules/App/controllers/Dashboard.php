<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Dashboard extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Dashboard_model','Custom_model'));       
     }

     public function index_post(){

     	if($_POST){

     		$userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


              $res = checkLogin($userId,$accessToken);
              if($res){

     		$resdata['banners']= $this->Dashboard_model->getBanners();
     		$topers= $this->Dashboard_model->getTopers();
     		$toppersArray = array();
     		foreach ($topers as  $value) {
     				$row["student_name"] = $value->student_name; 
     				$row["company"] = $value->company; 
					$row["voice_image"] = "upload/topper/".$value->voice_image;
					$row["voice_description"] = $value->voice_description; 
					$toppersArray[] = $row;
     		}
     		$resdata['toppers'] = $toppersArray;

			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'data' => $resdata
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	

            }else {
               $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }

     }
}
?>