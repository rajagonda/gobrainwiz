<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Practicetest Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Practicetest extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model','Practicetest_model'));  
        $this->load->helper('common_helper');        
     }

    public function getPracticetestList_post(){
        if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $allcategories = $this->Practicetest_model->getAllPracticetestList();

            
            $i=0;
            $a = array();
            foreach ($allcategories as $value) {
                $topics = $this->Practicetest_model->getTopics($value->subcat_id);
                $totlt = 0;
                foreach ($topics as  $value1) {
                    $totlt +=$value1->totalTests;
                }        

                $b['category_name'] =  $value->category_name;
                $b['c_id'] = $value->subcat_id;
                $b['totalTopics'] = $value->totalTopics;
                $b['totalTests'] = $totlt;
                $b['topics'] = $topics;
                array_push($a, $b);
                $i++;
            }          

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $a
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }

      public function getTestTopper_post(){
        if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');            
            $testId = $this->input->post('testId');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            
            //write internal andriod developer validations 
            if($testId == '' || $testId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $testScore = $this->Practicetest_model->getTestTopper($testId);

            if(!$testScore){
              $data = array();
            }else {
              $data = $testScore;
            }           

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $data
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }



    public function getTopicTestinfo_post(){
         if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            $topicId = $this->input->post('topicId');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $topicInfo = $this->Practicetest_model->getTopicInfo($topicId);

            if($topicInfo){

            $alltest = $this->Practicetest_model->getTopicsWithTestCount($topicId);
            $topicinfoData['topicId'] = $topicId;
            $topicinfoData['topicName'] = $topicInfo->topic_name;
		 $userInfo = $this->Practicetest_model->getUserInfo($userId);
            $createTime = strtotime($userInfo->examuser_create);
            $createDate = strtotime(date('Y-m-d',$createTime));
            $todayDate = strtotime(date('Y-m-d'));            



            $testInfo = array();
            foreach ($alltest as  $value) {

		$testCreateDate = strtotime(date('Y-m-d',strtotime($value->created_date)));
                $test['testId'] = $value->test_id;
                $test['testName'] = $value->test_name;
                $test['testTime'] = date('i:s', strtotime($value->test_time));
                $test['totalQuestions'] =$value->totalQuestions;

		if($createDate <= $testCreateDate)
                {
                    $test['newTest'] = 1;
                }else{
                    $test['newTest'] = 0;
                }

                $result = $this->Practicetest_model->checkTestResult($value->test_id,$userId);
                if($result){
                  $test['testAttempted'] = 1;
                }else {
                  $test['testAttempted'] = 0;
                }
                array_push($testInfo, $test);
            }
            $topicinfoData['testInfo'] = $testInfo;

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $topicinfoData
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'Invalid Topic Id',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }  

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    } 
    public function getQuetionsList_post(){
        if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            $topicId = $this->input->post('topicId');
            $testId = $this->input->post('testId');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            //write internal andriod developer validations 
            if($topicId == '' || $topicId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'topicId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            //write internal andriod developer validations 
            if($testId == '' || $testId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $allRestions = $this->Practicetest_model->getTestAllQuestions($topicId,$testId);



            if(!$allRestions){
              $q = array();
            }else {
                $q = array();
                foreach ($allRestions as  $value) {
                    $a['q_id'] = $value->q_id;
                    

                    $q1 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->question_name);
                    $aq = str_replace('../', '',$q1);                   
                    $a['question_name'] =  $aq;


                    $op1 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option1);
                    $op = str_replace('../', '',$op1);                   
                    $a['option1'] =  $op;


                    $op2 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option2);
                    $op = str_replace('../', '',$op2);                   
                    $a['option2'] =  $op;


                    $op3 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option3);
                    $op = str_replace('../', '',$op3);                   
                    $a['option3'] =  $op;

                    $op4 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option4);
                    $op = str_replace('../', '',$op4);                   
                    $a['option4'] =  $op;


                    $op5 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option5);
                    $op = str_replace('../', '',$op5);                   
                    $a['option5'] =  $op;          
                    
                    array_push($q, $a);

                }
              //$data = $allRestions;
            }           

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $q
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }
    public function viewSummery_post(){
         if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');            
            $testId = $this->input->post('testId');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            
            //write internal andriod developer validations 
            if($testId == '' || $testId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $allAnswers = $this->Practicetest_model->getAllQuetionsWithAnswer($testId,$userId);


            if(!$allAnswers){
              $q = array();
            }else {
                $q = array();
                foreach ($allAnswers as  $value) {
                    $a['q_id'] = $value->q_id;
                    $a['question_answer'] = $value->question_answer;
                   
                    


                    $a['video_link'] = $value->video_link;
                    $a['user_selected'] = $value->user_selected;
                    $a['q_time'] = $value->q_time;


                    $aq = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->question_name);
                    $q1 = str_replace('../', '',$aq);
                    $a['question_name'] =  $q1;

                    $op = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option1);
                    $op1 = str_replace('../', '',$op);
                    $a['option1'] =  $op1;

                    $op2 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option2);
                    $op21 = str_replace('../', '',$op2);
                    $a['option2'] =  $op21;

                    $op3 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option3);
                    $op31 = str_replace('../', '',$op3);
                    $a['option3'] =  $op31;

                    $op4 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option4);
                    $op41 = str_replace('../', '',$op4);
                    $a['option4'] =  $op41;

                    $op5 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->option5);
                    $op51 = str_replace('../', '',$op5);
                    $a['option5'] =  $op51;    


                     $ex1 = str_replace('../upload/', 'http://gobrainwiz.in/upload/',$value->question_explanation);
                     $ex = str_replace('../', '',$ex1);
                   
                    $a['question_explanation'] =  $ex;               
                    
                    array_push($q, $a);

                }
              //$data = $allRestions;
            }           

                 

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $q
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }

    public function saveAnswers_post(){
        if($_POST){

         //  echo "<pre>"; print_r($_POST);exit;

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');            
            $testId = $this->input->post('testId');

            // echo "<pre>"; print_r(json_decode($_POST['questions']));exit;
            $questions = json_decode($_POST['questions']);
         //   echo "<pre>"; print_r($questions[0]->q_id);exit;

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            
            //write internal andriod developer validations 
            if($testId == '' || $testId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

                //check test exit or already attempt

                $testRes = $this->Practicetest_model->checkTest($testId);
                if($testRes){

                //Already attemt ot not cehck 

               $userResult = $this->Practicetest_model->checkTestResult($testId,$userId);

               if(!$userResult){

                    $total_questions = 0;
                    $attempted_questions= 0;
                    $unattempted_questions = 0;
                    $correct_answers = 0;
                    $incorrect_answers = 0;
                    $total_marks = 0;        
                    $time_attempt = 0;            


                    $testQuestions = $this->Practicetest_model->getSingleTestQuestions($testId);
                    //echo "<pre>"; print_r($testQuestions);exit;

                for($i= 0 ;$i< count($questions);$i++){

                   
                    $attempt = 0;

                    $saveData['user_id'] = $userId;
                    $saveData['test_id'] = $testId;
                    $saveData['q_id']    = $questions[$i]->q_id;
                    $saveData['q_time']  = $questions[$i]->time_taken;
                    $saveData['answer']  = $questions[$i]->selected_option;
                    $time_attempt = $time_attempt + $questions[$i]->time_taken;



                    if($questions[$i]->selected_option !='0'){
                         $attempt = 1;
                    }
                    $saveData['attempet'] = $attempt;

                    $this->Common_model->insertSingle('gk_ptestinformation',$saveData);       

                    $question_id = $questions[$i]->q_id;

                    $neededObject = null;
                    foreach($testQuestions as $struct) {
                        if ($question_id == $struct->q_id) {
                          //  echo "hh";exit;
                            $neededObject = $struct;
                            break;
                        }
                    }

                   // echo "<pre>"; print_r($neededObject);exit;

                    if($neededObject->question_answer == $questions[$i]->selected_option){


                        $attempted_questions = $attempted_questions+1;
                        $total_marks = $total_marks+1;                       


                    }else if ($questions[$i]->selected_option !='0'){

                        $attempted_questions = $attempted_questions +1;                      
                        $incorrect_answers = $incorrect_answers+1;

                    }else {
                        $unattempted_questions = $unattempted_questions +1;
                    }
                }

                 $end_test = date('Y-m-d');

                 $testResult['test_id'] = $testId;
                 $testResult['user_Id'] = $userId;
                 $testResult['total_q'] = count($questions);
                 $testResult['attempted_q'] = $attempted_questions;
                 $testResult['notattempetd_q'] = $unattempted_questions;
               //  $testResult['correct_q'] = $total_marks;
                 $testResult['correct_q'] = $total_marks;
                 $testResult['wrong_q'] = $incorrect_answers;
                 $testResult['test_date'] = $end_test;
                 $testResult['marks'] = $total_marks;
                 $testResult['test_time'] = $time_attempt;
                // $testResult['total_data'] = $_POST;

               //  echo "<pre>"; print_r($testResult);exit;
			

		$maxMarks = $this->Practicetest_model->testMaxMarks($testId);
                 $maxRank = $this->Practicetest_model->testMaxRank($testId);
                 $testRes = $this->Practicetest_model->getalltestsProfileWithID($testId);
                 $testResDesc = $this->Practicetest_model->getalltestsProfileWithIDDesc($testId);
                 $maximum_marks = $maxMarks[0]->maxMarks;
                 $maximum_rank = $maxRank[0]->maxRank;
                 $presentRank = 0;
                 $temp = 0;
                 $temp1 = 0;
            if($maximum_rank == 0)
             {
                $presentRank = 1;
             }elseif($maximum_marks == 0 && $maximum_rank > 1){
                $presentRank = $maximum_rank +1;
             }else{
                if($maximum_marks < $total_marks)
                    {
                        for($t= 0 ;$t< count($testResDesc);$t++){
                            if($testResDesc[$t]->rank != 0){

                                $presentRank = $testResDesc[$t]->rank;
                                $form_values['rank'] =  (double)$testResDesc[$t]->rank+1;
                                $res = $this->Practicetest_model->update($form_values,$testResDesc[$t]->id);
                            } 
                        }  
                    }elseif ($maximum_marks == $total_marks) {
                        for($t= 0 ;$t< count($testResDesc);$t++){
                            if($testResDesc[$t]->rank != 0){
                             if($testResDesc[$t]->marks < $total_marks)
                             {
                                $presentRank = $testResDesc[$t]->rank;
                                $form_values['rank'] =  (double)$testResDesc[$t]->rank+1;
                                $res = $this->Practicetest_model->update($form_values,$testResDesc[$t]->id);
                             }elseif ($testResDesc[$t]->marks == $total_marks)  {
                                   if($testResDesc[$t]->test_time > $time_attempt){
                                   
                                        $presentRank = $testResDesc[$t]->rank;
                                       
                                        $form_values['rank'] =  (double)$testResDesc[$t]->rank+1;
                                        $res = $this->Practicetest_model->update($form_values,$testResDesc[$t]->id);
                                        $temp1 = 1;
                                       // break;
                                   }elseif ($testResDesc[$t]->test_time < $time_attempt) {
                                    
                                     /*$presentRank = $testResDesc[$t]->rank;
                                       
                                        $form_values['rank'] =  (double)$testResDesc[$t]->rank+1;
                                        $res = $this->Practicetest_model->update($form_values,$testResDesc[$t]->id);
                                        break;*/
                                    if($temp1 ==0)
                                      $presentRank = $testResDesc[$t]->rank+1;
                                        break;
                                     //  var_dump($t.'-----'.$presentRank);
                                   }else{
                                    
                                     if($temp1 ==0)
                                        $presentRank = $testResDesc[$t]->rank+1;
                                       //  var_dump($t.'-----'.$presentRank);
                                   }
                              }
                           }
                        }
                    }else{
                        for($t= 0 ;$t< count($testRes);$t++){
                            if($testRes[$t]->rank != 0){
                                if($testRes[$t]->marks > $total_marks){
                                    $presentRank = $testRes[$t]->rank+1;
                                   
                                }elseif ($testRes[$t]->marks == $total_marks) {
                                    if($testRes[$t]->test_time > $time_attempt){
                                        if($temp= 0){
                                            $presentRank = $testRes[$t]->rank;
                                            $form_values['rank'] =  (double)$testRes[$t]->rank+1;
                                            $res = $this->Practicetest_model->update($form_values,$testRes[$t]->id);
                                    }else{
                                        $form_values['rank'] =  (double)$testRes[$t]->rank+1;
                                        $res = $this->Practicetest_model->update($form_values,$testRes[$t]->id);
                                    }
                                        
                                   }elseif ($testRes[$t]->test_time < $time_attempt) {
                                        $presentRank = $testRes[$t]->rank+1;
                                   }else{
                                        $presentRank = $testRes[$t]->rank+1;
                                   }
                                }else{
                                     $form_values['rank'] =  (double)$testRes[$t]->rank+1;
                                        $res = $this->Practicetest_model->update($form_values,$testRes[$t]->id);
                                }
                            }
                        }
                    }
             }


             $testResult['rank'] = $presentRank;




                $res = $this->Common_model->insertSingle('gk_ptestresult',$testResult);    

          

           $testScore = $this->Practicetest_model->getTestScore($testId,$userId);

            if(!$testScore){
              $data = array();
            }else {
              $data = $testScore;
            }           

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $data
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   



            }else {
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'Test Already Attempted By User',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Not valid',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }
    public function getScore_post(){
        if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');            
            $testId = $this->input->post('testId');

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            
            //write internal andriod developer validations 
            if($testId == '' || $testId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $testScore = $this->Practicetest_model->getTestScore($testId,$userId);

            if(!$testScore){
              $data = array();
            }else {
              $data = $testScore;
            }           

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $data
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }

    public function getMytests_post(){
        if($_POST){

            $userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');            
          

            //write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             if($accessToken == '' || $accessToken == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            
           

            $res = checkLogin($userId,$accessToken);            
            if($res){

            $alltests = $this->Practicetest_model->getalltests();
            $data['completedTestResult'] = array();
            $data['remainingTestResult'] = array();
             foreach($alltests as $value){
                $testres = $this->Practicetest_model->getTestScore($value->test_id,$userId);
                if($testres !=''){
                    $alltest = $this->Practicetest_model->getTopicsWithTestTime($value->topic_id,$value->test_id,'y');
                    $testr['test_id'] = $value->test_id;
                    $testr['cat_id'] = $value->subcat_id;
                    $testr['topic_id'] = $value->topic_id;
                    $testr['test_name'] = $value->test_name;
                    $testr['testTime'] = '';
                    $testr['totalQuestions'] = 0;
                    if($alltest){
                        $testr['testTime'] =  date('i:s', strtotime($alltest->test_time));
                        $testr['totalQuestions'] = $alltest->totalQuestions;
                    }
                    $result = $this->Practicetest_model->checkTestResult($value->test_id,$userId);
                        if($result){
                          $testr['testAttempted'] = 1;
                        }else {
                          $testr['testAttempted'] = 0;
                      }
                    array_push($data['completedTestResult'],$testr);

                }else {
                    $alltest = $this->Practicetest_model->getTopicsWithTestTime($value->topic_id,$value->test_id,'y');
                    $testr['test_id'] = $value->test_id;
                    $testr['cat_id'] = $value->subcat_id;
                    $testr['topic_id'] = $value->topic_id;
                    $testr['test_name'] = $value->test_name;
                    $testr['testTime'] = '';
                    $testr['totalQuestions'] = 0;
                    if($alltest){
                        $testr['testTime'] = date('i:s', strtotime($alltest->test_time));
                        $testr['totalQuestions'] = $alltest->totalQuestions;
                    }
                    
                    $result = $this->Practicetest_model->checkTestResult($value->test_id,$userId);
                        if($result){
                          $testr['testAttempted'] = 1;
                        }else {
                          $testr['testAttempted'] = 0;
                      }
                  //  $testr['testAttempted'] = $alltest->testAttempted;
                    array_push($data['remainingTestResult'],$testr);

                }

             }

                     

            $this->response([
                'status' => TRUE,
                'message' => 'Successfully Get Data',
                'data' => $data
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code   

            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   

        }else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
      }
    }
    public function getTestRankById_post()
    {
        try {
            $userId = $this->input->post('userId');
            $testId = $this->input->post('testId');
            $accessToken = $this->input->post('accessToken');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            if ($testId == '' || $testId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'testId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {
                $data = $this->Practicetest_model->getUserRankForTestIncludesTimeConsume($userId, $testId); // $this->Practicetest_model->getUserRankForTest($userId, $testId);
		$totalAttendees = $this->Practicetest_model->getTotalAttendeesCount($testId);
                if ($data !== false) {
                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Get Data',
                        'data' => [$data, $totalAttendees],

                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                } else {
                    $this->response([
                        'status' => false,
                        'code' => '200',
                        'message' => 'No Rank Found',
                        'data' => [
                            $totalAttendees
                        ],
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
                
            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } catch (\Exception $e) {
            $this->response([
                'status' => false,
                'message' => $e->getMessage(),
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
 }
 ?>
