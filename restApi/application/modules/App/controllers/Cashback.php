<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Cashback extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model', 'Custom_model', 'User_model'));
        $this->load->helper('sendmail_helper');
        $this->load->helper('phpass_helper');
        $this->load->helper('common_helper');
        $this->load->library(array('authentication'));
    }
    public function cron_get()
    {
        $isCronRunning = $this->User_model->isCronRunning();
        
        if($isCronRunning) return;

        $this->User_model->updateCronStatus('1');

        $cashbackSettings = $this->User_model->getLastCashbackSettings();
        if(is_object($cashbackSettings)) {
            $paymentDate = date('Y-m-d H:i');
            $paymentDate=date('Y-m-d H:i', strtotime($paymentDate));
            $contractDateBegin = date('Y-m-d H:i', strtotime($cashbackSettings->start_date));
            $contractDateEnd = date('Y-m-d H:i', strtotime($cashbackSettings->end_date));

            if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd)){
                if($cashbackSettings->status == "0") {
                    $this->User_model->deleteInactiveReferralCodes();
                    $students = $this->User_model->getAllBrainwizStudents();
                    foreach ($students as $student) {
                        $this->User_model->updateReferralCode($student->examuser_id);
                    }
                    $this->User_model->updateCashbackSettings(
                        [
                            'status' => '1'
                        ],
                        [
                            'id' => $cashbackSettings->id
                        ]
                    );
                }
            }else{
                if($cashbackSettings->status == "1") {
                    $data = [
                        'status' => '0',
                        'deleted' => '1',
                        'deleted_at' => date('Y-m-d H:i:s')
                    ];
                    if($paymentDate < $contractDateEnd) {
                        $data = [
                            'status' => '0'
                        ];
                    } else {
                        $this->User_model->deactivateReferralCodes();
                    }
                    $this->User_model->updateCashbackSettings(
                        $data,
                        [
                            'id' => $cashbackSettings->id
                        ]
                    );
                }
            }
        }
        $this->User_model->updateCronStatus('0');
    }
    public function isValidReferralCode_get($code)
    {
        $isValid = $this->User_model->getExamuserByReferral($code);
        if (is_object($isValid) && $isValid->referral_code) {
            $this->response([
                'status' => true,
                'message' => 'Successfully Get Data',
                'data' => $isValid,
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response([
                'status' => false,
                'code' => '400',
                'message' => 'Please enter the correct referral code',
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function isCashbackActive_get()
    {
        $isValid = $this->User_model->isCashbackActive();
        $this->response([
            'status' => $isValid,
            'message' => 'Successfully Get Data'
        ], REST_Controller::HTTP_OK);
    }
    public function submitReferralCode_post()
    {
        try {
            if ($_POST) {
                $userId = $this->input->post('userId');
                $code = $this->input->post('referral_code');
                $accessToken = $this->input->post('accessToken');
    
                //write internal andriod developer validations
                if ($userId == '' || $userId == null) {
                    throw new \Exception('userId field is Mandatory');
                }
                if ($accessToken == '' || $accessToken == null) {
                    throw new \Exception('accessToken field is Mandatory');
                }
                if ($code == '' || $code == null) {
                    throw new \Exception('Referral code is Mandatory');
                }
    
                $res = checkLogin($userId, $accessToken);
                if ($res) {
                    $currentUser = $this->Common_model->getUserDetails($userId);
                    if(!is_object($currentUser)) {
                        throw new \Exception('User Does not exists');
                    }
                    $currentUserReferral = $this->User_model->getExamuserReferralById($currentUser->examuser_id);
                    if(is_object($currentUserReferral)) {
                        throw new \Exception('Already Referred By Others');
                    }
                    $referredUser = $this->User_model->getExamuserByReferral($code);
                    if (is_object($referredUser)) {
                        $getReferralJoin = $this->User_model->getReferralJoin($referredUser->examuser_id, $currentUser->examuser_id);
                        if(is_object($getReferralJoin)) {
                            throw new \Exception('Already Referred');
                        } else {
                            $this->User_model->saveNewCashbackReferral([
                                'new_examuser_id' => $currentUser->examuser_id,
                                'old_examuser_id' => $referredUser->examuser_id,
                                'referral_code' => $code
                            ]);
                            $this->response([
                                'status' => true,
                                'message' => 'Successfully Get Data',
                                'data' => $currentUser,
                            ], REST_Controller::HTTP_OK);        
                        }
                    } else {
                       throw new \Exception('This Referral Code has been expired. Please use another Referral Code');
                    }
                }
            } else {
                throw new \Exception('Not A Valid Request');
            }
        } catch(\Exception $e) {
            $this->response([
                'status' => false,
                'code' => '422',
                'message' => $e->getMessage(),
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function getUserReferralJoinData_post()
    {
        try {
            if ($_POST) {
                $userId = $this->input->post('userId');
                $accessToken = $this->input->post('accessToken');
                //write internal andriod developer validations
                if ($userId == '' || $userId == null) {
                    throw new \Exception('userId field is Mandatory');
                }
                if ($accessToken == '' || $accessToken == null) {
                    throw new \Exception('accessToken field is Mandatory');
                }
                $res = checkLogin($userId, $accessToken);
                if ($res) {
                    $currentUser = $this->Common_model->getUserDetails($userId);
                    if(!is_object($currentUser)) {
                        throw new \Exception('User Does not exists');
                    }
                    // if($currentUser->examuser_type == "n") {
                    //     throw new \Exception('Not A Brainwiz Student');
                    // }
                    
                    $currentUserReferredData = $this->User_model->getCurrentUserReferredData($currentUser->examuser_id);
                    $isActive = $this->User_model->isCashbackActive();
                    $cashbackSettings = $this->User_model->getLastCashbackSettings();
                    if($currentUser->examuser_type == "y") {
                        $currentUserReferral = $this->User_model->getExamuserReferralById($currentUser->examuser_id);
                        if(!is_object($currentUserReferral)) {
                            $currentUserReferral = $this->User_model->getExamuserLastReferralById($currentUser->examuser_id);
                        }
                    }
                    $currentUserReferralJoinData = $this->User_model->getReferralJoinsByoldId($currentUser->examuser_id);
                    $currentUsercashbackReferralJoinData = $this->User_model->getCashbackReferralJoinsByoldId($currentUser->examuser_id);
                    $ids = [];
                    foreach ($currentUsercashbackReferralJoinData as $row) {
                        $ids[] = $row->new_examuser_id;
                    }
                    $cashbackByUserDetails = count($ids) > 0 ? $this->User_model->getUserDetailsByIds($ids) : [];
                    
                    $cashbackByUserDetailsById = [];

                    foreach ($cashbackByUserDetails as $v) {
                        $cashbackByUserDetailsById[$v->examuser_id] = $v;
                    }
                    foreach ($currentUsercashbackReferralJoinData as $row) {
                        $row->userDetails = isset($cashbackByUserDetailsById[$row->new_examuser_id]) ? $cashbackByUserDetailsById[$row->new_examuser_id] :(object)[];
                    }

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Get Data',
                        'data' => [
                            'isCashbackActive' => $isActive,
                            'cashbackSettings' => is_object($cashbackSettings) ? $cashbackSettings : (object)[],
                            'userType' => $currentUser->examuser_type,
                            'referredData' => isset($currentUserReferredData) && is_object($currentUserReferredData) ? $currentUserReferredData : (object)[],
                            'referralData' => isset($currentUserReferral) && is_object($currentUserReferral) ? $currentUserReferral : (object)[],
                            'shareData' => $currentUserReferralJoinData,
                            'cashbackByUsersData' => $currentUsercashbackReferralJoinData,
                            'cashbackByUserDetails' => $cashbackByUserDetails
                        ],
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                throw new \Exception('Not A Valid Request');
            }
        } catch(\Exception $e) {
            $this->response([
                'status' => false,
                'code' => '422',
                'message' => $e->getMessage(),
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function increaseShareCount_post() {
        try {
            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                throw new \Exception('userId field is Mandatory');
            }
            if ($accessToken == '' || $accessToken == null) {
                throw new \Exception('accessToken field is Mandatory');
            }
            $res = checkLogin($userId, $accessToken);
            if ($res) {
                $currentUserReferral = $this->User_model->getExamuserReferralById($userId);
                if(is_object($currentUserReferral)) {
                    $previousShareCount = $currentUserReferral->shareCount;
                    $this->User_model->increaseShareCount($currentUserReferral->id);
                    $currentUserReferral = $this->User_model->getExamuserReferralById($userId);
                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Get Data',
                        'data' => [
                            'currentUserReferral' => $currentUserReferral
                        ],
                    ], REST_Controller::HTTP_OK);
                } else {
                    throw new \Exception('Error');
                }
            } else {
                throw new \Exception('Authentication Failed');
            }
        } catch(\Exception $e) {
            $this->response([
                'status' => false,
                'code' => '422',
                'message' => $e->getMessage(),
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
