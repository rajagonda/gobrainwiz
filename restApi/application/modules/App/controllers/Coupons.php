<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Coupons extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model', 'Custom_model', 'User_model', 'Coupons_model'));
        $this->load->helper('sendmail_helper');
        $this->load->helper('phpass_helper');
        $this->load->helper('common_helper');
        $this->load->library(array('authentication'));
    }

    public function getCoupons_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {
                $coupons = $this->Coupons_model->getAllUnusedCoupons();

                $data = array();

                if (count($coupons) > 0) {
                    $data = $coupons;
                }
                $this->response([
                    'status' => TRUE,
                    'message' => 'Successfully Get Data',
                    'data' => $data
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getUserByPhoneNumber_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $mobile = $this->input->post('mobile');

                if ($mobile == '' || $mobile == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'mobile Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $studentRes = $this->Common_model->checkForgotStudent($mobile);
                if ($studentRes) {
                    $resData['userInfo'] = $studentRes;
                } else {
                    $resData['userInfo'] = null;
                }
                $this->response([
                    'status' => true,
                    'message' => 'Successfully Sent Data',
                    'data' => $resData,
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function createUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $name = $this->input->post('name');
                $location = $this->input->post('location');
                $mobile = $this->input->post('mobile');
				$payment_options = $this->input->post('payment_options');

                if ($name == '' || $name == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'name Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($location == '' || $location == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Location Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($mobile == '' || $mobile == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'mobile Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $checkStudent = $this->Common_model->getUserDetailsWithMobile($mobile);
                if (!$checkStudent) {

                    $registerData['examuser_name'] = $name;
                    $registerData['examuser_mobile'] = $mobile;
                    $registerData['examuser_location'] = $location;
                    $registerData['examuser_type'] = 'n';
					$registerData['payment_options'] = $payment_options;

                    $otp = rand(8956, 2356);
                    $msg = "$otp is the OTP for your  Registration.Expires after use.Please do not share with anyone.
                        Team
                        BRAINWIZ";
                    smssend_new($mobile, $msg);
                    $registerData['otp_verify'] = $otp;

                    $this->Common_model->insertSingle('gk_examuserslist', $registerData);

                    $studentRes = $this->Common_model->checkForgotStudent($mobile);

                    if ($studentRes) {
                        $resData['userInfo'] = $studentRes;
                    } else {
                        $resData['userInfo'] = null;
                    }

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Sent Otp',
                        'data' => $resData,
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Mobile Number Already Register with us',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function assignCouponToUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userid = $this->input->post('coupon_userid');
                $couponid = $this->input->post('couponid');

                if ($userid == '' || $userid == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Coupon User id Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($couponid == '' || $couponid == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Couponid Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $registerData['acu_user_id'] = $userid;
                $registerData['acu_coupon_id'] = $couponid;
                $registerData['acu_assignee_id'] = $userId;

                $resultid = $this->Coupons_model->addAssignedCoupon($registerData);
                if ($resultid) {
                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Assigned coupon',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Operation failed try again',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getwalletByUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userid = $this->input->post('userid');

                $data = $this->Coupons_model->getWalletByUser($userid);
                if (count($data) > 0) {
                    $resultdata = $data;
                } else {
                    $resultdata = array();
                }
                $this->response([
                    'status' => true,
                    'data' => $resultdata,
                    'message' => 'Successfully Get Data',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getwithdrawRequestByUser_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $userid = $this->input->post('userid');

                $data = $this->Coupons_model->getwithdrawrequestsByUser($userid);

                if (count($data) > 0) {
                    $resultdata = $data;
                } else {
                    $resultdata = array();
                }
                $this->response([
                    'status' => true,
                    'data' => $resultdata,
                    'message' => 'Successfully Get Data',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getCouponInfo_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $couponcode = $this->input->post('couponcode');

                if ($couponcode == '' || $couponcode == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'CouponCode Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $couponInfo = $this->Coupons_model->getCouponByCode($couponcode);

                if (!empty($couponInfo)) {
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Successfully Get Data',
                        'data' => $couponInfo
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Invalid Coupon',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function redeemCoupon_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $coupon_used_userid = $this->input->post('coupon_used_userid');
                $coupon_used_date = $this->input->post('coupon_used_date');
                $couponid = $this->input->post('couponid');

                if ($coupon_used_userid == '' || $coupon_used_userid == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Coupon Used user is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($coupon_used_date == '' || $coupon_used_date == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Coupon Used date is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                if ($couponid == '' || $couponid == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Coupon id is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $assigncouponinfo = $this->Coupons_model->getAssignCouponByCouponID($couponid);


                if (!empty($assigncouponinfo)) {

                    $form['cu_provider_user_id'] = $assigncouponinfo->acu_user_id;
                    $form['cu_used_user_id'] = $coupon_used_userid;
                    $form['cu_used_date'] = date("Y-m-d", strtotime($coupon_used_date));
                    $form['cu_coupon_id'] = $couponid;
                    $form['cu_status'] = 'applied';

                    $couponinfo = $this->Coupons_model->getCouponById($couponid);

                    $wallet = array();
                    $wallet['uw_user_id'] = $coupon_used_userid;
                    $wallet['uw_coupon_id'] = $couponid;
                    $wallet['uw_coupon_type'] = "referral";
                    $wallet['uw_price_type'] = "credit";
                    $wallet['uw_price'] = $couponinfo->coupon_referral_price;
                    $this->Coupons_model->addWallet($wallet);

//                    $userinfo = $this->Common_model->getUserDetails($coupon_used_userid);
//
//                    $updatedamount = ($userinfo->examuser_wallet) + $couponinfo->coupon_referral_price;
//
//                    $updatedata = array();
//                    $updatedata['examuser_wallet'] = $updatedamount;
//                    $this->db->where('examuser_id', $coupon_used_userid);
//                    $this->db->update('gk_examuserslist', $updatedata);

                    $wallet = array();
                    $wallet['uw_user_id'] = $assigncouponinfo->acu_user_id;
                    $wallet['uw_coupon_id'] = $couponid;
                    $wallet['uw_coupon_type'] = "provider";
                    $wallet['uw_price_type'] = "credit";
                    $wallet['uw_scratch_status'] = 1;
                    $wallet['uw_price'] = $couponinfo->coupon_earning_price;
                    $this->Coupons_model->addWallet($wallet);

                    $input = array();
                    $input['wr_requested_amount'] = $couponinfo->coupon_earning_price;
                    $input['wr_requested_amount_type'] = "provider";
                    $input['wr_user_id'] = $assigncouponinfo->acu_user_id;
                    $this->Coupons_model->addWithdrawRequest($input);

                    $userinfo = $this->Common_model->getUserDetails($assigncouponinfo->acu_user_id);

                    $updatedamount = ($userinfo->examuser_wallet) + $couponinfo->coupon_earning_price;

                    $updatedata = array();
                    $updatedata['examuser_wallet'] = $updatedamount;
                    $this->db->where('examuser_id', $assigncouponinfo->acu_user_id);
                    $this->db->update('gk_examuserslist', $updatedata);

                    $this->Coupons_model->addCouponUseage($form);

                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Added Credits',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Assign Coupon to user first',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function getAssignedCouponsByAssigneeId_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $assigneeId = $this->input->post('assigneeId');

                if ($assigneeId == '' || $assigneeId == NULL) {
                    $this->response([
                        'status' => FALSE,
                        'code' => '400',
                        'message' => 'Assignee ID Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


                $coupons = $this->Coupons_model->getAssignedCouponsByUser($assigneeId);

                $data = array();

                if (count($coupons) > 0) {
                    $data = $coupons;
                }
                $this->response([
                    'status' => TRUE,
                    'message' => 'Successfully Get Data',
                    'data' => $data
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function getUsedCouponsByUsedUserId_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $usedUserId = $this->input->post('usedUserId');

                if ($usedUserId == '' || $usedUserId == NULL) {
                    $this->response([
                        'status' => FALSE,
                        'code' => '400',
                        'message' => 'Used User ID Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


                $coupons = $this->Coupons_model->getUsedCouponsByUsedUserId($usedUserId);

                $data = array();

                if (count($coupons) > 0) {
                    $data = $coupons;
                }
                $this->response([
                    'status' => TRUE,
                    'message' => 'Successfully Get Data',
                    'data' => $data
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function updateScratchCardDetails_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $accessToken = $this->input->post('accessToken');
            if ($userId == '' || $userId == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == NULL) {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }


            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $wallet_id = $this->input->post('wallet_id');

                if ($wallet_id == '' || $wallet_id == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Wallet ID is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


                $walletinfo = $this->Coupons_model->getWalletInfo($wallet_id);


                if (!empty($walletinfo)) {

                    $couponinfo = $this->Coupons_model->getCouponById($walletinfo->uw_coupon_id);

                    $input = array();
                    $input['wr_requested_amount'] = $couponinfo->coupon_referral_price;
                    $input['wr_requested_amount_type'] = "referral";
                    $input['wr_user_id'] = $walletinfo->uw_user_id;
                    $this->Coupons_model->addWithdrawRequest($input);


                    $userinfo = $this->Common_model->getUserDetails($walletinfo->uw_user_id);

                    $updatedamount = ($userinfo->examuser_wallet) + $couponinfo->coupon_referral_price;

                    $updatedata = array();
                    $updatedata['examuser_wallet'] = $updatedamount;
                    $this->db->where('examuser_id', $walletinfo->uw_user_id);
                    $this->db->update('gk_examuserslist', $updatedata);

                    $updatedata = array();
                    $updatedata['uw_scratch_status'] = 1;
                    $this->db->where('uw_id', $walletinfo->uw_id);
                    $this->db->update('gk_user_wallet', $updatedata);


                    $this->response([
                        'status' => true,
                        'message' => 'Successfully Added Credits',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Invalid Wallet ID',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


            } else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }


}
