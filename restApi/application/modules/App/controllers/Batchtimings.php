<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Batchtimings extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model','Custom_model','Batchtimings_model'));       
     }
     public function getAllBatches_post(){
     	if($_POST){

     		$userId =$this->input->post('userId');
            $accessToken = $this->input->post('accessToken');

     		//write internal andriod developer validations 
            if($userId == '' || $userId == NULL){
                 $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

             $res = checkLogin($userId,$accessToken);            
              if($res){

            $allBatchs = $this->Batchtimings_model->getAllBatchs();
     		
     		$currentBatchs = $this->Batchtimings_model->getCurrentBatchs();
            $toppers = $this->Batchtimings_model->getToppers();
		
		$toppersArr = array();
            foreach ($toppers as  $value) {
                $row['voice_id'] = $value ->voice_id;
                $row['student_name'] = $value ->student_name;
                $row['voice_description'] = str_replace("/n","" ,htmlspecialchars_decode($value ->voice_description,ENT_QUOTES));
                $row['company'] = $value ->company;
                $row['topperImage'] = $value ->topperImage;
		$row['college'] = $value ->college;
                array_push($toppersArr, $row);
            }



			$this->response([
				'status' => TRUE,
				'message' => 'Successfully Get Data',
				'batchs' => $allBatchs,
                'currentBatchs' => $currentBatchs,
                'toppers' => $toppersArr
			], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code	
            }else {
                $this->response([
                    'status' => FALSE,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',                    
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }   




     	}else {
          $this->response([
                'status' => FALSE,
                'message' => 'Bad Way Request'
          ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
      }
     }
     
     }
 
 ?>
