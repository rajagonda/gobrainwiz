<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require $_SERVER['DOCUMENT_ROOT'] . '/phpmailer/PHPMailerAutoload.php';

/**
 *
 * @package         App Services
 * @subpackage      Login Module
 * @category        Controller
 * @author          challagulla venkata sudhakar
 * @license         MIT
 * @link            ......
 */
class Enquiry extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array('Common_model', 'Custom_model'));
        $this->load->helper('sendmail_helper');
    }
    public function testMail_get()
    {
        $to = "phpguidance@gmail.com";
        $subject = "Test mail from app urls";
        $message = "HI Test from app urls";
        $from = "ch.v.sudhakar9@gmail.com";

// Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
        $headers .= 'From: $from' . "\r\n";

        if (mail($to, $subject, $message, $headers)) {

            echo "Message accepted";
        } else {
            echo "Error: Message not accepted";
        }

    }

    public function sendEnquiry_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $message = $this->input->post('message');
            $accessToken = $this->input->post('accessToken');
            //    $contactName = $this->input->post('contactName');
            //  $contactMobile = $this->input->post('contactMobile');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $message = $this->input->post('message');

                //write internal andriod developer validations
                if ($message == '' || $message == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Message Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $userDetails = $this->Common_model->getUserDetails($userId);

                // echo "<pre>"; print_r($userDetails);exit;

                //     $data['username'] = $this->input->post('contactName');
                //  $data['mobile'] = $this->input->post('contactMobile');
                $data['username'] = $userDetails->examuser_name;
                $data['mobile'] = $userDetails->examuser_mobile;
                $data['email'] = $userDetails->examuser_email;
                $data['message'] = $this->input->post('message');
                $user_email = $userDetails->examuser_email;

                $subject = "ENQUIRY TO BRAINWIZ From Mobile App (".$data['username'].")";

                $message = $this->load->view('contactSuccess', $data, 'true');

                $mail = new PHPMailer;

                $mail->isSMTP(); // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                $mail->SMTPAuth = true; // Enable SMTP authentication
                $mail->Username = 'brainwiz2015@gmail.com'; // SMTP username
                $mail->Password = 'Brainwiz@2015'; // SMTP password
                $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;
                $mail->setFrom('brainwiz2015@gmail.com', 'Brainwiz'); // Set From address

                $mail->smtpConnect(
                    array(
                        "ssl" => array(
                            "verify_peer" => false,
                            "verify_peer_name" => false,
                            "allow_self_signed" => true,
                        ),
                    )
                );

                $to = 'gobrainwiz@gmail.com';
                $name = '';
                $mail->addAddress($to, $name); // Add a recipient

                //$mail->addReplyTo($config->MAIL_REPLY_TO, $config->MAIL_REPLY_LABEL);

                $mail->isHTML(true); // Set email format to HTML

                $mail->Subject = $subject;
                $mail->Body = $message;
                if (!$mail->send()) {
                    //  var_dump($mail->ErrorInfo);die();
                    $resdata['message'] = "Technical Error Please Try Again Once!";
                    $resdata['id'] = "0";
                } else {
                    $formdata['enquiry_name'] = $userDetails->examuser_name;
                    $formdata['enquiry_email'] = $userDetails->examuser_email;
                    $formdata['enquiry_mobile'] = $userDetails->examuser_mobile;
                    $formdata['enquiry_message'] = $this->input->post('message');
                    //    $formdata['contactName'] = $this->input->post('contactName');
                    //        $formdata['contactMobile'] = $this->input->post('contactMobile');
                    // echo "<pre>"; print_r($formdata);exit;

                    $this->Common_model->insertSingle('gk_enquiry', $formdata);
                    $resdata['message'] = "Successfully Done!";
                    $resdata['id'] = "1";
                    // var_dump('expression');die();
                    // return true;
                }

//                   $to =  "phpguidance@gmail.com";

//         // $message = "HI Test from app urls";
                //          $from ="ch.v.sudhakar9@gmail.com";

// // Always set content-type when sending HTML email
                // $headers = "MIME-Version: 1.0" . "\r\n";
                // $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// // More headers
                // $headers .= 'From: ch.v.sudhakar9@gmail.com' . "\r\n";
                // mail($to,$subject,$message,$headers);

// if(sendMail($user_email,$message,$subject)){

//     $formdata['enquiry_name'] = $userDetails->examuser_name;
                //               $formdata['enquiry_email'] = $userDetails->examuser_email;
                //               $formdata['enquiry_mobile'] = $userDetails->examuser_mobile;
                //               $formdata['enquiry_message'] = $this->input->post('message');

//              // echo "<pre>"; print_r($formdata);exit;

//               $this->Common_model->insertSingle('gk_enquiry',$formdata);
                //               $resdata['message'] =  "Successfully Done!";
                //               $resdata['id'] =  "1";
                // }
                // else
                // {
                //     $resdata['message'] =  "Technical Error Please Try Again Once!";
                //               $resdata['id'] =  "0";
                // }

                $this->response([
                    'status' => true,
                    'message' => 'Successfully Get Data',
                    'data' => $resdata,
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function sendEnquiry1_post()
    {
        if ($_POST) {

            $userId = $this->input->post('userId');
            $message = $this->input->post('message');
            $accessToken = $this->input->post('accessToken');

            //write internal andriod developer validations
            if ($userId == '' || $userId == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'userId Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
            if ($accessToken == '' || $accessToken == null) {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Filed is Mandatory',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            $res = checkLogin($userId, $accessToken);
            if ($res) {

                $message = $this->input->post('message');

                //write internal andriod developer validations
                if ($message == '' || $message == null) {
                    $this->response([
                        'status' => false,
                        'code' => '400',
                        'message' => 'Message Filed is Mandatory',
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

                $userDetails = $this->Common_model->getUserDetails($userId);

                // echo "<pre>"; print_r($userDetails);exit;

                $data['username'] = $userDetails->examuser_name;
                $data['mobile'] = $userDetails->examuser_mobile;
                $data['email'] = $userDetails->examuser_email;
                $data['message'] = $this->input->post('message');
                $user_email = $userDetails->examuser_email;

                $subject = "ENQUIRY TO BRAINWIZ From Mobile App (".$data['username'].")";

                $message = $this->load->view('contactSuccess', $data, 'true');

                $to = "phpguidance@gmail.com";

                // $message = "HI Test from app urls";
                $from = "ch.v.sudhakar9@gmail.com";

// Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
                $headers .= 'From: ch.v.sudhakar9@gmail.com' . "\r\n";
                mail($to, $subject, $message, $headers);

                if (sendMail($user_email, $message, $subject)) {

                    $formdata['enquiry_name'] = $userDetails->examuser_name;
                    $formdata['enquiry_email'] = $userDetails->examuser_email;
                    $formdata['enquiry_mobile'] = $userDetails->examuser_mobile;
                    $formdata['enquiry_message'] = $this->input->post('message');

                    // echo "<pre>"; print_r($formdata);exit;

                    $this->Common_model->insertSingle('gk_enquiry', $formdata);
                    $resdata['message'] = "Successfully Done!";
                    $resdata['id'] = "1";
                } else {
                    $resdata['message'] = "Technical Error Please Try Again Once!";
                    $resdata['id'] = "0";
                }

                $this->response([
                    'status' => true,
                    'message' => 'Successfully Get Data',
                    'data' => $resdata,
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            } else {
                $this->response([
                    'status' => false,
                    'code' => '400',
                    'message' => 'accessToken Mismatch',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => false,
                'message' => 'Bad Way Request',
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
