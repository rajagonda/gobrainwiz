<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class Questions_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAnswersByQuestionid($id)
    {
        $sql = "SELECT * FROM gk_question_answers where qa_question_id=$id";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

    public function getRandomQuestionByUserid($uid)
    {

        $sql = "SELECT *
                FROM gk_questions WHERE question_status='active'
                AND question_id NOT IN
                    (SELECT qr_question_id 
                     FROM gk_question_results where qr_user_id=$uid) 
                 ORDER BY RAND() LIMIT 1 ";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $resultdata = (array)$result->row();
            $resultdata['answers'] = array();

            $answers = $this->getAnswersByQuestionid($resultdata['question_id']);
            if (count($answers) > 0) {
                $resultdata['answers'] = $answers;
            }
            return $resultdata;
        } else {
            return '';
        }
    }
    public function addQuestionsResult($form)
    {
        $this->db->insert('gk_question_results', $form);
        return $this->db->insert_id();

    }


}