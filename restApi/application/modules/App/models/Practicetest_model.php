<?php
class Practicetest_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getAllPracticetestList(){
    	$sql= "SELECT gk_pcategories.*,gk_ptopics.subcat_id,count(gk_ptopics.topic_id) as totalTopics FROM gk_pcategories 
    		   INNER JOIN gk_ptopics ON gk_ptopics.subcat_id = gk_pcategories.id              
    		  WHERE gk_pcategories.status='y' GROUP BY gk_ptopics.subcat_id";
    	$res = $this->db->query($sql);
		if($res->num_rows()>0){
		      return $res->result();
		}else {
		      return false;
		}
    }

    public function getUserInfo($userId){
        $sql = "SELECT examuser_create,examuser_id FROM gk_examuserslist WHERE gk_examuserslist.examuser_id = '$userId'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->row();
        }else {
            return false;
        }
    }

    public function getTopics($catId){
        $sql = "SELECT gk_ptopics.*, count(gk_ptests.test_id) as totalTests FROM gk_ptopics 
                INNER JOIN gk_ptests ON gk_ptests.topic_id = gk_ptopics.topic_id
                WHERE gk_ptopics.subcat_id = '$catId' AND gk_ptests.test_status ='y' GROUP BY gk_ptests.topic_id";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function getTopicsWithTestCount($topicId){
        $sql = "SELECT gk_ptests.test_id,gk_ptests.test_name,gk_ptests.test_time,
                count(gk_ptquetions.test_Id) as totalQuestions,gk_ptests.created_date  FROM gk_ptests      
                LEFT JOIN gk_ptquetions  ON gk_ptquetions.test_id = gk_ptests.test_Id          
                WHERE gk_ptests.topic_id = '$topicId' AND gk_ptests.test_status ='y'  GROUP by gk_ptquetions.test_Id ORDER BY gk_ptests.position ASC ";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function getTopicInfo($topicId){
        $sql = "SELECT topic_id,topic_name FROM gk_ptopics WHERE gk_ptopics.topic_id = '$topicId'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->row();
        }else {
            return false;
        }
    }
    public function checkTestResult($testId,$userId){
        $sql = "SELECT * FROM `gk_ptestresult` WHERE `test_id` = ".$testId." and user_Id= ".$userId;
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return true;
        }else {
            return false;
        }
    }
    public function getTestAllQuestions($topicId,$testId){
        $sql = "SELECT q_id,question_name,option1,option2,option3,option4,option5 
                FROM gk_ptquetions WHERE topic_id = '$topicId' AND test_Id='$testId' 
                AND  question_status ='y'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function getAllQuetionsWithAnswer($testId,$userid){
        $sql = "SELECT a.q_id,a.question_name,a.option1,a.option2,a.option3,a.option4,a.option5,a.question_answer,a.question_explanation,a.video_link,b.answer as user_selected,b.q_time FROM `gk_ptquetions` as a join `gk_ptestinformation` as b on a.q_id = b.q_id and b.user_id ='$userid'  WHERE a.test_id ='$testId'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function checkTest($testId){
        $sql = "SELECT * FROM `gk_ptests` WHERE `test_id` = '$testId'";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return true;
        }else {
            return false;
        }
    }
     public function getSingleTestQuestions($testId){
        $sql = "SELECT q_id,question_answer FROM `gk_ptquetions` WHERE `test_id` = '$testId'";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function getTestScore($testId, $userId){
         $sql = "SELECT * FROM `gk_ptestresult` WHERE `test_id` = '$testId' AND user_Id = '$userId'";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->row();
        }else {
            return false;
        }
    }
    public function getalltests(){
         $sql = "SELECT * FROM `gk_ptests` WHERE test_status='y'";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    
    public function getalltestsProfile(){
         $sql = "SELECT * FROM `gk_ptests` ";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    
     public function getTopicsWithTestTime($topicId,$testId,$status)
    {
       $sql = "SELECT gk_ptests.test_id,gk_ptests.test_name,gk_ptests.test_time,
                count(gk_ptquetions.test_Id) as totalQuestions  FROM gk_ptests      
                LEFT JOIN gk_ptquetions  ON gk_ptquetions.test_id = gk_ptests.test_Id          
                WHERE gk_ptests.topic_id = '$topicId' AND gk_ptests.test_id = '$testId' AND gk_ptests.test_status ='$status'  GROUP by gk_ptquetions.test_Id";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->row();
        }else {
            return false;
        }
    }


   public function getalltestsProfileWithID($testId){
         $sql = "SELECT * FROM `gk_ptestresult` WHERE test_id = '$testId' ORDER BY rank ASC ";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }


public function getalltestsProfileWithIDDesc($testId){
         $sql = "SELECT * FROM `gk_ptestresult` WHERE test_id = '$testId' ORDER BY rank DESC ";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }

    public function testMaxMarks($testId)
    {
        $sql = "SELECT MAX(marks) as maxMarks FROM `gk_ptestresult` WHERE test_id = '$testId'  ";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }
    public function testMaxRank($testId)
    {
        $sql = "SELECT MAX(rank) as maxRank FROM `gk_ptestresult` WHERE test_id = '$testId' ";
         $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }

    public function getTestTopper($testId)
    {

       $sql = "SELECT gk_ptestresult.test_id,gk_ptestresult.marks,gk_ptestresult.test_time,gk_ptestresult.rank,gk_examuserslist.examuser_name FROM gk_ptestresult JOIN gk_examuserslist ON gk_ptestresult.user_Id = gk_examuserslist.examuser_id WHERE gk_ptestresult.test_id = '$testId' AND gk_ptestresult.rank != 0 ORDER BY rank ASC  ";

        $res = $this->db->query($sql);
        if($res->num_rows()>0){
            return $res->result();
        }else {
            return false;
        }
    }

    public function update($data, $id){
        $this->db->where('id', $id);
        $this->db->update('gk_ptestresult', $data);
        return $this->db->affected_rows();
    }
    public function getUserRankForTest($userId, $testId)
    {
        $sql = "SELECT FIND_IN_SET( marks, (
            SELECT GROUP_CONCAT( marks
            ORDER BY marks DESC )
            FROM gk_ptestresult where test_id = '$testId' )
            ) AS current_rank
            FROM gk_ptestresult where test_id = '$testId' and user_id = '$userId'"; // only based on marks;

        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return false;
        }
    }

    public function getUserRankForTestIncludesTimeConsume($userId, $testId)
    {
        $sql = "SELECT FIND_IN_SET( marks, (
            SELECT GROUP_CONCAT( marks
            ORDER BY marks DESC )
            FROM gk_ptestresult where test_id = '$testId' )
            ) AS current_rank
            FROM gk_ptestresult where test_id = '$testId' and user_id = '$userId'"; // only based on marks;

        $sql = "select current_rank
        from (select r.*, (@rn := @rn + 1) as current_rank
              from gk_ptestresult r cross join (select @rn := 0) vars where test_id = '$testId'
              order by marks desc, test_time asc
             ) r where test_id = '$testId' and user_id = '$userId'"; // based on marks and test time;

        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getTotalAttendeesCount($testId) {
        $sql = "select count(user_Id) as total_attendees from gk_ptestresult where test_id = '$testId'";

        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
}
?>
