<?php
class Jobalerts_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getAllJobalerts(){

    	$sql = "SELECT title,message,drive_date,last_date,link,CONCAT('/upload/notification/',image) as videoImage  FROM `gk_job_alerts` WHERE 
		job_status = 'y'
		ORDER BY id";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return false;
		}
    }
    
}
?>
