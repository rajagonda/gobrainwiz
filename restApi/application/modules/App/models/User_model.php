<?php
class User_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getExamuserByReferral($code)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE referral_code ='$code' and active='1' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getAllBrainwizStudents()
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_type ='y' and examuser_status='y'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return false;
        }
    }
    public function getExamuserReferralById($id)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE examuser_id ='$id' and active='1' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function updateReferralCode($examuserId)
    {
        $creatNew = true;
        $prev = $this->getExamuserLastReferralById($examuserId);
        $isAlreadyDeactivated = true;
        if(is_object($prev)) {
            $creatNew = ((int)$prev->shareCount) > 0 ? false : true;
            if(!$creatNew) {
                $isAlreadyDeactivated = ((int)$prev->active) > 0 ? false : true;
            }
        }
        if($creatNew) {
            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            $res = "";
            for ($i = 0; $i < 6; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            return $this->db->insert('gk_examuser_referrals', ['referral_code' => $res, 'examuser_id' => $examuserId]);
        } else {
            if($isAlreadyDeactivated) {
                $this->deactivateReferralCodeById($examuserId);
            }
            return true;
        }
    }
    public function deactivateReferralCodeById($examuserId) {
        $this->db->where(['examuser_id' => $examuserId]);
        $this->db->update('gk_examuser_referrals', ['active' => '0', 'deletedAt' => date('Y-m-d H:i:s')]);
    }
    public function deleteInactiveReferralCodes() {
        $this->db->where(['shareCount' => '0']);
        $this->db->delete('gk_examuser_referrals');
    }
    public function updateCronStatus($status) {
        $this->db->update('gk_cron_status', ['status' => $status]);
    }
    public function isCronRunning() {
        $res = $this->db->get('gk_cron_status');
        if ($res->num_rows() > 0) {
            return $res->row()->status == "0" ? false : true;
        } else {
            return false;
        }
    }
    public function deactivateReferralCodes() {
        $this->db->where('active', '1');
        $this->db->update('gk_examuser_referrals', ['active' => '0', 'deletedAt' => date('Y-m-d H:i:s')]);
    }
    public function updateCashbackSettings($data, $where) {
        $this->db->where($where);
        $this->db->update('gk_cashback_settings', $data);
    }
    public function isCashbackActive()
    {
        $sql = "SELECT status FROM gk_cashback_settings WHERE deleted='0'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->status === "1" ? true : false;
        } else {
            return false;
        }
    }
    public function getLastCashbackSettings()
    {
        $sql = "SELECT * FROM gk_cashback_settings WHERE deleted='0' ORDER BY created_at DESC LIMIT 1";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function getReferralJoin($old, $new) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE new_examuser_id='$new'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function getCurrentUserReferredData($id) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE new_examuser_id='$id'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function getNumberOfReferralJoinsByoldId($old) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old'";
        $result = $this->db->query($sql);
        return $result->num_rows();
    }
    public function saveNewCashbackReferral($data) {
        if ($data != null) {
            $this->db->insert("gk_examuser_referral_joins", $data);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function getReferralJoinsByoldId($old)
    {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function getCashbackReferralJoinsByoldId($old)
    {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old' and cashback='1'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function getExamuserLastReferralById($old)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE examuser_id ='$old' ORDER BY `createdAt` DESC limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function increaseShareCount($id) {
        
        $this->db->where('id', $id);
        $this->db->set('shareCount', 'shareCount+1', FALSE);
        $this->db->update('gk_examuser_referrals');
        
        return $this->db->affected_rows();
    }
    public function getUserDetailsByIds($ids=[])
    {
        $this->db->where_in('examuser_id',$ids);
        $res = $this->db->get('gk_examuserslist');
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return [];
        }
    }
}
?>