<?php
class Common_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getWeeklySchudle()
    {
        $sql = "SELECT *  FROM gk_weekly ORDER BY id DESC  limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getUserDetails($userId)
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_id ='$userId' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function checkStudent($mobile)
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_mobile ='$mobile' AND examuser_type='y' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function checkForgotStudent($mobile)
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_mobile ='$mobile' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function checkOtp($userId, $otp)
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_id ='$userId' AND otp_verify ='$otp' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function insertSingle($tablename, $data = null)
    {
        if ($data != null) {
            $this->db->insert($tablename, $data);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function updateRow($tablename, $data, $params)
    {

        foreach ($params as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->update($tablename, $data);
        return $this->db->affected_rows();
    }
    public function getUserDetailsWithMobile($userName)
    {
        $sql = "SELECT examuser_cname as cname, examuser_id as userId,examuser_name as name,examuser_mobile as mobile,
              examuser_email as email,examuser_password as password,notification_token,
              examuser_type as userType,CONCAT('upload/profilepics/',profile_pic) as profilepic, examuser_role as role
              FROM gk_examuserslist WHERE examuser_mobile = '$userName'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getNotificationsCount($userId)
    {
        $sql = "SELECT * FROM gk_notifications_log as a
             WHERE a.student_id ='" . $userId . "' AND a.checked ='0'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->num_rows();
        } else {
            return false;
        }

    }
    public function getAllNotifications($userId)
    {
        $sql = "SELECT a.notification_id,a.checked,b.title, b.message, b.image,
      b.notify_type,  b.catid,b.url,b.topicid,b.subcatid,
      DATE_FORMAT(b.time_update, '%b %d, %Y %h:%i %p') as timeUpdate

      FROM gk_notifications_log as a
                  INNER JOIN gk_notifications as b ON b.nid = a.notification_id
                  WHERE a.student_id ='" . $userId . "' AND checked ='0' ORDER BY b.nid DESC";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return false;
        }
    }
    public function getNotification($nid, $uid)
    {
        $sql = "SELECT notification_id FROM gk_notifications_log WHERE  student_id ='$uid' AND notification_id ='$nid'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getVideoCatName($cid)
    {
        $sql = "SELECT cat_name FROM gk_hvcategoriess WHERE c_id = '$cid'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getTopicAndCatName($topicId)
    {
        $sql = "SELECT gk_ptopics.topic_name, gk_pcategories.category_name FROM gk_ptopics
              INNER JOIN gk_pcategories ON gk_pcategories.id = gk_ptopics.subcat_id
              WHERE gk_ptopics.topic_id = '$topicId'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }

}
