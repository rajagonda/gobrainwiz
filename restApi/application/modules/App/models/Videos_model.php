<?php
class Videos_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getAllvideoCategories(){

    	$sql = "SELECT gk_hvcategoriess.c_id,gk_hvcategoriess.cat_name as cname, 
    	COUNT(gk_hvideos.id) as totalVideos  FROM `gk_hvideos`
		INNER JOIN gk_hvcategoriess ON gk_hvcategoriess.c_id = gk_hvideos.cid
		GROUP BY gk_hvideos.cid";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return false;
		}
    }
    public function getAllvideos($catid){
    	$sql = "SELECT gk_hvcategoriess.c_id,gk_hvcategoriess.cat_name as cname, 
    	        gk_hvideos.id as videoId,gk_hvideos.youtube_link as videoUrl,
    	        gk_hvideos.video_format as videoFormat,gk_hvideos.video_title as videoTitle,
    	        gk_hvideos.video_desc as videoDesc,
    	        CONCAT('public/videoimages/',gk_hvideos.v_image) as videoImage    	         
		    	FROM `gk_hvideos`
				INNER JOIN gk_hvcategoriess ON gk_hvcategoriess.c_id = gk_hvideos.cid
				WHERE gk_hvideos.cid = $catid";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return false;
		}
    }
    public function getVideDetails($videoId){
    	$sql = "SELECT views,id FROM gk_hvideos WHERE id='$videoId'";
    	$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->row();
		}else {
		return false;
		}
    }
}
?>