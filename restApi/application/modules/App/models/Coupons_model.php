<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class Coupons_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCouponByCode($code)
    {
        $this->db->where('coupon_code', $code);
        return $this->db->get('gk_coupons')->row();
    }

	
	public function getCouponById($id)

    {
        $this->db->where('coupon_id', $id);
        return $this->db->get('gk_coupons')->row();
    }

    public function getAllUnusedCoupons()
    {
        $sql = "SELECT *
FROM gk_coupons
WHERE coupon_status='active' AND coupon_id NOT IN
    (SELECT acu_coupon_id 
     FROM gk_assign_coupons_users)";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }

    public function addAssignedCoupon($form)
    {
        $this->db->insert('gk_assign_coupons_users', $form);
        return $this->db->insert_id();
    }

    public function getWalletByUser($id)
    {

        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id='$id'
           		  ORDER BY uw_id DESC";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }

    }

    public function getwithdrawrequestsByUser($id)
    {

        $sql = "SELECT * FROM gk_withdraw_requests where  wr_user_id='$id'
			        ORDER BY wr_id DESC ";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }
    public function getAssignCouponByCouponID($id)
    {
        $this->db->where('acu_coupon_id', $id);
        return $this->db->get('gk_assign_coupons_users')->row();
    }

    public function addWallet($form)
    {
        return $this->db->insert('gk_user_wallet', $form);
    }
    public function addCouponUseage($form)
    {
        return $this->db->insert('gk_coupon_usages', $form);
    }
    public function getAssignedCouponsByUser($id)
    {

        $sql = "SELECT c.* FROM gk_assign_coupons_users uc 
INNER JOIN gk_coupons c ON c.coupon_id = uc.acu_coupon_id where uc.acu_assignee_id=$id
			        ORDER BY uc.acu_id DESC ";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }
    public function getUsedCouponsByUsedUserId($id)
    {

        $sql = "SELECT c.* FROM gk_coupon_usages cu 
INNER JOIN gk_coupons c ON c.coupon_id = cu.cu_coupon_id where cu.cu_used_user_id=$id
			        ORDER BY cu.cu_id DESC";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }
    public function addWithdrawRequest($form)
    {
        return $this->db->insert('gk_withdraw_requests', $form);
    }
    public function getWalletInfo($id){
        $where = array('uw_id' => $id, 'uw_scratch_status' => 0, 'uw_coupon_type' => 'referral');
        $this->db->where($where);
        return $this->db->get('gk_user_wallet')->row();
    }



}
