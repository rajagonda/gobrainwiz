<?php
class Dashboard_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getBanners(){
    	$sql = "SELECT  banner_name,CONCAT('assets/images/img/',banner_location) as banner_location,banner_position FROM gk_mobile_banners 
                WHERE banner_type ='m' AND banner_status='y' 
                ORDER BY banner_position DESC";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return false;
		}
    }
    public function getTopers(){
    	$sql = "SELECT  * FROM gk_voice_toper";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return false;
		}
    }

}
?>