<?php
class Batchtimings_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function getAllBatchs(){
	$temp = [];
    	$sql = "SELECT title,message,batch_date,batch_time,CONCAT('/upload/notification/',image) as batchImage,
 CONCAT('/upload/notification/',backgroundimage) as backgroundImage  FROM `gk_batch_timings` WHERE 
		batch_status = 'y'
		ORDER BY id";
		$res = $this->db->query($sql);
		if($res->num_rows()>0){
		return $res->result();
		}else {
		return $temp;
		}
    }

    public function getCurrentBatchs(){
            $temp = [];
            $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id WHERE batch_status ='y' ORDER BY batch_id DESC";

                 $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return $temp;
            }

	}

    public function getToppers(){
	$temp = [];
        $sql = "SELECT voice_id,student_name,voice_description,company,CONCAT('/upload/topper/',voice_image) as topperImage,college  FROM `gk_voice_toper` WHERE 
        voice_status = 'y'
        ORDER BY voice_id";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
        return $res->result();
        }else {
        return $temp;
        }
    }
    
}
?>
