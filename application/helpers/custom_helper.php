<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('dump')) {
    function dump($str, $exit = false)
    {
        $out = '';
        if (is_array($str)) {
            $out .= '<pre>';
            var_dump($str);
            $out .= '</pre>';
        } else
            var_dump($str);

        if ($exit) die();
    }
}
if (!function_exists('printdump')) {
    function printdump($str, $exit = false)
    {
        if (is_array($str)) {
            echo "<pre>";
            print_r($str);
        } else {
            echo $str;
        }
        if ($exit) die();
    }
}

if (!function_exists('breadcrumb')) {
    function breadcrumb($data)
    {
        $bread = '<ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>';
        $bread .= '<li class="active">Dashboard</li>';
        $bread .= '<li>
                   <a href="' . $data['link'] . '">' . ucfirst($data['label']) . '</a>
                   </li>';
        $bread .= '</ul>';
        return $bread;
    }
}
//By using this method get users sms count
if (!function_exists('usersms')) {
    function usersms()
    {
        $ci = &get_instance();
        $ci->load->model('common_model');
        $userid = $ci->session->userdata('user_id');
        $params = array('user_id' => $userid);
        $result = $ci->common_model->getSingleRow('gk_users', $params);
        if ($result->user_sms != '') {
            return $result->user_sms;
        } else {
            return 0;
        }


    }
}
//By using this method and get customer details as well as prepare the view
if (!function_exists('customer_details')) {
    function customer_details()
    {
        $ci =& get_instance();
        $ci->load->library('session');
        $ci->load->model('common_model');
        $uid = $ci->session->userdata('b2cuser_id');
        $params = array('member_id' => $uid);
        $userdetails = $ci->common_model->getSingleRow('gi_members', $params); ?>
        <div class="inner-banenr myaccount">
            <div class="grid_8">
                <div class=" your-cash-panel">
                    <div class="user push-left">
                        <?php if ($userdetails->member_image != '') { ?>
                            <img style="cursor:pointer;" onclick="$('#profilePicId').click();"
                                 src="<?php echo base_url(); ?>public/profileimages/<?php echo $userdetails->member_image; ?>"
                                 alt="">
                        <?php } else { ?>
                            <img style="cursor:pointer;" onclick="$('#profilePicId').click();"
                                 src="<?php echo base_url(); ?>public/profileimages/" alt="">
                        <?php } ?>
                    </div>
                    <form method="POST" action="<?php echo base_url(); ?>myaccount/fileupload" accept-charset="UTF-8"
                          style="display:none;" id="profilepicFrm" enctype="multipart/form-data"><input name="_token"
                                                                                                        type="hidden"
                                                                                                        value="iSdGw1ceEbuWRg5qCEpElh24AtzIuxzXHn1tOUxq">
                        <input id="profilePicId" name="profilePicId" type="file">
                        <button class="green-submit" type="submit">Upload</button>
                    </form>
                    <div class="grid_5">
                        <h2 class="">Nestham Money
                            <small class="text-color-01">₹ <?php echo $userdetails->member_balance; ?>/-</small>
                        </h2>
                        <div class="wrap push-t-15 search-block">
                            <form action="<?php echo base_url(); ?>myaccount/paycash" method="post" id="paycashform"
                                  name="paycashform" class="form">
                                <p class="amount-tf m-none">
                                    <input type="text" name="payamount" id="payamount" value=""
                                           placeholder="Enter Recharge Amount" class="amount" id="mo-re-amount">
                                    <span class="rupee-symbol" href="#"></span>
                                </p>
                                <input type="submit" class="btn m-none" value="Add Nestham Money">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php

    }
}
/*
 * This Method for send sms to users
*/
if (!function_exists('smssend_new')) {
    function smssend_new11111($mobileNumber = NULL, $msg)
    {
        if ($mobileNumber != null) {

            $senderid = "BRNWIZ";
            $apiurl = "http://www.metamorphsystems.com/index.php/api/bulk-sms";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "username=renegade&password=Renegade@123&to=" . $mobileNumber . "&from=" . $senderid . "&message=" . $msg);
            $buffer = curl_exec($ch);
            curl_close($ch);
            return true;


        }
    }
}

if (!function_exists('smssend_new')) {
    function smssend_new($mobileNumber = NULL, $msg)
    {
        if ($mobileNumber != null) {

            $senderid = "BRNWIZ";
            $apiurl = "http://sms.teleparishkar.com/SMS_API/sendsms.php";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "username=BRNWIZ&password=123456&mobile=" . $mobileNumber . "&sendername=" . $senderid . "&message=" . $msg . "&routetype=1");
            $buffer = curl_exec($ch);
            curl_close($ch);
            return true;


        }
    }
}

if (!function_exists('smssend_new1')) {
    function smssend_new1($mobileNumber = NULL, $msg)
    {
        if ($mobileNumber != null) {


            $query_string = 'www.krishsms.com/SendingSms.aspx?userid=brainwiz&pass=brainwiz@123&phone=' . $mobileNumber . '&msg=' . urlencode($msg);

            // echo $query_string;
            $url = $query_string;
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, $url);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($curl_handle);
            curl_close($curl_handle);


        }
    }
}
if (!function_exists('count_q_t')) {
    function count_q_t($param = NULL, $tid = NULL)
    {
        $ci = &get_instance();
        $ci->load->model('common_model');
        if ($param != NULL && $tid != NULL) {
            if ($param == 'q') {
                $result = $ci->common_model->count_q_t($param, $tid);
                if ($result) {
                    return $result;
                } else {
                    return 0;
                }

            } else {
                $result = $ci->common_model->count_q_t($param, $tid);
                if ($result) {
                    return $result;
                } else {
                    return 0;
                }
            }


        } else {
            return 0;
        }

    }
}
if (!function_exists('checkusertype')) {
    function checkusertype($uid)
    {
        $ci = &get_instance();
        $ci->load->model('common_model');
        if ($uid != NULL) {
            $param = array('userid' => $uid);

            $result = $ci->common_model->getSingleRow('gk_paymentusers', $param);
            if ($result) {
                return true;;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }
}
if (!function_exists('countTopicTests')) {
    function countTopicTests($tid)
    {
        $ci = &get_instance();
        $ci->load->model('common_model');

        //$param = array('userid'=>$uid);

        $result = $ci->common_model->countTopicTests($tid);
        if ($result) {
            return $result;
        } else {
            return 0;
        }


    }
}
if (!function_exists('getTestInfo')) {
    function getTestInfo($testId)
    {
        $ci = &get_instance();
        $ci->load->model('common_model');

        //$param = array('userid'=>$uid);

        $result = $ci->common_model->countTestQuestions($testId);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
}
if (!function_exists('getTestResult')) {
    function getTestResult($testId, $uid)
    {
        $ci = &get_instance();
        $ci->load->model('practicetest_model');

        //$param = array('userid'=>$uid);

        $result = $ci->practicetest_model->gettestresult1($testId, $uid);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}
if (!function_exists('getWalletByUser')) {
    function getWalletByUser($userid)
    {
        $ci = &get_instance();
        $ci->load->database();

        $sql = "SELECT examuser_wallet 
    FROM gk_examuserslist where examuser_id=" . $userid;
        $query = $ci->db->query($sql);
        $row = $query->row();
        return $row->examuser_wallet;
    }
}
if (!function_exists('getCouponCodeFromID')) {
    function getCouponCodeFromID($id)
    {

        $ci =& get_instance();
        $ci->load->database();

        $sql = "SELECT * 
    FROM gk_coupons where coupon_id=" . $id;
        $query = $ci->db->query($sql);
        $row = $query->row();
        return $row->coupon_code;
    }

}

