<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : client
 * Mail id : phpguidance@gmail.com
  */
class login extends MY_Controller {
      
	public function __construct() {
	    parent::__construct();
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('common_model'));
        $this->load->helper('phpass');
    }

public function testmail()
    {
        ini_set('display_errors', 1);
error_reporting(E_ALL);
$server_link = explode('/',$_SERVER['REQUEST_URI']);

$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"."/".$server_link[1];
//require '../../../../../phpmailer1/PHPMailerAutoload.php';
//var_dump($_SERVER['DOCUMENT_ROOT']);die();
require ($_SERVER['DOCUMENT_ROOT'].'/phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer;

$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'brainwiz2015@gmail.com';                 // SMTP username
	$mail->Password = 'Brainwiz@2015';                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;        
	$mail->setFrom('brainwiz2015@gmail.com', 'Brainwiz');			// Set From address
	
	$mail->smtpConnect(
    array(
        "ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
            "allow_self_signed" => true
        )
    )
);

	$to = 'hemanth.raghu23@gmail.com';
	$name = '';
	$mail->addAddress($to, $name);     // Add a recipient
	
	//$mail->addReplyTo($config->MAIL_REPLY_TO, $config->MAIL_REPLY_LABEL);

	
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'test';
	$mail->Body    = 'testing mail';
	/* $mail->AltBody = 'This is the body in plain text for non-HTML mail clients'; */
	if(!$mail->send()) {
		var_dump($mail->ErrorInfo);die();
		$msg = 'Message could not be sent.';
		$msg = 'Mailer Error: ' . $mail->ErrorInfo;
		//return false;
		error_log($mail->ErrorInfo);
		return $mail->ErrorInfo;
	} else {
		var_dump('expression');die();
		return true;
	}	
    }


    public function index()  {
    //   var_dump('cdf');die();
        if($this->authentication->checklogin()){            
        if($this->session->userdata('user_role') == '3'){
        redirect(base_url().'reseller/dashboard');
        }else if($this->session->userdata('user_role') == '4') {
        redirect(base_url().'client/dashboard');
        }else {
           redirect(base_url().'login/logout'); 
        }
        }
    	$data = array();
        $this->template->set_title('Welcome');
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }
        // Load script to setup form validation plugin
        $form_validation_js = assets_url('client/js/formvalidation.js');
        $json_rules = json_encode($rules);
        $script = <<< JS
$(function() {
    CIS.Script.require('{$form_validation_js}', function() {
        CIS.Form.validation('login-form', {$json_rules});
    });
});
JS;

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {       	

           $res = $this->common_model->get_by_username_email($this->input->post('user_name'));
           
           if(!$res){
              $data['username_email_error'] = lang('sign_in_username_email_does_not_exist');
           } else {
            if($res->role_id == '3' || $res->role_id == '4') {
			   if ( ! $this->authentication->check_password($res->user_password, $this->input->post('user_password', TRUE))) {
				$data['sign_in_error'] = lang('sign_in_combination_incorrect');
			    }  else   {
                //Here implement the user session data
                $userdata = array('user_email'=>$res->user_email,
                	              'user_id' => $res->user_id,
                	              'user_password' => $res->user_password,
                	              'user_role' => $res->role_id,
                                  'user_name' => $res->user_name,
                                  'under_userid' => $res->under_userid
                	              );
                if($res->role_id == '3'){
                	$this->session->set_userdata($userdata);
                redirect(base_url().'reseller/dashboard');
                }else {
                	$this->session->set_userdata($userdata);
                redirect(base_url().'client/dashboard');
                }                
				}
            }else {
                 $data['username_email_error'] = lang('sign_in_username_email_does_not_exist');
                }
            }
		}


         $this->load->view('login',$data);
         $this->_load_script($script);      

    }
     public function _rules() {
        $rules = array(
                array('field' => 'user_name','label' => lang('user_email'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'user_password','label' => lang('user_password'),'rules' => 'trim|required|xss_clean|max_length[250]'));
        return $rules;
    }
    public function logout(){
    	  $this->authentication->sign_out();
   	      redirect(base_url().'login');
    }


    

    
}

/* End of file skeleton.php */
/* Location: ./application/modules/skeleton/controllers/skeleton.php */
