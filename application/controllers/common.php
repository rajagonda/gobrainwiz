<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : common
 * Mail id : phpguidance@gmail.com
 */
class common extends MY_Controller {

	public function __construct() {
            parent::__construct();
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('No Access');	
	}
	public function index() {
 	    $this->template->client_view('client/noacess');
	}
	public function checkEmail(){
		$email = $this->input->post('user_email');
		$params = array('user_email'=>$email);
        $res = $this->common_model->getSingleRow('gk_users', $params);
	    if($res !='') {
	      echo 'false';
	    }else {
	      echo 'true';
	    }
	}
	public function checkUsername(){
		$username = $this->input->post('user_name');
		$params = array('user_name'=>$username);
        $res = $this->common_model->getSingleRow('gk_users', $params);
	    if($res !='') {
	      echo 'false';
	    }else {
	      echo 'true';
	    }
	}
	public function httpapi(){
		$this->template->set_title('Sms Api Document');
		$this->template->set_subpagetitle("Api Document");
        $breadcrumbarray = array('label'=> "Api Document",
                       'link' => base_url()."common/httpapi"
                       );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
		$this->template->client_view('client/httpapi');
	}
        

}

/* End of file common.php */
/* Location: ./application/controllers/common.php */