<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : changepassword
 * Mail id : phpguidance@gmail.com
  */
class changepassword extends MY_Controller {

	
      
	public function __construct() {
	    parent::__construct();
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('common_model'));
        $this->load->helper('phpass');
    }
	public function index()	{
		
		$this->template->set_title('changepassword');
       

      $this->template->set_subpagetitle("changepassword");
        $breadcrumbarray = array('label'=> "Change Password",
                       'link' => base_url()."changepassword"
                       );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
         $validationRules = $this->_changepasswordrules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }
      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('password_form',{$json_rules});
</script>
JS;
          
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
         if(demo != '1'){
            $user_id = $this->session->userdata('user_id');
            $oldpassword = $this->input->post('old_password');
            //check old password correct or not
            $params = array('user_id'=>$user_id);
            $res = $this->common_model->getSingleRow('gk_users', $params);
          if ( ! $this->authentication->check_password($res->user_password, $oldpassword, TRUE)) {
			      $data['old_password_error'] = lang('old_password_error');
			    } else   {
    			  $new_password = $this->input->post('new_password');
			      $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
	          $hashed_password = $hasher->HashPassword( $new_password);
	          //update new password
	          $formvalues['user_password'] = $hashed_password;
	         $result = $this->common_model->updateRow('gk_users', $formvalues,$params);
	          if($result !=''){
              $data['password_change_success'] = lang('password_change_success');
            }else {
              $data['old_password_error'] = lang('old_password_error');
            }
			    }

        }else {
          $data['old_password_error'] = "This is demo version";
        }  

        }


        $data['script'] = $script;
         $this->template->client_view('changepassword', $data);
		//$this->template->load_view1('ac/changepassword',$data);
	}
	public function _changepasswordrules() {
        $rules = array(
                array('field' => 'old_password','label' => lang('old_password'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'new_password','label' => lang('new_password'),'rules' => 'trim|required|xss_clean'),
                array('field' => 'confirm_password','label' => lang('confirm_password'),'rules' => 'trim|required|xss_clean|matches[new_password]'));
        return $rules;
    }

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */