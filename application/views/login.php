<!DOCTYPE html>
<html>
<head>
    <title>SMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <!-- Bootstrap CSS -->
    <link href="<?php echo assets_url('client/css/bootstrap.css');?>" rel="stylesheet" media="screen">
    <!-- Main CSS -->
    <link href="<?php echo assets_url('client/css/main.css');?>" rel="stylesheet" media="screen">
    <link href="<?php echo assets_url('client/css/login.css');?>" rel="stylesheet" media="screen">
    <!-- Font Awesome CSS -->
    <link href="<?php echo assets_url('client/fonts/font-awesome.css');?>" rel="stylesheet">
    <!--[if IE 7]>
      <link rel="stylesheet" href="fonts/font-awesome.css">
    <![endif]-->
    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo assets_url('client/js/jquery.js');?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo assets_url('client/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo assets_url('client/js/main.js');?>"></script>
  </head>
<?php
$url = current_url();
?>
   
  <body>
    <div id="login-container">
      <div id="logo">
        <a href="<?php echo base_url();?>"><sup><i class="icon-cloud"></i></sup></a>
      </div>
      <div id="login">
        <?php
         if (isset($sign_in_error) || isset($username_email_error)) { 
        ?>
        <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <b>Alert!</b> 
        <?php if (isset($sign_in_error)) { 
           echo $sign_in_error; 
         }elseif(isset($username_email_error)) {
            echo $username_email_error; 
            }?>
        </div>
        <?php } ?>
        <h5>Please sign in to get access.</h5>
        <form id="login-form" name="login-form" method="post" action="<?php echo $url;?>" class="form">
          <div class="form-group">
            <label for="login-username">Username</label>
            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="User Name">
          </div>
          <div class="form-group">
            <label for="login-password">Password</label>
            <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password">
          </div>
          <div class="form-group">
            <button type="submit" id="login-btn" class="btn btn-success btn-block">Signin</button>
          </div>
        </form>
        <a href="#" class="btn btn-default">Forgot Password?</a>
      </div>
    </div>
  </body>
</html>
