<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="video/mpeg exts=mpeg,mpg,mpe,mp4; charset=utf-8" />
    <title>Go Brainwiz</title>
    <link rel="icon" type="image/png" href="images/Untitled.png" />
    <!--<meta property="og:image"  content="images/logo12.png" />-->
    <!--<link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="images/Untitled.png" />
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="StyleSheet.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/lightbox.css" rel="stylesheet" />

    <script src="js/main.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->
    <!--<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="StyleSheet.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="js/bootstrap.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="StyleSheet.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <script src="js/bootstrap.js"></script>
    <link href="css/js-image-slider.css" rel="stylesheet" />
    <script src="js/js-image-slider.js"></script>
    <!--<script src="googlemap.js"></script>-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>

<body>
    <style>
        .modal-header, .modal-body, .modal-footer {
            background-color: #184682;
        }

        .modal-title {
            color: white;
        }

        #mymodel1 {
            border-style: solid;
            border-width: 5px;
            border-color: #fff #fff;
        }
    </style>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">


            <div class="modal-content" id="mymodel1">
                <div class="modal-header">

                    <h4 class="modal-title">BrainWiz Video Session</h4>
                </div>
                <div class="modal-body">
                    <video class="embed-responsive" controls id="videoindex1" autoplay="autoplay" loop="loop" style="width:100%;height:400px;">

                        <source src="Videos/RAMA7308.mp4" type="video/mp4" />
                    </video>
                </div>
                <div class="modal-footer">
                    <button type="button" id="replayVideo" class="btn btn-default clsmain" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!--<div class="maskConent">
        <div class="mask">
        </div>
        <div class="progress">
            <img src="images/_AMA7230.JPG" />
        </div>
    </div>-->
    <!--<div id="dropElem">
        <div id="dropContent">
            <div id="dropClose">X</div>

            <img src="images/_AMA7230.JPG" class="img-responsive" alt="blue" />
        </div>
    </div>-->
    <div id="mainpage" class="clsmain">

        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12" style="padding-left:0px; padding-right:0px;">
                        <div id="social_icon">
                            <div class="col-md-4 col-lg-4 socialicons">
                                <a href="#"><span class="glyphicon glyphicon-home"></span></a>&nbsp;&nbsp;
                                <a href="#"><span class="glyphicon glyphicon-envelope"></span></a>&nbsp; Pavanjaiswal5@gmail.com
                            </div>
                            <!--<div class="col-md-2 col-lg-2 col-md-offset-0 col-lg-offset-0 hidden-sm hidden-xs">
                                <div class="btn btn-primary btn-xs" style="margin-top:6px;color:white;" id="regdw">
                                    <a href="Rigister.html" style="color:white;"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Register to Download Papers</a>&nbsp;&nbsp;

                                </div>
                            </div>-->
                            <div class="col-md-4 col-lg-4 visible-md visible-lg  socialicons">
                                <div class="btn btn-primary">

                                    <a style="color:#fff;" href="ShareSuccess.html" target="_self">
                                        <i class="fa fa-pencil glyphicon glyphicon-envelope" aria-hidden="true">
                                        </i> Share Your Success
                                    </a>
                                </div>

                            </div>

                            <div class="col-md-4 col-lg-4 visible-md visible-lg  socialicons">
                                <div class="btn btn-primary">

                                    <a style="color:#fff;" href="Rigister.html" target="_self">
                                        <i class="fa fa-pencil glyphicon glyphicon-pencil" aria-hidden="true">
                                        </i> Register to Download Papers
                                    </a>
                                </div>
                                <a href="#"><img src="images/google.png" alt="googleicon" /></a>&nbsp;&nbsp;
                                <a href="#"><img src="images/facebook.png" alt="fbicon" /></a>&nbsp;&nbsp;
                                <a href="#"><img src="images/twitter.png" alt="twittericon" /></a>&nbsp;&nbsp;
                                <a href="#"><img src="images/youtube.png" alt="youtubeicon" /></a>&nbsp;&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--end social icons-->
        <!--<section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-xs-12" id="logo_baner" style="padding-left:0px; padding-right:0px; height:100px;">

                        <div class="col-md-5" style="padding-right:0px;">
                            <a href="index.html">
                                <img id="logobnr" src="images/LOGO.png" style="margin-top:10px; margin-left:00px;" class="img-responsive" />
                            </a>
                        </div>
                        <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                            <div class="col-md-11" style="padding-left:0px; color:#2D257B; padding-right:0px; margin-top:15px;">
                                <nav role="navigation" class="navbar" style="padding-left:0px;padding-right:0px;">

                                    <div class="navbar-header">
                                        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div id="navbarCollapse" class="collapse navbar-collapse  navbar-right">
                                        <ul class="nav navbar-nav">
                                            <li><a href="index.html">Home</a></li>

                                            <li><a href="DownloadNew.html">Downloads</a></li>
                                            <li><a href="videos.html">Videos</a></li>
                                            <li><a href="Aboutus.html">AboutUs</a></li>
                                            <li class=""><a href="contactus.html">ContactUs</a></li>
                                            <li class="active"><a href="Login.html">Login</a></li>
                                            <li class=""><a href="#">Elitmus</a></li>

                                        </ul>
                                    </div>
                                </nav>
                            </div>

                            <div class="col-md-1" id="enquiry" style="padding-left:0px; padding-right:0px; margin-top:30px;">
                                <style>
                                    @media only screen and (max-width: 420px) {
                                        #enquiry {
                                            display: none;
                                        }
                                        .carousel slide{
                                            margin-top:120px;
                                        }

                                    }
                                </style>
                                <div class="dropdown" id="enquiry" style="margin-top:5px;">
                                    <a>
                                        <span class="dropbtn">Enquiry</span>
                                    </a>
                                    <div class="dropdown-content">
                                        <input type="text" class="form-control" placeholder="Name" />
                                        <input type="text" class="form-control" placeholder="Email" />
                                        <input type="text" class="form-control" placeholder="Mobile" />
                                        <textarea rows="4" class="form-control"></textarea>
                                        <input type="button" class="btn btn-primary" value="submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->



        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-xs-12">

                        <div id="logo_baner" style="padding-left:0px; padding-right:0px; height:0px;">

                        </div>
                        <div class="row">
                            <div class="col-md-4" style="padding-right:0px;">
                                <a href="index.html">
                                    <img id="logobnr" src="images/LOGO.png" style="margin-top:-5px; margin-left:00px;" class="img-responsive" />
                                </a>
                            </div>

                            <div class="col-md-7" style="padding-left:0px; padding-right:0px;">
                                <div class="col-md-11" style="padding-left:0px; color:#2D257B; padding-right:0px; margin-top:15px;">
                                    <nav role="navigation" class="navbar" style="padding-left:0px;padding-right:0px;">

                                        <div class="navbar-header">
                                            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                        </div>

                                        <div id="navbarCollapse" class="collapse navbar-collapse  navbar-right">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="index.html">Home</a></li>

                                                <li><a href="videos.html">Videos</a></li>
                                                <li><a href="Aboutus.html">AboutUs</a></li>
                                                <li><a href="contactus.html">ContactUs</a></li>
                                                <li><a href="Amcat.html">Amcat</a></li>

                                                <li class=""><a href="Login.html">Login</a></li>

                                            </ul>
                                        </div>
                                    </nav>
                                </div>

                                <div class="col-md-1" id="enquiry" style="padding-left:0px; padding-right:0px; margin-top:30px;">

                                    <div class="dropdown">
                                        <a>
                                            <span class="dropbtn">Enquiry</span>
                                        </a>
                                        <div class="dropdown-content">
                                            <input type="text" class="form-control" placeholder="Name" />
                                            <input type="text" class="form-control" placeholder="Email" />
                                            <input type="text" class="form-control" placeholder="Mobile" />
                                            <textarea rows="4" class="form-control"></textarea>
                                            <input type="button" class="btn btn-primary" value="submit" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- header baner-->

        <style>
            /*#regdw a:hover {
                color: red;
            }*/

            .navbar {
                /*336699trythis color*/
                border-radius: 0px;
                margin-bottom: 10px;
            }

            li a {
                color: #000000;
                font-weight: bold;
            }

            .navbar-toggle {
                background-color: #fff;
            }

            .icon-bar {
                background-color: #184682;
            }

            .navbar li a:hover {
                color: none;
                background-color: goldenrod;
            }

            @media only screen and (max-width: 420px) {
                .navbar {
                    background-color: #184682;
                    border-radius: 5px;
                    color: white;
                    z-index: 50;
                    /*margin-bottom:50px;*/
                }

                #carousel-example-generic .img-responsive {
                    /*margin-top: 55px;*/
                }

                #imgcaro {
                    /*margin-top: 38px;*/
                }

                #carhide {
                    display: none;
                }

                #enquiry {
                    display: none;
                }

                li a {
                    color: white;
                }

                .navbar-collapse {
                    padding-left: 0px;
                    padding-right: 0px;
                }

                div.navbar-header {
                    padding-left: 0px;
                    padding-right: 0px;
                    /*margin-bottom: 10px;*/
                }


                #logobnr {
                    margin-left: 0px;
                }
            }
        </style>


        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <div id="imgcaro">
                <div id="carhide">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                </div>

                <!-- Wrapper for slides -->

                <div class="carousel-inner">
                    <div class="item active">
                        <img src="new_img//Brain Wiz new_tcs.jpg" class="img-responsive" alt="...">

                    </div>
                    <div class="item">
                        <img src="new_img/Brain Wiz_wipro.jpg" class="img-responsive" alt="...">

                    </div>
                    <div class="item">
                        <img src="new_img/Brain Wiz_mnc.jpg" class="img-responsive" alt="...">

                    </div>

                    <div class="item">
                        <img src="new_img/Brain Wiz new_modul.jpg" class="img-responsive" alt="...">

                    </div>
                    <div class="item">
                        <img src="new_img/Brain Wiz new_cmpny.jpg" class="img-responsive" alt="...">

                    </div>
                    <div class="item">
                        <img src="new_img/422.jpg" class="img-responsive" alt="...">

                    </div>
                    <div class="item">
                        <img src="new_img/5252.jpg" class="img-responsive" alt="...">

                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
        <!--slider show-->
        <!--<script type="text/javascript">
            $("#flipbook").turn({
                width: 400,
                height: 300,
                autoCenter: true
            });
        </script>-->

        <section style="padding-left:0px;padding-right:0px; margin-top:0px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12" style="margin-top:0px;">

                        <div id="table_marquee">
                            <div class="row">
                                <div class="col-md-9 col-xs-12 col-sm-12">
                                    <div id="latest_updates" style="margin-top:5px; padding-left:0px;padding-right:0px;">
                                        <style>
                                            #table_marquee h2 {
                                                background-color: #184682;
                                                padding: 10px;
                                                border-top-left-radius: 10px;
                                                border-top-right-radius: 10px;
                                                border: 2px solid #D9D9D9;
                                                color: white;
                                            }

                                            .bol {
                                                width: 100%;
                                                height: 50px;
                                                margin-top: 10px;
                                                -webkit-box-shadow: 0 3px 2px -2px #dbdcaa;
                                                border-radius: 6px;
                                                text-align: center;
                                                font-family: open_sansregular;
                                                font-weight: 500;
                                                font-size: 16px;
                                                padding: 5px;
                                                color: black;
                                                background-color: ghostwhite;
                                                transform-style: preserve-3d;
                                                position: relative;
                                                top: 0;
                                                transition: top ease 0.5s;
                                            }

                                                .bol:hover {
                                                    top: -8px;
                                                    -webkit-box-shadow: 0px 1px 1px 0px #184682;
                                                    box-shadow: 1px 2px 2px #184682;
                                                    position: relative;
                                                    /*-webkit-box-shadow: 0 8px 6px -6px black;
                                               -moz-box-shadow: 0 8px 6px -6px black;
                                                    box-shadow: 0 8px 6px -6px black;*/
                                                    /*-moz-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
                                            -webkit-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
                                            box-shadow: 5px 5px 5px rgba(68,68,68,0.6);*/
                                                    filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
                                                    -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
                                                    /*zoom: 1.1;*/
                                                    color: #184682;
                                                }


                                            @media only screen and (max-width: 420px) {
                                                .bol {
                                                    width: 230px;
                                                    height: 40px;
                                                    margin-top: 10px;
                                                    margin-left: -22px;
                                                    -webkit-box-shadow: 0 3px 2px -2px #dbdcaa;
                                                    border-radius: 6px;
                                                    text-align: center;
                                                    font-family: open_sansregular;
                                                    font-weight: 300;
                                                    font-size: 14px;
                                                    padding: 0px;
                                                    color: black;
                                                    background-color: #f3eeee;
                                                    transform-style: preserve-3d;
                                                    position: relative;
                                                    top: 0;
                                                    transition: top ease 0.5s;
                                                }

                                                    .bol:hover {
                                                        top: -6px;
                                                        -webkit-box-shadow: 0px 1px 1px 0px #184682;
                                                        box-shadow: 0 4px 3px #184682;
                                                        position: relative;
                                                        /*-webkit-box-shadow: 0 8px 6px -6px black;
                                               -moz-box-shadow: 0 8px 6px -6px black;
                                                    box-shadow: 0 8px 6px -6px black;*/
                                                        /*-moz-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
                                            -webkit-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
                                            box-shadow: 5px 5px 5px rgba(68,68,68,0.6);*/
                                                        filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
                                                        -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
                                                        /*zoom: 1.1;*/
                                                        color: #184682;
                                                    }
                                            }

                                            .testsrs_wdgt {
                                                border-radius: 10px;
                                                box-shadow: 0 2px 3px -1px #999;
                                                border-bottom: none;
                                                transition: .3s ease-out;
                                                background: rgba(251,251,251,1);
                                                background: -moz-linear-gradient(top,rgba(251,251,251,1) 0,rgba(226,226,225,1) 96%,rgba(199,199,198,1) 100%);
                                                background: -webkit-gradient(left top,left bottom,color-stop(0,rgba(251,251,251,1)),color-stop(96%,rgba(226,226,225,1)),color-stop(100%,rgba(199,199,198,1)));
                                                background: -webkit-linear-gradient(top,rgba(251,251,251,1) 0,rgba(226,226,225,1) 96%,rgba(199,199,198,1) 100%);
                                                background: -o-linear-gradient(top,rgba(251,251,251,1) 0,rgba(226,226,225,1) 96%,rgba(199,199,198,1) 100%);
                                                background: -ms-linear-gradient(top,rgba(251,251,251,1) 0,rgba(226,226,225,1) 96%,rgba(199,199,198,1) 100%);
                                                background: linear-gradient(to bottom,rgba(251,251,251,1) 0,rgba(226,226,225,1) 96%,rgba(199,199,198,1) 100%);
                                                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fbfbfb',endColorstr='#c7c7c6',GradientType=0);
                                            }
                                        </style>

                                        <h4 style="color:#fff;">Batch Timings</h4>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 col-sm-12 hidden-sm hidden-xs">
                                                <div style="overflow-x:auto;">
                                                    <table>
                                                        <tr>
                                                            <th>Course</th>

                                                            <th>Date</th>
                                                            <th>Timing</th>
                                                            <th>Faculty</th>
                                                            <th>Register</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Campus Recruitment Training</td>
                                                            <td>5th Dec 2016</td>
                                                            <td>02:00 PM to 4:00 PM</td>
                                                            <td>Mr.Pavan & Team</td>
                                                            <td><a href="#">Register</a></td>
                                                        </tr>
                                                        <tr>

                                                            <td>Campus Recruitment Training</td>
                                                            <td>8th Nov 2016</td>
                                                            <td>06:00 PM to 8:00 PM</td>
                                                            <td>Mr.Pavan & Team</td>
                                                            <td><a href="#">Register</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Campus Recruitment Training</td>
                                                            <td>3rd OCT 2016</td>
                                                            <td>09:00 AM to 11:00 AM</td>
                                                            <td>Mr.Pavan & Team</td>
                                                            <td></td>
                                                        </tr>
                                                       
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>

                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                        </tr>-->
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>


                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                        </tr>-->
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>

                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                        </tr>-->


                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-sm-12 hidden-md hidden-lg">
                                                <div style="overflow-x:auto;">
                                                    <table>
                                                        <tr>
                                                            <th>Course</th>

                                                            <th>Date</th>
                                                            <th>Timing</th>

                                                        </tr>
                                                        <tr>
                                                            <td>Campus Recruitment Training</td>
                                                            <td>5th Dec 2016</td>
                                                            <td>02:00 PM to 4:00 PM</td>

                                                        </tr>
                                                        <tr>
                                                            <td>Campus Recruitment Training</td>
                                                            <td>8th Nov 2016</td>
                                                            <td>06:00 PM to 8:00 PM</td>

                                                        </tr>
                                                        <tr>
                                                            <td>Campus Recruitment Training</td>
                                                            <td>3rd OCT 2016</td>
                                                            <td>09:00 AM to 11:00 AM</td>

                                                        </tr>
                                                       
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>


                                                        </tr>-->
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>


                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                        </tr>-->
                                                        <!--<tr>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>

                                                            <td>Testing Tools</td>
                                                            <td>Testing Tools</td>
                                                        </tr>-->


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <div class="col-md-1"></div>
                                                <div class="col-md-10">
                                                    <div class="col-md-4 col-xs-12 col-sm-12 ">

                                                        <button type="button" onclick="window.location.href = 'DownloadNew.html'" class="bol">
                                                            Download Papers&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span class="glyphicon glyphicon-cloud-download" style="align-items:flex-end"></span>
                                                        </button>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12 col-sm-12 ">

                                                        <button type="button" onclick="window.location.href='Elitmus.html'" class="bol">Know Elitmus&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-education" style="align-content:flex-end"></span> </button>

                                                    </div>
                                                    <div class="col-md-4 col-xs-12 col-sm-12 ">
                                                        <button type="button" onclick="window.location.href='#'" class="bol">Online Test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-modal-window" style="align-content:flex-end"></span> </button>

                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12 col-sm-12 ">
                                    <div id="latest_updates" style="margin-top:5px;">
                                        <h4 style="color:#fff;">Latest Updates</h4>
                                        <div class="latest_updates" style="margin:0px;">
                                            <!--<div id="flipbook">
                                                <div class="hard"><img src="icons/60x100twitter.jpg" /> </div>
                                                <div class="hard"></div>
                                                <div> Page 1 </div>
                                                <div> Page 2 </div>
                                                <div> Page 3 </div>
                                                <div> Page 4 </div>
                                                <div class="hard"></div>
                                                <div class="hard"></div>
                                            </div>-->
                                            <!--<marquee width=100% height=232 direction=up">-->
                                            <style>
                                                #slideshow p {
                                                    margin-top: 10px;
                                                    font-weight: 200;
                                                }
                                            </style>
                                            <!--<div id="slideshow">


                                                <div>
                                                    <img src="new_img/Banner 01.jpg" class="img-responsive" />
                                                </div>
                                                <div>
                                                    <img src="new_img/Banner 01.jpg" class="img-responsive" />
                                                </div>
                                                <div>
                                                    <img src="new_img/Banner 01.jpg" class="img-responsive" />
                                                </div>
                                            </div>-->

                                            <div id="sliderFrame">
                                                <div id="slider">

                                                    <img src="new_img/New Batch 8 Nov - Copy.jpg" class="img-responsive" alt="newbatch_5dec" />
                                                    <img src="new_img/New Batch 8 Nov.jpg" class="img-responsive" />
                                                    <img src="new_img/Aptitude Topics.jpg" alt="" class="img-responsive" />
                                                    <img src="new_img/Crack E litmus.jpg" alt="" class="img-responsive" />
                                                    <img src="new_img/CRT Comprises.jpg" class="img-responsive" />
                                                </div>

                                            </div>


                                            <style>
                                                #slideshow {
                                                    margin: 25px auto;
                                                    position: relative;
                                                    width: 100%;
                                                    height: 190px;
                                                    padding: 5px;
                                                    font-family: Georgia, 'Times New Roman', Times, serif;
                                                    color: #184682;
                                                    font-style: inherit;
                                                    box-shadow: 0 0 20px rgba(0,0,0,0.4);
                                                }

                                                    #slideshow > div {
                                                        position: absolute;
                                                        top: 10px;
                                                        left: 10px;
                                                        right: 10px;
                                                        bottom: 0px;
                                                    }
                                            </style>
                                            <!--<script>
                                                $("#slideshow > div:gt(0)").hide();

                                                setInterval(function () {
                                                    $('#slideshow > div:first')
                                                      .fadeOut(1000)
                                                      .next()
                                                      .fadeIn(1000)
                                                      .end()
                                                      .appendTo('#slideshow');
                                                }, 3000);
                                            </script>-->

                                        </div>
                                    </div>
                                </div>

                                <!--<div class="col-md-3 col-xs-12 col-sm-12 hidden-lg hidden-md">
                                    <div id="latest_updates" style="margin-top:5px;">
                                        <h4 style="color:#fff;">Latest Updates</h4>
                                        <div class="latest_updates" style="margin:0px;">

                                            <style>
                                                #slideshow p {
                                                    margin-top: 10px;
                                                    font-weight: 200;
                                                }
                                            </style>


                                            <div id="sliderFrame">
                                                <div id="slider">

                                                    <img src="new_img/New Batch 8 NovM.jpg" class="img-responsive" />
                                                    <img src="new_img/Aptitude TopicsM.jpg" alt="" class="img-responsive" />
                                                    <img src="new_img/Crack E litmusM.jpg" alt="" class="img-responsive" />
                                                    <img src="new_img/CRT ComprisesM.jpg" class="img-responsive" />
                                                </div>

                                            </div>


                                            <style>
                                                #slideshow {
                                                    margin: 25px auto;
                                                    position: relative;
                                                    width: 100%;
                                                    height: 190px;
                                                    padding: 5px;
                                                    font-family: Georgia, 'Times New Roman', Times, serif;
                                                    color: #184682;
                                                    font-style: inherit;
                                                    box-shadow: 0 0 20px rgba(0,0,0,0.4);
                                                }

                                                    #slideshow > div {
                                                        position: absolute;
                                                        top: 10px;
                                                        left: 10px;
                                                        right: 10px;
                                                        bottom: 0px;
                                                    }
                                            </style>

                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <style>
            Container {
                width: 100px;
                height: 100px;
                position: relative;
            }

            #Video #Message {
                width: 100px;
                height: 100px;
                position: absolute;
                top: 0;
                left: 0;
            }
            #Message {
                text-shadow: 2px 2px 2px #000000;
                color: white;
            }

                #Message h1 {
                    font-size: 3em;
                    color: #ffffff;
                    text-align: center;
                    padding: 0.4em;
                }
        </style>

        <section id="brain-1">

            <div class="container-fluid">
                <div class="row" id="portpol">
                    <div class="col-md-12" style="margin-top:20px;padding-left:0px; padding-right:0px;">
                        <div class="row">

                            <div class="col-md-4 col-xs-12 col-sm-12" style="">
                                <div id="imgsld">
                                    <center>
                                        <div id="img_md" style="padding-left:0px;padding-right:0px;">
                                            <a><img src="images/pavanjaisval.jpg" class="img-rounded img-responsive" alt="pavan" /></a>
                                            <h3>Pavan Jaiswal</h3>
                                            <div>
                                                <p>
                                                    He is well Know for Campus Recruitment
                                                    Trining.
                                                    He has Trained Thousands of students across INDIA. His vast teaching Experience and tremendous Knowledge helps in transforming Common students into efficient Employees in top MNC's by his smart and effective methods.

                                                </p>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12 col-sm-12" style="">
                                <div id="">
                                    <div id="img_md" style="height:312px;">
                                        <center>

                                            <div class="slider-wrap" id="img-slid">
                                                <div class="slider">
                                                    <ul>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <a><img src="images/1.png" style="border-radius:8px;" class="img-rounded" alt="pavan" /></a>
                                                                    <div>
                                                                        <h3>Swetha</h3>
                                                                        <p>
                                                                            Thank you very much pavan sir, I got selected for HCL Technologies., Ltd., You are the best teacher for aptitude training.
                                                                            Thans alot for your valuable training.

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md2">
                                                                    <a><img src="images/2.png" class="img-rounded" alt="pavan" /></a>
                                                                    <div>
                                                                        <h3>Sudanshu</h3>
                                                                        <p>
                                                                            Brainwiz is the best for CRT in india. Thank you very much for your teaching sir. I have been placed in CAPGemini with very good package.

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <a href="#" target="_self"><img src="images/1438115014_Srushti.png" class="img-rounded" alt="pavan" /></a>

                                                                    <div>

                                                                        <h3>Srushti</h3>
                                                                        <p>
                                                                            Thanks pavan sir for giving me such a quality training, only with the help of you, we can able to crack aptitude round of both Infosys and Wipro. Finallly selected in Infosys.

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md2">
                                                                    <img src="images/1438113785_Md._Sadiq.png" class="img-rounded" alt="pavan" />
                                                                    <div>
                                                                        <h3>Md. Sadiq</h3>
                                                                        <p>
                                                                            Hello sir, i wanna thank you for your CRT classes. I placed at TechMahindra. Also i was able to clear aptitude in Wipro. Thank you so much for your guideance.

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <img src="images/1438114912_Sony_Reddy.png" class="img-rounded" alt="pavan" />
                                                                    <div>
                                                                        <h3>Sony Reddy</h3>
                                                                        <p>
                                                                            Branwiz helped me alot in achieving my goal. your teaching is really extraordinary. Thanks for everything sir.

                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <img src="images/1438115188_Pinky_Patel.png" class="img-rounded" alt="pavan" />
                                                                    <div>
                                                                        <h3>Pinky Patel</h3>
                                                                        <p>
                                                                            Thank you very much pavan sir, I got selected for Wipro. Beacause of your aptitude training. Thanks alot for your exclusive training.

                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <img src="images/1438115776_Ankush_Mishra.png" class="img-rounded" alt="pavan" />
                                                                    <div>
                                                                        <h3>Ankush Mishra</h3>
                                                                        <p>
                                                                            Got selected for Wipro. Thanks to Brainwiz and your awesome lecture.

                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </center>
                                                        </li>
                                                        <li>
                                                            <center>
                                                                <div id="img_md1">
                                                                    <img src="images/1438115895_Souvik.png" class="img-rounded" alt="pavan" />
                                                                    <div>
                                                                        <h3>Souvik</h3>
                                                                        <p>
                                                                            Thank you very much for your teaching. I have been selected in Caliber in Campus selection. Thank you very much. Friends, you want to crack aptitude test just go and attend classes in Brainwiz.

                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </center>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="slider-arrow sa-left">&lt;</a> <a href="#" class="slider-arrow sa-right">&gt;</a>
                                            </div>

                                        </center>
                                    </div>
                                </div>
                            </div>


                            <style>
                                @media only screen and (max-width: 420px) {
                                    #img-slid {
                                        width: 245px;
                                    }

                                    #sliderFrame {
                                        width: 100%;
                                        margin: 0 auto; /*center-aligned*/
                                    }

                                    #slider, #slider .sliderInner {
                                        width: 100%;
                                        height: 250px; /* Must be the same size as the slider images */
                                        border-radius: 3px;
                                    }
                                }
                            </style>

                            <script>
                                $(document).ready(function () {
                                    $('#videoindex1')[0].pause();
                                    $('#replayVideo ').click(function () {
                                        debugger;
                                        $('#videoindex1')[0].pause();
                                        $('#mainpage').fadeTo(1000, 5);
                                        $('#videoindex')[0].play();
                                    });

                                });
                                function pauseVideo() {

                                    $('#videoindex')[0].pause();
                                    $('#mainpage').fadeTo(1000, 0.2);
                                    $('#videoindex1')[0].play();
                                    //if ($('#mainpage').click())
                                    //{
                                    //    $('#mainpage').click(function () {
                                    //        pagereload();
                                    //    });
                                    //}
                                }
                                function pagereload() {
                                    location.reload();
                                }
                                $('#mainpage').click(function () {
                                    //location.reload();

                                    //$('#videoindex1')[0].pause();
                                    //$('#mainpage').fadeTo(1000, 5);
                                    //$('#videoindex')[0].play();

                                });

                            </script>

                            <div class="col-md-4 col-xs-12 col-sm-12" style="">
                                <div id="imgsld">
                                    <center>
                                        <div id="img_md" style="padding-left:0px;padding-right:0px;height:312px;">
                                            <div>
                                                <!--<button  type="button" class="" data-toggle="modal" data-target="#myModal" style="background-color:ghostwhite;margin-left:5px;border-color:ghostwhite;">-->
                                                <a href="#" class="" onclick="pauseVideo()" data-toggle="modal" data-target="#myModal" style="background-color:ghostwhite;margin-left:5px;border-color:ghostwhite;">

                                                    <div id="Container">
                                                        <div id="Video">
                                                            <video class="embed-responsive" preload="auto" muted id="videoindex" autoplay="autoplay" loop="loop" style="width:100%;height:250px;">

                                                                <source src="Videos/RAMA7308.mp4" type="video/mp4" />
                                                            </video>

                                                            <!--<embed src="Videos/RAMA7308.mp4" type="video/mp4" allowfullscreen="true" width="425" height="344">
                                                            </embed>-->
                                                            <!--<iframe id="videoindex" height="185" src="https://www.youtube.com/embed/DqROxv0pDDA&amp;autoplay=1" frameborder="0" volume="0" allowfullscreen></iframe>-->
                                                            <!--<iframe id="myVideo" src="https://www.youtube.com/embed/uNRGWVJ10gQ?rel=0&amp;autoplay=1" width="560" height="315" frameborder="0" allowfullscreen></iframe>-->
                                                        </div>
                                                    </div>
                                                </a>
                                                <!--</button>-->
                                            </div>
                                            <!--<video width="320" height="240" id="some" autoplay="autoplay" controls muted>
                                                <source src="Videos/RAMA7308.mp4" type="video/mp4">

                                                Your browser does not support the video tag.
                                            </video>-->
                                        </div>
                                    </center>
                                </div>
                            </div>


                        </div>


                        <!--<a href="#" class="slider-arrow-1 sa-left-1">&lt;</a>
                        <a href="#" class="slider-arrow-1 sa-right-1">&gt;</a>-->
                    </div>
                </div>

            </div>

        </section>



        <section id="add">

            <div class="col-md-12 address-ads ">
                <div class="col-md-3 col-md-offset-1 address-grid">
                    <div class="vertical">
                        <h4>ADDRESS</h4>
                        <p>
                            <strong style="font-size:16px;">BRAINWIZ</strong> ,Inc.b <br />
                            # 3rd Floor, Beside Reliance Fresh ,<br />
                            Near Satyam Theatre ,
                            Srinivasa Nagar ,<br /> Ameerpet , <br />Hyderabad.
                        </p>
                        <h4>Email:</h4><a href="" style="color:white">pavanjaiswal5@gmail.com</a>
                        <p>Phone : +91-88862 33228</p>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 address-grid">
                    <div class="vertical">
                        <h4>QUICK LINKS</h4>
                        <a href="contactus.html"><img src="icons/01enroll.png" class="img-responsive" alt="o1enroll" /></a>
                        <a href="DownloadNew.html"><img src="icons/02downloads.jpg" class="img-responsive" alt="o1enroll" /></a>
                        <a href="#"><img src="icons/03placements.jpg" class="img-responsive" alt="o1enroll" /></a>
                    </div>

                </div>
                <div class="col-md-4 map" style="margin-top:50px; margin-bottom:10px;" id="map">

                    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>


                    <!--<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAgM0o_TdCyLmxDLC4sJ03X-0E80jFeQvg'></script><div style='overflow:hidden;height:400px;width:520px;'><div id='gmap_canvas' style='height:400px;width:400px;'></div>
                    <style>
                        #gmap_canvas img {
                            max-width: none !important;
                            background: none !important;
                        }
                    </style></div> <a href='https://addmap.net/'>map generator</a>
                    <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=a6bb97d250c846af27abf2678a69388a2496018d'></script>
                    <script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(17.4374614,78.4482878),mapTypeId: google.maps.MapTypeId.ROADMAP};
                        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(17.4374614,78.4482878)});infowindow = new google.maps.InfoWindow({content:'<strong>Brainwiz</strong><br>Ameerpet hyderabad<br>500038 Hyderabad<br>'});google.maps.event.addListener
                        (marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>-->



                    <div style='overflow:hidden;height:250px;width:100%;'>

                        <div id='gmap_canvas' style='height:100%;width:100%;'></div><div>
                            <small>
                                <a href="http://www.embedgooglemaps.com/en/">
                                    Generate your map here, quick and easy!                                 Give your customers directions
                                    Get found
                                </a>
                            </small>
                        </div>


                        <div><small><a href="https://www.amazon.com/Extra-Large-Folding-Kennel-Plastic/dp/B00M3NBJ8E/ref=sr_1_5?s=pet-supplies&ie=UTF8&qid=1470313482&sr=1-5keywords=pet+cage">Pet Wire Cage</a></small></div>
                        <style>
                            #gmap_canvas img {
                                max-width: none !important;
                                background: none !important;
                            }
                        </style>
                    </div>
                    <script type='text/javascript'>
                        function init_map() {
                            var myOptions = { zoom: 17, center: new google.maps.LatLng(17.43758422941661, 78.44885374610215), mapTypeId: google.maps.MapTypeId.ROADMAP };
                            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                            marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(17.43758422941661, 78.44885374610215) });
                            infowindow = new google.maps.InfoWindow({ content: '<strong>Brainwiz</strong><br>ameerpet, Hyderabad <br>' });
                            google.maps.event.addListener(marker, 'click', function () { infowindow.open(map, marker); });
                            infowindow.open(map, marker);
                        } google.maps.event.addDomListener(window, 'load', init_map);
                    </script>
                </div>
            </div>

        </section>
        <section></section>

        <!--adderss ends-->
        <!--footers -->
        <section id="foot">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="padding-left:0px;padding-right:0px; background-color:#FECA07" id="footer">
                        <footer>
                            <p><b>FOLLOW US ON:COPYRIGHT © 2016. ALL RIGHTS RESERVED: BRINWIZ</b></p>
                        </footer>
                    </div>
                </div>
            </div>
        </section>
        <style>
            #foot p {
                text-shadow: 1px 1px #184682;
            }
        </style>

        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="jquery.lbslider.js"></script>
        <script>
            jQuery('.slider').lbSlider({
                leftBtn: '.sa-left',
                rightBtn: '.sa-right',
                visible: 1,
                autoPlay: true,
                autoPlayDelay: 5
            });


        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#pomplent").click();
            });

        </script>


        <!--<div id="lightboxOverlay" class="lightboxOverlay" style="width: 1349px; height: 3454px; display: none;"></div><div id="lightbox" class="lightbox" style="display: none; top: 50px; left: 0px;"><div class="lb-outerContainer" style="width: 333px; height: 518px;"><div class="lb-container"><img class="lb-image" src="./శ్రీ సుందర లక్ష్మీనృసింహ స్వామి దేవాలయం - పాలెం_files/1.jpg" style="display: block; width: 325px; height: 510px;"><div class="lb-nav" style="display: block;"><a class="lb-prev" href="http://www.palemtemple.com/" style="display: none;"></a><a class="lb-next" href="http://www.palemtemple.com/" style="display: block;"></a></div><div class="lb-loader" style="display: none;"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer" style="display: block; width: 333px;"><div class="lb-data"><div class="lb-details"><span class="lb-caption" style="display: none;"></span><span class="lb-number">Image 1 of 4</span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div>-->
        <!--<button type="button" style="display:none" id="btnModel" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->


    </div>

</body>
</html>
