<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url();?>b_assets/exam_theme/images/favicon.ico">

    <title>Welcome to Brainwizz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>b_assets/exam_theme/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template 
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url();?>b_assets/exam_theme/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url();?>b_assets/exam_theme/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<link href="<?php echo base_url();?>b_assets/exam_theme/css/main.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>b_assets/exam_theme/css/TimeCircles.css" />
	<link href="<?php echo base_url();?>b_assets/exam_theme/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>b_assets/braintest_theme/custom_responsive.css" media="all"/>
 <script type="text/javascript">
 window.history.forward();
 function noBack(){
 window.history.forward();
 }
 </script>
</head>
<body class="extrapage" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
     <?php echo $body; ?>