       <!-- Custom styles for this template -->
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(17.36280, 78.50631),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
     <!-- Begin page content -->
     <div class="container">
      <div class="container-fluid" id="kolcon" style="margin-top: 50px;min-height: 510px;padding-bottom:20px;">
	  <div class="col-lg-12 col-sm-12 col-md-12 col-xs-16">
		<div class="row simplepage">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-16">
       	  <img src="<?php echo base_url();?>b_assets/images/images/contact1.jpg" class="img-responsive" alt="pawans contact"></div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-16">
        	<div id="map-canvas" style="width:500px;height:380px;"></div>
			 <div class="row">
		   <div id="lin6">
           <h2>Contact Us</h2><br>
           
           <address><span style="color:blue">BrainWiz</span><br>
5th Floor, Above Reliance Fresh,<br>
Opposite Satyam Theatre,<br>
Srinivasa Nagar, Ameerpet, Hyderabad.<br>
<abbr title="Phone">Phone : </abbr>8886233228 / 8142123938
           
           
           
<!--          <address><span style="color:blue">BrainWiz Academy</span><br>
					Opp: Satyam Theater<br>
					Srinivasa Nagar<br>
                    Ameerpet, Hyderabad 500038<br>
                    Telagana<br>
                    <br>
                    <abbr title="Phone">Contact No:</abbr> (91) 814-212 3938-->
		</address>
        <a href="#top"><span class="glyphicon glyphicon-chevron-up"></span></a>
		 </div>
         </div>
        </div>
       
	  </div>
	  
	</div>
	
	</div>
    </div>
 