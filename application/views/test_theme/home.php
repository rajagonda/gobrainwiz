
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header" style="margin-top:0;margin-bottom:0;">
		
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url();?>b_assets/images/slides/slide1.jpg" alt="...">
      <div class="carousel-caption">
	   
	  </div>
    </div>
    <div class="item">
      <img src="<?php echo base_url();?>b_assets/images/slides/slide2.jpg" alt="...">
      <div class="carousel-caption">
      </div>
    </div>
	<div class="item">
      <img src="<?php echo base_url();?>b_assets/images/slides/slide3.jpg" alt="...">
      <div class="carousel-caption">
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>

        
      </div>
	<div class="container-fluid" id="kolcon" >
		<div class="row">
		  <div class="col-xs-18 col-sm-9 col-md-8 col-lg-8">
		 
		 
                      
                      <div class="row testimonial">
          <div class="hdline"><h4 class="pull-right">Batch Timings</h4></div>
          	<table class="table table-bordered table-condensed table-hover">
            <tr>
            <th class="colblue">Course Name</th>
            <th class="colblue">Faculty</th>
            <th class="colblue">Date </th>
            <th class="colblue">Timings </th>
            </tr>
            <?php
            if($batches !=''){
           foreach($batches as $value){
           ?>
            <tr>
           <td><?php echo $value->subject_name;?></td>
            <td><?php echo $value->faculty_name;?> </td>
            <td>
            <?php //echo $value->start_date;?>
            <?php //echo date('jS M', strtotime($value->start_date));?>
                <?php echo date('jS M', strtotime($value->start_date));?>
            </td>
            <td><?php echo $value->from;?> - <?php echo $value->to;?> </td>
            </tr>
            <?php }} ?>
                       
            </table>
          
          
          </div>
                      
          
			
                      
                      
                      
                      
                      <div class="row testimonial">
			<div class="hdline"><h4 class="pull-right">Achiever</h4></div>
			<div id="testimonal">
				<ul>
                                <?php 
                                if($toppers!=''){
                                    $i =0;
                                   
                                    foreach($toppers as $value){
                                      ++$i;
                                     if($i == 1){
                                      ?>
                                    <li>
                                    <div class="col-xs-18 col-sm-6 col-md-6 col-lg-6">
                                    <?php if($value->voice_image !=''){   ?>
                                    <img src="<?php echo base_url();?>upload/topper/<?php echo $value->voice_image;?>" height="90" width="75" class="pull-right img-rounded" alt=""/>
                                    <?php }else { ?>
                                    <img src="<?php echo base_url();?>upload/topper/images.jpg" class="pull-right img-rounded" alt=""/>
                                     <?php  } ?>
                                    <p class="text11"><?php echo $value->voice_description;?></p>
                                    <p class="text12"> - <?php echo $value->student_name;?></p>
                                    <p class="text12"> (<?php echo $value->company;?>)</p>
                                    </div>
                                  <?php
                                    }else {
                                  ?>
                                    <div class="col-xs-18 col-sm-6 col-md-6 col-lg-6">
                                  <?php if($value->voice_image !=''){   ?>
                                    <img src="<?php echo base_url();?>upload/topper/<?php echo $value->voice_image;?>" height="90" width="75" class="pull-right img-rounded" alt=""/>
                                    <?php }else { ?>
                                    <img src="<?php echo base_url();?>upload/topper/images.jpg" class="pull-right img-rounded" alt=""/>
                                     <?php  } ?>
                                   <p class="text11"><?php echo $value->voice_description;?></p>
                                   <p class="text12"> - <?php echo $value->student_name;?></p>
                                   <p class="text12"> (<?php echo $value->company;?>)</p>
                                   </div>   
                                  <?php
                                    $i=0;}
                                   ?>
                                    <?php
                                    
                                    
                                } } ?>
				
				</ul>
				</div>
			</div>
			
			
		  </div>
		  <div class="col-xs-18 col-sm-3 col-md-4 col-lg-4">
			<aside class="article">
			
			<h3 class="articlehead"><span class="glyphicon glyphicon-thumbs-up"></span> Facebook</h3>
			<article class="fb-like-boxp">
			<div class="fb-like-box" data-href="https://www.facebook.com/pages/BrainWizz/305578692928665" data-height="400" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			
			</article>
			
			
		  
		</aside>
      </div>
      </div>
		  </div>
    </div>
   <script>
	$(document).ready(function() {
		$('.fb-like-box').attr('data-width', $('.fb-like-box').width());
		
	});
	</script>