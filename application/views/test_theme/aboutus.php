<!-- Begin page content -->

<div class="container">
  <div class="container-fluid" id="kolcon" style="min-height:560px;">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-16">
      <div class="row simplepage">
        <h2>About Us</h2>
        <div class="col-sm-6" style="margin-bottom:10px;">
        <p>Why CRT at BrainWiz?</p>
        <ul>
          <li>Offers total CRT and comprehensive training in <strong>Logical Reasoning, Quantitative Aptitude, Verbal sections</strong>, which are needed for almost every competitive exams.</li>
          <li><strong>Pavan Jaiswal</strong> has trained thousands of students only on Campus Aptitude.</li>
          <li>You can learn aptitude of different competitive exams under one roof.</li>
          <li>You can attend parallel session for Quick completion of the course.</li>
          <li>Individual doubt solving.</li>
          <li>Almost all students get selection in various IT companies in the campus</li>
          <li>Covers almost all companies written test questions.</li>
        </ul>
        </div>
        <div class="col-sm-6" style="margin-bottom:10px;">
   	    	<img src="<?php echo base_url();?>b_assets/images/imageSk.png" class="img-responsive" alt="" />
        </div>
        <div style="border-top:5px solid #F2F9FD;border-bottom:5px solid #F2F9FD; float:left;width:100%">
          <h2>Course Content</h2>
          <div class="col-sm-4">
            <p><strong style="color:#1A87D7">Logical Reasoning</strong></p>
            <ol>
              <li>Number Series</li>
              <li>Letter Series</li>
              <li>Coding &amp; Decoding</li>
              <li>Blood Relations</li>
              <li>Cubes &amp; Dices</li>
              <li>Deductions &amp; Syllogisms</li>
              <li>Logical Connectives</li>
              <li>Analytical Puzzles</li>
              <li>Seating Arrangement</li>
              <li>Direction Sense</li>
              <li>Statement &amp; Conclusions</li>
              <li>Time Sequencing &amp; Ranking</li>
              <li>Non &ndash; Verbal Reasoning</li>
            </ol>
          </div>
          <div class="col-sm-4">
            <p><strong style="color:#1A87D7">Arithmetic</strong></p>
            <ol>
              <li>Percentages</li>
              <li>Profit &amp; Loss</li>
              <li>Ages</li>
              <li>Ratio &amp; Proportions</li>
              <li>Partnership</li>
              <li>Chain Rule</li>
              <li>Time &amp; Work</li>
              <li>Time, Speed and Distance</li>
              <li>Problems on Trains</li>
              <li>Averages &amp; Mixtures</li>
              <li>Data Interpretation</li>
              <li>Data Sufficiency</li>
              <li>Discounts</li>
              <li>Number system</li>
              <li>Permutation &amp; Combination</li>
              <li>Probability</li>
            </ol>
          </div>
          <div class="col-sm-4">
            <p><strong style="color:#1A87D7">Verbal Ability</strong></p>
            <ol>
              <li>Spotting Errors</li>
              <li>Reading Comprehension</li>
              <li>Sentence Correction</li>
              <li>Sentence Arrangement</li>
              <li>Sentence Improvement</li>
              <li>Vocabulary</li>
              <li>Grammar</li>
              <li>Para Jumbles</li>
            </ol>
          </div>
        </div>
        <div class="col-sm-12" style="margin-top:10px;">
          <h5>Extra Focus for E-Litmus preparation</h5>
          <p><strong>Encrypting Algorithms</strong></p>
          <p><strong>Games</strong></p>
          <p><strong>Binary Logic</strong></p>
          <p><strong>Puzzles</strong></p>
          <p><strong>Numbers</strong></p>
          <p><strong>Permutation &amp; Combination</strong></p>
          <p><strong>Probability</strong></p>
          <p><strong>Geometry</strong></p>
          <p><strong>Time, Speed &amp; Distance</strong></p>
          <p></p>
          <ul>
            <li>Resume Building</li>
            <li>Interview Handling Skills</li>
            <li>Group Discussions</li>
            <li>Personality Development</li>
          </ul>
          <p><strong>Features of Classroom Study Course:</strong></p>
          <p><strong>Duration</strong>: 45 to 60 days (approx. 120 hours)</p>
          <p><strong>Timings</strong>: Classes will be conducted for 2 hours, classes might be extended as per the requirement.</p>
          <p><strong>Rapid Completion</strong>: You can attend multiple batches for the rapid completion of the course.</p>
          <p>For more details regarding, new batches &amp; fee Structure Call 8886233228 or</p>
          <p>visit: <a href="http://www.brainwizz.in/">www.brainwizz.in</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
