<html lang="en">
<head>
  <title>Forgot Password</title>
  <meta charset="utf-8">
</head>
<body style="font:Arial,Helvetica,sans-serif;color:#666666;">
    <table style="background-color:#F2F2F2;width:700px;margin:0 auto;">
        <tbody>
            <tr>
                <td>
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <ul style="width:95%;display:block;margin-bottom:5px;padding-bottom:5px;padding-top:20px;">
                                    <li style="display:inline-block;width:68%;">
                                        <img src="<?php echo assets_url();?>images/LOGO.png">
                                    </li>
                                    <li style="display:inline-block;width:10%;">
                                        <!-- <img src="images/logo/a3.png"> -->
                                    </li>
                                </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table style="width:600px;margin:0 auto;background-color:#fff;border-radius:5px; color:#333">
                        <tbody>
                            <tr>
                                <td style="padding-left:30px;">
                                    <p>Hi there!</p>
                                    <p>Thanks for choosing Go Brainwiz!</p>
                                    <p>Get started Brainwiz Service Get on simple and incredible experience.</p>
                                    <p>
                                     <h2>User Login details</h2>
                                     User Id : <?php echo $user_email; ?>
                                     User Pswd: <?php echo $password; ?>
                                    </p>                                    
                                    <p>Should you need any further assistance, contact us at<a href="#" style="color:#ED4146;text-decoration:none">  gobrainwiz@gmail.com</a></p>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>