<?php
$page = $this->uri->segment(1);
?>
<style>
.fa-google-plus {
    background: #dd4b39!important;
    color: white!important;
}
.input-lg {
    height: 39px;
}
.btn-group-lg>.btn, .btn-lg{
	    padding: 5px 16px;
}
.btn-warning {
height: 39px;}
</style>
<header class="home">
  <div class="top-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 col-md-5 col-xs-5"> 
		<a href="<?php echo base_url();?>pages/home" title=""><img src="<?php echo assets_url();?>/images/img/logo-primary.png" class="img-responsive" alt=""/></a>
		</div>
        <div class="col-sm-7 col-md-7 col-xs-7">
          <ul class="contact-info pull-right">
            <li><i class="fa fa-phone"></i>
				<p><span><a href="tel:+91-88862 33228">88862 33228</a></span><br>
                <!-- Monday-Friday, 8am - 8pm -->
                <span class="contact-sub">Feel Free to Call Us</span></p>
            </li>
            <li style="margin-top:-4px;"><i class="fa fa-envelope"></i>
				<p><a href="mailto:gobrainwiz@gmail.com">gobrainwiz@gmail.com</a><br>
                <span class="contact-sub">Write us a mail anytime!</span><br>
                <span class="contact-sub">Feel Free to mail us</span></p>
            </li>
            <!--li class="quote1 button"><a href="https://www.google.co.in/search?q=BrainWiz,+3rd+Floor,+Beside+Reliance+Fresh,+Near+Satyam+Theatre,,+Srinivasa+Nagar,+Ameerpet,+Hyderabad,+Telangana+500016&amp;ludocid=906366713197802333&amp;hl=en&amp;gl=IN&amp;sa=X&amp;ved=0ahUKEwj3_LPZy7_RAhWMuY8KHXaIDeIQ6tkBCCUoADAH#fpstate=lie&amp;lrd=0x3bcb90c62074a435:0xc940fb93864b35d,3" target="_blank">Give Review Here</a></li--> 
          </ul>
        </div>
      </div>
    </div>
    <!-- Navigation -->
 <div class="wow fadeInDown navigation" data-offset-top="197" data-spy="affix" > 
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <nav class="navbar navbar-default" >
              <div class="row"> 
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <p class="hidden-sm hidden-md hidden-lg m-head">Menu</a><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                  </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  
				  <ul class="nav navbar-nav">
                    <li class="<?php if($page == "") echo "active";?>"><a href="<?php echo base_url();?>">Home</a></li>
					<li class="<?php if($page =="Aboutus.html") echo "active";?>"><a href="<?php echo base_url();?>Aboutus.html">About Us</a></li>
					<li class="<?php if($page =="Amcat.html") echo "active";?>"><a href="<?php echo base_url();?>Amcat.html">Amcat</a></li>
					<li class="<?php if($page =="Elitmus.html") echo "active";?>"><a href="<?php echo base_url();?>pages/elitmus">Elitmus</a></li>					
                    <li class="<?php if($page =="videos") echo "active";?>"><a href="<?php echo base_url();?>videos">Videos</a></li>
                    <li class="<?php if($page =="pages") echo "active";?>"><a href="<?php echo base_url();?>pages/shareUs">Share your success</a></li>
					<?php if($this->session->userdata('user_login') == '1'){ ?>
					<li class="<?php if($page =="DownloadPapers.html") echo "active";?>"><a href="<?php echo base_url();?>download">Download Papers</a></li>
					<?php }else{?>
					<li class="<?php if($page =="DownloadPapers.html") echo "active";?>"><a href="<?php echo base_url(); ?>users/login">Download Papers</a></li>
					<?php }?>
					<li class="<?php if($page =="Contactus.html") echo "active";?>"><a href="<?php echo base_url();?>Contactus.html">Contact Us</a></li>
					<?php if($this->session->userdata('user_login') == '1'){ ?>
                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Account</a>
					<ul class="dropdown-menu" style="display: none;">
					<li><a href="<?php echo base_url();?>download/myaccount">My Account</a></li>
				    <li><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
					</ul>					
					</li>
					
                    <?php } else { ?>
					<li class=""><a href="<?php echo base_url(); ?>users/login">Login</a></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </nav>
          </div>         
        </div>
      </div>
    </div>
  </div>  
</header>  
<!-- header baner-->
<script>
function validateForm(){
//alert("hihi");
  var username,mobile,email,message;

  if( document.enquiry_form.username.value == "" ) {
      document.enquiry_form.username.className = 'form-control errorClass';
      return false;
  }else {
      document.enquiry_form.username.className = 'form-control';
      username = document.enquiry_form.username.value;

  }

  //check email validation
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if( document.enquiry_form.email.value == "" ) {
      document.enquiry_form.email.className = 'form-control errorClass';
      return false;
  }else if(!regex.test(document.enquiry_form.email.value)){
      document.enquiry_form.email.className = 'form-control errorClass';
      return false;
  }else {
      document.enquiry_form.email.className = 'form-control';
       email = document.enquiry_form.email.value; 

  }

    //check mobile validation
  //var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  var regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  

  if( document.enquiry_form.mobile.value == "" ) {
      document.enquiry_form.mobile.className = 'form-control errorClass';
      return false;
  }else if(!regex.test(document.enquiry_form.mobile.value)){
      document.enquiry_form.mobile.className = 'form-control errorClass';
      return false;
  }else {
      document.enquiry_form.mobile.className = 'form-control';
       mobile = document.enquiry_form.mobile.value; 

  }

  

  if( document.enquiry_form.message.value == "" ) {
      document.enquiry_form.message.className = 'form-control errorClass';
      return false;
  }else {
      document.enquiry_form.message.className = 'form-control';
       message = document.enquiry_form.message.value; 
  }

  var dataString = 'username='+ username +'&ajax=1&mobile='+mobile+'&email='+email+'&message='+message;

  $.ajax({
      url:'<?php echo base_url();?>pages/sendenquiry',
      type: 'POST',
      datatype:'application/json',
      data: dataString,
      success:function(response){  
        if(response.id == 1){          
            document.getElementById("failuremsg").style.display = "none";
            document.getElementById("successmsg").style.display = "block";
            document.getElementById("successmsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("successmsg").style.display = "none";
            }, 10000);          
            $('#enquiry_form').trigger("reset");
        }else   if(response.id == 0){          
            document.getElementById("successmsg").style.display = "none";
            document.getElementById("failuremsg").style.display = "block";
            document.getElementById("failuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("failuremsg").style.display = "none";
            }, 10000);          
            
        }else {          
            document.getElementById("successmsg").style.display = "none";
            document.getElementById("failuremsg").style.display = "block";
            document.getElementById("failuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("failuremsg").style.display = "none";
            }, 10000);          
            
        }
               
      }
  });




  return false;
}
        $(document).ready(function() {
           
            
        });
                
                
 function isNumberKey(evt) {
              var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57)) {
             return false;
         }
 
         return true;
}               

    </script>