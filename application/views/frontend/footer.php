<footer>
        <!-- top footer-->
        <div class="container">
            <!-- row -->
            <div class="row pb-3">
                <!-- col -->
                <div class="col-sm-4">                   
                    <p class="py-1 pr-3">BRAINWIZ is one of the leading CRT Institute in South India. It was established
                        in 2015 in Hyderabad to impart quality training for CRT (Campus Recruitment Training) & various
                        competitive Examinations.</p>

                    <a href="<?php echo base_url();?>Aboutus.html">Read More..</a>
                    <div class="social-nav">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="https://www.facebook.com/BrainWizTraining/" target="_blank" class="nav-link">
                                    <span class="icon-facebook icomoon"></span></a>
                            </li>
                            <li class="nav-item">
                                <a href="https://twitter.com/gobrainwiz" target="_blank" class="nav-link">
                                    <span class="icon-twitter icomoon"></span></a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.youtube.com/channel/UCwL2a5WEa-YGiP4OIstXdqQ" target="_blank" class="nav-link">
                                    <span class="icon-youtube icomoon"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-2 col-6">
                    <h4 class="footer-title">Brainwiz</h4>
                    <ul class="footer-nav">
                         <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="<?php echo base_url();?>Aboutus.html">About</a></li>
                        <li><a href="<?php echo base_url('blog');?>">Blog</a></li>
                        <li><a href="<?php echo base_url();?>Contactus.html">Contact</a></li>
                        <li><a href="<?php echo base_url();?>Students.html">Success Stories</a></li>
                        <li><a href="<?php echo base_url();?>pages/terms">Terms & Conditions</a></li>
                        <li><a href="<?php echo base_url();?>pages/privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url();?>FAQ.html">Faq</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-3 col-6">
                    <h4 class="footer-title">Videos</h4>
                    <ul class="footer-nav">
                        <li><a href="<?php echo base_url();?>videos/homevideos/3">Time Speed and Distance</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/4">Time & work</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/10">Number System</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/12">Crypthrithmetic</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/15">Mensuration</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/16">Cubes</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/17">Percentage</a></li>
                        <li><a href="<?php echo base_url();?>videos/homevideos/18">Data Suffiency</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-sm-3">
                    <h4 class="footer-title">Reach us</h4>
                    <table>
                        <tr>
                            <td><span class="icon-pin icomoon"></span></td>
                            <td>
                                <p>BRAINWIZ ,Inc.b
                                    # 3rd Floor, Beside Reliance Fresh , Near Satyam Theatre , Srinivasa Nagar,
                                    Ameerpet, Hyderabad.</p>
                            </td>
                        </tr>                                      
                    </table>

                    <div class="interested-course text-center pt-4">
                        <h5 class="h6 pb-3">Interested in Joining our Course?</h5>
						<?php
							$enroll_url ='';
							if($this->uri->uri_string() == '')
								$enroll_url ='#course-form';
							else
								$enroll_url = base_url()."".'Contactus.html';
						?>
						<a href="<?php echo $enroll_url;?>" class="orangebtn">Enroll Now</a>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ top footer -->
        <!-- bottom footer -->
        <div class="bottom-footer text-center">
            <p>© Copyright 2017 Brain Wiz | All Rights Reserved.</p>
        </div>
        <!--/ bottom footer -->
        <a id="movetop" class="movetop" href="javascript:void(0)"><span class="icon-arrow-up icomoon"></span></a>
        
    </footer>
 <!-- script files -->
    <script src="<?php echo assets_url();?>js/popper.min.js"></script>
    <script src="<?php echo assets_url();?>js/bootstrap.min.js"></script>  
    <script src="<?php echo assets_url();?>js/bootnavbar.js"></script>
    <script>
        $(function () {
            $('#main_navbar').bootnavbar();
        })
    </script>    
    <script src="<?php echo assets_url();?>js/slick.js"></script>
    <script src="<?php echo assets_url();?>js/easyResponsiveTabs.js"></script>
    <script src="<?php echo assets_url();?>js/accordian.js"></script>
    <script src="<?php echo assets_url();?>js/custom.js"></script>
   <?php if (!$this->uri->segment(1)) {?>
  <!-- mobile call and message -->
    <div class="call-mobile d-flex justify-content-around d-sm-none d-block">
        <a href="tel:+91 81421 23938"><span class="icon-call-answer icomoon"></span> Call us </a>
        <a href="javascript:void(0)"><span class="icon-comments icomoon"></span> Live Chat </a>
    </div>
   <?php }?>
    <!--/ mobile call and message -->

    <!-- weekly schedule pop-->    

    <!--/ weekly schedule pop-->

    <!-- on submit form success message  -->
<div id="modal-confirm" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
                    <img src="<?php echo assets_url();?>img/checkmark.gif" alt="">
				</div>				
				<h4 class="modal-title text-center">Brainwiz</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center">Thank you for choosing BRAINWIZ. Our team will contact you soon.</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>     

    <!--/ on submit form success message -->
<script type="text/javascript">
    $(document).ready(function(){
        $("#enrollbtn").click(function(){
            $("html, body").animate({
                scrollTop:$("#course-form").offset().top},'slow');
        });
    });
</script>
   

