<?php
$page = $this->uri->segment(1);
$page1 = $this->uri->segment(2);
?>
<script src="<?php echo assets_url();?>js/jquery-3.2.1.min.js"></script>
<script src="<?php echo assets_url();?>js/jquery.validate.min.js"></script>
<script src="<?php echo assets_url();?>js/scripts.js"></script>
<header class="fixed-top">
    <!-- top header -->
    <div class="top-header">
        <!-- container fluid -->
        <div class="container-fluid">
            <div class="row">
                <!-- col 6-->
                <div class="col-sm-6 col-4">                       
                    <ul class="nav ltnav">
                        <li class="nav-item d-none d-sm-block">
                            <a href="tel:+91 81421 23938" class="nav-link"><span class="icon-telephone icomoon"></span>+91 81421 23938</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url();?>" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a href="<?php echo base_url();?>Contactus.html" class="nav-link">Contact</a>
                        </li>
                        <li class="nav-item d-none d-sm-block">
                            <a href="<?php echo base_url('blog');?>" class="nav-link">Blog</a>
                        </li>                           
                    </ul>
                </div>
                <!--/ col 6--> 

                <!-- col 6-->
                <div class="col-sm-6 text-right col-8">
                    <ul class="float-right nav">
						<?php if($this->session->userdata('user_login') == '1'){ ?>
						<li class="nav-item dropdown">
							<a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi, <?php echo $this->session->userdata('examusername');?></a>
							<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
								<a class="dropdown-item" href="<?php echo base_url(); ?>onlinetest/dashboard">Dashboard</a>
								<a class="dropdown-item" href="<?php echo base_url(); ?>users/manageProfile">Edit Profile</a>
								<a class="dropdown-item" href="<?php echo base_url(); ?>users/logout">Logout</a>
							</div>
						</li>
						<?php } else { ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url();?>onlinetest/login" class="nav-link"><span class="icon-user-circle"></span> Login</a>
                        </li>
						<?php } ?>
                    </ul>
                </div>
                <!--/ col 6-->
            </div>
        </div>
        <!--/ container fludi -->
    </div>
    <!--/ top header -->

    <!-- nav -->
    <nav class="navbar navbar-expand-lg navbar-light" id="main_navbar">
        <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo assets_url();?>img/logo.svg" alt=""></a>
		
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon-bars icomoon"></i>
        </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto leftnav">
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url();?>Aboutus.html">About us </a>
                    </li>
					
					<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#"   id="navbarDropdownone"  role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses
                        </a>
                        <div class="dropdown-menu"  aria-labelledby="navbarDropdownone" >
                            <a class="dropdown-item" href="<?php echo base_url();?>pages/crt">CRT</a>
                            <a class="dropdown-item" href="<?php echo base_url();?>pages/amcat">AMCAT</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>pages/elitmus">E-Litmus</a>
                            <a class="dropdown-item" href="<?php echo base_url('elitmus/companypapers'); ?>">E-Litmus    questions</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>pages/cocubes">CoCubes</a>
                        </div>
                    </li>
					
				    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>Batches.html">Batches</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('videos/homevideos');?>">Tutorials</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('onlinetest/dashboard')?>">Test Series</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url();?>Students.html">Students</a>
                    </li>
					<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"  role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Examinations
                        </a>
                        <div class="dropdown-menu" >
                            <a class="dropdown-item" href="<?php echo base_url('pages/gate');?>">GATE</a>
                            <a class="dropdown-item" href="<?php echo base_url('pages/clat');?>">CLAT</a>
                            <a class="dropdown-item" href="<?php echo base_url('pages/ccat');?>">C-CAT</a>
                            <a class="dropdown-item" href="<?php echo base_url('pages/afcat');?>">AFCAT</a>
                            <a class="dropdown-item" href="<?php echo base_url('pages/ssccgl');?>">SSC-CGL</a>
                        </div>
                    </li> 
					                                     
                </ul>
                
            </div>
        </nav>
        <!--/ nav -->
    </header>
 <!-- header baner-->