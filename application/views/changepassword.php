      <!-- Row start -->
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <?php if (isset($old_password_error) || isset($password_change_success)) {  ?>
                                    <?php if (isset($old_password_error)) { ?>
                                    <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <b>Alert!</b> 
                                    <?php echo $old_password_error; ?>
                                    </div>
                                    <?php }else { ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> 
                                    <?php echo $password_change_success; ?>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>  
          <div class="panel panel-default">
            <div class="panel-heading clearfix">
            
              <h3 class="panel-title">Change Password Form</h3>
            </div>
<?php
$url = current_url();
?>

            <div class="panel-body">
              <form role="form" name="password_form" id="password_form" method="post" action="<?php echo $url;?>" >
               
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control"  name="old_password" id="old_password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control"  name="new_password" id="new_password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control"  name="confirm_password" id="confirm_password" placeholder="Password">
                </div>
                
                <input type="submit" class="btn btn-success" value="Submit">
              </form>
            </div>
          </div>
        </div>
       
      </div>
      <!-- Row end -->
      <?php echo $script; ?>

 