<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title;?></title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Plugins -->
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/owl.theme.default.min.css" rel="stylesheet">    
    <link href="<?php echo base_url();?>assets/css/style.red.flat.min.css" rel="stylesheet" id="theme">
    <?php echo $css;?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script  src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script   src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-105235512-1', 'auto');
  ga('send', 'pageview');

</script> 

<!-- <meta name="google-site-verification" content="oxajl4_K_8GdPDT5dbEnjI8Il3ilOYd4UBaM4pDajT8"> -->
  </head>
  <body class="fade-down">

    <!-- Top Header -->
    <div class="top-header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="list-inline pull-left hidden-sm hidden-xs">
              <!-- Promo bar -->
              <li><a href="#"><i class="fa fa-bullhorn"></i> Free 2 Business Day Shipping on all orders!</a></li>
              <li><a href="#"><i class="fa fa-android"></i> Download App</a></li>
            </ul>
            <ul class="list-inline pull-right">
              <li class="hidden-xs"><a href="#"><i class="fa fa-question-circle"></i> Help</a></li>
              <li class="hidden-xs"><a href="#"><i class="fa fa-phone"></i>+91 <?php echo $this->config->item('contact_number'); ?></a></li>
              <li>
                <select class="selectpicker" data-width="95px" data-style="btn-default">
                  <option value="INDIA" data-content="<img alt='India' src='<?php echo base_url();?>assets/images/in.png'> India">India</option>
                  
                </select>
              </li>
              <li>
                <select class="selectpicker" data-width="70px" data-style="btn-default">
                  <option value="inr">₹ INR</option>                  
                </select>
              </li>
              <li class="hidden-xs"><a href="#"><i class="fa fa-align-left"></i> Track Order</a></li>
              <li>
                <div class="dropdown">
                  <button class="btn dropdown-toggle" type="button" id="dropdownLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-user"></i> Login <span class="caret"></span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-login" aria-labelledby="dropdownLogin">
                    <form>
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" placeholder="Username">
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password">
                      </div>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"><span> Remember me</span>
                        </label>
                      </div>
                      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-long-arrow-right"></i> Login</button>
                      <a class="btn btn-default btn-sm pull-right" href="register.html" role="button">I Want to Register</a>
                    </form>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Top Header -->

    <!-- Middle Header -->
    <div class="middle-header">
      <div class="container">
        <div class="row">
          <div class="col-md-3 logo">
            <a href="<?php echo base_url();?>"><img alt="Logo" src="<?php echo base_url();?>upload/logos/<?php echo $this->config->item('site_logo'); ?>" class="img-responsive" data-text-logo="Botiga Online Shop" /></a>
          </div>
          <div class="col-sm-8 col-md-6 search-box m-t-2">
            <div class="input-group">
              <input type="text" class="form-control search-input" aria-label="Search here..." placeholder="Search here...">
              <div class="input-group-btn">
                <select class="selectpicker hidden-xs" data-width="150px">
                  <option value="0">All Categories</option>
                  <option value="1">Dresses</option>
                  <option value="2">Tops</option>
                  <option value="3">Bottoms</option>
                  <option value="4">Jackets / Coats</option>
                  <option value="5">Sweaters</option>
                  <option value="6">Gym Wear</option>
                  <option value="7">Others</option>
                </select>
                <button type="button" class="btn btn-default btn-search"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="col-sm-4 col-md-3 cart-btn hidden-xs m-t-2">
            <a href="" class="btn btn-theme dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-shopping-cart"></i> 0 <span class="caret"></span></a>
            
            <a href="#" class="btn btn-theme" data-toggle="tooltip" title="Wishlist" data-placement="bottom"><i class="fa fa-heart"></i>0</a>
          </div>
        </div>
      </div>
    </div>
    <!-- End Middle Header -->

    <!-- Navigation Bar -->
    <nav class="navbar navbar-default shadow-navbar" role="navigation">
      <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="<?php echo base_url();?>" class="btn btn-default btn-cart-xs visible-xs pull-right">
              <i class="fa fa-shopping-cart"></i> Cart : 0
            </a>
            <a href="<?php echo base_url();?>" class="btn btn-default btn-cart-xs visible-xs pull-right">
              <i class="fa fa-heart"></i> Wishlist : 0
            </a>
          </div>
          <div class="collapse navbar-collapse" id="navbar-ex1-collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
              <?php echo create_menu();?>


             

            </ul>
            <ul class="nav navbar-nav navbar-right navbar-feature visible-lg">
              <li><a><i class="fa fa-truck"></i> Free Shipping</a></li>
              <li><a><i class="fa fa-money"></i> Cash on Delivery</a></li>
              <li><a><i class="fa fa-lock"></i> Secure Payment</a></li>
            </ul>
          </div>
      </div>
    </nav>
    <!-- End Navigation Bar -->
<?php
echo $body;
?>
 <!-- Footer -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>About Us</span></div>
            <ul>
              <li>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et doloremmagna aliqua. Ut enim ad minim... <a href="#">Read More</a>
              </li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Information</span></div>
            <ul>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">FAQ</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Policy Privacy</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Terms and Conditions</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Shipping Methods</a></li>
            </ul>
          </div>
          <div class="clearfix visible-sm-block"></div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Categories</span></div>
            <ul>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Cras justo odio</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Dapibus ac facilisis in</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Morbi leo risus</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Porta ac consectetur ac</a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Newsletter</span></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, soluta, tempora, ipsa voluptatibus porro vel laboriosam</p>
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Email Address">
              <span class="input-group-btn">
                <button class="btn btn-default subscribe-button" type="button">Subscribe</button>
              </span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Our Store</span></div>
            <ul class="footer-icon">
              <li><span><i class="fa fa-map-marker"></i></span><?php echo $this->config->item('Address'); ?>,<?php echo $this->config->item('City'); ?>,<?php echo $this->config->item('State'); ?>,<?php echo $this->config->item('Zip_Code'); ?></li>
              <li><span><i class="fa fa-phone"></i></span>+91 <?php echo $this->config->item('contact_number'); ?></li>
              <li><span><i class="fa fa-envelope"></i></span> <a href="mailto:<?php echo $this->config->item('Contact_Mail'); ?>"><?php echo $this->config->item('Contact_Mail'); ?></a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Follow Us</span></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum</p>
            <ul class="follow-us">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul>
          </div>
          <div class="clearfix visible-sm-block"></div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>Payment Method</span></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, soluta, tempora, ipsa voluptatibus porro vel laboriosam</p>
            <img src="<?php echo base_url();?>assets/images/payment-1.png" alt="Payment-1">
            <img src="<?php echo base_url();?>assets/images/payment-2.png" alt="Payment-2">
            <img src="<?php echo base_url();?>assets/images/payment-3.png" alt="Payment-3">
            <img src="<?php echo base_url();?>assets/images/payment-4.png" alt="Payment-4">
            <img src="<?php echo base_url();?>assets/images/payment-5.png" alt="Payment-5">
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="title-footer"><span>My Account</span></div>
            <ul>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Orders</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Vouchers</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Points</a></li>
              <li><i class="fa fa-angle-double-right"></i> <a href="#">Login</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="text-center copyright">
        Copyright &copy; 2017 <?php echo $this->config->item('site_name'); ?> All right reserved
      </div>
    </div>
    <!-- End Footer -->

    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop:0},500); return false">
      <i class="fa fa-angle-double-up"></i>
    </a>
<!-- 
    <div class="chooser chooser-hide">
      <div class="chooser-toggle"><button class="btn btn-warning" type="button"><i class="fa fa-paint-brush bigger-130"></i></button></div>
      <div class="chooser-content">
        <label>Color</label>
        <select name="color-chooser" id="color-chooser" class="form-control input-sm selectpicker">
          <option value="indigo">Indigo</option>
          <option value="red">Red</option>
          <option value="teal">Teal</option>
          <option value="brown">Brown</option>
        </select>
        <label class="m-t-1">Style</label>
        <select name="style-chooser" id="style-chooser" class="form-control input-sm selectpicker">
          <option value="flat">Flat</option>
          <option value="rounded">Rounded</option>
        </select>
      </div>
    </div> -->


    <!-- Plugins -->
    <script   src="<?php echo base_url();?>assets/js/bootstrap-select.min.js"></script>    
    <script  src="<?php echo base_url();?>assets/js/bootstrap3-typeahead.min.js"></script>  
    <script  src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
    <script  src="<?php echo base_url();?>assets/js/bootstrap-toolkit.min.js"></script>
    <script   src="<?php echo base_url();?>assets/js/mimity.js"></script>    
     <?php echo $js;?>
  </body>
</html>