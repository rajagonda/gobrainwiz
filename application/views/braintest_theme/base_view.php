<!DOCTYPE HTML><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">    <meta name="viewport" content="width=device-width, initial-scale=1"><title></title><!-- Latest compiled and minified CSS --><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous" /><!-- Optional theme --><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous" /><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="all"/><link rel="stylesheet" type="text/css" href="<?php echo base_url();?>b_assets/braintest_theme/custom.css" media="all"/>
    <!-- custom responsive css -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>b_assets/braintest_theme/custom_responsive.css" media="all"/>
    
     <!--custom responsive css end-->

  <link rel="icon" type="<?php echo assets_url();?>image/png" href="<?php echo assets_url();?>images/Untitled.png" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   </script><script src="<?php echo base_url();?>assets/js/po.js" type="text/javascript"></script>
    <title>Welcome to Brainwiz</title>

   
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
 <script type="text/javascript">
 window.history.forward();
 function noBack(){
 window.history.forward();
 }
 </script>
 
<!-- <meta name="google-site-verification" content="oxajl4_K_8GdPDT5dbEnjI8Il3ilOYd4UBaM4pDajT8"> -->
</head>
<body class="extrapage" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
     <?php echo $body; ?>