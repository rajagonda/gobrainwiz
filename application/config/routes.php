<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages/home";
$route['home'] =  "pages/home";
$route['register'] =  "users/register";
$route['Aboutus.html'] =  "pages/about";
$route['Batches.html'] =  "pages/batches";
$route['Students.html'] =  "pages/students";
$route['Amcat.html'] =  "pages/amcat";

$route['applicationdevelopment.html'] =  "pages/services/servicepages/1";
$route['mobileappdevelopment.html'] =  "pages/services/servicepages/2";
$route['businessprocess.html'] =  "pages/services/servicepages/4";
$route['webapplication.html'] =  "pages/services/servicepages/5";
$route['qualityassurance.html'] =  "pages/services/servicepages/6";

$route['Contactus.html'] =  "pages/contactus";
$route['Elitmus.html'] =  "pages/Elitmus";
$route['ElitmusSyllabus.html'] =  "pages/ElitmusSyllabus";
$route['ExaminationPattern.html'] =  "pages/ExaminationPattern";
$route['RegistrationProcess.html'] =  "pages/RegistrationProcess";
$route['CompaniesHiring.html'] =  "pages/CompaniesHiring";
$route['FAQ.html'] =  "pages/FAQ";
$route['portfolio.html'] =  "front/pages/portfolio";
//$route['pages/services/1']
$route['pages/services/servicepages/(:num)'] = "pages/services/servicepages/$1";

$route['blog'] =  "blog/index/$1";
$route['blog/post_details/(:any)'] =  "blog/post_details/$1";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */