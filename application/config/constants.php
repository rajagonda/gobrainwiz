<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
 * Portable PHP password hashing framework
 * http://www.openwall.com/phpass/
 */
define('PHPASS_HASH_STRENGTH', 8);
define('PHPASS_HASH_PORTABLE', FALSE);

/*
* site in demo
* @ 1 demo
* @ 0 live
*/

define('demo', '0');
/*
* Local or server
* @ 1 local
* 0 0 live
*/
define('server', '0');
/*
* payu demo or liver
* @ 1 demo
* 0 0 live
*/
define('payu', '0');

define('DB_PREFIX','gk_');
if(server == '1'){	
   //Recharge payu response urls
	define('rechargeSuccess','http://localhost:8080/b2c/recharge/success');
	define('rechargeFailure','http://localhost:8080/b2c/recharge/failure');
	//Myaccount payu response urls
	define('myaccountSuccess','http://localhost:8080/b2c/myaccount/success');
	define('myaccountFailure','http://localhost:8080/b2c/myaccount/failure');
}else {
	//Recharge payu response urls
	define('rechargeSuccess','http://rechargenestham.com/recharge/success');
	define('rechargeFailure','http://rechargenestham.com/recharge/failure');
	//Myaccount payu response urls
	define('myaccountSuccess','http://rechargenestham.com/myaccount/success');
	define('myaccountFailure','http://rechargenestham.com/myaccount/failure');
}
if(payu == '1'){
	define('MERCHANT_KEY', 'gtKFFx');
	define('SALT', 'eCwWELxi');
	define('PAYU_BASE_URL', 'https://test.payu.in/_payment');
}else {
	define('MERCHANT_KEY', 'dnZnWL');
	define('SALT', 'MG5wYfFJ');
	define('PAYU_BASE_URL', 'https://secure.payu.in/_payment');
}



/* End of file constants.php */
/* Location: ./application/config/constants.php */