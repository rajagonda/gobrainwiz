<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : common_model
 * Mail id : phpguidance@gmail.com
  */
class common_model extends CI_Model {	
	/**
	* @$tablename : tablename
	* @$data : array
	* Usage : here insert single row
	*/
	public function insertSingle($tablename, $data =null){
      if($data !=null){
        $this->db->insert($tablename,$data);
		
		return $this->db->insert_id(); 
      }else {
      	return false;
      }
	}
	/*
	* @$tablename : tablename
	* @$data : multiple array's
	*  Usage : here insert multiple rows
	*/
	public function insertMany($tablename,$data = null){
	  if($data !=null){
		$this->db->insert_batch($tablename, $data);
	  }else {
	  	return false;
	  }
	}
	/*
	* @$tablename : table name
	* @$ params : array
	* usage : here udate single row with single condition or multiple where conditions
	*/
	public function updateRow($tablename, $data, $params){

	   foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
       $this->db->update($tablename, $data);
	   return $this->db->affected_rows();
	}
	/*
	* @$tablename : tablename
	* @$ params  : array
	* Usage : Here delete data from database based on param condiion
	*/
	public function deleteRow($tablename, $params){
	   foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
       $this->db->delete($tablename);
	}
	/*
	* @$tablename : tablename
	*  @params  : array
	* Usage : here we wil get result from table with conditions or with out conditions
	* Return :  object data
	*/
	public function getResult($tablename, $params = array()){
	   if(sizeof($params) >0) {
       foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
	   }
	   $query = $this->db->get($tablename);
	   if($query->num_rows() > 0) {
        return $query->result();
       }else {
        return false;
       }	
	}

	/*
	* @$tablename : tablename
	*  @params  : array
	* Usage : here we wil get result from table with conditions or with out conditions
	* Return : array data
	*/
	public function getResultArray($tablename, $params = array()){
	   if(sizeof($params) >0) {
       foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
	   }
	   $query = $this->db->get($tablename);
	   if($query->num_rows() > 0) {
        return $query->result_array();
       }else {
        return false;
       }	
	}
	/*
	* @$tablename : tablename
	*  @params  : array
	* Usage : here we wil get single row result from table with conditions or with out conditions
	* Return : array data
	*/
	public function getSingleRow($tablename, $params = array()){
	   if(sizeof($params) >0) {
       foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
	   }
	   $query = $this->db->get($tablename);
	   
	   if($query->num_rows() > 0) {
        return $query->row();
       }else {
        return false;
       }	
	}
	/*
	* @$tablename : tablename
	*  @params  : array
	* Usage : here we wil get single row result from table with conditions or with out conditions
	* Return : array data
	*/
	public function geCount($tablename, $params = array()){
	   if(sizeof($params) >0) {
       foreach ($params as $key => $value) {
        $this->db->where($key,$value);
       }
	   }
	   $query = $this->db->get($tablename);
	   if($query->num_rows() > 0) {
        return $query->num_rows();
       }else {
        return false;
       }	
	}
	/*
	* @$tablename : tablename
	*  @params  : array
	* Usage : Here we will truncate the database table
	* Return : array data
	*/
	public function truncate($tablename){
	   $this->db->truncate($tablename);       
	}
	/*
	* @$username_email : email id or name
	* Usage : check user exit or not
	* Return : array 
	*/
	public function get_by_username_email($username_email)	{		
		$this->db->where('user_name', $username_email);
		$this->db->or_where('user_email', $username_email);
		$query = $this->db->get('gk_users');
		if($query->num_rows() >0){
		 return $query->row();
	    }else {
	    	return false;
	    }
	}
    
    public function  getProfileData($user_id) {
    
           if($this->session->userdata('CRUD_AUTH')['is_admin'] != 1) {
            $sql = "SELECT * FROM  rc_users  INNER JOIN rc_user_details  ON  rc_users.user_id = rc_user_details.user_id WHERE rc_users.user_id = $user_id";
           }else {
           	$sql = "SELECT * FROM  rc_users  INNER JOIN rc_user_details  ON  rc_users.user_id = rc_user_details.user_id WHERE rc_users.group_id =1 ";
           }
    $query = $this->db->query($sql);
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }

    }
    public function get_by_email($email) {
    	$this->db->where('user_email', $email);
		$query = $this->db->get('rc_users');
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }
		
     }
	
	public function get_by_username($username) {
		$this->db->where('user_name', $username);
		$query = $this->db->get('rc_users');
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }
	}
	public function getUserGroup($user_id) {
		$sql = "SELECT * FROM rc_users WHERE user_id='$user_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }
	}

	public function getPermissions($group_id) {
	    $sql = "SELECT * FROM rc_group_permissions WHERE group_id='$group_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
         return $query->result();
        }else {
         return '';
      }	
	}
	public function checkPermission($module_id,$roleid, $permission_id) {
		//$sql = "SELECT * FROM rc_group_permissions WHERE group_id='$group_id' AND permission_id IN ($permission_id)";
		$this->db->where('module_id',$module_id);
		$this->db->where('role_id',$roleid);
		$this->db->where_in('permission_id',$permission_id);
		$query = $this->db->get(DB_PREFIX.'role_permissions');
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }	
	}	
	public function checkOldpassworsd($uid){
        $this->db->where('user_id', $uid);
        $query = $this->db->get(DB_PREFIX.'admin_users');
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }

	}
	public function updatepassword($uid, $data){
		$this->db->where('user_id', $uid);
	    $this->db->update(DB_PREFIX.'admin_users', $data);
	    return $this->db->affected_rows();
	}
	public function count_q_t($param, $tid){

	   if($param == 'q'){
        $sql = "SELECT count(q_id) as tq  FROM gk_braintestquestions WHERE test_id='$tid'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
        	return $res->row()->tq;
        }else {
        	return false;
        }
       }else {
       	$sql = "SELECT sum(time) as ttime FROM gk_test_cattime WHERE test_id='$tid'";
       	 $res = $this->db->query($sql);
        if($res->num_rows()>0){
        	return $res->row()->ttime;
        }else {
        	return false;
        }
       }
		
	}

	public function countTopicTests($tid) {
    	$this->db->where('topic_id', $tid);
		$query = $this->db->get('gk_ptests');
		if($query->num_rows() > 0) {
         return $query->num_rows();
        }else {
         return '0';
      }
		
     }

 public function countTestQuestions($testId){
     	$this->db->where('test_id', $testId);
		$query = $this->db->get('gk_ptquetions');
		if($query->num_rows() > 0) {
         return $query->num_rows();
        }else {
         return false;
      }
     }

}


/* End of file common_model.php */
/* Location: ./application/models/common_model.php */