<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| General
|--------------------------------------------------------------------------
*/

$lang['manage_settings'] = "Manage Site Settings";
$lang['status'] = 'Status';
$lang['Sno'] = "S.NO";
$lang['site_name'] = "Site Name";
$lang['contact_email'] = "Contact Email";
$lang['contact_number'] = "Contact Number";
$lang['Home_Banners'] = "Home Banners";
$lang['address'] = "Address";
$lang['City'] = 'City';
$lang['State'] = 'State';
$lang['Country'] = "Country";
$lang['Zip_Code'] = "Zip Code";
$lang['Powered_by'] = "Powered By";
$lang['Rights_Reserved'] = "Rights Reserved";
$lang['Fax'] = "Fax";
$lang['currency'] = 'Currency';
$lang['actions'] = "Actions";
$lang['contact_mails'] = "Contact Mails";

//chnage password
$lang['change_password'] = "Change Password";
$lang['old_password'] = "old Password";
$lang['new_password'] = "New Password";
$lang['confirm_password'] = "Confirm Password";
$lang['old_password_error'] = "Old password Not Match";
$lang['password_change_success'] = "Password Reset Successfully";


//login form
$lang['user_name'] = "User Name";
$lang['user_email'] = "User Email";
$lang['user_password'] = "Password";
$lang['sign_in_username_email_does_not_exist'] = "Username or Email not exist";
$lang['sign_in_combination_incorrect'] ="Inavlid Password";


/*
* Profile page
*/
$lang['manage_profile'] = "Manage Profile";
$lang['user_fname'] = "First Name";
$lang['user_lname'] = "Last Name";
$lang['user_mname'] = "Middle Name";
$lang['user_gender'] = "Gender";
$lang['user_address1'] = "Address 1";
$lang['user_address2'] = "Address 2";
$lang['user_country'] = "Country";
$lang['user_state'] = "State";
$lang['user_city'] = "City";
$lang['user_pincode'] = "Pincode";
$lang['user_image'] = "Profile Picture";
$lang['user_dob'] = "Date Of Birth";


/* End of file common_lang.php */
/* Location: ./application/language/english/common_lang.php */

