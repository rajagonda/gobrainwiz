<?php
class videos_model extends CI_Model {
    
    public function getlatesthomevideo($cid=NULL , $id=NULL) {
        if($id !='' && $cid !=NULL) {
           
          //$sql = "SELECT  * FROM gk_hvideos where id ='$id' ORDER BY id DESC";
           $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess  
                   on gk_hvcategoriess.c_id = gk_hvideos.cid WHERE gk_hvcategoriess.c_id ='$cid' 
                   AND gk_hvideos.id='$id' ORDER BY gk_hvideos.id DESC"; 
        }else if($cid !=NULL) {
          //  $sql = "SELECT  * FROM gk_hvideos  INNER JOIN ON ORDER BY id DESC";
            $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess  
                    on gk_hvcategoriess.c_id = gk_hvideos.cid 
                    WHERE gk_hvcategoriess.c_id ='$cid' ORDER BY gk_hvideos.id DESC"; 
        } else {
          //  $sql = "SELECT  * FROM gk_hvideos  INNER JOIN ON ORDER BY id DESC";
            $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess  
                    on gk_hvcategoriess.c_id = gk_hvideos.cid 
                    WHERE gk_hvcategoriess.h_cat ='1' ORDER BY gk_hvideos.id DESC"; 
        }
        
          $query = $this->db->query($sql);
         // echo $this->db->last_query();
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        } 
    }
    
    
    public function getlatesthomevideorandom() {
        
            $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess  
                    on gk_hvcategoriess.c_id = gk_hvideos.cid  ORDER BY gk_hvideos.id DESC limit 1"; 
        
        
          $query = $this->db->query($sql);
         // echo $this->db->last_query();
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        } 
    }

    public function getallcategories() {
        $sql = "SELECT  * FROM gk_hvcategoriess WHERE status='y' ";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getlatesthomervideos($cid, $id) {
       //  $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess ON 
       //     gk_hvcategoriess.c_id = gk_hvideos.cid where gk_hvideos.id !='$id' AND  gk_hvcategoriess.c_id = '$cid'";
        $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess ON 
            gk_hvcategoriess.c_id = gk_hvideos.cid where gk_hvcategoriess.c_id = '$cid'";
        
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        } 
    }
    

        public function gettopicrelatedvideo() {
        $sql = "SELECT  * FROM gk_pvidoes WHERE pv_id='$id'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     
     public function gettopicsvideo($id){
       $sql = "SELECT  * FROM gk_pvidoes WHERE topic_id='$id' AND related_video='n'";
       $query = $this->db->query($sql);
       if($query->num_rows()>0) {
       return $query->row();
       } else {
       return '';
       }
     }
     public function gettopicrelatedvideos($id) {
         $sql = "SELECT  * FROM gk_pvidoes WHERE topic_id='$id' AND related_video='y'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     
     
    public function getlikescount($id) {
        $sql = "SELECT *  FROM gk_pvidoes  WHERE  pv_id ='$id'" ;  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
    }
    public function updatelikes($data, $id) {
         $this->db->where('pv_id', $id);
        $this->db->update('gk_pvidoes', $data);
        return $this->db->affected_rows();
    }
    
     public function getlikeshomecount($id) {
        $sql = "SELECT *  FROM gk_hvideos  WHERE  id ='$id'" ;  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
    }
    public function updatehomelikes($data, $id) {
         $this->db->where('id', $id);
        $this->db->update('gk_hvideos', $data);
        return $this->db->affected_rows();
    }
    
    public function gethvideoviews($id) {
        $sql = "SELECT * FROM gk_hvideos  WHERE  id ='$id'" ;  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
    }
    public function updatehvideoViews($vid, $data){
         $this->db->where('id', $vid);
        $this->db->update('gk_hvideos', $data);
       // echo $this->db->last_query(); exit;
        return $this->db->affected_rows();
    }
    
    
     
        

    
}