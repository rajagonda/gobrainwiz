<!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header d-none d-sm-block">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Video <span class="fbold">Tutorials</span> </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active"><a>Video Tutorials</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                <!-- col left -->
                <div class="col-lg-4 leftnav-tutorial">
                    <h2 class="h6 d-flex justify-content-between">
                        <span id="dt-topic1">Detailed Topics</span>
						<span id="dt-topic2"><?php echo $homevideo->cat_name;?></span> 
                        <a class="fwhite" id="videos-show-icon" href="javascript:void(0)"><span class="icon-list-ul icomoon"></span></a>
                    </h2>

                    <ul class="nav flex-column flex-nowrap leftnav" id="video-listitems"> 
					<?php
                        if($categories !='') {
							foreach($categories as $value) {
                        ?>					
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url();?>videos/homevideos/<?php echo $value->c_id;?>"><?php echo $value->cat_name;?></a></li> 
                        <?php
							}
						}
                        ?>
                    </ul>

                </div>
                <!--/ col left -->

                <!-- col right -->
                <div class="col-lg-8 right-videos">
                    <!-- title -->
                    <div class="right-title d-flex justify-content-between">
                        <h2 class="h6"><?php echo $homevideo->cat_name;?></h2>
                       <span><?php echo count($homervideos);?> Videos</span> 
                    </div>
                    <!--/ title -->
                    <!-- row -->
                    <div class="row">
                        <!-- col video-->
						<?php  
						if($homervideos !=''){
							foreach ($homervideos as $value) {
						?>
                        <div class="col-lg-6">
                            <div class="videotestimonial video-col whitebox" data-toggle="modal" data-target="#videopop" data-url="<?php echo $value->youtube_link; ?>">
                                <h6>
                                    <a class="d-block pb-2" href="javascript:void(0)"><?php echo $value->video_desc; ?></a>
                                </h6>
                                <a href="javascript:;">
                                   <?php if($value->v_image){?>
                                    <img src="<?php echo base_url().'public/videoimages/'.$value->v_image; ?>" class="img-fluid">
                                   <?php }else{ ?>
                                     <img src="<?php echo assets_url().'images/noimage.jpg'; ?>" class="img-fluid">
                                   <?php }?>
                    
                                </a>
                                <p class="d-flex justify-content-between pt-2 pb-0 mb-0">
                                    <a class="fblue" href="javascript:void(0)">VIEW NOW</a>
                                </p>
								<span class="icon-youtube-play icomoon"></span>	
                            </div>
                        </div>
						<?php }
						}?>
                        <!--/ colvideo -->
                     
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col right -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
	<!-- Modal video -->
        <div class="modal-dialog  modal-lg" role="document">
    <div class="modal fade video-modal" id="videopop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <iframe class="modalvideo" width="100%" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        </div>
    </div>
    </div>
        </div>
    <!--/ modal video -->
		<script type="text/javascript">
		$(document).ready(function(){
        $("#videos-show-icon").click(function(){
			
            $("#video-listitems").slideToggle();
        });
    });
$('.videotestimonial').on( "click",function () {
		var url = $(this).attr('data-url');
		$('.video-modal').on('hidden.bs.modal', function (e) {
		$('.video-modal iframe').attr('src', '');
		});

		$('.video-modal').on('show.bs.modal', function (e) {
			$('.video-modal iframe').attr('src', url+'?rel=0&amp;autoplay=1');
		});
  });
   
    
    </script>