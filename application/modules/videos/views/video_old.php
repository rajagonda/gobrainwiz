
    <style>
        .navbar {
            /*336699trythis color*/
            border-radius: 0px;
            margin-bottom: 10px;
        }

        li a {
            color: #000000;
            font-weight: bold;
        }

        .navbar-toggle {
            background-color: #fff;
        }

        .icon-bar {
            background-color: #184682;
        }

        .navbar li a:hover {
            color: none;
            background-color: goldenrod;
        }

        @media only screen and (max-width: 420px) {
            .navbar {
                background-color: #184682;
                border-radius: 5px;
                color: white;
                z-index: 100;
            }

            li a {
                color: white;
            }

            .navbar-collapse {
                padding-left: 0px;
                padding-right: 0px;
            }

            div.navbar-header {
                padding-left: 0px;
                padding-right: 0px;
            }

            #logobnr {
                margin-left: 0px;
            }
        }
    </style>


    <style>
        #mouse {
            margin-left: 20px;
        }

            #mouse a {
                width: 260px;
                height: 38px;
                margin-left: 12px;
                /*border:1px #808080;*/
                /*border-top: 1px inset #808080;*/
                border-bottom: 1px solid #808080;
                padding-top: 8px;
            }

        #cat {
            width: 260px;
            margin-left: 16px;
        }
        /*#mouse a:hover{
            background-color:#184682;
        }*/
        @media only screen and (max-width: 420px) {
            #mouse a {
                width: 220px;
                height: 25px;
                padding-top: 4px;
                margin-left: -10px;
            }

            .html5gallery {
                width: 100%;
                height: 100%;
            }

            #cat {
                width: 220px;
                margin-left: 0px;
            }

            .Logicaldiv {
                color: #fff;
                font-weight: bold;
                background-color: #184682;
                font-weight: bold;
                height: 100%;
                padding: 10px 20px;
                border-top-left-radius: 8px;
                border-top-right-radius: 8px;
                margin-bottom: 5px;
                font-family: Tahoma;
                font-weight: 200;
                /*padding-left:10px;*/
                padding-right: 0px;
            }

                .Logicaldiv p {
                    color: #fff;
                    font-weight: bold;
                    background-color: #184682;
                    font-weight: bold;
                    height: 100%;
                    padding: 15px 30px;
                    border-top-left-radius: 8px;
                    border-top-right-radius: 8px;
                    margin-bottom: 5px;
                    font-family: 'Times New Roman', Times, serif;
                    font-size: 16px;
                }
        }
    </style>
    <section id="vid">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12"><h1 style="color:#ff6a00">Video section Brainwiz</h1> </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-xs-12 col-sm-12">

                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div id="cat">
                                <h4 class="Logicaldiv">categories </h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                        <div id="mouse">
                        <?php
						if($categories !='') {
						foreach($categories as $value) {
						?>
						<div><a class="hvr-sweep-to-bottom" href="<?php echo base_url();?>videos/homevideos/<?php echo $value->c_id;?>"><?php echo $value->cat_name;?></a></div>
                        <?php
                        }}
                        ?>
                        <!--<a href="#" class="hvr-sweep-to-bottom">Shutter Out Horizontal</a>-->
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        $('#btnframe').click(function () {
                            // alert();
                        });

                    });
                </script>
                <style>
                    .related-img ul li {
                        display: inline;
                        overflow: scroll;
                        width: inherit;
                        height: inherit;
                    }

                        .related-img ul li img {
                            -webkit-box-shadow: 0px 2px 0px 0px rgb(128,128,128);
                        }

                    .related {
                        font-family: 'Times New Roman';
                        font-weight: bolder;
                        font-size: 16px;
                        font-style: initial;
                        color: red;
                        text-align: -webkit-match-parent;
                    }

                    .basic_vid {
                        background-color: #fff;
                        font-family: 'Times New Roman';
                        font-weight: bold;
                        font-size: 10px;
                        font-style: initial;
                        color: black;
                        border: 1px solid #ccc;
                        width: 30%;
                        height: auto;
                        cursor: pointer;
                        margin: 1%;
                        bottom: 4px;
                        box-shadow: 0 5px 7px #999;
                        left: 8px;
                        z-index: 9;
                        /*-webkit-box-shadow: 0px 2px 0px 1px rgb(128,128,128);*/
                        /*height: 120px;*/
                    }

                    .vid12 {
                        height: 200px;
                        width: 100%;
                        padding: 5px 2px 5px;
                    }

                    #video_player {
                        display: table;
                        line-height: 0;
                        font-size: 0;
                        background: #000;
                        max-width: 1000px;
                        margin: 0 auto;
                    }

                    .videos {
                        margin-left: 55px;
                    }

                    #video_container {
                        position: relative;
                    }

                    #video_player div,
                    #video_player figcaption {
                        display: table-cell;
                        vertical-align: top;
                    }

                    #video_container video {
                        position: absolute;
                        display: block;
                        width: 100%;
                        height: 100%;
                        top: 0;
                    }

                    #video_player figcaption {
                        width: 25%;
                    }

                        #video_player figcaption a {
                            display: block;
                        }

                        #video_player figcaption a {
                            opacity: .5;
                            transition: 1s opacity;
                        }

                            #video_player figcaption a img,
                            figure video {
                                width: 100%;
                                height: auto;
                            }

                            #video_player figcaption a.currentvid,
                            #video_player figcaption a:hover,
                            #video_player figcaption a:focus {
                                opacity: 1;
                            }

                    @media (max-width: 700px) {
                        #video_player video,
                        #video_player figcaption {
                            display: table-row;
                        }

                        #video_container {
                            padding-top: 56.25%;
                        }

                        #video_player figcaption a {
                            display: inline-block;
                            width: 33.33%;
                        }
                    }
                </style>
                <script>
                    var video_player = document.getElementById("video_player");
                    video = video_player.getElementsByTagName("video")[0],
                    video_links = video_player.getElementsByTagName("figcaption")[0],
                    source = video.getElementsByTagName("source"),
                    link_list = [],
                    path = '',
                    currentVid = 0,
                    allLnks = video_links.children,
                    lnkNum = allLnks.length;
                    video.removeAttribute("controls");
                    video.removeAttribute("poster");

                </script>
                <div class="col-md-1"></div>
                <div class="col-md-8 col-xs-12 col-sm-12">
                    <div class="videos">

                        <div id="videopage" style="width:100%;  height:auto; background:#000;text-align: center;">
                            <iframe width="100%" id="mainframe" height="460" src="<?php echo $homevideo->youtube_link; ?>" frameborder="0" allowfullscreen=""></iframe>

                        </div>
                        <div class="related">
                            <p>Related Videos</p>
                        </div>
                        <div class="related-img">
                            <ul>
                            <?php 
                            if($homervideos !=''){
                                foreach ($homervideos as $value) {
                                    
                                
                            ?>
                                <li>
                                    <a id="<?php echo $value->youtube_link; ?>" href="#" class="rvds">
                                        <span class="basic_vid">
                                            <div class="vid12" style="">
                                                <img src="<?php echo base_url(); ?>public/videoimages/<?php echo $value->v_image; ?>" class="img-responsive img-thumbnail" />
                                            </div>
                                        </span>
                                    </a>
                                </li>
                              <?php } } ?>

                            </ul>
                        </div>
                        <!--<figure id="video_player">
                            <div id="video_container">
                                <video controls poster="vid-glacier.jpg">

                                    <source src="Videos/Binary Logic.mp4" type="video/mp4">
                                </video>
                            </div>
                            <figcaption>
                                <a href="Videos/Cryptarithmetic  Problem with an Example SEND   MORE = MONEY.mp4" class="currentvid">
                                    <img src="image_pic/criptharthematic.png" alt="Athabasca Glacier">
                                </a>
                                <a href="Videos/Data Sufficiency.mp4">
                                    <img src="image_pic/Datasufficient.png" alt="Athabasca Lake">
                                </a>
                                <a href="Videos/Logarithm Question.mp4">
                                    <img src="image_pic/lossandgain.png" alt="Mountain">
                                </a>
                            </figcaption>
                        </figure>-->
                    </div>

                    <style type="text/css">
                        div.guide {
                            margin: 12px 24px;
                        }

                            div.guide span {
                                color: #ff0000;
                                font: italic 14px Arial, Helvetica, sans-serif;
                            }

                            div.guide p {
                                color: #000000;
                                font: 14px Arial, Helvetica, sans-serif;
                            }

                            div.guide pre {
                                color: #990000;
                            }

                            div.guide p.title {
                                color: #df501f;
                                font: 18px Arial, Helvetica, sans-serif;
                            }
                    </style>

                </div>
                <!--<div class="col-md-1 col-xs-12 col-sm-12"></div>-->
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">


                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>



    <style>
        @media only screen and (max-width: 420px) {
            .vertical {
                border-right: 0px solid #fff;
            }

            h4 {
                font-size: 16px;
            }

            h3 {
                font-size: 19px;
            }

            h2 {
                font-size: 21px;
            }

            p {
                font-size: 11px;
            }


            #fixedsocial {
                top: 45%;
                height: 200px;
                width: 40px;
                position: fixed;
                z-index: 200;
            }

            .facebookflat {
                background: url('icons/50x50 facebook.jpg');
                height: 20px;
                width: 20px;
                transition: ease 200ms;
                background-size: 20px;
            }

                .facebookflat:hover {
                    background: url('icons/60x100 facebook.jpg');
                    height: 25px;
                    width: 40px;
                    background-size: 40px;
                    transition: ease 200ms;
                    margin-left: -20px;
                }

            .twitterflat {
                background: url('icons/50x50 twitter.jpg');
                height: 20px;
                width: 20px;
                transition: ease 200ms;
                background-size: 20px;
            }

                .twitterflat:hover {
                    background: url(icons/60x100twitter.jpg);
                    height: 25px;
                    width: 40px;
                    background-size: 40px;
                    transition: ease 200ms;
                    margin-left: -20px;
                }

            .Youtube {
                background: url('icons/50x50 youtube.jpg');
                height: 20px;
                width: 20px;
                transition: ease 200ms;
                background-size: 20px;
            }

                .Youtube:hover {
                    background: url('icons/60x100 youtube.jpg');
                    height: 25px;
                    width: 40px;
                    background-size: 40px;
                    transition: ease 200ms;
                    margin-left: -20px;
                }
        }

        .vertical {
            border-right: 0px solid #fff;
        }

        .map {
            padding-bottom: 30px;
        }

        }
    </style>

    <style>
        #video_player {
            display: table;
            line-height: 0;
            font-size: 0;
            background: #000;
            max-width: 1000px;
            margin: 0 auto;
        }

        #video_container {
            position: relative;
        }

        #video_player div,
        #video_player figcaption {
            display: table-cell;
            vertical-align: top;
        }

        #video_container video {
            position: absolute;
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
        }

        #video_player figcaption {
            width: 25%;
        }

            #video_player figcaption a {
                display: block;
            }

            #video_player figcaption a {
                opacity: .5;
                transition: 1s opacity;
            }

                #video_player figcaption a img,
                figure video {
                    width: 100%;
                    height: auto;
                }

                #video_player figcaption a.currentvid,
                #video_player figcaption a:hover,
                #video_player figcaption a:focus {
                    opacity: 1;
                }

        @media (max-width: 700px) {
            #video_player video,
            #video_player figcaption {
                display: table-row;
            }

            #video_container {
                padding-top: 56.25%;
            }

            #video_player figcaption a {
                display: inline-block;
                width: 33.33%;
            }
        }
    </style>

    <!--<section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <figure id="video_player">
                            <div id="video_container">
                                <video controls poster="vid-glacier.jpg">

                                    <source src="Videos/Binary Logic.mp4" type="video/mp4">
                                </video>
                            </div>
                            <figcaption>
                                <a href="Videos/Cryptarithmetic  Problem with an Example SEND   MORE = MONEY.mp4" class="currentvid">
                                    <img src="image_pic/criptharthematic.png" alt="Athabasca Glacier">
                                </a>
                                <a href="Videos/Data Sufficiency.mp4">
                                    <img src="image_pic/Datasufficient.png" alt="Athabasca Lake">
                                </a>
                                <a href="Videos/Logarithm Question.mp4">
                                    <img src="image_pic/logarithm.png" alt="Mountain">
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <section id="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">


                    <style type="text/css">
                        div.guide {
                            margin: 12px 24px;
                        }

                            div.guide span {
                                color: #ff0000;
                                font: italic 14px Arial, Helvetica, sans-serif;
                            }

                            div.guide p {
                                color: #000000;
                                font: 14px Arial, Helvetica, sans-serif;
                            }

                            div.guide pre {
                                color: #990000;
                            }

                            div.guide p.title {
                                color: #df501f;
                                font: 18px Arial, Helvetica, sans-serif;
                            }
                    </style>

                </div>
            </div>
        </div>
    </section>



    <!--address -->


    <script>


    $(document).ready(function(){

    $(".rvds").click(function() {       
        //alert($(this).attr("id"));
        //return false;
        $("#mainframe").attr("src", $(this).attr("id"));
        return true;
    })
});
        var videos = [];
        $(document).ready(function () {
            $('#timeS').click(function () {
                $.ajax({
                    url: "http://localhost:53390/TimeSpeedAndDistence/",
                    success: function (data) {
                        alert(data);
                        //$(data).find("a:contains(.mp4)").each(function () {
                        // will loop through
                        // videos = $(this).attr("href");

                        //$('<p></p>').html(images).appendTo('a div of your choice')

                        //  });
                    }
                });
                // alert();
                //alert(videos);
            });

        });

    </script>

</body>


</html>
