<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class videos extends MY_Controller {

        public function __construct() {
            parent::__construct();
            date_default_timezone_set('Asia/Kolkata');
            $this->load->library(array('template','form_validation'));
            $this->load->helper('download');
            $this->load->model('videos_model');
            $this->load->helper('cookie');            
        }        
        public function index()	{
          redirect(base_url().'videos/homevideos');        
	}        
        public function homevideos($cid=NULL, $id = NULL) 
        {
            if($id !=NULL && $cid !=NULL) 
            {
                $data['homevideo'] = $this->videos_model->getlatesthomevideo($cid, $id);
                if($data['homevideo'] !='')
                {
                    $eid = $id;
                    $cid = $cid;
                }
                else
                {
                    $eid ='';
                    $cid = '';
                }
            }
            else if($cid !=NULL) 
            {
                $data['homevideo'] = $this->videos_model->getlatesthomevideo($cid);

                if($data['homevideo'] !='')
                {
                    $eid = $data['homevideo']->id;
                    $cid = $data['homevideo']->c_id;
                }
                else
                {
                    $eid ='';
                    $cid = '';
                }
            }  
            else 
            {
                $data['homevideo'] = $this->videos_model->getlatesthomevideorandom();
                if($data['homevideo'] !='')
                {
                    $eid = $data['homevideo']->id;
                    $cid = $data['homevideo']->c_id;
                }
                else
                {
                    $eid ='';
                    $cid = '';
                }
            }
                
            $data['categories'] = $this->videos_model->getallcategories();
            if($data['homevideo'] !='')
            {
                $data['homervideos'] = $this->videos_model->getlatesthomervideos($cid,$eid);  
            }
            else
            {
                $data['homervideos'] ='';    
            }
         
            if($data['homevideo'] !='') 
            {
                $hvid = $data['homevideo']->id; 
         
                set_cookie (array(
                    'name'     => 'site_cookie',
                    'value'  => 'hvideocookie'.$hvid,
                    'expire'   => time()+3,
                ));
        
                $date = date('Y-m-d');
                $cook = get_cookie('site_cookie'); 
    
                if($cook == 'hvideocookie'.$hvid)
                {
                    $vCookie['is_cookie'] = '1';
                }
                else
                {
                    $vCookie['is_cookie'] = '0';
                }
                
                $res = $this->videos_model->gethvideoviews($hvid);
        //echo "<pre>";
                if($res)
                {
            
                    $cookUnique = get_cookie('site_Uniquecookie'); 
          
                    if ($cookUnique !='unique'.$hvid  && $vCookie['is_cookie']) 
                    {
                        // echo "hi";exit;
                        set_cookie (array(
                            'name'     => 'site_Uniquecookie',
                            'value'  => 'unique'.$hvid,
                            'expire'   => time()+3,

                        ));
         
                        $hid = intval($res->id);
                        $pageviews = $res->views;
                        $stats['views'] = intval($pageviews)+1;
                        $res = $this->videos_model->updatehvideoViews($hid, $stats);

                    }   
                }
                else
                {    
                    $istats['views'] = 1;
                    $res = $this->videos_model->updatehvideoViews($hvid, $istats);
                }
         
            }
         //echo "<pre>"; print_r($data);exit;
          $this->template->theme('videos',$data);
          
        }
        public function gettopicsvideo($tid, $vid=NULL) {
           // echo "hi"; exit;
            if($vid !=NULL) {
            $data['videe'] = $this->videos_model->gettopicrelatedvideo($vid);
            }else {
            $data['video'] = $this->videos_model->gettopicsvideo($tid);
            }
            $data['relatedvideos'] = $this->videos_model->gettopicrelatedvideos($tid);
          //echo "<pre>"; print_r($data); exit;
             $this->template->theme2('video', $data);
            
        }
        
        
        public function changelikes() {
            $type= $this->input->post('type');
            $id=$this->input->post('id');
            
            if(isset($_COOKIE['vlikeDislike'."_".$id])) // check cookie
            echo "Already Voted"; // if exist display message
            else{
            if($type=='like'){
                $result = $this->videos_model->getlikescount($id);
                $countlikes = $result->like;
                $totallikes = $countlikes+1;
                
                $formvalues['like'] = $totallikes;
                $this->videos_model->updatelikes($formvalues, $id);
                
                
                $expire=time()+3600;
                setcookie("vlikeDislike"."_".$id, "vlikeDislike"."_".$id, $expire);
                echo "success";
            }else if($type=='dislike'){
                $result = $this->videos_model->getlikescount($id);
                $countdislikes = $result->dislike;
                $totaldislikes = $countdislikes+1;
                
                $formvalues['dislike'] = $totaldislikes;
                $this->videos_model->updatelikes($formvalues, $id);
                
                $expire=time()+3600;
                setcookie("vlikeDislike"."_".$id, "vlikeDislike"."_".$id, $expire);
                echo "success";
            }
           
            }
         }
         public function changehomelikeslikes() {
            $type= $this->input->post('type');
            $id=$this->input->post('id');
            
            if(isset($_COOKIE['hvlikeDislike'."_".$id])) // check cookie
            echo "Already Voted"; // if exist display message
            else{
            if($type=='like'){
                $result = $this->videos_model->getlikeshomecount($id);
                $countlikes = $result->like;
                $totallikes = $countlikes+1;
                
                $formvalues['like'] = $totallikes;
                $this->videos_model->updatehomelikes($formvalues, $id);
                
                
                $expire=time()+3600;
                setcookie("hvlikeDislike"."_".$id, "hvlikeDislike"."_".$id, $expire);
                echo "success";
            }else if($type=='dislike'){
                $result = $this->videos_model->getlikeshomecount($id);
                $countdislikes = $result->dislike;
                $totaldislikes = $countdislikes+1;
                
                $formvalues['dislike'] = $totaldislikes;
                $this->videos_model->updatehomelikes($formvalues, $id);
                
                $expire=time()+3600;
                setcookie("hvlikeDislike"."_".$id, "hvlikeDislike"."_".$id, $expire);
                echo "success";
            }
           
            }
         }
         
       
       
         
       
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */