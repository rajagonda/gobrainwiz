<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : users
 * Mail id : phpguidance@gmail.com
 */
class users extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->library('template');
        $this->load->model(array('common_model', 'users_model', 'onlinetest/onlinetest_model'));
        $this->load->helper('sendmail_helper');
        $this->load->helper('phpass');
    }

    public function register()
    {
        $this->template->theme('register');
    }

    public function login()
    {

        $this->template->theme('login');
    }

    public function checkemail()
    {
        $email = $this->input->post('email');
        $res = $this->users_model->checkmail($email);
        if ($res != '') {
            echo 'false';
            exit;
        } else {
            echo 'true';
            exit;
        }
    }

    public function checkphonenumber()
    {
        $phone = $this->input->post('mobile');
        $res = $this->users_model->checkphonenumber($phone);
        //echo $res;exit;
        if ($res != '') {
            echo 'false';
            exit;
        } else {
            echo 'true';
            exit;
        }
    }

    function saveUser()
    {
        if ($_POST) {
            
            $uname = $this->input->post('uname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            //$mobile = $this->input->post('mobile');            


            if ($uname == '' || $email == '' || $password == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $data['message'] = "Invalid email format";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            } else {
                //check email was exit or not in database
                $res = $this->users_model->checkmail($email);
                if ($res) {
                    $data['message'] = "Email Already Exit";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }
            }


            $hash = md5(time() . rand(0, 1000));
            //prepare user table data
            $userData['member_name'] = $uname;
            $userData['member_email'] = $email;
            $userData['member_password'] = $email;


            $userData['user_hash'] = $hash;

            //create password

            //Convert Password To Security Hash Format
            $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $hashed_password = $hasher->HashPassword($password);
            $userData['member_password'] = $hashed_password;

            $memberId = $this->common_model->insertSingle('gk_members', $userData);
            if ($memberId) {

                $emaildata['user_email'] = $email;
                $emaildata['hash'] = $hash;
                $emaildata['password'] = $password;
                $message = $this->load->view('emailtemplates/emailverification', $emaildata, 'true');

                $subject = "Go Brainwizz Email Verification";
                if (!regsendMail($email, $message, $subject)) {
                    $data['message'] = "Technical Error Please Try Again Once1!";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                } else {
                    $data['message'] = "Your Registration Done PLease Verify Email Check sapm folder also";
                    $data['id'] = "1";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }

            } else {
                $data['message'] = "Technical Error Please Try Again Once!";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        } else {
            redirect('Home', 'refresh');
        }
    }

    function randomNumber()
    {
        $alphabet = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    function activation()
    {
        $email = $_REQUEST['email'];
        $hash = $_REQUEST['hash'];
        if ($email != '' && $hash != '') {
            //check   otp verification
            $emailVeryParams = array('user_hash' => $hash);
            $result = $this->common_model->getSingleRow('gk_members', $emailVeryParams);
            if ($result) {
                if ($result->user_hash != '1') {
                    $updateParams = array('member_id' => $result->member_id, 'user_hash' => $hash);
                    $memberUpdateData['user_hash'] = '1';
                    $memberUpdateData['user_email_verify'] = 'y';
                    $memberUpdateResult = $this->common_model->updateRow('gk_members', $memberUpdateData, $updateParams);
                    redirect(base_url() . 'users/login');
                    exit;
                } else {
                    echo "Already Veryfy Email Id";
                    exit;
                }
            } else {
                echo "Sorry Wrong Details passing or Already Veryfied this account";
                exit;
            }
        } else {
            echo "fff";
            exit;
        }
    }

    function loginsubmit()
    {
        if ($_POST) {
            //echo "<pre>"; print_r($_POST);exit;
            $loginid = $this->input->post('loginid');
            $password = $this->input->post('password');

            if ($loginid == '' || $password == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            if (!filter_var($loginid, FILTER_VALIDATE_EMAIL)) {
                $data['message'] = "Invalid email format";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            //check email exit or not 
            $loginDeatils = $this->users_model->checkmail($loginid);
            if ($loginDeatils) {

                if ($loginDeatils->user_email_verify == 'n') {
                    $data['message'] = "Email verification Was Not Done Please Verify Email Id";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }

                //check password is correct or not
                $password = trim($password);
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                if ($hasher->CheckPassword($password, $loginDeatils->member_password)) {
                    $userParams = array('member_email' => $loginid);
                    $userDeatils = $this->common_model->getSingleRow('gk_members', $userParams);
					

                    // $userDeatils = $this->users_model->getUserDeatils($loginDeatils->user_id);
                    if ($userDeatils) {
                        $this->session->set_userdata('user_login', '1');
                        $this->session->set_userdata('userId', $loginDeatils->member_id);
                        $this->session->set_userdata('useremail', $userDeatils->member_email);
                        $this->session->set_userdata('username', $userDeatils->member_name);
						$this->session->set_userdata('profile_pic', $userDeatils->profile_pic);
						
                        $data['message'] = 'Successfully Login Done ';
                        $data['id'] = "1";

                        header('Content-Type: application/json');
                        echo json_encode($data);
                        exit;


                    } else {
                        $data['message'] = "Sorry Technical Issue Try Once";
                        $data['id'] = "0";
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        exit;
                    }

                } else {
                    $data['message'] = "Invalid Password";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }

            } else {
                $data['message'] = "Invalid Login Email";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('pages/home');
    }

    function forgotpassword()
    {

        $this->template->theme('forgot');
    }

    function forgotpasswordsubmit()
    {

        if ($_POST) {
            //echo "<pre>"; print_r($_POST);exit;
            $loginid = $this->input->post('loginid');


            if ($loginid == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            if (!filter_var($loginid, FILTER_VALIDATE_EMAIL)) {
                $data['message'] = "Invalid email format";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            //check email exit or not 
            $loginDeatils = $this->users_model->checkmail($loginid);
            if (!$loginDeatils) {


                $data['message'] = "Invalid User Email Id Or Not A Member";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;

            }

            //create password
            $pswd = rand(789453, 128456);

            //Convert Password To Security Hash Format
            $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $hashed_password = $hasher->HashPassword($pswd);
            $userData['member_password'] = $hashed_password;
            $userParams = array('member_email' => $loginid);
            $res = $this->common_model->updateRow('gk_members', $userData, $userParams);
            if ($res) {

                $emaildata['user_email'] = $loginid;
                $emaildata['password'] = $pswd;
                $message = $this->load->view('emailtemplates/forgotpassword', $emaildata, 'true');

                $subject = "Go Brainwizz Forgot Password";
                if (!regsendMail($loginid, $message, $subject)) {
                    $data['message'] = "Technical Error Please Try Again Once1!";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                } else {
                    $data['message'] = "Please check your email id for new login details";
                    $data['id'] = "1";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }

            } else {
                $data['message'] = "Technical Error Please Try Again Once!";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        } else {
            redirect('Home', 'refresh');
        }
    }

    public function manageProfile()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $id = $this->session->userdata('studentId');

            $original_value = $this->db->query("select examuser_mobile from gk_examuserslist where examuser_id ='" . $id . "' ")->row()->examuser_mobile;

            if (isset($original_value) && $this->input->post('examuser_mobile') != $original_value) {
                $is_unique = '|is_unique[gk_examuserslist.examuser_mobile]';
            } else {
                $is_unique = '';
            }

            $this->form_validation->set_rules('examuser_name', 'Name', 'required');
            $this->form_validation->set_rules('examuser_email', 'Email', 'required');
            $this->form_validation->set_rules('examuser_mobile', 'mobile', 'required'.$is_unique);

            if ($this->form_validation->run() == TRUE) {


                $input = array();
                $input['examuser_name'] = $this->input->post('examuser_name');
                $input['examuser_email'] = $this->input->post('examuser_email');
                $input['examuser_mobile'] = $this->input->post('examuser_mobile');

                $this->common_model->updateRow('gk_examuserslist', $input, array('examuser_id' => $id));
                $this->session->set_userdata('profile_message', "Successfully Updated!");
                redirect('users/manageProfile');

            }
        }


        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }

        $data['completetestscount'] = $a;

        $data['user'] = $this->common_model->getSingleRow('gk_examuserslist', array('examuser_id' => $studentId));
        $this->template->theme('manage_profile', $data);

    }

	public function change_avatar()
	{
		if(isset($_FILES['image_upload_file']))
		{
				
                $filename = time()."_".$_FILES['image_upload_file']['name'];
                $config['upload_path'] = 'upload/profilepics/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
            
                if (!$this->upload->do_upload('image_upload_file')) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                } 
                
                $this->data = $this->upload->data();
                $post_image = $this->data['file_name'];
                $resized_image = $this->resizeImage($post_image);
                if($resized_image){
                    $split = explode('.', $resized_image);
                    $thumb_image =  $split[0] . '_thumb.' . $split[1];
                }
				$output['status']=TRUE;
				$output['image_medium']= base_url()."upload/profilepics/thumbnail/".$thumb_image;
				$output['image_small'] = base_url()."upload/profilepics/thumbnail/".$thumb_image;
						
		}
		//print_r($output);
		//$this->onlinetest_model->update_avatar($thumb_image,$this->session->userdata('studentId'));
                
		echo json_encode($output);
		
	}
	 public function resizeImage($filename)
    {
        $source_path = 'upload/profilepics/' . $filename;
        $target_path = 'upload/profilepics/thumbnail/';

        $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '_thumb',
                'width' => 360,
                'height' => 241
        );

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) 
        {
			echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();

        return $filename;
    }	
}



/* End of file skeleton.php */
/* Location: ./application/modules/skeleton/controllers/skeleton.php */