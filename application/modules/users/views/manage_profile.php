
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Profile</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript:;">Student Name Will be here</a></li>
						<li class="breadcrumb-item active"><a>My Profile</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col 4-->
                    <div class="col-lg-4">
                      <?php require_once('sidebar.php'); ?>
                    </div>
                    <!--/ left col 4-->

                    <!-- right container -->
                    <div class="col-lg-8">
                        <div class="right-usersection">
                            <!-- title -->
                            <div class="user-right-title">
                                <h3 class="h4 mb-0">My Profile</h3>
                                <p><small>Add information about yourself</small></p>
                            </div>
                            <!--/ title -->

                            <!-- right user body -->
                            <div class="user-rightsection">
							<?php
                        if ($this->session->userdata('profile_message')) {?>
                           <p style="color: green;">
								<strong>Success: </strong>
								<?php
								echo $this->session->userdata('profile_message');
								$this->session->set_userdata('profile_message', "");
								?>
								</p>
						<?php
                        }
                        ?>
                                <form id="userForm" action="" method="post">
                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input name="examuser_name" type="text" placeholder="Name" value="<?php echo $user->examuser_name ?>" class="form-control">
												<?php echo form_error('examuser_name', '<div class="error">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="examuser_email"
                                           placeholder="Email" value="<?php echo $user->examuser_email ?>" readonly>
										   <?php echo form_error('examuser_email', '<div class="error">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->

                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <input type="text" class="form-control" name="examuser_mobile"
                                           id="examuser_mobile" placeholder="Mobile Number" value="<?php echo $user->examuser_mobile ?>" readonly>
										   <?php echo form_error('examuser_mobile', '<div class="error">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->                                

                                    

                                     <!-- row -->
                                     <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-12">                                          
                                            <input type="submit" class="bluebtn" value="Save Profile">
                                        </div>
                                        <!--/ col -->                                        
                                    </div>
                                    <!--/ row -->
                                </form>
                            </div>
                            <!--/ right user body -->
                        </div>
                    </div>
                    <!--/ right container -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    