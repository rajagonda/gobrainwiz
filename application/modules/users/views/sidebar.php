<div class="whitebox left-usernav">
	<figure class="profile-pic position-relative">
		<a href="javascript:void(0)">
			<img src="<?php echo assets_url();?>img/user-profilepic.jpg" alt="Student Name will be here">
		</a>
		<div class="upload-btn-wrapper">
			<button class="whitebtn mx-auto"><span class="icon-edit icomoon"></span></button>
			<input type="file" name="myfile" />
		</div>
	</figure>
	<h2 class="h6 text-center pt-3 mb-0"><?php echo $this->session->userdata('examusername');?></h2>
	<p class="text-center"><small><?php echo $this->session->userdata('examuseremail');?></small></p>
	<?php 
		$active_url = $this->uri->segment(2);
	?>
	
	<ul>
		<li>	
			<a <?php if($active_url == 'dashboard'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/dashboard">
			Upcoming test <?php echo $tcount - $completetestscount; ?>
			</a>
		</li>
		<li>
			<a <?php if($active_url == 'testhistory'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/testhistory">
			Completed Test <?php echo $completetestscount; ?>
			</a>
		</li>
		<?php if ($this->session->userdata('user_login') == '1') { ?>
		
		<li><a <?php if($active_url == 'withdrawrequests'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/withdrawrequests">Withdraw Requests</a></li>
		
		<li><a <?php if($active_url == 'userWallet'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/userWallet">Wallet</a></li>
		
		<li><a <?php if($active_url == 'manageProfile'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>users/manageProfile">Update Profile</a></li>
		
		<li><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
		<?php } ?>
	</ul>
</div>