	<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>new/css/login.css">
<style>
b:hover{
	color:rgba(255, 87, 34, 0.71)!important;
}
b{
    font-size: 15px;
    color: #0e4d84;
}
b:hover{
    color: #1561a2!important;
}
.btn-group-lg>.btn, .btn-lg {
padding: 4px 16px;}
.btn-block {
	height: 36px;
}
.error {
    color: red;
}
.errorClass { border:  1px solid red !important; }
	.footer-wrapper {
    background: #222222;
    margin: 0;
    margin-top: -57px;
    padding: 42px 0 4px;
}	
</style>
	<div class="row main">
			
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
				<span  class="col-sm-12" style="display:none;color:#de2128" id="forgotfailuremsg"></span>
				<span  class="col-sm-12" style="display:none;color:green" id="forgotsuccessmsg"></span>
					<form method="post" action="#" name="forgot_form" id="forgot_form" onsubmit="return validateloginForm()">
						
			            <div class="form-group">
							<label for="email" class="cols-sm-2 control-label">User Name</label>
								<input type="email" class="form-control" id="loginid" name="loginid" placeholder="Enter Email Id"/>
						</div>
					    <div class="form-group ">
							<button type="submit" name="log" class="btn btn-primary btn-lg btn-block login-button" style="background-color: #1561a2;border: 1px solid #1c1c43;">Submit</button>
						</div>
						<div class="login-register">
				         <a href="<?php echo base_url();?>users/login" style="font-size:15px;"><b>Login here!!</b></a>
				        </div>
					</form>
				</div>
			</div>
		</div>

<script type="text/javascript">

function validateloginForm(){




 var loginid,password;

 var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if( document.forgot_form.loginid.value == "" ) {
      document.forgot_form.loginid.className = 'form-control errorClass';
      return false;
  }else {
      document.forgot_form.loginid.className = 'form-control';
      loginid = document.forgot_form.loginid.value;

  }

    if( document.forgot_form.loginid.value == "" ) {
      document.forgot_form.loginid.className = 'form-control errorClass';
      document.forgot_form.loginid.focus();
      return false;
  }else if(!regex.test(document.forgot_form.loginid.value)){
      document.forgot_form.loginid.className = 'form-control errorClass';
      return false;
  }else {
      document.forgot_form.loginid.className = 'form-control';
      loginid = document.forgot_form.loginid.value; 
  }



  
  var dataString = 'loginid='+ loginid +'&ajax=1';
  //document.getElementById("login").style.display = "none";
  
         
// return false;
  $.ajax({
      url:'<?php echo base_url();?>users/forgotpasswordsubmit',
      type: 'POST',
      datatype:'application/json',
      data: dataString,
      success:function(msg){  
        //alert(response.message);return false;
        if(msg.id == "0"){          

			$("#forgotfailuremsg").css("display", "block");
			$("#forgotfailuremsg").html(msg.message);
			$('#forgot_form').trigger("reset");
              
         }else if(msg.id == '1'){
                $("#forgotfailuremsg").css("display", "none");
                $("#forgotsuccessmsg").css("display", "block");
                 $("#forgotsuccessmsg").html(msg.message);
                 $("#forgotsuccessmsg").css("display", "block").delay(10000).fadeOut();
                  $('#forgot_form').trigger("reset");
              }else {
                 $("#forgotfailuremsg").css("display", "none");
                 $("#forgotsuccessmsg").css("display", "block").delay(10000).fadeOut();
                  $('#forgot_form').trigger("reset");

              }
               
      }
  });

  


  return false;
}
</script>
