		<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>new/css/login.css">
<style>
   .error {
    color: red;
}
.errorClass { border:  1px solid red !important; }
b:hover{
    color: #1561a2!important;
}
b{
    font-size: 15px;
    color: #0e4d84;
}
.btn-group-lg>.btn, .btn-lg {
padding: 4px 16px;}
.btn-block {
	height: 36px;
}
.login-button:hover{
	background-color: #105fa5!important;
    color: white;
    border: 1px solid #9dcef9!important;
}
.footer-wrapper {
    background: #222222;
    margin: 0;
    margin-top: -57px;
    padding: 42px 0 4px;
}
</style>

		<div class="container-fluid" id="login-area">
			<div class="row main">
			
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
				<span  class="col-sm-12" style="display:none;color:#de2128" id="regfailuremsg"></span>
            <span  class="col-sm-12" style="display:none;color:green" id="regsuccessmsg"></span>
					<form method="post" action="#" name="rigister_form" id="rigister_form" class="form-horizontal rigister_form">
						
			            <div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Name</label>
								<input type="text" class="form-control" id="uname" name="uname" placeholder="Enter your Fullname"/>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Email</label>
                              <input type="email" class="form-control" id="email" name="email" placeholder="Enter your Email"/>
						</div>
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
                              <input type="password" class="form-control" id="password" name="password" placeholder="Enter your Password"/>
						</div>
						<div class="form-group">
							<label for="confpassword" class="cols-sm-2 control-label">Confirm Password</label>
                              <input type="password" class="form-control" id="password_again" name="password_again" placeholder="Enter Confirm Password"/>
						</div>
					    <div class="form-group ">
							<button type="submit" name="log" class="btn btn-primary btn-lg btn-block login-button" style="background-color: #1561a2;border: 1px solid #1c1c43;">Register</button>
						</div>
						<div class="login-register">
				         <a href="<?php echo base_url();?>users/login" style="font-size:15px;"><b>Login Now!!</b></a>
				        </div>
					</form>
				</div>
			</div>
		</div>

 </div>
<script>
$(function() {
 $("#rigister_form").validate({    
        // Specify the validation rules
        rules: {            
             uname: {
                required: true                
            },
            email: {
                required: true,
                email: true,
                remote: {
                url: "<?php echo base_url(); ?>users/checkemail",
                type: "post"
                }
            },
            // mobile: {
            //     required: true,                
            //     digits: true,
            //     minlength: 10,
            //     maxlength: 10,
            //     remote: {
            //     url: "<?php echo base_url(); ?>users/checkphonenumber",
            //     type: "post"
            //     }
            // },
             password: {
                required: true,
                minlength: 5
            },
            password_again: {
                equalTo: "#password"
            }            
        },
        
        // Specify the validation error messages
        messages: {
           email : {          
                remote: "Email Already Used!"
          }
          // mobile : {               
          //       remote: "Phone Number Already Used!"
          // }
        },
        
        submitHandler: function(form) {
           //form.preventDefault();
        $.ajax({
            type: "POST",
            url:'<?php echo base_url();?>users/saveUser',
            datatype:'application/json',
            data: $('form.rigister_form').serialize(),
            success: function (msg) {
              //console.log(msg);
              if(msg.id == '0') {
                 $("#regfailuremsg").css("display", "block");
                  $("#regfailuremsg").html(msg.message);
                    $('#rigister_form').trigger("reset");
              }else if(msg.id == '1'){
                $("#regfailuremsg").css("display", "none");
                $("#regsuccessmsg").css("display", "block");
                 $("#regsuccessmsg").html(msg.message);
                 $("#regsuccessmsg").css("display", "block").delay(10000).fadeOut();
                  $('#rigister_form').trigger("reset");
              }else {
                 $("#regfailuremsg").css("display", "none");
                 $("#regsuccessmsg").css("display", "block").delay(10000).fadeOut();
                  $('#rigister_form').trigger("reset");

              }
              
             
                //$("form.noteform").modal('hide');
            },
            error: function () {
                alert("failure");
            }
        });
        return false;
    
        }
    });

 

  });


  </script>