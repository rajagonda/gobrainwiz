		<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>new/css/login.css">
<style>
   .error {
    color: red;
}
.errorClass { border:  1px solid red !important; }
        .navbar {
            /*336699trythis color*/
            border-radius: 0px;
        }
b:hover{
    color: #1561a2!important;
}
b{
font-size: 15px;
color: #0e4d84;
}
.btn-group-lg>.btn, .btn-lg {
padding: 4px 16px;}
.btn-block {
	height: 36px;
}
a:href{
	color:white;
}
strong{
    color: white;
    font-weight: 600!important;
}
.footer-wrapper {
    background: #222222;
    margin: 0;
    margin-top: -57px;
    padding: 42px 0 4px;
}
</style>

   <div class="container-fluid" id="login-area">
			<div class="row main">
			
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
				  <span class="col-sm-12" style="display:none;color:#de2128" id="loginfailuremsg"></span>
					<form action="#" method="post" name="login_form" id="login_form" onsubmit="return validateloginForm()">
						
			            <div class="form-group">
						<label for="email" class="cols-sm-2 control-label">User Name</label>
					    <input type="text" class="form-control" id="loginid" name="loginid" placeholder="Enter your EmailId"/>
						</div>

						<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Password</label>
					    <input type="password" class="form-control" id="password" name="password" placeholder="Enter your Password"/>
						</div>
					    <div class="form-group ">
							<button type="submit" name="log" class="btn btn-primary btn-lg btn-block login-button" style="background-color: #1561a2;border: 1px solid #1c1c43;">Login</button>
						</div>
						<div class="login-register">
				         <a href="<?php echo base_url();?>users/register" style="font-size:15px;"><strong>New User ? </strong><b>Register Here</b></a><br>
						 <a href="<?php echo base_url();?>users/forgotpassword" style="font-size:15px;"><strong>Forgot Password ? </strong><b>Click Here</b></a>
				        </div>
					</form>
				</div>
			</div>
		</div>


<script type="text/javascript">

function validateloginForm(){


 var loginid,password;

 var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if( document.login_form.loginid.value == "" ) {
      document.login_form.loginid.className = 'form-control errorClass';
      return false;
  }else {
      document.login_form.loginid.className = 'form-control';
      loginid = document.login_form.loginid.value;

  }

    if( document.login_form.loginid.value == "" ) {
      document.login_form.loginid.className = 'form-control errorClass';
      document.login_form.loginid.focus();
      return false;
  }else if(!regex.test(document.login_form.loginid.value)){
      document.login_form.loginid.className = 'form-control errorClass';
      return false;
  }else {
      document.login_form.loginid.className = 'form-control';
      loginid = document.login_form.loginid.value; 
  }



  if( document.login_form.password.value == "" ) {
      document.login_form.password.className = 'form-control errorClass';
      return false;
  }else {
      document.login_form.password.className = 'form-control';
      password = document.login_form.password.value; 
  }
  
  var dataString = 'loginid='+ loginid +'&ajax=1&password='+password;
  //document.getElementById("login").style.display = "none";
  
         
// return false;
  $.ajax({
      url:'<?php echo base_url();?>users/loginsubmit',
      type: 'POST',
      datatype:'application/json',
      data: dataString,
      success:function(response){  
        //alert(response.message);return false;
        if(response.id == "1"){          
              
              window.location.replace("<?php echo base_url(); ?>download");
              
        }else   if(response.id == 0){          
            
            document.getElementById("loginfailuremsg").style.display = "block";
            document.getElementById("loginfailuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("loginfailuremsg").style.display = "none";
            }, 10000);          
            
        }else {                      
            document.getElementById("loginfailuremsg").style.display = "block";
            document.getElementById("loginfailuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("loginfailuremsg").style.display = "none";
            }, 10000);          
            
        }
               
      }
  });

  


  return false;
}
</script>
