<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : elitmus
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class elitmus extends MY_Controller {

     public function __construct() {
        parent::__construct();
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('elitmus_Model');
        $this->load->language('elitmus');
        $this->load->helper('download');
        $this->load->library('pagination');
     }
     public function index()	{
		
         $data['categories'] = $this->elitmus_Model->getallcategories();
        //echo "<pre>"; print_r($data);exit;
        if($data['categories'] !='') {
        foreach($data['categories'] as $category) {
        $data['subcatciunt'][$category->id] = $this->elitmus_Model->getsubcatcount($category->id);
        }
        }
        $this->template->theme('placementpapers',$data);
    }
    public function companypapers($id=null,$pgid=null) {
		
//echo $pgid;

          $data['categories'] = $this->elitmus_Model->getallcategories();
         

           // echo "<pre>"; print_r($data['categories'] );

            if($id==null){

           if($data['categories'] !=''){
                $id= $data['categories'][0]->id;
            }else {
                $id=0;
            }

            }
             if($pgid == null){
                $pgid=0;
            }
                        

        $data['categories'] = $this->elitmus_Model->getallcategories();


        $data['ccatdetails'] = $this->elitmus_Model->getcategory($id);


        $config['base_url'] = base_url().'/elitmus/companypapers/'.$id.'/'; 
        $config["total_rows"] = $this->elitmus_Model->questionscount($id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config["cur_page"] = $this->uri->segment(4);


        

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="page-item"> ';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="page-item active" ><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';     
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["questions"] = $this->elitmus_Model->getallquetions($id,$config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
      //  echo "<pre>"; print_r($data);exit;






            
            // foreach($data['subcategeries'] as $abc) {
            //   $data['questioncount'][$abc->id] = $this->elitmus_Model->getqestioncatcount($abc->id);
            // }

            // if($data['questions'] !=''){
            // foreach($data['questions'] as $val) {
            //   $data['questionanswer'][$val->id] = $this->elitmus_Model->getquetionanswer($val->question_answer,$val->id);
            // }
            // }

            // $data['companyname'] = $this->elitmus_Model->getcategory($id);
            // $data['subcatname'] = $this->elitmus_Model->getcategory($data['subcatid']);
            // $data['catid'] = $id;
            // $sql = "SELECT  * FROM gk_companyqas WHERE cat_id='{$data['subcatid']}' AND question_status='y'";
            // $query = $this->db->query($sql);
            // $totalCount=$query->num_rows();
            // $data['totalCount']=$totalCount;
             $data['page_no']=(int)$this->uri->segment(4,0);
            // $data['pagination']=$this->customPagination($totalCount,10,$this->uri->segment(4,0),site_url("elitmus/companypapers/$id"));
            $this->template->theme('companytestview',$data);
          
        }

        public function customPagination($total, $per_page = 20,$page = 1, $url){
        // $query = "SELECT COUNT(*) as `num` FROM {$query}";
        // $row = mysql_fetch_array(mysql_query($query));
        // $total = $row['num'];
        $adjacents = "2";

        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;

        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;

        $pagination = "";
        if($lastpage > 1)
        {
            $pagination .= "<ul class='pagination'>";
                    $pagination .= "<li class='details' style='position:relative;top:8px;left:10px'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='{$url}/$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}/$lastpage'>$lastpage</a></li>";
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<li><a href='{$url}/1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}/2'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='{$url}/$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}/$lastpage'>$lastpage</a></li>";
                }
                else
                {
                    $pagination.= "<li><a href='{$url}/1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}/2'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                }
            }

            if ($page < $counter - 1){
                $pagination.= "<li><a href='{$url}/$next'>Next</a></li>";
                $pagination.= "<li><a href='{$url}/$lastpage'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='currentd'>Next</a></li>";
                $pagination.= "<li><a class='currentd'>Last</a></li>";
            }
            $pagination.= "</ul>\n";
        }


        return $pagination;
    }

    public function downloadpapers(){
        $data['categories'] = $this->elitmus_Model->getallcat();
        //$data['categories1'] = $this->elitmus_Model->getallcategories();
        //echo "<pre>"; print_r($data);exit;
        if($data['categories'] !=''){
        foreach($data['categories'] as $category) {
        $data['cn'][$category->cat_id] = $this->elitmus_Model->getcatpdfcount($category->cat_id);
        }
        }
        //echo "<pre>"; print_r($data);exit;
        $this->template->theme('downloadpapers',$data);
    }
    public function companypapersdownload($id) {

            $data['subcategeries']  = $this->elitmus_Model->getsubcategeries($id);

            foreach($data['subcategeries'] as $abc) {
              $data['pdffiles'][$abc->id] = $this->elitmus_Model->getpdffiles($abc->id);
            }
            $data['catid'] = $id;
            $data['companyname'] = $this->elitmus_Model->getcategory($id);


             $data['totalfiles'] = count(array_filter($data['pdffiles']));
            //echo "<pre>"; print_r($data); exit;

            $this->template->theme('papersdownload',$data);


        }
        public function downloadpdf($id) {
            $result  = $this->elitmus_Model->getpdffile($id);

        if($result !='') {
          $file_name =  $result->path;
         $path = "upload/company_pdfs/".$result->path;
         $data = file_get_contents($path); // Read the file's contents
	 $name = $file_name;
 	 force_download($name, $data);
        }
        }

//This is the ajax function.
        public function get_pdf_iframe()
        {
          $id=$this->input->post('rec_id');
          $result  = $this->elitmus_Model->getpdffile($id);
          if($result !='') {
            $pdf=base_url("/upload/company_pdfs/".$result->path);
          //echo "<iframe src='$pdf'></iframe>";
          echo "<iframe src='http://docs.google.com/viewer?url=$pdf&embedded=true' width='100%' height='600' style='border: none;'></iframe>";
        }
        else{
          echo "Sorry No PDF Found !";
        }
        }


}
