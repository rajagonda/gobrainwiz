<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BRAINWIZ</title>

	<meta name="description" content=" " />
	<meta name="keywords" content="" />
	<meta name="google-site-verification" content="" />
	<meta name="msvalidate.01" content="" />
	<meta name="ROBOTS" content="INDEX, FOLLOW" />
	<meta name="ROBOTS" content="ALL" />
	<meta name="googlebot" content="index,follow,archive" />
	<meta name="msnbot" content="index,follow,archive" />
	<meta name="Slurp" content="index,follow,archive" />
	<meta name="author" content="" />
	<meta name="publisher" content="" />
	<meta name="owner" content="" />
	<meta http-equiv="content-language" content="English" />
	<meta name="doc-type" content="Web Page" />
	<meta name="doc-rights" content="Copywritten Work" />
	<meta name="rating" content="All" />
	<meta name="distribution" content="Global" />
	<meta name="document-type" content="Public" />
	<meta name="revisit-after" CONTENT= "daily"/>
	<meta name="geo.region" content="IN-TS" />
	<meta name="geo.placename" content="Hyderabad" />
   <meta name="format-detection" content="telephone=no">
    
	<!--header Styles -->
	 <link href="<?php echo assets_url();?>new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <link href="<?php echo assets_url();?>new/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
     <link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.carousel.css" rel="stylesheet">
     <link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.theme.css" rel="stylesheet">
     <link href="<?php echo assets_url();?>new/css/style.css" rel="stylesheet">
     <link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_url();?>images/img/favicon.png">
	 <link href="<?php echo assets_url();?>new/css/hover.css" rel="stylesheet" media="all">
	<!--script src="http://code.jquery.com/jquery-latest.min.js"></script-->
</head>
<link href="<?php echo assets_url();?>new/css/easy.css" rel="stylesheet" type="text/css">
<link href="<?php echo assets_url();?>new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.theme.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/css/style.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/css/responsive.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_url();?>new/images/favicon.png">
<link href="<?php echo assets_url();?>new/css/hover.css" rel="stylesheet" media="all">
<!-- Popup Starts Here -->
 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

 <!--checkbox image button start-->
 <script>
$(document).ready(function(){
$('ul.options li span#tick-icon').click(function(){
//alert("hi");
$("ul.options li span#tick-icon").removeClass("select");
$(this).addClass('select');
});
});
</script>
<!--checkbox image button end-->
<!--tab button start-->
<script>
$(document).ready(function(){
$('div.left div.pic-container div.pic-row a.btn').click(function(){
$(this).addClass('select');
});
});
</script>
<!--tab button end-->
<body>
<section id="taketest">
<div class="container-fluid">
 <div class="row">
 <div class="col-sm-2 tab1" id="gate1">
 <h3>QUESTION</h3>
 <div class="pic-container">
  <div class="pic-row">
    <a data-toggle="tab" href="#1" class="btn">1</a>
    <a data-toggle="tab" href="#2" class="btn">2</a>
    <a data-toggle="tab" href="#3" class="btn">3</a>
    <a data-toggle="tab" href="#4" class="btn" >4</a>
    <a data-toggle="tab" href="#5" class="btn" >5</a>
    <a data-toggle="tab" href="#6" class="btn" >6</a>
    <a data-toggle="tab" href="#7" class="btn">7</a>
    <a data-toggle="tab" href="#8" class="btn" >8</a>
    <a data-toggle="tab" href="#9" class="btn">9</a>
    <a data-toggle="tab" href="#10" class="btn" >10</a>
    <a data-toggle="tab" href="#11" class="btn" >11</a>
    <a data-toggle="tab" href="#12" class="btn" >12</a>
    <a data-toggle="tab" href="#13" class="btn" >13</a>
    <a data-toggle="tab" href="#14" class="btn">14</a>
    <a data-toggle="tab" href="#15" class="btn" >15</a>
    <a data-toggle="tab" href="#16" class="btn">16</a>
    <a data-toggle="tab" href="#17" class="btn">17</a>
    <a data-toggle="tab" href="#18" class="btn">18</a>
    <a data-toggle="tab" href="#19" class="btn">19</a>
    <a data-toggle="tab" href="#20" class="btn">20</a>
	 <hr>
	 <div id="symbols">
	 <a class="btn" style="border: 2px solid #06558e;background-color: #06558e;box-shadow: none;color: #9db6de;height: 19px; width: 17px;margin-top: 9px;" id="display-btn"></a><span class="display-text" id="btn-sample">Current Question</span><br>
	 <a class="btn" style="background-color: rgb(171, 214, 0);
    color: #8a1818;
    box-shadow: none;
    height: 19px;
    width: 17px;
    margin-top: 9px;" id="display-btn"></a><span id="btn-sample" class="display-text">Attempted</span><br>
	 <a class="btn" style="box-shadow: none;height: 19px;width: 17px;margin-top: 9px;" id="display-btn"></a><span id="btn-sample" class="display-text">Not Attempted</span>
	 </div>
	  <hr id="display-btn">
</div>
 </div>
 <div class="col-md-8" id="section-middle">
 <div class="tab-content">
<div class="inner-coding-area">
<article class="post">
<h4 id="top-head">Question <?php echo $question->serial_number;?></h4>
<div class="tab-pane fade" id="1">
<div class="form1">
<form name="qfrm" id="qfrm" action="<?php echo base_url()."braintest/submitanswer"?>" method="post" >
<h4 style="white-space:-moz-pre-space;white-space:pre-line;"><?php echo $question->question_name;?>
</h4>
<div class="mcq-description">
</div>
<ul class="options">
<li id="01">
<a href="#" id="opt">
<span id="tick-icon"><img src="<?php echo assets_url();?>images/checkbox.png" alt="tick"></span>
<span class="option"><span>A</span></span><span class="question-text"><span>Variables</span></span>
</a>
</li>
<li id="02">
<a href="#" id="opt">
<span id="tick-icon"><img src="<?php echo assets_url();?>images/checkbox.png" alt="tick"></span>
<span class="option"><span>B</span></span><span class="question-text"><span>Strings</span></span>
</a>
</li>
<li id="03">
<a href="#" id="opt">
<span id="tick-icon"><img src="<?php echo assets_url();?>images/checkbox.png" alt="tick"></span>
<span class="option"><span>C</span></span><span class="question-text"><span>Data types</span></span>
</a>
</li>
<li id="04">
<a href="#" id="opt">
<span id="tick-icon"><img src="<?php echo assets_url();?>images/checkbox.png" alt="tick"></span>
<span class="option"><span>D</span></span><span class="question-text"><span>Function</span></span>
</a>
</li>
</ul>
<p> <a href="#">Clear Selection</a> </p>
<div class="coding-footer clearfix">
<div class="container-fluid" id="btn-container" style="margin-top: -2px;margin-left: 685px;">
<div class="inner-coding-footer">
<div class="user-actions-block pull-left">
<a href="" class="hvr-underline-from-right" id="button11">Previous</a>
<a class="hvr-underline-from-left" data-toggle="modal" data-target="#error" id="button12">Save &amp; Next </a>
</div>
<a href="#" class="hvr-rectangle" data-toggle="modal" data-target="#submitTest" id="button13">Submit Test</a>
</div>
</div>
</div>
</form>
</div>
</div>
</article>
</div>
<div role="dialog" id="submitTest" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
<h4 class="modal-title">Confirm Test to submit</h4>
</div>
<div class="modal-body">
<p>Please solve all question to submit test</p>
</div>
<div class="modal-footer" style="border-top: 0px;">
<button type="button" class="btn button5" data-dismiss="modal" style="width: auto;padding: 5px;height: 33px;background-color:#172261;color: white;">Confirm</button>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<div role="dialog" id="error" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
<h4 class="modal-title">Submit Error</h4>
</div>
<div class="modal-body">
<p>Please answer the question before submitting. </p>
</div>
<div class="modal-footer" style="border-top: 0px;">
<button type="button" class="btn button5" data-dismiss="modal" style="width: 47px;background-color:#172261;color:white;height:31px;">OK</button>
</div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
</div>
</div>
 <div class="col-sm-2 tab2" id="gate1">
 <div class="right-tab">
 <div>
<span id="remainSideBar" class="time" ><span><img src="<?php echo assets_url();?>images/timecon.png" id="timer-img"></span><span id="hr">00</span>:<span id="min">20</span>:<span id="sec">00</span></span>
<small class="text" id="display-btn">time remaining</small>
</div>
</div>
<hr>
<div class="right-tab">
<div>
<span style="color: white;font-size: 21px;font-weight: 600;" id="display-btn">Total question</span><br>
<span style="color: white;font-size: 20px;text-align:center;margin-left: 66px;" id="display-btn">20</span><br><br>
<span style="color: white;font-size: 21px;font-weight: 600;" class="attempted" id="display-btn">Total Attempted</span><br>
<span style="color: white;font-size: 20px;text-align:center;margin-left: 66px;" id="display-btn">10</span><br>
</div>
</div>
</div>  
</div>
</div>
</section>
<style>
footer{
	    margin-top: -45px;
}
</style>
<script>
function startTimer(duration, min,sec) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        min.text(minutes);
		sec.text(seconds);

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

jQuery(function ($) {
    var fiveMinutes = 60 * 20,
        min = $('#min');
		sec = $('#sec');
    startTimer(fiveMinutes, min,sec);
});
</script>
    <script src="<?php echo assets_url();?>new/assets/bootstrap/js/bootstrap.min.js"></script> 
	<script src="<?php echo assets_url();?>new/assets/jquery/jquery-3.1.1.min.js"></script>
    <script src="<?php echo assets_url();?>new/assets/jquery/jquery.animateNumber.min.js"></script> 
    <script src="<?php echo assets_url();?>new/assets/owl-carousel/js/owl.carousel.js"></script> 
    <script src="<?php echo assets_url();?>new/assets/jquery/plugins.js"></script> 
    <script src="<?php echo assets_url();?>new/js/custom.js"></script>
    <script src="<?php echo assets_url();?>new/js/side-form.js"></script>
</body>
</html>