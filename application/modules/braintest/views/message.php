<?php 
$error_message=$this->session->userdata('error_message');
//print_r(Session::display());
	if($error_message!='') 
	{
?>
	<div class="alert alert-danger alert_pad">
		<?php echo $error_message;?>
	</div>
<?php
	}
	$this->session->unset_userdata('error_message');
?>
<?php 
	$success_message=$this->session->userdata('success_message');
	if($success_message!='') 
	{
?>
	<div class="alert alert-success alert_pad">
		<?php echo $success_message;?>
	</div>
<?php
	}
	$this->session->unset_userdata('success_message');

	$warning_message=$this->session->userdata('warning_message');
	if($warning_message!='') 
	{
?>
	<div class="alert alert-warning alert_pad">
		<?php echo $warning_message;?>
	</div>
<?php
	}
	$this->session->unset_userdata('warning_message');
?>
<?php 

$info_message=$this->session->userdata('info_message');
	if($info_message!='') 
	{
?>
	<div class="alert alert-info alert_pad">
		<?php echo $info_message;?>
	</div>
<?php
	}
	$this->session->unset_userdata('info_message');
?>


<?php	
	if(isset($data['warning_message']) && !empty($data['warning_message']))
	{
	?>
	<div class="alert alert-warning alert_pad">
		<?php echo $data['warning_message'];?>
	</div>
<?php
	
	}
?>