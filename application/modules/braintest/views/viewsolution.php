<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome To</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/font-awesome.css" media="all">
    <!-- own style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/new/css/view-solutions.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header class="view_solutions_hdr">
      <div class="col-md-12">
      	 <div class="col-md-1s"><a href="<?php echo base_url();?>onlinetest/dashboard">Dashboard</a></div>
		 <div class="col-md-9"><h4><i class="fa fa-laptop" aria-hidden="true"></i><?php echo $testdetails->test_name;?></h4>

				

		<span class="total_time"><?php echo $testdetails->time;?>:00 MIN</span>
		<span class="answer_panlel_icon">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</span>
	</div>
	  </div>
    </header>
    <div class="clearfix"></div>
    <section class="vs_wrapper">
      <div class="vs_wrap">
          <div class="vs_wrap_lft">
            <div class="vs_sections">
				
				<div class="tabs">
					<ul class="nav nav-tabs clearfix">
						

                     <?php  $i = 1; foreach ($testcats as  $value) {  ?>

						<li class="<?php if($i == '1') echo 'active'; ?> dropdown"><a data-toggle="tab" onclick="tabac('<?php echo $value->id;?>','<?php echo $this->uri->segment('3');?>','tab<?php echo $i;?>')" href="#tab<?php echo $i?>"><?php echo $value->category_name;?>

								<ul class="dropdown-menu">
								  <li><i class="fa fa-square" aria-hidden="true"></i>Correct<span class="cont"><?php echo $value->correct_q;?></span></li>
								    <li><i class="fa fa-square" aria-hidden="true"></i>Incorrect<span class="cont"><?php echo ($value->attempet1-$value->correct_q);?></span></li>
								 
							
							
							  <li><i class="fa fa-square" aria-hidden="true"></i>Answered<span class="cont"><?php echo $value->attempet1;?></span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>UnAnswered<span class="cont"><?php echo ($value->total_q-$value->attempet1);?></span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>Total<span class="cont"><?php echo $value->total_q;?></span></li>

							
							</ul>
							</a>
						</li>
						<?php $i++;} ?> 
						<!-- <li class="dropdown"><a data-toggle="tab" href="#tab2">Arthamatic
							<ul class="dropdown-menu">
							  <li><i class="fa fa-square" aria-hidden="true"></i>Correct<span class="cont">10</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>Incorrect<span class="cont">20</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>UnAnswered<span class="cont">5</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>Unseen<span class="cont">10</span></li>
							</ul>
							</a>
						</li>
						<li class="dropdown"><a data-toggle="tab" href="#tab3">Reasoning
							<ul class="dropdown-menu">
							  <li><i class="fa fa-square" aria-hidden="true"></i>Correct<span class="cont">10</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>Incorrect<span class="cont">20</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>UnAnswered<span class="cont">5</span></li>
							  <li><i class="fa fa-square" aria-hidden="true"></i>Unseen<span class="cont">10</span></li>
							</ul>
							</a>
						</li> -->
						
					</ul>
					
					<div class="vs_qstns tab-content">
						<div id="tab1" class="tab-pane active">


							 <div class="vs_qstn" id="ajaxq">
								<div class="vs_qstn_num correct_ans"><?php echo $qdetails->serial_number;?></div>
								<div class="vs_qstn_txt">
									<p>
										<?php echo $qdetails->question_name; ?>
									</p>
								</div>
								
								<div class="vs_optns">

                                <ul>
									<?php
									if($qdetails->attempet > 0 ) {
                                       if($qdetails->answer == '') {
									?>    
									<li class="<?php if($qdetails->question_answer == '1') { echo "correct_answer"; }?>">


											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option1; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '2') { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option2; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '3') { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option3; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '4' ) { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option4; ?></span>
										</li>
									<?php } else { ?>                      

											<?php if($qdetails->question_answer == $qdetails->answer ) { ?>

											<li class='<?php if($qdetails->question_answer == '1' && $qdetails->answer == '1') { echo "both_correct";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option1; ?></span>
										</li>
										
										
											<li class='<?php if($qdetails->question_answer == '2' && $qdetails->answer == '2') { echo "both_correct";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option2; ?></span>
										</li>
										
											
											<li class='<?php if($qdetails->question_answer == '3' && $qdetails->answer == '3') { echo "both_correct";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option3; ?></span>
										</li>
										
										
											<li class='<?php if($qdetails->question_answer == '4' && $qdetails->answer == '4') { echo "both_correct";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option4; ?></span>
										</li>
											<?php } else {?>

									
										<li class='<?php if($qdetails->answer == '1') { echo "wrong_answer";} else if($qdetails->question_answer == '1') { echo "correct_answer";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option1; ?></span>
										</li>
										
										<li class='<?php if($qdetails->answer == '2') { echo "wrong_answer";} else if($qdetails->question_answer == '2') { echo "correct_answer";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option2; ?></span>
										</li>
										
										<li class='<?php if($qdetails->answer == '3') { echo "wrong_answer";} else if($qdetails->question_answer == '3') { echo "correct_answer";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option3; ?></span>
										</li>
										
										<li class='<?php if($qdetails->answer == '4') { echo "wrong_answer";} else if($qdetails->question_answer == '4') { echo "correct_answer";}?>' >
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option4; ?></span>
										</li>
										<?php } } }  else { ?>


										<li class="<?php if($qdetails->question_answer == '1') { echo "correct_answer"; }?>">


											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option1; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '2') { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $qdetails->option2; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '3') { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option3; ?></span>
										</li>
										
										<li class="<?php if($qdetails->question_answer == '4' ) { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option4; ?></span>
										</li>
										<?php if($qdetails->option5!='') { ?>

										<li class="<?php if($qdetails->question_answer == '5' ) { echo "correct_answer"; }?>">
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $qdetails->option5; ?></span>
										</li>
										<?php } ?>

										<?php } ?>
									</ul>
								</div>
								<?php if($qdetails->question_explanation  !=''){ ?>
								<div class="vs_soln_exp">
									<h4>Explanation:</h4>
									<p><?php echo $qdetails->question_explanation; ?><br>
									<!-- 	<b>Ans : 2411</b>
 -->
									</p>
								</div>
								<?php } ?>
								<input type="hidden" name="pqid"  id="pqid" value="<?php echo $qdetails->q_id;?>">
								
								<div class="vs_wrap_btm">

													<?php if($qdetails->video_link  !=''){ ?>
								  	<button class="pull-left btn btn-default watch_video_solution  openPopup" style="display: block;" data-toggle="modal" data-target="#video_solution_popup">Video Solution</button>
								  	<?php } else { ?>
								  	  	<button class="pull-left btn btn-default watch_video_solution  openPopup" style="display: none;" data-toggle="modal" data-target="#video_solution_popup">Video Solution</button>
								  	<?php } ?>


				<div class="vs_actin_btns">

					
					<?php  if($qdetails->serial_number>1) {	?>

					<button class="btn btn-default" onclick="getq('<?php echo $qdetails->test_id;?>','<?php echo $qdetails->cat_id;?>','<?php echo $qdetails->serial_number-1;?>','<?php echo $value->id;?>')">Previous</button>
					<?php } ?>

					<?php if($qdetails->serial_number<count($allkeys)) { ?>

					<button class="btn btn-default" onclick="getq('<?php echo $qdetails->test_id;?>','<?php echo $qdetails->cat_id;?>','<?php echo $qdetails->serial_number+1;?>','<?php echo $value->id;?>')"> Next</button>
					<?php } ?>
				</div>
				
				
			  </div>



							
							 </div>
							  <div class="vs_wrap_rht">
              <h4><?php echo $testcats[0]->category_name;?><span class="tme_taken">00:00 MIN</span></h4>
              <div class="panel_options">
                <ul>
                  <li><i class="fa fa-square" aria-hidden="true"></i><span class="cont"><?php echo $testcats[0]->correct_q;?></span></li>
                  <li><i class="fa fa-square" aria-hidden="true"></i><span class="cont"><?php echo ($testcats[0]->attempet1-$testcats[0]->correct_q);?></span></li>
                  <li><i class="fa fa-square" aria-hidden="true"></i><span class="cont"><?php echo ($testcats[0]->attempet1);?></span></li>
                  <li><i class="fa fa-square" aria-hidden="true"></i><span class="cont"><?php echo ($testcats[0]->total_q-$testcats[0]->attempet1);?></span></li>
                  <li><i class="fa fa-square" aria-hidden="true"></i><span class="cont"><?php echo ($testcats[0]->total_q);?></span></li>
                  <div class="clearfix"></div>
                </ul>
              </div>

              <div class="panel_answers_nms">
                <ul>

                	<?php $i=1; foreach ($allkeys as $value) { ?>

                	<?php
                   if($value->answer !='' && $value->answer !='0'){
                        if($value->correct == '1'){
                            $class= 'correct_ans';
                        }else {
							$class= 'wrong_ans';
                        }
                   }else {
                   
                   $class= '';
                         
                   }
                	?>
                      
                      <li onclick="getq('<?php echo $value->test_id;?>','<?php echo $value->cat_id;?>','<?php echo $value->serial_number;?>','<?php echo $value->id;?>')" class="current_qs <?php echo $class;?>" id="<?php echo $value->q_id;?>"><?php echo $i;?></li>


                      <?php $i++; }  ?>



				  
                  <div class="clearfix"></div>
                </ul>
              </div>
            </div>  
						</div>
						<div id="tab2" class="tab-pane">
							<div class="vs_qstn" id="ajaxq1">
								
						
							 </div>
							  
						</div>
						<div id="tab3" class="tab-pane">
							 <div class="vs_qstn" id="ajaxq">
								

             
            </div>  
					</div>
				
			
				
              </div>
			  
			  
			  
			
            </div>
              
          </div>
		  
		  
        </div>
    </section>


    <!-- video solution popup -->

 <div class="modal fade solution_video_popup" id="video_solution_popup" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
                <div class="solution_video_container">
                	<div class="solution_video_iframe">
                		
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>


   <script>



   $(document).ready(function(){
    $('.openPopup').on('click',function(){   
        var id = $("#pqid").val();
       // alert(id);
       $('.solution_video_iframe').load('<?php echo base_url();?>braintest/getVideoSol/'+id,function(){
            $('#video_solution_popup').modal({show:true});
        });
    }); 
    $('.solution_video_popup .close').click(function() {
	  var embedCode = '';
		$('.solution_video_iframe').html(embedCode);  
	});
});


	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});

	var a;

	function tabac(cid,tid,tab){

    $(".tab-pane").removeClass("active");
    $('#'+tab).addClass("active"); 
	var id = $("#pqid").val();
    alert(id);

	//alert(tab);
	   $.ajax({
       url:'<?php echo base_url(); ?>braintest/getcatinfo',
       type: 'POST',
       datatype:'application/json',
       data: 'cid='+cid+'&tid='+tid,
       success:function(data){
       	//alert(data);
       

         $('#'+tab).html(data);
       }
      
});
	}

	
		function getq(tid,cid,qid){

			var embedCode = '';
		$('.solution_video_iframe').html(embedCode);    
		 $('#video_solution_popup').modal({show:false});

			//alert("ff");
//alert(tab);
	   $.ajax({
       url:'<?php echo base_url(); ?>braintest/singleQuestion',
       type: 'POST',
       datatype:'application/json',
       data: 'cid='+cid+'&tid='+tid+'&qid='+qid,
       success:function(data){
       	    

         $('#ajaxq').html(data);
       }
      
});
	}
	</script>
	
	<!-- scroller -->
	
	
	<script type="text/javascript">
		$(window).resize(function(){
			$('.vs_qstns').height($(window).height()-230);

		});
		$(window).trigger('resize');
	</script>

	
	<script>
	
		
		$('.answer_panlel_icon').click(function() {
			$('.vs_wrap_rht').animate({
				width: 'toggle'
			});
			
			if ($('.answer_panlel_icon').hasClass("clicked")) {
				$('.answer_panlel_icon').removeClass('clicked');
				$('.vs_wrap_lft').css('padding-right','320px');
			} else {
				$('.answer_panlel_icon').addClass('clicked');
				$('.vs_wrap_lft').css('padding-right','0px');

			}
			
			
		});
		
		

</script>

  </body>
</html>