<div class="btwrapper">
    <header class="brainwiz-header exam_page_hdr clearfix">
	<?php 
	if(isset($timeleft) && $timeleft>0) {
	?>

	<div class="pull-left col-md-3 col-sm-4 brk_time_col">
		<h3 class="brk_tme_txt">Its Break Time</h3>
        <div class="timer timer_count">
			<div id="CountDownTimer" data-timer="<?php echo $timeleft;?>" data-tc-id="c57b9366-5660-e6f4-bf70-d8e29fc5e6a8">
				<div class="time_circles">
					
					<div class="textDiv_Minutes">
						<h4>Mins</h4>
						<span>2</span>
					</div>
					<div class="textDiv_Seconds">
						<h4>Secs</h4>
						<span>0</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pull-right col-md-4 col-sm-6 wel_rht brk_tme_rht">
	<?php } ?>
	    <div class="pull-right col-md-4 col-sm-6 wel_rht">
    	<p class="text-right text-info test_name"><?php echo $this->session->userdata('testname');?></p>
        <p class="text-right test_person_nme"><i class="fa fa-user" aria-hidden="true"></i>Welcome <?php echo $this->session->userdata('examusername');?></p>

    </div>

  </header>




 <div class="clearfix"></div>
  <div class="btinnerWrapper blue-border">
    <div class="container-fluid">
    	
		 <?php $this->load->view('message'); ?>
		<div class="clearfix"></div></br>
      <div class="btn-group" role="group" aria-label="...">
	  <?php
		
		if(isset($categories))
		{
			foreach($categories as $cat)
			{
				$buttunClass="btn-default";
				if($cat->complete==1)
					$buttunClass="btn-success";
				?>
				      <button type="button" class="btn <?php echo $buttunClass;?> btn-lg categoryBtn"  onClick="getUrlTest('<?php echo base_url()."braintest/brainteststart/".$cat->id; ?>')"><?php echo $cat->category_name; ?></button>
					 
				<?php
				

			}
		}
		
	  ?>
      
    </div>
      <div class="table-features">
	  <p><b>Instructions:</b></p>
<ol>
<li>Test consists of <?php echo count($categories);?> sections i.e. <?php foreach($categories as $cat){echo $cat->category_name.", ";}?></li>
<!-- <li>These section will be of <?php echo $testdetails->duration/count($categories) ;?> mins each.</li> -->
<li>After finishing each section you will have a break time of <?php echo $testdetails->break_time; ?> min.</li>
<li>Once the section is completed you cannot go back to that section.</li>
<li>You can chose the section you want to attempt from Instruction page(current).</li>
<li>Section can be selected from the top navigation tab.</li>
<li>Once you click on compete button a confirmation will be asked whether you want to complete the section or not.</li>
<li>Each question will be of 1 mark.</li>
<li>You will not have multiple choice questions.</li>
<li>You need to click on <b>"Next/ Save "</b> button on order to save your marked answer.</li>
<li>Result will be displayed immediately after you complete all the three sections.</li>
</ol>
<br/>
All The Best.
<br/>
<p><b>BRAINWIZ</b>
	  </p>
    </div>
    </div>
  </div>
  