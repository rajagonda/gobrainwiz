

<!DOCTYPE html>
<html>
<head>
<title>Hire the best developers</title>
<link rel="shortcut icon" href="https://www.techgig.com/favicon.ico" type="image/x-icon"/>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="author" content="www.Techgig.com"/>
<meta name="description" content="Tech Community, Project Networking, Tech News, IT Software, Hardware, Technology Jobs"/>
<meta name="keywords" content="Tech Community, Project Networking, Tech News, IT Software, Hardware, Technology Jobs"/>
<meta name="robots" content="index, follow"/>
<meta name="Author" content="https://www.techgig.com/"/>
<meta name="copyright" content="Techgig.com"/>
<meta name="rating" content="safe for kids"/>
<meta name="googlebot" content="index, follow"/>
<meta name="yahooSeeker" content="index, follow"/>
<meta name="msnbot" content="index, follow"/>
<meta name="reply-to" content="customercare@techgig.com"/>
<meta name="allow-search" content="yes"/>
<meta name="revisit-after" content="daily"/>
<meta name="distribution" content="global"/>
<meta name="Rating" content="General"/>
<meta name="expires" content="never"/>
<meta name="language" content="english"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="tpngage:name" content="SSM4483823622137230625TIL"/>
<meta name="google-site-verification" content="v4MHwT9OV0R4GjUsvklVC84QhzrhiYmesC7an3We64E"/>
<!-- Javascript Variables required for the project in any section -->
<script type="text/javascript" language="javascript">var base_url;base_url="https://www.techgig.com";var saas_prefix;saas_prefix="recruit";var skin_path;skin_path="https://www.techgig.com/Themes/Release";var THEME_PATH;THEME_PATH="https://www.techgig.com/Themes/Release";var login_uid;login_uid="1912838";var assessment_url="assessment";var loadAceJs=false;var contest_saas_prefix;contest_saas_prefix="";</script>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/ace-builds/src-noconflict/ace.js" language="javascript"></script>
<!-- Css file Includes section (Only Important Ones go here.. ) -->
<link href="https://www.techgig.com/Themes/Release/bootstrap-3.3.7.min.css" rel="stylesheet" type="text/css">
<link href="https://www.techgig.com/Themes/Release/tg_common_v11.css" rel="stylesheet" type="text/css">
<link href="https://www.techgig.com/Themes/Release/tg_main_v10.css" rel="stylesheet" type="text/css">
<!-- Javascript file Includes section (Only Important Ones go here.. ) -->
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/jquery-1.12.2.min.js" language="javascript"></script>
</head>
<body class="no-padding-top">
<!-- GA TAG added just after Body tag as recommendation. Earlier it was present inside head tag -->
<script type="text/javascript">var _gaq=_gaq||[];_gaq.push(['_setAccount','UA-1688637-14']);_gaq.push(['_trackPageview']);(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src='https://www.techgig.com/_third_party_js/dc.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})();(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.techgig.com/_third_party_js/analytics.js','ga');ga('create','UA-46012303-7','auto');ga('send','pageview');</script>
<!-- crazzy egg -->
<script type="text/javascript">setTimeout(function(){var a=document.createElement("script");var b=document.getElementsByTagName("script")[0];a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0066/1644.js?"+Math.floor(new Date().getTime()/3600000);a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)},1);</script>
<style type="text/css">body>iframe{position:absolute}</style>
<!-- Browser push Notifications by Notifyvisitors -->
<link rel="manifest" href="https://www.techgig.com/notifyvisitors_push/chrome/manifest.json">
<!-- Message Box -->
<div class="msgErrortop">
<div class="message-box clearfix">
<a href="#" class="close">x</a>
<span class="alert-message-icon">
<i class="fa fa-check" aria-hidden="true"></i>
<i class="fa fa-times" aria-hidden="true"></i>
<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
<i class="fa fa-info-circle" aria-hidden="true"></i>
</span>
<div class="alert-message-content">
<h6 class="message-header">
<span class="error-txt">Error</span>
<span class="success-txt">Success</span>
<span class="warning-txt">Please Note</span>
<span class="info-txt">Info</span>
</h6>
<p></p>
</div>
</div>
</div>
<!-- /Message Box -->
<div id="container-wrap" class="loggedin ">
<!--[if lt IE 9]>
<link href="https://www.techgig.com/Themes/Release/techgig_ie8_specific.css" rel="stylesheet" type="text/css">
<![endif]-->
<!-- Main page content section going here -->
<div id="TechGigbootStrapModal" class="modal fade" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content"><button aria-label="Close" data-dismiss="modal" class="close" type="button"></button>
<div class="modal-header">
<h4 class="modal-title"> </h4>
</div>
<div class="modal-body"> </div>
</div>
</div>
</div>
<div id="TechGigbootStrapModal" class="modal fade" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content"><button aria-label="Close" data-dismiss="modal" class="close" type="button"></button>
<div class="modal-header">
<h4 class="modal-title"> </h4>
</div>
<div class="modal-body"> </div>
</div>
</div>
</div>
<script type="text/javascript">$(document).ready(function(){Tg_CommonFunction.init();});</script>
<!-- Main page content section going here -->
<!-- Header -->
<script type="text/javascript">setCountDown();var remaining_time=1345;var days=0;var hours=0;var minutes=22;var seconds=25;if(remaining_time<=0){$('#codejudge_requirement').submit();}function setCountDown(){if(days==0&&hours==0&&minutes==0&&seconds==0){$("#auto_submit_code").val('Y');bootbox.alert({title:"Contest time is over",message:"<p class='alert alert-warning'>Warning : Contest time is over. Click OK to see the result</p>",callback:function(result){if($("#defaultCode").length>0){$('#submit_code input').trigger("click");}$('#codejudge_requirement').submit();}});}else{seconds--;if(seconds<0){minutes--;seconds=59}if(minutes<0){hours--;minutes=59}if(hours<0){days--;hours=23}if(days==0){if(minutes<10){minutes=('0'+minutes).slice(-2);}if(hours<10){hours=('0'+hours).slice(-2);}if(seconds<10){seconds=('0'+seconds).slice(-2);}$('#remain').html(hours+":"+minutes+":"+seconds);$('#remainSideBar').html(hours+":"+minutes+":"+seconds);}else{if(days<10){days=('0'+days).slice(-2);}if(hours<10){hours=('0'+hours).slice(-2);}if(minutes<10){minutes=('0'+minutes).slice(-2);}if(seconds<10){seconds=('0'+seconds).slice(-2);}$('#remain').html(days+" day and "+hours+":"+minutes+":"+seconds);$('#remainSideBar').html(days+" day and "+hours+":"+minutes+":"+seconds);}SD=window.setTimeout("setCountDown()",1000);if(days==00&&hours==00&&minutes==00&&seconds==00){seconds=0;window.clearTimeout(SD);$("#auto_submit_code").val('Y');bootbox.alert({title:"Contest time is over",message:"<p class='alert alert-warning'>Warning : Contest time is over. Click OK to see the result</p>",callback:function(result){if($("#defaultCode").length>0){$('#submit_code input').trigger("click");}$('#codejudge_requirement').submit();}});}}}function disableCtrlKeyCombination(e){var forbiddenKeys=new Array('a','n','x','j','w');var key;var isCtrl;if(window.event){key=window.event.keyCode;if(window.event.ctrlKey)isCtrl=true;else
isCtrl=false;}else{key=e.which;if(e.ctrlKey)isCtrl=true;else
isCtrl=false;}if(isCtrl){for(i=0;i<forbiddenKeys.length;i++){if(forbiddenKeys[i]==String.fromCharCode(key)){var key_ctrl_disable='Key combination CTRL + '+String.fromCharCode(key)+' has been disabled.';$('#disable_ctrl_key_combination .modal-body p').html(key_ctrl_disable);$('#disable_ctrl_key_combination').modal('show');return false;}}}return true;}function clickIE(){if(document.all){$('#disable_ctrl_key_combination .modal-body p').html('Right click disabled');$('#disable_ctrl_key_combination').modal('show');}}function clickNS(e){}if(document.layers){document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}document.oncontextmenu=new Function("return false");function disableSelection(target){if(typeof target.onselectstart!="undefined")target.onselectstart=function(){return false}
else if(typeof target.style.MozUserSelect!="undefined")target.style.MozUserSelect="none"
else
target.onmousedown=function(){return false}
target.style.cursor="default"}$(document).ready(function(){var category_key=$("#category_key_problem").val();if(category_key!='approximate_solution'){disableSelection(document.body);}});</script>
<div id="coding-platform-head" class="clearfix">
<div>
<!-- Logo -->
<div class="logo">
<div class="inner">
<a href="https://www.techgig.com/challenge/csharp/c-easy-level"><img src="https://www.techgig.com/Themes/Release/images//tg_images/logo.png" alt="logo"></a>
</div>
</div>
<h5>C# (Easy Level)</h5>
<span class="big hide ">
Time Remaining -
<span id="remain">
00:00:00
</span>
</span>
<div class="attempt-count hide">Questions Attempted - <span> <span id="attempt_question_cnt">0</span>/25 </span> </div>
</div>
</div>
<form method="post" action="https://www.techgig.com/skilltest/result/csharp/c-easy-level/WHlWVV5bVw==" id="codejudge_requirement" name="codejudge_requirement" style="display:none">
<h2 id="submit_msg"></h2>
<input type="submit" value="I'm Done with the Test">
</form>
<div class="modal fade" id="confirm-test-close" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button"></button>
<h4 id="mySmallModalLabel" class="modal-title">Submit the test</h4>
</div>
<div class="modal-body">
<p>Once closed, you can no longer view or modify this test. Are you sure you are done, and want to close the test?</p>
</div>
<div class="modal-footer">
<a href="javascript:void(0)" class="btn button1" onclick="javascript:$('#codejudge_requirement').submit();">Yes, Submit the test</a>
<button data-dismiss="modal" class="btn button7" type="button">No, do not Submit</button>
</div>
</div>
</div>
</div>
<!-- /Header -->
<div id="body_user_disabled" style="display:none;">
</div>
<script></script>
<input type="hidden" id="warning_count" value="0">
<input type="hidden" id="test_id" value="302">
<input type="hidden" id="test_attempt_id" value="4952863">
<div class="modal fade" id="incomplete-test-close" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button"></button>
<h4 id="mySmallModalLabel" class="modal-title">Confirm test close</h4>
</div>
<div class="modal-body">
<p>Please solve the question(s) to submit the test.</p>
</div>
</div>
</div>
</div>
<div class="modal fade" id="disable_ctrl_key_combination" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button"></button>
<h4 id="mySmallModalLabel" class="modal-title">Key Disabled</h4>
</div>
<div class="modal-body">
<p></p>
</div>
<div class="modal-footer">
<button data-dismiss="modal" class="btn button1" type="button">OK</button>
</div>
</div>
</div>
</div>
<div id="coding-platform" class="clearfix">
<div id="question-controller">
<!-- Question Controller -->
<div class="question-controller widget-block">
<h5>Questions</h5>
<div class="questions-list">
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/1" class="btn btn-xs ">1</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/2" class="btn btn-xs ">2</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/3" class="btn btn-xs ">3</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/4" class="btn btn-xs ">4</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/5" class="btn btn-xs ">5</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/6" class="btn btn-xs ">6</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/7" class="btn btn-xs ">7</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/8" class="btn btn-xs ">8</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/9" class="btn btn-xs ">9</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/10" class="btn btn-xs ">10</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/11" class="btn btn-xs ">11</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/12" class="btn btn-xs ">12</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/13" class="btn btn-xs ">13</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/14" class="btn btn-xs ">14</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/15" class="btn btn-xs ">15</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/16" class="btn btn-xs ">16</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/17" class="btn btn-xs ">17</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/18" class="btn btn-xs ">18</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/19" class="btn btn-xs ">19</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/20" class="btn btn-xs ">20</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/21" class="btn btn-xs ">21</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/22" class="btn btn-xs ">22</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/23" class="btn btn-xs ">23</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/24" class="btn btn-xs ">24</a>
<a href="https://www.techgig.com/challenge/question/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/25" class="btn btn-xs ">25</a>
</div>
</div>
<!-- /Question Controller -->
<!-- Legends Info -->
<div class="legends-info widget-block">
<ul>
<li class="skipped"><span class="icon"></span>Current Question</li>
<li class="attempted"><span class="icon"></span>Attempted</li>
<li class="not-attempted"><span class="icon"></span>Not attempted</li>
</ul>
</div>
<!-- /Legends Info -->
<!-- Legends Info -->
<!-- /Legends Info -->	</div>
<div id="coding-content-area">
<div id="candidate_question_listing">
<div class="table-responsive">
<table class="table2">
<thead>
<tr>
<th></th>
<th class="text-left">Question</th>
<th>Status</th>
<th></th>
</tr>
</thead>
<tbody>
<tr>
<td class="question-number">Q1</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/1">C# does not support which of the following statement? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/1" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q2</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/2">Which of the following is the application of C#? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/2" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q3</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/3">A class in a namespace can be accessed using which of the following op... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/3" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q4</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/4">_______is used in replacement patterns. </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/4" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q5</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/5">______ is defined as the execution path of a program that defines a un... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/5" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q6</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/6">_____ contains the executable statement that helps in getting or setti... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/6" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q7</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/7">_____ is a programming paradigm that melds a declarative approach with... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/7" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q8</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/8">____is the locations in memory of computer that are reserved for stori... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/8" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q9</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/9">The process of converting an object into a stream of bytes is called__... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/9" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q10</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/10">How will you access member function of a class? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/10" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q11</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/11">How will you define call by reference parameter in C# .NET? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/11" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q12</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/12">Which of the following datatype is not used with Enum in c#? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/12" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q13</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/13">How will you access the members of base class are accessible to derive... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/13" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q14</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/14">The indexer uses which of the following keyword for its declaration? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/14" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q15</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/15">FileStream defines which of the following method that reads bytes from... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/15" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q16</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/16">Which of the following backreference construct matches the value of a ... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/16" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q17</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/17">Which is the runtime unit of isolation in which a .NET program that<br/>
r... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/17" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q18</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/18">Which of the following is an ordered collection class?<br/>
Map, stack, que... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/18" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q19</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/19">What is virtual keyword in c# </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/19" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q20</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/20">Name the software which is used to create the c# programs? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/20" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q21</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/21">Name the integrated environment that executes applications using .NET ... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/21" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q22</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/22">Which information contains four parts: major version, minor version, b... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/22" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q23</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/23">Name the tool that enables user to develop user friendly desktop based... </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/23" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q24</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/24">Name the function which is used to display the output on the screen? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/24" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
<tr>
<td class="question-number">Q25</td>
<td class="text-left"><p><a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/25">Name the operator used to access class in a namespace? </a></p></td>
<td>
<span class="status-icon"></span>
</td>
<td>
<a href="https://www.techgig.com/skilltest/question/csharp/c-easy-level/MzAyQCMkQCMkMTkxMjgzOEAjJEAjJDQ5NTI4NjNAIyRAIyQxNDk5ODM5MTc3/25" class="btn button4 action-btn">Solve Question</a>
</td>
</tr>
</tbody>
</table>
</div>
<footer class="coding-footer text-center">
<a href="javascript:void(0)" class="btn button1 btn-lg" data-toggle="modal" data-target="#incomplete-test-close">I am done with the test <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
</footer>
</div>
<script>$(document).ready(function(){Tg_CommonFunction.init();});</script>
</div>
<div id="user-attentions">
<!-- Remaining Time -->
<div class="widget-block text-center">
<div class="progress">
<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
</div>
</div>
<div>
<span id="remainSideBar" class="big">0:22:25</span>
<small>time remaining</small>
</div>
</div>
<!-- /Remaining Time -->
<!-- Pending Attempt -->
<div class="widget-block text-center">
<div class="progress">
<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
</div>
</div>
<div id="pending-attempt">
<span class="big">0/25</span>
<small>Questions Attempted</small>
</div>
</div>
<!-- /Pending Attempt -->
</div>
</div>
<!-- Forgot Password Block -->
<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content form1">
<form action="" method="post" name="reset_pwd">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
<h4 class="modal-title">Forgot Password</h4>
</div>
<div class="modal-body form1">
<fieldset class="">
<label>Please enter your email to reset your password</label>
<ul>
<li>
<input type="text" name="reset_email" id="reset_email" placeholder="Enter your email" class="form-control"/>
<span style="display:none" id="err_reset_email" class="error_msg">Please provide valid email ID!</span>
</li>
</ul>
<input type="button" id="reset_submit_btn" class="btn button1 default-submit-btn btn-lg" value="Submit" onclick="return Tg_CommonFunction.reSetPassword();"/>
</fieldset>
</div>
</form>
</div>
</div>
</div>
<!-- Forgot Password Block -->
</div>
<!---webcam section ------>
<!---eof webcam section ------>
<!--  Proctoring and Moving Out of Tab Block Ends Here -->
<!--  Proctoring and Moving Out of Tab Block Ends Here -->
<!-- interview disable button --->
<!-- CSS Includes section -->
<link href="https://www.techgig.com/Themes/Release/font-awesome-4.7.0.min.css" rel="stylesheet" type="text/css">
<!-- Javascript file Includes section (Only Important Ones go here.. ) -->
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/bootstrap-3.3.7.min.js" language="javascript"></script>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/saas_main_v1.js" language="javascript"></script>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/bootbox.min.js" language="javascript"></script>
<script type="text/javascript">var editor=ace.edit("editor");var headditor=ace.edit('split-editor');var taileditor=ace.edit('split-tail-editor');</script>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/techgig_common_main_v7.js" language="javascript"></script>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/techgig_new_main_v7.js" language="javascript"></script>
<div id="page-overlay1">
<p><img src="https://www.techgig.com/Themes/Release/images//TG-Loader.gif" alt="loader"></p>
</div>
<script type="text/javascript" src="https://www.techgig.com/Themes/Release/javascript/deep-link.js" language="javascript"></script>
</body>
</html>
<script>window.fbAsyncInit=function(){FB.init({appId:'206991022695378',cookie:true,xfbml:true,version:'v2.8'});};(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://www.techgig.com/_third_party_js/sdk.js";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));function testAPI(return_url){FB.api('/me?fields=email,name',function(response){var action_url=base_url+'/ajax_files/saas_candidate_function.php?action=social_fb_login';$.ajax({type:"POST",url:action_url,data:{response_data:response},success:function(data){data=$.trim(data);var msg=$.parseJSON(data);if(msg.status='success'){if(return_url){window.location.href=return_url+'&auto_login='+msg.auto_login+'&login_through_social=Y';}else{window.location.href=base_url+'?auto_login='+msg.auto_login;}}else{window.location.href=base_url+'?msg=unable to facebook login due to some technichal error! try other social login';}}});});}function fb_login(return_url){FB.login(function(response){if(response.status==='connected'){testAPI(return_url);}},{scope:'public_profile,email'});}</script>
