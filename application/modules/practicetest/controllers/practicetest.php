<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class practicetest extends MY_Controller {
        public function __construct() {
           parent::__construct();
           $this->load->library(array('template','form_validation'));
           date_default_timezone_set('Asia/Kolkata');
           $this->load->helper('download');
           $this->load->model('practicetest_model');
          
            
         

        }
        public function index($id=NULL)	{

           $data['cats'] = $this->practicetest_model->getCats();
           $catId = $data['cats'][0]->id;
           $data['topics'] = $this->practicetest_model->get_cats_topics();
           $this->template->theme('pract',$data);
	   }
        public function gettopics() {
          $cid = $this->input->post('cid');
           $data['topics'] = $this->practicetest_model->getalltopics($cid);
            $this->load->view('ajaxdata',$data);
        }
        public function gettopicTests() {

           $topic = $this->input->post('topicId');
           $ids = explode('t', $topic);
           if($ids['1'] !=''){
              $data['topicInfo'] = $this->practicetest_model->gettopicInfo($ids['1']);
              $data['tests'] = $this->practicetest_model->gettopicspapers($ids['1']);

              foreach ($data['tests'] as  $value) {
                $data['testQ'][$value->test_id] = $this->practicetest_model->getQnum($value->test_id);
              }
            //  echo "hello";exit;
              $this->load->view('ajaxtestdata',$data);
           }                        
        }
        public function testStart($tid){

        if(!$this->session->userdata('user_login') == '1'){
              $this->session->set_userdata('url','practicetest');
            redirect(base_url()."onlinetest/login");
        }


          $uid =$this->session->userdata('studentId');   
          $res = $this->practicetest_model->gettestresult($tid, $uid);
         
          if($res !='') {
             redirect(base_url()."practicetest");  
          }
           $data['testdetails'] = $this->practicetest_model->gettestdetails($tid);
          $data['testquestions'] = $this->practicetest_model->testquestions($tid);
           $data['quetionscount'] = count($data['testquestions']);
          $this->load->view('testQuestions', $data);
        }
        public function teststart1($tid) {
            
           //$uid =$this->session->userdata('buser_id');
            $uid =$this->session->userdata('studentId');   
          $res = $this->practicetest_model->gettestresult($tid, $uid);
         
          if($res !='') {
             redirect(base_url()."practicetest");  
          }
            
           $data['testdetails'] = $this->practicetest_model->gettestdetails($tid);
           $data['testquestions'] = $this->practicetest_model->testquestions($tid);
           if($data['testquestions'] !='') {
           $data['quetionscount'] = count($data['testquestions']);
           }else {
               $data['quetionscount'] = 0;
           }
           //echo "<pre>"; print_r($data); exit;
            $data['c'] = $this->challengeroftheday();
           $this->template->theme('testinfo', $data);
        }
        public function practiceteststart1($tid, $topicid) { 
            
          $uid =$this->session->userdata('studentId');   
          $res = $this->practicetest_model->gettestresult($tid, $uid);
         
          if($res !='') {
             redirect(base_url()."practicetest");  
          }
           //$this->template->set_layout('testtemplate');
           $data['testdetails'] = $this->practicetest_model->gettestdetails($tid);
           $data['testquestions'] = $this->practicetest_model->testquestions($tid);
           foreach($data['testquestions'] as $value) {
          //     echo $value->passage_id; exit;
           if($value->passage_id !='') {
              $data['passage'][$value->q_id] = $this->practicetest_model->getpassagename($value->passage_id);
           }
           }
           $data['quetionscount'] = count($data['testquestions']);
         //  echo $data['testdetails']->test_time;
           //echo "<br>";
          // echo strtotime($data['testdetails']->test_time);
         // echo "<pre>"; print_r($data); exit;
           $this->template->exam_theme('managetestquestions', $data);
           
        }
        public function submitquestions() {
            
             $uid =$this->session->userdata('studentId');
             $tid =  $this->input->post('test_id');
             
             $this->practicetest_model->checktestresult($uid,$tid);
             $this->practicetest_model->checktestinfo($uid,$tid);
             
             
             
            //echo "<pre>"; print_r($_POST); exit;
           $dataIn['test_id'] = $this->input->post('test_id');
           $tid = $this->input->post('test_id');
           $dataIn['user_id'] = $uid;
           $data['question'] = $this->input->post('question_id');
           $data['checked'] = $this->input->post('checked');
           for($i = 0; $i < count($data['question']); $i++) {
             $answerString = "opt_" . $data['question'][$i];
             $dataIn['q_id'] = $data['question'][$i];
           if($data['checked'][$i] != '') {
             $dataIn['attempet'] = $data['checked'][$i];
           } else {
             $dataIn['attempet'] = 0;
           }
           if($this->input->post($answerString) != '') {
             $dataIn['answer'] = $this->input->post($answerString);
           } else {
             $dataIn['answer'] = 'no result';
           }
           // echo "<pre>"; print_r($dataIn);
           $this->practicetest_model->insertanswerdetails($dataIn);
           }
           $correctCount = "";
           $wrongAnswers = "";
           foreach ($data['question'] as $questionId) {
                $answerString = "opt_" . $questionId; // To get related answer
                $answer = $this->input->post($answerString);
                $qid = $questionId;

                $res =  $this->practicetest_model->getquetiondetails($answer,$qid,$tid);
                $correctCount += $res->num;
           }  
           if(in_array(1,$data['checked'])){
           $checkedCount=  count(array_filter($data['checked']));
           }else {
               $checkedCount = 0;
           }
           if($checkedCount !=0){
             $shj = $checkedCount;
           } else {
             $shj = 0;
           }
          // echo $shj; exit;
           
            $resultData['test_id'] = $tid;
            $resultData['user_id'] = $uid;
            $resultData['total_q'] = count($data['question']);
            $resultData['attempted_q'] = $shj;
            $resultData['notattempetd_q'] = count($data['question'])-$shj;
            $resultData['correct_q'] = $correctCount;
            $resultData['wrong_q'] = $shj- $correctCount;
            $resultData['test_date'] = date("Y-m-d");
            $resultData['total_data'] = serialize($_POST);
            $resultData['marks'] = $correctCount;
           // echo "<pre>"; print_r($resultData); exit;
            $this->practicetest_model->inserttestresult($resultData);
           redirect(base_url()."practicetest/testresultview/".$tid);  
          
           
        }
        public function testresultview($tid) {
            $uid =$this->session->userdata('studentId');
             
             $data['testresult'] = $this->practicetest_model->gettestresult($tid, $uid);
             $data['testdetails'] = $this->practicetest_model->gettestdetails($tid);
             
             //echo "<pre>"; print_r($data);exit;
             $this->template->theme('scorecard', $data);
        }
        public function showanswerkey($tid) {
             $uid =$this->session->userdata('studentId');
             
             $data['testresult'] = $this->practicetest_model->gettestresultkeys($tid, $uid);
             if($data['testresult'] !=''){
              $data['testdetails'] = $this->practicetest_model->gettestdetails($tid);
             foreach($data['testresult'] as $value) {
                 $data['answer'][$value->id]  = $this->practicetest_model->getquestionanswers($value->q_id);
                 
           }
            $data['testresultss'] = $this->practicetest_model->gettestresult($tid, $uid);
            // echo "<pre>"; print_r($data);exit;
             $data['quetionscount'] = count($data['testresult']);
             $this->load->view('solutions', $data);
           }else {
             redirect(base_url()."practicetest");  
           }
        }
        
        public function showanswerkey1($tid) {
             $uid =$this->session->userdata('studentId');
             
             $data['testresult'] = $this->practicetest_model->gettestresultkeys($tid, $uid);
             foreach($data['testresult'] as $value) {
                 $data['answer'][$value->id]  = $this->practicetest_model->getquestionanswers($value->q_id);
                 
           }
            $data['testresultss'] = $this->practicetest_model->gettestresult($tid, $uid);
            echo "<pre>"; print_r($data);exit;
             $data['quetionscount'] = count($data['testresult']);
             $this->template->exam_theme('answerkeys', $data);
        }
        public function getVideoSol($qid){
          $qid = $qid;
          $data['qdetails'] = $this->practicetest_model->getquestionanswers($qid);
          //echo "<pre>"; print_r($data);
          $this->load->view('video',$data);
        }
        public function checkVideoSol(){
         $qid = $this->input->post('qid');
          $data['qdetails'] = $this->practicetest_model->getquestionanswers($qid);
          if( $data['qdetails']->video_link !=''){
          echo "1";
          }else {
            echo "0";
          }
          
        }
       

        

        public function challengeroftheday() {
            $d = date('d-m-Y');
            $challengeroftheday = '';
            return $challengeroftheday;
         }
        
        public function fbloginsubmit() {
            $user = $this->facebook->getUser();
            if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
            }else {
            $this->facebook->destroySession();
            }
            $name = $data['user_profile']['first_name'];
            //echo "<pre>";print_r($data['user_profile']['first_name']);
            $email = $data['user_profile']['email'];
            $result = $this->register_model->checkmail($email);
            if($result !='') {
            $session_data = array(
		'logged_in'  => TRUE,
		'lastlogin' => date("H:i"),
		'buser_id' => $result->member_id,
                'member_name' =>$result->member_name,
                 'member_image' =>$result->member_image,
		'member_email' =>$result->member_email,
                'member_phone' => $result->member_phone);
                $this->session->set_userdata($session_data);
            redirect(base_url()."practicetest"); 
            }else {
            $config = array();
            $config['useragent']           = "CodeIgniter";
            $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol']            = "smtp";
            $config['smtp_host']           = "localhost";
            $config['smtp_port']           = "25";
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;
            $this->load->library('email');
            $this->email->initialize($config);
            $pass = $this->referencenumber();
            $password = hash('sha256',$pass);
            $mobile = "";
            $formvalues['member_email'] = $email;
            $formvalues['member_password'] = $password;
            $formvalues['member_ogpass'] = $pass;
            $lid = $this->register_model->registermember($formvalues);
            if($lid) {
            ob_start();	 
            ?>

 <!--mail template stsrt here-->
		<table border="0" cellspacing="2"  cellpadding="2" width="100%" bgcolor="#323232">
                    <tr>
                    <td bgcolor='#323232' colspan='6'><h3 style='color: #fff; font-family: ubuntuc; padding-left:15px;'>Login Details</h3></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >User Name</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $email;?></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Password </td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $pass;?></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Phone Number</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $mobile;?></td>
                    </tr>
                </table>
                  <!--mail template end here-->
				    


	   <?php	
	   $sendermessage  = ob_get_contents();
	   ob_end_clean(); 
           $this->email->from('admin@brainwizz.in','Admin');
           $this->email->to($email); 
           $this->email->subject('Login Details');
           $this->email->message($sendermessage);	
           if($this->email->send()){
           } else {
           }  
           
           
           }
           $session_data = array(
		'logged_in'  => TRUE,
		'lastlogin' => date("H:i"),
		'buser_id' => $lid,
                'member_name' =>$name,
		'member_email' =>$email,
                'member_phone' => '');
                $this->session->set_userdata($session_data);
            redirect(base_url()."practicetest"); 
           }
       }
       
        public function referencenumber() {
       //get lat order id
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	$pass = array(); //remember to declare $pass as an array
	$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
	$n = rand(0, $alphaLength);
	$pass[] = $alphabet[$n];
	}
	return implode($pass); //turn the array into a string
        }
        public function checktestuser(){
          $uid =$this->session->userdata('buser_id');   
          $tid = $this->input->post('tid');
          
          $res = $this->practicetest_model->gettestresult($tid, $uid);
          if($res !='') {
              echo '0';
          }else {
              echo '1';
          }
        }
       
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */