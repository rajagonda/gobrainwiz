<?php 
$timestamp = $testdetails->test_time;
$time = $timestamp;
$parsed = date_parse($time);
$diff = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
$hours = floor($diff / 3600) . ' : ';
$diff = $diff % 3600;
$minutes = floor($diff / 60) . ' : ';
$diff = $diff % 60;
$seconds = $diff;
$total_min  =  10;//$minutes*60;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome To</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/new/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/new/css/font-awesome.css" media="all">
    <!-- own style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/new/css/view-solutions.css">
   <link href="<?php echo base_url();?>assets/new/css/practice-test-styles.css" rel="stylesheet" media="all">
  	<script type="text/javascript" src="<?php echo base_url();?>assets/mspace.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header class="view_solutions_hdr practice_test_hdr">
      <div class="col-md-12 practice_test_hdr_col">
		<h4><i class="fa fa-laptop" aria-hidden="true"></i><?php echo $testdetails->test_name; ?></h4>
		<span class="total_time" id="total_time"><?php echo $total_min;?></span>

		</div>
    </header>
    <div class="clearfix"></div>
    <section class="vs_wrapper practice_test_q_wraper">
    	<form method="post" name="form1" id="formsubmit" class="myform" action="<?php echo base_url();?>practicetest/submitquestions">
      <div class="vs_wrap" >
          <div class="vs_wrap_lft">
          	<?php 
          	$j=1;
    	if($testquestions !=''){
    		$i=1;
    		
foreach ($testquestions as $value) {?>
            <div class="vs_sections" id="<?php echo $i;?>" style="display:<?php if($i=='1') { echo "block"; } else { echo "none";}?>">
				<div class="vs_qstns">
					<div class="vs_qstn">
						<div class="vs_qstn_num">Question <b><?php echo $i; ?></b> of <span><?php echo count($testquestions);?></span></div>
						<div class="vs_qstn_txt">
							<?php echo $value->question_name; ?>
						
						</div>
							
						<div class="vs_optns">
							<ul>
								 <?php if($value->option1 !='') { ?>
								<li>
									<label>
										<input class="answerValue"  type="radio" id="opt_<?php echo $value->q_id;?>" name="opt_<?php echo $value->q_id;?>" value="1">
										<span class="col-xs-1 col-sm-2 option_chk">A</span>
										<span class="col-xs-11 col-sm-10"><?php echo str_replace("<p>", '',$value->option1);?> </span>
									</label>
								</li>
								<?php } ?>
								
								 <?php if($value->option2 !='') { ?>
								<li>
									<label>
										<input class="answerValue"  type="radio" id="opt_<?php echo $value->q_id;?>" name="opt_<?php echo $value->q_id;?>" value="2">
										<span class="col-xs-1 col-sm-2 option_chk">B</span>
										<span class="col-xs-11 col-sm-10"><?php echo str_replace("<p>", '',$value->option2);?> </span>
									</label>
								</li>
								<?php } ?>
								
								 <?php if($value->option3 !='') { ?>
								<li>
									<label>
										<input  class="answerValue" type="radio" id="opt_<?php echo $value->q_id;?>" name="opt_<?php echo $value->q_id;?>" value="3">
										<span class="col-xs-1 col-sm-2 option_chk">C</span>
										<span class="col-xs-11 col-sm-10"><?php echo str_replace("<p>", '',$value->option3);?> </span>
									</label>
								</li>
								<?php } ?>
								
								 <?php if($value->option4 !='') { ?>
								<li>
									<label>
										<input class="answerValue"  type="radio" id="opt_<?php echo $value->q_id;?>" name="opt_<?php echo $value->q_id;?>" value="4">
										<span class="col-xs-1 col-sm-2 option_chk">D</span>
										<span class="col-xs-11 col-sm-10"><?php echo str_replace("<p>", '',$value->option4);?> </span>
									</label>
								</li>
								<?php } ?>
							</ul>
							<input type="hidden" value="" name="checked[]" class="radioCheck" />
						</div>

					</div>
					
					<div class="clearfix"></div>
				</div>
			 </div>
			  
                <input type="hidden" name="question_id[]" value="<?php echo $value->q_id; ?>" />
			  
			    <?php $i++;} }?>
			 <!--  <div class="vs_wrap_btm">
				<div class="vs_actin_btns">
					
					
					<button type="button" class="btn btn-default" id="pre">Previous</button>
				  
					<button  type="button"  class="btn btn-default" id="nex">Next</button>

					<button type="button" name="finish" class="btn btn-default btn-success submit_test_btn pull-right" data-toggle="modal" data-target="#submit_popup" rel="<?php echo base_url()."braintest/completetest" ?>" >Submit Test</button>

					

				</div>
				
			</div> -->

			
			
<div class="vs_wrap_btm">
				<button type="button" name="finish" class="btn btn-default btn-success submit_test_btn pull-left" data-toggle="modal" data-target="#submit_popup" el="<?php echo base_url()."braintest/completetest" ?>">Submit Test</button>
				<div class="vs_actin_btns">
					<button type="button" class="btn btn-default" id="pre"> Previous</button>
					<button type="button" class="btn btn-default" id="nex">Next</button>
					
				</div>
				
			</div>

			<input type="hidden" id="presentTab" value="<?php echo $j; ?>">
			<input type="hidden" id="totalq" value="<?php echo count($testquestions);?>">
			  <input type="hidden" name="total_questions" value="<?php echo $quetionscount; ?>" />
    <input type="hidden" value="<?php echo  $testdetails->test_id; ?>" id="test_id" name="test_id">
    <input type="hidden" value="<?php echo  $testdetails->test_time; ?>" id="test_time" name="test_time">
			
            </div>
                  
          </div>
		    
</form>
		  
        </div>
      
    </section>


  <div id="submit_popup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
            <h4>Do You Really Want to Submit Test ?</h4>
            <div class="yes_no_btns">
            	<button class="btn btn-default yes_btn" id="az">Yes</button>
                <button class="btn btn-default no_btn" id="no_btn">No</button>
            </div>

      </div>
    </div>
  </div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/new/js/bootstrap.min.js"></script>

<script type="text/javascript">
 var hour = "<?php echo floor($hours); ?>";
 var min = "<?php echo floor($minutes); ?>";
 var sec = "<?php echo floor($seconds); ?>";

function countdown() {
 
 if(sec <= 0 && min > 0) {
  sec = 59;
  min -= 1;
 }
 else if(hour <=0 && min <= 0 && sec <= 0) {
  hour = 0;
  min = 0;
  sec = 0;
   $("#CountDownTimer").hide();
  alert("your test time is completed & Submit now");
  document.form1.submit();
  return false;
  
 }
 else {
  sec -= 1;
 }
 
 if(min <= 0 && hour > 0) {
  min = 59;
  hour -= 1;
 }
 
 var pat = /^[0-9]{1}$/;
 sec = (pat.test(sec) === true) ? '0'+sec : sec;
 min = (pat.test(min) === true) ? '0'+min : min;
 hour = (pat.test(hour) === true) ? '0'+hour : hour;
 
 document.getElementById('total_time').innerHTML = ""+min+":"+sec;
 //document.getElementById('timeLeft').value = hour+":"+min+":"+sec;
 setTimeout("countdown()",1000);
 }
 countdown();
</script>
   <script>
	$(document).ready(function(){
		$("#pre").hide();
		 $(".answerValue").change(function(){
           // alert("flkjfdl");
            $(this).parent().parent().parent().parent().find(".radioCheck").val(1);
        });   
		$("#nex").click(function(){ 		
 		var pt = $("#presentTab").val()
 		var t = $("#totalq").val();
		var nt = parseInt(pt)+1;	
		if(nt >1){
			$("#pre").show();
		}
		if(nt<=t){		
 		
 		$("#presentTab").val(nt) 		
 		$("#"+pt).css("display", "none");
		$("#"+nt).css("display", "block");
		
}
if(nt == t){
			$("#nex").hide();
		}

		});
		$("#pre").click(function(){ 		
 		var pt = $("#presentTab").val() 		
 		var nt = parseInt(pt)-1;
 		if(nt !='0'){
 			if(nt == '1'){
			$("#pre").hide();
				$("#nex").show();
 			}else {
 			$("#nex").show();	
 			}
 		$("#presentTab").val(nt) 	
 		$("#"+pt).css("display", "none");
		$("#"+nt).css("display", "block");		
		}

		});
	});
	</script>
	
	<!-- scroller -->
	
<!-- 	
	<script type="text/javascript">
		$(window).resize(function(){
			$('.vs_qstns').height($(window).height()-230);

		});
		$(window).trigger('resize');
	</script> -->

	<script type="text/javascript">
	$(window).resize(function(){
		$('.vs_qstns').height($(window).height()-145);

	});
	$(window).trigger('resize');
</script>
	<script>
	
		
		$('.answer_panlel_icon').click(function() {
			$('.vs_wrap_rht').animate({
				width: 'toggle'
			});
			
			if ($('.answer_panlel_icon').hasClass("clicked")) {
				$('.answer_panlel_icon').removeClass('clicked');
				$('.vs_wrap_lft').css('padding-right','320px');
			} else {
				$('.answer_panlel_icon').addClass('clicked');
				$('.vs_wrap_lft').css('padding-right','0px');

			}
			
			
		});
		
		$(document ).ready(function() {

	$("#az").on("click", function(e) {
         $("#formsubmit").submit();
          //alert("ffff");
	});

	$("#no_btn").on("click", function(e) {
         $('#submit_popup').modal("hide");
	});
});


</script>

  </body>
</html>