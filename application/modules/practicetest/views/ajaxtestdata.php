


		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?php echo $topicInfo->topic_name;?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
		</div>
      <div class="modal-body custom-modalbody">
			<div class="row test-list-row justify-content-center">
					<?php if($tests !=''){ foreach ($tests as  $value) {  ?>
                      
                       <div class="col-lg-3 col-sm-4 text-center">
							<div class="test-list-col">
								<img src="<?php echo assets_url()?>img/exam.png" alt="">
								<h4 class="h6"><?php echo $value->test_name;?></h4>
								 <div class="py-2">
									 <span class="small d-block">No. of Questions :<b><?php echo $testQ[$value->test_id];?></b></span>
									<span class="small d-block">Duaration : <b><?php echo date('i:s', strtotime($value->test_time));?> Mins</b></span>
								 </div>
								<?php
									$uid =$this->session->userdata('studentId'); 
									$res = getTestResult($value->test_id, $uid);
									if($res){ ?>
										<a  class="whitebtn"href="<?php echo base_url();?>practicetest/testresultview/<?php echo $value->test_id; ?>">View Score</a>
									<?php }else { ?>
										<a  class="whitebtn"href="<?php echo base_url();?>practicetest/testStart/<?php echo $value->test_id; ?>">Start Test</a>
									<?php } ?>
							</div>
						</div>
                     <?php } }?>
       		</div>
      </div>
	  
