 <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Practise <span class="fbold">Tests </span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Practise Tests</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 3 -->
                <div class="col-lg-3 lefttab">
                    <ul class="resp-tabs-list ">
					<?php
						$i='1';
						foreach ($cats as  $value) {   ?>
                        <li <?if($i==1){?>class="selectnav" <?php }?>>
							<a href="javascript:;" onclick="return getTopics('<?php echo $value->id;?>');">
								<?php echo $value->category_name;?>
							</a>
						</li>
                    <?php  $i++;}?>    
                    </ul>
                </div>
                <!--/ col 3-->

                <!-- col 9-->
                <div class="col-lg-9">
				<?php if($topics !=''){
						  ?>
                  <!-- test row-->
                  <div class="row test-list-row" id="ajaxdata">
                        <!-- test col -->
						<?php foreach ($topics as  $value) { ?>
                        <div class="col-lg-3 col-sm-4 text-center">
                            <div class="test-list-col">
                                <img src="<?php echo assets_url()?>img/exam.png" alt="">
                                <h4 class="h6"><?php echo $value->topic_name;?></</h4>
                                <p>Total Tests <?php echo countTopicTests($value->topic_id);?></p>
								<?php if(countTopicTests($value->topic_id) !='0'){?>
                                <a href="javascript:;" class="whitebtn sudhi" data-id="t<?php echo $value->topic_id;?>" data-toggle="modal" data-target="#test-pop">Take Test</a>
								<?php } ?>
                            </div>                                        
                        </div>
						<?php }?>
                        <!--/ test col -->
                        
                    </div>
					<?php   } else { ?>
						<div class="pt_info">NO data Found</div>
				<?php }?>
                    <!--/ test row -->   
					
				
                    </div>
                    <!--/ col 9-->                  
                </div>         
               <!--/ col 9-->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
	
	
	<!-- popup table -->
    <div class="modal fade" id="test-pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content ">
				<div id="result"></div>
			</div>
		</div>
    </div>
    <!--/ popup table -->
	
<script type="text/javascript">
$(document).ready(function() {
    
    $('.resp-tabs-list li').click(function(e) {
    $(this).addClass('selectnav').siblings().removeClass('selectnav');
});
    
    $('.sudhi').click(function (event) {
		$('#testajax').html('');
		
       // var id = $(this).attr('id');
	   var id = $(this).data("id");
		console.log("ID: "+id)
        var p_url= "<?php echo base_url(); ?>practicetest/gettopicTests";
		jQuery.ajax({
			type: "POST",             
			url: p_url,
			data: 'topicId='+id,
			success: function(data) {
				$('#result').html(data);
				ajaxLoading = false;
			}
		});  
        $('#related_sub_tests1').modal('show')
    });
});

    function testsView(){
        $('#related_sub_tests1').dialog({
			modal: true,
			autoOpen: false
		});
    }

	function getTopics(cid)
	{
		$("li").removeClass("selectnav");
		var p_url= "<?php echo base_url(); ?>practicetest/gettopics";
		jQuery.ajax({
			type: "POST",             
			url: p_url,
			data: 'cid='+cid,
			success: function(data) {
		 		$('#ajaxdata').html(data);
				ajaxLoading = false;
			}
		});  
	}

    $(document).ready(function(){

		$(".rvds").click(function() {       
        
			var img = $("#videodiv").find("span").attr('id');   
		  
					 
			var desc =  $("#desc").text();
			var vlink = $("#mainframe").attr("src");
			$("#mainframe").attr("src", $(this).attr("id")); 
			$("#desc").text($(this).find('h4').text());
			$("#videodiv").find("span").attr('id',$(this).find('img').attr("src"));  
			$(this).find('img').attr("src",img);
			$(this).find('h4').text(desc);
			$(this).attr('id', vlink);  
					
			return true;
		})
	});
	var videos = [];
	$(document).ready(function () {
		$('#timeS').click(function () {
			$.ajax({
				url: "http://localhost:53390/TimeSpeedAndDistence/",
				success: function (data) {
					alert(data);
					
				}
			});
			
		});

	});
</script>