   <link href="<?php echo base_url();?>assets/new/css/practice-test-styles.css" rel="stylesheet" media="all">
<section class="elitm_wrapper practice_score_card_wrap">
  <div class="container">
    <div class="row practice_score_card">
     <div class="col-md-12">
       <div class="psc_wrap">
         <div class="psc_wrap_top">
            <div class="row">
              <div class="col-md-3 col-xs-12">

                  <?php 
                                    
                   $per = ($testresult->correct_q/$testresult->total_q)*100;            




                  
                  $p=  round($per,0);
                  ?>

                <div class="pie_progress" role="progressbar" data-goal="<?php echo $p;?>" data-barcolor="#1b1b42" data-barsize="15" aria-valuemin="0" aria-valuemax="100">
                  <div class="pie_progress__number"></div>
                </div>
              </div>
              <div class="col-md-9 col-xs-12">   
                  <img src="<?php echo base_url();?>assets/images/certified.png" class="img-responsive">
                 <h3>Congrats <?php echo $testresult->examuser_name?> !</h3>
              <!--    <h4>Your Rank Is <b>250</b> out of <b>1000</b> Test Takers</h4> -->
              </div>
            </div>
         </div>

         <div class="psc_wrap_btm">
           <h4>Your Score Card</h4>
           <div class="score_table">
             <table class="table table-condensed table-striped">
              
              <tbody>
                 <tr>
                  <td>Test Name</td>
                  <td><?php echo $testdetails->test_name;?></td>
                </tr>
                <tr>
                  <td>Total Questions</td>
                  <td><?php echo $testresult->total_q;?></td>
                </tr>
                <tr>
                  <td>Questions Attempted</td>
                  <td><?php echo $testresult->attempted_q;?></td>
                </tr>
                <tr>
                  <td>Correct</td>
                  <td><?php echo $testresult->correct_q;?></td>
                </tr>
               <!--  <tr>
                  <td>Time Spent</td>
                  <td>5 Mins</td>
                </tr>
                <tr>
                  <td>Rank</td>
                  <td style="color: #00a861;">250</td>
                </tr> -->
              </tbody>
            </table>
          </div>
          <div class="practice_score_card_btns">
            <h5><a href="<?php echo base_url(); ?>practicetest/showanswerkey/<?php echo $testdetails->test_id;?>">View Solutions</a></h5>
            <h5 class="back_btn"><a href="<?php echo base_url(); ?>practicetest">Practice Tests</h5>
         </div>
       </div>
     </div> 
    </div>
  </div>
  </div>
</section>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-asPieProgress.min.js"></script>
  <script type="text/javascript">
    jQuery(function($) {
      $('.pie_progress').asPieProgress({
        namespace: 'pie_progress'
      });
    });
    
    $(document).ready(function() {
      $('.pie_progress').asPieProgress('start');
    });
  </script>