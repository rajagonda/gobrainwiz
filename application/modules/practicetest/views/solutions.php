<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/new/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/new/css/font-awesome.css" media="all">   <link rel="stylesheet" href="<?php echo base_url();?>assets/new/css/view-solutions.css">
   <link href="<?php echo base_url();?>assets/new/css/practice-test-styles.css" rel="stylesheet" media="all">
    	<script type="text/javascript" src="<?php echo base_url();?>assets/mspace.js"></script>
  	<!-- <style>
  		mstyle mn:nth-child(2) {
    font-size: 50%;
    top: -10px;
    position: relative;
	}
		msup mn:nth-child(2) {
    font-size: 50%;
    top: -10px;
    position: relative;
	}
  	</style> -->
  	</head>  
  <body>
    <header class="view_solutions_hdr practice_test_hdr">
      <div class="col-md-12 practice_test_hdr_col">
		<h4><i class="fa fa-laptop" aria-hidden="true"></i><?php echo $testdetails->test_name;?></h4>
		 <span class="pull-right back_btn" style="color:green;"><a href="<?php echo base_url(); ?>practicetest">Back To Practice Test</a></span>
		
		</div>
    </header>
    <div class="clearfix"></div>
    <section class="vs_wrapper practice_test_q_wraper">
      <div class="vs_wrap">
          <div class="vs_wrap_lft">
          	  	<?php 
          	$j=1;
    	if($testresult !=''){
    		$i=1;
    		
foreach ($testresult as $value) {?>
            <div class="vs_sections" id="<?php echo $i;?>" style="display:<?php if($i=='1') { echo "block"; } else { echo "none";}?>">
				<div class="vs_qstns">
					<div class="vs_qstn">
						<div class="vs_qstn_num">Question <b><?php echo $i; ?></b> of <span><?php echo count($testresult);?></span></div>
						<div class="vs_qstn_txt">
							<?php echo $answer[$value->id]->question_name; ?>
						</div>
													
						<div class="vs_optns">
							
							<ul>
									<?php
									if($value->attempet > 0 ) {
                                       if($value->answer == '') {
									?>    
									<li class="<?php if($answer[$value->id]->question_answer == '1') { echo "correct_answer"; }?>">
										<label>

											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option1; ?></span>
										</label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '2') { echo "correct_answer"; }?>"><label>
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option2; ?></span></label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '3') { echo "correct_answer"; }?>">
											<label>
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option3; ?></span></label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '4' ) { echo "correct_answer"; }?>">
											<label>
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option4; ?></span></label>
										</li>
									<?php } else { ?>                      

											<?php if($answer[$value->id]->question_answer == $value->answer ) { ?>

											<li class='<?php if($answer[$value->id]->question_answer == '1' && $value->answer == '1') { echo "both_correct";}?>' >
												<label>
											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option1; ?></span></label>
										</li>
										
										
											<li class='<?php if($answer[$value->id]->question_answer == '2' && $value->answer == '2') { echo "both_correct";}?>' >
												<label>
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option2; ?></span></label>
										</li>
										
											
											<li class='<?php if($answer[$value->id]->question_answer == '3' && $value->answer == '3') { echo "both_correct";}?>' >
												<label>
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option3; ?></span></label>
										</li>
										
										
											<li class='<?php if($answer[$value->id]->question_answer == '4' && $value->answer == '4') { echo "both_correct";}?>' ><label>
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option4; ?></span></label>
										</li>
											<?php } else {?>

									
										<li class='<?php if($value->answer == '1') { echo "wrong_answer";} else if($answer[$value->id]->question_answer == '1') { echo "correct_answer";}?>' ><label>
											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option1; ?></span></label>
										</li>
										
										<li class='<?php if($value->answer == '2') { echo "wrong_answer";} else if($answer[$value->id]->question_answer == '2') { echo "correct_answer";}?>' ><label>
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option2; ?></span></label>
										</li>
										
										<li class='<?php if($value->answer == '3') { echo "wrong_answer";} else if($answer[$value->id]->question_answer == '3') { echo "correct_answer";}?>' ><label>
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option3; ?></span></label>
										</li>
										
										<li class='<?php if($value->answer == '4') { echo "wrong_answer";} else if($answer[$value->id]->question_answer == '4') { echo "correct_answer";}?>' ><label>
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option4; ?></span></label>
										</li>
										<?php } } }  else { ?>


										<li class="<?php if($answer[$value->id]->question_answer == '1') { echo "correct_answer"; }?>">

<label>
											<span class="col-xs-1 col-sm-2 option_chk">A</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option1; ?></span></label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '2') { echo "correct_answer"; }?>"><label>
											<span class="col-xs-1 col-sm-2 option_chk">B</span>
											<span class="col-xs-11 col-sm-10"><?php echo $answer[$value->id]->option2; ?></span></label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '3') { echo "correct_answer"; }?>"><label>
											<span class="col-xs-1 col-sm-2 option_chk">C</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option3; ?></span></label>
										</li>
										
										<li class="<?php if($answer[$value->id]->question_answer == '4' ) { echo "correct_answer"; }?>"><label>
											<span class="col-xs-1 col-sm-2 option_chk">D</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option4; ?></span></label>
										</li>
										<?php if($answer[$value->id]->option5!='') { ?>

										<li class="<?php if($answer[$value->id]->question_answer == '5' ) { echo "correct_answer"; }?>"><label>
											<span class="col-xs-1 col-sm-2 option_chk">E</span>
											<span class="col-xs-11 col-xs-10"><?php echo $answer[$value->id]->option5; ?></span></label>
										</li>
										<?php } ?>

										<?php } ?>
									</ul>
						</div>
						<?php if($answer[$value->id]->question_explanation !='0' && $answer[$value->id]->question_explanation !=''){ ?>
						<div class="vs_soln_exp">
							<h4>Explanation:</h4>
							<?php echo $answer[$value->id]->question_explanation; ?>
						</div>
						<?php } ?>
						  <input type="hidden" name="qid<?php echo $i; ?>" id="qid<?php echo $i; ?>" value="<?php echo $answer[$value->id]->q_id; ?>">
					</div>
					
					<div class="clearfix"></div>
				</div>
			 </div>
			
			   <?php $i++;} }?>
			  <div class="vs_wrap_btm">
				<div class="vs_actin_btns">
			<!-- 	<button class="pull-left btn btn-default watch_video_solution openPopup" data-toggle="modal" data-target="#video_solution_popup">Video Solution</button> -->
				<button class="pull-left btn btn-default watch_video_solution openPopup" style="display: none;">Video Solution</button>
					<!-- <input type="hidden" name="pq" id="pq" value="1"> -->

					<button type="button" class="btn btn-default" id="pre" styel="display:none;">Previous</button>
					<button  type="button"  class="btn btn-default" id="nex">Next</button>
				</div>
					<input type="hidden" id="presentTab" value="<?php echo $j; ?>">
					<input type="hidden" id="totalq" value="<?php echo count($testresult);?>">
			</div>
			
            </div>
                  
          </div>
		  
		  
        </div>
    </section>

    <!-- video solution popup -->

 <div class="modal fade solution_video_popup" id="video_solution_popup" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
                <div class="solution_video_container">
                	<div class="solution_video_iframe">
                		
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/new/js/bootstrap.min.js"></script>

   <script>

   $(document).ready(function(){
    $('.openPopup').on('click',function(){
    	var pt = $("#presentTab").val()
        var id = $("#qid"+pt).val();
        //alert(id);
       $('.solution_video_iframe').load('<?php echo base_url();?>practicetest/getVideoSol/'+id,function(){
            $('#video_solution_popup').modal({show:true});
        });
    }); 
    $('.solution_video_popup .close').click(function() {
	  var embedCode = '';
		$('.solution_video_iframe').html(embedCode);  
	});
});

	$(document).ready(function(){

				var pt = $("#presentTab").val()				
				var qid = $("#qid"+pt).val();

				$.ajax({
				url:"<?php echo base_url();?>practicetest/checkVideoSol",
				type: 'POST',
				datatype:'application/json',
				data: 'qid='+qid,
				success: function(data){
				if(data == 1){
				$(".openPopup").css("display", "block");
				}else {					     	
				 $(".openPopup").css("display", "none");				 
				}
				}
				});

		$("#pre").hide();
		
		$("#nex").click(function(){ 	

				var pt = $("#presentTab").val()
				var nt = parseInt(pt)+1;
				var qid = $("#qid"+nt).val();
					var t = $("#totalq").val();
			
		//alert(nt);
		if(nt >1){
			$("#pre").show();
		}
		if(nt<=t){ 		
 		$("#presentTab").val(nt) 		
 		$("#"+pt).css("display", "none");
		$("#"+nt).css("display", "block");

				$.ajax({
				url:"<?php echo base_url();?>practicetest/checkVideoSol",
				type: 'POST',
				datatype:'application/json',
				data: 'qid='+qid,
				success: function(data){
				if(data == 1){
				$(".openPopup").css("display", "block");
				}else {					     	
				 $(".openPopup").css("display", "none");				 
				}
				}
				});

		
 	
		
}
if(nt == t){
			$("#nex").hide();
		}

		});
		$("#pre").click(function(){ 		
 		var pt = $("#presentTab").val() 	
		var nt = parseInt(pt)-1;
 		var qid = $("#qid"+nt).val();
//alert(nt);
 		if(nt !='0'){
 			if(nt == '1'){
			$("#pre").hide();
 			}else {
 			$("#nex").show();	
 			}
 			
 		$("#presentTab").val(nt) 	
 		$("#"+pt).css("display", "none");
		$("#"+nt).css("display", "block");		
		}
				$.ajax({
				url:"<?php echo base_url();?>practicetest/checkVideoSol",
				type: 'POST',
				datatype:'application/json',
				data: 'qid='+qid,
				success: function(data){
				if(data == 1){
				$(".openPopup").css("display", "block");
				}else {					     	
				 $(".openPopup").css("display", "none");				 
				}
				}
				});

 	
 		

		});
	});

	</script>
	<script type="text/javascript">
	$(window).resize(function(){
		$('.vs_qstns').height($(window).height()-145);

	});
	$(window).trigger('resize');
</script>
	