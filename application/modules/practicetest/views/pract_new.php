<main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Practise <span class="fbold">Tests </span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>                        
                        <li class="breadcrumb-item active"><a>Practise Tests</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
               <!-- col 9 -->
                <div class="col-lg-9">
                    <!-- tab -->
                    <div class="parentHorizontalTab simpletabs practisetests">
                        <ul class="resp-tabs-list hor_1">
							<?php
								
								foreach ($cats as  $value) { 
							?>
							<li>
								<a href="#" onclick="return getTopics('<?php echo $value->id;?>');">
									<?php echo $value->category_name;?>
								</a>
							</li>
                           <?php } ?>
                        </ul>
                        <!-- responsvie container -->
					
                        <div class="resp-tabs-container hor_1">
                            <!-- Arithmetic -->
                             <div class="custom-accord">                              
                                <h3 class="h5 pb-3">Arithmetic</h3>
                                <div class="table-section">
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Test Name</th>
                                            <th scope="col">Total Tests </th>
                                            <th scope="col">Start </th>                               
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php	
											if($topics !=''){
												
											foreach ($topics as  $value) {   ?>
                                        <tr>
                                            <td scope="row"><?php echo $value->topic_name;?></td>
                                            <td><?php echo countTopicTests($value->topic_id);?></td>
											<?php if(countTopicTests($value->topic_id) !='0'){?>
                                            <td><a data-toggle="modal" data-target="#test-pop" href="javascript:void(0)" class="fblue">Take Test</a></td>   
											<?php } ?>											
                                        </tr>
										<?php } }?>
                                    </tbody>

                                    </table>
                                </div>
                            </div>
                            <!--/ Arithmetic -->

                            
                        </div>
                        <!-- responsive container -->
                    </div>
                    <!--/ taB -->               
                  
                </div>         
               <!--/ col 9-->

               <!-- col 3-->
               <div class="col-lg-3">
                   <div class="career-block text-center">
                        <img src="img/premade-image-09.png">
                        <h2 class="h5 py-3">Advance Your Career</h2>
                        <p class="text-center">Enter your email below to download one of our free career guides</p>
                        <form>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter your Email"> 
                            </div>
                            <div class="form-group">
                                <input class="bluebtn w-100" type="submit" value="Submit"> 
                            </div>
                        </form>                      
                   </div>
               </div>
               <!--/ col 3-->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>