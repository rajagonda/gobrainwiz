<?php
class practicetest_model extends CI_Model {
	
	public function get_cats_topics(){
		$sql = "SELECT * FROM gk_pcategories c JOIN gk_ptopics t ON c.id = t.cat_id AND c.status='y' AND t.topic_status='y' ORDER BY c_id ASC";
		$query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
		
	}
	
    public function getCats() {
        $sql = "SELECT  * FROM gk_pcategories WHERE c_id='1'";  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
    
     public function getallsubcategories() {
        $sql = "SELECT  * FROM gk_pcategories  WHERE c_id='1'";  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     
     public function getalltopics($id){
         $sql = "SELECT  * FROM gk_ptopics WHERE subcat_id='$id'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     public function gettopicInfo($tid){
         $sql = "SELECT  * FROM gk_ptopics WHERE topic_id='$tid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     public function gettopicspapers($id) {
         $sql = "SELECT  test_id, test_time,test_name  FROM gk_ptests WHERE topic_id='$id' AND test_status='y'";
         //echo $sql;exit;
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     
     public function gettestdetails($tid) {
          $sql = "SELECT  * FROM gk_ptests WHERE test_id='$tid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     
     public function testquestions($tid) {
          $sql = "SELECT  * FROM gk_ptquetions WHERE test_id='$tid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     public function getpassagename($pid) {
         $sql = "SELECT  * FROM gk_ptpassages WHERE id='$pid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     public function insertanswerdetails($data) {
          $this->db->insert('gk_ptestinformation', $data); 
         return $this->db->insert_id();
     }
     public function getquetiondetails($answer,$qid,$tid) {
         $sql = "SELECT COUNT(*) as num, q_id "
                    . "\n FROM gk_ptquetions "
                    . "\n WHERE test_id = '".$tid."'"
                    . "\n AND q_id = '".$qid."'"
                    . "\n AND question_answer = '".$answer."'"; 
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     public function inserttestresult($data) {
          $this->db->insert('gk_ptestresult', $data); 
         return $this->db->insert_id();
     }
     public function gettestresult($tid, $uid) {
          $sql = "SELECT  * FROM gk_ptestresult 
                    INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id 
                    WHERE gk_ptestresult.test_id='$tid' AND gk_ptestresult.user_id='$uid'";
                    //echo $sql;exit;
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return false;
        }
     }
        public function gettestresultkeys($tid, $uid) {
          $sql = "SELECT  * FROM gk_ptestinformation WHERE test_id='$tid' AND user_id='$uid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
     public function getquestionanswers($qid) {
         $sql = "SELECT  *  FROM gk_ptquetions WHERE q_id='$qid'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
     
     public function checktestresult($uid, $tid) {
        $this->db->where('test_id', $tid);
        $this->db->where('user_id', $uid);
        $this->db->delete('gk_ptestresult');
        
        //echo $this->db->last_query();
     }
       public function checktestinfo($uid, $tid) {
        $this->db->where('test_id', $tid);
        $this->db->where('user_id', $uid);
        $this->db->delete('gk_ptestinformation');
     }

      public function gettestresult1($tid, $uid) {
          $sql = "SELECT  gk_ptestresult.test_id FROM gk_ptestresult 
                    INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id 
                    WHERE gk_ptestresult.test_id='$tid' AND gk_ptestresult.user_id='$uid'";
                    //echo $sql;exit;
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return true;
        } else {
            return false;
        }
     }
     public function getQnum($tid){
        $sql = "SELECT test_id FROM gk_ptquetions WHERE test_id='$tid'";
        $query = $this->db->query($sql);
         if($query->num_rows() > 0) {
            return $query->num_rows();
         }else {
            return false;
         }
     }

     
     
     
        

    
}