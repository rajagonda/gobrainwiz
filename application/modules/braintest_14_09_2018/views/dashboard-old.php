<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;background-color: #fefffe;
    color: #1282cd;}
    
/* Style the tab */
div.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 30%;
    height: 300px;
}

/* Style the buttons inside the tab */
div.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:after {background:#1b1b42;content:"";position:absolute;
z-index:-1;top:0;left:0;right:0;bottom:0;-webkit-transform:scaleX(0); transform:scaleX(0);-webkit-transform-origin:0 50%;transform-origin:0 50%;-webkit-transition-property:transform;transition-property:transform; 
transition-duration:.4s;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}

.qklink_wpr .linkgate .accordion-item:hover .accordion-header:after {background:#00a861;    
width: 100%;-webkit-transform:scaleX(1);transform:scaleX(1)}

.main_wrapper {width:1104px;margin:0 auto;clear:both}
.main_wrapper {text-align: center;}
.main_wrapper{position:relative}
.lnk_ansaddwpr{display:inline-block;width:100%;}
.inr_title{background:#1b1b42;border-bottom:3px solid #00a861;font-size:20px;color:#fff;
font-weight:400;position:relative;padding-left:50px;height:43px;line-height:43px}
.linkgate .accordion a, .linkgate .accordion1 a {
    font-family: Arial, Helvetica, sans-serif;
    text-decoration: none;
    font-size: 15px;
    padding: 9px 0;
    margin: 0;
    display: block;
    cursor: pointer;
    background: #fafafa;
    border-bottom: 1px solid #a6a6a6;
    outline: none;
    display: inline-block;
    width: 74%;
	color: #151719;
    font-weight: 500;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:hover a {
    color: #fff;
}
.inrright_cntr #gate h1,.book_package .package_head{ 
background:#d6d4d4;
border: 1px solid #ccc;    
border-radius: 3px;    
color: #202090;    
font-size: 26px;    
font-weight: 600;    
margin-left: 0;
padding: 3px 10px;    
position: relative;}

.qklink_wpr .linkgate .accordion-item .accordion-header:hover a{color:#fff}
.linkgate .accordion a.abtacordian,.accordion-header h1 > a{border-bottom:none!important;
background:none!important;display:inline-block!important;padding:0!important;font-family:open_sansregular;font-size:14px}
.accordion1 .accordion-header h1{float:left;font-size:15px;font-weight:400;margin:0;line-height:1em;color:#00a861;}
.inrright_cntr #gate p {
    font-size: 15px;
    line-height: 1.5em;
    padding: 3px 0 0 11px;
    text-align: justify;
    font-weight: 400;
}
.inrright_cntr #gate ul li {
    padding: 0px 0;
    font-size: 14px;
    margin: 0 0 0 5px;
    font-family: Arial, Helvetica, sans-serif;
    list-style: disc;
}
.card-cascade .view, .card-cascade-2 .view, .card-overlay, .colorful-select .dropdown-content li a:hover, .colorful-select .dropdown-content li span:hover, .comments-list img, .dropdown .dropdown-menu .dropdown-item:active, .dropdown .dropdown-menu .dropdown-item:hover, .modal-dialog.cascading-modal.modal-avatar .modal-header img, .reply-form img, .section .author-box, .section .jumbotron, .testimonial-carousel .testimonial .avatar img, .z-depth-2 {
    -webkit-box-shadow: 0 8px 17px 0 rgba(0,0,0,.2), 0 6px 20px 0 rgba(0,0,0,.19);
    box-shadow: 0 8px 17px 0 rgba(0,0,0,.2), 0 6px 20px 0 rgba(0,0,0,.19);
}

#toast-container>div, .author-box, .badge, .btn, .btn-floating, .card, .card-cascade-2 .card-block, .card-cascade.wider .card-block, .card-wrapper .back, .card-wrapper .front, .cascading-media .view, .chip, .dropdown-content, .dropdown-menu, .jumbotron, .list-group, .modal-dialog.cascading-modal .modal-c-tabs .nav-tabs, .modal-dialog.modal-notify .modal-header,.dropdown-menu.dropdown a:active,.dropdown-menu.dropdown a:focus,.dropdown-menu.dropdown a:hover, .pager li a, .pagination .active .page-link, .popover, .pricing-card .price .version, .section-blog-fw .view, .side-nav, .social-list, .z-depth-1 {
    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
}
.card-cascade .card-block {
    padding-top: 1.8rem;
}
h5{
padding: 1px;
    font-size: 12px;
}
h4{
padding: 0px 2px 0px 4px;
}
.btn{
    margin-bottom: 10px;
}
#img{
 height: 93px;
    width: 69px;
    margin-left: 38px;
}
.overlay{
    background-color:rgb(0, 0, 0);
}
@media (min-width: 375px) and (max-width: 667px){
.inrright_cntr{
    width: 100%;
    margin-right: 0px;
}
}
.btn-primary{
background-color:#00bcd4;
}
#logout{
    margin-left: 1034px;
    background-color: rgb(0, 107, 48);
    color: #e4d4d4;
    height: 35px;
    width: 103px;
    font-weight: 700;
    margin-top: -33px;
}
.inrright_cntr #gate h1, .book_package .package_head {
    height: 44px;
    color: rgba(245, 245, 245, 0.98);
    border-radius: 3px;
    padding: 5px 10px;
    font-size: 22px;
    font-weight: 700;
    box-shadow: inset -4px 0px 53px 9px #003366, inset -30px 0px 30px -20px #8ed693;
    text-shadow: 1px 0px 1px #003366;
}
.btn-block {
    margin-left: -10px;
    display: block;
    width: 100%;
}
.inrright_cntr {
    box-shadow: 0 0 0 1px #e3e3e3 inset;
    margin-bottom: 25px;
}
.categoryOutter .categoryNameBox .catName {
    float: left;
    width: 100%;
}
.catName h3 {
    background: #1282cd;
    float: left;
    font-family: cursive;
    font-size: 17px;
    padding: 4px 0 5px 10px;
    width: 953px;
    margin: 25px -95px 0;
    font-weight: normal;
    text-transform: uppercase;
    margin-bottom: 8px;
    color:#f2f2f2;
}
.catName span {
    height: 66px;
    width: 51px;
    float: left;
    margin-left: -145px;
    margin-top: 1px;
    background: url(<?php echo assets_url();?>images/instruction2.png) left 9px no-repeat;
}
#row{
	background-color: rgba(128, 128, 128, 0.2);
	    margin-bottom: 35px;
}
.catName{
 margin-left: 4px;
}
#b{
color:#233390;
    text-align: left;
    font-size: 21px;
    margin-left: -25px;
    font-weight: 600;
	 margin-top: -16px;

}
#h4{
color:#233390;
    text-align: left;
    font-size: 21px;
    margin-left:-121px;
    font-weight: 600;
}
li{
	    color: #044571;
}
#endtext{
	margin-left: -19px;color: #ec4337;
}
.btn-group{
	margin-top: 13px;
    margin-left: 2px;
}
#logical{
    border: 1px solid #1282cd;
    height: 42px;
    width: 199px;
    margin-left: -117px;
    border-radius: 4px;
	color:#008dcd;
	display:block;
}
.btn-success{
border: 1px solid #ebecec!important;
color: #f2f2f2!important;
}
#current{
    color: white;
    background-color: #1282cd;
    border: 1px solid #1282cd;
    height: 42px;
    width: 199px;
    margin-left: -117px;
    border-radius:4px;
    font-size: 17px;
}
#verbal{
	border: 1px solid #1282cd;
    height: 42px;
    width: 199px;
    margin-left: -117px;
    border-radius: 4px;
	color:#008dcd;
}
#arithmetic{
	border: 1px solid #1282cd;
    height: 42px;
    width: 199px;
    margin-left: -117px;
    border-radius: 4px;
	color:#008dcd;
}
#ol{
	width: 497px;
}
#col5{
	margin-top:12px;
}
#col7{
	margin-top:12px;
}
@media (max-width: 480px) and (min-width: 320px)
{
#col5{
	margin-bottom: 27px;
	}
#ol {
    width: 257px;
    text-align: justify;
    }	
}
@media (max-width: 568px) and (min-width: 320px)
{
	.gate {
    margin-top: 11px;
    }
	#col5{
	margin-top: 47px;
	margin-left: 86px;
	}
	#col7{
	margin-left: -16px;
	}
	.catName {
    margin-left: 128px;
    }
	.catName h3{
	    width: 271px;
    font-size: 15px;
	}
	#ol {
    width: 277px;
    text-align: justify;
    }
#h4 {
 font-size: 18px;
}
#b {
 font-size: 18px;	
}
#logical{
	height: 39px;
    font-size: 16px;
}
#verbal{
	height: 39px;
    font-size: 16px;
}
#arithmetic{
	height: 39px;
    font-size: 16px;
}
}
@media (max-width: 640px) and (min-width: 360px){
	.catName h3 {
    width: 295px;
    font-size: 16px;
}
#ol {
    width: 303px;
    text-align: justify;
}
}
@media (max-width: 667px) and (min-width: 375px)
{
	.gate {
    margin-top: 11px;
    }
	#col5{
	margin-top: 47px;
	margin-left: 86px;
	}
	#col7{
	margin-left: -7px;
	}
	.catName {
    margin-left: 128px;
    }
	.catName h3{
	width: 326px;
	}
	#ol {
    width: 307px;
    text-align: justify;
}
#h4 {
 font-size: 18px;
}
#b {
 font-size: 18px;	
}
#logical{
	height: 39px;
    font-size: 16px;
}
#verbal{
	height: 39px;
    font-size: 16px;
}
#arithmetic{
	height: 39px;
    font-size: 16px;
}
}

@media (max-width: 690px) and (min-width: 412px){
	.catName h3 {
    width: 346px;
    font-size: 17px;
}
#ol {
    width: 354px;
    text-align: justify;
}
}
@media (max-width: 853px) and (min-width: 480px){
	.catName h3 {
    width: 431px;
    font-size: 18px;
}
#ol {
    width: 431px;
    text-align: justify;
}
}
@media (max-width: 1024px) and (min-width: 768px){
	#col5{
	margin-left: 81px;
	}
	#col7 {
    margin-top: 18px;
    margin-left: -103px;
    width: 190px;
}
	.catName {
    margin-left: 132px;
}
	.catName h3 {
    width: 695px;
    font-size: 18px;
    }
#ol {
    width: 431px;
    text-align: justify;
}
}
.alert{
	    margin-top: -53px!important;
    width: 306px!important;
}
#gate{
	    margin-left: 191px;
}
</style>
<!-- Popup Ends Here -->
<div class="btwrapper">
 <header class="brainwiz-header">
	<?php 
	if(isset($timeleft) && $timeleft>0)
	{
	?>
	    <h3 class="pull-left col-md-5">Its Break Time</h3>

	<div class="pull-center col-md-3">
		<div class="timer">
         <div id="breakCountDownTimer" data-timer="<?php echo $timeleft;?>" style="background-color:#4382C5;"></div>
		</div>
	</div>
	<?php 
	} 
	else
		echo '<h1 class="pull-left col-md-6">Welcome '.$this->session->userdata('examusername') .' ,</h1>';

	?>
    <div class="pull-right col-md-3">
    	<p class="text-right text-info">Welcome <?php echo $this->session->userdata('examusername');?></p>
        <p class="text-right"><span id="day"><span id="date_time"></span>
        <script type="text/javascript">window.onload = date_time('date_time');</script>  </span></p>
		<!-- <p class="text-right"><a href="<?php echo base_url()."onlinetest/logout"?>"><span id="">Log Out</span></a></p> -->
    </div>
</header>
<section>
<div class="container">
 <div class="row" id="row">
 <div class="col-md-2">
 </div>
 <div class="col-md-8">
 <?php $this->load->view('message'); ?>
 <div class="inrright_cntr" style="box-shadow:none;">
 <div class="gate" id="gate">
<div class="catName">
<span class="GovPvtJobIconLarge"></span>
<h3>Brainwiz  Test  Categories</h3>
</div><br>
<div class="col-sm-12">
<div class="col-sm-5" id="col5">
<h4 id="h4">Test Categories:</h4>
<div class="btn-group" role="group" aria-label="...">
 <?php
		
		if(isset($categories))
		{
			foreach($categories as $cat)
			{
				$buttunClass="btn-default";
				if($cat->complete==1)
					$buttunClass="btn-success";
				?>
				      <button type="button" class="btn <?php echo $buttunClass;?> btn-lg categoryBtn"  onClick="getUrlTest('<?php echo base_url()."braintest/brainteststart/".$cat->id; ?>')" id="logical"><?php echo $cat->category_name; ?></button><br>
					  <?php  /*echo (isset($timeleft) && $timeleft>0)?"disabled":""*/?>
				<?php
			}
		}		
	  ?>
</div>
</div>
<div class="col-sm-7" id="col7">
<div class="table-features">
	  <p>
	  <h4 id="b">Instructions:</h4>
</p><ol id="ol">
<li>Test consists of <?php echo count($categories);?> sections i.e. <?php foreach($categories as $cat){echo $cat->category_name.", ";}?></li>
<!-- <li>These section will be of 40 mins each.</li> -->
<li>After finishing each section you will have a break time of <?php echo $testdetails->break_time; ?> min.</li>
<li>Once the section is completed you cannot go back to that section.</li>
<li>You can chose the section you want to attempt from Instruction page(current).</li>
<li>Section can be selected from the top navigation tab.</li>
<li>Once you click on compete button a confirmation will be asked whether you want to complete the section or not.</li>
<li>Each question will be of 1 mark.</li>
<li>You will not have multiple choice questions.</li>
<li>You need to click on <b>"Next/ Save "</b> button on order to save your marked answer.</li>
<li>Result will be displayed immediately after you complete all the three sections.</li>
</ol>
<br>
<b id="endtext" style="font-weight: 500;">All The Best.</b>
<br>
<b style="color:#008dcd;margin-left: -19px;">BRAINWIZ</b>
<p></p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-2">
</div>	  
</div>
</div>
</section>
