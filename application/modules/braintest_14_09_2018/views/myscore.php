
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BRAINWIZ</title>
    <meta name="description" content=" " />
	<meta name="keywords" content="" />
	<meta name="google-site-verification" content="" />
	<meta name="msvalidate.01" content="" />
	<meta name="ROBOTS" content="INDEX, FOLLOW" />
	<meta name="ROBOTS" content="ALL" />
	<meta name="googlebot" content="index,follow,archive" />
	<meta name="msnbot" content="index,follow,archive" />
	<meta name="Slurp" content="index,follow,archive" />
	<meta name="author" content="" />
	<meta name="publisher" content="" />
	<meta name="owner" content="" />
	<meta http-equiv="content-language" content="English" />
	<meta name="doc-type" content="Web Page" />
	<meta name="doc-rights" content="Copywritten Work" />
	<meta name="rating" content="All" />
	<meta name="distribution" content="Global" />
	<meta name="document-type" content="Public" />
	<meta name="revisit-after" CONTENT= "daily"/>
	<meta name="geo.region" content="IN-TS" />
	<meta name="geo.placename" content="Hyderabad" />
<link href="<?php echo assets_url();?>new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.theme.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_url();?>images/img/favicon.png">
<link href="<?php echo assets_url();?>new/css/hover.css" rel="stylesheet" media="all">
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link href="<?php echo assets_url();?>new/css/jquery.lineProgressbar.css" rel="stylesheet" type="text/css">
<!-- Popup Starts Here -->
<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="<?php echo assets_url();?>new/js/jquery.svgDoughnutChart.js"></script>
<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

h5{
	
    margin-left: -71px;
    padding: 1px;
    font-size: 18px;
    font-weight: 500;
    color: rgb(158, 154, 154);
    text-align: center;
}
h4{
padding: 0px 2px 0px 4px;
}
.btn{
    margin-bottom: 10px;
}
#img{
 height: 93px;
    width: 69px;
    margin-left: 38px;
}
.overlay{
    background-color:rgb(0, 0, 0);
}

.btn-primary{
background-color:#00bcd4;
}
.btn{
       background-color: #167f92;
    height: 31px;
    width: 36px!important;
    box-shadow: -2px 4px 2px #999;
    color: #f5f5f5;
}
.btn:hover{
       height: 29px;
	    color: #f14646;
    box-shadow: 0px -3px 9px 2px #184b6f;
}

body::-webkit-scrollbar {
    width: 1em;
}
 
body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
body::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}

.home .top-wrapper {
    padding: 4px;
    top: 0;
    z-index: 9;
    height: 82px;
}
#container2{
	margin-left: 90px;
	height: 248px;	
    padding: 50px;
}
svg{
	    height: 148px;
    width: 492px;
}
#title{
font-size: 26px;
    font-weight: 500;
    color: #fff;
    text-transform: uppercase;
}
#logo{
	margin-top: 25px;
    margin-left: 65px;
}
.top-wrapper{
	background-color: #12128c!important;
}
.progressbar{
	background-color:rgb(202, 193, 193)!important;
}
#icon-img{
    height: 89px;
    width: 83px;
}
#icon-mimg{
height: 99px;
    width: 100px;
}
.cta-col {
    padding: 0px 0px 0px 117px;
}
h3{
	color: #8a8888;
    font-size: 16px;

    margin-top: 16px;
}
.percentCount{
	display:none;
}
#attempted .proggress{
	    background-color: rgb(0, 202, 178)!important;
    height: 9px!important;
    border-radius: 3px;
    width: 90%;
}
#correct .proggress{
	    background-color: rgb(65, 83, 187)!important;
    height: 9px!important;
    border-radius: 3px;
    width: 90%;
}
#time .proggress{
background-color:rgb(121, 119, 119)!important;
    height: 9px!important;
    border-radius: 3px;
    width: 90%;	
}
h4{
    margin-left: 127px;
    margin-top: -42px;
	color:#3777e2;
}
.progressbar{
	width:76%!important;
}
#section{
	margin-left: 50px;
    font-size: 25px;
	margin-left:12px;
}
#span{
	    font-size: 19px;
    color: #847c7c;
}
#big{
	    font-size: 29px;
    font-family: arial;
    color: #FF9800;
}
@media (max-width: 667px) and (min-width: 375px)
{
	#score-section #score-div{
		    margin-left: -148px;
	}
.col-xs-4 {
    width: 100.333333%;
	margin-left: 28px;
}
#footer-score .col-xs-4{
	width: 100.333333%;
	margin-left: -9%;
}
#section {
   font-size: 18px!important;
    font-weight: 600!important;
    margin-left: 91px!important;
}
#span {
    font-size: 15px;
	font-weight: 400;
}
#big3{
	font-size:22px!important;
}
#big2{
	font-size:28px!important;
}
#big {
    font-size: 20px;
    font-family: arial;
    color: #FF9800;
}
}
@media (max-width: 568px) and (min-width: 320px)
{
#score-section #score-div{
margin-left: -191px;
}
.col-xs-4 {
    width: 100.333333%;
	margin-left: 28px;
}
#section {
   font-size: 16px;
    font-weight: 600;
    margin-left: 52px;
}
#span {
    font-size: 15px;
	font-weight: 400;
}
#big3{
	font-size:22px!important;
	margin-left:-39px;
}
#big2{
	font-size:28px!important;
}
#big {
    font-size: 20px;
    font-family: arial;
    color: #FF9800;
}
#footer-score .col-xs-4{
	width: 100.333333%;
	margin-left: -9%;
}
}
@media (max-width: 640px) and (min-width: 360px){
	#section {
    font-size: 16px;
    font-weight: 600;
    margin-left: 87px;
}
}
big{
	font-size: 44px;
}
footer{
	display:none!important;
}
</style>
<!-- Popup Ends Here -->
</head>
<body>
<header class="home">
  <div class="top-wrapper" style="box-shadow:0px 2px 8px grey;">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 col-md-5 col-xs-5" id="logo"> 
		<a href="#" title="" id="title">Your Score -> <?php echo $testdetails->test_name;?> </a>
		</div>
      </div>
    </div>
  </div>  
</header>

<section id="score-section">
<div class="container-fluid">
<div class="row">
  <div class="col-sm-12" style="background-color:rgba(204, 204, 204, 0.52);margin-top:-55px;">
  <div class="col-sm-1">
  </div>
  <div class="col-sm-4" id="score-div">
   <div id="container2"></div>
   </div>
   <div class="col-sm-7">
   </div>
  </div>
  <div class="col-sm-12" style="background-color:white;margin-top:44px;">
  </th>
  <?php
        $totalQ=0;
        $totalUnattempted=0;
        $totalAttempted=0;
        $totalCorrect=0;
        foreach($tresult as $keys => $testresult )
        {
            $totalQ+=$testresult->total_q;
            $totalUnattempted+=$testresult->notattempetd_q;
            $totalAttempted+=($testresult->total_q-$testresult->notattempetd_q);
            $totalCorrect+=$testresult->correct_q;
		}
			?>
       
  <div class="col-xs-4">
  <div id="attempted"></div>
  <img src="<?php echo assets_url();?>images/icons/pre1.png" id="icon-img">
  <span><h4><big style="color: #00cab2;" id="big2"><?php echo $totalAttempted;?></big>/<?php echo $totalQ;?></h4><br><h5>Attempted</h5></span>
  <input type="hidden" id="attemptedss" value="<?php echo $totalAttempted;?>">
  </div>
  <div class="col-xs-4">
  <div id="correct"></div>
  <img src="<?php echo assets_url();?>images/icons/correct.png" id="icon-mimg"><br>
  <span><h4><big style="color: #00a867;" id="big2"><?php echo $totalCorrect;?></big>/<?php echo $totalAttempted;?></h4><br><h5>Correct</h5></span>
   <input type="hidden" id="correctss" value="<?php echo $totalCorrect;?>">
  </div>
  <div class="col-xs-4">
  <div id="time"></div>
  <img src="<?php echo assets_url();?>images/icons/time.png" id="icon-img">
  <span><h4><big style="color: #6e6e6e;" id="big3">28:40</big>/30:00</h4><br><h5>Time taken</h5></span>
  </div>
  </div>

  <div class="col-sm-12" style="margin-top:5px;" id="footer-score">
   <div class="col-xs-4">
   <p><h1 id="section" style="color:#3949a7;">Verbal Score:  <span id="span"><big id="big"><?php echo $testresult->correct_q;?></big>/<?php echo $testresult->total_q;?></span></p></h1>
  </div>
  <div class="col-xs-4">
  <p><h1 id="section" style="color:#2da732;">Arithematic Score:  <span id="span"><big id="big"><?php echo $testresult->correct_q;?></big>/<?php echo $testresult->total_q;?></span></p></h1>
  </div>
  <div class="col-xs-4">
  <p><h1 id="section" style="color:#227dc5;">Reasoning Score:  <span id="span"><big id="big"><?php echo $testresult->correct_q;?></big>/<?php echo $testresult->total_q;?></span></p></h1>
  </div>
</div>
</div>
</section>
<input type="hidden" id="percent" value="
<?php 
$per="";
$totalQ1= '';
$totalCorrect1 ='';
        foreach($tresult as $keys => $testresult )
        {
            $per += ($testresult->correct_q/$testresult->total_q)*100;


            $totalQ1+=$testresult->total_q;                        
            $totalCorrect1+=$testresult->correct_q;

            
        }
        echo round(($totalCorrect1/$totalQ1)*100,2);
    ?>
">
<script>
var p=$('#percent').val();
    $('#container2').doughnutChart({

        positiveColor: "#4153bb",
        negativeColor: "#00a861",
        backgroundColor: "#e4e4e4",
        percentage: p,
        size: 100,
        doughnutSize: 0.35,
        innerText: p+"%",
        innerTextOffset: 12,
        Title: "Overall Score",
		Title2: "90/100",
        positiveText: "Secured",
        negativeText: "Total"
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="<?php echo assets_url();?>new/js/jquery.lineProgressbar.js"></script>

<script>
var attemptedss=$("#attemptedss").val();
//alert(attemptedss);
$('#attempted').LineProgressbar({
percentage:attemptedss,
radius: '3px',
height: '20px',
});
var correctss=$("#correctss").val();
$('#correct').LineProgressbar({
percentage:correctss,
radius: '3px',
height: '20px',
fillBackgroundColor: '#DA4453'
});
$('#time').LineProgressbar({
percentage:70,
radius: '3px',
height: '20px',
fillBackgroundColor: '#E0C341'
});
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="<?php echo assets_url();?>new/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo assets_url();?>new/assets/jquery/plugins.js"></script> 
</body>
</html>