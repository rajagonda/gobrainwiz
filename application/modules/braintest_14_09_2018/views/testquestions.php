
</script>
<div class="btwrapper">

	<header class="brainwiz-header exam_page_hdr exam_page_hdr_mobile clearfix">
	<!--<h3 class="pull-left col-md-6 col-sm-6 wel_lft">Welcome sudhakar ,</h3> -->   
    <div class="pull-right col-md-12 col-sm-12 wel_rht">
    	<p class="text-right text-info test_name"><?php echo $this->session->userdata('testname');?></p>
        <p class="text-right test_person_nme"><i class="fa fa-user" aria-hidden="true"></i><?php echo $this->session->userdata('examusername');?></p>

    </div>

  </header>


   <div class="btinnerWrapper blue-border btinnerWrapper_qpaper">
    

        <div class="container-fluid pos_relative">
    	<div class="col-md-8 col-sm-7 col-xs-12 q_paper">
        <div class="pg-title"><?php echo $category;?></div>

		<?php 
			if(!empty($question) && isset($question->question_name))
			{
				if($question->attempet==2)
				{
				?>
				  		<button id="markreview" rel="<?php echo base_url(); ?>braintest/markreview" class="btn btn-warning pull-right btn-sm mark_for_rev"><i class="fa fa-flag"></i></button>

				<?php
				}
				else
				{
				?>
				 <button id="markreview" rel="<?php echo base_url(); ?>braintest/markreview" class="btn btn-default pull-right btn-sm mark_for_rev"><i class="fa fa-flag"></i></button>

				<?php 
				
				}
			
			}
		?>
		<div class="clearfix"></div>
  <div class="bt_paper">
  <?php 
  //print_r($question);
  //print_r($questions);
  $c = count($questions);
  if(!empty($question) && isset($question->question_name))
  {
  ?>
	<form name="qfrm" id="qfrm" action="<?php echo base_url()."braintest/submitanswer"?>" method="post" >
  	<!-- <div class="bt_question_title">Question <?php echo $question->serial_number;?></div> -->
  	  	<div class="bt_question_title">Question <?php echo $question->serial_number;?> of <b><?php echo $c;?></b></div>
    <div class="bt_question"><?php echo $question->question_name;?></div>
    <div class="bt_answer">
    	<ul class="list-unstyled">
		<?php 
			for($i=1 ; $i<=5;$i++)
			{
				$varname="option".$i;
				$option= $question->$varname;
				if($option!="")
				{
		?>
        <li>
		<input type="radio"  class="answerdetails" id="<?php  echo $i;?>_<?php echo $question->q_id;?>_<?php echo $question->cat_id;?>"  value="<?php  echo $i;?>" <?php echo($question->answer!="" && $question->answer==$i)?"checked":"";?> name="answer">&nbsp;<label>
      <?php 
		
	  echo $option;?></label>
	  <div class="clearfix"></div>
		</li>
		
		<?php
			}
		} ?>
   
        </ul>
    </div>
    <div class="bt_action">
	
		<?php 
			if($question->serial_number>1)
			{
		?>
    	<button type="button" class="btn btn-primary next_pre_btns" onClick="getUrlTest('<?php echo base_url()."braintest/braintestquestion/".$question->cat_id."/".($question->serial_number-1); ?>')" >Previous</button>
		<?php
			}
		
			if($question->serial_number<count($questions))
			{
		?>
        <button type="button" id="next_q_button" class="btn btn-primary next_pre_btns" onClick="getUrlTest('<?php echo base_url()."braintest/braintestquestion/".$question->cat_id."/".($question->serial_number+1); ?>')" >Next</button>
		
				<!-- <input type="hidden" name="next_q_id"  value="<?php echo $question->serial_number+1 ;?>"/>
				<button type="submit" class="btn btn-primary">Next</button> -->

			<?php
			}
			elseif(!$activate_complete_button)
				{
			?>
				<!-- <button type="submit" class="btn btn-primary">Save</button> -->
			<?php 
				}
			?>
		        
		<input type="hidden" id="q_id" name="q_id" value="<?php echo $question->q_id ;?>"/>
		<input type="hidden" id="cat_id" name="cat_id" value="<?php echo $question->cat_id ;?>"/>
		<input type="hidden" id="q_no" name="q_no" value="<?php echo $question->serial_number ;?>"/>
		
		<button type="button" id="completetest" data-toggle="modal" data-target="#submit_popup" class="btn btn-default pull-right" rel="<?php echo base_url()."braintest/completetest" ?>" >Complete</button>
    </div>
	</form>
  <?php 
  }
  else
  {
	echo " No more questions available for this section.";
	?>
	  <button type="button" id="completetest" data-toggle="modal" data-target="#submit_popup"  class="btn btn-default pull-right" rel="<?php echo base_url()."braintest/completetest" ?>" >Complete</button>

	<?php 
	
  }
  ?>
  </div>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 right_panel">

        	<div class="timer clearfix">
        	<h4 class="tme_lft">Time Remaining</h4>
            <div class="timer timer_count">
                <div id="CountDownTimer" data-timer="<?php echo $timeleft;?>" data-tc-id="c57b9366-5660-e6f4-bf70-d8e29fc5e6a8">
					<div class="time_circles">
						
						<div class="textDiv_Minutes">
							<h4>Mins</h4>
							<span>19</span>
						</div>
						<div class="textDiv_Seconds">
							<h4>Secs</h4>
							<span>47</span>
						</div>
					</div>
				</div>
            </div>
            
            <!--<div class="countdown-timer-wrapper">
      			<div class="timer" id="countdown"></div>
        	</div>-->
        </div>

    	
      <div class="mobile_analyse"><i class="fa fa-bars"></i></div>
        <div class="analyse">
        	<h4> Review your Questions </h4>
            <table class="table table-condensed table-bordered">
			
		<?php 
				if(is_array($questions) && count($questions)>0)
				{
				foreach($questions as $qkey => $qValue)
				{
					switch($qValue->attempet)
							{
								case 0:
									$class="";
									$icon="";
								break;
								case 1:
									$class="not_attempt";
									$icon='<i class="fa fa-stop"></i>';
								break;
								case 2:
									$class="mark_for_rev";
									$icon='<i class="fa fa-flag">';

								break;
								case 3:
									$class="passed";
									$icon='<i class="fa fa-strikethrough"></i>';
									
								break;

							}
							
							if( isset($question->q_id) && $qValue->q_id==$question->q_id)
							{
								$class="current";
								$icon='<i class="fa fa-dot-circle-o"></i>';
									
							}

							//echo $class;exit;
							
					if(($qkey+1)%5==0)
					{

						echo '<td class="'.$class.'" rel="'.base_url()."braintest/braintestquestion/".$qValue->cat_id."/".$qValue->serial_number.'">'.$icon.($qkey+1). '</td>';
						
					echo '</tr>';


					}
					else
					{
						if(($qkey+1)%5==1)
							echo '<tr>';
							
						echo '<td class="'.$class.'" rel="'.base_url()."braintest/braintestquestion/".$qValue->cat_id."/".$qValue->serial_number.'">'.$icon.($qkey+1). '</td>';
	
					}
				}
			}
		?>
           
            </table>
             <div class="legend">
            	<ul>
                	<li class="current"><i class="fa fa-dot-circle-o"></i>Current</li>
                    <li class="passed"><i class="fa fa-strikethrough"></i></i>Attempt</li>
                    <li class="not_attempt"><i class="fa fa-stop"></i>Not Attempt</li>
                    
                </ul>
            </div>
        </div>
      
      </div>
    </div>
  </div>

  <div id="submit_popup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
            <h4>Do You Really Want to Submit Test ?</h4>
            <div class="yes_no_btns">
            	<button class="btn btn-default yes_btn" id="az">Yes</button>
                <button class="btn btn-default no_btn" id="no_btn">No</button>
            </div>

      </div>
    </div>
  </div>
</div>
  
  <!-- mobile analyse -->
<script>
$('.mobile_analyse').click(function() {
	$('html').toggleClass('open-menu');
    $('.analyse').animate({
        width: 'toggle'
    });
});

</script>

<script type="text/javascript">





$(document ).ready(function() {

	$("#az").on("click", function(e) {
          window.location = $("#completetest").attr('rel');
          //alert("ffff");
	});

	$("#no_btn").on("click", function(e) {
         $('#submit_popup').modal("hide");
	});

	//$('#dvModalDialog').dialog("close").

 var answid ;

$(".answerdetails").click(function(){
      answid = this.id;
      var dataString = 'answerid='+ answid;
  //document.getElementById("login").style.display = "none";
  
  //alert("ggg");
         

  $.ajax({
      url:'<?php echo base_url();?>braintest/submitanswer',
      type: 'POST',
      datatype:'application/json',
      data: dataString,
      success:function(response){  
        //alert(response);return false;
       
               
      }
  });
          
});
  




 });


</script>

  
  