<?php
class braintest_model extends CI_Model {


	public function insertMany($tablename,$data){

            $this->db->insert(DB_PREFIX.$tablename,$data);

	    return $this->db->insert_id();

        }

        public function updateMany($tablename,$data,$fname,$value){

            $this->db->where($fname, $value);

	    $this->db->update(DB_PREFIX.$tablename, $data);

	    return $this->db->affected_rows();

        }

        public function deleteMany($tablename,$fname,$value){

	    $this->db->where($fname, $value);

            $this->db->delete(DB_PREFIX.$tablename);

	} 

        public function get(){


            $query = $this->db->get(DB_PREFIX.'braintests');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }

        }

public function getstudents($id)

	{
			if($id!='')
            {
				$this->db->where('brainTestId',$id);
			}
            $query = $this->db->get(DB_PREFIX.'braintest_student');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }

        }
		
	

public function getQuestion($id,$set_id)

	{
			if($id!='' && $set_id!='')
			{
				$sql = "SELECT q.*, a.category_name as category FROM gk_braintestquestions as q 

						INNER JOIN gk_braintestquestioncat as a ON a.id = q.cat_id where q.test_id=".$id." and set_id=$set_id order by q.cat_id
						";

				$query = $this->db->query($sql);

				if($query->num_rows()>0) {

					return $query->result();

				} 
				else {

					return '';
					} 
			}	
			else {

                return '';
            } 			
   }
	
public function getQuestionByCategory($test_id,$set_id,$cat_id)

	{
			if($test_id!='' && $set_id!='')
			{
				$sql = "SELECT @s:=@s+1 serial_number,q.*, a.category_name as category FROM  gk_braintestquestions as q 

						INNER JOIN gk_braintestquestioncat as a ON a.id = q.cat_id ,
        (SELECT @s:= 0) AS s where q.test_id=".$test_id." and set_id=$set_id and q.cat_id = $cat_id and q.question_status='y' order by q.cat_id
						";

				$query = $this->db->query($sql);
//echo "<pre>"; print_r($sql);
				if($query->num_rows()>0) {

					return $query->result();

				} 
				else {

					return '';
					} 
			}	
			else {

                return '';
            } 			
   }
   
   public function getQuestionAnswareByCategory($test_id,$set_id,$cat_id)

	{
					$studentId= $this->session->userdata('studentId');

			if($test_id!='' && $set_id!='')
			{
				$sql = "SELECT @s:=@s+1 serial_number,q.*, 

				         a.category_name as category, 
				         ans.answer,ans.attempet  

				         FROM  gk_braintestquestions as q
				          LEFT JOIN gk_braintestquestioncat as a ON a.id = q.cat_id 
				          LEFT JOIN gk_braintest_answer as ans ON ans.q_id = q.q_id ,

                       (SELECT @s:= 0) AS s 
                       where q.test_id=".$test_id." 
                       and set_id=$set_id 
                       and q.cat_id = $cat_id 
                       and q.question_status='y' 
                       and ans.student_id='".$studentId."' 
                       order by q.cat_id
						";

				$query = $this->db->query($sql);

				if($query->num_rows()>0) {

					return $query->result();

				} 
				else {

					return '';
					} 
			}	
			else {

                return '';
            } 			
   }
   
	public function getQuestionset($id)
	{
		if($id!='')
            {
				$this->db->where('test_id',$id);
			}
            $query = $this->db->get(DB_PREFIX.'braintestquestionset');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }
	}
	public function getRandomQuestionsetByTest($id)
	{
		$this->db->select('set_id');

		if($id!='')
            {
				$this->db->where('test_id',$id);
				$this->db->where('status','y');

			}
			$this->db->order_by("set_id", "random");
			 $this->db->limit(1);
			 
            $query = $this->db->get(DB_PREFIX.'braintestquestionset');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }
	}
	public function getQuestionsetById($set_id)
	{
		if($set_id!='')
            {
				$this->db->where('set_id',$set_id);
			}
            $query = $this->db->get(DB_PREFIX.'braintestquestionset');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }
	}
	
	public function getTestCategoryIdByName($catname)

	{
			if($catname!='')
            {
				$this->db->where('category_name',$catname);
			}
            $query = $this->db->get(DB_PREFIX.'braintestquestioncat');

            if($query->num_rows()>0){

				$category= $query->result();
				$catID=$category[0]->id;
				return $catID;
				
            }
			else 
			{
	
			$data['category_name']=$catname;

			 $this->db->insert(DB_PREFIX.'braintestquestioncat',$data);
				$catID = $this->db->insert_id();
				return $catID;
            }

     }
	public function getAllCategoryByTestId($test_id,$set_id=null)
	
	{
		if($test_id=='')
			return "";
		else
		{	
			$where = 'WHERE `test_id` ='.$test_id;
			if($set_id!=null)
			{
				$where .=' and set_id ='.$set_id;
			}
		 $sql='SELECT *
			FROM `gk_braintestquestioncat`
			WHERE id
			IN (
			SELECT cat_id
			FROM `gk_braintestquestions` '.$where.' GROUP BY `cat_id`
			)';

			$query = $this->db->query($sql);
			//echo $this->db->last_query();exit;
				$lastId=0;
			  if($query->num_rows()>0){

				 return $query->result();

            }
			else {

				return "";
            }
		}	
	}
	
	public function getCategoryById($cat_id)
	{
		if($cat_id=='')
			return "";
		else
		{		
		 $sql='SELECT *	FROM `gk_braintestquestioncat` WHERE id ='.$cat_id;

			$query = $this->db->query($sql);
				$lastId=0;
			  if($query->num_rows()>0){

				 return $query->result();

            }
			else {

				return "";
            }
		}	
	}
	
public function getstudent($id,$tid=null)

	{
			if($id!='')
            {
				$this->db->where('studentId',$id);
			}
			if($tid!='' && $tid !=null)
            {
				$this->db->where('brainTestId',$tid);
			}
            $query = $this->db->get(DB_PREFIX.'braintest_student');

            if($query->num_rows()>0){

				return $query->row();

            }else {

                return '';

            }

     }

public function varifystudent($id,$password)

	{
			if($id!='')
            {
				$this->db->where('studentId',$id);
				$this->db->where('password',$password);
				$this->db->where('status','y');
				
			}
			
            $query = $this->db->get(DB_PREFIX.'braintest_student');

            if($query->num_rows()>0){

				return $query->row();

            }else {

                return '';

            }

     }	 
	public function save($data)
	{

	    $this->db->insert(DB_PREFIX.'braintests',$data);
		$testID = $this->db->insert_id();
		$this->createStudent($data,$testID);
	    return $testID;

	}
	
public function addquestion($data)
	{

	    $this->db->insert(DB_PREFIX.'braintestquestions',$data);
		$testID = $this->db->insert_id();
	    return $testID;

	}
public function addquestionSet($data)
	{

	    $this->db->insert(DB_PREFIX.'braintestquestionset',$data);
		$setID = $this->db->insert_id();
	    return $setID;

	}
	
	public function laststudentID($testID)

	{
		$query = "SELECT `studentId`
						FROM ".DB_PREFIX.'braintest_student'."
						WHERE `brainTestId` =$testID
						ORDER BY `studentId` DESC
						LIMIT 0 , 1";

			  $student = $this->db->query($query);
				$lastId=0;
			  if($student->num_rows()>0){

				 $lastId= $student->result();
				$lastId=substr($lastId[0]->studentId,-3);

            }
			else {

                 $lastId=0;

            }
			return $lastId;
	}
	
public function savestudent($data,$testID)
	{
		
			$lastId	= $this->laststudentID($testID);

			$studentId=str_pad($lastId+1, 3, '0', STR_PAD_LEFT);
			$data["studentId"]="BW".$testID.$studentId;
			$data["password"]="br@inT3st".$testID;
			$data["brainTestId"]=$testID;
			
			$this->db->insert(DB_PREFIX.'braintest_student',$data);
			
			$query = "UPDATE ".DB_PREFIX.'braintests'." SET no_of_student = no_of_student + 1   WHERE brain_test_id =".$testID;
              
			  $query = $this->db->query($query);
			  

	}
	
	public function createStudent($testdata,$testID)
	{
		$studentCount=$testdata['no_of_student'];
		$testcode = strtoupper(substr($testdata['test_name'],0,3));
		for($i=1; $i<=$studentCount ; $i++)
		{
			$data=array();
			$studentId=str_pad($i, 3, '0', STR_PAD_LEFT);
			$data["studentId"]="BW".$testID.$studentId;
			$data["password"]="br@inT3st".$testID;
			$data["brainTestId"]=$testID;
			
			$this->db->insert(DB_PREFIX.'braintest_student',$data);
		}

	}
	
	public function update($data, $id){

	    $this->db->where('brain_test_id', $id);

	    $this->db->update(DB_PREFIX.'braintests', $data);

	    return $this->db->affected_rows();

	}

public function updatestudent($data, $id,$test_id){

	    $this->db->where('studentId', $id);
	    $this->db->where('brainTestId', $test_id);
	    $this->db->update(DB_PREFIX.'braintest_student', $data);

	    return $this->db->affected_rows();

	}
	
	
	public function update_answer($data, $id,$qid){

	    $this->db->where('student_id', $id);
	    $this->db->where('q_id', $qid);

	    $this->db->update(DB_PREFIX.'braintest_answer', $data);
		 $this->db->last_query();
	    return $this->db->affected_rows();

	}
	public function get_answer($id,$qid){

	    $this->db->where('student_id', $id);
	    $this->db->where('q_id', $qid);

	    $query = $this->db->get(DB_PREFIX.'braintest_answer');

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

	}
	public function getTestDetails($id){

	    $this->db->where('brain_test_id', $id);

	    $query = $this->db->get(DB_PREFIX.'braintests');

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

     }
     public function gettesttimedetails($tid,$cid){

	    $this->db->where('test_id', $tid);
	    $this->db->where('cat_id', $cid);

	    $query = $this->db->get(DB_PREFIX.'test_cattime');

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

     }
      public function getTestAllCats($tid){

      	$sql = "SELECT * FROM `gk_test_cattime`
      	        INNER JOIN `gk_braintestquestioncat` ON `gk_braintestquestioncat`.id = `gk_test_cattime`.cat_id
      	        WHERE gk_test_cattime.test_id = '$tid'";



	    
	    $query = $this->db->query($sql);
	    if($query->num_rows()>0){
	    return $query->result();
	    }else {
	    return false;
	    }

     }

     public function getTestcatDetails($tid,$catid){

      	$sql = "SELECT * FROM `gk_test_cattime`
      	        INNER JOIN `gk_braintestquestioncat` ON `gk_braintestquestioncat`.id = `gk_test_cattime`.cat_id
      	        WHERE gk_test_cattime.test_id = '$tid' AND gk_test_cattime.cat_id='$catid'";



	    
	    $query = $this->db->query($sql);
	    if($query->num_rows()>0){
	    return $query->row();
	    }else {
	    return false;
	    }

     }
        public function gettestresultCAt1($uid,$test_id,$cid) 
	{
		
          $sql = "SELECT c.category_name,c.id, count(q_id) AS total_q ,
					count(if(answer !=0,1,NULL))AS attempet1,
                    count(if(answer ='',1,NULL))AS notattempetd_q1,
					count(if(attempet=0,1,NULL))AS notattempetd_q,
					count(if(attempet>0,1,NULL))AS attempet,
					count(if(correct=0,1,NULL))AS wrong,
					count(if(correct=1,1,NULL))AS correct_q,
					SUM(if(correct=1, 1, 0)) AS score,
					count(if(attempet != '0' AND correct = 0,0,NULL)) AS wrong1

					FROM 

					  `gk_braintest_answer` AS a 

					  join gk_braintestquestioncat AS c  on a.cat_id=c.id 

					  

					   WHERE a.student_id='$uid' AND a.test_id='$test_id' AND a.cat_id='$cid' group by a.cat_id";
					   //echo $sql;exit;

          $query = $this->db->query($sql);

        if($query->num_rows()>0) {

            return $query->row();

        } else {

            return '';

        }

     }

     public function getTestDetails1($id){

	    $sql = "SELECT gk_braintests.test_name,sum(`gk_test_cattime`.`time`) as time  FROM `gk_braintests`
      	        INNER JOIN `gk_test_cattime` ON `gk_test_cattime`.test_id = `gk_braintests`.brain_test_id
      	        WHERE gk_braintests.brain_test_id = '$id'";	    
	    $query = $this->db->query($sql);
	    if($query->num_rows()>0){
	    return $query->row();
	    }else {
	    return false;
	    }

     }


       public function gettestresultCAt($uid,$test_id) 
	{
		
          $sql = "SELECT c.category_name,c.id, count(q_id) AS total_q ,
                    count(if(answer !=0,1,NULL))AS attempet1,
                    count(if(answer ='',1,NULL))AS notattempetd_q1,
					count(if(attempet=0,1,NULL))AS notattempetd_q,
					count(if(attempet>0,1,NULL))AS attempet,
					count(if(correct=0,1,NULL))AS wrong,
					count(if(correct=1,1,NULL))AS correct_q,
					SUM(if(correct=1, 1, 0)) AS score,
					count(if(attempet != '0' AND correct = 0,0,NULL)) AS wrong1

					FROM 

					  `gk_braintest_answer` AS a 

					  join gk_braintestquestioncat AS c  on a.cat_id=c.id 

					  

					   WHERE a.student_id='$uid' AND a.test_id='$test_id' group by a.cat_id";
					   //echo $sql;exit;

          $query = $this->db->query($sql);

        if($query->num_rows()>0) {

            return $query->result();

        } else {

            return '';

        }

     }
     public function getallkeys($tid,$uid,$cid){

     	$sql = "SELECT @s:=@s+1 serial_number, gk_braintestquestions.*, gk_braintest_answer.* 
     	       FROM gk_braintest_answer 
     	       INNER JOIN gk_braintestquestions On gk_braintestquestions.q_id = gk_braintest_answer.q_id,
     	       (SELECT @s:= 0) AS s 
     	        WHERE gk_braintest_answer.test_id ='$tid' 
     	        AND gk_braintest_answer.student_id ='$uid'
     	        AND  gk_braintest_answer.cat_id='$cid'";


     	       //echo $sql;exit;
     	$res = $this->db->query($sql);
     	if($res->num_rows()>0){
     		//echo "dd";
     		return $res->result();
     	}else {
     		return false;
     	}

     }
     public function getqdetails($tid,$uid,$cid){
     	$sql = "SELECT * FROM gk_braintestquestions 
     	       INNER JOIN gk_braintest_answer On gk_braintest_answer.q_id = gk_braintestquestions.q_id
     	        WHERE gk_braintestquestions.test_id ='$tid' AND gk_braintest_answer.student_id ='$uid' AND  gk_braintest_answer.cat_id='$cid'";
     	        //echo $sql;
     	$res = $this->db->query($sql);
     	if($res->num_rows()>0){
     		//echo "dd";
     		return $res->row();
     	}else {
     		return false;
     	}
     }
	 
	 
	
		
	 
	 public function getLatestLogOfstudent($student_id,$test_id)
	 {

		$query = "select * from ".DB_PREFIX.'braintest_student_log'."  WHERE student_id ='".$student_id."' AND test_id = '".$test_id."'AND start_time!='0000-00-00 00:00:00' and end_time='0000-00-00 00:00:00'  order by start_time desc Limit 1";
              
			  $query = $this->db->query($query);
			  

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

     }
	 
	 
	 public function getLastLogOfstudent($student_id,$test_id)
	 {

		$query = "select * from ".DB_PREFIX.'braintest_student_log'."  WHERE student_id ='".$student_id."' AND test_id ='".$test_id."' and start_time!='0000-00-00 00:00:00' and end_time!='0000-00-00 00:00:00'  order by end_time desc Limit 1";
              
			  $query = $this->db->query($query);
			  

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

     }
	 
	 
	 public function getFirstLogOfstudent($student_id,$test_id)
	 {

		$query = "select * from ".DB_PREFIX.'braintest_student_log'."  WHERE student_id ='".$student_id."' AND test_id='".$test_id."' and start_time!='0000-00-00 00:00:00'  order by start_time  Limit 1";
              
			  $query = $this->db->query($query);
			  

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

     }
	 public function addLog($data)
	{

	    $this->db->insert(DB_PREFIX.'braintest_student_log',$data);
		$logId = $this->db->insert_id();
	    return $logId;

	}
	
	  public function getStudentLog($data=array())
	  {

			if(isset($data['student_id']))
			{
				$this->db->where('student_id', $data['student_id']);

			}
			
			if(isset($data['log_id']))
			{
				$this->db->where('log_id', $data['log_id']);

			}
			
			if(isset($data['cat_id']))
			{
				$this->db->where('cat_id', $data['cat_id']);

			}
			if(isset($data['test_id']))
			{
				$this->db->where('test_id', $data['test_id']);

			}

			
			
            $query = $this->db->get(DB_PREFIX.'braintest_student_log');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }

        }
	
	
	 public function getCompletedTestLog($data=array())
	  {

			if(isset($data['student_id']))
			{
				$this->db->where('student_id', $data['student_id']);

			}
			
			if(isset($data['log_id']))
			{
				$this->db->where('log_id', $data['log_id']);

			}
			
			if(isset($data['cat_id']))
			{
				$this->db->where('cat_id', $data['cat_id']);

			}
			if(isset($data['test_id']))
			{
				$this->db->where('test_id', $data['test_id']);

			}
			$this->db->where('start_time !=', '0000-00-00 00:00:00');
			$this->db->where('end_time !=', '0000-00-00 00:00:00');
			
            $query = $this->db->get(DB_PREFIX.'braintest_student_log');
//echo $this->db->last_query();exit;
            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }

        }
		
	
	public function updateLogOfstudent($log_id,$data)
	{
		$this->db->where('log_id', $log_id);

	    $this->db->update(DB_PREFIX.'braintest_student_log', $data);

	    return $this->db->affected_rows();
	}
	public function prepareAnswareData($question,$studentId,$currentLog)
	{
		if(count($question)>0)
		{
			foreach($question as $key => $qValue)
			{
				$test_id= $this->session->userdata('brainTestId');
				$answer['log_id']=$currentLog;
				$answer['student_id']=$studentId;
				$answer['test_id']=$test_id;
				$answer['q_id']=$qValue->q_id;
				$answer['q_no']=$qValue->serial_number;
				$answer['cat_id']=$qValue->cat_id;
				$this->db->insert(DB_PREFIX.'braintest_answer',$answer);
				//echo $this->db->last_query();exit;
			}
		}
	}
	public function getIncompleteAnswareBylog($log_id)
	{
		$query = "select * from ".DB_PREFIX.'braintest_answer'."  WHERE log_id =".$log_id." and start_time!='0000-00-00 00:00:00' and end_time='0000-00-00 00:00:00' Limit 1";
              
			  $query = $this->db->query($query);
			  

	    if($query->num_rows()>0)
		{

	    return $query->row();

	    }
		else 
		{

				$query = "select * from ".DB_PREFIX.'braintest_answer'."  WHERE log_id =".$log_id." and start_time ='0000-00-00 00:00:00' and end_time='0000-00-00 00:00:00' Limit 1";
              
			  $query = $this->db->query($query);
			  

				if($query->num_rows()>0)
				{

				return $query->row();

				}
				else {

				return '';

				}
	    }
	}
	
	public function gettestresult($uid,$test_id) 
	{
		
          $sql = "SELECT c.category_name, count(q_id) AS total_q ,
					count(if(attempet=0,1,NULL))AS notattempetd_q,
					count(if(attempet>0,1,NULL))AS attempet,
					count(if(correct=0,1,NULL))AS wrong,

					 SUM(IF(attempet != '' AND correct = 0, 1, 0)) AS wrong1,

					count(if(correct=1,1,NULL))AS correct_q,
					SUM(if(correct=1, 1, 0)) AS score
					  FROM `gk_braintest_answer` AS a join gk_braintestquestioncat AS c  on a.cat_id=c.id  WHERE student_id='$uid' AND test_id='$test_id' group by cat_id";

          $query = $this->db->query($sql);

        if($query->num_rows()>0) {

            return $query->result();

        } else {

            return '';

        }

     }

	public function changeTestStatus($data, $id){

	    $this->db->where('brain_test_id', $id);

	    $this->db->update(DB_PREFIX.'braintests', $data);

            return $this->db->affected_rows();

	}
	

	public function changeStudentStatus($data, $id)
	{

	    $this->db->where('studentId', $id);

	    $this->db->update(DB_PREFIX.'braintest_student', $data);

            return $this->db->affected_rows();

	}
	
	public function changequestionsetStatus($data, $id)
	{

	    $this->db->where('set_id', $id);

	    $this->db->update(DB_PREFIX.'braintestquestionset', $data);

            return $this->db->affected_rows();

	}
	
	public function deleteTest($id)
	{

	    $this->db->where('brain_test_id', $id);

        $this->db->delete(DB_PREFIX.'braintests');
		$this->deleteStudentByTest($id);
		$allQusSet =$this->getQuestionset($id);
		
		foreach($allQusSet as $qset)
		{
			$this->deleteQuestionSet($qset->set_id);
		}

	}        
public function deleteQuestionSet($id)
	{
		$qusSet =$this->getQuestionsetById($id);
		$file_name = $qusSet[0]->file_name;
		$path='./assets_new/uploads/'.$file_name;

		if(file_exists($path))
		{
			unlink($path) or die('failed deleting: ' . $path);;

		}
	    $this->db->where('set_id', $id);

        $this->db->delete(DB_PREFIX.'braintestquestionset');
		$this->deleteQuestionByset($id);
		
	}
	
	public function deleteQuestionByset($id)
	{

	    $this->db->where('set_id', $id);

        $this->db->delete(DB_PREFIX.'braintestquestions');
			
	}
	
	public function deleteStudentByTest($id)
	{

	    $this->db->where('brainTestId', $id);

        $this->db->delete(DB_PREFIX.'braintest_student');
			
	}
	
	public function deleteStudent($id)
	{
		$row = $this->getstudent($id);

		$query = "UPDATE ".DB_PREFIX.'braintests'." SET no_of_student = no_of_student - 1   WHERE brain_test_id =".$row->brainTestId;
              
			  $query = $this->db->query($query);


	    $this->db->where('studentId', $id);

        $this->db->delete(DB_PREFIX.'braintest_student');
			
	}
        public function gettopics(){

            $sql = "SELECT gk_ptopics.*, a.category_name as category FROM gk_ptopics

                   INNER JOIN gk_pcategories as a ON a.id = gk_ptopics.subcat_id";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            }

        }

        public function gettopicDetails($tpid){

            $sql = "SELECT * FROM gk_ptopics WHERE  topic_id = $tpid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->row();

            } else {

                return '';

            }

        }

        public function getPassages(){

            $sql = "SELECT c.*,gk_ptpassages.*, a.category_name as category FROM gk_ptpassages

                    INNER JOIN gk_pcategories as a ON a.id = gk_ptpassages.subcat_id

                    INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptpassages.topic_id";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            } 

        }

        public function getcattopics($catid){

            $sql = "SELECT * FROM gk_ptopics

                   WHERE gk_ptopics.subcat_id = $catid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            }

        }

        public function getpassageDetails($passageid){

            $sql = "SELECT * FROM gk_ptpassages

                    WHERE gk_ptpassages.id=$passageid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->row();

            } else {

                return '';

            } 

        }

        public function getTests(){

            $sql = "SELECT c.*,gk_ptests.*, a.category_name as category FROM gk_ptests

                    INNER JOIN gk_pcategories as a ON a.id = gk_ptests.subcat_id

                    INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptests.topic_id

                    ";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            } 

        }

        public function getTestdetailsbystudent($tid,$sid){

			$this->db->where('studentId',$sid);
			$this->db->where('brainTestId',$tid);            
            $query = $this->db->get(DB_PREFIX.'braintest_student');

            if($query->num_rows()>0){
				return $query->row();
            }else {
                return false;
            }
        }


      public function insertteststudent($data){
 		$this->db->insert(DB_PREFIX.'braintest_student',$data);
		$logId = $this->db->insert_id();
	    return $logId;     	
      }
       public function updatestudentdata($data,$sid,$tid){

 		$this->db->where('studentId', $sid);
 		$this->db->where('brainTestId', $tid);

	    $this->db->update(DB_PREFIX.'braintest_student', $data);

	    return $this->db->affected_rows();
      }

}
?>