<link href="<?php echo assets_url();?>new/css/easy.css" rel="stylesheet" type="text/css">
<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 30%;
    height: 300px;
}

/* Style the buttons inside the tab */
div.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:after {background:#1b1b42;content:"";position:absolute;
z-index:-1;top:0;left:0;right:0;bottom:0;-webkit-transform:scaleX(0); transform:scaleX(0);-webkit-transform-origin:0 50%;transform-origin:0 50%;-webkit-transition-property:transform;transition-property:transform; 
transition-duration:.4s;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}

.qklink_wpr .linkgate .accordion-item:hover .accordion-header:after {background:#00a861;    
width: 100%;-webkit-transform:scaleX(1);transform:scaleX(1)}
</style>
<style>
.main_wrapper {width:1104px;margin:0 auto;clear:both}
.main_wrapper {text-align: center;}
.main_wrapper{position:relative}
.lnk_ansaddwpr{display:inline-block;width:100%;}
.inr_title{background:#1b1b42;border-bottom:3px solid #00a861;font-size:20px;color:#fff;
font-weight:400;position:relative;padding-left:50px;height:43px;line-height:43px}
.linkgate .accordion a, .linkgate .accordion1 a {
    font-family: Arial, Helvetica, sans-serif;
    text-decoration: none;
    font-size: 15px;
    padding: 9px 0;
    margin: 0;
    display: block;
    cursor: pointer;
    background: #fafafa;
    border-bottom: 1px solid #a6a6a6;
    outline: none;
    display: inline-block;
    width: 74%;
	color: #151719;
    font-weight: 500;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:hover a {
    color: #fff;
}
.inrright_cntr .gate h1,.book_package .package_head{ 
background:#d6d4d4;
border: 1px solid #ccc;    
border-radius: 3px;    
color: #202090;    
font-size: 26px;    
font-weight: 600;    
margin-left: 0;
padding: 3px 10px;    
position: relative;

}

.qklink_wpr .linkgate .accordion-item .accordion-header:hover a{color:#fff}
.linkgate .accordion a.abtacordian,.accordion-header h1 > a{border-bottom:none!important;
background:none!important;display:inline-block!important;padding:0!important;font-family:open_sansregular;font-size:14px}
.accordion1 .accordion-header h1{float:left;font-size:15px;font-weight:400;margin:0;line-height:1em;color:#00a861;}
.inrright_cntr .gate p {
    font-size: 15px;
    line-height: 1.5em;
    padding: 3px 0 0 11px;
    text-align: justify;
    font-weight: 400;
}
.inrright_cntr .gate ul li {
    padding: 0px 0;
    font-size: 14px;
    margin: 0 0 0 5px;
    font-family: Arial, Helvetica, sans-serif;
    list-style: disc;
}
@media (max-width: 991px) and (min-width: 768px)
{
.navbar-nav > li > a {
    padding: 10px;
}	
}

</style>	
<section>
 <div class="container">
 <div class="row">

    <!-- for Quick Link Section -->
	<div class="col-md-4">
   <div class="qklink_wpr">
    <h1 class="inr_title"><i class="sprite qklink"></i>Categories</h1>
    <div class="linkgate"> 
        <div class="accordion1">
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/elitmus#gate">What Is Elitmus</a>
         </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		 <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/elitmusSyllabus#gate">Elitmus Syllabus</a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/examPattern#gate">Examination Pattern
		<span class="newimg"></span>
		</a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
	    <a href="<?php echo base_url();?>pages/registrationProcess#gate">Registration Process
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/cryptArithmetic#gate">CryptArithmetic Videos
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		<div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
        <a href="<?php echo base_url(); ?>pages/companiesHiringthrowElitmus#gate" class="active">Companies hiring through Elitmus
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		 <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
        <a href="<?php echo base_url(); ?>pages/previousQuestions#gate">Previous Questions
        </a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url(); ?>pages/faq#gate"> Frequently Asked Questions</a>
		</h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        </div>
   </div>
</div></div>
	<div class="col-md-8">
	<div class="inrright_cntr" style="box-shadow:none;">
    <div class="gate" id="gate">
        <h1>Companies hiring through Elitmus</h1>

<p>&nbsp;</p>
<div class="row">
<table class="table">
<tbody>
<tr>
<td>Ericsson(ICT)</td>
<td>DELL International Services</td>
</tr>
<tr>
<td>HCL Technologies</td>
<td>Hewlett Packard(HP)</td>
</tr>
<tr>
<td>Accenture</td>
<td>Ikanos Communications India Private Limited</td>
</tr>
<tr>
<td>Innovation Technology Transfer India</td>
<td>L &T Infotech(Global IT Services And Solution Provider)</td>
</tr>
<tr>
<td>Microsoft India Private Limited</td>
<td>Motorola India Electronics Private Limited</td>
</tr>
<tr>
<td>L &T Infotech(Global IT Services And Solution Provider)</td>
<td>CGI</td>
</tr>
</tbody></table>
 <p><strong> There are many big companies hiring through eLitmus pH test score but few companies see the sectional cut off and few companies see the overall cutoff. So if you are not happy with your current pH score then you must take retest.
                     </strong></p>
                <div class="col-md-1"></div>

                <div class="col-md-10">
                    <!--div id="cont-p">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-12">Ericsson(ICT)</div>
                            <div class="col-md-6 col col-xs-12 col-sm-12">DELL International Services</div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">HCL Technologies</div>
                            <div class="col-md-6">Hewlett Packard(HP)</div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">Accenture</div>
                            <div class="col-md-6">Ikanos Communications India Private Limited</div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">Innovation Technology Transfer India</div>
                            <div class="col-md-6">L &T Infotech(Global IT Services And Solution Provider)</div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">Microsoft India Private Limited</div>

                            <div class="col-md-6">Motorola India Electronics Private Limited</div>
                        
                        </div>
                        <div class="row">
                            <div class="col-md-6">L &T Infotech(Global IT Services And Solution Provider)</div>
                            <div class="col-md-6">CGI</div>
                        </div>
                     <strong> There are many big companies hiring through eLitmus pH test score but few companies see the sectional cut off and few companies see the overall cutoff. So if you are not happy with your current pH score then you must take retest.
                     </strong></p>
                    </div-->
					
                </div>
      </div>
      </div>
      </div> 
      </div>
 </div>
</section>
