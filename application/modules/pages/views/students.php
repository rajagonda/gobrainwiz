
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Our Success  <span class="fbold">Stories</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active"><a>Students</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row">
                <!-- col -->
				<?php 
					
				foreach($video_testimonials as $vt){
					$img_url = $img_url = base_url().'upload/topper/'.$vt->voice_image;
					?>
                <div class="col-lg-6">
                    <div class="student-col d-flex position-relative">
                        <figure class="align-self-center">
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".student-testimonial"><img src="<?php echo $img_url;?>"></a>
                        </figure>
                        <article class="align-self-center">
                            <h2 class="h5"><?php echo $vt->student_name;?></h2>
                            <p class="d-flex justify-content-between small">
                                <span><?php echo $vt->student_native_place;?></span>
                                <span class="fblue fbold">Placed in <?php echo $vt->company;?></span>
                            </p>
                            <p class="quote-student">
                            <?php echo $vt->voice_description;?>
                            </p>
                        </article>

                        <div class="linktext d-flex" >
                            <h6 class="align-self-end pr-4 fblue">Watch Video</h6>
                            <div class="video-testimonil-link" data-toggle="modal" data-target=".student-testimonial" data-url="<?php echo $vt->student_video_url;?>">
                                <a href="javascript:void(0)"> 
                                    <span class="icon-play icomoon"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
				<?php }?>
                <!--/ col -->








             </div>
             <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    
    <!-- Modal video -->
    <div class="modal fade video-modal student-testimonial"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" data-keyboard="false" data-backdrop="static" aria-modal="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <iframe allow="autoplay" id="ytplayer" class="modalvideo" width="100%" src="" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->


    <script>
        $('.video-testimonil-link').on( "click",function () {
            var url = $(this).attr('data-url');
            $('.video-modal').on('hidden.bs.modal', function (e) {
                $('.video-modal iframe').attr('src', '');
            });

            $('.video-modal').on('show.bs.modal', function (e) {
                    $('.video-modal iframe').attr('src', url+'?rel=0&amp;autoplay=1');
            });
        });
   </script>
