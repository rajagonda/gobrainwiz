<!DOCTYPE html>
<html>
  <head>
    <title>Brainwiz</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_url();?>images/img/favicon.png">
    <link href="<?php echo assets_url();?>new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo assets_url();?>new/css/shareus.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="<?php echo assets_url();?>new/js/jquery.cropit.js"></script>
    <style>
	.btn-primary {
    color: #ffffff;
    background-color: #2d8ecb;
    border-color: #547183;
    width: 116px;
}
      .cropit-preview {
    background-color: rgba(104, 162, 208, 0.71);
    background-size: cover;
    border: 2px solid rgba(43, 47, 130, 0.84);
    border-radius: 3px;
    margin-top: 7px;
        width: 250px;
        height: 250px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .cropit-preview-background {
        opacity: .2;
        cursor: auto;
      }

      .image-size-label {
        margin-top: 10px;
       font-weight: 600;
      }

      input, .export {
        position: relative;
        z-index: 10;
        display: block;
      }

      button {
        margin-top: 10px;
      }
</style>
<style>

body{
background-image: url(../assets/images/back.png);
 background-size: cover;
 background-position: center center;
  background-attachment: fixed;
}

.frame {
  border: 1px solid #414193;
  padding: 5px;
  margin-bottom: 20px;
}

.frame > img { width: 100%; }

#controls { margin-bottom: 40px; }

#controls button {
  margin: 0 5px;
  min-width: 40px;
  color: #555;
  font-size: 16px;
  font-weight: bold;
  line-height: 30px;
}

#controls button:hover { color: #333; }

#data {
  list-style: none;
  display: inline-block;
  padding: 0;
  margin: 0;
}

#data .column {
  display: inline-block;
  margin: 0 15px;
}

#data li {
  margin-bottom: 10px;
  font-weight: bold;
  text-align: left;
}

#data span {
  display: inline-block;
  font-weight: normal;
}

.form-control {
	    border: 1px solid #5c91bc;
}
.rotate-ccw{
       border-color: #feffff;
    background-color: #1d72b3;
    font-size: 17px;
    color: white;
        margin-left:32px;
}
.rotate-cw{
    border-color: #feffff;
    background-color: #1d72b3;
    font-size: 17px;
    color: white;
}
.btn.focus, .btn:focus, .btn:hover{
	    color: #6de2f1;
}
.btn-default {
	color:white;
    background-color: #4f697d;
    border-color: #ccc;
}
</style>
<!--Pop up Styles start-->
<style>

body:before {
  content: "";
  visibility: hidden;
  display: inline-block;
  vertical-align: middle;
  height: 100%;
}
#maincontent {
  display: inline-block;
  vertical-align: middle;
}

#overlay {
  position: absolute;
  opacity: 0;
  top: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
  visibility: hidden;
  transition: all 1s ease;
}
#popup {
  position: absolute;
  background: #fff;
    width: 477px;
    height: 436px;
  left: 32%;
  margin-top: -280px; 
  z-index: -1;
  visibility: hidden;
}
.popupcontent {
  padding: 10px;

}
#button {
  cursor: pointer;
}
@-webkit-keyframes pop-swirl {
  0% {
    transform: scale(0) rotate(360deg);
  }

  60% {
    transform: scale(0.8) rotate(-10deg);
  }

  100% {
    transform: scale(1) rotate(0deg);
  }
}
@-moz-keyframes pop-swirl {
  0% {
    transform: scale(0) rotate(360deg);
  }

  60% {
    transform: scale(0.8) rotate(-10deg);
  }

  100% {
    transform: scale(1) rotate(0deg);
  }
}
@-ms-keyframes pop-swirl {
  0% {
    transform: scale(0) rotate(360deg);
  }

  60% {
    transform: scale(0.8) rotate(-10deg);
  }

  100% {
    transform: scale(1) rotate(0deg);
  }
}
@-o-keyframes pop-swirl {
  0% {
    transform: scale(0) rotate(360deg);
  }

  60% {
    transform: scale(0.8) rotate(-10deg);
  }

  100% {
    transform: scale(1) rotate(0deg);
  }
}
@keyframes pop-swirl {
  0% {
    transform: scale(0) rotate(360deg);
  }

  60% {
    transform: scale(0.8) rotate(-10deg);
  }

  100% {
    transform: scale(1) rotate(0deg);
  }
}
@-webkit-keyframes anvil {
  0% {
    transform: scale(5) rotate(0);
    opacity: 0;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0);
  }

  50% {
    transform: scale(1) rotate(-0.2deg);
    opacity: 1;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0.5);
  }

  75% {
    transform: scale(1) rotate(0.2deg);
    opacity: 1;
    box-shadow: 0 0 250px rgba(241, 241, 241, 0.5);
  }

  100% {
    transform: scale(1) rotate(0);
    opacity: 1;
    box-shadow: 0 0 500px rgba(241, 241, 241, 0);
  }
}
@-moz-keyframes anvil {
  0% {
    transform: scale(5) rotate(0);
    opacity: 0;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0);
  }

  50% {
    transform: scale(1) rotate(-0.2deg);
    opacity: 1;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0.5);
  }

  75% {
    transform: scale(1) rotate(0.2deg);
    opacity: 1;
    box-shadow: 0 0 250px rgba(241, 241, 241, 0.5);
  }

  100% {
    transform: scale(1) rotate(0);
    opacity: 1;
    box-shadow: 0 0 500px rgba(241, 241, 241, 0);
  }
}
@-ms-keyframes anvil {
  0% {
    transform: scale(5) rotate(0);
    opacity: 0;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0);
  }

  50% {
    transform: scale(1) rotate(-0.2deg);
    opacity: 1;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0.5);
  }

  75% {
    transform: scale(1) rotate(0.2deg);
    opacity: 1;
    box-shadow: 0 0 250px rgba(241, 241, 241, 0.5);
  }

  100% {
    transform: scale(1) rotate(0);
    opacity: 1;
    box-shadow: 0 0 500px rgba(241, 241, 241, 0);
  }
}
@-o-keyframes anvil {
  0% {
    transform: scale(5) rotate(0);
    opacity: 0;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0);
  }

  50% {
    transform: scale(1) rotate(-0.2deg);
    opacity: 1;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0.5);
  }

  75% {
    transform: scale(1) rotate(0.2deg);
    opacity: 1;
    box-shadow: 0 0 250px rgba(241, 241, 241, 0.5);
  }

  100% {
    transform: scale(1) rotate(0);
    opacity: 1;
    box-shadow: 0 0 500px rgba(241, 241, 241, 0);
  }
}
@keyframes anvil {
  0% {
    transform: scale(5) rotate(0);
    opacity: 0;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0);
  }

  50% {
    transform: scale(1) rotate(-0.2deg);
    opacity: 1;
    box-shadow: 0 0 0 rgba(241, 241, 241, 0.5);
  }

  75% {
    transform: scale(1) rotate(0.2deg);
    opacity: 1;
    box-shadow: 0 0 250px rgba(241, 241, 241, 0.5);
  }

  100% {
    transform: scale(1) rotate(0);
    opacity: 1;
    box-shadow: 0 0 500px rgba(241, 241, 241, 0);
  }
}




.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.fa-facebook{
	color: white!important;
}

#content #popup{ 
text-align:center;
}
#cong-text{
	    font-size: 18px;
    font-family: cursive;
}
#image{
	    height: 93px;
    margin-top: 81px;
    border-top: 2px solid green;
    border-bottom: 2px solid green;
    width: 470px;
    margin-left: -10px;
}

#spantext{
	    color: #008b9a;
    font-weight: 500;
    font-size: 14px;
	    margin-left: 73px;
}
#text-area{
    margin-right: -10px;
    margin-left: -10px;
    background-color:#ffffff;
	margin-top: 210px;
}


#info{
	margin-top: 213px;
    width: 34.666667%;
}
.cropit-preview{
    position: relative;
    width: 150px;
    height: 165px;
}
.form-group {
    margin-bottom: 13px;
}
.btn-default:hover {
    color: #333;
    background-color: #5c94bf;
    border-color: #1d72b3;
}
#span{
	font-size: 15px;
    color:#03A9F5;
    font-weight: 500;
}
</style>
<!--Style end -->
</head>
<body>
<div id="back">
<section id="shareus">
<div class="container">
<div class="row">
<div class="col-sm-12" id="div">
  <div id="content">
  <a href="index.php" title=""><img src="<?php echo assets_url();?>images/img/logo-primary.png" class="img-responsive" alt=""/></a>
  <form class="form-horizontal" role="form" action="#" method="post" enctype="multipart/form-data">
  <div class="col-sm-4">
  <div class="image-editor">
      <input type="file" class="cropit-image-input" name="file">
      <div class="cropit-preview"></div>
      <div class="image-size-label">
        Resize image
      </div>
      <input type="range" class="cropit-image-zoom-input" accept="image/*">
      <div class="rotate-ccw btn">&lt;</div>
      <div class="rotate-cw btn">&gt;</div>
      <div class="export btn">Crop</div><br>
      </div>
  </div>
  <div class="col-sm-8">
  <div class="form-group">
            <label class="col-sm-3 control-label">Name:</label>
            <div class="col-sm-8">
              <input class="form-control" type="text" name="name" value="" placeholder="Enter Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Mobile No:</label>			
            <div class="col-sm-8">
              <input class="form-control" type="text" value="" name="mobile" placeholder="Phone Number (format: xxxx-xxx-xxxx)*">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Email:</label>
            <div class="col-sm-8">
              <input class="form-control" type="text" name="email" value="" placeholder="Enter a Valid Email id">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">College Name:</label>
            <div class="col-sm-8">
              <input class="form-control" type="text" name="cname" value="" placeholder="Enter User Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Company:</label>
            <div class="col-sm-8">
              <input class="form-control" type="password" name="company" value="" placeholder="Enter Password">
            </div>
          </div>
         <div class="form-group">
            <label class="col-sm-3 control-label">Feedback:</label>
            <div class="col-sm-8">
              <textarea name="feedbk" class="form-control" placeholder="Give Your Feedback Here">
			  </textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-8">
              <input type="button" name="sub" id="button" class="btn btn-primary" data-toggle="modal" data-target="#popup1" value="Submit">
              <span></span>
              <input type="reset" class="btn btn-default" style="color:white;" value="Cancel">
<div class="modal fade" id="popup1" role="dialog">
<div data-pop="slide-down" id="popup">
<!--button type="button" class="close" data-dismiss="modal">&times;</button-->
<div class="popupcontrols">
<span id="popupclose" class="close" data-dismiss="modal">&times;</span>
</div>
<div class="popupcontent">
<div id="logo" style="height:30px;background-color:white;">
<img src="<?php echo assets_url();?>images/popupback.jpg" id="outer-img"/>
<img src="<?php echo assets_url();?>images/img/logo-primary.png" id="popup-logo" alt=""/>
<p id="congp">Congratulations to our Student</p>
<div class="col-sm-2">
<img src="<?php echo assets_url();?>images/Rupal_Gourha.jpg" id="cimg"/>
<br><p id="pname">Rupal Gourha<br><span id="span">(Placed in HP)</span></p>
<img src="<?php echo assets_url();?>images/silver trophy.gif" id="left-img"/>
<img src="<?php echo assets_url();?>images/silver trophy.gif" id="right-img"/>
</div>
</div>
<div id="text-area">
<p id="text-content">
I never saw such methods and such an approach for learning aptitude
and reasoning.I never saw such methods and such an approach for learning aptitudeand reasoning.
</p><br>
<div class="col-sm-2">
<a href="#" class="btn" id="linkbtn"><span><i class="fa fa-facebook" aria-hidden="true" style="color: white!important;"></i> Share to Let Everyone Know</span></a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
<a href="index.php" style="color:#013f63;text-decoration:underline;">Back to Home</a>
<div id="overlay"></div>
</div> 
</div>
</div>
</section>
</div>
<script type="text/javascript">
    // Initialize Variables
    var closePopup = document.getElementById("popupclose");
    var overlay = document.getElementById("overlay");
    var popup = document.getElementById("popup");
    var button = document.getElementById("button");
    // Close Popup Event
    overlay.onclick = function() {
        overlay.className = '';
        popup.className = '';
    };
    // Close Popup Event
    closePopup.onclick = function() {
        overlay.className = '';
        popup.className = '';
    };
    // Show Overlay and Popup
    button.onclick = function() {
        overlay.className = 'show';
        popup.className = 'show';
    }
</script>
    <script>
      $(function() {
        $('.image-editor').cropit({
          exportZoom: 1.25,
          imageBackground: true,
          imageBackgroundBorderWidth: 20,
          imageState: {
            src: 'http://lorempixel.com/500/400/',
          },
        });

        $('.rotate-cw').click(function() {
          $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw').click(function() {
          $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function() {
          var imageData = $('.image-editor').cropit('export');
          window.open(imageData);
        });
      });
    </script>  </body>
</html>
