
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>
<style type="text/css">
#overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: #000;
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	z-index: 100;
	display: none;
}
.cnt223 a{
	text-decoration: none;
}
.popup{
	width: 100%;
	margin: 0 auto;
	display: none;
	position: fixed;
	z-index: 999999999;
}
.cnt223{
	min-width: 300px;
	width: 40%;
	min-height: 150px;
	margin: 84px auto;
	position: relative;
	z-index: 103;
	padding: 10px;
	margin-top:-40px;
	border-radius: 5px;
	box-shadow: 0 2px 5px transparent;
}
.cnt223 p{
	clear: both;
	color: #555555;
	text-align: justify;
	background:#f0ad4e;
	padding:1px;
}
.cnt223 p a{
	color: #d91900;
	font-weight: bold;
}
.cnt223 .x{
	float: right;
	height: 35px;
	left: 22px;
	position: relative;
	top: -25px;
	width: 34px;
}
.cnt223 .x:hover{
	cursor: pointer;
}
.cnt223 a.closepop {
    float: right;
    font-size: 14px;
    font-weight: 500;
    line-height: 1;
    color: #fff;
    filter: alpha(opacity=20);
    opacity: 1;
    background-color:#f0ad4e;
    padding: 8px 11px;
    margin: -8px 0px;
    width: 32px;
    height: 32px;
}
#popupfoot{font-size:12px;text-align:center;}
.cnt223 p img{height:auto;}
.cnt223 p a.popup2{cursor:default;}
@media (min-width:768px){
	.cnt223 p a.popup2{border-left: 11px solid #fff;}
}
@media (max-width:767px){
	.cnt223 p a.popup2{border-left: 11px solid #fff;}
}
.carousel-control.left{
	background-image: none;
}
.carousel-control.right{
	background-image: none;
}
</style>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}
th{color:#ffffff;
}
th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #88edf7}
.carousel-control .glyphicon-chevron-left, 
.carousel-control .glyphicon-chevron-right, 
.carousel-control .icon-next, 
.carousel-control .icon-prev{
position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
}
.carousel-control .glyphicon-chevron-left, 
.carousel-control .glyphicon-chevron-right, 
.carousel-control .icon-next, .carousel-control .icon-prev{
font-size:20px;
}
#icon-left{
    margin-top: 83px;
    height: 20px;
}
#icon-right{
    margin-top: 83px;
    height: 20px;
}
#cta {
    padding: 0;
    margin: 0 auto;
    position: relative;
}

#cta .cta-col {
    padding: 0;
}
#cta .cta-circle {
    width: 8.429em;
    height: 7.429em;
    display: block;
    position: relative;
    border: 0.357em solid #ffffff;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    margin: -1.214em auto 0;
    background:rgb(103, 188, 250);
}
#cta .cta-n-icon {
    background: url(<?php echo assets_url();?>images/img/88.png) no-repeat center center transparent;
    -webkit-background-size: 5.357143em 4.714286em;
    -moz-background-size: 5.357143em 4.714286em;
    -o-background-size: 5.357143em 4.714286em;
    background-size: 5.357143em 4.714286em;
}
#cta .cta-v-icon {
    background: url(<?php echo assets_url();?>images/img/66.png) no-repeat center center transparent;
    -webkit-background-size: 5.357143em 4.714286em;
    -moz-background-size: 5.357143em 4.714286em;
    -o-background-size: 5.357143em 4.714286em;
    background-size: 5.357143em 4.714286em;
}
#cta .cta-d-icon {
    background: url(<?php echo assets_url();?>images/img/77.png) no-repeat center center transparent;
    -webkit-background-size: 5.357143em 4.714286em;
    -moz-background-size: 5.357143em 4.714286em;
    -o-background-size: 5.357143em 4.714286em;
    background-size: 5.357143em 4.714286em;
}
#cta .cta-p-icon {
    background: url(<?php echo assets_url();?>images/img/practice.png) no-repeat center center transparent;
    -webkit-background-size: 5.357143em 4.714286em;
    -moz-background-size: 5.357143em 4.714286em;
    -o-background-size: 5.357143em 4.714286em;
    background-size: 5.357143em 4.714286em;
}
#cta .cta-icon {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
}
@-webkit-keyframes tada {
  0% {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%, 20% {
    -webkit-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
  }

  30%, 50%, 70%, 90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%, 60%, 80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  100% {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes tada {
  0% {
    -webkit-transform: scale3d(1, 1, 1);
    -ms-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%, 20% {
    -webkit-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
    -ms-transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(.9, .9, .9) rotate3d(0, 0, 1, -3deg);
  }

  30%, 50%, 70%, 90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    -ms-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%, 60%, 80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    -ms-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  100% {
    -webkit-transform: scale3d(1, 1, 1);
    -ms-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.tada {
  -webkit-animation-name: tada;
  animation-name: tada;
}
#cta .cta-title {
    text-align: center;
    margin: 0.75em auto 3em;
}
.animated {
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}
.animated:hover {
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
}
h4, .xs-title {
    font-size: 1.357em;
    margin: 0 auto 1.125em;
    font-weight: normal;
    line-height: normal;
}
.il {
font-size: 20px;
    color: white;
    font-weight: 600;
}
.tdn, .tdn:hover, .tdn:focus, .tdn:active {
    text-decoration: none;
}
a:hover, a:focus {
    color: #00BCDF;
}
</style>
<style>
/*==================================================
 * Effect 2
 * ===============================================*/
.effect2
{
  position: relative;
      height: 392px;
}
.effect2:before, .effect2:after
{
  z-index: -1;
  position: absolute;
  content: "";
  bottom: 15px;
  left: 10px;
  width: 50%;
  top: 80%;
  max-width:300px;
  background: #777;
  -webkit-box-shadow: 0 15px 10px #777;
  -moz-box-shadow: 0 15px 10px #777;
  box-shadow: 0 15px 10px #777;
  -webkit-transform: rotate(-3deg);
  -moz-transform: rotate(-3deg);
  -o-transform: rotate(-3deg);
  -ms-transform: rotate(-3deg);
  transform: rotate(-3deg);
}
.effect2:after
{
  -webkit-transform: rotate(3deg);
  -moz-transform: rotate(3deg);
  -o-transform: rotate(3deg);
  -ms-transform: rotate(3deg);
  transform: rotate(3deg);
  right: 10px;
  left: auto;
}
.fa-youtube {
  background: #bb0000!important;
  color: white!important;
}
.fa-facebook {
  background: #3B5998!important;
  color: white!important;
}
.fa-google-plus {
  background: #dd4b39!important;
  color: white!important;
}
.fa:hover {
    opacity: 0.7;
}
table tr:nth-child(odd) td{
	color:white;
}
.owl-pagination{
	margin-top: -27px;
}
.testimonials .item img {
    text-align: center;
    width: 123px;
    height: 132px;
}
.testimonials.three .carousel-indicators .active {
    background-color: #1b1b42;
}
.testimonials.three .carousel-indicators li {
    background-color:rgba(121, 85, 72, 0.45);
    border: none;
}
.carousel-indicators{
	font-size: 12px;
}
.inner-wrapper{
	    margin: 24px 0;
		    height: 780px;
}
</style>
<script type='text/javascript'>
	$(function(){
		var overlay = $('<div id="overlay"></div>');	
		overlay.show();
		overlay.appendTo(document.body);
		$('.popup').show();
		$('.closepop').click(function(){
			$('.popup').hide();
			overlay.appendTo(document.body).remove();
			return false;
		});
		$('.x').click(function(){
			$('.popup').hide();
			overlay.appendTo(document.body).remove();
			return false;
		});
	});
</script>
	<div class="popup">
		<div class="cnt223">
			<a style="float:right" href="" class="closepop">X</a>
			<br/>
			<p><img src="<?php echo assets_url();?>images/img/popup.jpg" alt=""></p>
		</div>
	</div>
<!-- Banner Wrapper Start -->
	<div class="banner-wrapper">
		<div id="banner" class="owl-carousel">			
			<div class="item"><img src="<?php echo assets_url();?>images/img/banner1.jpg" alt="">
			<!-- <div class="banner-caption">
					<h2>Trained Thousands of students across INDIA</h2>
					
					<!-- <a href="javascript:void(0)">Read More <i class="fa fa-caret-right" aria-hidden="true"></i> </a>
				</div> -->
			</div>
			<div class="item"><img src="<?php echo assets_url();?>images/img/banner2.jpg" alt="">
				<!-- <div class="banner-caption">
					<h2>A Premium Plotted Development</h2>
					<h3>Located between Airport & Adibatla</h3>
					<!-- <a href="javascript:void(0)">Read More <i class="fa fa-caret-right" aria-hidden="true"></i> </a> 
				</div> -->
			</div>
			<div class="item"><img src="<?php echo assets_url();?>images/img/banner4.jpg" alt="">	
			</div>
			<div class="item"><img src="<?php echo assets_url();?>images/img/banner5.jpg" alt="">
			</div>
		</div>
			<div class="overlay">
                <div class="container">
                    <div class="row">
                <div class="col-md-8 col-sm-8">
				</div>	
                        <div class="col-md-4 col-sm-4" id="top-form">
	
                           <div class="signup-header">
                                <h3 class="form-title text-center"><span>Like to know about CRT Course?</span></h3>
		 						<span id="successmsg" class="text-center" style="display:none; color:white"></span>
                                <span id="failuremsg" class="text-center" style="display:none; color:red"></span>
                                <form action="#" method="post" class="form-header" name="enquiry_form" id="enquiry_form" onsubmit="return validateForm()" >
								
                                    <div class="form-group" >
										<input class="form-control input-lg" id="username" name="username" type="text" 
										placeholder="Your name*" style="font-size: 15px;">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control input-lg" type="email" id="email" name="email" placeholder=
										"Email Id*" style="font-size: 15px;">
                                    </div>
									<div class="form-group">
									  <input class="form-control input-lg" type="text" id="mobile" name="mobile" 
									  placeholder="Phone Number (format: xxxx-xxx-xxxx)*" style="font-size: 15px;">
									</div>
									<div class="form-group">
									  <textarea name="message" id="message" class="form-control" placeholder="Leave any query.." 
									  ></textarea>
									</div>
									<div class="form-group last">
                                        <button type="submit" class="btn btn-warning btn-block btn-lg">Submit</button>
                                     
                                    </div>
							    </form>
                            </div>			

                        </div>
                    </div>
                </div> 
            </div>
	</div>
	
	<section class="services-wrapper" id="section1">
<div class="container-fluid" style="margin-top: -43px;margin-bottom: -55px;">
<div class="row">
<div id="cta">
	<div id="cta-bkg-top"></div>
	<div class="container">
		<div class="clearfix">
			<div class="cta-col col-xs-3">
				<div class="cta-circle">
					<a href="#"><div class="cta-icon animated cta-v-icon tada"></div></a>
				</div>
				<h3 class="cta-title xs-title"><a class="il tdn" href="#">Weekly Schedule</a></h3>
			</div>
			<div class="cta-col col-xs-3">
				<div class="cta-circle">
					<a href="<?php echo base_url(); ?>videos/homevideos"><div class="cta-icon animated cta-d-icon tada"></div></a>
				</div>
				<h3 class="cta-title xs-title"><a class="il tdn" href="<?php echo base_url(); ?>videos/homevideos">Videos</a></h3>
			</div>
			<div class="cta-col col-xs-3">
				<div class="cta-circle">
					<a href="<?php echo base_url(); ?>onlinetest/login"><div class="cta-icon animated cta-n-icon tada"></div></a>
				</div>
				<h3 class="cta-title xs-title"><a class="il tdn" href="<?php echo base_url(); ?>onlinetest/login">Online Test</a></h3>
			</div>
			
			<div class="cta-col col-xs-3">
				<div class="cta-circle">
					<a href="#"><div class="cta-icon animated cta-p-icon tada"></div></a>
				</div>
				<h3 class="cta-title xs-title"><a class="il tdn" href="#">Practice Test</a></h3>
			</div>
			
		</div>
	</div>
</div>
	</div>
	</div><hr>
	<div class="container" id="latest-updates">	
	
		<!-- <p class="des">Your Perfect Future.</p> -->
		<div class="row">	
		
			<div class="col-sm-8" style="background-color: #0097a6; border-radius: 21px;height: auto;"><br>
			<h2 style="margin:0 0 23px 0!important;"><b style="color:#ffffff;font-size: 20px; font-weight:800;">Batch Timings</b><span class="sub-text"></span></h2>

                                                    <table class="table">
                                                        <tr>
                                                            <th>Course</th>

                                                            <th>Date</th>
                                                            <th>Timing</th>
                                                            <th>Faculty</th>
                                                            
                                                        </tr>
                                                                <?php
                                                                if($batches !=''){
                                                                foreach($batches as $value){

                                                                    //Our "then" date.
                                                                $then =  $value->start_date;
                                                                 
                                                                //Convert it into a timestamp.
                                                                $then = strtotime($then);
                                                                 
                                                                //Get the current timestamp.
                                                                $now = time();
                                                                 
                                                                //Calculate the difference.
                                                                $difference = $now - $then;
                                                                 
                                                                //Convert seconds into days.
                                                                $days = floor($difference / (60*60*24) );
                                                                 
                                                              
                                                                ?>
                                                                <tr>
                                                                <td><?php echo $value->subject_name;?>
                                                                    <?php if($days <30) { ?>
                                                                    <img src="<?php echo assets_url();?>images/new_icon.gif"></td>
                                                                    <?php } ?>
                                                                <td><?php echo date('jS M', strtotime($value->start_date));?></td>
                                                                <td><?php echo $value->from;?> - <?php echo $value->to;?> </td>
                                                                <td><?php echo $value->faculty_name;?> </td>
                                                                </tr>
                                                                <?php }} ?>
                                                    </table>
                                               
			</div>
	
			<div class="col-sm-3" id="col" style="background-color: #0097a6;height: 286px; border-radius: 21px;margin-left:10px;"><br>
			<h2 style="margin:0 0 23px 0!important;"><b style="color:#ffffff;font-size: 20px;font-weight:800;">Latest Updates</b><span class="sub-text"></span></h2>
          <hr>
		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators hidden">
	<?php
                                                    if($updates !=''){
                                                    foreach($updates as $i=> $value){
                                                    ?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"<?php echo !$i ? ' class="active"' : ''; ?>></li>
													<?php }}?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
	<?php $item_class = 'active';?>
	 <?php
                                                    if($updates !=''){
                                                    foreach($updates as $value){
                                                    ?>
    <div class="item <?php echo $item_class;?>">
        <img id="update" src="<?php echo base_url();?>upload/updates/<?php echo $value->update_image; ?>" 
		style="width: 247px;height: 171px;">
    </div>
	  <?php $item_class = '';?>
	<?php }} ?>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
       <span class="left"><img id="icon-left" src="<?php echo assets_url();?>images/img/left1.png"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
       <span class="right"><img id="icon-right" src="<?php echo assets_url();?>images/img/right1.png"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
          </div>
			
		</div>
	</div>
</section>
<section class="callouts-wrapper" style="margin-top: 24px;">
  <div class="container">
    <div class="row">
		<div class="col-sm-6">
			<div class="choose">
      <h2 style="font-size: 25px; color:#04632f;">Why Most of the Students Prefer<b style="color:#1c1b61; font-weight:800;"><br>BRAINWIZ!</b></h2>
		<div class="course-box text-left effect2">
							<div class="course-heading">
								<h3>Comprehensive Coverage</h3>
							</div>
							<p style="    margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> More than 120 hours of Training with 50+ topics of Aptitude</p>
							<p style="        margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Fresher’s can be easily Placed</p>
							 <p style="    margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Basic level to advanced level</p>
                             <p style="    margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Coverage of whole syllabus (Aptitude, Verbal and Soft skills)</p>
                             <p style="    margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Extra focus for AMCAT And E-LITMUS</p>
                             <p style="    margin: 18px 0px 18px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Sunday Special sessions covering all MNC’s Questions</p>
							 <div class="call-action-button1"><a href="" style="color: white;">READ MORE</a></div>
						</div>
						
	  </div>
		</div>
		<div class="col-sm-6">
		<h2 style="font-size: 25px; color:#04632f;">Video Gallery <b style="color:#1c1b61; font-weight:800;">BRAINWIZ!</b></h2>
			<div class="callout1">
			<div class="course-heading">
								<h3>Trained Thousands of students across INDIA</h3>
							</div>
				<video width="100%" height="auto" controls autoplay >
  <source src="<?php echo base_url();?>upload/Videos/RAMA7308.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video><br>
			<div class="call-action-button3"><a href="<?php echo base_url(); ?>videos/homevideos" style="color: white;">VIEW MORE</a></div>
			</div>
		</div>
    </div>
  </div>
</section>

<style>
#top{
	    text-align: center;
    width: 123px;
    height: 132px;
	line-height: 100px;
    color: #fff;
    font-size: 40px;
    border: 2px solid #f7f5f4;
    margin: 0 0 30px;
    padding: 0;
    border-radius: 48%;
}
#test{
	background-image:url(<?php echo assets_url();?>images/img/mid-pattern.png);height:476px;
}
#reg-section{
	margin:-135px 0px -28px;
}
</style>
<section class="inner-wrapper">
  <div class="container-fluid" id="test">
  <h3 style="color:#22228c;text-align: center;margin-top: 33px;">TESTIMONIALS</h3>
    <div class="testimonials three" style="margin-top: 22px;">
      <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
        <!-- Testimonials Indicators -->
        <ol class="carousel-indicators">
		<?php 
                                                    if($toppers!=''){
                                                    $j =0;
                                                    foreach($toppers as $j=> $value){                                                    
                                                    ?>
          <li data-target="#myCarousel1" data-slide-to="<?php echo $j;?>"<?php echo !$j ? ' class="active"' : ''; ?>></li>
													<?php }}?>
        </ol>
        <!-- Testimonials slides -->
        <div class="carousel-inner" role="listbox">
		<?php $item1 = 'active';?>
		<?php 
                                                    if($toppers!=''){
                                                    foreach($toppers as $value){                                                    
                                                    ?>
          <div class="item <?php echo $item1;?>">
            <blockquote>
			<img src="<?php echo base_url();?>upload/topper/<?php echo $value->voice_image;?>" id="top" alt="Testimonials">
              <p style="color:#22228c;"><?php echo $value->voice_description;?></p>
              <h3><?php echo $value->student_name;?></h3>
            </blockquote>
          </div>
		  <?php $item1 = '';?>
          <?php
                                                        } } 
                                                        ?>
        </div>
      </div>
    </div>
  </div>
</section>

<!--section class="choose-wrapper">	
	  <div class="testimonials">
    <div class="container" id="testimon-con">
	<h3 style="color: #fff;">TESTIMONIALS</h3>
	<div class="row">
	<div class="col-sm-2">
	</div>
	 <div class="col-sm-8">
	 <div id="about" class="owl-carousel">
	 	                                                <?php 
                                                    if($toppers!=''){
                                                    $i =0;

                                                    foreach($toppers as $value){                                                    
                                                    ?>
	 <div class="item">
        <div class="tes-wrapper"> <span> <i><img src="<?php echo base_url();?>upload/topper/<?php echo $value->voice_image;?>">
		</i></span>
          <p><?php echo $value->voice_description;?></p>
          <h3><?php echo $value->student_name;?></h3>
        </div>
      </div>
	 <?php
                                                        } } 
                                                        ?>
	  </div>
	  </div>
	  
	 <div class="col-sm-2">
	</div>
	 </div>
	 </div>
  </div>	
	
</section-->
<section id="reg-section">
<div class="container">
                <div class="row">
					<div class="col-md-6 col-sm-6">
						<img id="reg-img" src="<?php echo assets_url();?>images/img/register.jpg" class="img-responsive" alt="" style="margin-top: -2px;">
					</div>
					<div class="col-md-6 col-sm-6 call-to-right">
						<h1 style="color: #fff;">How to Register @ BRAINWIZ?</h1>
						<p style="color: #fff;">Please fill your details in above web-form.</p>
						<div class="no-padding">
							<a href="<?php echo base_url();?>pages/home" class="call-action-button">Register Now</a>
						</div>
					</div>
				</div>
			</div>
			</section>
			
