
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Gate Exam </h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:;">Examinations</a></li>
                        <li class="breadcrumb-item active"><a>Gate</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
               <!-- row -->
               <div class="row">
                    <!-- col 8 -->
                    <div class="col-lg-8">
                        <h2 class="h2 pb-2">GATE Exam Pattern 2020: GATE Syllabus 2020, Test Structure for all Papers and Subject-wise marking Scheme</h2>
                        <h3 class="h4">About GATE</h3>
                        <p>GATE score facilitates admissions Post Graduate Programmes such as ME, M.Tech, MS, and direct PhD in institutes like IIT and IISc along with financial assistance offered by MHRD. Scores from GATE are also accepted by PSUs of the Indian government.</p>

                        <h3 class="h4">GATE EXAM 2020:</h3>
                        <p>Exam Pattern 2020 consists of all important information, such as a total number of questions, the number of sections, types of questions asked, and marking scheme of the exam. So, candidates planning to appear for GATE 2020 exam must be aware of GATE 2020 Exam Pattern.  Having an idea about the GATE exam pattern will help candidates in their preparation as it will help them formulate the best strategy required for taking the exam successfully.</p>

                        <p>GATE 2020 will be conducted for 25 papers and candidates must choose any one of these papers. GATE will be conducted in online mode. The duration of the GATE exam is 3 hours and the maximum marks are 100. In this article, we will give detailed information about GATE Exam Pattern 2020 to help you with your preparation. Read on to find more about GATE 2020 Paper Pattern</p>

                        <h3 class="h4">GATE Exam Pattern 2020</h3>
                        <p>The highlights of the GATE 2020 exam pattern is tabulated below:</p>
                        <table class="table table-responsive">
                            <tr>
                                <td class="fbold">Mode of Exam</td>
                                <td>Online (Computer-based mode)</td>
                            </tr>

                            <tr>
                                <td class="fbold">Sections</td>
                                <td>Three</td>
                            </tr>

                            <tr>
                                <td class="fbold">Time Duration</td>
                                <td>3 hours</td>
                            </tr>

                            <tr>
                                <td class="fbold">Total Questions</td>
                                <td>65</td>
                            </tr>

                            <tr>
                                <td class="fbold">Question type</td>
                                <td>MCQ and NAT (Numerical Answer Type)</td>
                            </tr>

                            <tr>
                                <td class="fbold">Negative Marking</td>
                                <td>Only for MCQ’s: 1/3rd for 1-mark questions, 2/3rd for 2 marks questions</td>
                            </tr>

                            <tr>
                                <td class="fbold">Total Marks</td>
                                <td>100</td>
                            </tr>
                           
                        </table>

                        <p>General Aptitude acts as a compulsory section for all papers and it carries 15% weightage of the total marks.</p>

                        <h3 class="h4">GATE Exam Pattern 2020: GATE Question Type</h3>
                        <p>The GATE 2020 question paper consists of two types of questions and they are listed below:</p>
                        <ul class="page-list">
                           <ul>
                               <li>Multiple Choice Questions (MCQs): For every question, 4 options will be there out which one is correct.</li>
                               <li>Numerical Answer Type :</li>

                               <ul class="pl-3 page-list">
                                   <li>a. All the answers to these questions will be real numbers where the candidate must enter the answer with the help of virtual keyboard.</li>
                                   <li>b. No choices will be provided for this type of questions.</li>
                                   <li>c. No negative marking will be there for NAT questions.</li>
                               </ul>                           
                        </ul>

                        <h3 class="h4">GATE Exam Pattern 2020 for Various Paper Codes</h3>
                        <p>The GATE 2020 exam pattern for various paper codes are tabulated below:</p>

                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Paper Code</th>
                                    <th>Exam Pattern</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CE, CS, EC, AE, AG, BT, CH, ME, MN, MT, EE, IN, TF, XE, PE, ST and PI</td>
                                    <td><p>General Aptitude– 15%</p>
                                        <p>Engineering Mathematics – 15%</p>
                                        <p>Subject of the Paper –  70%</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    AR, CY, GG, EY, XL, PH and MA
                                    </td>
                                    <td>
                                        <p>General Aptitude – 15%</p>
                                        <p>Subject of the Paper – 85%</p>
                                    </td>
                                </tr>
                            </tbody>                           
                        </table>
                        <h3 class="h4">GATE Exam Pattern 2020: GATE Marking Scheme</h3>
                        <p>The GATE 2020 marking scheme will vary from one code to another. The GATE marking scheme is tabulated below:</p>

                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Paper Code</th>
                                    <th>General Aptitude Marks</th>
                                    <th>Subject Marks</th>
                                    <th>Total Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CS, CY, EC, EE, CE, CH, AR, AE, AG, EY, BT, GG, IN, ME, MA, PH, PI, TF, MN, MT, PE, ST</td>
                                    <td>15</td>
                                    <td>85 </td>
                                    <td>100</td>
                                    
                                </tr>
                                <tr>
                                    <td>XE (Section A + any two sections)</td>
                                    <td>15</td>
                                    <td>15+2*35 </td>
                                    <td>100</td>                                    
                                </tr>
                                <tr>
                                    <td>XL (Section P + any two sections)</td>
                                    <td>15</td>
                                    <td>25+2*30 </td>
                                    <td>100</td>                                    
                                </tr>
                            </tbody>                           
                        </table>

                        <h3 class="h4">GATE Exam Pattern for All Papers</h3>
                        <h3 class="h4">GATE Exam Pattern for General Aptitude</h3>

                        <ul class="page-list pb-3">                          
                               <li>1.	General Aptitude acts as a compulsory section in all GATE papers.</li>
                               <li>2.	15 % weightage of total marks will be given to General Aptitude section.</li>
                               <li>3.	In this section, candidates will be examined based on their language and assessment skills.</li>
                               <li>4.	The detailed GATE exam pattern for General Aptitude is tabulated below:</li>
                        </ul>

                        <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>Question Type</th>
                                    <th>Total Questions</th>
                                    <th>Total Marks</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1- Mark questions</td>
                                    <td>5 questions</td>
                                    <td>5 marks</td>
                                </tr>
                                <tr>
                                    <td>2- Mark questions</td>
                                    <td>5 questions</td>
                                    <td>10 marks</td>  
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>10 Questions</td>
                                    <td>15 marks </td>         
                                </tr>
                            </tbody>                           
                        </table>

                        <h3 class="h4">GATE 2020 Exam Pattern for All Papers except GG, XE, XL</h3>
                        <ul class="page-list pb-3">                           
                               <li>1. For all the papers except for the paper codes GG, XE, XL, the questions will be MCQs and numerical answer type in nature.</li>
                               <li>2.There will be a total of 55 questions out of which 25 questions will be of 1 mark and 30 questions will be of 2 marks each.</li>                               
                        </ul>

                        <h3 class="h4">GATE Exam Pattern for GG</h3>
                        <ul class="page-list pb-3">                           
                               <li>1.	Geology and Geophysics paper consists of two parts. Part A consists of Multiple Choice Questions and Part B is further divided into 2 sections i.e., Section 1 – Geology and Section 2 – Geophysics.</li>
                               <li>2.	Part A consists of 25 questions each carrying 1 mark.</li>    
                               <li>3.	Part B consists of 30 questions each carrying 2 marks.</li>
                        </ul>

                        <h3 class="h4">GATE Exam Pattern for Engineering Sciences (XE)</h3>
                        <ul class="page-list pb-3">
                          
                               <li>1. There will be a total of 33 questions.</li>
                               <li>2.	Engineering Mathematics (Section A) acts as a compulsory subject.</li>    
                               <li>3.	A total of 11 questions will be asked in Engineering Mathematics.</li>    
                               <li>4.	Now coming to the Section B, candidates can select any two subjects as optional from the paper code B to H.</li>                       
                               <li>5.	The detailed GATE Exam Pattern for paper code XE is tabulated below:</li>
                        </ul>

                        <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>Section</th>
                                    <th>Total Questions</th>
                                    <th>Total Marks</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Section A (Compulsory)</td>
                                    <td>7 (1-mark question), 4 (2-mark question)</td>
                                    <td>7, 8</td>
                                </tr>
                                <tr>
                                    <td>Section B to H (Any two)</td>
                                    <td>9 (1-mark question) in each of the two sections, 13 (2-mark question) in each of the two sections</td>
                                    <td>18, 52</td>
                                                              
                                </tr>
                                <tr>
                                    <td>Total ( 2 Sections)</td>
                                    <td>55 Questions</td>
                                    <td>85 marks </td> 
                                </tr>
                            </tbody>                           
                        </table>

                        <h3 class="h4">Note – General Aptitude section of total 15 marks should also be attempted along with the above sections. </h3>

                        <h2 class="h2 pb-2">GATE Exam Pattern for Life Sciences (XL)</h2>

                        <ul class="page-list pb-3">                          
                            <li>1.	General Aptitude section of total 15 should be attempted.</li>
                            <li>2.	Chemistry acts as a compulsory subject in section A.</li>    
                            <li>3.	Now in Section B, aspirants can choose any 2 optional subjects from the paper code Q to U.</li>    
                            <li>4.	The detailed GATE Life Sciences exam pattern is tabulated below:</li> 
                         </ul>

                         <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>Section</th>
                                    <th>Total Questions</th>
                                    <th>Total Marks</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>P or Chemistry Section</td>
                                    <td>5 (1-mark each), 10 (2-marks each)</td>
                                    <td>5, 20</td>
                                </tr>
                                <tr>
                                    <td>Sections Q to U (Any two)</td>
                                    <td>10 (1-mark each) in each section, 10(2-marks each) in each section</td>
                                    <td>20, 40</td>                                                              
                                </tr>                               
                            </tbody>                           
                        </table>
                        <h2 class="h2 pb-2">GATE Syllabus 2020</h2>
                        <p>Before getting into the details of GATE syllabus for all paper codes, let’s have an overview of the list of branches and paper codes for each of the subject:</p>

                        <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>GATE Paper  </th>
                                    <th>Code</th>
                                    <th>GATE Paper  </th>                                    
                                    <th>Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Aerospace Engineering</td>
                                    <td>AE</td>
                                    <td>Instrumentation Engineering</td>
                                    <td> IN</td>
                                </tr>
                                <tr>
                                    <td>Agricultural Engineering</td>
                                    <td>AG </td>
                                    <td>Mathematics</td>
                                    <td> MA</td>
                                </tr>
                                <tr>
                                    <td>Architecture and Planning</td>
                                    <td>AR </td>
                                    <td>Mechanical Engineering</td>
                                    <td>ME</td>
                                </tr>
                                <tr>
                                    <td>Bio-medical Engineering</td>
                                    <td>BM</td>
                                    <td>Mining Engineering</td>
                                    <td>MN</td>
                                </tr>
                                <tr>
                                    <td>Biotechnology</td>
                                    <td>BT</td>
                                    <td>Metallurgical Engineering</td>
                                    <td>MT</td>
                                </tr>
                                <tr>
                                    <td>Civil Engineering</td>
                                    <td>CE</td>
                                    <td>Petroleum Engineering</td>
                                    <td>PE</td>
                                </tr>
                                <tr>
                                    <td>Chemical Engineering</td>
                                    <td>CH</td>
                                    <td>Physics</td>
                                    <td>PH</td>
                                </tr>
                                <tr>
                                    <td>Computer Science and Information Technology</td>
                                    <td>CS</td>
                                    <td>Production and Industrial Engineering</td>
                                    <td>PI</td>
                                </tr>
                                <tr>
                                    <td>Chemistry</td>
                                    <td>CY</td>
                                    <td>Statistics</td>
                                    <td>ST</td>
                                </tr>
                                <tr>
                                    <td>Electronics and Communication Engineering</td>
                                    <td>EC</td>
                                    <td>Textile Engineering and Fibre Science</td>
                                    <td>TF</td>
                                </tr>
                                <tr>
                                    <td>Electrical Engineering</td>
                                    <td>EE</td>
                                    <td>Engineering Sciences</td>
                                    <td>XE*</td>
                                </tr>
                                <tr>
                                    <td>Ecology and Evolution</td>
                                    <td>EY</td>
                                    <td>Life Sciences</td>
                                    <td>XL**</td>
                                </tr>                               
                               
                            </tbody>                           
                        </table>
                        <h3 class="h4">XE & XL Paper Sections </h3>
                        <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>*XE Paper Sections </th>
                                    <th>Code</th>
                                    <th>**XL Paper Sections  </th>                                    
                                    <th>Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Engineering Mathematics(Compulsory)</td>
                                    <td>A</td>
                                    <td>Chemistry (Compulsory)</td>
                                    <td> P</td>
                                </tr>
                                <tr>
                                    <td>Any two optional sections</td>
                                    <td> </td>
                                    <td>Any two optional sections</td>
                                    <td></td>
                                    </tr>
                                <tr>
                                    <td>Fluid Mechanics</td>
                                    <td>B</td>
                                    <td>Biochemistry</td>
                                    <td>Q</td>
                                </tr>
                                <tr>
                                    <td>Materials Science</td>
                                    <td>C</td>
                                    <td>Botany</td>
                                    <td>R</td>
                                </tr>
                                <tr>
                                    <td>Solid Mechanics</td>
                                    <td>D</td>
                                    <td>Microbiology</td>
                                    <td>S</td>
                                </tr>
                                <tr>
                                    <td>Thermodynamics</td>
                                    <td>E</td>
                                    <td>Zoology</td>
                                    <td>T</td>
                                </tr>
                                <tr>
                                    <td>Polymer Science and Engineering</td>
                                    <td>F</td>
                                    <td>Food Technology</td>
                                    <td>U</td>
                                </tr>
                                <tr>
                                    <td>Food Technology</td>
                                    <td>G</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Atmospheric and Oceanic Sciences</td>
                                    <td>H</td>
                                    <td></td>
                                    <td></td>
                                </tr>  
                            </tbody>                           
                        </table>
                        <h3 class="h4">GATE Syllabus 2020 for General Aptitude </h3>
                        <p>The General Aptitude section is common to all papers. This section includes questions based on Verbal Ability and Numerical Ability. The different topics included in the GATE syllabus for General Aptitude are as under:</p>

                        <h3 class="h4">Verbal Ability: </h3>
                        <ul class="page-list pb-3">                          
                            <li>1.	English grammar</li>
                            <li>2.	Sentence completion</li>    
                            <li>3.	Verbal analogies</li>    
                            <li>4.	Word groups</li> 
                            <li>5.	Critical reasoning and</li>
                            <li>6.	Verbal deduction</li>
                         </ul>

                         <h3 class="h4">Numerical Ability: </h3>
                        <ul class="page-list pb-3">                          
                            <li>1.	Numerical computation</li>
                            <li>2.	Numerical estimation</li>    
                            <li>3.	Numerical reasoning and</li>    
                            <li>4.	Data interpretation</li>                           
                         </ul>

                         <h3 class="h4">GATE Syllabus 2020 – Section-wise Exam Pattern for GATE 2020 </h3>
                         <p>The GATE exam pattern for admission to M.E/ M.Tech – different sections, number of questions and maximum marks for each section are tabulated below:</p>

                         
                        <table class="table mt-4 table-responsive">
                            <thead>
                                <tr>
                                    <th>Sections  </th>
                                    <th>Number of Questions/ Maximum Marks</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>General Aptitude</td>
                                    <td>10/15</td>
                                </tr>
                                <tr>
                                    <td>Engineering Mathematics</td>
                                    <td>11/15</td>
                                </tr>
                                <tr>
                                    <td>Subject Specific Section</td>
                                    <td>44/70</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>65/100</td>
                                </tr>
                            </tbody>                           
                        </table>
                        <p>Revise all the topics from time to time and take GATE mock tests and GATE previous papers regularly. Schedule the whole preparation plan in a way that you get at least two months just for revision. It is vital that candidates stay motivated the whole time to crack GATE 2020.</p>
                    </div>
                    <!--/ col 8--> 

                     <!-- course right faq -->
                   <div class="col-lg-4">   
                   <!-- sticky top -->
                   <div class="sticky-top">                     
                        <div class="custom-accord course-faq">
                            <h3 class="h5 pb-3">GATE Exam Pattern 2020 – Frequently Asked Questions (FAQs)</h3>
                            <!-- accordion -->
                            <div class="accordion">
                                <!-- acc-->
                                <h3 class="panel-title">Is GATE conducted offline as well?</h3>
                                <div class="panel-content">
                                    <p>No, GATE is conducted only in the online mode.
                                    </p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">What kind of questions are asked in GATE?</h3>
                                <div class="panel-content">
                                    <p>There are two types of questions asked in GATE – 1) Multiple Choice Questions (MCQ), and 2) Numerical Answer Type (NAT) questions.</p>
                                </div>
                                <!--/ acc-->
                                <!-- acc-->
                                <h3 class="panel-title">Is there any penalty for wrong answers in GATE?</h3>
                                <div class="panel-content">
                                    <p>Yes, for 1-marks questions, the penalty is 1/3 mark; for 2-marks questions, the penalty is 2/3 marks. However, there is no negative marking for wrong answers of NAT questions.</p>                                   
                                </div>
                                <!--/ acc-->
                            </div>
                            <!--/ accordion -->
                        </div>
                        </div>    
                        <!--/ sticky top -->    
                   </div>
                   <!--/ course right faq -->
               </div>
               <!--/ row -->
           </div>
            <!-- /container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ sub page main -->
    