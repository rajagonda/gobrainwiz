   <!--

    Envor site content start

    //-->
    <div class="envor-content">
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Products</h1>
            </div>
                      </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="index.html">Home</a><i class="fa fa-angle-double-right"></i><a href="pages.html">pages</a><i class="fa fa-angle-double-right"></i>Products 
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section envor-single-page">
        <div class="container">
          <div class="row">
            <!--

            Side Navigation

            //-->
          <div class="col-lg-6 col-md-6">
           
           <img align="center" alt="layer1-background" src="http://innoprimetek.com/assets/img/VishwaLogo.png"   height="0%" width="30%"  >

              <!-- <p>In commodo interdum quam, sed rutrum dui vulputate in. Praesent tempus viverra adipiscing. Fusce lacinia lectus eu magna pharetra, sit amet tempus nibh adipiscing. Quisque sit amet lacinia ligula. Aliquam quis magna blandit, gravida enim non, convallis leo. Quisque iaculis sem purus, sed cursus diam tincidunt sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis varius felis erat, quis fermentum felis sagittis lacinia.</p> -->
              <p>&nbsp;</p>
              <div class="envor-career-1">
                
                  <p class=""><b>Name : Vishwa Developers</b></p> 
                   <p class=""><b>Website Address: <a href="http://www.viswadevelopers.com/"> http://www.viswadevelopers.com</a></b></p> 
                  
               
                <div class="details">
                  <p class="title">Production Description:</p>
                  <p>Viswa Developers is a leading real estate developer in Warangal, Telangana, India. We have deployed a web portal for their business and we are even supporting it 24/7 till today(and will be till Viswa Developers exist) Viswa Developers has completed projects comprising fully integrated gated community villas, premium apartments and commercial buildings. These projects are widely recognized for their innovations, continuously surpassing industry standards and many of which have become Landmark communities. Currently, Viswa Developers projects are developed with an unwavering vision to offer a sustainable balance between affordability and luxury with global standards of construction and environmental responsibility. We have successfully built and handed over more than 1 million Square feet to our esteemed clientele.</p>
                
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
            </div>
            <!--

            Partners List

            //-->
            <div class="col-lg-6 col-md-6">
             <img alt="layer1-background" src="http://innoprimetek.com/assets/img/SocietyPro.png" height="170px" width="250px">
              <!-- <p>In commodo interdum quam, sed rutrum dui vulputate in. Praesent tempus viverra adipiscing. Fusce lacinia lectus eu magna pharetra, sit amet tempus nibh adipiscing. Quisque sit amet lacinia ligula. Aliquam quis magna blandit, gravida enim non, convallis leo. Quisque iaculis sem purus, sed cursus diam tincidunt sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis varius felis erat, quis fermentum felis sagittis lacinia.</p> -->
              <p>&nbsp;</p>
              <!--
              * Envor Career Item
              //-->
              <div class="envor-career-1">
                <p class=""><b>Name : Society Pro</b></p> 
                   
                <div class="details">
                  <p class="title">Production Description:</p>
                  <p>Society Pro, a Telugu software developed for automating the financial systems of the Society groups. This software is a completely User Friendly, secured, easy to use and maintain. The automation helps in reducing manpower and dependency. </p>
                  <p class="title">Quick Facts:</p>
                  <ul class="e-color-list">
                    <li><i class="glyphicon glyphicon-check"></i> Software in Telugu</li>
                    <li><i class="glyphicon glyphicon-check"></i> Complete Automation with fully secured</li>
                    <li><i class="glyphicon glyphicon-check"></i> Management Information Reports for Strategic Planning</li>
                    <li><i class="glyphicon glyphicon-check"></i> Easy to use and maintain.</li>                    
                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
            
              
               
            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
    <!--

    Envor site content end

    //-->
    </div>
   