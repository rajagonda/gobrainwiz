
<style>
.section_schedule {
    background: #fff;
    color: #1b1b42;
	
}
.center {
    width: 100%;
    max-width: 1173px;
    margin: 0 auto;
    padding: 0 30px;
}
.section__title {
    font-size: 20px;
    text-transform: uppercase;
    letter-spacing: 4.44px;
}
.fadeInUp {
    -webkit-animation-name: fadeInUp;
    animation-name: fadeInUp;
}
0% {
    opacity: 0;
    -webkit-transform: translate3d(0, 15%, 0);
    transform: translate3d(0, 15%, 0);
}
.section__content {
    max-width: 920px;
    margin: 40px auto 0;
    opacity: .7;
    font-size: 35px;
    line-height: 1.48;
}
.section_schedule .section__body {
    padding-top: 20px;
}
.schedule__container {
    margin-bottom: 70px;
}
.schedule__head {
    padding-bottom: 18px;
    border-bottom: 2px solid;
    font-size: 34px;
    letter-spacing: 3.2px;
    color: #4CAF4F;
}
.schedule__head_red {
    color: #515190;
}
.schedule__th {
    position: relative;
    top: 8px;
    margin-left: 2px;
    text-transform: uppercase;
    font-size: 16px;
    vertical-align: top;
}
.schedule__row {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    padding: 28px 0;
    border-bottom: 1px solid rgba(255, 255, 255, 0.15);
    font-size: 24px;
    line-height: 1.16;
}
.schedule__time {
    padding-right: 30px;
    -webkit-flex: 0 0 284px;
    -ms-flex: 0 0 284px;
    flex: 0 0 284px;
    font-weight: 400;
}
.schedule__wrap {
    -webkit-flex: 1 1 auto;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
}
.schedule__title {
    margin-bottom: 12px;
    font-weight: 400;
}
.schedule__text {
    opacity: .7;
    font-size: 20px;
    line-height: 1.35;
}
.more {
    text-align: center;
}
.btn_white {
    border-color: rgba(255, 255, 255, 0.15);
    color: #FFF;
}
.inrright_cntr .achievement th, td{
	letter-spacing: 1.2px;
	border: 1px solid #92bed2;
	    padding: 11px 9px;
		    font-family: open_sanssemibold;
}
table tr td{
background: #fff;
    font-weight: 500;
    font-size: 14px;
    text-align: center;
    color: rgb(24, 24, 191);
}
table tr th {
    font-weight: 500;
    font-size: 16px;
    background-color: rgb(218, 218, 218);
    color: #0d1335;
    padding: 12px 48px;
    border-top: none !important;
}
table tr td a{
    color: rgb(3, 17, 95);
    font-size: 17px;

}
table tr td a:hover{
	color:#0d776d;
}
table{
display: table;
    border-collapse: separate;
    border-spacing: 1px;
    border-color: grey;
	overflow-y: auto;
		display: block;
	
}
</style>	
<section>
 <div class="section section_schedule">
          <div class="section__center center">
             <div class="section__body">
              <div class="schedule">
                <div class="schedule__container">
                  <div class="schedule__head schedule__head_red wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Companies Hiring</div>
				  <table class="table-responsive">
	             <tr>
				 <th>Date</th>
				 <th>Job Title</th>
                 <th>Location</th>
                 <th>Position</th>
				 <th>Eligibility</th>
				 <th>Last Date</th>
				 <th>Details</th>
                 </tr>
				 <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr><tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr><tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  <tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr><tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr><tr>
				  <td>13/06</td>
				  <td>Wipro</td>
				  <td>Hyderabad</td>
				  <td>Android Developer</td>
				  <td>B.Tech/M.Tech/MCA</td>
				  <td>16/06</td>
				  <td><a href="#">Details</a></td>
				  </tr>
				  </table>
                 </div>
              </div>
              <div class="more wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><a href="#" class="more__btn btn btn_white">SHOW MORE</a></div>
            </div>
          </div>
        </div>
</section>