   <!--

    Envor site content start

    //-->
    <div class="envor-content">
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Career</h1>
            </div>
                      </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="index.html">Home</a><i class="fa fa-angle-double-right"></i><a href="pages.html">pages</a><i class="fa fa-angle-double-right"></i>Career
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section envor-single-page">
        <div class="container">
          <div class="row">
            <!--

            Side Navigation

            //-->
            <div class="col-lg-3 col-md-3">
             
            </div>
            <!--

            Partners List

            //-->
            <div class="col-lg-9 col-md-9">
              <h2 class="align-left">career</h2>
              <!-- <p>In commodo interdum quam, sed rutrum dui vulputate in. Praesent tempus viverra adipiscing. Fusce lacinia lectus eu magna pharetra, sit amet tempus nibh adipiscing. Quisque sit amet lacinia ligula. Aliquam quis magna blandit, gravida enim non, convallis leo. Quisque iaculis sem purus, sed cursus diam tincidunt sed. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis varius felis erat, quis fermentum felis sagittis lacinia.</p> -->
              <p>&nbsp;</p>
              <!--
              * Envor Career Item
              //-->
              <div class="envor-career-1">
                <header>
                  <i class="fa fa-user"></i>
                  <small>Job Description</small>
                  <p>1. Java</p>
                </header>
                <div class="details">
                  <p class="title">Overview:</p>
                  
                  <p class="title">Skills:</p>
                  <ul class="e-color-list">
                    <li><i class="glyphicon glyphicon-check"></i> Exp : 3-4 Yrs</li>
                    <li><i class="glyphicon glyphicon-check"></i> Urgent Opening for Java Developer Spoors Technology Solutions India Pvt. Ltd - Hyderabad, Andhra Pradesh</li>
                    <li><i class="glyphicon glyphicon-check"></i> OOPs methodologies, Core Java, JSP, Servlets, EJB, Ajax, JQuery</li>
                    <li><i class="glyphicon glyphicon-check"></i> Spring, Spring Batch, Hibernate, SOAP or Restful webservices, Maven and Jenkins.</li>
                    <li><i class="glyphicon glyphicon-check"></i> HTML5 and CSS3.</li>
                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
              <!--
              * Envor Career Item
              //-->
              <div class="envor-career-1">
                <header>
                  <i class="fa fa-user"></i>
                  <small>Job Description</small>
                  <p>2. .NET</p>
                </header>
                <div class="details">
                  <p class="title">Overview:</p>
                  
                  <p class="title">Skills:</p>
                  <ul class="e-color-list">
                  <li><i class="glyphicon glyphicon-check"></i>Exp : 3-4 Yrs</li>
 <li><i class="glyphicon glyphicon-check"></i> Key Skills </li>

 <li><i class="glyphicon glyphicon-check"></i>OOPs methodologies</li>
 <li><i class="glyphicon glyphicon-check"></i>Proficient in .NET framework, MVC</li>
 <li><i class="glyphicon glyphicon-check"></i>Hands on Web Services development (REST API)</li>
 <li><i class="glyphicon glyphicon-check"></i>Javascript frameworks like Ext.JS, JQuery, Etc.</li>
 <li><i class="glyphicon glyphicon-check"></i>Good in OOPs concepts, Ajax</li>
 <li><i class="glyphicon glyphicon-check"></i>RDBMS (MS SQL)</li>
 <li><i class="glyphicon glyphicon-check"></i>Knowledge on WCF</li>
 <li><i class="glyphicon glyphicon-check"></i>HTML5 and CSS3</li>

                   
                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
              <!--
              * Envor Career Item
              //-->
              <div class="envor-career-1">
                <header>
                  <i class="fa fa-user"></i>
                  <small>Job Description</small>
                  <p>3. Testing</p>
                </header>
                <div class="details">
                  <p class="title">Overview:</p>
                  <p class="title">Skills:</p>
                  <ul class="e-color-list">

 <li><i class="glyphicon glyphicon-check"></i> Experience in creating Test Cases and Test Strategies.</li>
 <li><i class="glyphicon glyphicon-check"></i> Experience in Black Box Testing, Functional Testing and techniques.</li>
 <li><i class="glyphicon glyphicon-check"></i> Working knowledge of system, integration, regression, and user interface testing.</li>
 <li><i class="glyphicon glyphicon-check"></i> Good understanding of SDLC, STLC cycles and its various models.</li>
 <li><i class="glyphicon glyphicon-check"></i> Strong Experience in Manual Testing (Writing and executing Test Cases).</li>
 <li><i class="glyphicon glyphicon-check"></i> Prepare and maintain Test environments.</li>
 <li><i class="glyphicon glyphicon-check"></i> Working knowledge of SQL and RDBMS, Should be able to write SQL queries.</li>
 <li><i class="glyphicon glyphicon-check"></i> Excellent Communication skills.</li>
 <li><i class="glyphicon glyphicon-check"></i> Experience of developing and/or executing Manual Test suites.</li>


                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
              <!--
              * Envor Career Item
              //-->
              <div class="envor-career-1">
                <header>
                  <i class="fa fa-user"></i>
                  <small>Job Description</small>
                  <p>4. PHP Developer</p>
                </header>
                <div class="details">
                  <p class="title">Overview:</p>
                  <p>We are looking for a PHP Developer responsible for managing back-end services and the interchange of data between the server and the users. Your primary focus will be the development of all server-side logic, definition and maintenance of the central database, and ensuring high performance and responsiveness to requests from the front-end. You will also be responsible for integrating the front-end elements built by your co-workers into the application. Therefore, a basic understanding of front-end technologies is necessary as well.</p>
                  
        
 <p class="title">Responsibilities:</p>
                  <ul class="e-color-list">

 <li><i class="glyphicon glyphicon-check"></i> Integration of user-facing elements developed by front-end developers.</li>
 <li><i class="glyphicon glyphicon-check"></i>Build efficient, testable, and reusable PHP modules.</li>
 <li><i class="glyphicon glyphicon-check"></i> Solve complex performance problems and architectural challenges.</li>
 <li><i class="glyphicon glyphicon-check"></i> Integration of data storage solutions may include databases, key-value stores, blob stores, etc.</li>
 <li><i class="glyphicon glyphicon-check"></i> Add other responsibilities here that are relevant</li>
  </ul>




                  <p class="title">Skills And Qualifications :</p>
                  <ul class="e-color-list">

                  <li><i class="glyphicon glyphicon-check"></i>Strong knowledge of PHP web frameworks such as Laravel, Yii, etc depending on your technology stack</li>
<li><i class="glyphicon glyphicon-check"></i> Understanding the fully synchronous behavior of PHP</li>
<li><i class="glyphicon glyphicon-check"></i> Understanding of MVC design patterns</li>
<li><i class="glyphicon glyphicon-check"></i> Basic understanding of front-end technologies, such as JavaScript, HTML5, and CS3</li>
<li><i class="glyphicon glyphicon-check"></i> Knowledge of object oriented PHP programming</li>
<li><i class="glyphicon glyphicon-check"></i> Understanding accessibility and security compliance Depending on the specific project</li>
<li><i class="glyphicon glyphicon-check"></i> Strong knowledge of the common PHP or web server exploits and their solutions</li>
<li><i class="glyphicon glyphicon-check"></i> Understanding fundamental design principles behind a scalable application</li>
<li><i class="glyphicon glyphicon-check"></i> User authentication and authorization between multiple systems, servers, and environments</li>
<li><i class="glyphicon glyphicon-check"></i> Integration of multiple data sources and databases into one system</li>
<li><i class="glyphicon glyphicon-check"></i> Familiarity with limitations of PHP as a platform and its workarounds</li>
<li><i class="glyphicon glyphicon-check"></i> Creating database schemas that represent and support business processes</li>
<li><i class="glyphicon glyphicon-check"></i> Familiarity with SQL/NoSQL databases and their declarative query languages</li>
<li><i class="glyphicon glyphicon-check"></i> Proficient understanding of code versioning tools, such as Git</li>
<li><i class="glyphicon glyphicon-check"></i> Make sure to mention other frameworks, libraries, or any other technology related to your development stack</li>
<li><i class="glyphicon glyphicon-check"></i> List education level or certification you require</li>

                   
                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
               <div class="envor-career-1">
                <header>
                  <i class="fa fa-user"></i>
                  <small>Job Description</small>
                  <p> 5.  UI / UX Developer</p>
                </header>
                <div class="details">
                  <p class="title">Overview:</p>
                  <p>Excellent knowledge in HTML4, HTML5, CSS2, CSS3 and W3C standards<br>
 Must be very strong at JavaScript and JQuery</br>
Should have experience in developing custom UI/UX components with JQuery</br>
Very good Knowledge of Google Maps API, Google Analytics, SEO concept</br>
</p>
                  <p class="title">Skills:</p>
                  <ul class="e-color-list">


 <li><i class="glyphicon glyphicon-check"></i> W3 Standards, CSS Standards, Dreamweaver standards, Photoshop, HTML5, .</li>
 <li><i class="glyphicon glyphicon-check"></i> CSS 3, Angular JS, JQuery.</li>


                  </ul>
                </div>
                <div class="ca-btn">
                  <!-- <a href="" class="envor-btn envor-btn-primary envor-btn-small">apply now!</a> -->
                  <a href="" class="envor-btn envor-btn-secondary envor-btn-small show-details"><i class="fa fa-plus"></i> details</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
    <!--

    Envor site content end

    //-->
    </div>
   