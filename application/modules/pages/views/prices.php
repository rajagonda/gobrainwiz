
    <section class="title">
        <div class="container">
            <div class="row-fluid">
                <div class="span6">
                    <h1>Pricing</h1>
                </div>
                <div class="span6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="<?php echo base_url(); ?>">Home</a> <span class="divider">/</span></li>
                        <li class="active">Pricing</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- / .title -->

    <section id="pricing-table" class="container">
        <div class="center">
            <h2>Pellentesque habitant morbi tristique senectus et netus</h2>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        </div>

        <div class="big-gap"></div>

        <div class="row-fluid center clearfix">
            <div class="span4">
                <ul class="plan plan1">
                    <li class="plan-name">
                        <h3>Basic</h3>
                    </li>
                    <li class="plan-price">
                        <strong>Price : 1000 </strong>Rs only
                    </li>
                    <li>
                        <strong>Unlimited </strong> Validity
                    </li>
                    <li>
                        <strong>Sms Charge</strong> Based on plan
                    </li>
                    <li>
                        <strong>Delivery ratio</strong> 100%
                    </li>
                    <li>
                        <strong>3000</strong> Free Sms
                    </li>
                    <li>
                        <strong>24/7 Customer</strong> Support
                    </li>
                    <li class="plan-action">
                        <a href="<?php echo base_url(); ?>contactus.html" class="btn btn-transparent">Contact  Now!</a> Or Call me +91 8686994774
                    </li>
                </ul>
            </div>

            <div class="span4">
                <ul class="plan plan2 featured">
                    <li class="plan-name">
                        <h3>Standard</h3>
                    </li>
                    <li class="plan-price">
                        <strong>Price : 6000 </strong>Rs only
                    </li>
                    <li>
                        <strong>Unlimited </strong> Validity
                    </li>
                    <li>
                        <strong>Sms Charge</strong> Based on plan
                    </li>
                    <li>
                        <strong>Delivery ratio</strong> 100%
                    </li>
                    <li>
                        <strong>50000</strong> Free Sms
                    </li>
                    <li>
                        <strong>24/7 Customer</strong> Support
                    </li>
                    <li class="plan-action">
                        <a href="<?php echo base_url(); ?>contactus.html" class="btn btn-transparent">Contact  Now!</a> Or Call me +91 8686994774
                    </li>
                </ul>
            </div>

            <div class="span4">
                <ul class="plan plan3">
                    <li class="plan-name">
                        <h3>Advanced</h3>
                    </li>
                    <li class="plan-price">
                        <strong>Price : 10000 </strong>Rs only
                    </li>
                    <li>
                        <strong>Unlimited </strong> Validity
                    </li>
                    <li>
                        <strong>Sms Charge</strong> Based on plan
                    </li>
                    <li>
                        <strong>Delivery ratio</strong> 100%
                    </li>
                    <li>
                        <strong>50000</strong> Free Sms
                    </li>
                    <li>
                        <strong>24/7 Customer</strong> Support
                    </li>
                    <li class="plan-action">
                        <a href="<?php echo base_url(); ?>contactus.html" class="btn btn-transparent">Contact  Now!</a> Or Call me +91 8686994774
                    </li>
                </ul>
            </div>

           <!--  <div class="span3">
                <ul class="plan plan4">
                    <li class="plan-name">
                        <h3>Advanced</h3>
                    </li>
                    <li class="plan-price">
                        <strong>$199</strong> / month
                    </li>
                    <li>
                        <strong>50GB</strong> Storage
                    </li>
                    <li>
                        <strong>8GB</strong> RAM
                    </li>
                    <li>
                        <strong>1024GB</strong> Bandwidth
                    </li>
                    <li>
                        <strong>Unlimited</strong> Email Address
                    </li>
                    <li>
                        <strong>Forum</strong> Support
                    </li>
                    <li class="plan-action">
                        <a href="#" class="btn btn-transparent">Signup Now!</a>
                    </li>
                </ul>
            </div> -->
        </div>
        <p>&nbsp;</p>
    </section>