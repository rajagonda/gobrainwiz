<main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Weekly <span class="fbold text-uppercase">Schedules</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- brudcrumb -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a>Weekly Schedule</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body schedulebody">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col 3-->
                    <div class="col-lg-3 col-sm-3 align-self-center text-center">
                        <img src="<?php echo assets_url();?>img/schedule-calendar.png" alt="" class="img-fluid weeklycal">
                        <p class="pt-2 text-center">
                            Keeping Daily Track on syllabus for Quick completion
                        </p>
                    </div>
                    <!--/ col 3-->

                    <!--/ col 9-->
                    <div class="col-lg-9 col-sm-9">
                        <?php if($weekly){ ?>
                  <img src="<?php echo base_url();?>upload/weekly/<?php echo $weekly->image; ?>" class="img-fluid">
			<?php }  else { ?>
                 <img src="<?php echo assets_url();?>images/weeklyimg/June 19.jpg" class="img-fluid">
			<?php } ?>
                        
                    </div>
                    <!--/ col 9-->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
          

           
        </div>
        <!--/ sub page body -->
    </main>
    