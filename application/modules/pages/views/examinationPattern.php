<link href="<?php echo assets_url();?>new/css/easy.css" rel="stylesheet" type="text/css">
<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 30%;
    height: 300px;
}

/* Style the buttons inside the tab */
div.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:after {background:#1b1b42;content:"";position:absolute;
z-index:-1;top:0;left:0;right:0;bottom:0;-webkit-transform:scaleX(0); transform:scaleX(0);-webkit-transform-origin:0 50%;transform-origin:0 50%;-webkit-transition-property:transform;transition-property:transform; 
transition-duration:.4s;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}

.qklink_wpr .linkgate .accordion-item:hover .accordion-header:after {background:#00a861;    
width: 100%;-webkit-transform:scaleX(1);transform:scaleX(1)}
</style>
<style>
.main_wrapper {width:1104px;margin:0 auto;clear:both}
.main_wrapper {text-align: center;}
.main_wrapper{position:relative}
.lnk_ansaddwpr{display:inline-block;width:100%;}
.inr_title{background:#1b1b42;border-bottom:3px solid #00a861;font-size:20px;color:#fff;
font-weight:400;position:relative;padding-left:50px;height:43px;line-height:43px}
.linkgate .accordion a, .linkgate .accordion1 a {
    font-family: Arial, Helvetica, sans-serif;
    text-decoration: none;
    font-size: 15px;
    padding: 9px 0;
    margin: 0;
    display: block;
    cursor: pointer;
    background: #fafafa;
    border-bottom: 1px solid #a6a6a6;
    outline: none;
    display: inline-block;
    width: 74%;
	color: #151719;
    font-weight: 500;
}
.qklink_wpr .linkgate .accordion-item .accordion-header:hover a {
    color: #fff;
}
.inrright_cntr .gate h1,.book_package .package_head{ 
background:#d6d4d4;
border: 1px solid #ccc;    
border-radius: 3px;    
color: #202090;    
font-size: 26px;    
font-weight: 600;    
margin-left: 0;
padding: 3px 10px;    
position: relative;}

.qklink_wpr .linkgate .accordion-item .accordion-header:hover a{color:#fff}
.linkgate .accordion a.abtacordian,.accordion-header h1 > a{border-bottom:none!important;
background:none!important;display:inline-block!important;padding:0!important;font-family:open_sansregular;font-size:14px}
.accordion1 .accordion-header h1{float:left;font-size:15px;font-weight:400;margin:0;line-height:1em;color:#00a861;}
.inrright_cntr .gate p {
    font-size: 15px;
    line-height: 1.5em;
    padding: 3px 0 0 11px;
    text-align: justify;
    font-weight: 400;
}
.inrright_cntr .gate ul li {
    padding: 0px 0;
    font-size: 14px;
    margin: 0 0 0 5px;
    font-family: Arial, Helvetica, sans-serif;
    list-style: disc;
}
@media (max-width: 991px) and (min-width: 768px)
{
.navbar-nav > li > a {
    padding: 10px;
}	
}
</style>	
<section>
 <div class="container">
 <div class="row">

    <!-- for Quick Link Section -->
	<div class="col-md-4">
   <div class="qklink_wpr">
    <h1 class="inr_title"><i class="sprite qklink"></i>Categories</h1>
    <div class="linkgate"> 
        <div class="accordion1">
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/elitmus#gate">What Is Elitmus</a>
         </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		 <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/elitmusSyllabus#gate">Elitmus Syllabus</a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/examPattern#gate" class="active">Examination Pattern
		<span class="newimg"></span>
		</a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
	    <a href="<?php echo base_url();?>pages/registrationProcess#gate">Registration Process
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url();?>pages/cryptArithmetic#gate">CryptArithmetic Videos
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		<div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
        <a href="<?php echo base_url(); ?>pages/companiesHiringthrowElitmus#gate">Companies hiring through Elitmus
        <span class="newimg"></span></a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
		 <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
        <a href="<?php echo base_url(); ?>pages/previousQuestions#gate">Previous Questions
        </a>
        </h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        <div class="drawer">
		<div class="accordion-item">
		<div class="accordion-header">
		<h1 class="abtacordian">
		<span class="glyphicon glyphicon-play"></span>
		<a href="<?php echo base_url(); ?>pages/faq#gate"> Frequently Asked Questions</a>
		</h1>
		 <div class="accordion-header-icon"></div>
		 </div>
		 </div>
		 </div>
        </div>
   </div>
</div></div>
 
	<div class="col-md-8">
		<div class="inrright_cntr" style="box-shadow:none;">
    <div class="gate" id="gate">
        <h1>Examination Pattern</h1>

<p>&nbsp;</p>

<p><strong style="color: #006b30;">Quantitative Ability (20 Questions)</strong></p>
<div class="row">
 <div class="col-12 col-sm-12 col-lg-12">
    <div style="overflow-x:auto;">
<table class="table table-responsive">
       
          <tr class="text-center">
                <td scope="row">1</td>
               
                <td class="text-center">Number System</td>
               
            </tr>
             <tr class="text-center">
                <td scope="row">2</td>
               
                <td class="text-center">Permutation and Combinations</td>
               
            </tr>
             <tr class="text-center">
                <td scope="row">3</td>
              
                <td class="text-center">Probability</td>
               
            </tr>
             <tr class="text-center">
                <td scope="row">4</td>
               
                <td class="text-center">Geometry</td>

            </tr>
             <tr class="text-center">
                <td scope="row">5</td>
                
                <td class="text-center">Mensuration</td>

            </tr>
             <tr class="text-center">
                <td scope="row">6</td>
               
                <td class="text-center">Time and Distance</td>

            </tr>
             <tr class="text-center">
                <td scope="row">7</td>
               
                <td class="text-center">Time and Work</td>

            </tr>
             <tr class="text-center">
                <td scope="row">8</td>
               
                <td class="text-center">Algebra</td>

            </tr>
           
        
    </table>
</div></div></div>
<p><b>(Around 10-15 questions are from the above mentioned topics)

(Solve 5-8 Questions in section to score a good percentile) (See Elitmus Score Vs-Percentile chart below)</b></p>

<p><strong style="color: #006b30;">Verbal Ability (20 Questions)</strong></p>

<div class="row">
                 <div class="col-12 col-sm-12 col-lg-12">
    <div style="overflow-x:auto;">
                    <table class="table">

           
                 <tr class="text-center">
                    <td scope="row">1</td>

                    <td class="text-center">Sentence Completion 1</td>
                    <td class="text-center">4 Questions</td>
                    <td class="text-center">(Based on Subject-Verb Agreement)</td>
                </tr>
                 <tr class="text-center">
                    <th scope="row">2</th>

                    <td class="text-center">Reading Comprehension 1</td>
                    <td class="text-center">4 Questions</td>
                    <td class="text-center">(Around 200-230 words)</td>

                </tr>
                 <tr class="text-center">
                    <td scope="row">3</td>

                    <td class="text-center">Reading Comprehension 2</td>
                    <td class="text-center">4 Questions</td>
                    <td class="text-center">(Around 200-230 words)</td>
                </tr>
                 <tr class="text-center">
                    <td scope="row">4</td>

                    <td class="text-center">Reading Comprehension 3</td>
                    <td class="text-center">4 Questions</td>
                    <td class="text-center">(Around 200-230 words)</td>

                </tr>
                 <tr class="text-center">
                    <td scope="row">5</td>

                    <td class="text-center">Sentence Completion 2</td>
                    <td class="text-center">2 Questions</td>
                    <td class="text-center">(Based on Vocabulary)</td>

                </tr>
                 <tr class="text-center">
                    <td scope="row">6</td>

                    <td class="text-center">Para Jumbles</td>
                    <td class="text-center">2 Questions</td>
                    <td class="text-center"></td>

                </tr>
                
        </table>
                </div>
				</div>
      </div>

<p><strong style="color: #006b30;">Problem Solving (20 Questions)</strong></p>
<div id="tbl_Last">
        <table class="table">

            <tbody class="text-center">
                 <tr class="text-center">
                    <td scope="row">1</td>

                    <td class="text-center">Logical Reasoning-1</td>
                    <td class="text-center">5 Questions</td>

                </tr>
                 <tr class="text-center">
                    <td scope="row">2</td>

                    <td class="text-center">Logical Reasoning-1</td>
                    <td class="text-center">3 Questions</td>


                </tr>
                 <tr class="text-center">
                    <td scope="row">3</td>

                    <td class="text-center">Data Sufficiency</td>
                    <td class="text-center">4 Questions</td>

                </tr>
                 <tr class="text-center">
                    <td scope="row">4</td>

                    <td class="text-center">Data Interpretation</td>
                    <td class="text-center">5 Questions</td>


                </tr>
                 <tr class="text-center">
                    <td scope="row">5</td>

                    <td class="text-center">Cryptarithmetic</td>
                    <td class="text-center">3 Questions</td>


                </tr>
               
                       


            </tbody>
        </table>
        <div id="cont-p">
           <p> (Solve 5-10 Questions in this section of getting good percentile)
            (See Marks-Percentile chart below)
 </p>
        </div>
    </div>
		<p><strong>(Solve 5-10 Questions in this section of getting good percentile) (See Marks-Percentile chart below)</strong></p>
        <br>
		<p><strong style="color: #006b30;"> Marking Scheme</strong></p>
 		<p><strong>Each Question Carries 10 Marks.</strong></p>
        <p><strong>Marks distribution in 3 different sections.</strong></p>
		<div class="row">
                 <div class="col-12 col-sm-12 col-lg-12">
    <div style="overflow-x:auto;">
		 <table class="table">
                         <tr class="text-center">
                            <td class="text-center">A.</td>
                            <td class="text-center">Quantitative Ability</td>
                            <td class="text-center">20 Questions</td>
                            <td class="text-center">200 Marks</td>
                        </tr>
                         <tr class="text-center">
                            <td class="text-center">B.</td>
                            <td class="text-center">Problem Solving</td>
                            <td class="text-center">20 Questions</td>
                            <td class="text-center">200 Marks</td>
                        </tr>
                         <tr class="text-center">
                            <td class="text-center">C.</td>
                            <td class="text-center">Verbal Ability</td>
                            <td class="text-center">20 Questions</td>
                            <td class="text-center">200 Marks</td>
                        </tr>

                    </table>
					</div></div></div>
			<p style="color:red;text-align:center;">
    If more than 25% of the attempted questions are wrong, then negative marking starts.
</p>	<br>	
<p><strong style="color: #006b30;"> Negative Marking Rule</strong></p>
<div class="row">
                 <div class="col-12 col-sm-12 col-lg-12">
    <div style="overflow-x:auto;">
				<table class="table">
            <thead class="text-center">
                 <tr class="text-center">
                    <td class="text-center">SI No</td>
                    <td class="text-center">No. of quest. Attempted</td>
                    <td class="text-center">Correct</td>
                    <td class="text-center">Incorrect</td>
                    <td class="text-center">Marks&nbsp;for correct&nbsp; answer</td>
                    <td class="text-center">Negative(*approx)</td>
                    <td class="text-center">Marks Obtained (*approx)</td>
                </tr>
            </thead>
            <tbody class="text-center">
                 <tr class="text-center">
                    <td scope="row">1</td>
                    <td class="text-center">4</td>
                    <td class="text-center">3</td>
                    <td class="text-center">1</td>
                    <td class="text-center">30</td>
                    <td class="text-center">0</td>
                    <td class="text-center">30</td>
                </tr>
                 <tr class="text-center">
                    <td scope="row">2</td>
                    <td class="text-center">4</td>
                    <td class="text-center">2</td>
                    <td class="text-center">2</td>
                    <td class="text-center">20</td>
                    <td class="text-center">-5</td>
                    <td class="text-center">15</td>
                </tr>
                 <tr class="text-center">
                    <td scope="row">3</td>
                    <td class="text-center">8</td>
                    <td class="text-center">6</td>
                    <td class="text-center">2</td>
                    <td class="text-center">60</td>
                    <td class="text-center">0</td>
                    <td class="text-center">60</td>
                </tr>
                 <tr class="text-center">
                    <td scope="row">4</td>
                    <td class="text-center">8</td>
                    <td class="text-center">5</td>
                    <td class="text-center">3</td>
                    <td class="text-center">50</td>
                    <td class="text-center">-5</td>
                    <td class="text-center">45</td>
                </tr>
            </tbody>
        </table> </div>	</div>	</div>	<br>
		<p><strong style="color: #006b30;">Elitmus Score Vs Percentile Chart</strong></p>
		<div class="row">
        
        <div class="col-md-4 col-xs-12 col-sm-12">
            <div class="qty" style="color:red;text-align:center;">Quantitative Ability	</div>
            <table id="tblqty">
                <thead class="text-center">
                     <tr class="text-center">
                        <th class="text-center">
                            Marks
                        </th>
                        <th class="text-center">
                            Percentile
                        </th>
                    </tr>
                </thead>
                <tbody class="text-center">
                     <tr class="text-center">
                        <td class="text-center">120.00</td>
                        <td class="text-center">99.80</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           90.00
                        </td>
                        <td class="text-center">
                          98.75
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 78.80</td>
                        <td class="text-center"> 97.23</td>
                    </tr>

                     <tr class="text-center">
                        <td class="text-center">70.00   </td>
                        <td class="text-center">96.19		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">63.80	</td>
                        <td class="text-center">94.80	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">60.00	</td>
                        <td class="text-center">94.11	</td>
                    </tr> <tr class="text-center">
                             <td class="text-center">56.30</td>
                             <td class="text-center">92.42	</td>
</tr>
                     <tr class="text-center">
                        <td class="text-center"> 120.00</td>
                        <td class="text-center"> 99.80	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 52.5</td>
                        <td class="text-center"> 91.37	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">48.8</td>
                        <td class="text-center">89.57	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">41.3</td>
                        <td class="text-center">85.97	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">40</td>
                        <td class="text-center">84.44		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">37.5</td>
                        <td class="text-center">83.32		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">33.8	</td>
                        <td class="text-center">80.51	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">30</td>
                        <td class="text-center">77.8	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 26.3	</td>
                        <td class="text-center"> 73.55	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 22.5	</td>
                        <td class="text-center"> 69.87	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 18.8	</td>
                        <td class="text-center"> 64.76		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">15	</td>
                        <td class="text-center">60</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">11.3		</td>
                        <td class="text-center">54.74	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">7.5	</td>
                        <td class="text-center">49.14		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center"> 3.8	</td>
                        <td class="text-center"> 43.31</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">-3.8		</td>
                        <td class="text-center">30	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">-7.5		</td>
                        <td class="text-center">24.2		</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">-15			</td>
                        <td class="text-center">14.45	</td>
                    </tr>
                   
                </tbody>
            </table>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
            <div class="qty" style="color:red;text-align:center;">Quantitative Ability	</div>

            <table id="tblqty">
                <thead class="text-center">
                     <tr class="text-center">
                        <th class="text-center">
                            Marks
                        </th>
                        <th class="text-center">
                            Percentile
                        </th>
                    </tr>
                </thead>
                <tbody class="text-center">
                     <tr class="text-center">
                        <td class="text-center"> 100.00	</td>
                        <td class="text-center"> 99.5	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           90.00
                        </td>
                        <td class="text-center">
                            99.20	                        </td>
                    </tr>


                     <tr class="text-center">
                        <td class="text-center">
                           80.00
                        </td>
                        <td class="text-center">
                            98.42	
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            70.00
                        </td>
                        <td class="text-center">
                            96.93
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           67.50	
                        </td>
                        <td class="text-center">
                           96.34
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            60.00	
                        </td>
                        <td class="text-center">
                           95.07	
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           56.30
                        </td>
                        <td class="text-center">
                           93.31
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           100.00	    
                        </td>
                        <td class="text-center">
                           99.5
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            52.5
                        </td>
                        <td class="text-center">
                            92.14	
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            48.8	
                        </td>
                        <td class="text-center">
                            90.47	
                        </td>
                        </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            45	
                        </td>
                        <td class="text-center">
                            88.61	
                        </td>
                        </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            41.3	
                        </td>
                        <td class="text-center">
                            86.21
                        </td>
                        </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           40
                        </td>
                        <td class="text-center">
                            84.46	
                        </td>
                        </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            37.5	
                        </td>
                        <td class="text-center">
                           83.16	
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           26.3	    
                        </td>
                        <td class="text-center">
                           72.73	
                        </td>
                        </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           22.5	
                        </td>
                        <td class="text-center">
                            68.47	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           11.3	
                        </td>
                        <td class="text-center">
                            52.66	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           7.5	
                        </td>
                        <td class="text-center">
                    47.04	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                       3.8
                        </td>
                        <td class="text-center">
                          41.19	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            0	    
                        </td>
                        <td class="text-center">
                           35.31
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            -3.8	
                        </td>
                        <td class="text-center">
                           27.96	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           -11.3	
                        </td>
                        <td class="text-center">
                           17.78	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            -15	
                        </td>
                        <td class="text-center">
                           13.71	
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           
                        </td>
                        <td class="text-center">
                            
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           
                        </td>
                        <td class="text-center">
                          
                        </td>

                    </tr>
                   
                </tbody>
            </table>
        </div>

        <div class="col-md-4 col-xs-12 col-sm-12">
            <div class="qty" style="color:red;text-align:center;">Quantitative Ability	</div>

            <table id="tblqty">
                <thead class="text-center"> 
                     <tr class="text-center">
                        <th class="text-center">
                            Marks
                        </th>
                        <th class="text-center">
                            Percentile
                        </th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <tr class="text-center">
                        <td class="text-center">170.00</td>
                        <td class="text-center">99.92	</td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            150.00	
                        </td>
                        <td class="text-center">
                            99.27
                        </td>
                    </tr>


                     <tr class="text-center">
                        <td class="text-center">
                            130.00
                        </td>
                        <td class="text-center">
                            96.89
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            110.00
                        </td>
                        <td class="text-center">
                            92.79

                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            105.00  
                        </td>
                        <td class="text-center">
                            91.39

                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            97.50
                        </td>
                        <td class="text-center">
                            88.54
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            93.80
                        </td>
                        <td class="text-center">
                            87.26
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            170.00
                        </td>
                        <td class="text-center">
                            99.92
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            82.5	
                        </td>
                        <td class="text-center">
                            80.92

                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            78.8
                        </td>
                        <td class="text-center">
                           78.95

                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            75
                        </td>
                        <td class="text-center">
                           76.57
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            71.3
                        </td>
                        <td class="text-center">
                            72.84
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           67.5	
                        </td>
                        <td class="text-center">
                            70.69
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            63.8
                        </td>
                        <td class="text-center">
                            67.52
                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            60	
                        </td>
                        <td class="text-center">
                            65.34

                        </td>
                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            52.5
                        </td>
                        <td class="text-center">
                            57.97
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            48.8	
                        </td>
                        <td class="text-center">
                           54.9
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                           45	
                        </td>
                        <td class="text-center">
                           51.42
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            41.3
                        </td>
                        <td class="text-center">
                            46.37
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            33.8
                        </td>
                        <td class="text-center">
                            40.1

                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            30
                        </td>
                        <td class="text-center">
                            36.4
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            26.3
                        </td>
                        <td class="text-center">
                            31.99

                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">
                            22.5	
                        </td>
                        <td class="text-center">
                            28.36

                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">18.8</td>
                        <td class="text-center">
                            25.27
                        </td>

                    </tr>
                     <tr class="text-center">
                        <td class="text-center">15</td>
                        <td class="text-center">
                            22.11
                        </td>

                    </tr>

                </tbody> 
            </table>
        </div>
    </div>
		</div>
      </div>
     </div>
 </div>
</section>
