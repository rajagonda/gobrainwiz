    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Batches</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active"><a>Batches</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                <!-- col left table -->
                <div class="col-lg-8">
                   <!-- table --> 
					<?php if(count($batches)>0) {?>				   
                    <div class="table-section">
                        <table class="table table-responsive">
                            <thead>
                                <tr>   
                                    <th>Batch Name</th>  
									<th>Faculty</th>	
                                    <th>Date</th>
                                    <th>Time</th>
                                   
                                    
                                </tr>
                            </thead>
                            <tbody>
								<?php foreach($batches as $batch){
									
									//Our "then" date.
									$then =  $batch->start_date;
									 
									//Convert it into a timestamp.
									$then = strtotime($then);
									 
									//Get the current timestamp.
									$now = time();
									 
									//Calculate the difference.
									$difference = $now - $then;
									 
									//Convert seconds into days.
									$days = floor($difference / (60*60*24) );		
									
									
								?>
                                <tr>  
                                    <td><?php echo $batch->batch_name;?></td>  
									<td><?php echo $batch->faculty_name;?></td>          	
                                    <td><?php echo date('jS M Y', strtotime($batch->start_date));?></td>
                                    <td><?php echo $batch->from;?>  <?php //echo "-".$batch->to;?></td>
                                   
                                    
                                </tr>
								<?php } ?>
                            </tbody>
                        </table>
                    </div>
					<?php }?>
                   <!--/ table -->
                </div>
                <!--/ col left table-->

                <!-- col right contact form -->
                <div class="col-lg-4">
                    <!-- gray bg -->
                    <div class="bggray p-4 mb-3">
                        <h2 class="h5 py-2 fbold">Request for Demo </h2>
                            <div id="ajax_loader" class="wait" style="display:none"></div>
                        <form name="frmDemo" id="frmDemo" method="post">
                            <!-- form group -->
                            <div class="form-group">
                                <label>Name</label>
                                <div class="input-group">
                                    <input type="text" name="yourname" id="yourname" placeholder="First Name" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->


                             <!-- form group -->
                             <div class="form-group">
                                <label>Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" id="mobile" name="mobile" placeholder="Mobile Number" class="form-control">
                                </div>
                            </div>
                            <!--/ form group-->

                            <!-- form group -->
                            <div class="form-group">
                                <label>Email ID</label>
                                <div class="input-group">
                                    <input type="text" id="email" name="email" placeholder="Email ID" class="form-control">
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label>Select Course</label>
                                <div class="input-group">
                                    <select class="form-control" id="course" name="course">
                                        <option>CRT</option>
                                        <option>C Programming</option>
                                        <option>Amcat</option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group">
                                <label>Message</label>
								<div class="input-group">
								<textarea id="message" name="message" placeholder="Message..." style="height:60px" class="form-control"></textarea>
								</div>
							</div>	
								
							
                            <!--/ form group-->

                            <input type="submit" value="Submit Enquiry" class="bluebtn">

                        </form>
                    </div>
                    <!--/ gray bg -->
                    <!-- bg gray -->
                    <div class="bggray p-4 mb-3">
                        <h2 class="h5 py-2 fbold">Key Features</h4>
                        <ul class="page-list">
                            <li>Comprehensive Training on CRT </li>
                            <li>More than 120 hours of Training</li>
                            <li>30 to 45 days Program</li>
                            <li>Covering 55+ Topics with latest MNC Questions</li>
                            <li>Sunday Special Sessions covering AMCAT & E-Litmus</li>                           
                        </ul>
                    </div>   
                    <!--/ bg gray -->                 
                </div>
                <!--/ col right contact form -->
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>