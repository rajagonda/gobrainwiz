<!-- slider home -->
    <div id="carouselExampleIndicators" class="carousel slide homecarousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php 
				if(count($header_banners) > 0){
					$i = 1;
					foreach($header_banners as $hbanner){
						
						if($i== 1)
							$active = 'active';
						else 
							$active = '';
					
			?>
            <div class="carousel-item <?php echo $active;?>" style="background-image: url('<?php echo base_url();?>upload/banners/<?php echo $hbanner->banner_location;?>')">
                <div class="carousel-caption">
                    <?php if($hbanner->banner_title) {?>
                        <h1 class="fbold py-1"><?php echo $hbanner->banner_title;?></h1>
                    <?php }?>      
                      <?php if($hbanner->banner_desc) {?>   
                        <p><?php echo $hbanner->banner_desc;?></p>
                      <?php }?>  
                </div>
            </div>  
					<?php $i++; }
					
					}
					?>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="icon-chevron-left icomoon"></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="icon-chevron-right icomoon"></span> </a>

            <!-- crt course form -->
            <div id="ajax_loader" class="wait" style="display:none"></div>
            <div class="course-form" id="course-form">
                <div class="course-formin">
                    <h3 class="text-center">Like to know about CRT Course?</h3>               
                    <form name="frmHome" id="frmHome">
                        <input class="form-control" type="text" name="yourname" id="yourname" placeholder="Your Name">                   
                        <input class="form-control" type="text" name="mobile" id="mobile" placeholder="Phone Number">
                        <textarea class="form-control" name="message" id="message" placeholder="Write Query" style="height:100px;"></textarea>  
						<input  type="submit" value="Submit"> 						
                    </form>
                   
                </div>
            </div>
            <!--/ crt course form -->
    </div>
<!--/ slider home --> 

      <!-- services -->
    <div class="services-home">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a  href="<?php echo base_url('pages/weekly_schedule');?>"><span class="icon-timetable icomoon"></span></a>
                        <h4>Weekly Schedule</h4>
                        <p>Keeping daily track on syllabus for Quick completion </p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="<?php echo base_url('videos/homevideos');?>"><span class="icon-youtube icomoon"></span></a>
                        <h4>Video Tutorials</h4>
                        <p>Learn More from our Online Tutorials</p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="<?php echo base_url('onlinetest/dashboard')?>"><span class="icon-computer icomoon"></span></a>
                        <h4>Test Series</h4>
                        <p>Get live Exam Experience by Various MNC Mock Tests</p>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 text-center">
                    <div class="service-col">
                        <a href="<?php echo base_url('practicetest');?>"><span class="icon-wedding-planning icomoon"></span></a>
                        <h4>Practice Tests</h4>
                        <p>Improve your speed & accuracy by Topic wise Tests. </p>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ services -->

       <!-- top Tutorials -->
     <div class="top-tutorial">
        <!-- container -->
        <div class="container">
             <!-- title row -->
             <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Courses <span> We Offer</span></h3>
                    <p>Explore more with our featured programs.</p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--/ container -->

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row pt-4">
               <!-- col -->
               <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="<?php echo base_url('pages/crt');?>"><img src="<?php echo assets_url();?>img/crtcourseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="<?php echo base_url('pages/crt');?>">Campus Recruitment Training </a></h3>
                            <p  class="pb-3 text-center">Campus Recruitment Training is a comprehensive program which is exclusively designed for the graduate students preparing them for recruitment of various IT and NON IT companies...... </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue"></h5>
                                <!-- <h6 class="h6 fblue"> <a href="<?php echo base_url()?>/upload/curriculams/sample.pdf" download><span class="icon-download icomoon"></span> Curriculum</a></h6>--> 
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="<?php echo base_url('pages/amcat');?>"><img src="<?php echo assets_url();?>img/amcat-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="<?php echo base_url('pages/amcat');?>">Amcat </a></h3>
                            <p  class="pb-3 text-center">The AMCAT is an computer adaptive test which measures the job applicants on critical areas like logical reasoning, communication skills, quantitative skills and job specific domain skills.... </p>
                            <div class="d-flex justify-content-between">
                            <h5 class="h6 fblue"></h5>
                            <!--
                            <h6 class="h6 fblue"> <a href="<?php echo base_url()?>/upload/curriculams/sample.pdf#" download><span class="icon-download icomoon"></span> Curriculum</a></h6>          -->            
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="course-col">                       
                        <a href="<?php echo base_url('pages/elitmus');?>"><img src="<?php echo assets_url();?>img/elitmus-courseimg.svg" alt="" class="img-fluid "></a>
                        <article class="p-3 text-center">
                            <h3><a href="<?php echo base_url('pages/elitmus');?>">E-Litmus </a></h3>
                            <p  class="pb-3 text-center">E-litmus is an Indian recruitment organization founded by ex-Infosys employees. It helps companies in hiring fresher’s for their entry-level jobs through its effective and unique test.... </p>
                            <div class="d-flex justify-content-between">
                                <h5 class="h6 fblue"></h5>
                               <!-- <h6 class="h6 fblue"> <a href="<?php echo base_url()?>/upload/curriculams/sample.pdf" download><span class="icon-download icomoon"></span> Curriculum</a></h6>  --> 
                            </div>
                        </article>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ top Tutorials -->
   
    <!-- table section -->
    <div class="table-section sectionmedium">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-7 align-self-center">
                    <!-- table -->
					<?php
					if($batches !=''){
						?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Course name</th>
                                <th scope="col">Date </th>
                                <th scope="col">Timings </th>                               
                            </tr>
                        </thead>
                        <tbody>
						    <?php foreach($batches as $value){
									//Our "then" date.
									$then =  $value->start_date;
									 
									//Convert it into a timestamp.
									$then = strtotime($then);
									 
									//Get the current timestamp.
									$now = time();
									 
									//Calculate the difference.
									$difference = $now - $then;
									 
									//Convert seconds into days.
									$days = floor($difference / (60*60*24) );	
							?>
                            <tr>
                                <td scope="row"><?php echo $value->batch_name;?>
								 <?php if($days <16) { ?>
								<span class="fgreen blink-text batchtxt">(New Batch)</span>
								 <?php }?>
                                </td>
                                <td><?php echo date('jS M Y', strtotime($value->start_date));?></td>
                                <td><?php echo $value->from;?> - <?php echo $value->to;?></td>                               
                            </tr>
							<?php }?>
                        </tbody>
                    </table>
					<?php }?>
                    <!--/ table -->
                </div>
                <!--/ col -->

                <!-- slider col -->
                <div class="col-lg-5">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php 
								$i=1;	
								foreach($pbanners as $pbanner){
									if($i== 1)
										$active = 'active';
									else 
										$active = '';
									
							?>
							<div class="carousel-item <?php echo $active;?>">
                                <img class="d-block w-100" src="<?php echo base_url().'upload/banners/'.$pbanner->banner_location;?>" alt="First slide">
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <!--/ slider col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ table section -->
    <!-- why choose section -->
    <div class="why-choosesection">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">                  
                    
                    <!-- video -->
                    <div class="video-utube pt-2">
                        <div class="whitebox">
                            <video width="100%"  playsinline controls="" muted="" autoplay="autoplay">
                                <source src="http://www.gobrainwiz.in/upload/Videos/RAMA7308.mp4" type="video/mp4">
                            Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                    <!--/ video -->
                </div>
                <!--/ col -->
                <!-- col 6-->
                <div class="col-lg-6">
                    <h5 class="m-text-center m-py-10">Why Most of the Students <span>Prefer BRAINWIZ!</span></h5>
                    <p class="fmediumsize m-text-center"> Stay ahead in your career by exploring our featured programs <a class="fgreen d-block" href="<?php echo base_url();?>Aboutus.html">Read More</a> </p>
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-professor icomoon"></span>
                            <h6>Expert Faculty</h6>
                            <p>Mentoring by expert faculty to learn in depth concepts efficiently</p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-first icomoon"></span>
                            <h6>Assessments</h6>
                            <p>Get the Quality Training for all Assessment Exams like AMCAT, Elitmus & Co-Cubes to grab a good score. </p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-customer icomoon"></span>
                            <h6>Comprehensive Training</h6>
                            <p>Giving In-depth Training on 5 modules covering 55+ topics with complete Tricks & Shortcuts </p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 whycol">
                            <span class="icon-person-giving-a-lecture-for-big-auditory icomoon"></span>
                            <h6>Best Results</h6>
                            <p>Trained more than 45,000+ Students with more than 90% Success rate. </p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col 6-->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ why choose section -->

    <!-- our track record -->
    <div class="our-trackrecord d-none d-sm-block">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 leftTrackRecordColumn align-self-center">
                    <article class="position-relative">
                        <h2>Our Track Record</h2>                        
                    </article>
                </div>
                <!--/ col -->

                <!-- right column -->
                <div class="col-lg-8 align-self-center">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-reading icomoon"></span>
                            <h2 class="mb-0">1800+</h2>
                            <p>Students we trained</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-select icomoon"></span>
                            <h2 class="mb-0">1300+</h2>
                            <p>Students we Placed</p>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 text-center recordcolumn">
                            <span class="icon-presentation icomoon"></span>
                            <h2 class="mb-0">760+</h2>
                            <p>Taken Batches</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!-- right column -->
            </div>
            <!--/ row -->
        </div>
        <!-- container fluid -->
    </div>
    <!--/ our track record -->

        <!-- our success stories -->
    <div class="success-stories">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Our Success <span>Stories</span></h3>
                    <p>Students Trained by Pavan Jaiswal 2019 Batch <a href="<?php echo base_url();?>Students.html" class="fgreen d-inline-block pl-2"> View All</a></p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--/ container -->
        <!-- container fluid -->
        <div class="container">
            <!-- slick -->
            <!-- slick slider row -->
            <div class="successStories custom-slick students-slick">
                <!-- slide -->
				<?php foreach($video_testimonials as $vt){
						$img_url = $img_url = base_url().'upload/topper/'.$vt->voice_image;
				?>
                <div class="slide">
                    <a class="videotestimonial" href="javascript:;" data-toggle="modal" data-target="#student-testimonial" data-url="<?php echo $vt->student_video_url;?>">
                        <img src="<?php echo $img_url;?>" alt="" class="img-fluid">
                        <h4><?php echo $vt->student_name;?></h4>
                        <h5><?php echo $vt->company;?></h5>
                    </a>
                </div>
				<?php }?>
                <!--/ slide -->
            </div>
            <!--/ slick slider row -->
            <!--/ slick -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ our success stories -->

    <!-- home page testimonials -->
    <div class="home-testimonials">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>What our<span> Students says</span></h3>
                    <p>                        
                        <a target="_blank" href="https://www.google.com/search?sxsrf=ACYBGNRTLTAgLveMpFHo3LHZKhUJW6X-9A%3A1577011136159&source=hp&ei=wEf_XYfNB5yc4-EPoOaP2As&q=brainwiz&oq=brainwiz&gs_l=psy-ab.3..35i39j0l7j0i10j0.3358.4032..4179...1.0..0.171.943.3j5......0....1..gws-wiz.......0i131j0i67j0i10i67j0i131i67.Z7_diWgoIew&ved=0ahUKEwiHrc2kiMnmAhUczjgGHSDzA7sQ4dUDCAY&uact=5#lrd=0x3bcb90c62074a435:0xc940fb93864b35d,1,,," class="fgreen d-inline-block pl-4">Google Reviews</a>
                    </p>
                </div>
            </div>
            <!--/ title row -->

            <!-- slick slide row -->
            <div class="students-testimonials custom-slick">
                <!-- slide -->
				<?php 
					if($toppers!=''){
						foreach($toppers as $value){                                                    
				?>
                <div class="slide text-center">
                    <img src="<?php echo base_url();?>upload/topper/<?php echo $value->voice_image;?>" alt="" title="" class="img-fluid">
                    <h5><?php echo $value->student_name;?></h5>
                    <h6>Placed in <?php echo $value->company;?></h6>
                    <p><?php echo $value->voice_description;?></p>
                </div>
				<?php } 
				}?>
                <!--/ slide -->
            </div>
            <!--/ slick slide row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ hoem page testimonials -->

     <!-- android mobile app-->
    <div class="apstore">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                    <article>
                        <h3>Download & Enjoy</h3>
                        <p class="py-2">Access your courses anywhere, anytime & prepare with practice tests</p>
                        <a href="https://play.google.com/store/apps/details?id=com.gobrainwiz" target="_blank" class="d-flex p-2">
                            <span class="icon-android icomoon"></span>
                            <div>
                                <p class="small">Available on</p>
                                <h4>Google Play</h4>
                            </div>
                        </a>
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 text-right align-self-end d-none d-sm-block">
                    <img src="<?php echo assets_url();?>img/appmobileimg.png" class="img-fluid" alt="">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ coontainer -->
    </div>
    <!--/ android mobile app-->
    <!-- major companies -->
    <div class="major-companies">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Training Focus on <span>Major Companies</span></h3>
                    <p>Join 1,700 companies using BRAINWIZ to upskill their employees</p>
                </div>
            </div>
            <!--/ title row -->
        </div>
        <!--./ container -->

        <!-- container fluid -->
        <div class="container-fluid">
            <div class="companies custom-slick">
                <!-- slide -->
				<?php 
					if(!empty($companies_placed)){
						foreach($companies_placed as $company_placed){
							$comp_img_url = base_url().'upload/companies/'.$company_placed->cimage;
				?>
                <div class="slider">
                    <img src="<?php echo $comp_img_url;?>" alt="<?php echo $company_placed->cname;?>" title="<?php echo $company_placed->cname;?>">
                </div>
				<?php } 
				}?>
                <!--/ slide -->
            </div>
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ major companies -->

    <!-- blogs -->
    
    <div class="home-blogs d-none d-sm-block">
        <!-- container -->
        <div class="container">
            <!-- title row -->
            <div class="row justify-content-center titlerow">
                <div class="col-lg-6 text-center">
                    <h3>Our <span>Blog</span></h3>
                    <p>News & Updates <a href="<?php  echo base_url('blog');?>"class="fgreen d-inline-block pl-2">View All</a></p>
                </div>
            </div>
            <!--/ title row -->

            <!-- row -->
            <div class="row py-3">
			    <!-- blog col -->
				<?php 
                               
                                foreach($posts as $post){
						$post_url = base_url('blog').'/post_details/'.$post->post_slug;
				?>
                <div class="col-lg-4">
                    <div class="blogcol">
                        <a href="<?php echo $post_url;?>">
							<?php 
								if(empty($post->post_thumb_image))
									$img_url = base_url().'assets/images/noimage.jpg';
								else 
									$img_url = base_url().'upload/post_images/thumbnail/'.$post->post_thumb_image;
							?>
							<img src="<?php echo $img_url;?>" alt="" class="img-fluid">
                        </a>
                        <article>
                            <h4><a href="<?php echo $post_url;?>"><?php echo $post->post_title;?></a></h4>
                            <?php echo substr(trim($post->post_description),0,80);?>...
                        </article>
                        <p class="d-flex justify-content-between">
                            <span class="fblue">Posted on <?php echo date("D d, Y",strtotime($post->created_on));?></span>
                        </p>
                    </div>
                </div>
				<?php } ?>
                <!--/ blog col -->
			</div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ blogs -->
    <!--/ main -->
<!-- weekly schedule pop-->    
<div class="modal fade" id="weeklyschedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Weekly Schedule</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
			<?php if($weekly){ ?>
                  <img src="<?php echo base_url();?>upload/weekly/<?php echo $weekly->image; ?>" class="img-fluid">
			<?php }  else { ?>
                 <img src="<?php echo assets_url();?>images/weeklyimg/June 19.jpg" class="img-fluid">
			<?php } ?>
		</div>
        
        </div>
    </div>
</div>
<!--/ weekly schedule pop-->
<!-- Modal video -->
<!-- Modal video -->
<div class="modal fade video-modal" id="student-testimonial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">               
                <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="ytplayer" mute="1" class="modalvideo" width="100%" src="" allow="autoplay" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>               
            </div>
        </div>
    </div>
</div>
<!--/ modal video --> 

<script type="text/javascript">
$('.videotestimonial').on( "click",function () {
	var url = $(this).attr('data-url');
	$('.video-modal').on('hidden.bs.modal', function (e) {
	$('.video-modal iframe').attr('src', '');
	});

	$('.video-modal').on('show.bs.modal', function (e) {
		$('.video-modal iframe').attr('src', url+'?rel=0&amp;autoplay=1');
	});
});
</script>