<!-- page-intro start-->
			<!-- ================ -->
			<div class="page-intro">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li><i class="fa fa-home pr-10"></i><a href="index.html">Home</a></li>
								<li class="active">Portfolio 4 Columns</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<!-- page-intro end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Portfolio 4 Columns</h1>
							<div class="separator-2"></div>
							<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas nulla suscipit <br class="hidden-sm hidden-xs"> unde rerum mollitia dolorum.</p>
							<!-- page-title end -->
							
							<!-- isotope filters start -->
							<div class="filters">
								<ul class="nav nav-pills">
									<li class="active"><a href="#" data-filter="*">All</a></li>
									<li><a href="#" data-filter=".web-design">Web design</a></li>
									<li><a href="#" data-filter=".app-development">App development</a></li>
									<li><a href="#" data-filter=".site-building">Site building</a></li>
								</ul>
							</div>
							<!-- isotope filters end -->

						</div>
						<!-- main end -->
					</div>
				</div>

				<!-- section start -->
				<!-- ================ -->
				<div class="gray-bg section">
					<div class="container">
						<!-- portfolio items start -->
						<div class="isotope-container row grid-space-10">
							<div class="col-sm-6 col-md-3 isotope-item web-design">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/brine.jpg" alt="brine">
										<a href="http://brainwizz.in/" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Web Design</span>
										</a>
									</div>
									<h3><a href="#">Brainwizz</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item app-development">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/mym.jpg" alt="mym">
										<a href="http://mymrecharge.com/" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>App Development</span>
										</a>
									</div>
									<h3><a href="#">MYM Recharge</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item web-design">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/sms.jpg" alt="sms">
										<a href="#" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Web Design</span>
										</a>
									</div>
									<h3><a href="#">SMS Nestham</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item site-building">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/nestam.jpg" alt="nestham">
										<a href="http://rechargenestham.com/" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Site Building</span>
										</a>
									</div>
									<h3><a href="#">Recharge Nestham</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item app-development">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/ride.jpg" alt="ride">
										<a href="http://nesthamride.com/" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>App Development</span>
										</a>
									</div>
									<h3><a href="#">Nestham Ride</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item web-design">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/pay.png" alt="pay">
										<a href="payduniya.com" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Web Design</span>
										</a>
									</div>
									<h3><a href="#">Pay Duniya</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item site-building">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/bvp.jpg" alt="bvp">
										<a href="#" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Site Building</span>
										</a>
									</div>
									<h3><a href="http://bvpbusinesssolutions.com/">BVP Business Solutions</a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="http://bvpbusinesssolutions.com/" class="btn btn-default">Read More</a>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 isotope-item web-design">
								<div class="box-style-1 white-bg">
									<div class="overlay-container">
										<img src="<?php echo assets_url();?>img/sites/online.jpg" alt="online">
										<a href="https://www.cubelegal.co.uk/" class="overlay small">
											<i class="fa fa-plus"></i>
											<span>Web Design</span>
										</a>
									</div>
									<h3><a href="#">Cube Legal online </a></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="#" class="btn btn-default">Read More</a>
								</div>
							</div>
						</div>
						<!-- portfolio items end -->
					</div>
				</div>
				<!-- section end -->
			</section>
			<!-- main-container end -->