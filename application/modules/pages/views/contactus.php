<!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Contact <span class="fbold">Us</span></h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->
         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active"><a>Contact</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body" style="min-height:auto">
           <!-- container -->
           <div class="container contactpage">
              <!-- row -->
              <div class="row">
                    <!-- col -->
                    <div class="col-lg-8">                      
                        <div id="ajax_loader" class="wait" style="display:none"></div>
                        <h2 class="h5 py-2">Drop us Message</h2>
                        <div id="message" style="display: none;"></div>
                        <form class="contact-form" id="contactFrm" name="contactFrm" >
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="yourname" name="yourname" placeholder="Your Name">
                                    </div>
                                </div>
                                <!--/ col -->
                                  <!-- col -->
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Your Email">
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                             <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone Number">
                                    </div>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <select class="form-control" name="course">
                                            <option>Select Course</option>
                                            <option>Campus Recruitment</option>
                                            <option>C Programming</option>
                                            <option>Web Development</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                            
                            <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" id="message" placeholder="Message..." style="height:100px;"></textarea>
                                    </div>
                                </div>
                                <!--/ col -->                                 
                            </div>
                            <!--/ row -->
                            <!--row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="submit" class="bluebtn" value="Submit Your Query">
                                </div>
                            </div>
                            <!--/ row -->
                        </form>
                    </div>
                    <!--/ col -->

                    <!-- contact address column-->
                    <div class="col-lg-4">
                        <h2 class="h5 py-2">Location</h2>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d30451.17976932182!2d78.448345!3d17.44068!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc940fb93864b35d!2sBRAINWIZ_CRT%20Training%20Institute!5e0!3m2!1sen!2sin!4v1573179284981!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                    <!--/ contact address column -->
              </div>
              <!--/ row -->
            </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
