<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : pages_model
 * Mail id : phpguidance@gmail.com
  */
class pages_model extends CI_Model {	

	 public function getTesmonials(){     
        
        $sql = "SELECT  * FROM gi_testmonials WHERE testmonial_status='y'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
           
    }
    public function get_banners($type){

       $sql = "SELECT  * FROM gk_mobile_banners 
              WHERE  banner_status='y' AND banner_type='".$type."'
              ORDER BY banner_position ASC";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        } 
    }
    public function getToppers(){
        $this->db->order_by('voice_status','y');
        $query = $this->db->get('gk_voice_toper');
        if($query->num_rows()>0){
          return $query->result();
        }else {
            return '';
        }
    }
	
	public function home_blog_posts(){
		$sql = "SELECT  * FROM gk_blog_posts 
              WHERE status ='1' ORDER BY post_id DESC Limit 0,3";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        } 
	}
	public function companies_placed(){
		$sql = "SELECT  * FROM gk_companies_placed 
              WHERE status ='1' ORDER BY cid";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        } 
	}
	
	public function home_video_tutorials(){
		$sql = "SELECT  * FROM gk_voice_toper 
              WHERE voice_status ='y' and student_video_url!='' ORDER BY voice_id limit 0,4";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
		
	}
	
	public function video_tutorials(){
		$sql = "SELECT  * FROM gk_voice_toper 
              WHERE voice_status ='y' and student_video_url!='' ORDER BY voice_id";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
	}
	
    public function getWeekly(){
        $this->db->order_by('status','y');
        $query = $this->db->get('gk_weekly');
        if($query->num_rows()>0){
          return $query->row();
        }else {
            return false;
        }
    }
    public function getupdates(){
        $this->db->order_by('update_status','y');
        $query = $this->db->get('gk_latestupdates');
    if($query->num_rows()>0){
      return $query->result();
    }else {
      return '';
    }
        }

        
        
	public function home_getBatchs(){
             
             $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id ORDER BY start_date desc Limit 0,4";
             
              $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }
        }
        
        
        public function getBatchs(){
             
             $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id ORDER BY start_date desc";
             
              $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }
        }

}