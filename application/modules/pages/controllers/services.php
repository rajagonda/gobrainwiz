<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : services
 * Mail id : phpguidance@gmail.com
  */
class services extends MY_Controller {
      
	public function __construct() {

	    parent::__construct();
           $this->load->library('template');  
        }
       
    public function servicepages($id){

       $this->template->set_title('Innoprie Technologies');
       if($id == '1'){
            $this->template->theme('services');
       }else if($id == '2'){
            $this->template->theme('mobileappdevelopment');
       } else if($id == '4'){
            $this->template->theme('businessprocess');
       } else if($id == '5'){
            $this->template->theme('webapplication');
       } else if($id == '6'){
            $this->template->theme('qualityassurance');
       } 
             
       
    }

    public function about(){
        $this->template->set_title('About Us');
        $this->template->theme('about');	
    }
    
  
    public function career(){
		$this->template->set_title('Career');
		$this->template->theme('career');
    }
    public function faq(){
    	$this->template->set_title('faq');
		$this->template->theme('faq');
    }
    public function contactus(){
		$this->template->set_title('Contactus');
		$this->template->theme('contactus');
    }    
    public function submitcontact() {   
        $config = array();
        $config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "smtp";
        $config['smtp_host']           = "localhost";
        $config['smtp_port']           = "25";
        $config['mailtype'] = 'html';
        $config['charset']  = 'utf-8';
        $config['newline']  = "\r\n";
        $config['wordwrap'] = TRUE;
        $this->load->library('email');
        $this->email->initialize($config);
            if(isset($_POST['submit'])) {

            $name = $this->input->post('uname');
            $fromemail = $this->input->post('email');
            $mobile = $this->input->post('phone');
            $message = $this->input->post('comment');
            ob_start();  
            ?>

 <!--mail template stsrt here-->
        <table border="0" cellspacing="2"  cellpadding="2" width="100%" bgcolor="#323232">
                    <tr>
                    <td bgcolor='#323232' colspan='6'><h3 style='color: #fff; font-family: ubuntuc; padding-left:15px;'>Contact Us</h3></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Name</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $name;?></td>
                    </tr>
                  
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Email </td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $fromemail;?></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Phone Number</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $mobile;?></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Message</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $message;?></td>
                    </tr>
                    </table>
                  <!--mail template end here-->
                    


    <?php   
     
    
    

                $sendermessage  = ob_get_contents();
                ob_end_clean(); 
            
            
            
            
          $this->email->from($fromemail, $name);
          $this->email->to('smsnestham@gmail.com'); 
          $this->email->cc('phpguidance@gmail.com'); 
          $this->email->subject('Contact Us');
          $this->email->message($sendermessage);    
          if($this->email->send()){
             // echo "ok"; exit;
          } else {
              //echo $this->email->print_debugger();exit;
          }
          $this->session->set_userdata('contact_success', "Thank you for the request.<br />We will be in touch shortly!<br /><br />Need to speak to someone straight away?
            Why not call us on 8686994774.");
         // echo $this->session->userdata('contact_success');exit;
          redirect(base_url()."contactus.html");
          } else {
           $this->session->set_userdata('contact_filure', "Sorry Unable to submit from..");
             redirect(base_url()."contactus.html");
          }
        }
    



       
        
    }
    


/* End of file skeleton.php */
/* Location: ./application/modules/skeleton/controllers/skeleton.php */