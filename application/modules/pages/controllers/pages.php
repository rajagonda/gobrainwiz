<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : pages
 * Mail id : phpguidance@gmail.com
  */
require (FCPATH .'/phpmailer/PHPMailerAutoload.php');

class pages extends MY_Controller 
{
      
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('template'); 
        $this->load->model(array('common_model','pages_model'));
        $this->load->helper('sendmail_helper');   
    }
    public function home()
    {
        $data['toppers']            = $this->pages_model->getToppers();
        $data['video_testimonials'] = $this->pages_model->home_video_tutorials();
        $data['header_banners']     = $this->pages_model->get_banners('h');
        $data['pbanners']           = $this->pages_model->get_banners('p');
        $data['batches']            = $this->pages_model->home_getBatchs();
        $data['updates']            = $this->pages_model->getupdates();
        $data['companies_placed']   = $this->pages_model->companies_placed();
        $data['posts']              = $this->pages_model->home_blog_posts();
        $data['weekly']             = $this->pages_model->getWeekly();
		
        $this->template->theme('home',$data);
    }
    public function weeklySchedule()
    {
        $this->template->set_title('weeklySchedule');
        $data['weekly'] = $this->pages_model->getWeekly();
        $this->template->theme('weeklySchedule',$data);
    }
    public function about()
    {
        $this->template->set_title('About Us');
        $this->template->theme('about');	
    }
    public function amcat()
    {
        $this->template->set_title('amcat');
        $this->template->theme('amcat');
    }
    public function crt()
    {
        $this->template->set_title('crt');
        $this->template->theme('crt');
    }
	
    public function cocubes()
    {
        $this->template->set_title('cocubes');
        $this->template->theme('cocubes');
    }
    public function allcourses()
    {
        $this->template->set_title('allcourses');
        $this->template->theme('allcourses');
    }
    
    public function weekly_schedule(){
        
        $this->template->set_title('weeklySchedule');
        $data['weekly'] = $this->pages_model->getWeekly();
        $this->template->theme('weekly_schedule',$data);
        
        
    }
	
    public function batches()
    {
       $data['batches'] = $this->pages_model->getBatchs();
       $this->template->set_title('batches');
       $this->template->theme('batches',$data);
    }
	
    public function students()
    {
        $data['video_testimonials'] = $this->pages_model->video_tutorials();
        $this->template->set_title('students');
        $this->template->theme('students',$data);
    }
    
    public function privacy()
    {
        $this->template->set_title('privacy');
        $this->template->theme('privacy');
    }
	
    public function terms()
    {
        $this->template->set_title('terms');
        $this->template->theme('terms');
    }
	
    public function elitmus()
    {
    	$this->template->set_title('elitmus');
        $this->template->theme('elitmus');
    }
    public function gate()
    {
    	$this->template->set_title('gate');
        $this->template->theme('gate');
    }
	
    public function clat()
    {
    	$this->template->set_title('clat');
        $this->template->theme('clat');
    }
    public function afcat()
    {
    	$this->template->set_title('afcat');
        $this->template->theme('afcat');
    }
    public function ccat()
    {
    	$this->template->set_title('ccat');
        $this->template->theme('ccat');
    }
    public function ssccgl()
    {
    	$this->template->set_title('ssc-cgl');
        $this->template->theme('ssc-cgl');
    }
	
    public function elitmusSyllabus()
    {
    	$this->template->set_title('elitmusSyllabus');
        $this->template->theme('elitmusSyllabus');
    }
    public function examPattern()
    {
    	$this->template->set_title('examinationPattern');
        $this->template->theme('examinationPattern');
    }
    public function registrationProcess()
    {
    	$this->template->set_title('registrationProcess');
        $this->template->theme('registrationProcess');
    }
    public function companiesHiringthrowElitmus()
    {
    	$this->template->set_title('companiesHiring');
        $this->template->theme('companiesHiring');
    }
    public function faq()
    {
    	$this->template->set_title('faq');
        $this->template->theme('faq');
    }
    public function shareUs()
    {
        $this->load->view('shareUs');
    }
    public function product()
    {
    $this->template->set_title('product');
    $this->template->theme('product');
    }
    public function contactus()
    {
        $this->template->set_title('Contactus');
        $this->template->theme('contactus');
    }
    public function newsletter()
    {
        $this->template->set_title('newsletter');
        $this->template->theme('newsletter');
    }

    public function sendenquiry()
    {
	
        if($_POST)
        {
            $data['username'] = $this->input->post('yourname');
            $data['mobile'] = $this->input->post('mobile');
            $data['email'] = $this->input->post('email'); 
            $data['course'] = $this->input->post('course');            
            $data['message'] = $this->input->post('message');            
            $user_email = $this->input->post('email');
            $message = $this->input->post('message'); 
            $contact = 0;
            $contact = $this->input->post('contact');
            if($contact == '1')
            {
                $subject = "CONTACT US TO BRAINWIZ (".$data['username'].")";
            }
            else 
            {
                $subject = "ENQUIRY TO BRAINWIZ (".$data['username'].")";
            }

            
           $message = $this->load->view('emailtemplates/contactSuccess',$data,'true');
           $this->load->library('email'); 
           /*
            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'brainwiz2015@gmail.com';
            $config['smtp_pass']    = 'Brainwiz@2015';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      
            */
           
            $config=array(
                'charset'=>'utf-8',
                'wordwrap'=> TRUE,
                'mailtype' => 'html'
            );
           
            $this->email->initialize($config);
            $to = 'gobrainwiz@gmail.com';
            
            $this->email->from('info@gobrainwiz.in', 'Brainwiz');
            $this->email->to($to); 
            $this->email->cc('mridhassunil@gmail.com'); 
            $this->email->subject($subject);
            $this->email->message($message);  

            $this->common_model->insertSingle('gk_enquiry',$formdata);
            if ( ! $this->email->send())
            {
                $data['message'] =  "Technical Error Please Try Again Once!";
                $data['id'] =  "0";
            }
            else
            {
                $formdata['enquiry_name'] = $this->input->post('username');
                $formdata['enquiry_email'] = $this->input->post('email');
                $formdata['enquiry_mobile'] = $this->input->post('mobile');
                $formdata['enquiry_message'] = $this->input->post('message');
                $formdata['enquiry_subject'] = $subject;

               // $this->common_model->insertSingle('gk_enquiry',$formdata);
                $data['message'] =  "Thank you for choosing BRAINWIZ. Our team will contact you soon.";
                $data['id'] =  "1";
            }
 
            header('Content-Type: application/json');
            echo  json_encode($data);
            exit;
        }
    }


     public function senddemo()
    {
        if($_POST)
        {
            $data['yourname'] = $this->input->post('yourname');
            $data['mobile'] = $this->input->post('mobile');
            $data['email'] = $this->input->post('email');  
            $data['course'] = $this->input->post('course');   		
            $data['message'] = $this->input->post('message');            
            $user_email = $this->input->post('email');
            $message = $this->input->post('message'); 
           
            $subject = "Request for Demo";
            $message = $this->load->view('emailtemplates/requestdemo',$data,'true');
            
            $config=array(
                'charset'=>'utf-8',
                'wordwrap'=> TRUE,
                'mailtype' => 'html'
            );

            $this->load->library('email'); 
           /* 
            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'brainwiz2015@gmail.com';
            $config['smtp_pass']    = 'Brainwiz@2015';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      
            */
            $this->email->initialize($config);
            $to = 'gobrainwiz@gmail.com';
            $this->email->from('info@gobrainwiz.in', 'Brainwiz');
            $this->email->to($to); 
            $this->email->cc('mridhassunil@gmail.com'); 
            $this->email->subject($subject);
            $this->email->message($message);  

            $this->common_model->insertSingle('gk_enquiry',$formdata);
            if ( ! $this->email->send())
            {
                $data['message'] =  "Technical Error Please Try Again Once!";
                $data['id'] =  "0";
            } 
            else 
            {
                $formdata['enquiry_name'] = $this->input->post('username');
                $formdata['enquiry_email'] = $this->input->post('email');
                $formdata['enquiry_mobile'] = $this->input->post('mobile');
                $formdata['enquiry_message'] = $this->input->post('message');
                $formdata['enquiry_subject'] = $subject;

               
                $data['message'] =  "Thank you for choosing BRAINWIZ. Our team will contact you soon.";
                $data['id'] =  "1";
            }
            header('Content-Type: application/json');
            echo  json_encode($data);
            exit;
        }
        
        
            
    } 


public function send_mail() {
    
    
        $data['yourname'] = 'Name';
        $data['mobile'] = '8885589494';
        $data['email'] = 'test@test.com';  
        $data['course'] = 'CRT';   		
        $data['message'] = 'TEST';            
       
        $message = 'Test Message'; 
           
            $subject = "Request for Demo";
            $message = $this->load->view('emailtemplates/requestdemo',$data,'true');
    
        $from_email = "brainwiz2015@gmail.com";
        $to_email = 'mridhassunil@gmail.com';
       
        $this->load->library('email');
        
        $config=array(
            'charset'=>'utf-8',
            'wordwrap'=> TRUE,
            'mailtype' => 'html'
        );

        $this->email->initialize($config);
        
     
        $to = 'mridhassunil@gmail.com';
        $this->email->from('brainwiz2015@gmail.com', 'Brainwiz');
        $this->email->to($to); 
      // $this->email->cc('mridhassunil@gmail.com'); 
        $this->email->subject($subject);
        $this->email->message($message);  
        //Send mail
        if($this->email->send())
           echo "mail sent";
        else
           echo "mail failed";
    }    
    
}    


/* End of file skeleton.php */
/* Location: ./application/modules/skeleton/controllers/skeleton.php */
