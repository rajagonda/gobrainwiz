<!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Blog</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
			<?php $post_details = $post_details[0];?>
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('blog');?>">Blog</a></li>
                        <li class="breadcrumb-item active"><a><?php echo $post_details->post_title;?></a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->
        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
                  <!-- left description -->
                  <div class="col-lg-9">
                      <div class="blog-detail">
                          <h2 class="h4"><?php echo $post_details->post_title;?></h2>
                          <ul class="list-seperator nav">
                                
                                <li>
                                    <a>On  <?php echo date("D d, Y",strtotime($post_details->created_on));?></a>
                                </li>
                          </ul>
                          <figure class="py-3">
						  <?php 
							if(empty($post_details->post_image))
								$img_url = base_url().'assets/images/noimage.jpg';
							else 
								$img_url = base_url().'upload/post_images/'.$post_details->post_image;
						?>
                              <img src="<?php echo $img_url;?>" alt="" class="img-fluid">
                          </figure>
                          <article>
                              <?php echo $post_details->post_description;?>
                          </article>
                      </div>
                  </div>
                  <!--/ left description -->

                  <!-- recnet posts -->
                  <div class="col-lg-3 recent-posts">
                      <h3 class="h4">Recent Posts</h3>
						<?php 
						foreach($recent_posts as $recent_post){
							$post_url ='';
							$img_url = base_url().'upload/post_images/thumbnail/'.$recent_post->post_thumb_image;
							$post_url = base_url('blog').'/post_details/'.$recent_post->post_slug;
						?>	
                      <figure class="d-flex recentblog">
                            <a href="<?php echo $post_url;?>"><img src="<?php echo $img_url;?>"></a>
                            <a href="<?php echo $post_url;?>" class="h6"><?php echo $recent_post->post_title;?><span class="fgray"><?php echo date("D d, Y",strtotime($recent_post->created_on));?></span></a>
                      </figure>
					  <?php } ?>


                      

                  </div>
                  <!--/ recent posts -->
             
              

              



              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>

    <!--/ sub page main -->
