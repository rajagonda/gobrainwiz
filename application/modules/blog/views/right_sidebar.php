<div class="RecentPostsLists">	
	<h2><span>Recent Posts</span></h2>
	<ul>
		<?php foreach($recent_posts as $recent_post){ ?>
				<li><a href="<?php echo base_url();?>blog/post_details/<?php echo $recent_post->post_id;?>"><?php echo $recent_post->post_title;?></a></li>
		<?php } ?>
	</ul>
</div>
				
<div class="Subscription">
	<h3>Subscribe to Our Newsletter</h3>
	<span id="successmsg1" class="text-center" style="display:block; color:green"></span>
	<span id="failuremsg1" class="text-center" style="display:none; color:red"></span>

	<form action="#" method="post" name="subscriber_form" id="subscriber_form" onsubmit="return validateSubscriber()">
		<div class="form-group">
			<input type="email" id="email" name="email" class="form-control" placeholder="Enter Your Email" >
		 </div>
		 <button class="btn btn-default" type="submit">Subscribe Now</button>
	</form>
</div>