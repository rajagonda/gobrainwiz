
    <!--sub page main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="page-header">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                       <h1>Blog</h1>
                   </div>
               </div>
               <!--/ row --> 
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

         <!-- brudcrumb -->
         <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                   
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active"><a>Blog</a></li>
                    </ul>                    
                </div>
                <!--/col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ brudcrumb -->

        <!-- sub page body -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container">
              <!-- row -->
              <div class="row">
              <!-- col -->
			  <?php foreach($posts as $post){
				  $img_url = base_url().'upload/post_images/thumbnail/'.$post->post_thumb_image;
				  $post_url = base_url('blog').'/post_details/'.$post->post_slug;
				  
				  ?>
              <div class="col-lg-4">
			  
                    <div class="blog-col text-center">
                        <a href="<?php echo $post_url;?>">
							<img src="<?php echo $img_url;?>" alt="" class="img-fluid">
						</a>
                        <article>
                            <h2 class="h5 pt-2">
                                <a href="<?php echo $post_url;?>"><?php echo $post->post_title;?></a>
                            </h2>
                            <ul class="list-seperator nav justify-content-center">
                                
                                <li><a>Posted on <?php echo date("D d, Y",strtotime($post->created_on));?></a> </li>
                            </ul>
                            <p class="text-center"><?php echo substr(trim($post->post_description),0,100);?>...</p>
                        </article>
                    </div>
              </div>
			  <?php } ?>
			 <!--/ col -->

               
              </div>
              <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
