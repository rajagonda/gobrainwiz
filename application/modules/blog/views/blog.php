
<section class="BlogListWrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="BlogBanner">
          <h1>Our Blog</h1>
          <div class="breadcrumb">
            <ul>
              <li><a href="#" title="Home">Home</a></li>
                <li>/</li>
                <li>Blog</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="BlogItems">
          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfnrkUTyYuWU0y2zYC_eaej2SdMtbxd2Y5u2Ml2Bmst0w3yq5t" class="img-responsive">
                <div class="BlogDate">
                  <b>16<sup>th</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="assets/images/blog-img-2.jpg" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfnrkUTyYuWU0y2zYC_eaej2SdMtbxd2Y5u2Ml2Bmst0w3yq5t" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <!-- <div class="BlogDate var1">
                  <b>Sep 16, 2018</span></b>
                </div> -->
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="assets/images/blog-img-2.jpg" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfnrkUTyYuWU0y2zYC_eaej2SdMtbxd2Y5u2Ml2Bmst0w3yq5t" class="img-responsive">
                <div class="BlogDate">
                  <b>16<sup>th</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="assets/images/blog-img-2.jpg" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfnrkUTyYuWU0y2zYC_eaej2SdMtbxd2Y5u2Ml2Bmst0w3yq5t" class="img-responsive">
                <div class="BlogDate">
                  <b>16<sup>th</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="assets/images/blog-img-2.jpg" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfnrkUTyYuWU0y2zYC_eaej2SdMtbxd2Y5u2Ml2Bmst0w3yq5t" class="img-responsive">
                <div class="BlogDate">
                  <b>16<sup>th</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

          <div class="BlogItem">
            <div class="row">
              <figure class="col-md-4 col-sm-4 col-xs-12">
                <img src="assets/images/blog-img-2.jpg" class="img-responsive">
                <div class="BlogDate">
                  <b>02<sup>nd</sup><span>Sep 2018</span></b>
                </div>
              </figure>
              <figcaption class="col-md-8 col-sm-8 col-xs-12">
                <h3>Cosy Bright Office In Yellow And Grey Colors</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged....</p>
                <a href="#" class="BlogReadMore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
              </figcaption>
            </div>
          </div>

        </div>
        <div class="TestHistoryPagination">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item disabled">
              <a class="page-link" href="#" aria-label="Previous"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
              </li>
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
              
              <li class="page-item">
              <a class="page-link" href="#" aria-label="Next"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </li>
            </ul>
          </nav>
        </div>
      </div>

      <div class="col-md-3 col-sm-12 col-xs-12 BlogRhtWrap">
        <div class="RecentPostsLists">
          <h2><span>Recent Posts</span></h2>
          <ul>
            <li><a href="#">Losy Bright Office In Yellow And Grey Colors</a></li>
            <li><a href="#">Smart Website Personalization by Unless.com | Unless</a></li>
            <li><a href="#">Losy Bright Office In Yellow And Grey Colors</a></li>
            <li><a href="#">Smart Website Personalization by Unless.com | Unless</a></li>
            <li><a href="#">Losy Bright Office In Yellow And Grey Colors</a></li>
            <li><a href="#">Smart Website Personalization by Unless.com | Unless</a></li>
          </ul>
        </div>
        <div class="Subscription">
          <h3>Subscribe to Our Newsletter</h3>
          <form method="Post">
            <div class="form-group">
              <input type="emaail" class="form-control" placeholder="Enter Your Email">
            </div>
            <button class="btn btn-default" type="button">Subscribe Now</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>