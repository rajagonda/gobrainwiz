<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : pages_model
 * Mail id : phpguidance@gmail.com
  */
class blog_model extends CI_Model {	

	public function postcount()
	{
		$sql = "SELECT * FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1'"; 
	    $result = $this->db->query($sql);
           
		if($result->num_rows() > 0 ) 
		{
            return $result->num_rows();
        } 
		else 
		{
           return '';
        }
	}
	
	public function getPosts($limit, $start)
	{     
        $sql = "SELECT gbp.post_id,gbp.cat_id,gbp.meta_title,gbp.meta_keywords,gbp.meta_desc,gbp.post_title,gbp.post_slug,gbp.post_image,gbp.post_thumb_image,gbp.post_description,gbp.created_on FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1' LIMIT $start, $limit";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) 
		{
            return $query->result();
        } else 
		{
            return '';
        }
    }
	
	public function blog_posts()
	{
		$sql = "SELECT gbp.post_id,gbp.cat_id,gbp.post_title,gbp.post_slug,gbp.created_on,gbp.post_image,gbp.post_description,gbp.post_thumb_image FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1'  ORDER BY gbp.post_id DESC";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) 
		{
            return $query->result();
        } 
		else 
		{
            return '';
        }
	}
	
	public function recent_posts()
	{
		$sql = "SELECT gbp.post_id,gbp.cat_id,gbp.post_title,gbp.post_slug,gbp.created_on,gbp.post_image,gbp.post_thumb_image FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1'   ORDER BY gbp.post_id DESC LIMIT 0,5";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) 
		{
            return $query->result();
        } 
		else 
		{
            return '';
        }
	}
	
	public function post_details($post_id)
	{
		$sql = "SELECT gbp.cat_id,gbp.meta_title,gbp.meta_keywords,gbp.meta_desc,gbp.post_title,gbp.post_image,gbp.post_description,gbp.created_on FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1' AND gbp.post_id = '".$post_id."'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) 
		{
            return $query->result();
        } else 
		{
            return '';
        } 
	}
	
	public function post_views_count($post_id)
	{
		$sql = "select * from gk_post_views where post_id = '".$post_id."' ";
		$result = $this->db->query($sql);
           
		if($result->num_rows() > 0 ) 
		{
            return $result->num_rows();
        } 
		else 
		{
           return  0;
        }
	}
	
	public function update_post_view_counter($ip,$post_id)
	{
		$sql = "SELECT viewed_on from gk_post_views where ip_addr = '".$ip."' AND post_id = '".$post_id."' ORDER BY  id DESC limit 0,1  ";
		$query = $this->db->query($sql);
		
		if($query->num_rows()>0)
		{
			$result = $query->result();
			$current_time = new DateTime();
			$post_viewed_time = new DateTime($result[0]->viewed_on);
			
			$interval = $current_time->diff($post_viewed_time);	
			$elapsed = $interval->format('%h');
			
			if($elapsed > 1)
			{
				$this->insert_post_view_counter($ip,$post_id);
			}
		}
		else
		{
			$this->insert_post_view_counter($ip,$post_id);
		}
	}
	
    public function insert_post_view_counter($ip,$post_id)
    {
        $data = array('ip_addr' =>$ip,'post_id' => $post_id); 
        $this->db->insert(DB_PREFIX.'post_views',$data);
    }
	
    public function check_subscriber($email)
    {
        $sql = "select * from gk_nl_subscribers where subscr_email = '".$email."' AND status = 1";	
        $result = $this->db->query($sql);

        if($result->num_rows() > 0 ) 
        {
            return $result->num_rows();
        } 
        else 
        {
            return 0;
        }
    }
	
    public function insert_subscriber($data)
    {
        $this->db->insert(DB_PREFIX.'nl_subscribers',$data);
        return $this->db->insert_id();
    }
    
    
    public function get_post_id_by_slug($post_slug)
    {
        $sql = "SELECT gbp.post_id FROM gk_blog_posts gbp JOIN gk_blog_categories gbc ON gbp.cat_id = gbc.cat_id WHERE gbp.status='1' AND gbc.status = '1' AND gbp.post_slug = '".$post_slug."'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) 
        {
            $row = $query->result();
            return $row[0]->post_id;
        } 
        else 
	{
            return '';
        } 
		
    }
    

        
        
         

}