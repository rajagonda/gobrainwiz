<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : pages_model
 * Mail id : phpguidance@gmail.com
  */
class pages_model extends CI_Model {	

	 public function getTesmonials(){     
        
        $sql = "SELECT  * FROM gi_testmonials WHERE testmonial_status='y'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
           
    }
    public function get(){

       $sql = "SELECT  * FROM gk_mobile_banners 
              WHERE banner_type ='w' AND banner_status='y' 
              ORDER BY banner_position DESC";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        } 
    }
    public function getToppers(){
        $this->db->order_by('voice_status','y');
        $query = $this->db->get('gk_voice_toper');
        if($query->num_rows()>0){
          return $query->result();
        }else {
            return '';
        }
    }
     public function getWeekly(){
        $this->db->order_by('status','y');
        $query = $this->db->get('gk_weekly');
        if($query->num_rows()>0){
          return $query->row();
        }else {
            return false;
        }
    }
    public function getupdates(){
        $this->db->order_by('update_status','y');
        $query = $this->db->get('gk_latestupdates');
    if($query->num_rows()>0){
      return $query->result();
    }else {
      return '';
    }
        }

        
        
         public function getBatchs(){
             
             $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id ORDER BY batch_id DESC";
             
              $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }
        }

}