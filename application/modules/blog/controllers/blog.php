<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Controller : pages
 * Mail id : phpguidance@gmail.com
  */
class blog extends MY_Controller {
      
	public function __construct() 
	{
	   parent::__construct();
	   $this->load->library('template'); 
	   $this->load->library('parser');
	   $this->load->library('pagination');
	   $this->load->model(array('common_model','blog_model'));
	}
    
	public function index()
	{
		
		/*$config['base_url'] = base_url().'/blog/index'; 
		$config["total_rows"] = $this->blog_model->postcount();
		$config["per_page"] = 2;
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="TestHistoryPagination">
					<nav aria-label="Page navigation example"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['first_link'] = true;
		$config['last_link'] = true;
                $config['last_link'] = 'last';
                $config['first_link'] = 'first';
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-angle-double-left" aria-hidden="true"></i>';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] =  '<li class="active" ><a  class="page-link"  href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';		
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["posts"] = $this->blog_model->getPosts($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		*/
		
		$data['posts']  = $this->blog_model->blog_posts();
		
		$this->template->theme('home',$data);
    }
	
       
     
	public function post_details()
	{
		$post_id = $this->blog_model->get_post_id_by_slug($this->uri->segment(3,0));
				
		$ip = $this->input->ip_address();
		$this->blog_model->update_post_view_counter($ip,$post_id);
		$data['views'] = $this->blog_model->post_views_count($post_id);
		$data['post_details']  = $this->blog_model->post_details($post_id); 
		$data['recent_posts']  = $this->blog_model->recent_posts(); 
		
		$this->template->theme('post_details',$data);
		
	}

	public function subscribers(){
		
		if($_POST)
		{
            $data['subscr_email'] = $this->input->post('email'); 
            if($this->blog_model->check_subscriber($this->input->post('email'))>0)
			{
				$data['message'] =  "Email already subscribed.";
				$data['id'] =  "0"; 
			}
			else
			{
				$this->blog_model->insert_subscriber($data);
				$data['message'] =  "Thanks for subscribing to us.";
				$data['id'] =  "1"; 
			}
			header('Content-Type: application/json');
            echo  json_encode($data);
            exit;

		}			
	}	
}
    


/* End of file skeleton.php */
/* Location: ./application/modules/skeleton/controllers/skeleton.php */