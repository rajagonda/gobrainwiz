<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : download
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class download extends MY_Controller {

     public function __construct() {
        parent::__construct();
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('download_Model');
        $this->load->language('download');
        $this->load->helper('download');
        $this->load->library('pagination');
     }
     public function index()	{
       
     if(!$this->session->userdata('user_login') == '1'){
           redirect(base_url()."users/login");
      }


         $data['categories'] = $this->download_Model->getallcat1();
        //echo "<pre>"; print_r($data);exit;
        if($data['categories'] !='') {
        foreach($data['categories'] as $category) {
        $data['subcatciunt'][$category->id] = $this->download_Model->getsubcatcount($category->id);
        }
        }
        $this->template->theme('placementpapers',$data);
    }
	 public function myaccount()	{
       
      if(!$this->session->userdata('user_login') == '1'){
      redirect(base_url()."users/login");
      }
        $this->template->theme('myaccount');
    }
	public function editprofile()	{
       
      if(!$this->session->userdata('user_login') == '1'){
       redirect(base_url()."users/login");
       }
       //$this->template->theme('editprofile');
		$this->load->view('editprofile', array('page' => 'editprofile'));
    }
    public function companypapers() {
            $id=$this->uri->segment(3,0);
            $sid="";
            $data['subcategeries']  = $this->download_Model->getsubcategeries($id);
            if($sid != NULL) {
              $data['subcatid'] = $sid;
              $data['questions'] = $this->download_Model->getallquetions($sid);
            }else {
            foreach($data['subcategeries'] as $csubat) {
               $data['subcatid'] = $csubat->id;
               $data['questions'] = $this->download_Model->getallquetions($csubat->id);
               break;
            }
            }
            foreach($data['subcategeries'] as $abc) {
              $data['questioncount'][$abc->id] = $this->download_Model->getqestioncatcount($abc->id);
            }

            if($data['questions'] !=''){
            foreach($data['questions'] as $val) {
              $data['questionanswer'][$val->id] = $this->download_Model->getquetionanswer($val->question_answer,$val->id);
            }
            }

            $data['companyname'] = $this->download_Model->getcategory($id);
            $data['subcatname'] = $this->download_Model->getcategory($data['subcatid']);
            $data['catid'] = $id;
            $sql = "SELECT  * FROM gk_companyqas WHERE sub_id='{$data['subcatid']}' AND question_status='y'";
            $query = $this->db->query($sql);
            $totalCount=$query->num_rows();
            $data['totalCount']=$totalCount;
            $data['page_no']=(int)$this->uri->segment(4,0);
            $data['pagination']=$this->customPagination($totalCount,10,$this->uri->segment(4,0),site_url("placement/companypapers/$id"));
            $this->template->theme('companytestview',$data);
        }

        public function customPagination($total, $per_page = 20,$page = 1, $url){
        // $query = "SELECT COUNT(*) as `num` FROM {$query}";
        // $row = mysql_fetch_array(mysql_query($query));
        // $total = $row['num'];
        $adjacents = "2";

        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;

        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;

        $pagination = "";
        if($lastpage > 1)
        {
            $pagination .= "<ul class='pagination'>";
                    $pagination .= "<li class='details' style='position:relative;top:8px;left:10px'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='{$url}/$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}/$lastpage'>$lastpage</a></li>";
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<li><a href='{$url}/1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}/2'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='{$url}/$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}/$lastpage'>$lastpage</a></li>";
                }
                else
                {
                    $pagination.= "<li><a href='{$url}/1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}/2'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}/$counter'>$counter</a></li>";
                    }
                }
            }

            if ($page < $counter - 1){
                $pagination.= "<li><a href='{$url}/$next'>Next</a></li>";
                $pagination.= "<li><a href='{$url}/$lastpage'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='currentd'>Next</a></li>";
                $pagination.= "<li><a class='currentd'>Last</a></li>";
            }
            $pagination.= "</ul>\n";
        }


        return $pagination;
    }

    public function downloadpapers(){
        $data['categories'] = $this->download_Model->getallcat();
        //$data['categories1'] = $this->download_Model->getallcategories();
        //echo "<pre>"; print_r($data);exit;
        if($data['categories'] !=''){
        foreach($data['categories'] as $category) {
        $data['cn'][$category->cat_id] = $this->download_Model->getcatpdfcount($category->cat_id);
        }
        }
        //echo "<pre>"; print_r($data);exit;
        $this->template->theme('downloadpapers',$data);
    }
    public function companypapersdownload($id) {


        $data['companydetails'] = $this->download_Model->getcategory($id);

            $data['subcategeries']  = $this->download_Model->getsubcategeries($id);

            foreach($data['subcategeries'] as $abc) {
              $data['pdffiles'][$abc->id] = $this->download_Model->getpdffiles($abc->id);
            }
            $data['catid'] = $id;
            $data['companyname'] = $this->download_Model->getcategory($id);


             $data['totalfiles'] = count(array_filter($data['pdffiles']));
            //echo "<pre>"; print_r($data); exit;

            $this->template->theme('papersdownload',$data);


        }
		
        public function downloadpdf($id) {
            $result  = $this->download_Model->getpdffile($id);

        if($result !='') {
          $file_name =  $result->path;
         $path = "upload/company_pdfs/".$result->path;
         $data = file_get_contents($path); // Read the file's contents
	 $name = $file_name;
 	 force_download($name, $data);
        }
        }

//This is the ajax function.
        public function get_pdf_iframe()
        {
          $id=$this->input->post('rec_id');
          $result  = $this->download_Model->getpdffile($id);
          if($result !='') {
            $pdf=base_url("/upload/company_pdfs/".$result->path);
          //echo "<iframe src='$pdf'></iframe>";
          echo "<iframe src='http://docs.google.com/viewer?url=$pdf&embedded=true' width='100%' height='600' style='border: none;'></iframe>";
        }
        else{
          echo "Sorry No PDF Found !";
        }
        }


}
