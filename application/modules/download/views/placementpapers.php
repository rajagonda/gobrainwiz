
<style>
* {box-sizing: border-box}

/* Style the tab */
div.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 30%;
    height: 300px;
}

/* Style the buttons inside the tab */
div.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}

.box {
    border: 3px solid #2c3761;
    background: #3f6a94;
    border-radius: 6px transition-property: background, border-radius;
    transition-duration: 1s;
    transition-timing-function: linear;
       width: 199px;
    height: 90px;
	color: white;
	margin-top: 14px;
  }
  .box:hover {
  }
#dashboard li{
	    background-color: rgba(169, 169, 169, 0.48);
    margin-bottom: 2px;
	width:100%;
}
#dashboard li a:hover{
	background-color:#2c3761!important;
    margin-bottom: 2px;
	color:white;
}
#dashboard li a{
	color:#003366;
	font-size: 16px;
}
.num{
	padding: 2px 1px 0px 5px;
    margin-right: 22px;
    float: right;
    color: #FF9800;
    width: 20px;
    height: 20px;
    margin-top: -3px;
}
#h6{
	text-align:center;
	font-size:16px;
	line-height: 88px;
}
</style>	

<section style="margin: 5px 0;">
 <div class="container-fluid">
      <div class="row" id="placement-papers">
        <div class="col-sm-3 col-md-3">
          <ul class="nav nav-sidebar" id="dashboard">
		  <?php
                              if($categories !='') {
                                 $i=1;
                              foreach($categories as $value) {

                            ?>
            <li class="active">
			<a href="<?php echo base_url();?>download/companypapersdownload/<?php echo $value->id;?>">
			<?php echo $value->com_name;?><span class="sr-only"></span></a>
			</li>
            <?php $i++; } } ?>
          </ul>
        </div>
        <div class="col-sm-9 col-md-9" id="col-9">

          <h1 class="page-header" style="color: #003366;">Companies</h1>

          <div class="row placeholders" id="placeholders">
		            <?php
                    if($categories !='') {
                    $i=1;
                    foreach($categories as $value) {
                    ?>
              <div class="col-xs-6 col-sm-3 placeholder">
              <a href="<?php echo base_url();?>download/companypapersdownload/<?php echo $value->id;?>">
			  <div class="box" id="box">
			  <h6 id="h6"><?php echo $value->com_name;?><span class="num"> <?php 
                                            echo $subcatciunt[$value->id]->count;
                                            ?></span></h6>
			  </div>
              </a>
			</div>
			<?php $i++; } } ?>
          </div>
        </div>
      </div>
    </div>
</section>
