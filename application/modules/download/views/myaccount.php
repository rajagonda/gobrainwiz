	
<style>
#heading{
       font-size: 23px;
    margin-top: 32px;
    color: crimson;
    text-align: center;
}
#h4{
    font-size: 20px;
    color: #006b30;
    font-weight: 600;
}
#strong{
color:#f10803;
}
#btnShow1{
    font-size: 16px;
    color: #006b30;
    font-weight: 600;
    background-color: palegoldenrod;
	box-shadow: 3px -2px 10px firebrick;
}
#container{
 background-color: rgba(169, 169, 169, 0.38);
}
#profile-div{
	margin-left:76px;
}
.edit-btn .btn{
    font-weight: 700;
    font-size: 18px;
    padding: 12px;
    color: white;
    background-color: #1b1b42;
    width: 242px;
}
.log-btn .btn{
    font-weight: 700;
    font-size: 18px;
    padding: 12px;
    color: white;
    background-color: #1b1b42;
    width: 242px;	
}
.edit-btn{
	    margin-top: 142px;
}
.log-btn{
	    margin-top: 11px;
}
#profile-wrapper{
	// background-color:rgba(0, 0, 0, 0.41);
    border: 7px solid #8ed693;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
}
.img-rounded{
	border:2px solid #8BC34A;;
}
</style>
<section >
<div class="container text-center" id="profile-wrapper">
               <div class="row">
				<div class="col-md-4" id="image-wrap">
				<img src="<?php echo assets_url();?>images/img/profile.jpg" alt="" style=" margin-bottom: 10px;" class="img-rounded img-responsive" /></div>
						<div class="col-md-4" style="margin-top: 151px;" id="text-wrap">
						    <h4 style="font-size: 30px;color: #002147;margin-bottom: 5px;font-weight: 500;">Bhaumik Patel</h4>
                            <p>
							<i class="fa fa-phone"></i> +91 0123456789</a>
							<br />
                            <i class="fa fa-envelope"></i> email@example.com
                            <br />
							<i class="fa fa-graduation-cap"></i>Government College of Engineering
                            <br />
                            </div>
				<div class="col-md-4" id="btn-wrap">
				<div class="edit-btn">
				<a href="<?php echo base_url();?>download/editprofile" class="btn" id="edit">Edit Profile</a></br>
				</div>
				<div class="log-btn">
				<a href="<?php echo base_url(); ?>users/logout" class="btn" id="log">Logout</a></div>
				</div>
				
				</div>     
</div>	
</section>