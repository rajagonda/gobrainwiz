

<link href="<?php echo assets_url();?>new/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/assets/owl-carousel/css/owl.theme.css" rel="stylesheet">
<link href="<?php echo assets_url();?>new/css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo assets_url();?>new/images/favicon.png">
		<link href="<?php echo assets_url();?>new/css/hover.css" rel="stylesheet" media="all">
	
<header class="home">
  <div class="top-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 col-md-5 col-xs-5" id="logo"> 
		<a href="<?php echo base_url();?>pages/home" title=""><img src="<?php echo assets_url();?>images/img/logo-primary.png" class="img-responsive" 
		alt=""/></a>
		</div>
        <div class="col-sm-7 col-md-7 col-xs-7">
          <ul class="contact-info pull-right">
            <!--li><i class="fa fa-phone"></i>
				<p><span>+91-81421 23938</span><br>
                <span class="contact-sub">Feel Free to Call Us</span></p>
            </li>
            <li><i class="fa fa-envelope"></i>
				<p><a href="mailto:gobrainwiz@gmail.com">gobrainwiz@gmail.com</a><br>
                <span class="contact-sub">Write us a mail anytime!</span><br>
            </li-->         
          </ul>
        </div>
      </div>
    </div>
    <!-- Navigation -->

  </div>  
</header>

<!-- Header End -->
<style>

  .header1 {
    background-color: #3e9de2;
    color: white;
    font-size: 12px;
    padding: 1rem;
    text-align: right;
    
}
.header2 {
    background-color: #167f92;
    color: white;
    font-size: 1.5em;
    padding: 1rem;
    text-align: center;
    text-transform: uppercase;
	
	
}
}
</style>
     <!-- Price Table Area 1 Start Here -->
        <div class="price-table1-area">
		<div class="container">
		<div class="header1" style="padding: 1rem; text-align: right;">
		<a href="" style="color: white;">My Account </a>| <a href="" style="color: white;"> Uttam </a></div>
		</div>
		
		<hr>
            <div class="container">
			
                <div class="row">
                    <div class=" col-md-12 ">
                        <div class="container">
    <h1>Edit Profile</h1>
  	<hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
          <h6>Upload a different photo...</h6>
          
          <input type="file" class="form-control">
        </div>
      </div>
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        <h3>Personal info</h3>
        
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-lg-3 control-label">Full Name:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="" placeholder="Enter Full Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Mobile No:</label>			
            <div class="col-lg-8">
              <input class="form-control" type="text" value="" name="txtContact" required="" placeholder="Phone Number (format: xxxx-xxx-xxxx)*" title="Please enter a Valid mobile number" pattern=".*[0-9]{10}.*" maxlength="10">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" type="text" value="" placeholder="Enter a Valid Email id">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">User Name:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="" placeholder="Enter User Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" placeholder="Enter Password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Confirm Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" value="" placeholder="Enter Confirm Password">
            </div>
          </div>
		  <div class="form-group">
            <label class="col-md-3 control-label">College Name:</label>
            <div class="col-md-8">
              <input class="form-control" type="text" value="" placeholder="Enter College Name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="button" class="btn btn-primary" value="Save Changes">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<hr>
                    </div>      
                
                </div>
            </div>
        </div>