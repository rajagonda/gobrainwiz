<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : Bulk smsgateway 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : users_model
 * Mail id : phpguidance@gmail.com
  */
class users_model extends CI_Model {	

	 
    public function checkmail($mailid) {
        $this->db->where('member_email', $mailid);
        $query = $this->db->get('gk_members');       
       // echo $this->db->last_query(); exit; 
  if ($query->num_rows() > 0) {      
      return $query->row();
         }
         else {
            return '';
   }
    }
    public function checkphonenumber($mobile) {
        $this->db->where('member_phone', $mobile);
        $query = $this->db->get('gk_members');        
  if ($query->num_rows() > 0) {      
      return $query->row();
         }
         else {
            return '';
   }
    }

}