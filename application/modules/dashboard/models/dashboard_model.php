<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Author  : Challagulla venkata sudhakar

 * Company : Renegade It Solutions

 * Version v1.0

 * Model : dashboard_model

 * Mail id : phpguidance@gmail.com

  */

class dashboard_model extends CI_Model {	



	 

    public function getUserDetails($userId) {

        $this->db->where('examuser_id', $userId);
        $query = $this->db->get('gk_examuserslist');       
        if ($query->num_rows() > 0) {      
            return $query->row();
        }else {
            return false;
        }

    }

    public function checkphonenumber($mobile) {

        $this->db->where('member_phone', $mobile);

        $query = $this->db->get('gk_members');        

  if ($query->num_rows() > 0) {      

      return $query->row();

         }

         else {

            return '';

   }

    }



}