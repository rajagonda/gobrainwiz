<section class="DbWrapper">
  <div class="container">
    <div class="DbWrap">
      <div class="row">
        <div class="col-md-4 DbLftCol">
          <div class="DbLft">
            <div class="UserProfilePic">
                           <?php if($userDetails->profile_pic !='') { ?>
                <img src="<?php echo base_url();?>upload/profilepics/<?php echo $userDetails->profile_pic;?>" class="mg-responsive">
              <?php }else { ?>
              <img src="<?php echo base_url();?>upload/profilepics/user-pic.png" class="mg-responsive">
            <?php } ?>
            </div>
            <div class="UserInfo">
              <h3><?php echo $userDetails->examuser_name;?></h3>
              <p><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo $userDetails->examuser_email;?></p>
              <p><i class="fa fa-phone" aria-hidden="true"></i>+91-<?php echo $userDetails->examuser_mobile;?></p>
              <p><i class="fa fa-graduation-cap" aria-hidden="true"></i>Gradutaion</p>
            </div>
          </div>
        </div>
        <div class="col-md-8 DbRhtCol">
          <div class="DbItems">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="<?php echo base_url();?>dashboard/profile">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <p>Edit Profile</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="<?php echo base_url();?>dashboard/changePassword">
                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                    <p>Change Password</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="<?php echo base_url();?>onlinetest/dashboard">
                    <i class="fa fa-laptop" aria-hidden="true"></i>
                    <p>My Online Tests</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="<?php echo base_url();?>dashboard/practicetest">
                    <i class="fa fa-television" aria-hidden="true"></i>
                    <p>My Practice Tests</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="#">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <p>Invite Friends</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="#">
                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                    <p>Facebook Share</p>
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="DbItem">
                  <a href="#">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <p>Help</p>
                  </a>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>