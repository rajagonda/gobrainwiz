<section class="DbWrapper">
  <div class="container">
    <div class="DbWrap">
      <div class="DbProfileEdit section_related_form">
        <h3 class="text-center">Edit Profile</h3>
       <form action="<?php echo base_url(); ?>dashboard/updateProfile" method="post" onsubmit="return validation();" enctype="multipart/form-data">
        <?php if($this->session->userdata('pfailure') !='' || $this->session->userdata('psuccess') ){
                    if($this->session->userdata('psuccess')){ ?>
                    <span style="color:green;"><?php echo $this->session->userdata('psuccess'); ?></span>
                   <?php  $this->session->set_userdata('psuccess','');
                    }else { ?>
                      <span style="color:red"><?php echo $this->session->userdata('pfailure');?>
                    </span>
                    <?php $this->session->set_userdata('pfailure',''); }
                   } ?>
                    <div class="form-group DbProfilePhotofg">
            <label>Profile Photo :</label>
            <div class="DbProfilePhoto">
              <?php if($userDetails->profile_pic !='') { ?>
                <img src="<?php echo base_url();?>upload/profilepics/<?php echo $userDetails->profile_pic;?>" class="image">
              <?php }else { ?>
              <img src="<?php echo base_url();?>upload/profilepics/user-pic.png" class="image">
            <?php } ?>
              <label for="profile_image"><i class="fa fa-camera" aria-hidden="true"></i></label>
              <input type="file" id="profile_image" name="profile_image">
             </div>
          </div>
                    <div class="form-group">
            <label>Name</label>
            <div class="form_label_info">
            <i class="fa fa-user" aria-hidden="true"></i>
            <input class="form-control" type="text" value="<?php echo $userDetails->examuser_name;?>" 
            name="examuser_name" id="examuser_name">
             <span style="color:red;" id="uname"></span>
                      </div>
                    </div>

                    <div class="form-group">
            <label>Mobile Number</label>
            <div class="form_label_info">
              <i class="fa fa-mobile" aria-hidden="true"></i>
              <input class="form-control" type="text" value="<?php echo $userDetails->examuser_mobile;?>" 
            name="examuser_mobile" id="examuser_mobile" maxlength='10'>
             <span style="color:red;" id="mobile"></span>
                      </div>
                    </div> 

                    <div class="form-group">
            <label>Email</label>
            <div class="form_label_info">
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <input class="form-control" type="text" value="<?php echo $userDetails->examuser_email;?>" 
            name="examuser_email" id="examuser_email">
             <span style="color:red;" id="email"></span>
            </div>
                     </div>

                    <div class="form-group">
            <label>College Name</label>
            <div class="form_label_info">
              <i class="fa fa-graduation-cap" aria-hidden="true"></i>
              <input class="form-control" type="text" value="Graduation">
            </div>
                    </div>
          <div class="form_action_btns">
                      <button type="submit" id="save_btn" class="btn btn-default">Save</button>          
            <a href="<?php echo base_url();?>dashboard" class="DbBack pull-right"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back</a>
                    </div>
                </form>
        
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">

   function validation(){
     

     var uname = document.getElementById('examuser_name');
     var mobile = document.getElementById('examuser_mobile');
     var email = document.getElementById('examuser_email');
     var numbers = /^[0-9]+$/;
     var emailreg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

     if (uname.value=='') {          
          document.getElementById("uname").innerHTML = "Usr Name Mandatory";
          return false;
      }
       if (mobile.value=='') {          
          document.getElementById("mobile").innerHTML = "Mobile Mandatory";
          return false;
      }else if(!mobile.value.match(numbers)){
          document.getElementById("mobile").innerHTML = "Please Enter Valid Mobile Number ";
          return false;
      }
       if (email.value=='') {          
          document.getElementById("email").innerHTML = "Email Mandatory";
          return false;
      }else if(!email.value.match(emailreg)){
        document.getElementById("email").innerHTML = "Enter Valid Email ";
          return false;
      }



      if (newpass.value != confirm.value) {
        document.getElementById("confPswd").innerHTML = "New And Confirm Password Not Match";
          return false;
      }        


    return  true;
  }
 </script>
<!-- profile photo change sript -->

<script type="text/javascript">
$(function(){
  $('#profile_image').change( function(e) {
    var img = URL.createObjectURL(e.target.files[0]);
    $('.image').attr('src', img);
  });
});    
</script> 