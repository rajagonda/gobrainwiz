<section class="DbWrapper">
  <div class="container">
    <div class="DbWrap">
      <div class="DbProfileEdit section_related_form">
        <h3 class="text-center">Change Password</h3>

        <form action="<?php echo base_url(); ?>dashboard/submitPassword" method="post" onsubmit="return validation();">
                   <?php if($this->session->userdata('failure') !='' || $this->session->userdata('success') ){
                    if($this->session->userdata('success')){ ?>
                    <span style="color:green;"><?php echo $this->session->userdata('success'); ?></span>
                   <?php  $this->session->set_userdata('success','');
                    }else { ?>
                      <span style="color:red"><?php echo $this->session->userdata('failure');?>
                    </span>
                    <?php $this->session->set_userdata('failure',''); }
                   } ?>
            <div class="form-group">
            <label>Old Password</label>
            <div class="form_label_info">
            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
            <input class="form-control" type="password" placeholder="Enter Old Password" name="old_password" id="old_password">
            <span style="color:red;" id="oldPswd"></span>
                      </div>
                    </div>

                    <div class="form-group">
            <label>New Password</label>
            <div class="form_label_info">
              <i class="fa fa-unlock-alt" aria-hidden="true"></i>
              <input class="form-control" type="password" placeholder="Enter New Password" name="new_password" id="new_password">
              <span style="color:red;" id="newPswd"></span>
                      </div>
                    </div>

                    <div class="form-group">
             <label>Confirm New Password</label>
            <div class="form_label_info">
              <i class="fa fa-unlock-alt" aria-hidden="true"></i>
              <input class="form-control" type="password" placeholder="Confirm New Password" name="confirm_password" id="confirm_password">
              <span style="color:red;" id="confPswd"></span>
            </div>
                     </div>

                    
          <div class="form_action_btns">
                      <button type="submit" id="save_btn" class="btn btn-default">Save</button>
            <!-- <button type="submit" id="cancel_btn" class="btn btn-default">Cancel</button> -->
            <a href="<?php echo base_url();?>dashboard" class="DbBack pull-right"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back</a>
                    </div>
                </form>
        
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

   function validation(){
     

     var oldpass = document.getElementById('old_password');
     var newpass = document.getElementById('new_password');
     var confirm = document.getElementById('confirm_password');

     if (oldpass.value=='') {          
          document.getElementById("oldPswd").innerHTML = "Old Password Mandatory";
          return false;
      }
       if (newpass.value=='') {          
          document.getElementById("newPswd").innerHTML = "New Password Mandatory";
          return false;
      }
       if (confirm.value=='') {          
          document.getElementById("confPswd").innerHTML = "Confirm Password Mandatory";
          return false;
      }

      if (newpass.value != confirm.value) {
        document.getElementById("confPswd").innerHTML = "New And Confirm Password Not Match";
          return false;
      }        


    return  true;
  }
 </script>
