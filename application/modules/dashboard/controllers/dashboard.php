<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Author  : Challagulla venkata sudhakar

 * Company : Renegade It Solutions

 * Version v1.0

 * Controller : dashboard

 * Mail id : phpguidance@gmail.com

  */
class dashboard extends MY_Controller {
      

	public function __construct() {

	    parent::__construct();
           $this->load->library('template'); 
           $this->load->model(array('common_model','dashboard_model'));
           $this->load->helper('sendmail_helper');   
           $this->load->helper('phpass'); 
        }

    public function index(){

        if($this->checkstudentlogin())
        {
            $userId = $this->session->userdata('studentId');
            $data['userDetails'] = $this->dashboard_model->getUserDetails($userId);
            $this->template->theme('dashboard',$data);
        }else {
            redirect(base_url(),"onlinetest/login");
        }
    }
    public function changePassword() {

       $this->template->theme('changepassword');

    }

    public function profile(){
            $userId = $this->session->userdata('studentId');
            $data['userDetails'] = $this->dashboard_model->getUserDetails($userId);
         $this->template->theme('profile',$data);
    }
    public function updateProfile(){      
        $pid = 0;
         if($_FILES['profile_image']['name'] !='') {
           
           
            $filename = time()."_".$_FILES['profile_image']['name'];
            $config['upload_path'] = 'upload/profilepics/';
            $config['allowed_types'] = 'jpg|png|';
            $config['max_size'] = 1024 * 50;
            $config['file_name'] = $filename;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('profile_image')) {
                
                $status = 'error';
              echo "<pre>"; print_r($this->upload->display_errors());
                $pid=1;
                $this->session->set_userdata('pfailure',"Profile Pic Issue");
            }  else {
                 
                    $data = $this->upload->data();
                    $timg = $data['file_name'];
                    $userData['profile_pic'] = $timg; 
            }
            
           }
           if($pid == '0'){
           $userData['examuser_name'] = $this->input->post('examuser_name');
           $userData['examuser_mobile'] = $this->input->post('examuser_mobile');
           $userData['examuser_email'] = $this->input->post('examuser_email');
           $userId = $this->session->userdata('studentId');
           $userParams = array('examuser_id' => $userId);
           $res = $this->common_model->updateRow('gk_examuserslist', $userData, $userParams);
           $this->session->set_userdata('psuccess',"Successfully  Updated");
           }
           redirect(base_url().'dashboard/profile');


    }
    public function submitPassword(){
       // echo "<pre>"; print_r($_POST);exit;

        $oldPass = $this->input->post('old_password');
        $newPass = $this->input->post('new_password');

        $userId = $this->session->userdata('studentId');
        $userDetails = $this->dashboard_model->getUserDetails($userId);

       $oldPass = trim($oldPass);
            $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            if($hasher->CheckPassword( $oldPass, $userDetails->examuser_password)){

                        $hashed_password = $hasher->HashPassword($newPass);
                        $userData['examuser_password'] = $hashed_password;
                        $userParams = array('examuser_id' => $userId);
                        $res = $this->common_model->updateRow('gk_examuserslist', $userData, $userParams);
                        $this->session->set_userdata('success',"Successfully  Password updated");

            }else {
                $this->session->set_userdata('failure',"Old Password Wrong");
            }
            redirect(base_url()."dashboard/changePassword");

    }

       
    public function checkstudentlogin()
    {
    
    
          $uid =$this->session->userdata('studentId');
         
            if(!empty($uid))
            {
                return true;
            }
            else
            {
                redirect(base_url()."braintest");   
            }
    }
        

    }

    





/* End of file skeleton.php */

/* Location: ./application/modules/skeleton/controllers/skeleton.php */