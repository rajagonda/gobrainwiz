
    <!-- Begin page content -->
    <div class="container">
		<div class="container-fluid" id="kolcon">
			<div class="row">
		<!-- <div class="col-lg-9 col-sm-9 col-md-9 col-xs-16"> -->
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-16">

		  <h3 class="hdtxt"><strong id="companyName"> <?php echo $companyname->com_name;?> </strong> <span class="pull-right">Total (<?php echo $totalfiles;?>)</span></h3>
			<div class="listingnames">
				  <div class="listingnameslist_content">

					<div class="col-lg-12 col-sm-12 col-md-12 col-xs-16 rightdiv">
						<div class="innersection">
						<p></p>
							<div class="elsemRow">


   <?php
    if($subcategeries !='') {
        foreach($subcategeries as $value) {
 if($pdffiles[$value->id] !='')    {
    ?>


    <?php
    foreach($pdffiles[$value->id] as $a ) {
        ?>
    <div class="elsemR1">
 <span class="pdfname">
    <?php echo $value->com_name;?>
 </span>
    <span class="pdfinfo">

      <?php echo $a->title; ?>
    </span>
    <!-- <a onclick="return checklogin();"href="<?php echo base_url();?>placement/downloadpdf/<?php echo $a->sub_id; ?>"> -->
    <a href='javascript:void(0)' onclick="viewPDF('<?php echo $a->sub_id; ?>')">
    <span class="download">

    </span>
        </a>

    </div>
    <?php } ?>

 <?php } } } ?>
</div>
					</div>
				  </div>
			  </div>
			</div>
		 </div>
		<?php echo $this->load->view('brainwizz/rightpanel'); ?>
	  </div>

		</div>
    </div>


<div class='PdfOverlay'></div>
    <div class='PdfPitch'>
      <a href='javascript:void(0)' style='display:none' class='innerDownload'>Download</a>
      <a class='closeOverlay' href='javascript:void(0)'>X</a>
      <div id='downloadIframe'></div>
    </div>

    <style>

    .closeOverlay{
      background: #000;
      color: #FFF;
      float: right;
      padding: 4px;
      border-radius: 50%;
      left: 30px;
      position: relative;
      bottom: 25px;
    }

    .PdfOverlay{
      background: #000;
      opacity: 0.7;
      top: 0px;
      height: 100%;
      width: 100%;
      position: absolute;
      display: none;
      z-index: 77777;
    }

    .PdfPitch{
      width: 1100px;
      height: 700px;
      border: 15px solid #000;
      background: #FFF;
      z-index: 99999;
      display: none;
      border-radius: 20px;
      top: 7%;
      left: 7%;
      position: absolute;
      padding: 30px;
      padding-top: 10px;
      line-height: 16px;
    }
    </style>

    <script>
    jQuery('.closeOverlay').click(function(){
      jQuery('.PdfPitch,.PdfOverlay,.innerDownload').fadeToggle();
      jQuery('#downloadIframe').html("");
    })

    function viewPDF(pdf_id){
          jQuery('.PdfPitch,.PdfOverlay,.innerDownload').fadeToggle();
          jQuery('#downloadIframe').html("<div style='text-align:center;padding-top:300px'><img src='<?php echo base_url();?>b_assets/images/ajax-loader.gif'></div>");
          var pdfUrl="<?php echo site_url(); ?>/placement/get_pdf_iframe";
          var download_link="<?php echo base_url();?>placement/downloadpdf/"+pdf_id;
          jQuery.ajax({
            url:pdfUrl,
            type:'post',
            data:{'rec_id':pdf_id},
            async:true,
            success:function(response){
              jQuery('#downloadIframe').html(response);
              jQuery('.innerDownload').show().attr('href',download_link);
            },
            error:function(response){
              jQuery('#downloadIframe').html(response);
            }
          });
    }
    </script>
