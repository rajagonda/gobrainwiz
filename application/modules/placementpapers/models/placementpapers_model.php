<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : placement
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class placementpapers_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}
        public function getAllcompanies() {
                $sql = "SELECT  * FROM gk_placement_comp  WHERE com_status='y'";
                 $query = $this->db->query($sql);
                if($query->num_rows()>0) {
                   return $query->result();
                } else {
                   return false;
                }
        }
         public function getcompanyData($cid) {
                $sql = "SELECT  * FROM gk_placement_comp  WHERE comp_id='$cid'";
                 $query = $this->db->query($sql);
                if($query->num_rows()>0) {
                   return $query->row();
                } else {
                   return false;
                }
        }
    
   
     public function getallquetions($id,$limit, $start) {
        $sql = "SELECT  * FROM gk_companyqas_placement WHERE cat_id='$id' AND question_status='y' ORDER BY id ASC LIMIT $start, $limit"; 

        //echo $sql;exit;
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
        return $query->result();
        } else {
        return '';
        }
    }
         public function questionscount($cid){
        $sql = "SELECT  * FROM gk_companyqas_placement WHERE cat_id='$cid' ORDER BY id DESC";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
        return $query->num_rows();
        } else {
        return false;
    }
    }




        public function getallcat(){
                $sql = "SELECT * FROM gk_companies INNER JOIN gk_companypdf
                ON gk_companypdf.cat_id=gk_companies.id GROUP BY gk_companypdf.cat_id";
                $query = $this->db->query($sql);
                if($query->num_rows()>0) {
                return $query->result();
                } else {
                return '';
                }
        }
        public function getcatpdfcount($id){
        $sql = "SELECT  * FROM gk_companypdf WHERE cat_id='$id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->num_rows();
        } else {
            return '';
        }
        }

       // getpdfcatcount



         public function getsubcatcount($id){
         $sql = "SELECT  count(*) as count FROM gk_companies WHERE c_id='$id' ORDER BY id DESC";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }

     

      public function getqestioncatcount($id) {
          $sql = "SELECT  count(*) as count FROM gk_companyqas WHERE sub_id='$id' ORDER BY id DESC";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }

     public function getquetionanswer($id, $qid){
         if($id == '1') {
         $sql = "SELECT  option1 as abc FROM gk_companyqas WHERE id='$qid'";
         }else if($id=='2'){
             $sql = "SELECT  option2 as abc FROM gk_companyqas WHERE id='$qid'";
         }else if($id== '3') {
             $sql = "SELECT  option3 as abc FROM gk_companyqas WHERE id='$qid'";
         }else if($id == '4') {
             $sql = "SELECT  option4 as abc FROM gk_companyqas WHERE id='$qid'";
         }else  {
             $sql = "SELECT  option5 as abc FROM gk_companyqas WHERE id='$qid'";
         }

        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
       public function getsubcategeries($id) {
          $sql = "SELECT  * FROM gk_companies WHERE c_id='$id'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
      public function getpdffiles($id) {
        $sql = "SELECT  * FROM gk_companypdf WHERE sub_id='$id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
     }
      public function getcategory($id) {
          $sql = "SELECT  * FROM gk_companies WHERE id='$id'";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
       public function getpdffile($id) {
        $sql = "SELECT  * FROM gk_companypdf WHERE sub_id='$id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }

}

/* End of file voice_toper_model.php */
/* Location: ./application/models/voice_toper_model.php */
