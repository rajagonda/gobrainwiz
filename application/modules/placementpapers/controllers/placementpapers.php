<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : placementpapers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class placementpapers extends MY_Controller {

     public function __construct() {
        parent::__construct();
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('placementpapers_Model');
        $this->load->library('pagination');
     }
     public function companies($cid=null,$id=null)	{

        //echo $cid;exit;
          $data['companies'] = $this->placementpapers_Model->getAllcompanies();
          if($cid==''){
            if($data['companies'] !=''){
                $cid =   $data['companies'][0]->comp_id;
                $data['companyData'] = $this->placementpapers_Model->getcompanyData($cid);
            }else {
                $data['companyData'] = '';                
            }
          }else {
                $cid = $cid;
                $data['companyData'] = $this->placementpapers_Model->getcompanyData($cid);
          }
          if($cid !=''){
              $config['base_url'] = base_url().'/placementpapers/companies/'.$cid.'/'; 
              $config["total_rows"] = $this->placementpapers_Model->questionscount($cid);
              $config["per_page"] = 5;
              $config["uri_segment"] = 4;
              $config["cur_page"] = $this->uri->segment(4);       

              $config['full_tag_open'] = '<ul class="pagination">';
              $config['full_tag_close'] = '</ul>';
              $config['first_link'] = true;
              $config['last_link'] = true;
              $config['last_link'] = 'last';
              $config['first_link'] = 'first';
              $config['first_tag_open'] = '<li class="page-item">';
              $config['first_tag_close'] = '</li>';
              $config['prev_link'] = 'Previous';
              $config['prev_tag_open'] = '<li class="page-item">';
              $config['prev_tag_close'] = '</li>';
              $config['next_link'] = 'Next';
              $config['next_tag_open'] = '<li class="page-item"> ';
              $config['next_tag_close'] = '</li>';
              $config['last_tag_open'] = '<li class="page-item">';
              $config['last_tag_close'] = '</li>';
              $config['cur_tag_open'] =  '<li class="page-item active" ><a class="page-link" href="#">';
              $config['cur_tag_close'] = '</a></li>';
              $config['num_tag_open'] = '<li class="page-item">';
              $config['num_tag_close'] = '</li>';     
              $this->pagination->initialize($config);
              $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
              $data["quetions"] = $this->placementpapers_Model->getallquetions($cid,$config["per_page"], $page);
              $data["links"] = $this->pagination->create_links();
       // echo "<pre>"; print_r($data);exit;
          }

          $this->template->theme('placementpapers',$data);
    }
   



}
