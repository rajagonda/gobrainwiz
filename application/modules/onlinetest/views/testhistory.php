	
<main class="subpage">
	<div class="page-header">
		<!-- container -->
		<div class="container">
		   <!-- row -->
		   <div class="row">
			   <div class="col-lg-6">
				   <h1>Completed Test</h1>
			   </div>
		   </div>
		   <!--/ row --> 
		</div>
		<!--/ container -->
	</div>
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- col -->
			<div class="col-lg-12">                   
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;"><?php echo $this->session->userdata('examusername');?></a></li>
					<li class="breadcrumb-item active"><a>Completed Test</a></li>
				</ul>                    
			</div>
			<!--/col -->
		</div>
		<!--/ row -->
	</div>
	<div class="container">
		<div class="row">

			<!-- left col 4-->
			<div class="col-lg-4"> 
				<?php require_once('sidebar.php'); ?>
			</div>

			<div class="col-lg-8">
				<div class="inrright_cntr" style="box-shadow:none;">
					<div class="wrapdiv" style="overflow: auto;">
						<h1 class="h5 pl-2 pb-2">Completed Test</h1>
						<?php
							foreach ($tests as  $value) { 
								if($completetests[$value->brain_test_id] !='') { ?>
									<div class="col-sm-6">
										<span itemscope="" itemtype="">
											<div class="skilltest-box clearfix">
												<figure class="image">
													<a href="#">
													<?php if($value->test_image !=''){ ?>
														<img alt="image" border="0" src="<?php echo base_url();?>public/testimages/<?php echo $value->test_image;?>" width="270" height="270">
													<?php } else {?>
														<img alt="image" border="0" src="<?php echo assets_url();?>images/logooo.jpg" width="270" height="270">
													<?php } ?>
													</a>
												</figure>
												<div class="details clearfix">
													<h5><a href=""><?php echo $value->test_name; ?></a></h5>
													<a href="<?php echo base_url(); ?>onlinetest/scorecard/<?php echo $value->brain_test_id;?>" class="btn button1" id="view_score">View Score</a>
													<p>Questions : <?php echo count_q_t('q',$value->brain_test_id); ?><br>
													Duration : <?php echo count_q_t('t',$value->brain_test_id); ?> mins</p>
												</div>
											</div>
										</span>
									</div>
							<?php } ?>
						<?php } ?>                                                   
					</div>
			  </div>
			</div> 
		</div>
	</div>	
</main>    