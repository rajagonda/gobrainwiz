<div class="whitebox  left-usernav">
<?php if ($this->session->userdata('user_login') == '1') { ?>	
    <figure class="profile-pic position-relative">
            <form enctype="multipart/form-data" action="<?php echo base_url('')?>users/change_avatar" method="post" name="image_upload_form" id="image_upload_form">
		<div id="imgArea">
                    <?php 
                         $avatar_url =base_url().'upload/profilepics/thumbnail/'.$this->session->userdata('profile_pic');
                    if (@getimagesize($avatar_url)) {?>
                        <img src="<?php echo $avatar_url;?>" alt="profile image">
                    <?php }else{?>
                    <img src="<?php echo base_url()?>upload/profilepics/no_image.png" alt="profile image">
                    <?php }?>
                    <div class="upload-btn-wrapper" id="imgChange"><button class="whitebtn mx-auto"><span class="icon-edit icomoon"></span></button>
                          <input type="file" accept="image/*" name="image_upload_file" id="image_upload_file">
                    </div>
                </div>
            </form>
	</figure>
	<h2 class="h6 text-center pt-3 mb-0"><?php echo $this->session->userdata('examusername');?></h2>
	<p class="text-center"><small><?php echo $this->session->userdata('examuseremail');?></small></p>
	<?php 
}
		$active_url = $this->uri->segment(2);
    ?>
    
    <a href="javascript:void(0)" class="fblue justify-content-between  w-100 text-center p-4"  id="profile-mob-nav">
        <span>My Profile Navigation</span>
        <span class="icon-list-ul icomoon"></span>
    </a>
	
	<ul id="user-profile-navlist">
		<li>	
			<a <?php if($active_url == 'dashboard'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/dashboard">
			Upcoming test <?php echo $tcount - $completetestscount; ?>
			</a>
		</li>
		<li>
			<a <?php if($active_url == 'testhistory'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/testhistory">
			Completed Test <?php echo $completetestscount; ?>
			</a>
		</li>
		<?php if ($this->session->userdata('user_login') == '1') { ?>
		
		<li><a <?php if($active_url == 'withdrawrequests'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/withdrawrequests">Withdraw Requests</a></li>
		
		<li><a <?php if($active_url == 'userWallet'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>onlinetest/userWallet">Wallet</a></li>
		
		<li><a <?php if($active_url == 'manageProfile'){?> class="usernav-active"<?php }?> href="<?php echo base_url(); ?>users/manageProfile">Update Profile</a></li>
		
		<li><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
		<?php } ?>
	</ul>
</div>
<script src="<?php echo assets_url()?>js/jquery.form.js"></script>
<script>
$(document).on('change', '#image_upload_file', function () {
var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

$('#image_upload_form').ajaxForm({
    beforeSend: function() {
        progressBar.fadeIn();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    success: function(html, statusText, xhr, $form) {		
        obj = $.parseJSON(html);	
        if(obj.status){		
            $("#imgArea>img").prop('src',obj.image_medium);			
        }else{
            alert(obj.error);
        }
    }
		
}).submit();		

});
</script>

<script>
    $(document).ready(function(){
        $("#profile-mob-nav").click(function(){
            $("#user-profile-navlist").slideToggle();
        });
    });
</script> 


