

<main class="subpage">
	<div class="page-header">
		<!-- container -->
		<div class="container">
		   <!-- row -->
		   <div class="row">
			   <div class="col-lg-6">
				   <h1>Withdraw Requests</h1>
			   </div>
		   </div>
		   <!--/ row --> 
		</div>
		<!--/ container -->
	</div>
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- col -->
			<div class="col-lg-12">                   
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;"><?php echo $this->session->userdata('examusername');?></a></li>
					<li class="breadcrumb-item active"><a>Withdraw Requests</a></li>
				</ul>                    
			</div>
			<!--/col -->
		</div>
		<!--/ row -->
	</div>


    <div class="container">
        <div class="row">
            <!-- for Quick Link Section -->

            <!-- left col 4-->
			<div class="col-lg-4"> 
				<?php require_once('sidebar.php'); ?>
			</div>

            <div class="col-lg-8">
                <div class="inrright_cntr" style="box-shadow:none;">
                    <div class="wrapdiv" style="overflow: auto;">
                       

                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if ($this->session->userdata('request_message')) {
                                    ?>
                                    <div class="box" style="border-top: #fff;">
                                        <div class="box-header">
                                            <div class="nNote nSuccess hideit"
                                                 style="color: green;text-align: center;font-size: 18px;">
                                                <p style="margin:10px">
                                                    <strong>SUCCESS: </strong>
                                                    <?php
                                                    echo $this->session->userdata('request_message');
                                                    $this->session->set_userdata('request_message', "");
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title" style="width:100%">
                                            <a class="" title="Add Coupon Useage"
                                               href="<?php echo base_url(); ?>onlinetest/sendWithdrawRequest"
                                               style="float:right">
                                                <span class="glyphicon glyphicon-plus "
                                                      style="font-size:150%;color:#438EB9;"></span>
                                            </a>
                                        </h3>
                                        <br/>
                                        <br/>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th style="width: 10px"> Sno</th>
                                                    <th> Amount</th>
                                                    <th> Description</th>
                                                    <th> Paid Amount</th>
                                                    <th>Comments by Admin</th>
                                                    <th> Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if (count($records) > 0) {
                                                    foreach ($records as $index => $record) {
                                                        ?>
                                                        <tr>
                                                            <td> <?= $index + 1 ?></td>
                                                            <td title="<?= $record->wr_requested_amount ?>"> <?= $record->wr_requested_amount ?></td>
                                                            <td title="<?= $record->wr_requested_user_description ?>"> <?= $record->wr_requested_user_description ?></td>
                                                            <td title="<?= $record->wr_paid_amount ?>"> <?= $record->wr_paid_amount ?></td>
                                                            <td title="<?= $record->wr_admin_description ?>"> <?= $record->wr_admin_description ?></td>
                                                            <td title="<?= $record->wr_status ?>"> <?= $record->wr_status ?></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="6" style="text-align:center">No Records Found</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <?php echo $links; ?>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>    