<link href="<?php echo assets_url(); ?>new/css/easy.css" rel="stylesheet" type="text/css">
<style>
    * {
        box-sizing: border-box
    }

    body {
        font-family: "Lato", sans-serif;
    }

    /* Style the tab */
    div.tab {
        float: left;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
        width: 30%;
        height: 300px;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        display: block;
        background-color: inherit;
        color: black;
        padding: 22px 16px;
        width: 100%;
        border: none;
        outline: none;
        text-align: left;
        cursor: pointer;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current "tab button" class */
    div.tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        float: left;
        padding: 0px 12px;
        border: 1px solid #ccc;
        width: 70%;
        border-left: none;
        height: 300px;
    }

    .qklink_wpr .linkgate .accordion-item .accordion-header:after {
        background: #1b1b42;
        content: "";
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        -webkit-transform: scaleX(0);
        transform: scaleX(0);
        -webkit-transform-origin: 0 50%;
        transform-origin: 0 50%;
        -webkit-transition-property: transform;
        transition-property: transform;
        transition-duration: .4s;
        -webkit-transition-timing-function: ease-out;
        transition-timing-function: ease-out
    }

    .qklink_wpr .linkgate .accordion-item:hover .accordion-header:after {
        background: #00a861;
        width: 100%;
        -webkit-transform: scaleX(1);
        transform: scaleX(1)
    }
</style>
<style>
    .main_wrapper {
        width: 1104px;
        margin: 0 auto;
        clear: both
    }

    .main_wrapper {
        text-align: center;
    }

    .main_wrapper {
        position: relative
    }

    .lnk_ansaddwpr {
        display: inline-block;
        width: 100%;
    }

    .inr_title {
        background: #1b1b42;
        border-bottom: 3px solid #00a861;
        font-size: 20px;
        color: #fff;
        font-weight: 400;
        position: relative;
        padding-left: 50px;
        height: 43px;
        line-height: 43px
    }

    .linkgate .accordion a, .linkgate .accordion1 a {
        font-family: Arial, Helvetica, sans-serif;
        text-decoration: none;
        font-size: 15px;
        padding: 9px 0;
        margin: 0;
        display: block;
        cursor: pointer;
        background: #fafafa;
        border-bottom: 1px solid #a6a6a6;
        outline: none;
        display: inline-block;
        width: 74%;
        color: #151719;
        font-weight: 500;
    }

    .qklink_wpr .linkgate .accordion-item .accordion-header:hover a {
        color: #fff;
    }

    .inrright_cntr .gate h1, .book_package .package_head {
        background: #d6d4d4;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #202090;
        font-size: 26px;
        font-weight: 600;
        margin-left: 0;
        padding: 3px 10px;
        position: relative;
    }

    .qklink_wpr .linkgate .accordion-item .accordion-header:hover a {
        color: #fff
    }

    .linkgate .accordion a.abtacordian, .accordion-header h1 > a {
        border-bottom: none !important;
        background: none !important;
        display: inline-block !important;
        padding: 0 !important;
        font-family: open_sansregular;
        font-size: 14px
    }

    .accordion1 .accordion-header h1 {
        float: left;
        font-size: 15px;
        font-weight: 400;
        margin: 0;
        line-height: 1em;
        color: #00a861;
    }

    .inrright_cntr .gate p {
        font-size: 15px;
        line-height: 1.5em;
        padding: 3px 0 0 11px;
        text-align: justify;
        font-weight: 400;
    }

    .inrright_cntr .gate ul li {
        padding: 0px 0;
        font-size: 14px;
        margin: 0 0 0 5px;
        font-family: Arial, Helvetica, sans-serif;
        list-style: disc;
    }

    .navbar-default .navbar-nav > li > a {
        font-weight: 500;
    }

    #dropdown-btn {
        border-radius: 1px;
        margin-top: 7px;
        height: 41px;
        font-size: 14px;
        margin-left: 81px;
        color: white;
        background-color: #114775;
        width: 191px;
    }

    #dropdown-btn:hover {
        margin-left: 81px;
        border-radius: 1px;
        margin-top: 7px;
        height: 41px;
        font-size: 14px;
        color: #1b1b42;
        background-color: #20b4c7;
        width: 191px;
    }

    #dropdown-menu li a {
        border-bottom: 1px solid #ffffff !important;
        width: 190px !important;
        font-size: 14px !important;
        color: white !important;
        padding-left: 53px !important;
        background-color: #00a861 !important;
    }

    #dropdown-menu li a:hover {
        border-bottom: 1px solid #ffffff !important;
        width: 190px !important;
        font-size: 14px !important;
        color: #00a861 !important;
        padding-left: 53px !important;
        background-color: #e3e3e3 !important;
    }

    #dropdown-menu {
        margin-left: 82px;
        padding: 0;
        box-shadow: none;
        width: 190px;
        text-transform: uppercase;
        border-radius: 0;
    }

    .inrright_cntr .wrapdiv h1, .book_package .package_head {
        background-color: #1561a2;
        border: 1px dashed #fff;
        color: #fff;
        height: 37px;
        font-size: 23px;
        padding: 4px;
    }

    .wrapdiv {
        border: 1px solid #dedede;
        padding: 13px 21px 18px;
        position: relative;
        background: #fff;
    }

    .skilltest-box {
        background: rgba(158, 158, 158, 0.21) !important;
        box-shadow: 0 2px 6px rgba(158, 158, 158, 0.82);
        margin-bottom: 20px !important;
        padding: 15px 9px 15px !important;
        border-left: 2px solid #ff4307;
        height: 100px;
    }

    .skilltest-box .image {
        float: left;
        width: 70px;
    }

    a {
        color: #2f94d7;
        text-decoration: none;
        line-height: inherit;
    }

    .skilltest-box .image img {
        height: 70px;
        width: 70px;
    }

    .skilltest-box h5 {
        margin-bottom: 10px;
        height: 23px;
        overflow: hidden;
    }

    .skilltest-box .details {
        margin-left: 90px;
    }

    .skilltest-box h5 a {
        color: #e4400b;
        font-weight: 700;
        font-size: 15px;
    }

    .skilltest-box p {
        font-size: 12px;
        line-height: 22px;
        margin-bottom: 0;
        color: #717173;
    }

    .skilltest-box .button1 {
        float: right;
        margin-top: 7px;
    }

    .btn {
        display: inline-block;
        height: 32px;
        margin-bottom: 0;
        font-weight: normal;
        text-align: center;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 7px 12px;
        font-weight: 600;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 4px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .button1 {
        color: #fff;
        border-radius: 2px;
        display: inline-block;
        vertical-align: middle;
        -webkit-transform: perspective(1px) translateZ(0);
        transform: perspective(1px) translateZ(0);
        box-shadow: 0 0 1px transparent;
        position: relative;
        background: #1a1a56;
        -webkit-transition-property: color;
        transition-property: color;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        text-transform: uppercase;
        font-weight: 500;
    }

    .button1:before {
        content: "";
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: #00a861;
        outline: none;
        -webkit-transform: scale(0);
        transform: scale(0);
        -webkit-transition-property: transform;
        transition-property: transform;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-timing-function: ease-out;
        transition-timing-function: ease-out;
    }

    .button1:hover, .button1:focus, .button1:active {
        color: white;
    }

    .button1:hover:before, .button1:focus:before, .button1:active:before {
        -webkit-transform: scale(1);
        transform: scale(1);
    }

    @media (min-width: 375px) and (max-width: 667px) {
        .inrright_cntr {
            width: 100%;
            margin-right: 0px;
        }
    }

    @media (max-width: 667px) and (min-width: 375px) {
        .skilltest-box h5 {
            margin-bottom: 0px;
            height: 23px;
            overflow: hidden;
        }

        .skilltest-box p {
            font-size: 12px;
            line-height: 19px;
            margin-bottom: 0;
            color: #717173;
        }

        .skilltest-box .button1 {
            margin-right: 18px;
        }

        .col-sm-3 {
            width: 70%;
            margin-left: -18px;
        }

        .col-sm-1 {
            margin-top: -39%;
            margin-right: -12%;
        }
    }

    @media (min-width: 320px) and (max-width: 568px) {
        .inrright_cntr .col-sm-3 {
            width: 70%;
            margin-left: -18px;
        }

        .inrright_cntr .col-sm-1 {
            margin-top: -39%;
            margin-right: -13px;
        }

        .inrright_cntr .col-sm-1 .button1 {
            font-size: 12px;
        }

        .inrright_cntr .col-sm-6 {
            width: 126%;
            margin-left: -28px;
        }

        .skilltest-box p {
            line-height: 18px;
        }

        .dropdown #dropdown-btn {
            margin-left: 18%;
        }
    }

    @media (min-width: 384px) and (max-width: 640px) {
        .skilltest-box .button1 {
            margin-right: 27px !important;
        }
    }

    #count-span {
        float: right;
        padding: 2px 0px 0px 5px;
        color: #fff;
        background-color: #1561a2;
        width: 20px;
        height: 20px;
    }
</style>
<section>
    <div class="container">
        <div class="row">
            <!-- for Quick Link Section -->
            <?php require_once('sidebar.php'); ?>

            <div class="col-md-8">
                <div class="inrright_cntr" style="box-shadow:none;">
                    <div class="wrapdiv" style="overflow: auto;">
                        <h1 style="text-align:center;font-family: 'Raleway', sans-serif!important;">Send Withdraw
                            Request</h1>

                        <?php
                        if ($this->session->userdata('request_message')) {
                            ?>
                            <div class="box" style="border-top: #fff;">
                                <div class="box-header">
                                    <div class="nNote nerror hideit"
                                         style="color: green;text-align: center;font-size: 18px;">
                                        <p style="margin:10px">
                                            <strong>Error: </strong>
                                            <?php
                                            echo $this->session->userdata('request_message');
                                            $this->session->set_userdata('request_message', "");
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <form id="withdrawForm" action="" method="post">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Withdraw Amount</label>
                                    <input type="text" class="form-control" name="wr_requested_amount"
                                           placeholder="Withdraw Amount" required>
                                    <?php echo form_error('wr_requested_amount', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Description</label>
                                    <input type="text" class="form-control" name="wr_requested_user_description"
                                           id="wr_requested_user_description" placeholder="Description">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary" onClick="onSubmit();">Submit</button>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    