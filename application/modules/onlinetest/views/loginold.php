		<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>new/css/login.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<style>
b:hover{
    color: #1561a2!important;
}
b{
font-size: 15px;
color: #0e4d84;
}
.btn-group-lg>.btn, .btn-lg {
padding: 4px 16px;}
.btn-block {
	height: 36px;
}
    .error {
    color: red;
}
.errorClass { border:  1px solid red !important; }
.footer-wrapper {
    background: #222222;
    margin: 0;
    margin-top: -57px;
    padding: 42px 0 4px;
}
.login-button:hover{
	background-color: #105fa5!important;
    color: white;
    border: 1px solid #9dcef9!important;
}
</style>
		<div class="container-fluid" id="login-area">
			<div class="row main">
			
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
				<span  class="col-sm-12" style="display:none;color:#de2128" id="loginfailuremsg"></span>
					<form method="post" action="#" method="post" name="login_form" id="login_form" onsubmit="return validateloginForm()">
						
			            <div class="form-group">
							<label for="email" class="cols-sm-2 control-label">User Name</label>
								<input type="text" class="form-control" id="loginid" name="loginid" placeholder="Enter Mobile Number"/>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="Enter your Password"/>
						</div>
					    <div class="form-group ">
							<button type="submit" name="log" style="background-color: #1561a2;border: 1px solid #1c1c43;" class="btn btn-primary btn-lg btn-block login-button">Login</button>
						</div> 
						<div class="login-register">
				         <a href="<?php echo base_url();?>onlinetest/getotp" style="font-size:15px;"><b>First Time User</b></a>
				        </div>
					</form>
				</div>
			</div>
		</div>

 </div>

<script type="text/javascript">

function validateloginForm(){




 var loginid,password;

 //var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  var regex      = /^[0-9]{10}$/;

    if( document.login_form.loginid.value == "" ) {
      document.login_form.loginid.className = 'form-control errorClass';
      document.login_form.loginid.focus();
      return false;
   }else if( document.login_form.loginid.value.length != 10 ) {
    
      document.login_form.loginid.className = 'form-control errorClass';
      return false;
   }else if(!regex.test(document.login_form.loginid.value)){
    alert("ffff1");
      document.login_form.loginid.className = 'form-control errorClass';
      return false;
   }else {
      document.login_form.loginid.className = 'form-control';
      loginid = document.login_form.loginid.value; 
   }



  if( document.login_form.password.value == "" ) {
      document.login_form.password.className = 'form-control errorClass';
      return false;
  }else {
      document.login_form.password.className = 'form-control';
      password = document.login_form.password.value; 
  }
  
  var dataString = 'loginid='+ loginid +'&ajax=1&password='+password;
  //document.getElementById("login").style.display = "none";
  
         
// return false;
  $.ajax({
      url:'<?php echo base_url();?>onlinetest/loginsubmit',
      type: 'POST',
      datatype:'application/json',
      data: dataString,
      success:function(response){  
        //alert(response.message);return false;
        if(response.id == "1"){          
              
              window.location.replace("<?php echo base_url(); ?>onlinetest/dashboard");
              
        }else   if(response.id == 0){          
            
            document.getElementById("loginfailuremsg").style.display = "block";
            document.getElementById("loginfailuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("loginfailuremsg").style.display = "none";
            }, 10000);          
            
        }else {                      
            document.getElementById("loginfailuremsg").style.display = "block";
            document.getElementById("loginfailuremsg").innerHTML= response.message;
            setTimeout(function(){ 
            document.getElementById("loginfailuremsg").style.display = "none";
            }, 10000);          
            
        }
               
      }
  });

  


  return false;
}
</script>
