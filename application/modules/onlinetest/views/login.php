<div class="sign">
	<!-- container fluid -->
	<div class="container-fluid">
		<!-- row -->
		<div class="row align-items-center">
			<!-- left col -->
			<div class="col-lg-6 leftcol d-flex align-items-center">  
				<div class="row justify-content-center w-100">
					<div class="col-lg-10">
						<p><a class="pagelink" href="<?php echo base_url();?>">Home</a></p>
					   <div>
							<h1>BRAINWIZ  <span class="fbold">Test Series !</span></h1> 
							<p>Evaluate Yourself by our Online Test Series.</p>
						</div>
						<ul class="sign-list pb-4">
							<li>Real Time Examination Environment</li>
							<li>Designed according to the Updated questions and Pattern Papers</li>
							<li>Improves your Speed and Accuracy</li>
							<li>Find your Strong and weak areas</li>
							<li>Quick results with solutions</li>
							<li>Keep track of your performance</li>
						</ul>
						<!-- statics row -->
						<div class="row statsrow">
							<div class="col-lg-12">
								<h5 class="h5">We Help You Succeed In Your Campus Interviews.</h5>
							</div>
							<!-- col -->
							<div class="col-lg-4 user_stat">
								<div class="statin text-center">
									<figure>
										<img src="<?php echo base_url();?>/assets/img/statistics-1.png" alt="" title="">
									</figure>
									<p>Learn & Practice</p>
								</div>
							</div>
							<!--/ col -->
							 <!-- col -->
							 <div class="col-lg-4 user_stat">
								<div class="statin text-center">
									<figure>
										<img src="<?php echo base_url();?>/assets/img/statistics-2.png" alt="" title="">
									</figure>
									<p>Improve Your Self</p>
								</div>
							</div>
							<!--/ col -->
							 <!-- col -->
							 <div class="col-lg-4 user_stat">
								<div class=" statin text-center">
									<figure>
										<img src="<?php echo base_url();?>/assets/img/statistics.png" alt="" title="">
									</figure>
									<p>Get Hired</p>
								</div>
							</div>
							<!--/ col -->
						</div>
						<!--/ statics row -->
					</div>
				</div>
			</div>
			<!--/left col -->
			<!-- right col -->
			<div class="col-lg-6"> 
				<div class="signcol">
					<div class="login_wrapper cd-user-modal is-visible">
					
						<!-- login form -->
						<span  class="col-sm-12" style="display:none;color:#de2128" id="loginfailuremsg"></span>
						<div class="login_form animated" id="cd-login">
							<h2 class="h5">Login to your Account</h2>	
							
							<form  id="login_form" name="login_form">
								<div class="form-group">
									<input class="form-control" type="text" id="loginid" name="loginid" placeholder="Mobile Number">
								</div>
								<div class="form-group">
									<input class="form-control" type="password" id="pwd" name="pwd" placeholder="Password">
							  	</div>
								<div class="form-group">
									<input type="submit" value="Login" class="bluebtn w-100 text-uppercase">
								</div>
							
								<div class="form-group text-center">
									<p class="forgot_password_text cd-form-bottom-message"><a href="javascript:;">Forgot password ?</a></p> 
							  	</div>
								<div class="form-group text-center">
									<p class="cd-form-signup">Don't Have an account? <a href="javascript:;">Sign up</a></p>
								</div> 
							</form>
						   
							<form action="" method="post" id="getotp_form" name="getotp_form">
								<span  class="col-sm-12" style="display:none;color:#de2128" id="getotpfailuremsg"></span>
								<span  class="col-sm-12" style="display:none;color:green" id="getotpsuccessmsg"></span>
								<h2 class="h5">For BRAINWIZ Students</h2> 
								<div class="form-group">
									<div class="input-group">
										<input class="form-control" type="" name="mobilenumber" id="mobilenumber" placeholder="Enter Registered Mobile Number">
									</div>
								</div>
								<div class="form-group">
									<input type="submit" id="get_otp_btn1"  value="Get OTP" class="bluebtn w-100 text-uppercase">
								</div>
							</form>
							
							
							<!-- Forgot Password form -->
						
							
							
						</div>
						<div class="login_form register_form forgot_form animated" id="cd-reset-password">
								<h2 class="h5">Reset Password</h2>
                                        <span  class="col-sm-12" style="display:none;color:#de2128" id="getotpfailuremsg1"></span>
                                        <span  class="col-sm-12" style="display:none;color:green" id="getotpsuccessmsg"></span>
								<form action="" method="post" id="getotp_form1" name="getotp_form1">
									<div class="form-group text-center">Lost Your Password ? Please Enter Registered email/Mobile Number below To Reset Password</div>
									<div class="form-group">
									
										<input class="form-control" type="" name="phone" id="phone" placeholder="Enter Registered Mobile Number">
									</div>
									<div class="form-group">
										<input type="submit" value="Reset Password" class="bluebtn w-100 text-uppercase">
									</div>
									
									<div class="form-group text-center">
									  
									  <p id="back_btn" class="cd-form-bottom-message pull-right">Already have Password? 
									  <a class="fblue fbold" href="#">Sign in</a>
									  </p>
									</div>
								</form>
							</div>
							
							<!-- Register form -->
							
						<div class="login_form register_form animated" id="cd-signup">
							<h2 class="h5">Create your student account</h2>
							<form action="" method="post" id="reg_from" name="reg_from">
							
								<div class="form-group">
								  <input class="form-control" type="text" placeholder="Name" name="uname" id="uname">
								</div>

								<div class="form-group">
								  <input class="form-control" type="text" placeholder="Mobile Number" id="mobile" name="mobile">
								</div>

								<div class="form-group">
								  <input class="form-control" type="text" placeholder="Email" name="email" id="email">
								 </div>

								<div class="form-group">
								  <input class="form-control" type="text" placeholder="College Name" name="cname" id="cname" >
								</div>

								<div class="form-group">
								  <input type="submit" id="register_btnd" value="Signup" class="bluebtn w-100 text-uppercase">
								</div>
								
								<div class="form-group text-center">
									  
									  <p id="back_btn" class="cd-form-signin pull-right">Already have an Account?
									  <a class="fblue fbold" href="<?php echo base_url()?>onlinetest/login">Sign in</a>
									  </p>
								</div>
								<div class="form-group text-center">
									<p>By signing up, you agree to our <a class="fblue" href="javascript:;">Terms of Use</a> and <a class="fblue" href="javascript:;">Privacy Policy</a>
									</p>
                            </div>
							</form>
						</div>
						
						  
						
							<!-- get OTP and After Register Form -->
						  
						<div id="after_otp_form" class="animated">
							<form action="" method="post" id="otp_from" name="otp_from">
								<span  class="msg" style="display:none;color:#de2128" id="otpmsg"></span>
								<div class="form-group">
									<input class="form-control" type="text" name="otp" id="otp" placeholder="Enter OTP">
								</div>
							  
								<div class="form-group">
									<input class="form-control" type="password" name="optpwd" id="optpwd" placeholder="Password">
								</div>
								<div class="form-group">
									<input class="form-control" type="password" name="cpwd" id="cpwd" placeholder="Confirm Password">
								</div>
								<div class="form-group">
									<button type="submit" class="bluebtn w-100 text-uppercase">submit</button>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>	
			<!-- right col -->
		</div>
	</div>
</div>
<script src="<?php echo assets_url();?>js/jquery-3.2.1.min.js"></script>
<script src="<?php echo assets_url();?>js/jquery.validate.min.js"></script>
<script src="<?php echo assets_url();?>js/scripts.js"></script>
<script src="<?php echo assets_url();?>js/login-register.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.6/animate.min.css"></script>
<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo assets_url();?>css/modifies-style.css">