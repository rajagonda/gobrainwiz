
<!--sub page main -->
<main class="subpage">
	 <!-- sub page header -->
    <div class="page-header">
		<!-- container -->
		<div class="container">
		   <!-- row -->
		   <div class="row">
			   <div class="col-lg-6">
                               <?php if ($this->session->userdata('user_login') == '1') { ?>	
				   
                                   <h1>Dashboard</h1>
                               <?php } else {?>
                                    <h1>Practice Tests</h1>

                               <?php }?>   
			   </div>
		   </div>
		   <!--/ row --> 
		</div>
		<!--/ container -->
	</div>
	<!--/ sub page header -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- col -->
			<div class="col-lg-12">                   
				<ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
                                    <?php if ($this->session->userdata('user_login') == '1') { ?>	
                                    <li class="breadcrumb-item"><a href="javascript:;"><?php echo $this->session->userdata('examusername');?></a></li>
                                    <li class="breadcrumb-item active"><a>My Profile</a></li>
                                     <?php } else {?>
                                    <li class="breadcrumb-item"><a href="javascript:;">Practice Tests</a></li>
                                     <?php } ?>
                                    
				</ul>                    
			</div>
			<!--/col -->
		</div>
		<!--/ row -->
	</div>
	
    <div class="container">
			<div class="row">	
				<!-- left col 4-->
				<div class="col-lg-4"> 
					<?php require_once('sidebar.php'); ?>
				</div>

            <div class="col-lg-8">
               <h2 class="h5">Upcoming Test</h1>
                    <div class="row">
                        
                        <?php
                        $i = 1;
                        foreach ($tests as $value) {
                            ?>
                            <?php if ($completetests[$value->brain_test_id] != '') { ?>

                            <?php } else { ?>
                                <div class="col-lg-6">

<div class="skilltest-box clearfix">
<figure class="image"><a href="#">
<?php if ($value->test_image != '') { ?>
    <img alt="image" border="0" src="<?php echo base_url(); ?>public/testimages/<?php echo $value->test_image; ?>"
         width="270" height="270">
<?php } else { ?>
    <img alt="image" border="0" src="<?php echo assets_url(); ?>images/logooo.jpg" width="270" height="270">
<?php } ?>


</a></figure>
<div class="details clearfix">
<h5><a href=""><?php echo $value->test_name; ?></a></h5>

    <?php
    if ($this->session->userdata('user_login') == '1') {
        if ($this->session->userdata('examuser_type') != 'y') {
            if (checkusertype($this->session->userdata('studentId'))) {
                ?>
                <a href="<?php echo base_url(); ?>braintest/examboard/<?php echo $value->brain_test_id; ?>"
                   class="btn button1">Take Test</a>
                <?php

            } else {
                if ($i == 1 || $i == 2) {
                    ?>
                    <a href="<?php echo base_url(); ?>braintest/examboard/<?php echo $value->brain_test_id; ?>"
                       class="btn button1">Take Test</a>
                    <?php
                } else { ?>
                    <a href="<?php echo base_url(); ?>onlinetest/package" class="btn button1"
                       style="background:#006400"><span>BUY NOW</span></a>
                    <?php

                }

            }
        } else { ?>
            <a href="<?php echo base_url(); ?>braintest/examboard/<?php echo $value->brain_test_id; ?>"
               class="btn button1">Take Test</a>
            <?php
        }
    } else { ?>
        <a href="<?php echo base_url(); ?>braintest/examboard/<?php echo $value->brain_test_id; ?>" class="btn button1">Take Test</a>
    <?php }

    ?>


    <p>Questions : <?php echo count_q_t('q', $value->brain_test_id); ?><br>
Duration : <?php echo count_q_t('t', $value->brain_test_id); ?> mins</p>
</div>
</div>

                                </div>
                            <?php } ?>
                            <?php $i++;
                        } ?>
                    </div>
                
            </div>
        </div>
    
</main>    