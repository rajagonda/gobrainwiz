  <script src="http://localhost/brainwiz/assets/js/jquery-3.2.1.min.js"></script>
<script src="http://localhost/brainwiz/assets/js/jquery.validate.min.js"></script>
<script src="http://localhost/brainwiz/assets/js/scripts.js"></script>
    <!-- sign in -->
    <div class="sign">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row align-items-center">
                <!-- left col -->
                 <div class="col-lg-6 leftcol d-flex align-items-center">  
                    <div class="row justify-content-center w-100">
                        <div class="col-lg-10">
                            <p>
                                <a class="pagelink" href="<?php echo base_url();?>">Home</a> 
                            </p>
                           <div>
                            <h1>BRAINWIZ  <span class="fbold">Test Series !</span></h1> 
                                <p>Evaluate Yourself by our Online Test Series.</p>
                               
                            </div>
                            <ul class="sign-list pb-4">
                                <li>Real Time Examination Environment</li>
                                <li>Designed according to the Updated questions and Pattern Papers</li>
                                <li>Improves your Speed and Accuracy</li>
                                <li>Find your Strong and weak areas</li>
                                <li>Quick results with solutions</li>
                                <li>Keep track of your performance</li>
                            </ul>
                            <!-- statics row -->
                            <div class="row statsrow">
                            <div class="col-lg-12">
                                <h5 class="h5">We Help You Succeed In Your Campus Interviews.</h5>
                            </div>
                                <!-- col -->
                                <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics-1.png" alt="" title="">
                                        </figure>
                                        <p>Learn & Practice</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics-2.png" alt="" title="">
                                        </figure>
                                        <p>Improve Your Self</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class=" statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics.png" alt="" title="">
                                        </figure>
                                        <p>Get Hired</p>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ statics row -->
                            
                        </div>
                    </div>
                </div>
                <!--/left col -->

                <!-- right col -->
                <div class="col-lg-6">                   
                    <div class="signcol">
                        <h2 class="h5">Create your student account</h2>

                        <form action="" method="post" id="regfrm" name="regfrm">
                            <div class="form-group ">
                                <input type="text"  placeholder="Name" name="uname" id="uname"  class="form-control">
                            </div>
                           
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Mobile Number" id="mobile" name="mobile">
							</div>

                            <div class="form-group ">                               
                                <input class="form-control" type="text" placeholder="Email" name="email" id="email">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="College Name" name="cname" id="cname">
                            </div>
                          
                            <div class="form-group">
                                <input type="submit" value="Sign up" class="bluebtn w-100 text-uppercase">
                            </div>
                           
                            <div class="form-group text-center">
                                <p>Already have an Account? <a class="fblue fbold" href="<?php echo base_url();?>onlinetest/login">Sign in</a></p>
                            </div>
                            
                            <div class="form-group text-center">
                               <p>
                                   By signing up, you agree to our <a class="fblue" href="javascript:void(0)">Terms of Use</a> and <a class="fblue" href="javascript:void(0)">Privacy Policy</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
   