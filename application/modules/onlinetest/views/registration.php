<div class="btwrapper details">
  <header class="brainwiz-header">
  	<div class="pull-center col-md-5">
		<p class="pull-left"><h3>Fill out details.</h3></p>
	</div>

	<div class="pull-center col-md-3">
		<div class="timer">
					<div id="StartCountDownTimer" data-timer="<?php echo $timeleft;?>" style="background-color:#4382C5;"></div>
		  </div>
	</div>	
    <div class="pull-right col-md-4">
    	<p class="text-right text-info"><?php echo $this->session->userdata('studentId');?></p>
        <p class="text-right"><span id="day"><?php echo date('d/m/Y');?></span></p>
    </div>
  </header>
  <div class="btinnerWrapper blue-border">
    <div class="container-fluid">
				 <?php $this->load->view('message'); ?>

		<?php 
							if(!empty($error))
							{
							?>
								<div class="alert alert-danger"><?php echo $error;?></div>
							<?php 
							}
							
							?>
							
      <form id="regFrm" action="" method="post">
      
        <h3 id="buttons-tags">Personal Information</h3>
        <div class="row">
        <div class="form-group col-md-6">
          <label for="exampleInputEmail1">Name</label>
          <input type="text" class="form-control" value="<?php echo (isset($_POST['name']))?$_POST['name']:""?>" name="name" id="btUsername" placeholder="Johnson Kooper" required>
        </div>
        <div class="form-group col-md-6">
          <label for="exampleInputPassword1">Email</label>
          <input type="email" class="form-control" name="email" value="<?php echo (isset($_POST['email']))?$_POST['email']:""?>" id="btEmail" placeholder="Email" required email>
        </div>       
        <div class="form-group col-md-6">
          <label for="exampleInputEmail1">Phone No(10 Digit number start with 7,8 or 9)</label>
          <input type="text" maxlength="10" class="form-control" name="phone" value="<?php echo (isset($_POST['phone']))?$_POST['phone']:""?>"  id="btphone" placeholder="Phone No" required  pattern="[789][0-9]{9}">
        </div>
        <div class="form-group col-md-6">
          <label for="exampleInputPassword1">Address</label>
          <input type="text" class="form-control" name="address" value="<?php echo (isset($_POST['address']))?$_POST['address']:""?>"  id="btAddress" placeholder="Address">
        </div>
        </div>
        
        <h3 id="buttons-tags">Other Information</h3>
        <div class="row">
        <div class="form-group col-md-6">
          <label for="exampleInputEmail1">Name of the College:</label>
          <input type="text" class="form-control" name="college" value="<?php echo (isset($_POST['college']))?$_POST['college']:""?>"  id="btColname" placeholder="College Name" required>
        </div>
		
		<div class="form-group col-md-6">
          <label for="exampleInputEmail1">Fathers Name:</label>
          <input type="text" class="form-control" name="father_name" value="<?php echo (isset($_POST['father_name']))?$_POST['father_name']:""?>"  id="btColname" placeholder="Fathers Name" required>
        </div>
        </div>
        
        <button type="submit" class="btn btn-primary" onClick="onSubmit();">Submit</button>
        
        
      </form>
      
    </div>
  </div>
  <?php
	echo $script;
?>