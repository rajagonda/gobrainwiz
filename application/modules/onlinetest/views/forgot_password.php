
    <!-- sign in -->
    <div class="sign">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row align-items-center">
                <!-- left col -->
               <div class="col-lg-6 leftcol d-flex align-items-center">  
                    <div class="row justify-content-center w-100">
                        <div class="col-lg-10">
                            <p>
                                <a class="pagelink" href="<?php echo base_url();?>">Home</a> 
                            </p>
                           <div>
                            <h1>BRAINWIZ  <span class="fbold">Test Series !</span></h1> 
                                <p>Evaluate Yourself by our Online Test Series.</p>
                               
                            </div>
                            <ul class="sign-list pb-4">
                                <li>Real Time Examination Environment</li>
                                <li>Designed according to the Updated questions and Pattern Papers</li>
                                <li>Improves your Speed and Accuracy</li>
                                <li>Find your Strong and weak areas</li>
                                <li>Quick results with solutions</li>
                                <li>Keep track of your performance</li>
                            </ul>
                            <!-- statics row -->
                            <div class="row statsrow">
                            <div class="col-lg-12">
                                <h5 class="h5">We Help You Succeed In Your Campus Interviews.</h5>
                            </div>
                                <!-- col -->
                                <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics-1.png" alt="" title="">
                                        </figure>
                                        <p>Learn & Practice</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class="statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics-2.png" alt="" title="">
                                        </figure>
                                        <p>Improve Your Self</p>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 user_stat">
                                    <div class=" statin text-center">
                                        <figure>
                                            <img src="<?php echo base_url();?>/assets/img/statistics.png" alt="" title="">
                                        </figure>
                                        <p>Get Hired</p>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ statics row -->
                            
                        </div>
                    </div>
                </div>
                <!--/left col -->

                <!-- right col -->
                <div class="col-lg-6">                   
                    <div class="signcol">
                        <h2 class="h5">Reset Password</h2>
						<p>Lost Your Password ? Please Enter Registered email/Mobile Number below To Reset Password</p>	
                        <form action="" method="post" id="getotp_form" name="getotp_form1">                           
                            <div class="form-group">
								<input class="form-control" type="text" name="mobilenumber" id="mobilenumber" placeholder="Email / Phone Number">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Reset Password" class="bluebtn w-100 text-uppercase">
                            </div>
                           
                            <div class="form-group text-center">
                                <p>Already have Password? <a class="fblue fbold" href="<?php echo base_url();?>onlinetest/login">Sign in</a></p>
                            </div>
                            <div class="form-group text-center">
                               <p>On click Reset Password you will get Reset link to your Registered Email.</p>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->

    </div>
    <!--/ sign in --> 
   