<link href="<?php echo assets_url();?>new/css/easy.css" rel="stylesheet" type="text/css">
<style>
.section_schedule {
      margin-top: -46px;
    background: #d4d4d4;
    margin-bottom: -99px;
    color: #1b1b42;
}
.center {
    width: 100%;
    max-width: 1173px;
    margin: 0 auto;
    padding: 0 30px;
}
.section__title {
    font-size: 20px;
    text-transform: uppercase;
    letter-spacing: 4.44px;
}
.fadeInUp {
    -webkit-animation-name: fadeInUp;
    animation-name: fadeInUp;
}
0% {
    opacity: 0;
    -webkit-transform: translate3d(0, 15%, 0);
    transform: translate3d(0, 15%, 0);
}
.section__content {
    max-width: 920px;
    margin: 40px auto 0;
    opacity: .7;
    font-size: 35px;
    line-height: 1.48;
}
.section_schedule .section__body {
    padding-top: 20px;
}
.schedule__container {
    margin-bottom: 70px;
}


.schedule__th {
    position: relative;
    top: 8px;
    margin-left: 2px;
    text-transform: uppercase;
    font-size: 16px;
    vertical-align: top;
}
.schedule__row {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    padding: 28px 0;
    border-bottom: 1px solid rgba(255, 255, 255, 0.15);
    font-size: 24px;
    line-height: 1.16;
}
.schedule__time {
    padding-right: 30px;
    -webkit-flex: 0 0 284px;
    -ms-flex: 0 0 284px;
    flex: 0 0 284px;
    font-weight: 400;
}
.schedule__wrap {
    -webkit-flex: 1 1 auto;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
}
.schedule__title {
    margin-bottom: 12px;
    font-weight: 400;
}
.schedule__text {
    opacity: .7;
    font-size: 20px;
    line-height: 1.35;
}
.more {
    text-align: center;
}
.btn_white {
    border-color: rgba(255, 255, 255, 0.15);
    color: #FFF;
}
.inrright_cntr .achievement th, td{
	letter-spacing: 1.2px;
}
table tr td{
background: #fff;
    text-align: center;
    color:rgb(88, 88, 156);	
}
table tr th {
    font-size: 17px;
    background-color: rgb(142, 214, 147);
    color: #F44336;
    padding: 18px 76px;
    border-top: none !important;
}
table tr td a{
color:rgb(27, 27, 66);
}
table tr td a:hover{
	color:#0d776d;
}
.navbar-default .navbar-nav > li > a {
font-weight:500;
}
</style>	
<section>
 <div class="section section_schedule">
          <div class="section__center center">
             <div class="section__body">
              <div class="schedule">
                <div class="schedule__container">
                  <div class="schedule__head schedule__head_red wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Order Status</div>
				
<?php
                echo "<h3>Thank You. Your order status is ". $status .".</h3>";
          echo "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
          
          ?>
		          </div>
              </div>
              
            </div>
          </div>
        </div>
</section>