<div class="btwrapper">
  <header class="brainwiz-header">
    <div class="pull-left col-md-6">
		<div class="btn-group" role="group" aria-label="...">
	  <?php
		
		if(isset($categories))
		{
			foreach($categories as $cat)
			{
				$buttunClass="btn-default";
				if($cat->complete==1)
					$buttunClass="btn-success";
				else if($category==$cat->category_name)
					$buttunClass="btn-primary";
				?>
				      <button type="button" class="btn <?php echo $buttunClass;?> btn-sm " disabled ><?php echo $cat->category_name?></button>
				<?php
				

			}
		}
		
	  ?>
      
    </div>
    </div>
    <div class="pull-right col-md-6">
    	<p class="text-right text-info"><?php echo $this->session->userdata('studentId');?></p>
        <p class="text-right"><span id="day"><?php echo date('d/m/Y');?></span></p>
        <p class="text-right"><a href="<?php echo base_url()."braintest/logout"?>"><span id="">Log Out</span></a></p>
		
    </div>
  </header>
  <div class="btinnerWrapper blue-border">
    <div class="container-fluid pos_relative">
    	<div class="col-md-8">
        <div class="pg-title"><?php echo $category;?></div>
		<?php 
			if(!empty($question) && isset($question->question_name))
			{
				if($question->attempet==2)
				{
				?>
				  		<button id="markreview" rel="<?php echo base_url(); ?>braintest/markreview" class="btn btn-warning pull-right btn-sm mark_for_rev"><i class="fa fa-flag"></i></button>

				<?php
				}
				else
				{
				?>
				 <button id="markreview" rel="<?php echo base_url(); ?>braintest/markreview" class="btn btn-default pull-right btn-sm mark_for_rev"><i class="fa fa-flag"></i></button>

				<?php 
				
				}
			
			}
		?>
  <div class="bt_paper">
  <?php 
  //print_r($question);
  //print_r($questions);
  if(!empty($question) && isset($question->question_name))
  {
  ?>
	<form name="qfrm" id="qfrm" action="<?php echo base_url()."braintest/submitanswer"?>" method="post" >
  	<div class="bt_question_title">Question <?php echo $question->serial_number;?></div>
    <div class="bt_question"><?php echo $question->question_name;?></div>
    <div class="bt_answer">
    	<ul class="list-unstyled">
		<?php 
			for($i=1 ; $i<=5;$i++)
			{
				$varname="option".$i;
				$option= $question->$varname;
				if($option!="")
				{
		?>
        <li>
		<input type="radio"  value="<?php  echo $i;?>" <?php echo($question->answer!="" && $question->answer==$i)?"checked":"";?> name="answer">&nbsp;<label>
      <?php 
		
	  echo $option;?></label>
		</li>
		
		<?php
			}
		} ?>
   
        </ul>
    </div>
    <div class="bt_action">
	
		<?php 
			if($question->serial_number>1)
			{
		?>
    	<button type="button" class="btn btn-default" onClick="getUrlTest('<?php echo base_url()."braintest/braintestquestion/".$question->cat_id."/".($question->serial_number-1); ?>')" >Previous</button>
		<?php
			}
		
			if($question->serial_number<count($questions))
			{
		?>
        <!--<button type="button" id="next_q_button" class="btn btn-default" onClick="getUrlTest('<?php echo base_url()."braintest/braintestquestion/".$question->cat_id."/".($question->serial_number+1); ?>')" >Next</button>
		-->
				<input type="hidden" name="next_q_id"  value="<?php echo $question->serial_number+1 ;?>"/>
				<button type="submit" class="btn btn-primary">Next</button>

			<?php
			}
			elseif(!$activate_complete_button)
				{
			?>
				<button type="submit" class="btn btn-primary">Save</button>
			<?php 
				}
			?>
		        
		<input type="hidden" id="q_id" name="q_id" value="<?php echo $question->q_id ;?>"/>
		<input type="hidden" id="cat_id" name="cat_id" value="<?php echo $question->cat_id ;?>"/>
		<input type="hidden" id="q_no" name="q_no" value="<?php echo $question->serial_number ;?>"/>
		
		<button type="button" id="completetest" class="btn btn-default pull-right" rel="<?php echo base_url()."braintest/completetest" ?>" >Complete</button>
    </div>
	</form>
  <?php 
  }
  else
  {
	echo " No more questions available for this section.";
	?>
	  <button type="button" id="completetest" class="btn btn-default pull-right" rel="<?php echo base_url()."braintest/completetest" ?>" >Complete</button>

	<?php 
	
  }
  ?>
  </div>
        </div>
        <div class="col-md-4 right_panel">
    	<div class="timer">
        	<h4>Time left</h4>
            <div class="timer">
                <div id="CountDownTimer" data-timer="<?php echo $timeleft;?>" style="width: 238px; height: 120px;"></div>
            </div>
        </div>
        <div class="analyse" style="overflow: auto; height: 260px;">
        	<h4>Questions </h4>
            <table class="table table-condensed table-bordered">
			
		<?php 
				if(is_array($questions) && count($questions)>0)
				{
				foreach($questions as $qkey => $qValue)
				{
					switch($qValue->attempet)
							{
								case 0:
									$class="";
									$icon="";
								break;
								case 1:
									$class="not_attempt";
									$icon='<i class="fa fa-stop"></i>';
								break;
								case 2:
									$class="mark_for_rev";
									$icon='<i class="fa fa-flag">';

								break;
								case 3:
									$class="passed";
									$icon='<i class="fa fa-strikethrough"></i>';
									
								break;

							}
							
							if( isset($question->q_id) && $qValue->q_id==$question->q_id)
							{
								$class="current";
								$icon='<i class="fa fa-dot-circle-o"></i>';
									
							}
							
					if(($qkey+1)%5==0)
					{

						echo '<td class="'.$class.'" rel="'.base_url()."braintest/braintestquestion/".$qValue->cat_id."/".$qValue->serial_number.'">'.$icon.($qkey+1). '</td>';
						
					echo '</tr>';


					}
					else
					{
						if(($qkey+1)%5==1)
							echo '<tr>';
							
						echo '<td class="'.$class.'" rel="'.base_url()."braintest/braintestquestion/".$qValue->cat_id."/".$qValue->serial_number.'">'.$icon.($qkey+1). '</td>';
	
					}
				}
			}
		?>
            <!-- <tr>
            <td class="passed"><i class="fa fa-strikethrough"></i>1</td>
            <td class="passed"><i class="fa fa-strikethrough"></i>2</td>
            <td class="passed"><i class="fa fa-strikethrough"></i>3</td>
            <td class="passed"><i class="fa fa-strikethrough"></i>4</td>
            <td class="mark_for_rev"><i class="fa fa-flag"></i>5</td>
            </tr>
            <tr>
            <td class="not_attempt"><i class="fa fa-stop"></i>6</td>
            <td class="current"><i class="fa fa-dot-circle-o"></i>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            </tr>
			-->
            </table>
        </div>
      
      </div>
    </div>
  </div>
  
  