<div class="btwrapper">

  <header class="brainwiz-header">
	<?php 
	if(isset($timeleft) && $timeleft>0)
	{
	?>
	    <h3 class="pull-left col-md-5">Its Break Time</h3>

	<div class="pull-center col-md-3">
		<div class="timer">

					<div id="breakCountDownTimer" data-timer="<?php echo $timeleft;?>" style="background-color:#4382C5;"></div>
		  </div>
	</div>
	<?php 
	} 
	else
		echo '<h3 class="pull-left col-md-6">Welcome '.$this->session->userdata('name') .' ,</h3>';

	?>
    <div class="pull-right col-md-3">
    	<p class="text-right text-info"><?php echo $this->session->userdata('studentId');?></p>
        <p class="text-right"><span id="day"><?php echo date('d/m/Y');?></span></p>
		<p class="text-right"><a href="<?php echo base_url()."braintest/logout"?>"><span id="">Log Out</span></a></p>

    </div>
  </header>
<div class="clear"></div>
  <div class="btinnerWrapper blue-border">
    <div class="container-fluid">
    	<h3 id="buttons-tags">Dashboard</h3>
		 <?php $this->load->view('message'); ?>

      <div class="btn-group" role="group" aria-label="...">
	  <?php
		
		if(isset($categories))
		{
			foreach($categories as $cat)
			{
				$buttunClass="btn-default";
				if($cat->complete==1)
					$buttunClass="btn-success";
				?>
				      <button type="button" class="btn <?php echo $buttunClass;?> btn-lg categoryBtn"  onClick="getUrlTest('<?php echo base_url()."braintest/brainteststart/".$cat->id; ?>')"><?php echo $cat->category_name; ?></button>
					  <?php  /*echo (isset($timeleft) && $timeleft>0)?"disabled":""*/?>
				<?php
				

			}
		}
		
	  ?>
      
    </div>
      <div class="table-features">
	  <p>
	  <b>Instructions:</b>
<ol>
<li>Test consists of <?php echo count($categories);?> sections i.e. <?php foreach($categories as $cat){echo $cat->category_name.", ";}?></li>
<li>These section will be of <?php echo $testdetails->duration/count($categories) ;?> mins each.</li>
<li>After finishing each section you will have a break time of <?php echo $testdetails->break_time; ?> min.</li>
<li>Once the section is completed you cannot go back to that section.</li>
<li>You can chose the section you want to attempt from Instruction page(current).</li>
<li>Section can be selected from the top navigation tab.</li>
<li>Once you click on compete button a confirmation will be asked whether you want to complete the section or not.</li>
<li>Each question will be of 1 mark.</li>
<li>You will not have multiple choice questions.</li>
<li>You need to click on <b>"Next/ Save "</b> button on order to save your marked answer.</li>
<li>Result will be displayed immediately after you complete all the three sections.</li>
</ol>
<br/>
All The Best.
<br/>
<b>Pavan Jaiswal.</b>
	  </p>
    </div>
    </div>
  </div>
  