
<main class="subpage">
	<div class="page-header">
		<!-- container -->
		<div class="container">
		   <!-- row -->
		   <div class="row">
			   <div class="col-lg-6">
				   <h1>Wallet</h1>
			   </div>
		   </div>
		   <!--/ row --> 
		</div>
		<!--/ container -->
	</div>
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- col -->
			<div class="col-lg-12">                   
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;"><?php echo $this->session->userdata('examusername');?></a></li>
					<li class="breadcrumb-item active"><a>Wallet</a></li>
				</ul>                    
			</div>
			<!--/col -->
		</div>
		<!--/ row -->
	</div>



    <div class="container">
        <div class="row">
            <!-- left col 4-->
			<div class="col-lg-4"> 
				<?php require_once('sidebar.php'); ?>
			</div>

             <div class="col-lg-8">
                <div class="inrright_cntr" style="box-shadow:none;">
                    <div class="wrapdiv" style="overflow: auto;">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if ($this->session->userdata('wallet_message')) {
                                    ?>
                                    <div class="box" style="border-top: #fff;">
                                        <div class="box-header">
                                            <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                                                <p style="margin:10px">
                                                    <strong>SUCCESS: </strong>
                                                    <?php
                                                    echo $this->session->userdata('wallet_message');
                                                    $this->session->set_userdata('wallet_message', "");
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="box">
                                    <div class="box-header">

                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th style="width: 10px"> Sno</th>
                                                    <th> Coupon ID</th>
                                                    <th> Coupon Type</th>
                                                    <th> Price Type</th>
                                                    <th> Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if (count($wallet) > 0) {
                                                    foreach ($wallet as $index => $record) {
                                                        ?>
                                                        <tr>
                                                            <td> <?= $index + 1 ?></td>

                                                            <td title="<?php if(isset($record->uw_coupon_id)){ echo getCouponCodeFromID($record->uw_coupon_id); }  ?>"> <?php if(isset($record->uw_coupon_id)){ echo getCouponCodeFromID($record->uw_coupon_id); } ?></td>

                                                            <td title="<?= $record->uw_coupon_type ?>"> <?= $record->uw_coupon_type ?></td>
                                                            <td title="<?= $record->uw_price_type ?>"> <?= $record->uw_price_type ?></td>
                                                            <td title="<?= $record->uw_price ?>"> <?= $record->uw_price ?></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="5" style="text-align:center">No Records Found</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <?php echo $links; ?>
                                    </div>
                                    <span>Total : ₹ <?php echo getWalletByUser($this->session->userdata('studentId'));?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>    