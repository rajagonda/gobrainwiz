<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Company braintestquestions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class onlinetest_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();

    }

    public function checkmobile($mobile)
    {
        $this->db->where('examuser_mobile', $mobile);
        $query = $this->db->get('gk_examuserslist');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkotp($mobile, $otp)
    {

        $sql = "SELECT * FROM gk_temotps WHERE mobile_number='$mobile' AND otp='$otp' ORDER BY otp_id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getonlinetests()
    {

        $sql = "SELECT * FROM gk_braintests WHERE test_status='y'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getcompletetdtests($tid = null, $sid)
    {
        $sql = "SELECT * FROM gk_braintest_student WHERE  studentId = '$sid' AND brainTestId ='$tid' AND test_status='3'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getdetails($id)
    {
        $sql = "SELECT * FROM gk_paymentusers WHERE  userid = '$id' AND status='y'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function insertorderinfo($data)
    {
		
    }
	public function update_avatar($avatar,$studentId){
		$sql = "update gk_examuserslist SET profile_pic = '".$avatar."' where examuser_id = '".$studentId."'";
                
		$query = $this->db->query($sql);
                                

       
	}

    public function addWithdrawRequest($form)
    {
        return $this->db->insert('gk_withdraw_requests', $form);
    }
    public function withdrawRequestByUser($id)
    {
        $sql = "SELECT * FROM gk_withdraw_requests where wr_user_id=".$id;


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }
    public function getwithdrawrequestsByUser($id,$limit, $start)
    {

        $sql = "SELECT * FROM gk_withdraw_requests where  wr_user_id='$id'
			        ORDER BY wr_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }
    public function userWalletCountByUser($id)
    {

        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id=" . $id;


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }
    public function getWalletByUser($id, $limit, $start)
    {

        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id='$id'
           		  ORDER BY uw_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }

    }


}