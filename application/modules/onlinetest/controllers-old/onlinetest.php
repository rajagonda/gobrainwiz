<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class onlinetest extends MY_Controller {
        public function __construct() {
           parent::__construct();
           $this->load->library(array('template','form_validation'));
           date_default_timezone_set('Asia/Kolkata');
           $this->load->helper('download');
           $this->load->model('braintest/braintest_model');
           $this->load->model('onlinetest_model');                      
           $this->load->helper('custom');
           $this->load->helper('phpass'); 
          // $this->load->library('facebook');
         

        }
    public function login()  {

      $this->template->theme('login');
    }


 function loginsubmit(){
        if($_POST){
            //echo "<pre>"; print_r($_POST);exit;
            $loginid = $this->input->post('loginid');            
            $password = $this->input->post('password');  

            if($loginid == '' || $password == ''){
                $data['message'] =  "Please Fill Required Details";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;
            }
             if(!preg_match('/^[0-9]{10}+$/', $loginid)) {
                $data['message'] =  "Enter Valid Format Mobile ";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;
                }
                    


            //check email exit or not 
            $loginDeatils = $this->onlinetest_model->checkmobile($loginid);
            if($loginDeatils){

           
                //check password is correct or not
                $password = trim($password);
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                if($hasher->CheckPassword( $password, $loginDeatils->examuser_password)){
                  

                    $this->session->set_userdata('user_login','2');
                    $this->session->set_userdata('studentId',$loginDeatils->examuser_id);
                    $this->session->set_userdata('examuseremail',$loginDeatils->examuser_email);
                    $this->session->set_userdata('examusername',$loginDeatils->examuser_name);                    
                    $this->session->set_userdata('examusermobile',$loginDeatils->examuser_mobile);                    
                    $data['message'] =  'Successfully Login Done ';
                    $data['id'] =  "1";

                    header('Content-Type: application/json');
                    echo  json_encode($data);
                    exit; 


                

                }else {
                    $data['message'] =  "Invalid Password";
                    $data['id'] =  "0";
                    header('Content-Type: application/json');
                    echo  json_encode($data);
                    exit;         
                }

            }else {
                $data['message'] =  "Invalid Mobile/password";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;   
            }


        }
    }


    public function getotp()  {

    $this->template->theme('getotp');
    }
    public function generateotp(){
       if($_POST){
            //echo "<pre>"; print_r($_POST);exit;

            $loginid = $this->input->post('loginid');                                  
            
            

            if($loginid == ''){
                $data['message'] =  "Please Fill Required Details";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;     
            }

            if(!preg_match('/^[0-9]{10}+$/', $loginid)) {
                $data['message'] =  "Enter Valid Format Mobile ";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;
                }else {
                    //check email was exit or not in database
                    $res = $this->onlinetest_model->checkmobile($loginid);
                    if(!$res){
                    $data['message'] =  " You are not registered with us. Contact with Management!";
                    $data['id'] =  "0";
                    header('Content-Type: application/json');
                    echo  json_encode($data);
                    exit;
                    }
                  }

                
                $otp = rand(8956,2356);   

                //prepare user table data
                $otpData['mobile_number'] = $loginid;
                $otpData['otp'] = $otp;
                $otpData['send_to'] = 'mobile';
                $otpData['type'] = 'reg';                                             
                $otpData['otp_from'] = 'web';
                $otpId = $this->common_model->insertSingle('gk_temotps',$otpData);
                if($otpId){
                
                // $msg = "$otp is the OTP for your Online Test Registration.
                //         Expires after use.Please do not share with anyone.";

                $msg = "$otp is the OTP for your Online Test Registration.Expires after use.Please do not share with anyone.
                        Team    
                        BRAINWIZ";


               // $msg = str_replace(' ','%20',$msg);
                smssend_new($loginid, $msg);


                $data['message'] =  "Otp Sent Requested Mobile Number";
                $data['id'] =  "1";

                //echo "<pre>"; print_r($data);exit;
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;
               
                
                
                }else {
                $data['message'] =  "Technical Error Please Try Again Once!";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit; 
                }      
      
        }else {
         redirect('Home','refresh');
        }
    }
    public function checkotp(){

        if($_POST){
            //echo "<pre>"; print_r($_POST);exit;

            $otp = $this->input->post('otp');                                   
            $password = $this->input->post('password');   
            $password_again = $this->input->post('password_again');    
            $mobile = $this->input->post('mobile');    
                    
            

            if($otp == '' || $password == '' || $password_again == '' || $mobile=='' ){
                $data['message'] =  "Please Fill Required Details";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit;     
            }
           
               
                             
                
            //check email was exit or not in database
            $res = $this->onlinetest_model->checkotp($mobile, $otp);
            if(!$res){
            $data['message'] =  "Please Enter Valid Otp";
            $data['id'] =  "0";
            header('Content-Type: application/json');
            echo  json_encode($data);
            exit;
            }
                 

                
                
                
                
                //Convert Password To Security Hash Format          
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                $hashed_password = $hasher->HashPassword( $password);
                $userData['examuser_password'] = $hashed_password;


                $updateParams = array('examuser_mobile'=> $mobile);
                $memberUpdateResult= $this->common_model->updateRow('gk_examuserslist', $userData, $updateParams);
                
                if($memberUpdateResult){
               
               
                    $data['message'] =  "Your Password Updation done";
                    $data['id'] =  "1";
                    header('Content-Type: application/json');
                    echo  json_encode($data);
                    exit;
               
                
                }else {
                $data['message'] =  "Technical Error Please Try Again Once!";
                $data['id'] =  "0";
                header('Content-Type: application/json');
                echo  json_encode($data);
                exit; 
                }      
      
        }else {
         redirect('Home','refresh');
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('pages/home');
    }
    function dashboard(){
        if(!$this->session->userdata('user_login') == '2'){
            redirect(base_url()."onlinetest/login");
        }
        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if($data['tests']){
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        }else {
            $data['tcount'] = 0;
        }
        $studentId= $this->session->userdata('studentId');


        if($data['tests'] !=''){
            $a = '0';
            foreach($data['tests'] as $value){
            $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id,$studentId);
            if($data['completetests'][$value->brain_test_id] !='')
            {
                $a+= 1;
            }
        }
        }
        //echo $a;

       
            $data['completetestscount'] = $a;
        




        //echo "<pre>"; print_r($data);exit;

        $this->template->theme('newdashboard', $data);
    }
     function testhistory(){
        if(!$this->session->userdata('user_login') == '2'){
            redirect(base_url()."onlinetest/login");
        }
        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if($data['tests']){
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        }else {
            $data['tcount'] = 0;
        }
        $studentId= $this->session->userdata('studentId');


        if($data['tests'] !=''){
            $a = '0';
            foreach($data['tests'] as $value){
            $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id,$studentId);
            if($data['completetests'][$value->brain_test_id] !='')
            {
                $a+= 1;
            }
        }
        }
        //echo $a;

       
            $data['completetestscount'] = $a;
        




        //echo "<pre>"; print_r($data);exit;

        $this->template->theme('testhistory', $data);
    }
    public function scorecard($id){

        $studentId= $this->session->userdata('studentId');
        //$test_id= $this->session->userdata('brainTestId');
             
        $data['tresult'] = $this->braintest_model->gettestresult($studentId,$id);
        $data['testdetails'] = $this->braintest_model->getTestDetails($id);
             
             $this->template->exam_theme('testresult', $data);
   
    
    }



  }