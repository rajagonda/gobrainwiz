<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class braintest extends MY_Controller {
        public function __construct() {
           parent::__construct();
           $this->load->library(array('template','form_validation'));
           date_default_timezone_set('Asia/Kolkata');
           $this->load->helper('download');
           $this->load->model('braintest_model');
           $this->load->model('questions_model');
           $this->load->model('my_model');
          // $this->load->model('register_model');
          // $this->load->library('facebook');
         

        }
    public function index()	
	{
			$data['error']="";
		if($_POST)
		{
			          $username = $this->input->post('btInputUsername');
			          $password = $this->input->post('btPassword');
					  $data['student'] = $this->braintest_model->varifystudent($username,$password);

					if($data['student']=="")
					{
						$data['error']="Wrong credentials";
				
					}
					else
					{
			
						$data['student']->last_login=date("Y-m-d H:i:s");
						$updateData = $this->braintest_model->updatestudent(array("last_login"=>$data['student']->last_login),$data['student']->studentId);
						
						$student_data = $data['student'];
						if($this->check_valid_test_time($student_data->brainTestId))
						{
							if($student_data->test_status==3)
							{
								$data['error']="You have already completed the test ";
							}
							else
							{
								$this->session->set_userdata($student_data);
								if($student_data->test_status==0)
								{
									redirect(base_url()."braintest/registration"); 
								}
								elseif($student_data->test_status==1)
								{
									redirect(base_url()."braintest/dashboard"); 

								}
								elseif($student_data->test_status==2)
								{
									redirect(base_url()."braintest/dashboard"); 

								}
							}
						}
						else
						{
							$data['error']="Sorry You are late. Your Test Time Over.";

						}
						
					}

		}

           $this->template->braintest_theme('login',$data);
	}
	
	public function registration() 
	{
		if($this->checkstudentlogin())
		{
        $data['error']="";
		
		$id =$this->session->userdata('studentId');   
		$test_id= $this->session->userdata('brainTestId');

        $data['student'] = $this->braintest_model->getstudent($id);
		
		if($data['student']->test_status==0)
		
		{
		
			$validationRules = $this->_rules2();

        foreach ($validationRules as $form_field)   {

        $rules[] = array(

        'name' => $form_field['field'],

        'display' => $form_field['label'],

        'rules' => $form_field['rules'],

        );

        }

      

$json_rules = json_encode($rules);

$script = <<< JS

<script>

var CIS = CIS || { Script: { queue: [] } };

CIS.Form.validation('topper_from',{$json_rules});

</script>

JS;
        $this->form_validation->set_rules($validationRules);

		if (isset($_POST) && is_array($_POST) && count($_POST) > 0) 
		{
		 if($this->check_valid_test_start_time()==true)
			{
				
				$set_id=$this->braintest_model->getRandomQuestionsetByTest($test_id);
				
				if($set_id!="")
				{
					$form_values['name'] = $this->input->post('name');
					$form_values['email'] = $this->input->post('email');
					$form_values['phone'] = $this->input->post('phone');
					$form_values['address'] = $this->input->post('address');
					$form_values['college'] = $this->input->post('college');
					$form_values['father_name'] = $this->input->post('father_name');
					$form_values['set_id'] = $set_id[0]->set_id;
								
					$form_values['test_status'] = 1;


					if($id !='')
					{

					$res = $this->braintest_model->updatestudent($form_values,$id);

					}
					        $student_data = $this->braintest_model->getstudent($id);
							$this->session->set_userdata($student_data);

							redirect(base_url()."braintest/dashboard");
							
				}
				else
				{
					$data['error']="Test generation fail.Cannot generate Question set.";
				}
			
			}
			else
			{
				$messagedata = array('error_message'  =>'It’s too early to start the test please start the test on time.');
				$this->session->set_userdata($messagedata);

			}

        }         
  		   $testdetails= $this->braintest_model->getTestDetails($test_id);	

			$testtimetxt=$testdetails->test_date." ".$testdetails->test_time;
		    $testtime=strtotime($testtimetxt);
		   $data["timeleft"]=$testtime-time();
		   
		$data['script'] = $script;

		$this->template->braintest_theme('registration',$data);
		
		}
		else
		{
			redirect(base_url()."braintest/dashboard");

		}
	  }
    }
		
	public function dashboard($breaktime=null) 
	{

		if($this->checkstudentlogin())
		{
	
			
			//$data['questionset']=$this->braintest_model->getQuestionsetById($sid);
		
			$studentId= $this->session->userdata('studentId');
			$test_id= $this->session->userdata('brainTestId');
			$data['testdetails']= $this->braintest_model->getTestDetails($test_id);	

		
			$data['categories'] = $this->getAllCategoryWithComplition();
						
			$this->session->set_userdata(array("total_category"=>count($data['categories'])));
			if($breaktime==1)
			{
				 $timeleft=$this->break_time_for_test();
				
				if($timeleft>0)
					{ 
						$data["timeleft"]= $timeleft;
					}

			}
			$this->template->braintest_theme('dashboard',$data);

		}
            
    }
		
	public function brainteststart($catid=null) 
	{ 
            
			
          if($this->checkstudentlogin())
		{
			if($catid!=null)
			{
				
				$studentId= $this->session->userdata('studentId');
				$test_id= $this->session->userdata('brainTestId');
				
				
				$data['student'] = $this->braintest_model->getstudent($studentId);
				$set_id= $data['student']->set_id;

				if($data['student']->test_status==1)
				{
		
					$form_values['test_status'] = 2;
					$res = $this->braintest_model->updatestudent($form_values,$studentId);
					
				}
				
				$currentLog = $this->braintest_model->getLatestLogOfstudent($studentId);
				
				if($currentLog=="")
				{
					$log	= $this->braintest_model->getStudentLog(array('cat_id'=>$catid,'student_id'=>$studentId));
					if($log=="")
					{
						$logData['student_id']=$studentId;
						$logData['cat_id']=$catid;
						$logData['start_time']=date("Y-m-d H:i:s");
						
						$currentLog = $this->braintest_model->addLog($logData);
						$question=$this->braintest_model->getQuestionByCategory($test_id,$set_id,$catid);
						
						 $this->braintest_model->prepareAnswareData($question,$studentId,$currentLog);

						redirect(base_url()."braintest/braintestquestion/".$catid);
					}
					else if($log[0]->start_time!="0000-00-00 00:00:00" && $log[0]->end_time!="0000-00-00 00:00:00")
					{
						$messagedata = array('info_message'  => 'This section already completed.');

						$this->session->set_userdata($messagedata);

						redirect(base_url()."braintest/dashboard");

					}

				}
				else
				{
					$log_id=$currentLog->log_id;
					$catid=$currentLog->cat_id;
					
					$incompleteAnsware=$this->braintest_model->getIncompleteAnswareBylog($log_id);
					
					if($incompleteAnsware!="")
					{
						$q_id=$incompleteAnsware->q_no;
						redirect(base_url()."braintest/braintestquestion/".$catid."/".$q_id);
					}
					else
					{
					
						redirect(base_url()."braintest/braintestquestion/".$catid);

						// this category is completed
						//redirect(base_url()."braintest/dashboard");

					}
					
				}
				
								

			}
			else
			{
				redirect(base_url()."braintest/dashboard");
			}
		}
           
    }
        
	public function braintestquestion($catid=null,$qNo=null) 
	{ 
            
          if($this->checkstudentlogin())
		{
			if($catid!=null && $this->check_valid_category($catid))
			{
				
				$studentId= $this->session->userdata('studentId');
				$test_id= $this->session->userdata('brainTestId');
				
				
				$data['student'] = $this->braintest_model->getstudent($studentId);
				$data['activate_complete_button']=false;

				
				$set_id= $data['student']->set_id;
				$data['questions']=$this->braintest_model->getQuestionAnswareByCategory($test_id,$set_id,$catid);
				
				$category=$this->braintest_model->getCategoryById($catid);
					$data['category']="";
					
					if(!$category=="")
					{ 
						$data['category']=$category[0]->category_name;
					}
					
				if(count($data['questions'])>0 && isset($data['questions'][0]->q_id))
				{
					$data['question']="";

					if($qNo==null && isset($data['questions'][0]))
					{
					  $data['question']=$data['questions'][0];
					}
					else if(isset($data['questions'][$qNo-1]))
					{
						$data['question']=$data['questions'][$qNo-1];
					}
					else
					{
						
					}
					if($data['question']->attempet==0)
					{
							// Update answer flag
						$answer['end_time'] ="0000-00-00 00:00:00" ;
						$answer['start_time'] =date("Y-m-d H:i:s") ;
						$answer['attempet'] =1;
						$qid=$data['question']->q_id;
						
						$res = $this->braintest_model->update_answer($answer,$studentId,$qid);
					}
					
											
				} 
					$data['timeleft']=$this->time_left_for_currentCategory();
				    if($data['timeleft']>0)
					{
						$data['categories'] = $this->getAllCategoryWithComplition();
						$this->template->braintest_theme('testquestions', $data);

					}
					else
					{
						redirect(base_url()."braintest/completetest");

					}
			}
			else
			{
				$messagedata = array('error_message'  => 'This  is not currently active or valid section.');
				$this->session->set_userdata($messagedata);

				redirect(base_url()."braintest/dashboard");
				
			}
		}
           
    }
        
	public function submitanswer()
	{
		if($this->checkstudentlogin())
		{
			
			if($_POST)
			{
				$studentId= $this->session->userdata('studentId');
				$test_id= $this->session->userdata('brainTestId');
				
				$q_id =  $this->input->post('q_id');
				$cat_id =  $this->input->post('cat_id');
				$answer =  $this->input->post('answer');
				$question = $this->questions_model->getquestiondetails($q_id);
				
				$data['student'] = $this->braintest_model->getstudent($studentId);
				$set_id= $data['student']->set_id;
				
				$oldValue=$this->braintest_model->get_answer($studentId,$q_id);

				
				$form_values['answer'] =$answer ;
				$form_values['end_time'] =date("Y-m-d H:i:s") ;
				
				if(isset($oldValue->attempet) && ($oldValue->attempet!=2))
				{
					if($answer!="")
					{
						$form_values['attempet'] =3;
					}
					else
					{
						$form_values['attempet'] =1 ;
					}
				}


				if($answer!="")
				{
					if($answer==$question->question_answer)
					{
						$form_values['correct'] =1 ;

					}
					else
					{
						$form_values['correct'] =0 ;
					}
	
				}
						
				
				$res = $this->braintest_model->update_answer($form_values,$studentId,$q_id);
				
				if(isset($_POST['next_q_id']))
				{	$nextqid=$this->input->post('next_q_id');
					redirect(base_url()."braintest/braintestquestion/".$cat_id."/".$nextqid);
				}
				else
				{
					$data['questions']=$this->braintest_model->getQuestionAnswareByCategory($test_id,$set_id,$cat_id);

					$data['question']="";
					$category=$this->braintest_model->getCategoryById($cat_id);
					$data['category']="";
					
					if(!$category=="")
					{ 
						$data['category']=$category[0]->category_name;
					}
					$data['activate_complete_button']=true;
					$data['timeleft']=$this->time_left_for_currentCategory();

					$this->template->braintest_theme('testquestions', $data);


				}

			}

		}
	}	
	
	
	public function markreview()
	{
		if($this->checkstudentlogin())
		{
			
			if($_POST)
			{
				$studentId= $this->session->userdata('studentId');
				$test_id= $this->session->userdata('brainTestId');
				
				$q_id =  $this->input->post('q_id');
				$cat_id =  $this->input->post('cat_id');
				$answer =  $this->input->post('answer');
				$question = $this->questions_model->getquestiondetails($q_id);
				
				$data['student'] = $this->braintest_model->getstudent($studentId);
				$set_id= $data['student']->set_id;
		
				$oldValue=$this->braintest_model->get_answer($studentId,$q_id);
				
				if(isset($oldValue->attempet) && ($oldValue->attempet!=2))
				{
					$form_values['attempet'] =2;
				}
				else
				{
					if($answer!="")
					{
						$form_values['attempet'] = 3;
					}
					else
					{
						$form_values['attempet'] =1;

					}					

				}
				$form_values['answer'] =$answer ;


				if($answer!="")
				{
					if($answer==$question->question_answer)
					{
						$form_values['correct'] =1 ;

					}
					else
					{
						$form_values['correct'] =0 ;
					}
	
				}
						
				
				$res = $this->braintest_model->update_answer($form_values,$studentId,$q_id);
				
					if($res)
					{
						echo 1;
					}
					else
					{
						echo 0;
					}
								
			}

		}
	}
	
	
	public function completetest($cat_id=null)
	{
		if($this->checkstudentlogin())
		{
					$studentId= $this->session->userdata('studentId');
					$test_id= $this->session->userdata('brainTestId');
				
					$currentLog = $this->braintest_model->getLatestLogOfstudent($studentId);
					/*echo "currentlog";
					print_r($currentLog);*/
					if($currentLog!="")
					{
					
						$category=$this->braintest_model->getCategoryById($currentLog->cat_id);
			 
						$data['category']="";
						
						if(!$category=="")
						{ 
							$data['category']=$category[0]->category_name;
						}
						
						$logData['end_time']=date("Y-m-d H:i:s");
						$this->braintest_model->updateLogOfstudent($currentLog->log_id,$logData);
						
						/* check if all test completed */
						$allLog=  $this->braintest_model->getCompletedTestLog(array('student_id'=>$studentId));
						$categories = $this->getAllCategoryWithComplition();
					
						if(count($allLog)==count($categories))
						{
							$data['student'] = $this->braintest_model->getstudent($studentId);

							if($data['student']->test_status==2)
							{
					
								$form_values['test_status'] = 3;
								$res = $this->braintest_model->updatestudent($form_values,$studentId);
								
							}
							
							redirect(base_url()."braintest/result"); 

						}
						else
						{
							$messagedata = array('success_message'  => $category[0]->category_name.' section completed successfully.');
							$this->session->set_userdata($messagedata);

							redirect(base_url()."braintest/dashboard/1");
						}
					}
					else
						{
						redirect(base_url()."braintest/dashboard/1");
						}
					
					

					
		}
	}
	
	public function result()
	{
		$studentId= $this->session->userdata('studentId');
		$test_id= $this->session->userdata('brainTestId');
             
        $data['tresult'] = $this->braintest_model->gettestresult($studentId,$test_id);
        $data['testdetails'] = $this->braintest_model->getTestDetails($test_id);
             
             $this->template->exam_theme('testresult', $data);
	}
	
	public function checkstudentlogin()
	{
	
	
          $uid =$this->session->userdata('studentId');
		 
			if(!empty($uid))
			{
				return true;
			}
			else
			{
				redirect(base_url()."braintest");	
			}
	}
	
	public function check_valid_test_start_time()
	{
	
	
          $uid =$this->session->userdata('studentId');
		  $test_id= $this->session->userdata('brainTestId');
		  $testdetails= $this->braintest_model->getTestDetails($test_id);	
		 $testtimetxt=$testdetails->test_date." ".$testdetails->test_time;
		
		   $testtime=strtotime($testtimetxt);
		  
		 
			if($testtime-time()>0)
			{
				return false;	

			}
			else
			{
				return true;

			}
	}
	
	public function check_valid_test_time($test_id)
	{
	
	
		$testdetails= $this->braintest_model->getTestDetails($test_id);	
		$testtimetxt=$testdetails->test_date." ".$testdetails->test_time;
		$categories = $this->braintest_model->getAllCategoryByTestId($test_id);

		$testtime=strtotime($testtimetxt);

		$max_test_duration=$testdetails->duration+($testdetails->break_time*(count($categories)-1))+30;

		$totaltime= $testtime+($max_test_duration*60);


		if($totaltime-time()>0)
		{
			return true;	

		}
		else
		{
			return false;

		}
	}
	
	public function time_left_for_currentCategory()
	{
		      
					$studentId =$this->session->userdata('studentId');
					$test_id= $this->session->userdata('brainTestId');
		  
					$testdetails= $this->braintest_model->getTestDetails($test_id);	
					$total_category= $this->session->userdata('total_category');
					$currentLog = $this->braintest_model->getLatestLogOfstudent($studentId);
 
					$startTime=$currentLog->start_time;
					$startTimestamp=strtotime($startTime);
					
					$effectiveTotalTime= $testdetails->duration - $this->late_time_for_candidate();
					if($effectiveTotalTime>0)
					{
						$endTime= round($effectiveTotalTime/$total_category);
						$endTimestamp=$startTimestamp+($endTime*60);
					
						$timeleft=$endTimestamp-time();
					
						return $timeleft;
					}
					else
						return 0;
					
					
	}
	
	public function late_time_for_candidate()
	{
					$timeDeduction= $this->session->userdata('timeDeduction');
					
					if($timeDeduction!="")
					{
						return $timeDeduction;
					}
					else
					{
							$studentId =$this->session->userdata('studentId');
							$test_id= $this->session->userdata('brainTestId');
				  
							$testdetails= $this->braintest_model->getTestDetails($test_id);	
							
							$firstLog = $this->braintest_model->getFirstLogOfstudent($studentId);
							if($firstLog!="")
							{
								$start_time=strtotime($firstLog->start_time);
								
								$testtimetxt=$testdetails->test_date." ".$testdetails->test_time;
				
								$testtime=strtotime($testtimetxt);
								
								$lateInMint=ceil($start_time - $testtime)/60;

								if($lateInMint>30)
								{
									$timeDeduction = $lateInMint - 30;
								}
								else
								{
									$timeDeduction =0;
								}						
								

							}
							else
							{
									$timeDeduction =0;
							}
							$this->session->set_userdata(array("timeDeduction"=>$timeDeduction));
							return $timeDeduction;

					}

					
	}
	
	
	public function break_time_for_test()
	{
		      
					$studentId =$this->session->userdata('studentId');
					$test_id= $this->session->userdata('brainTestId');
		  
					$testdetails= $this->braintest_model->getTestDetails($test_id);	
					$total_category= $this->session->userdata('total_category');
					
					$lastLog = $this->braintest_model->getLastLogOfstudent($studentId);
					if($lastLog!="")
					{
						$endTime=$lastLog->end_time;
						$endTime=strtotime($endTime);
						$brekTime= ($testdetails->break_time)*60;

						$endTimestamp=$endTime+$brekTime;
						
						$timeleft=$endTimestamp-time();
						
						return $timeleft;
					}
					else
					{
						return 0;
					}
					
	}
	public function logout()
	{
			$user_data = $this->session->all_userdata();
		
			foreach ($user_data as $key => $value) 
			{

				if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
				}
			}
			redirect(base_url()."braintest");
	}
	
	function find_in_array($array, $key, $val) 
	{
	//echo $key."---".$val;
	//print_r($array);
    foreach ($array as $item)
        if (isset($item->$key) && $item->$key == $val)
            return true;
    return false;
	}
	
	public function getAllCategoryWithComplition()
	{
		$studentId =$this->session->userdata('studentId');
		$test_id= $this->session->userdata('brainTestId');
		$data['student'] = $this->braintest_model->getstudent($studentId);
		$set_id= $data['student']->set_id;
			
		$categories = $this->braintest_model->getAllCategoryByTestId($test_id,$set_id);
			$completedCatgory=$this->braintest_model->getCompletedTestLog(array("student_id"=>$studentId));
			
			foreach($categories  as $catKey => $cat)
			{
					if($completedCatgory!="" && $this->find_in_array($completedCatgory, 'cat_id', $cat->id))
					{
						$categories[$catKey]->complete=1;
					}
					else
						$categories[$catKey]->complete=0;
			}
		return 	$categories;
		
	}
	
	public function check_valid_category($catid)
	{
	
	
         $studentId= $this->session->userdata('studentId');
		 $test_id= $this->session->userdata('brainTestId');
			
		$log	= $this->braintest_model->getStudentLog(array('cat_id'=>$catid,'student_id'=>$studentId));
		if(isset($log[0]->start_time))
		{
			if(isset($log[0]->start_time) && $log[0]->start_time!="0000-00-00 00:00:00" && $log[0]->end_time!="0000-00-00 00:00:00")
			{
				$messagedata = array('info_message'  => 'This section already completed.');

				$this->session->set_userdata($messagedata);

				redirect(base_url()."braintest/dashboard");

			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
		
	}
	
	public function _rules2() {

        $rules = array(

                array('field' => 'name','label' => lang('student_name'),'rules' => 'trim|xss_clean|max_length[250]'),
				array('field' => 'email','label' => lang('student_email'),'rules' => 'trim|xss_clean|valid_email'),
				array('field' => 'phone','label' => lang('student_phone'),'rules' => 'trim|xss_clean|min_length[10]|integer')
                 );

        return $rules;

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */