<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MY_Controller {
        public function __construct() {
           parent::__construct();
           $this->load->library(array('template','form_validation'));
           date_default_timezone_set('Asia/Kolkata');          
           $this->load->model(array('onlinetest_model','common_model'));                      
           $this->load->helper('custom');
        if(!$this->session->userdata('user_login') == '2'){
            redirect(base_url()."onlinetest/login");
        }
        }
        public function paymentProcess(){

        

      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
      $this->session->set_userdata('txnid',$txnid);
      $key = '8dXwhJQa';

      $amount = '500';
      $firstname = $this->session->userdata('examusername');
      $productinfo = 'Online Exam Module';
      $email = $this->session->userdata('examuseremail');
      $phone = $this->session->userdata('examusermobile');
      $salt = 'VMSjFMzE5t';

       $data['details'] = array ( 'key' => $key, 'txnid' => $txnid, 'amount' => $amount,
      'firstname' => $firstname, 'email' => $email, 'phone' => $phone,
      'productinfo' => $productinfo,'surl'=>base_url().'onlinetest/payment/sucess','furl'=>base_url().'onlinetest/payment/failure');
       $hash_string = "$key|$txnid|$amount|$firstname|$productinfo|$email|||||||||||$salt";
      
      $data['hash'] = hash( 'sha512', $hash_string);
      $data['txnid'] = $txnid;
      $data['amount'] = $amount;
      $this->load->view('paymentProcess', $data);
       //$this->load->view('paymentform', $data);
      


  
       
        }
        public function sucess(){

//echo "<pre>"; print_r($_POST);

          $status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="VMSjFMzE5t";
 
If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
 else {   
 
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
 
         }
 $hash = hash("sha512", $retHashSeq);
 
       if ($hash != $posted_hash) {
        echo "Invalid Transaction. Please try again";
    }
    else {



      $updatedata['userid'] = $this->session->userdata('studentId');
      $updatedata['pkgid'] = '1';
      $updatedata['orderid'] = $txnid;
      $updatedata['orderinfo'] = '';
      $updatedata['status']  ='y';

       $this->common_model->insertSingle('gk_paymentusers',$updatedata);
       $data['status'] = 'Success';
       $data['txnid']  = $txnid;
              
         $this->template->theme('paymentfinal',$data);
           
    }         


        }
        public function failure(){
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
 
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="VMSjFMzE5t";
 
If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
 else {   
 
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
 
         }
 $hash = hash("sha512", $retHashSeq);
  
       if ($hash != $posted_hash) {
        echo "Invalid Transaction. Please try again";
    }
    else {
 
         // echo "<h3>Your order status is ". $status .".</h3>";
         // echo "<h4>Your transaction id for this transaction is ".$txnid.". You may try making the payment by clicking the link below.</h4>";
          $data['status'] = 'Failure';
       $data['txnid']  = $txnid;
              
         $this->template->theme('paymentfinal',$data);
 } 
        }



}
      ?>