<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class onlinetest extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('template', 'form_validation'));
        date_default_timezone_set('Asia/Kolkata');
        $this->load->helper('download');
        $this->load->model('braintest/braintest_model');
        $this->load->model(array('onlinetest_model', 'common_model'));
        $this->load->helper('custom');
        $this->load->helper('phpass');
        $this->load->helper('sendmail_helper');
        $this->load->library('pagination');
        // $this->load->library('facebook');


    }

    public function userRegistration()
    {
		
        if ($_POST) {
           // echo "<pre>"; print_r($_POST);exit;
            $uname = $this->input->post('uname');
            $mobile = $this->input->post('mobile');
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');

            if ($uname == '' || $mobile == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            if (!preg_match('/^[0-9]{10}+$/', $mobile)) {
                $data['message'] = "Enter Valid Format Mobile ";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


            //check email exit or not 
            $loginDeatils = $this->onlinetest_model->checkmobile($mobile);
			
            if ($loginDeatils) {
                $data['message'] = "Sorry Mobile Number Already Registered With Us";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

            $formdata['examuser_name'] = $uname;
            $formdata['examuser_mobile'] = $mobile;
            $formdata['examuser_email'] = $email;
            $formdata['examuser_cname'] = $cname;
            $formdata['examuser_type'] = 'n';


            $res = $this->common_model->insertSingle('gk_examuserslist', $formdata);
            if ($res) {

                $otp = rand(8956, 2356);
                $msg = "$otp is the OTP for your Online Test Registration.Expires after use.Please do not share with anyone.
                        Team    
                        BRAINWIZ";


                // $msg = str_replace(' ','%20',$msg);
                smssend_new($mobile, $msg);

                $this->session->set_userdata('uid', $res);
                $this->session->set_userdata('otp', $otp);
                $data['message'] = "Successfully Registration Completed! Please Enter OTP And Set Password";
                $data['id'] = "1";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;


            } else {
                $data['message'] = "Technical Issue Occur Please Try Again Once";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        }
    }

    public function regcheckotp()
    {

        if ($_POST) {
            $otp = $this->input->post('otp');
            $password = $this->input->post('cpwd');


            if ($otp == '' || $password == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

            $sotp = $this->session->userdata('otp');
           
            $uid = $this->session->userdata('uid');
            if ($sotp != $otp) {
                $data['message'] = "Please Enter Valid Otp";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            //Convert Password To Security Hash Format
            $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $hashed_password = $hasher->HashPassword($password);
            $userData['examuser_password'] = $hashed_password;


            $updateParams = array('examuser_id' => $uid);
            $memberUpdateResult = $this->common_model->updateRow('gk_examuserslist', $userData, $updateParams);

            if ($memberUpdateResult) {


                $data['message'] = "Your Password Updation done ! Please Login";
                $data['id'] = "1";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;


            } else {
                $data['message'] = "Technical Error Please Try Again Once!";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        } else {
            redirect('Home', 'refresh');
        }
    }

	public function register(){
		$this->template->theme('signup');
	}
	
	
	public function forgot_password(){
		$this->template->theme('forgot_password');
	}
    public function login()
    {
        if ($this->session->userdata('user_login') == '1') {
            redirect(base_url() . "onlinetest/dashboard");
        } else {
            $this->template->theme('login');
        }

    }


    function loginsubmit()
    {
        if ($_POST) {
           
            $loginid = $this->input->post('loginid');
            $password = $this->input->post('pwd');

            if ($loginid == '' || $password == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }
            


            //check email exit or not 
            $loginDeatils = $this->onlinetest_model->checkmobile($loginid);
            if ($loginDeatils) {


                //check password is correct or not
                $password = trim($password);
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                if ($hasher->CheckPassword($password, $loginDeatils->examuser_password)) {


                    $this->session->set_userdata('user_login', '1');
                    $this->session->set_userdata('studentId', $loginDeatils->examuser_id);
                    $this->session->set_userdata('examuseremail', $loginDeatils->examuser_email);
                    $this->session->set_userdata('examusername', $loginDeatils->examuser_name);
                    $this->session->set_userdata('examusermobile', $loginDeatils->examuser_mobile);
                    $this->session->set_userdata('examuser_type', $loginDeatils->examuser_type);
					$this->session->set_userdata('profile_pic', $loginDeatils->profile_pic);
                    $this->session->set_userdata('url', '');

                    $data['message'] = 'Successfully Login Done ';
                    $data['id'] = "1";

                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;


                } else {
                    $data['message'] = "Invalid Password";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }

            } else {
                $data['message'] = "Invalid Mobile/password";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


        }
    }


    public function getotp()
    {

        $this->template->theme('getotp');
    }

    public function generateotp()
    {
        if ($_POST) {

            $loginid = $this->input->post('phone');


            if ($loginid == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

            if (!preg_match('/^[0-9]{10}+$/', $loginid)) {
                $data['message'] = "Enter Valid Format Mobile ";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            } else {
                //check email was exit or not in database
                $res = $this->onlinetest_model->checkmobile($loginid);
                if (!$res) {
                    $data['message'] = " You are not registered with us. Contact with Management!";
                    $data['id'] = "0";
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    exit;
                }
            }


            $otp = rand(8956, 2356);

            //prepare user table data
            $otpData['mobile_number'] = $loginid;
            $otpData['otp'] = $otp;
            $otpData['send_to'] = 'mobile';
            $otpData['type'] = 'reg';
            $otpData['otp_from'] = 'web';
            $otpId = $this->common_model->insertSingle('gk_temotps', $otpData);
            if ($otpId) {


                $msg = "$otp is the OTP for your Online Test Registration.Expires after use.Please do not share with anyone.
                        Team    
                        BRAINWIZ";
                $this->session->set_userdata('uid', $res->examuser_id);
                $this->session->set_userdata('otp', $otp);
                smssend_new($loginid, $msg);


                if ($res->examuser_email != '') {

                    $fromemail = 'gobrainwiz@gmail.com';
                    $subject = 'OTP';


                    $to = $res->examuser_email;
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "From: $fromemail" . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                    $this_mail = mail($to, $subject, $msg, $headers);

                }


                $data['message'] = "Otp Sent Requested Mobile Number AND  Registered Email ID";
                $data['id'] = "1";

                header('Content-Type: application/json');
                echo json_encode($data);
                exit;


            } else {
                $data['message'] = "Technical Error Please Try Again Once!";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        } else {
            redirect('Home', 'refresh');
        }
    }

    public function checkotp()
    {

        if ($_POST) {
            //echo "<pre>"; print_r($_POST);exit;

            $otp = $this->input->post('otp');
            $password = $this->input->post('password');
            $password_again = $this->input->post('password_again');
            $mobile = $this->input->post('mobile');


            if ($otp == '' || $password == '' || $password_again == '' || $mobile == '') {
                $data['message'] = "Please Fill Required Details";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


            //check email was exit or not in database
            $res = $this->onlinetest_model->checkotp($mobile, $otp);
            if (!$res) {
                $data['message'] = "Please Enter Valid Otp";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }


            //Convert Password To Security Hash Format
            $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
            $hashed_password = $hasher->HashPassword($password);
            $userData['examuser_password'] = $hashed_password;


            $updateParams = array('examuser_mobile' => $mobile);
            $memberUpdateResult = $this->common_model->updateRow('gk_examuserslist', $userData, $updateParams);

            if ($memberUpdateResult) {


                $data['message'] = "Your Password Updation done";
                $data['id'] = "1";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;


            } else {
                $data['message'] = "Technical Error Please Try Again Once!";
                $data['id'] = "0";
                header('Content-Type: application/json');
                echo json_encode($data);
                exit;
            }

        } else {
            redirect('Home', 'refresh');
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('pages/home');
    }

    function dashboard()
    {
        // if(!$this->session->userdata('user_login') == '2'){
        //     redirect(base_url()."onlinetest/login");
        // }
        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }
        //echo $a;


        $data['completetestscount'] = $a;


        //echo "<pre>"; print_r($data);exit;

        $this->template->theme('newdashboard', $data);
    }

    function testhistory()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }
        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }
        //echo $a;


        $data['completetestscount'] = $a;


        //echo "<pre>"; print_r($data);exit;

        $this->template->theme('testhistory', $data);
    }

    public function scorecard($id)
    {

        $studentId = $this->session->userdata('studentId');
        //$test_id= $this->session->userdata('brainTestId');

        $data['tresult'] = $this->braintest_model->gettestresult($studentId, $id);
        $data['testdetails'] = $this->braintest_model->getTestDetails($id);

        // echo "<pre>"; print_r($data);exit;

        $this->template->exam_theme('testresult', $data);


    }

    public function package()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }
        $data['tests'] = $this->common_model->getResult('gk_braintests');
        $this->template->theme('package', $data);
    }

    public function sendWithdrawRequest()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }

        $maxvalue = getWalletByUser($this->session->userdata('studentId'));
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('wr_requested_amount', 'Amount', 'required');

            if ($this->form_validation->run() == TRUE) {
                if ($maxvalue >= $this->input->post('wr_requested_amount')) {
                    $input = array();
                    $input['wr_requested_amount'] = $this->input->post('wr_requested_amount');
                    $input['wr_user_id'] = $this->session->userdata('studentId');
                    $input['wr_requested_user_description'] = $this->input->post('wr_requested_user_description');
                    $this->onlinetest_model->addWithdrawRequest($input);
                    $this->session->set_userdata('request_message', "Successfully Sent request!");
                    redirect('onlinetest/withdrawrequests');
                } else {
                    $this->session->set_userdata('request_message', "Please enter a valid value!");
                    redirect('onlinetest/sendWithdrawRequest');
                }


            }
        }


        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }

        $data['completetestscount'] = $a;


        $this->template->theme('send_withdraw_request', $data);

    }

    public function withdrawrequests()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }
        $breadcrumbarray = [
            'label' => "Withdraw requests",
            'link' => base_url() . "onlinetest/withdrawrequests",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Withdraw Requests List");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/onlinetest/withdrawrequests/';
        $config["total_rows"] = $this->onlinetest_model->withdrawRequestByUser($this->session->userdata('studentId'));
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["records"] = $this->onlinetest_model->getwithdrawrequestsByUser($this->session->userdata('studentId'), $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }

        $data['completetestscount'] = $a;


        $this->template->theme('withdraw_request_list', $data);
    }

    public function userWallet()
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }

        $id = $this->session->userdata('studentId');

        $breadcrumbarray = [
            'label' => "User Wallet",
            'link' => base_url() . "onlinetest/userWallet/",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("User Wallet");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/onlinetest/userWallet/';
        $config["total_rows"] = $this->onlinetest_model->userWalletCountByUser($id);
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($config["total_rows"] > 0) {
            $data["wallet"] = $this->onlinetest_model->getWalletByUser($id, $config["per_page"], $page);
        } else {
            $data["wallet"] = array();
        }
        $data["links"] = $this->pagination->create_links();

        $data['tests'] = $this->onlinetest_model->getonlinetests();


        if ($data['tests']) {
            $data['tcount'] = count($data['tests'], COUNT_RECURSIVE);
        } else {
            $data['tcount'] = 0;
        }
        $studentId = $this->session->userdata('studentId');


        if ($data['tests'] != '') {
            $a = '0';
            foreach ($data['tests'] as $value) {
                $data['completetests'][$value->brain_test_id] = $this->onlinetest_model->getcompletetdtests($value->brain_test_id, $studentId);
                if ($data['completetests'][$value->brain_test_id] != '') {
                    $a += 1;
                }
            }
        }

        $data['completetestscount'] = $a;


        $this->template->theme('user_wallet', $data);
    }

}