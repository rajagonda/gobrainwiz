(function($) {
    'use strict'; // Start of use strict
    /*------------------------------------------------------------------
        Header Navigation
    ------------------------------------------------------------------*/
    var windowSize = $(window).width();

    if (windowSize >= 767) {
        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    }
    /*------------------------------------------------------------------
    	Scrool Top
    ------------------------------------------------------------------*/
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });
    /*------------------------------------------------------------------
    	Header Search
    ------------------------------------------------------------------*/
    $("#search-form").hide();
    $(".fa-search").on('click', function() {
        $("#search-form").toggle();
        $("#search-form").fadeIn().addClass("open");
    });
    $("#search-form .close").on('click', function() {
        $("#search-form").fadeOut().removeClass("open");
        $("#this").hide();
    });
    /*------------------------------------------------------------------
           Get a Free Quote
    ------------------------------------------------------------------*/
    $('.quote').find('a').on('click', function() {
        $('.quote-popup').show();
    });
    $('.quote-popup').find('.fa-times').on('click', function() {
        $('.quote-popup').hide();
    });
    /*------------------------------------------------------------------
        Loader
    ------------------------------------------------------------------*/
    $(window).on("load scroll", function() {
        $("#dvLoading").fadeOut("fast");
    });
    /*------------------------------------------------------------------
       Projects tabs
    ------------------------------------------------------------------*/
    $('.tabs .tab-links a').on('click', function(e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    /*------------------------------------------------------------------
        Animation Numbers
    ------------------------------------------------------------------*/
    $('.animateNumber').each(function() {
        var num = jQuery(this).attr('data-num');

        var top = jQuery(document).scrollTop() + (jQuery(window).height());
        var pos_top = jQuery(this).offset().top;
        if (top > pos_top && !jQuery(this).hasClass('active')) {
            jQuery(this).addClass('active').animateNumber({
                number: num
            }, 2000);
        }
    });
    /*------------------------------------------------------------------
        Owl Carousel for Banner
    ------------------------------------------------------------------*/
    $("#banner").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: true,
        autoPlay: true,
        stopOnHover: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    /*------------------------------------------------------------------
        Owl Carousel for Blog
    ------------------------------------------------------------------*/
    $("#blog").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: true,
        autoPlay: true,
        stopOnHover: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    /*------------------------------------------------------------------
        Owl Carousel for Testimonials
    ------------------------------------------------------------------*/
    $("#testimonials").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: true,
        autoPlay: true,
        stopOnHover: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    /*------------------------------------------------------------------
        Owl Carousel for Partners
    ------------------------------------------------------------------*/
    var owl = $("#partners");
    owl.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 2],
            [600, 3],
            [700, 3],
            [1000, 3],
            [1200, 5],
            [1400, 5],
            [1600, 5]
        ],
        navigation: true
    });
    /*------------------------------------------------------------------
        Owl Carousel for About Us
    ------------------------------------------------------------------*/
    $("#about").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        pagination: true,
        autoPlay: true,
        stopOnHover: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    /*------------------------------------------------------------------
    Count Down
    ------------------------------------------------------------------*/
    if ($(".count-down").length) {
        var year = parseInt($(".count-down").attr("data-countdown-year"), 10);
        var month = parseInt($(".count-down").attr("data-countdown-month"), 3) - 1;
        var day = parseInt($(".count-down").attr("data-countdown-day"), 10);
        $(".count-down").countdown({
            until: new Date(year, month, day),
            padZeroes: true
        });
    }
})(jQuery);