<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Dashboard
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class dashboard extends  MY_Controller {

	public function __construct()
	{

		parent::__construct();
		 if(!$this->authentication->checklogin()){
          redirect('login');
        }
		$this->load->library('template');
        $this->template->set_title('Welcome');
	}

	public function index()
	{
		  $this->template->load_view1('ac/dashboard');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */