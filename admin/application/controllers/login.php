<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Subject
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Login extends  MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		$this->load->library('template');
        $this->template->set_title('Welcome');
         $this->load->model(array('login_model','common_model'));
        $this->load->helper('phpass');
	}

	public function index() {
       if($this->session->userdata('appadminuser_id') !=''){
              redirect(base_url().'dashboard');
        }
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('login_form',{$json_rules});
</script>
JS;
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
        if ( ! $user = $this->login_model->get_by_username($this->input->post('user_name', TRUE))) {
         // echo "ok";exit;
                $data['username_email_error'] = lang('sign_in_username_email_does_not_exist');
        } else {

            
               if ( ! $this->authentication->check_password($user->admin_password, $this->input->post('user_password', TRUE))) {
                $data['sign_in_error'] = lang('sign_in_combination_incorrect');
                }  else   {
                //Here implement the user session data
                $userdata = array('user_email'=>$user->admin_email,
                                  'appadminuser_id' => $user->admin_id,
                                  'user_password' => $user->admin_password,
                                  'user_role' => $user->admin_role,
                                  'user_name' => $user->admin_uname                                  
                                  );
                if($user->admin_role == '1'){
                  $this->session->set_userdata($userdata);
                redirect(base_url().'dashboard'); 
                }else {
                   $data['username_email_error'] = lang('sign_in_username_email_does_not_exist');
                }
                
                }
            
                }
            }
        

        $data['script'] = $script;
        $this->load->view('ac/login', $data);
    }
     public function _rules() {
        $rules = array(
                array('field' => 'user_name','label' => lang('user_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'user_password','label' => lang('user_password'),'rules' => 'trim|required|xss_clean|max_length[250]'));
        return $rules;
    }   
    public function logout(){
           $this->authentication->sign_out();
          redirect(base_url().'login');
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */