<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : challagulla venkata sudhakar
* Project : rehargenestjam
 * Company : Renegade it solutions
 * Version v1.0
 * Controller : changepassword
 */
class changepassword extends  MY_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library('template');
        $this->template->set_title('Welcome');
        $this->load->model('changepassword_model');
        $this->load->model(array('common_model'));
        $this->load->helper('phpass');
         
	}

	public function index()	{
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }
      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('changepassword_from',{$json_rules});
</script>
JS;
          
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
        
            $user_id = $this->session->userdata('appadminuser_id');
            $oldpassword = $this->input->post('old_password');
            //check old password correct or not
            $params = array('admin_id'=>$user_id);
            $res = $this->common_model->getSingleRow('gk_adminusers', $params);
          if ( ! $this->authentication->check_password($res->admin_password, $oldpassword, TRUE)) {
			      $data['old_password_error'] = lang('old_password_error');
			    } else   {
    			  $new_password = $this->input->post('new_password');
			      $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
	          $hashed_password = $hasher->HashPassword( $new_password);
	          //update new password
	          $formvalues['admin_password'] = $hashed_password;
	          $result = $this->common_model->updateRow('gk_adminusers', $formvalues,$params);
	          if($result !=''){
              $data['password_change_success'] = lang('password_change_success');
            }else {
              $data['old_password_error'] = lang('old_password_error');
            }
			    }

           

        }


        $data['script'] = $script;
       // print_r($data);exit;
		$this->template->load_view1('ac/changepassword',$data);
	}
	 public function _rules() {
        $rules = array(
                array('field' => 'old_password','label' => lang('old_password'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'new_password','label' => lang('new_password'),'rules' => 'trim|required|xss_clean'),
                array('field' => 'confirm_password','label' => lang('confirm_password'),'rules' => 'trim|required|xss_clean|matches[new_password]'));
        return $rules;
    }


}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */