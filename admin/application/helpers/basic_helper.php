<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('getCouponCodeFromID')) {
    function getCouponCodeFromID($id)
    {

        $ci =& get_instance();
        $ci->load->database();

        $sql = "SELECT * 
    FROM gk_coupons where coupon_id=" . $id;
        $query = $ci->db->query($sql);
        $row = $query->row();
        return $row->coupon_code;
    }

}

if (!function_exists('getWalletByUser')) {
    function getWalletByUser($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id='$id'
           		  ORDER BY uw_id DESC";

        $result = $ci->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }

    }
}

include(FCPATH . 'application/helpers/phpqrcode/qrlib.php');

if (!function_exists('qRCodeCreate')) {

    function qRCodeCreate($text)
    {

//        include('./phpqrcode/qrlib.php');
//       return QRcode::png($text);

//        return FCPATH.'application/helpers/phpqrcode/qrlib.php';




        $fileName = 'code_' . md5($text) . '.png';
        $coupansDir =  '../upload/coupons/';
        $coupansurl = 'http://gobrainwiz.in/upload/coupons/';
        $pngAbsoluteFilePath = $coupansDir . $fileName;
        $urlRelativeFilePath = $coupansurl . $fileName;

        // generating
        if (!file_exists($pngAbsoluteFilePath)) {
            QRcode::png($text, $pngAbsoluteFilePath, QR_ECLEVEL_L, 5);
//            echo 'File generated!';
//            echo '<hr />';
        } else {
//            echo 'File already generated! We can use this cached file to speed up site on common codes!';
//            echo '<hr />';
        }

       return $urlRelativeFilePath;

//        return QRcode::png($text);

//        $barcode = new \Com\Tecnick\Barcode\Barcode();
//        $targetPath = "qr-code/";
//
////        $bobj = $barcode->getBarcodeObj(
////            'QRCODE,H',                     // barcode type and additional comma-separated parameters
////            $text,          // data string to encode
////            -4,                             // bar width (use absolute or negative value as multiplication factor)
////            -4,                             // bar height (use absolute or negative value as multiplication factor)
////            'black',                        // foreground color
////            array(-2, -2, -2, -2)           // padding (use absolute or negative values as multiplication factors)
////        )->setBackgroundColor('white'); // background color
////        return $bobj->getHtmlDiv();
//// output the barcode as HTML div (see other output formats in the documentation and examples)
//
//        $bobj = $barcode->getBarcodeObj('QRCODE,H', $text, -4, -4, 'black', array(-2, -2, -2, -2))->setBackgroundColor('#f0f0f0');
//
//     //return $bobj->getPngData();
//     return $bobj->getSvgCode();
    }
}
if (!function_exists('getwithdrawrequestsByUser')) {
    function getwithdrawrequestsByUser($id)
    {
        $ci =& get_instance();
        $ci->load->database();
        $sql = "SELECT * FROM gk_withdraw_requests where  wr_user_id='$id'
			        ORDER BY wr_id DESC";

        $result = $ci->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return array();
        }

    }

}