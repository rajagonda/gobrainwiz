<header class="header">
    <a href="<?php echo base_url(); ?>dashboard" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->               Admin Area
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button--> <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas"
                                         role="button"> <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->


                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i>
                        <span>Admin<i class="caret"></i></span> </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="<?php echo base_url(); ?>assets_new/img/avatar.png" class="img-circle"
                                 alt="User Image"/>
                            <p> Admin </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center"><a href="#">Followers</a></div>
                            <div class="col-xs-4 text-center"><a href="#">Sales</a></div>
                            <div class="col-xs-4 text-center"><a href="#">Friends</a></div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left"><a href="#" class="btn btn-default btn-flat">Profile</a></div>
                            <div class="pull-right"><a href="<?php echo base_url(); ?>login/logout"
                                                       class="btn btn-default btn-flat">Sign out</a></div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left info">
                    <p>Hello, Admin</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <!--   <form action="#" method="get" class="sidebar-form">                        <div class="input-group">                            <input type="text" name="q" class="form-control" placeholder="Search..."/>                            <span class="input-group-btn">                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>                            </span>                        </div>                    </form> -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less --> <?php $page = $this->uri->segment(1); ?>
            <ul class="sidebar-menu">
                <li class="<?php if ($page == "" || $page == "dashboard") echo "active"; ?>"><a
                            href="<?php echo base_url(); ?>dashboard"> <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span> </a></li>


                <li class="<?php if ($page == "" || $page == "fileUpload") echo "active"; ?>"><a
                            href="<?php echo base_url(); ?>fileUpload">
                        <i class="fa fa-dashboard"></i> <span>Image Upload</span> </a></li>

                <li class="<?php if ($page == "" || $page == "BRAINWIZ students") echo "active"; ?>"><a
                            href="<?php echo base_url(); ?>examusers"><i class="fa fa-angle-double-right"></i>BRAINWIZ
                        students</a></li>
                <li class="<?php if ($page == "" || $page == "Registered students") echo "active"; ?>"><a
                            href="<?php echo base_url(); ?>examusers/register"><i class="fa fa-mobile"></i>Registered
                        students</a></li>

                <li class="<?php if ($page == "Cashback Settings") echo 'active'; ?>"><a
                            href="<?php echo base_url(); ?>examusers/cashback"><i class="fa fa-money"></i>Cashback
                        Settings</a></li>
                <li><a href="<?php echo base_url(); ?>examusers/cashback/notifications"><i
                                class="fa fa-paper-plane"></i>Cashback Notifications</a></li>
                <li class="<?php if ($page == "CheckReferralCode") echo 'active'; ?>"><a
                            href="<?php echo base_url(); ?>examusers/cashback/CheckReferralCode"><i
                                class="fa fa-check"></i>Check Referral Code</a></li>
                <li class="<?php if ($page == "Referral Joinees") echo 'active'; ?>"><a
                            href="<?php echo base_url(); ?>examusers/cashback/referralJoinees"><i
                                class="fa fa-user"></i>Referral Joinees</a></li>


                <li class="treeview <?php if ($page == "practicetest") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Practice test</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>practicetest"><i class="fa fa-angle-double-right"></i>Categories</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>practicetest/topics"><i
                                        class="fa fa-angle-double-right"></i>Topics</a></li>
                        <li><a href="<?php echo base_url(); ?>practicetest/passages"><i
                                        class="fa fa-angle-double-right"></i>Passages</a></li>
                        <li><a href="<?php echo base_url(); ?>practicetest/gettests"><i
                                        class="fa fa-angle-double-right"></i>Manage Tests</a></li>
                        <li><a href="<?php echo base_url(); ?>practicetest/testhistory/testInfo"><i
                                        class="fa fa-angle-double-right"></i>Manage Tests History</a></li>

                    </ul>
                </li>

                <li class="treeview <?php if ($page == "videos") echo "active"; ?>">
                    <a href="#"> <i class="fa  fa-windows"></i> <span>Videos</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>videos/categories"><i
                                        class="fa fa-angle-double-right"></i>Categories</a></li>
                        <li><a href="<?php echo base_url(); ?>videos"><i class="fa fa-angle-double-right"></i>videos</a>
                        </li>
                    </ul>
                </li>


                <li class="treeview <?php if ($page == "subjects") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Batch Timings</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>subjects"><i class="fa fa-angle-double-right"></i>subjects</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>subjects/save"><i class="fa fa-angle-double-right"></i>Add
                                Subject</a></li>
                        <li><a href="<?php echo base_url(); ?>subjects/timings"><i class="fa fa-angle-double-right"></i>Batch
                                Timings</a></li>
                        <li><a href="<?php echo base_url(); ?>subjects/timings/save"><i
                                        class="fa fa-angle-double-right"></i>Add Batch Timing</a></li>
                    </ul>
                </li>


                <li class="treeview <?php if ($page == "jobAlerts") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Notification</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>jobAlerts"><i class="fa fa-angle-double-right"></i>Job
                                Alert</a></li>
                        <li><a href="<?php echo base_url(); ?>batchTimings"><i class="fa fa-angle-double-right"></i>Batch
                                Timings</a></li>


                        
                    </ul>
                </li>


                <li class="treeview <?php if ($page == "braintest") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Online Test Series</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>braintest"><i class="fa fa-angle-double-right"></i>Manage
                                Tests</a></li>
                        <li><a href="<?php echo base_url(); ?>braintest/testhistory"><i
                                        class="fa fa-angle-double-right"></i>Test Series History</a></li>

                    </ul>
                </li>
                <li class="treeview <?php if ($page == "placement") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Elitmus</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>placement/company"><i
                                        class="fa fa-angle-double-right"></i>Companies</a></li>
                        <li><a href="<?php echo base_url(); ?>placement/company/papers"><i
                                        class="fa fa-angle-double-right"></i>Questions</a></li>
                        
                    </ul>
                </li>
                <li class="treeview <?php if ($page == "placement") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Placement Papers</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>placementpapers/company"><i
                                        class="fa fa-angle-double-right"></i>Companies</a></li>
        
                    </ul>
                </li>
                <li class="treeview <?php if ($page == "topper") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-mortar-board"></i> <span>Toppers</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>topper"><i
                                        class="fa fa-angle-double-right"></i>Toppers</a></li>
                        <li><a href="<?php echo base_url(); ?>topper/save"><i class="fa fa-angle-double-right"></i>Add
                                Topper</a></li>
                    </ul>
                </li>
               
                <li class="treeview <?php if ($page == "groups") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-gears"></i> <span>Colleges</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>groups"><i class="fa fa-angle-double-right"></i>All
                                Colleges</a></li>
                        <li><a href="<?php echo base_url(); ?>groups/save"><i class="fa fa-angle-double-right"></i>Add
                                College</a></li>
                    </ul>
                </li>

                <li><a href="<?php echo base_url(); ?>emails/send"> <i class="fa fa-envelope"></i>
                        <span>Send Mails</span> </a></li>
                <!--<li >                        <a href="<?php echo base_url(); ?>resumes">                        <i class="fa fa-edit"></i> <span>Manage Resumes</span>                        </a>                        </li> -->
                <li><a href="<?php echo base_url(); ?>userquestions"> <i class="fa fa-database"></i> <span>Manage User Questions</span>
                    </a></li>

                <li class="treeview <?php if ($page == "aptitude") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-clock-o"></i> <span>Aptitude</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>aptitude"><i class="fa fa-angle-double-right"></i>Categories</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>aptitude/subcategories"><i
                                        class="fa fa-angle-double-right"></i>Sub Categories</a></li>
                        <li><a href="<?php echo base_url(); ?>aptitude/getalltopics"><i
                                        class="fa fa-angle-double-right"></i>Topics</a></li>
                       
                    </ul>
                </li>
                <li class="treeview <?php if ($page == "puzzles") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-puzzle-piece"></i> <span> Puzzles</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>puzzles"><i class="fa fa-angle-double-right"></i>Puzzle
                                Topics</a></li>
                        <li><a href="<?php echo base_url(); ?>puzzles/questions"><i
                                        class="fa fa-angle-double-right"></i>Manage Questions</a></li>
                       
                    </ul>
                </li>


                <li class="treeview <?php if ($page == "pages") echo "active"; ?>">
                    <a href="#"> <i class="fa  fa-windows"></i> <span>Pages</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>pages"><i class="fa fa-angle-double-right"></i>Pages</a>
                        </li>
                    </ul>
                </li>
				
				<li class="treeview <?php if ($page == "blog") echo "active"; ?>">
                    <a href="#"> <i class="fa  fa-wordpress"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
						<li><a href="<?php echo base_url(); ?>blog/categories"><i class="fa fa-angle-double-right"></i>Categoies</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>blog/posts"><i class="fa fa-angle-double-right"></i>Posts</a>
                        </li>
                    </ul>
                </li>
				<li class="treeview <?php if ($page == "companies") echo "active"; ?>">
                    <a href="#"> <i class="fa  fa-building"></i> <span>Companies Placed</span> <i class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
						<li><a href="<?php echo base_url(); ?>companies"><i class="fa fa-angle-double-right"></i>Companies</a>
                        </li>
                        
                    </ul>
                </li>
				
          

                <li class="treeview <?php if ($page == "settings") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-cog"></i> <span>Settings</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>settings/banners"><i class="fa fa-angle-double-right"></i>
                                Banners</a></li>
                        <li><a href="<?php echo base_url(); ?>settings"><i class="fa fa-angle-double-right"></i>Site
                                Settings</a></li>
                        <li><a href="<?php echo base_url(); ?>settings/seoupdate"><i
                                        class="fa fa-angle-double-right"></i>Seo Settings</a></li>
                        <li><a href="<?php echo base_url(); ?>updates"><i class="fa fa-angle-double-right"></i>Latest
                                Updates</a></li>
                        <li><a href="<?php echo base_url(); ?>weekly"><i class="fa fa-angle-double-right"></i>Weekly
                                Schedule</a></li>
                    </ul>
                </li>

                <li class="<?php if ($page == "changepassword") echo "active"; ?>">
                    <a href="<?php echo base_url(); ?>changepassword">
                        <i class="fa fa-unlock-alt"></i> <span>Change Password</span>
                    </a>
                </li>


                <li class="treeview <?php if ($page == "coupons") echo "active"; ?>">
                    <a href="#"> <i class="fa fa-cog"></i> <span>Coupons</span> <i
                                class="fa fa-angle-left pull-right"></i> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>coupons"><i class="fa fa-angle-double-right"></i> Coupons</a>
                        <li><a href="<?php echo base_url(); ?>coupons/assignCouponsToUsers"><i
                                        class="fa fa-angle-double-right"></i> Assign Coupons To Users</a>
                        <li><a href="<?php echo base_url(); ?>coupons/couponUseages"><i
                                        class="fa fa-angle-double-right"></i> Coupon Useages</a>
                        </li>
                        <li><a href="<?php echo base_url(); ?>coupons/withdrawrequests"><i
                                        class="fa fa-angle-double-right"></i>Withdraw Requests</a>
                        </li>
                    </ul>
                </li>


                <li><a href="<?php echo base_url(); ?>login/logout"> <i class="fa fa-power-off"></i> <span>Logout</span>
                    </a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> Dashboard
                <small>                    <?php echo $subpagetitle; ?>                </small>
            </h1>
            <?php echo $breadcrumb; ?>
        </section>
        <!-- Main content -->
        <section class="content">

