<div class="row">
            <div class="col-md-10 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-10">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('change_password');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                  <?php
         if ( isset($old_password_error)) { 
        ?>
        <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <b>Alert!</b> 
        <?php if(isset($old_password_error)) {
            echo $old_password_error; 
            }?>
        </div>
        <?php } ?>
                                     <?php
         if ( isset($password_change_success)) { 
        ?>
        <div class="alert alert-success alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <b>OK!</b> 
        <?php
        if(isset($password_change_success)) {
            echo $password_change_success; 
            }?>
        </div>
        <?php } ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="changepassword_from" id="changepassword_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="old_password"><?php echo lang('old_password'); ?></label>
                                            <input type="text" placeholder="<?php echo lang('old_password'); ?>" name="old_password" id="old_password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('new_password'); ?></label>
                                            <input type="text"  placeholder="<?php echo lang('new_password'); ?>" id="new_password" name="new_password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('confirm_password'); ?></label>
                                            <input type="text"  placeholder="<?php echo lang('confirm_password'); ?>" id="confirm_password" name="confirm_password" class="form-control">
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
