<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author  : Challagulla venkata sudhakar
 * Project : App Admin 
 * Company : Renegade It Solutions
 * Version v1.0
 * Model : login_model
 * Mail id : phpguidance@gmail.com
  */
class login_model extends CI_Model {	
	
   
	
	public function get_by_username($username) {
		$this->db->where('admin_uname', $username);
		$this->db->where('admin_role', '1');
		$query = $this->db->get('gk_adminusers');
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }
	}
	public function getUserGroup($user_id) {
		$sql = "SELECT * FROM rc_users WHERE user_id='$user_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }
	}

	public function getPermissions($group_id) {
	    $sql = "SELECT * FROM rc_group_permissions WHERE group_id='$group_id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
         return $query->result();
        }else {
         return '';
      }	
	}
	public function checkPermission($module_id,$roleid, $permission_id) {
		//$sql = "SELECT * FROM rc_group_permissions WHERE group_id='$group_id' AND permission_id IN ($permission_id)";
		$this->db->where('module_id',$module_id);
		$this->db->where('role_id',$roleid);
		$this->db->where_in('permission_id',$permission_id);
		$query = $this->db->get(DB_PREFIX.'role_permissions');
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }	
	}	
	public function checkOldpassworsd($uid){
        $this->db->where('user_id', $uid);
        $query = $this->db->get('gk_users');
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0) {
         return $query->row();
        }else {
         return '';
      }

	}
	public function updatepassword($uid, $data){
		$this->db->where('user_id', $uid);
	    $this->db->update('gk_users', $data);
	    return $this->db->affected_rows();
	}


}


/* End of file common_model.php */
/* Location: ./application/models/common_model.php */