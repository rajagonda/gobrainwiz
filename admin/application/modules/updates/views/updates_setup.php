<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('update_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="updates_from" id="updates_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('update_title'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $update->update_title; ?>" placeholder="<?php echo lang('update_title'); ?>" name="update_title" id="update_title" class="form-control">
                                        </div>
                                      
                                         <div class="form-group">
                                              <?php if($action=='edit'){ ?>
                                            <?php if($update->update_image !=''){   ?>
                                    <img src="http://gobrainwiz.in/upload/updates/<?php echo $update->update_image;?>" height="90" width="75" class="pull-right img-rounded" alt=""/>
                                    <?php }else { ?>
                                    <img src="http://gobrainwiz.in/upload/updates/images.jpg" class="pull-right img-rounded" alt=""/>
                                     <?php  } ?>
                                            <?php } ?>
                                            <label for="exampleInputFile"><?php echo lang('update_image'); ?></label>
                                            <input type="file" name="update_image" id="update_image">
                                           
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
