<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Toppers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Toppers
|--------------------------------------------------------------------------
*/


$lang['manage_updates'] = 'Manage Updates';
$lang['update_add'] = 'Add Update';
$lang['update_edit'] = "Edit Update";
$lang['update_title'] = "Update Title";
$lang['update_image'] = "Image";


/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
