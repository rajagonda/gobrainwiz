<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : updates
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class updates extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('updates_Model');
        $this->load->language('updates');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "updates",
                            'link' => base_url()."updates"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage updates");
           $data['updates'] = $this->updates_Model->get();
           $this->template->load_view1('updates', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "updates",
                           'link' => base_url()."updates"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('updates_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['update'] = $this->updates_Model->gettopperDetails($id);
            $this->template->set_subpagetitle("Edit updates");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add updates");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          //echo "<pre>"; var_dump($this->form_validation->run());exit;
          if ($this->form_validation->run() == false)  {
          
            $form_values['update_title'] = $this->input->post('update_title');            
            
            
            if($id !=''){
            if($_FILES['update_image']['name'] !='') {
            
            $filename = time()."_".$_FILES['update_image']['name'];
            $config['upload_path'] = '../upload/updates/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('update_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
             $form_values['update_image'] = $update_image;
           }
          }else {
             if($_FILES['update_image']['name'] !='') {
            
            $filename = time()."_".$_FILES['update_image']['name'];
            $config['upload_path'] = '../upload/updates/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('update_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
          }else {
            $update_image = '';
            }
             $form_values['update_image'] = $update_image;
          }
          //echo "<pre>"; print_r($form_values);exit;
            if($id !=''){
             $res = $this->updates_Model->update($form_values,$id);
            }else {
              $res = $this->updates_Model->save($form_values);

            }
             redirect('updates');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('updates_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'update_title','label' => lang('update_title'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'update_image','label' => lang('update_image'),'rules' => 'required')
                );
        return $rules;
    }

	 public function changetopperStatus(){
        $id = $_POST['update_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['update_status'] = 'n';
            echo "0";
        } else {
            $formvalues['update_status'] = 'y';
            echo "1";
        }
        $this->Topper_Model->changetopperStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->updates_Model->deletetopper($id);

    }

}
