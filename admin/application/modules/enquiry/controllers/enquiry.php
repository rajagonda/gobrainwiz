<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : enquiry
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Enquiry extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('Enquiry_Model');
        $this->load->language('enquiry');
        $this->load->library('pagination');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Enquiry",
                            'link' => base_url()."enquiry"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Enquiry");
         
           $total_rows = $this->Enquiry_Model->getEnquirycount();
           if($total_rows == ''){
           $total_rows = 0;
           }
           
        $config['base_url'] = base_url().'enquiry';
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 30;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active"> <a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['enquiry'] = $this->Enquiry_Model->get($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('enquiry', $data);
	}

	

	 public function changeenquiryStatus(){
        $id = $_POST['enquiry_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['enquiry_status'] = 'n';
            echo "0";
        } else {
            $formvalues['enquiry_status'] = 'y';
            echo "1";
        }
        $this->Enquiry_Model->changeenquiryStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Enquiry_Model->deleteenquiry($id);

    }

}
