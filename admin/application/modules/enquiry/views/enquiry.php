<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_enquiry');?></h3>
                            
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th ><?php echo lang('enquiry_name');?></th>
                                        <th ><?php echo lang('enquiry_email');?></th>
                                        <th ><?php echo lang('enquiry_mobile');?></th>
                                        <th>Date</th>                                        
                                        <th ><?php echo lang('enquiry_subject');?></th>
                                        <th ><?php echo lang('enquiry_message');?></th>
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        </th>
                                        </tr>
                                      <?php
                                      if($enquiry !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($enquiry as  $value) {
                                      ?>
                                        <tr id='enquiry_<?php echo $value->enquiry_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->enquiry_name;?></td>
                                        <td><?php echo $value->enquiry_email;?></td>
                                        <td><?php echo $value->enquiry_mobile;?></td>
                                          <td><?php echo $value->enquiry_date;?></td>
                                        
                                        <td><?php echo $value->enquiry_subject;?></td>
                                        <td><?php echo $value->enquiry_message;?></td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->enquiry_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->enquiry_id;?>', '<?php echo $value->enquiry_status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->enquiry_id;?>', '<?php echo $value->enquiry_status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                       <a class="red trash" id="<?php echo $value->enquiry_id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                 <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <?php echo $links; ?>
                                    </ul>
                                </div>
                            </div> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>enquiry/delete',
          success: function(msg) {
            $("#enquiry_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id, enquiry_id,  status) { 
     var p_url= "<?php echo base_url(); ?>enquiry/changeenquiryStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'enquiry_id='+enquiry_id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id).html('<a class="green" href="#" onclick="changeStatus(\''+id+'\', \''+enquiry_id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id).html('<a class="red" href="#" onclick="changeStatus(\''+id+'\', \''+enquiry_id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>