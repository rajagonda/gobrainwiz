<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Enquiry
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Enquiry_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
        public function getEnquirycount(){
		$query = $this->db->get(DB_PREFIX.'enquiry');
		if($query->num_rows()>0){
			return $query->num_rows();
		}else {
			return '';
		}

	}
        
	public function get($limit=NULL, $start=NULL){
              

		$sql = "SELECT * FROM ".DB_PREFIX."enquiry ORDER BY enquiry_id DESC LIMIT $start, $limit";
        $query  =   $this->db->query($sql);                
        if($query->num_rows() > 0) {
          return $query->result();
        } else {
         return '';
        }

		
	}
	public function changeenquiryStatus($data, $id){
	    $this->db->where('enquiry_id', $id);
	    $this->db->update(DB_PREFIX.'enquiry', $data);
            return $this->db->affected_rows();
	}
	public function deleteenquiry($id){
		$this->db->where('enquiry_id', $id);
        $this->db->delete(DB_PREFIX.'enquiry');
	}


}

/* End of file enquiry_model.php */
/* Location: ./application/models/enquiry_model.php */