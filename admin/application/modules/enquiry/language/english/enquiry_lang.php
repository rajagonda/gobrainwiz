<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : enquiry
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Enquiry
|--------------------------------------------------------------------------
*/


$lang['manage_enquiry'] = 'Manage Enquiry';
$lang['enquiry_name'] = "Name";
$lang['enquiry_email'] = "Email";
$lang['enquiry_mobile'] = "Mobile";
$lang['enquiry_subject'] = "Subject";
$lang['enquiry_message'] = "Message";

/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
