  
<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="" role="form" name="question_from" id="question_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php 									if($action=='edit') 										echo lang('question_edit');									else										echo lang('question_add');																		?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        
                                      
                                        <!-- <div class="form-group">
                                            <label>Question</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_name" id="question_name" class="form-control"><?php if($action=='edit') echo $details->question_name; ?></textarea>
                                        </div> -->
                                        <div class="form-group">
                                            <label>Question</label>
                                            <textarea id="example" class="wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" name="question_name"><?php if($action=='edit'){ echo $details->question_name;} else {echo "<br>";} ?>
                            </textarea>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 1</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option1" id="example1" class="form-control"><?php if($action=='edit'){ echo $details->option1;} else { echo "<br>";} ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 2</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option2" id="example2" class="form-control"><?php if($action=='edit'){ echo $details->option2; } else { echo "<br>";} ?></textarea>
                                        </div>
                                       
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php 									if($action=='edit') 										echo lang('question_edit');									else										echo lang('question_add');									?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                               
                                    <div class="box-body">
                                        
                                        <div class="form-group">                                            <label>Choice 3</label>                                            <textarea placeholder="Enter ..." rows="3" name="option3" id="example3" class="form-control"><?php if($action=='edit'){ echo $details->option3; } else { echo "<br>";} ?></textarea>                                        </div>
                                        <div class="form-group">
                                            <label>Choice 4</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option4" id="example4" class="form-control"><?php if($action=='edit'){ echo $details->option4; } else { echo "<br>";} ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 5</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option5" id="example5" class="form-control"><?php if($action=='edit'){ echo $details->option5; } else { echo "<br>";} ?></textarea>
                                        </div>	
                                        <div class="form-group">
                                            <label>Explanation</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_explanation" id="example6" class="form-control"><?php if($action=='edit'){ echo $details->question_explanation; } else { echo "<br>";} ?></textarea>
                                        </div>  									
                                      <!--   <div class="form-group">
                                            <label>Explanation</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_explanation" id="example6" class="form-control"><?php if($action=='edit') echo $details->question_explanation; ?></textarea>
                                        </div>	 -->
                                         <div class="form-group">
                                            <label>Video Explanation</label>
                                            <textarea placeholder="Enter ..." rows="3" name="video_link" id="video_link" class="form-control"><?php if($action=='edit') echo $details->video_link; ?></textarea>
                                        </div>  									
                                        <div class="form-group">                                            <label>Categories</label>											 <select name="cat_id" id="cat_id"  class="form-control">												<option value="">Select Category</option>																								<?php																									foreach($categories as $key =>$cat)													{													?>														<option value="<?php echo $cat->id?>" <?php if($action=='edit' && $details->cat_id==$cat->id) echo "selected"; ?>><?php echo $cat->category_name?></option>																										<?php													}												?>											  											 </select>           											<div class="clear"></div>										</div> 



                                        <div class="form-group">
                                            <label>Answer</label>
                         <select name="question_answer" id="question_answer"  class="form-control">
                            <option value="">Select Answer</option>
                            <option value="1" <?php if($action=='edit' && $details->question_answer==1) echo "selected"; ?>>Choice 1</option>
                            <option value="2" <?php if($action=='edit' && $details->question_answer==2) echo "selected"; ?>>Choice 2</option>
                            <option value="3" <?php if($action=='edit' && $details->question_answer==3) echo "selected"; ?> >Choice 3</option>
                            <option value="4" <?php if($action=='edit' && $details->question_answer==4) echo "selected"; ?> >Choice 4</option>
                            <option value="5" <?php if($action=='edit' && $details->question_answer==5) echo "selected"; ?> >Choice 5</option>
                         </select>           
                                
                        <div class="clear"></div>
                    </div> 
                                       
                                    </div><!-- /.box-body -->

                        <div class="box-footer">
                        <input type="hidden" name="set_id" id="set_id" value="<?php echo $questionset[0]->set_id; ?>">
                        <input type="hidden" name="testid" id="testid" value="<?php echo $questionset[0]->test_id ?>">

                      
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
<?php

echo $script;
?>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/tinymce4/js/tinymce/tinymce.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd5.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd4.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd3.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd2.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd1.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/wirislib.js"></script>
 <script type="text/javascript">
     

    



            $(function() {


                //  CKEDITOR.replace( 'question_name', {
                // extraPlugins: 'mathjax'
                // });

                //   CKEDITOR.replace( 'option1', {
                // extraPlugins: 'mathjax'
                // });
                
                //   CKEDITOR.replace( 'option2', {
                // extraPlugins: 'mathjax'
                // });
                
                //   CKEDITOR.replace( 'option3', {
                // extraPlugins: 'mathjax'
                // });
                
                //   CKEDITOR.replace( 'option4', {
                // extraPlugins: 'mathjax'
                // });
                
                //   CKEDITOR.replace( 'option5', {
                // extraPlugins: 'mathjax'
                // });
                //   CKEDITOR.replace( 'question_explanation', {
                // extraPlugins: 'mathjax'
                // });
                  
               
                


               
               
            });
        </script>
        
 <script type="text/javascript"> 
 $(document).ready(function () {
     
     $("#question_from").validate({
                ignore: [], 
		rules: {
			
			
                    question_name: {
                    required: function() 
                    {
                    CKEDITOR.instances.question_name.updateElement();
                    },
                    maxlength: 3000
                    },
                        question_answer :{
                            required: true
                        },
                       
                        option1: {
                            required: true
                        },
                        option2: {
                            required: true
                        },
                        option3: {
                            required: true
                        },
		},
		
	});
	
 
 
  
        
       
 
    });
 </script>