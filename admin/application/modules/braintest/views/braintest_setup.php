<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('company_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topper_from" id="topper_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('com_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $braintest->test_name; ?>" placeholder="<?php echo lang('com_name'); ?>" name="test_name" id="test_name" class="form-control">
                                        </div>
                                         <div class="form-group"> 
                                    <label for="exampleInputEmail1">Comany Logo</label> 
                                    <input type="file"  placeholder="Company Logo" name="test_image" id="test_image" class="form-control"  >
                                </div>  
                                    <!-- <div class="form-group"> 
                                         <label for="exampleInputEmail1"><?php echo lang('no_of_student'); ?></label>
                                        <input type="text" value="<?php if($action=='edit') echo $braintest->no_of_student; ?>" placeholder="<?php echo lang('no_of_student'); ?>" name="no_of_student" id="no_of_student" class="form-control" <?php if($action=='edit') echo "readonly"?> >                           
                                    </div>

                                <div class="form-group">                                           
                                 <label for="exampleInputEmail1"><?php echo lang('student_password'); ?></label>      
                                    <input type="text" value="<?php if($action=='edit') echo $braintest->student_password; ?>" placeholder="<?php echo lang('student_password'); ?>" name="student_password" id="student_password" class="form-control" <?php if($action=='edit') echo "readonly"?> > 
                                </div> -->

                                <div class="form-group"> 
                                    <label for="exampleInputEmail1"><?php echo lang('duration'); ?></label> 
                                    <input type="text" value="<?php if($action=='edit') echo $braintest->duration; else echo 120; ?>" placeholder="<?php echo lang('duration'); ?>" name="duration" id="duration" class="form-control" >
                                </div>	

                                <div class="form-group"> 
                                    <label for="exampleInputEmail1"><?php echo lang('break_time'); ?></label> 
                                    <input type="text" value="<?php if($action=='edit') echo $braintest->break_time;else echo 2; ?>" placeholder="<?php echo lang('break_time'); ?>" name="break_time" id="break_time" class="form-control"  >
                                </div>			


                                <div class="form-group"> 
                                   <label><?php echo lang('test_date'); ?>:</label>     
                                    <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i></div>
                                    <input type="text" id='datepicker' name='test_date' value="<?php if($action=='edit') echo $braintest->test_date; ?>" class="form-control" readonly=""  />
                                </div><!-- /.input group -->                                   
                                </div><!-- /.form group -->	


                                <div class="bootstrap-timepicker">                                      
                                   <div class="form-group">                                           
                                    <label><?php echo lang('test_time'); ?>:</label>                                            
                                    <div class="input-group">                                                
                                        <input type="text" name="test_time" id="test_time" value="<?php if($action=='edit') echo $braintest->test_time; ?>" class="form-control timepicker"/>  
                                <div class="input-group-addon">     
                                <i class="fa fa-clock-o"></i>  
                                </div>    
                                </div><!-- /.input group -->                                                                                                                                                                                                                                                                  
                                </div><!-- /.form group -->   
                                </div>

                                
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>        <!-- bootstrap time picker -->        <script src="<?php echo base_url();?>assets_new/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>		
	<script type="text/javascript">            $(function () {							$("#test_time").timepicker({                    showInputs: false                });								$( "#datepicker" ).datepicker({					format: 'dd-mm-yyyy'                });            });        </script>
       <?php 
echo $script;
?>
