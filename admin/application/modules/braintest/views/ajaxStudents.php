<div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_student');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">								<div class="table-responsive">
									<table class="table table-bordered table-hover">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th>Student Name</th> 
                                        <th>Student Mobile</th> 
                                        <th>Exam Date</th>
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        Actions
                                        </th>
                                        </tr>
                                      <?php
                                      if($students !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($students as  $value) {
                                      ?>
                                        <tr>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->examuser_name;?></td>                                    
                                        <td align="center"><?php echo $value->examuser_mobile;?></td>                                      
                                        <td align="center"><?php echo $value->last_login;?></td>                                                                                
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->test_status == '2' || $value->test_status == '1' || $value->test_status == '0') {
                                        ?>
                                        
                                        <a class="red" href="#">
                                        Started
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" >
                                       Completed
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="action-buttons">                                        
										                    <?php if ($value->test_status==3){ ?>	
                  											 <a class="green" title="view Result" href="<?php echo base_url();?>braintest/testhistory/viewresult/<?php echo $value->brainTestId;?>/<?php echo $value->studentId;?>" >
                  											<i class="ace-icon fa fa-arrows-alt bigger-130"></i>
                  											</a>
										                    <?php } ?>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.table-responsive -->                            </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->