<section class="content">
  <!-- right column -->
        <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Search For Students 
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
             From Date
                  <input type="text" class="form-control" id="datepicker" name="datepicker" placeholder="From Date">
                   <span id="fromerror" style="color: Red; display: none"></span>
                </div>
                <div class="col-xs-3">
             To Date
                  <input type="text" class="form-control" id="datepicker1" name="datepicke1r" placeholder="To Date">
                   <span id="toerror" style="color: Red; display: none"></span>
                </div>
                <div class="col-xs-3">
                <label></label>
                <button type="button" id="search" class="btn btn-info pull-right">Search</button>
              </div>
               
              </div>
      

            </div>
            
          </div>
          
          
        
        </div>
        <!--/.col (right) -->
  <div id="ajaxdata">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title">Test History</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">								<div class="table-responsive">
									<table class="table table-bordered table-striped">
                                    <tbody>
										<tr>
											<th align="center"><?php echo lang('Sno');?></th>
											<th align="center" style="text-align: center !important;" ><?php echo lang('com_name');?></th>
                      <th  align="center" style="text-align: center !important;">Attend Students</th>
											<th align="center" style="text-align: center !important;">
											Actions
											</th>
                                        </tr>
                                      <?php
                                      if($braintest !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($braintest as  $value) {
                                      ?>
                                        <tr id='com_<?php echo $value->brain_test_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->test_name;?></td> 
                                        <td align="center">
                                        <?php echo $countstudents[$value->brain_test_id]; ?>
                                        </td>
                                      
                                        <td align="center">
                                        <div class="action-buttons">
                                        
                                        <a class="" alt="View Result" title="Question set"  href="<?php echo base_url();?>braintest/testhistory/viewstudents/<?php echo $value->brain_test_id;?>" >
                                          <i class="ace-icon fa fa-arrows-alt bigger-130"></i>
                                        </a>   

                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.table-responsive -->                            </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    </div>
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>braintest/delete',
          success: function(msg) {
            $("#com_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id1, id,  status) { 
     var p_url= "<?php echo base_url(); ?>braintest/changeTestStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'id='+id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>

  <script src="<?php echo base_url();?>assets_new/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
        

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script>

$(function() {

$( "#datepicker" ).datepicker();
$( "#datepicker1" ).datepicker();

});

</script>
<script>
$(document).ready(function(){

$("#search").on('click',function(){

    var datepicker1,datepicke2;  
    datepicker1 = $("#datepicker").val();    
    if(datepicker1 == ''){
          $("#fromerror").css('display','block');
          $("#fromerror").html('From Date Required');
          return false;
    }
     datepicker2 = $("#datepicker1").val();    
    if(datepicker2 == ''){
          $("#toerror").css('display','block');
          $("#toerror").html('To Date Required');
          return false;
    }
         
   

 var p_url= "<?php echo base_url(); ?>braintest/testhistory/getStudentsData";
      var ajaxLoading = false;
      if(!ajaxLoading) {
      var ajaxLoading = true;
      $('#ajaxdata').html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
      $.ajax({
            type: "POST",             
            url: p_url,
            data: 'fromDate='+datepicker1+'&toDate='+datepicker2,
            success: function(data) {              
            $("#ajaxdata").html(data);
            ajaxLoading = false;
            }

      }); 
      } 
  return false;
 
});


$(".studentData").on('click',function(){

  var testId = $(".studentData").attr('id').val();
  alert(testId);

    var datepicker1,datepicke2;  
    datepicker1 = $("#datepicker").val();    
    if(datepicker1 == ''){
          $("#fromerror").css('display','block');
          $("#fromerror").html('From Date Required');
          return false;
    }
     datepicker2 = $("#datepicker1").val();    
    if(datepicker2 == ''){
          $("#toerror").css('display','block');
          $("#toerror").html('To Date Required');
          return false;
    }
         
   

 var p_url= "<?php echo base_url(); ?>practicetest/testhistory/getStudentsData";
      var ajaxLoading = false;
      if(!ajaxLoading) {
      var ajaxLoading = true;
      $('#ajaxdata').html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
      $.ajax({
            type: "POST",             
            url: p_url,
            data: 'fromDate='+datepicker1+'&toDate='+datepicker2,
            success: function(data) {              
            $("#ajaxdata").html(data);
            ajaxLoading = false;
            }

      }); 
      } 
  return false;
 
});
 

});

function getStudentdata(tid){

  
  var tid= tid;
    var datepicker1,datepicke2;  
    datepicker1 = $("#datepicker").val();    
    if(datepicker1 == ''){
          $("#fromerror").css('display','block');
          $("#fromerror").html('From Date Required');
          return false;
    }
     datepicker2 = $("#datepicker1").val();    
    if(datepicker2 == ''){
          $("#toerror").css('display','block');
          $("#toerror").html('To Date Required');
          return false;
    }
         
   

 var p_url= "<?php echo base_url(); ?>braintest/testhistory/getStudentsDataHistory";
      var ajaxLoading = false;
      if(!ajaxLoading) {
      var ajaxLoading = true;
      $('#ajaxdata').html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
      $.ajax({
            type: "POST",             
            url: p_url,
            data: 'fromDate='+datepicker1+'&toDate='+datepicker2+'&testId='+tid,
            success: function(data) {              
            $("#ajaxdata").html(data);
            ajaxLoading = false;
            }

      }); 
      } 
  return false;
}


</script>