<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Author : Venkata Sudhakar 

 * Project : Brainwizz

 * Company : renegade it solutions

 * Version v1.0

 * Model : practicetest

 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com

 * Phone :8686994774

 * Website : phpguidance.com

 */



class practicetest_Model extends CI_Model {



	public $variable;



	public function __construct() {

	    parent::__construct();

		

	}

        public function insertMany($tablename,$data){

            $this->db->insert(DB_PREFIX.$tablename,$data);

	    return $this->db->insert_id();

        }

        public function updateMany($tablename,$data,$fname,$value){

            $this->db->where($fname, $value);

	    $this->db->update(DB_PREFIX.$tablename, $data);

	    return $this->db->affected_rows();

        }

        public function deleteMany($tablename,$fname,$value){

	    $this->db->where($fname, $value);

            $this->db->delete(DB_PREFIX.$tablename);

	} 

        public function get(){

            $this->db->where('c_id !=','0');

            $query = $this->db->get(DB_PREFIX.'pcategories');

            if($query->num_rows()>0){

                 return $query->result();

            }else {

                return '';

            }

        }

	public function save($data){

	    $this->db->insert(DB_PREFIX.'pcategories',$data);

	    return $this->db->insert_id();

	}

	public function update($data, $id){

	    $this->db->where('id', $id);

	    $this->db->update(DB_PREFIX.'pcategories', $data);

	    return $this->db->affected_rows();

	}

	public function getcatgeoryDetails($id){

	    $this->db->where('id', $id);

	    $query = $this->db->get(DB_PREFIX.'pcategories');

	    if($query->num_rows()>0){

	    return $query->row();

	    }else {

	    return '';

	    }

        }

	public function changecategoryStatus($data, $id){

	    $this->db->where('id', $id);

	    $this->db->update(DB_PREFIX.'pcategories', $data);

            return $this->db->affected_rows();

	}
    public function changeTestStatus($data, $id){

        $this->db->where('test_id', $id);

        $this->db->update('gk_ptests', $data);

            return $this->db->affected_rows();

    }
    

	public function deletecompany($id){
	    $this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'pcategories');
	}        
    public function deleteTest($id){
        $this->db->where('test_id', $id);
        $this->db->delete('gk_ptests');
    }        

        public function gettopics(){

            $sql = "SELECT gk_ptopics.*, a.category_name as category FROM gk_ptopics

                   INNER JOIN gk_pcategories as a ON a.id = gk_ptopics.subcat_id";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            }

        }

        public function gettopicDetails($tpid){

            $sql = "SELECT * FROM gk_ptopics WHERE  topic_id = $tpid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->row();

            } else {

                return '';

            }

        }

        public function getPassages(){

            $sql = "SELECT c.*,gk_ptpassages.*, a.category_name as category FROM gk_ptpassages

                    INNER JOIN gk_pcategories as a ON a.id = gk_ptpassages.subcat_id

                    INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptpassages.topic_id";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            } 

        }

        public function getcattopics($catid){

            $sql = "SELECT * FROM gk_ptopics

                   WHERE gk_ptopics.subcat_id = $catid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            }

        }

        public function getpassageDetails($passageid){

            $sql = "SELECT * FROM gk_ptpassages

                    WHERE gk_ptpassages.id=$passageid";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->row();

            } else {

                return '';

            } 

        }

        public function getTests(){

            $sql = "SELECT c.*,gk_ptests.*, a.category_name as category FROM gk_ptests

                    INNER JOIN gk_pcategories as a ON a.id = gk_ptests.subcat_id

                    INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptests.topic_id
                     ORDER BY gk_ptests.position ASC
                    ";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->result();

            } else {

                return '';

            } 

        }

        public function gettestDetails($tid){

            $sql = "SELECT * FROM gk_ptests   WHERE test_id = $tid ";

            $query = $this->db->query($sql);

            if($query->num_rows()>0) {

                return $query->row();

            } else {

                return '';

            } 

        }
        public function getTestAttenedstudentslist($tid){

         $sql = "SELECT gk_ptestresult.id from gk_ptestresult 
                 INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                 WHERE gk_ptestresult.test_id='$tid'";
          $query = $this->db->query($sql);
          if($query->num_rows()>0) {
            return $query->num_rows();
        } else {
            return 0;
        }

     }

         public function testgetstudents($id)   {
        $sql = "SELECT gk_examuserslist.examuser_id,gk_examuserslist.examuser_name,gk_examuserslist.examuser_mobile,gk_ptestresult.* from gk_ptestresult 
                INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                WHERE test_id='$id' ORDER BY gk_ptestresult.id DESC";                
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
        return $query->result();
        }else {
        return  false;
        }
    }
    public function getq(){
        $sql = "SELECT q_id,question_name,option1,option2,option3,option4,option5,question_explanation from gk_ptquetions                             
                WHERE option2 like '%<w:WordDocument>%'";                
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
        return $query->result();
        }else {
        return  false;
        }
    }
    public function updateq($data, $id){

        $this->db->where('q_id', $id);

        $this->db->update('gk_ptquetions', $data);
//echo $this->db->last_query();exit;
        return $this->db->affected_rows();

    }
    public function testResult($tid,$sid)   {
        $sql = "SELECT gk_examuserslist.examuser_id,gk_examuserslist.examuser_name,gk_examuserslist.examuser_mobile,gk_ptestresult.*,gk_ptests.* from gk_ptestresult 
                INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                INNER JOIN gk_ptests ON gk_ptests.test_id = gk_ptestresult.test_id                
                WHERE gk_ptestresult.test_id='$tid' AND gk_ptestresult.user_id='$sid'";                
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
        return $query->row();
        }else {
        return  false;
        }
    }
    public function getAttendtests($fromDate,$toDate){

        $sql = "SELECT c.*,gk_ptests.*, a.category_name as category FROM gk_ptests

                INNER JOIN gk_pcategories as a ON a.id = gk_ptests.subcat_id
                INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptests.topic_id
                INNER JOIN gk_ptestresult ON gk_ptestresult.test_id = gk_ptests.test_id
                INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                WHERE (gk_ptestresult.test_date BETWEEN '$fromDate' AND '$toDate')
                 GROUp BY gk_ptestresult.test_id
                ";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        } 

    }
        public function getTestAttenedstudentslistDates($tid,$fdate,$tdate){

         $sql = "SELECT gk_ptestresult.id from gk_ptestresult 
                 INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                  WHERE (gk_ptestresult.test_date BETWEEN '$fdate' AND '$tdate') AND  gk_ptestresult.test_id='$tid' ";
          $query = $this->db->query($sql);
          if($query->num_rows()>0) {
            return $query->num_rows();
        } else {
            return 0;
        }

     }
     public function getStudentsDataHistory($tid,$fdate,$tdate){
         $sql = "SELECT gk_examuserslist.examuser_id,gk_examuserslist.examuser_name,gk_examuserslist.examuser_mobile,gk_ptestresult.* from gk_ptestresult 
                INNER JOIN gk_examuserslist ON gk_examuserslist.examuser_id = gk_ptestresult.user_id
                WHERE (gk_ptestresult.test_date BETWEEN '$fdate' AND '$tdate') AND  gk_ptestresult.test_id='$tid' ORDER BY gk_ptestresult.id DESC";                
        $query = $this->db->query($sql);
        if($query->num_rows()>0){
        return $query->result();
        }else {
        return  false;
        }
     }

 public function getNotStudents(){
        $sql = "SELECT examuser_id,notification_token FROM gk_examuserslist WHERE notification_token !=''";
         $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }
        public function getTopicAndCatName($topicId){
      $sql = "SELECT gk_ptopics.topic_name, gk_pcategories.category_name FROM gk_ptopics
              INNER JOIN gk_pcategories ON gk_pcategories.id = gk_ptopics.subcat_id  
              WHERE gk_ptopics.topic_id = '$topicId'";
        $res = $this->db->query($sql);
        if($res->num_rows()>0){
           return $res->row(); 
        }else {
           return false;
        }
    }





}



/* End of file companies_model.php */

/* Location: ./application/models/companies_model.php */
