<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Company
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class questions_model extends CI_Model {

	public $variable;

	public function __construct() {
	    parent::__construct();
		
	}
	public function getallpassages($tid){
            $this->db->where('topic_id', $tid);
            $query = $this->db->get('gk_ptpassages');
            if ($query->num_rows() > 0) {
            return $query->result();
            } else {
            return '';
            }
        }
        public function getpassagedetails($id) {
            $this->db->where('id', $id);
            $query = $this->db->get('gk_ptpassages');
            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return '';
            }
        }
        public function addquestiondetails($data) {
            $this->db->insert('gk_ptquetions', $data); 
            return $this->db->insert_id();
        }
        public function getpttestquestion($tid) {
            $this->db->where('test_id', $tid);
            $query = $this->db->get('gk_ptquetions');
            if ($query->num_rows() > 0) {
               return $query->result();
            } else {
               return '';
            }
        }
          public function updatequestion($data, $id){
    $this->db->where('q_id', $id);
      $this->db->update('gk_ptquetions', $data);
      return $this->db->affected_rows();
  }
        
        
        
        public function getquestiondetails($qid){
            
            $sql = "SELECT gk_ptquetions.*,test.*, topic.topic_name as topic_name FROM gk_ptquetions
                    INNER JOIN gk_ptopics as topic ON topic.topic_id = gk_ptquetions.topic_id
                    INNER JOIN gk_ptests as test ON test.test_Id = gk_ptquetions.test_Id
                     WHERE gk_ptquetions.q_id='$qid'";
             
            
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
        }
        
         public function getsubcategories($cid) {
         $this->db->where('c_id', $cid);
         $query = $this->db->get(DB_PREFIX.'companies');
	  if ($query->num_rows() > 0) {      
	    return $query->result();
         }
         else {
            return '';
	 } 
    }
	public function save($data){
		$this->db->insert(DB_PREFIX.'companyqas',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas', $data);
	    return $this->db->affected_rows();
	}
	public function getcomapnyDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'companypdf');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changequestionStatus($data, $id){
	    $this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas', $data);
            return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('q_id', $id);
        $this->db->delete('gk_ptquetions');
	}
        
        public function getPapers(){
             $sql = "SELECT  cat.com_name , subcat.id as SubID ,subcat.status, subcat.com_name as SubCategory
              FROM gk_companies as cat inner join gk_companies as subcat
              on cat.id = subcat.c_id where cat.c_id =0";  
             //left outer join
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
        }


}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */