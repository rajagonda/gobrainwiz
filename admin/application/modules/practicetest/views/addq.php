<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('type_question');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo base_url();?>practicetest/questions/addtestquetion" role="form" name="type_from" id="type_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('type'); ?>:</label>
                                            <select id="qtype" name="qtype" class="form-control">
                                                <option value=''>Select Type Of Question</option>
                                                <option value="normal">Normal</option>
                                                <option value="passage">Passage</option>
                                            </select>
                                        </div>
                                         <div class="form-group detailsview">
                                            <label><?php echo lang('passage'); ?>:</label>
                                            <select id="ptitle" name="ptitle" class="form-control">
                                                <option value=''>Select passage</option>                                                 
                                            </select>
                                        </div>
                                        <div id="detailsview1">

                                        </div>
                                     
                                        
                                    </div><!-- /.box-body -->
                                    <input type="hidden" name="topicid" id="topicid" value="<?php echo $topicid; ?>">
                                    <input type="hidden" name="testid" id="testid" value="<?php echo $testid ?>">
                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
//echo $script;
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets_new/js/jquery.validation.js"></script>
 <script type="text/javascript"> 
     
     $("#type_from").validate({
                //ignore: [], 
		rules: {
			
			qtype: {
				required: true
				
			},
                        ptitle :{
                            required: true
                        },
		},
		
	});
     
 $(document).ready(function () {
 $(".detailsview").hide();
     $("#qtype").change(function (){
       var selectVal = $('#qtype :selected').val();
       //alert(selectVal);
       if(selectVal == 'passage') {
       $(".detailsview").show();
       $("#detailsview1").html('');
       $("#ptitle").css("display", "block");
       var topicid = $('#topicid').val();
       $.ajax({
       url:'<?php echo base_url(); ?>practicetest/questions/getpasagetitles',
       type: 'POST',
       datatype:'application/json',
       data: {
            'topicid' : topicid
       },
       success:function(data){
           var htmlString ='';
            htmlString+="<option value=''>Select Passage Title</option>"
            $.each(data,function(i){
                  htmlString+="<option value="+data[i]['id']+">"+data[i]['passage_title']+"</option>"
                });
            $("#ptitle").html(htmlString);
       
       }
      
       
});
}else {
$(".detailsview").hide();
 //$("#ptitle").css("display", "block");
$("#detailsview1").hide();
}

 });
$("#ptitle").change(function (){
    $("#detailsview1").show();
    var selectVal = $('#ptitle :selected').val();

       $.ajax({
       url:'<?php echo base_url(); ?>practicetest/questions/getsinglepassagedetails',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
        //alert(data);
       $('#detailsview1').html('<div class="formRow">\n\
       <label>Passage Title:<span class="req">*</span></label>\n\
       <div class="formRight">'+data["passage_title"]+'</div><div class="clear"></div>\n\
       </div>\n\
       <div class="formRow">\n\
       <label>Passage Details:<span class="req">*</span></label>\n\
       <div class="formRight">'+data["passage"]+'</div><div class="clear"></div>\n\
       </div>');
           
       
       }
      
       
});

 });
 
 
  
        
       
 
    });
 </script>