<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('company_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topic_from" id="topic_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('c_id'); ?>:</label>
                                            <select id="cat_id" name="cat_id" class="form-control">
                                                <option value=''>Please Select Category</option>
                                                <?php foreach($catgeories as $value) { ?>
                                                <option value="<?php echo $value->id; ?>" <?php if($action=='edit')  if($category->subcat_id == $value->id) echo "selected";  ?>><?php echo $value->category_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('topic_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $category->topic_name; ?>" placeholder="<?php echo lang('topic_name'); ?>" name="topic_name" id="topic_name" class="form-control">
                                        </div>
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
