<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('test_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="test_form" id="test_form" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('c_id'); ?>:</label>
                                            <select id="cat_id" name="cat_id" class="form-control">
                                                <option value=''>Please Select Category</option>
                                                <?php foreach($catgeories as $value) { ?>
                                                <option value="<?php echo $value->id; ?>" <?php if($action=='edit')  if($test->subcat_id == $value->id) echo "selected";  ?>><?php echo $value->category_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label><?php echo lang('topic_id'); ?>:</label>
                                            <select id="topic_id" name="topic_id" class="form-control">
                                                <option value=''>Please Select Topic</option>
                                             <?php if($action=='edit') foreach($topics as $topic){ ?>
                                                 <option value="<?php echo $topic->topic_id; ?>" <?php if($action=='edit')  if($test->topic_id == $topic->topic_id) echo "selected";  ?>><?php echo $topic->topic_name; ?></option>
                                             <?php } ?>
                                            </select>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label><?php echo lang('test_name'); ?> :</label>
                                            <input type="text" placeholder="<?php echo lang('test_name'); ?>" value="<?php if($action == 'edit') echo $test->test_name; ?>" name="test_name" id="test_name" class="form-control">
                                        </div>
                                          <div class="form-group">
                                              <?php
                                              if($action == 'edit'){                                             
	                                      $time = strtotime($test->test_time);
                                              $time_selected =  date('i', $time);                                              
                                              }
                                              ?>
                                            <label><?php echo lang('test_time'); ?>:</label>
                                            <select id="test_time" name="test_time" class="form-control">
                                            <option value=''>Please Select Time</option>
                                            <option value="05" <?php if($action=='edit')  if($time_selected == "05") echo "selected";  ?>>05</option>
                                            <option value="10" <?php if($action=='edit')  if($time_selected == "10") echo "selected";  ?>>10</option>
                                            <option value="15"<?php if($action=='edit')  if($time_selected == "15") echo "selected";  ?>>15</option>
                                            <option value="20" <?php if($action=='edit')  if($time_selected == "20") echo "selected";  ?>>20</option>
                                            <option value="25"<?php if($action=='edit')  if($time_selected == "25") echo "selected";  ?>>25</option>
                                            <option value="30" <?php if($action=='edit')  if($time_selected == "30") echo "selected";  ?>>30</option>
                                            <option value="35" <?php if($action=='edit')  if($time_selected == "35") echo "selected";  ?>>35</option>
                                            <option value="40" <?php if($action=='edit')  if($time_selected == "40") echo "selected";  ?>>40</option>
                                            <option value="45"<?php if($action=='edit')  if($time_selected == "45") echo "selected";  ?>>45</option>
                                            <option value="50" <?php if($action=='edit')  if($time_selected == "50") echo "selected";  ?>>50</option>
                                            <option value="55" <?php if($action=='edit')  if($time_selected == "55") echo "selected";  ?>>55</option>
                                             
                                            </select>
                                        </div>
                                       <div class="form-group">
                                       <label><?php echo lang('test_show'); ?> :</label>
                                       <input type="radio" id="radio1" name="test_show" checked="checked" value="y" ><label for="radio1">All</label>
                                       <input type="radio" id="radio2" name="test_show" value="n" ><label for="radio2">Paid</label>

                                      
                                        </div>
					
					<div class="form-group">
   <label>Description :</label>
    <textarea placeholder="Description"  cols="" name="weekly_desc"class="form-control"><?php if($action=='edit') echo $test->description; ?></textarea>
   

  
    </div>
    <div class="form-group">
   <label>Image :</label>
    <input type="file" name="image" id="image">
  

  
    </div>                                       
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>


<script type="text/javascript"> 
 $(document).ready(function () {
 $("#cat_id").change(function (){
     var selectVal = $('#cat_id :selected').val();
     $.ajax({
       url:'<?php echo base_url(); ?>practicetest/getTopics',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
       var htmlString ='';
        htmlString+="<option value=''>Select Topic</option>"
       $.each(data,function(i){
             htmlString+="<option value="+data[i]['topic_id']+">"+data[i]['topic_name']+"</option>"
           });
       $("#topic_id").html(htmlString);
       }
});

 });
 
 
  
        
       
 
    });
 </script>
