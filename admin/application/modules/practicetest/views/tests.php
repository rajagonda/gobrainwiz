
<style type="text/css">

.green {
    color: #69aa46 !important;
}
.red{
  color:red !important;
}  
</style>
<section class="content">

                    <div class="row">

                        <div class="col-md-12">

                           <div class="box">

                           <div class="box-header">

                           <h3 class="box-title"><?php echo lang('manage_test');?></h3>

                            </div><!-- /.box-header -->

                            <div class="box-body">

                            <table class="table table-bordered table-striped" id="tableList" >
				
				<thead>
                                      <tr>

                                        <th style="width: 10px"><?php echo lang('Sno');?></th>

                                        <th ><?php echo lang('subcat_id');?></th>

                                        <th ><?php echo lang('topic_name');?></th>

                                        <th ><?php echo lang('test_name');?></th>

                                        <th ><?php echo lang('test_time');?></th>

                                        <th ><?php echo lang('question');?></th>

                                        <th ><?php echo lang('questions');?></th>

                                        <th ><?php echo lang('test_show');?></th>

                                        

                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>

                                        <th>

                                        <a class="blue" href="<?php echo base_url();?>practicetest/testsave">

                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>

                                        </a>

                                        </th>

                                        </tr>
                                    </thead>





                                    <tbody>

                                      <?php

                                      if($tests !=''){

                                        $i=1;

                                        $j=1;

                                      foreach ($tests as  $value) {

                                      ?>

                                        <tr class="com_<?php echo $value->test_id;?>"   id='<?php echo $value->test_id;?>'>

                                        <td><?php echo $i;?></td>

                                        <td ><?php echo $value->category;?></td>

                                        <td ><?php echo $value->topic_name;?></td>

                                        <td><?php echo $value->test_name;?></td>

                                        <td><?php echo $value->test_time;?></td>

                                         <td align="center"><a href="<?php echo base_url();?>practicetest/questions/addtestquetion/<?php echo $value->test_id; ?>/<?php echo $value->topic_id; ?>">Add</</td>

                                            <td align="center"><a href="<?php echo base_url();?>practicetest/questions/ptestquetions/<?php echo $value->test_id; ?>/<?php echo $value->topic_id; ?>">View</a></td>

                                       

                                        <td><?php echo $value->test_show;?></td>

                                        

                                        

                                        <td align="center" id="status<?php echo $j;?>">

                                         <?php

                                        if($value->test_status == 'n') {

                                        ?>

                                        

                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->test_id;?>', '<?php echo $value->test_status;?>')">

                                        <i class="ace-icon fa fa-close bigger-130"></i>

                                        </a>

                                        <?php } else { ?>

                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->test_id;?>', '<?php echo $value->test_status;?>')">

                                        <i class="ace-icon fa fa-check bigger-130"></i>

                                        </a>

                                        <?php } ?> 

                                        </td>

                                        <td>

                                        <div class="hidden-sm hidden-xs action-buttons">

                                        <a class="green" href="<?php echo base_url();?>practicetest/testsave/<?php echo $value->test_id;?>" >

                                        <i class="ace-icon fa fa-pencil bigger-130"></i>

                                        </a>

                                        |

                                        <a class="red trash" id="<?php echo $value->test_id;?>" href="#" >

                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>

                                        </a>

                                        </div>

                                        </td>
					<input type="hidden" name="test_name" id="item" value="<?php echo $value->test_id;?>">

                                        </tr>

                                      <?php

                                        $i++;

                                        $j++;

                                       }

                                       }

                                      ?>

                                    </tbody></table>

                                </div><!-- /.box-body -->

                                <!-- <div class="box-footer clearfix">

                                    <ul class="pagination pagination-sm no-margin pull-right">

                                        <li><a href="#">«</a></li>

                                        <li><a href="#">1</a></li>

                                        <li><a href="#">2</a></li>

                                        <li><a href="#">3</a></li>

                                        <li><a href="#">»</a></li>

                                    </ul>

                                </div>

                            </div>--> 



                           

                        </div><!-- /.col -->

                       

                    </div><!-- /.row -->

                    

                </section>





<script type="text/javascript">

var $sortable = $(" #tableList > tbody");
$sortable.sortable({
  stop: function(event,ui){
    var parameters = $sortable.sortable( "toArray" );
 //   console.log(parameters)

    $.post('<?php echo base_url();?>practicetest/updateSorting',{value:parameters},function(result){
              //alert(result);
          location.reload();
          });
  }
})



    

function chk1( url )  {

    if( confirm('Are you sure you want to delete this row?') ) {

    return true;

    }else {

        return false;

    }

}



 $(".trash ").click(function() {

        if(chk1()){

         var del_id= $(this).attr('id');

          $.ajax({

          type: "POST",

          data: "id="+ del_id,

          url: '<?php echo base_url();?>practicetest/deletetest',

          success: function(msg) {

            $(".com_"+del_id).hide();

          }

        });

       

        }

        return false;

      });



    

 function changeStatus(id1, id,  status) { 

     var p_url= "<?php echo base_url(); ?>practicetest/changeTestStatus";

     var ajaxLoading = false;

     if(!ajaxLoading) {

     var ajaxLoading = true;

     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');

     jQuery.ajax({

     type: "POST",             

     url: p_url,

     data: 'id='+id+'&status='+status,

     success: function(data) {

      if(data == 1){

        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');

       

      } else {

        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');

         

      }

     ajaxLoading = false;

         }

         

     });  

        

    }

     }

     

  

</script>
