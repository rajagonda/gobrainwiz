<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('passage_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="passage_from" id="passage_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('c_id'); ?>:</label>
                                            <select id="cat_id" name="cat_id" class="form-control">
                                                <option value=''>Please Select Category</option>
                                                <?php foreach($catgeories as $value) { ?>
                                                <option value="<?php echo $value->id; ?>" <?php if($action=='edit')  if($passage->subcat_id == $value->id) echo "selected";  ?>><?php echo $value->category_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label><?php echo lang('topic_id'); ?>:</label>
                                            <select id="topic_id" name="topic_id" class="form-control">
                                                <option value=''>Please Select Topic</option>
                                             
                                            </select>
                                        </div>
                                        
                                       
                                           <div class="form-group">
                                            <label><?php echo lang('passage_title'); ?> :</label>
                                            <textarea placeholder="Enter ..." rows="3" name="passage_title" id="passage_title" class="form-control"><?php if($action == 'edit') echo $passage->passage_title; ?></textarea>
                                        </div>
                                         <div class="form-group">
                                            <label><?php echo lang('passage'); ?> :</label>
                                            <textarea placeholder="Enter ..." rows="3" name="passage" id="passage" class="form-control"><?php if($action == 'edit') echo $passage->passage; ?></textarea>
                                        </div>
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
//echo $script;
?>

 <script type="text/javascript">
            $(function() {


                 CKEDITOR.replace( 'passage', {
                extraPlugins: 'mathjax'
                });

             
               
                


               
               
            });
        </script>
         <script src="http://localhost/brainwizz_beta/admin/assets_new/js/jquery.validation.js"></script>
<script type="text/javascript"> 
 $(document).ready(function () {
     
     
$("#passage_from").validate({
                ignore: [], 
                           
		rules: {
			
			topic_id: {
				required: true
				
			},
                        cat_id :{
                            required: true
                        },
                        passage: {
                    required: function() 
                    {
                    CKEDITOR.instances.passage.updateElement();
                    },
                    maxlength: 3000
                    },
                         
		},
		
	});
	

     
 
 $("#cat_id").change(function (){
     var selectVal = $('#cat_id :selected').val();
     $.ajax({
       url:'<?php echo base_url(); ?>practicetest/getTopics',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
       var htmlString ='';
        htmlString+="<option value=''>Select Topic</option>"
       $.each(data,function(i){
             htmlString+="<option value="+data[i]['topic_id']+">"+data[i]['topic_name']+"</option>"
           });
       $("#topic_id").html(htmlString);
       }
});

 });
 
 
  
        
       
 
    });
 </script>