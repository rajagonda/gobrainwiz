<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : placement
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| practicetest
|--------------------------------------------------------------------------
*/


$lang['manage_categories'] = 'Manage Categories';
$lang['category_add'] = 'Add Category';
$lang['category_edit'] = "Edit Category";
$lang['com_name'] = "Category  Name";
$lang['c_id'] = "Parent";



$lang['manage_topics'] = 'Manage Topics';
$lang['topic_add'] = 'Add Topic';
$lang['topic_edit'] = "Edit Topic";
$lang['topic_name'] = "Topic Name";
$lang['subcat_id'] = "Category";
$lang['topic_id'] = "Topic";

$lang['add'] = "Add";
$lang['view'] = 'view';
$lang['question'] = 'Question';
$lang['questions'] = 'Questions';


$lang['manage_passages'] = 'Manage Passages';
$lang['passage_add'] = 'Add Passage';
$lang['passage_edit'] = "Edit Passage";
$lang['passage_title'] = "Passage Title";
$lang['passage'] = "Passage";


$lang['manage_test'] = 'Manage Test';
$lang['test_add'] = 'Add Test';
$lang['test_edit'] = "Edit Test";
$lang['test_name'] = "Test Name";
$lang['test_time'] = "Time";
$lang['test_show'] = "Test Show";







$lang['manage_questions'] = 'Manage Questions';
$lang['question_add'] = 'Add Question';
$lang['question_edit'] = "Edit Question";
$lang['question_details'] = "Question Details";
$lang['option1'] = "option1";
$lang['type_question'] = "Type Of Question";
$lang['type'] = "Type";





/* End of file practicetest_lang.php */
/* Location: ./application/module_core/practicetest/language/english/practicetest_lang.php */
