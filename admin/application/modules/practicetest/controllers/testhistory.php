<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Author : Venkata Sudhakar 

 * Project : Brainwizz

 * Company : renegade it solutions

 * Version v1.0

 * Controller : practicetest

 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com

 * Phone :8686994774

 * Website : phpguidance.com

 */

class testhistory extends MY_Controller {



     public function __construct(){

            

        parent::__construct();

         if(!$this->authentication->checklogin()){

          redirect('login');

        }

        $this->load->library(array('template','form_validation'));

        $this->template->set_title('Welcome');

        $this->load->model('practicetest_Model');

        $this->load->language('practicetest');

		//$this->load->helper('text');

     }

     public function testInfo(){
		$breadcrumbarray = array('label'=> "Tests",
						  'link' => base_url()."practicetest/gettests"
		                  );
		$link = breadcrumb($breadcrumbarray);
		$this->template->set_breadcrumb($link);
		$this->template->set_subpagetitle("Manage Tests");
		$data['tests'] = $this->practicetest_Model->getTests();
		foreach ($data['tests'] as  $value) {
		$data['countstudents'][$value->test_id] = $this->practicetest_Model->getTestAttenedstudentslist($value->test_id);
		}
		$this->template->load_view1('testHistory', $data);
    }
	public function viewstudents($tid=null)	 
	{        

		$breadcrumbarray = array('label'=> "Practice Test",'link' => base_url()."practicetest/testhistory/testInfo");
		$link = breadcrumb($breadcrumbarray);
		$this->template->set_breadcrumb($link);		 
		$data['students'] = $this->practicetest_Model->testgetstudents($tid);		      
		$this->template->load_view1('teststudents', $data);     

	}
	public function viewresult($test_id,$studentId=null)
	{	
	    $breadcrumbarray = array('label'=> "Practice Test students",'link' => base_url()."practicetest/testhistory/viewstudents/".$test_id);							  
	    $link = breadcrumb($breadcrumbarray);
	    $this->template->set_breadcrumb($link);
		$data['testdetails'] = $this->practicetest_Model->testResult($test_id,$studentId);		
	    $this->template->load_view1('testresult',$data);
    }
    public function getStudentsData(){
    	$fromDate = date("Y-m-d", strtotime($this->input->post('fromDate')));
    	$toDate = date("Y-m-d",strtotime($this->input->post('toDate')));
    	$data['tests'] = $this->practicetest_Model->getAttendtests($fromDate,$toDate);

    	//$data['tests'] = $this->practicetest_Model->getTests();
		foreach ($data['tests'] as  $value) {
		$sres = $this->practicetest_Model->getTestAttenedstudentslistDates($value->test_id,$fromDate,$toDate);
		if($sres > 0){
		$data['countstudents'][$value->test_id] = $sres;	
		}
		
		}
		$this->load->view('ajaxHistory', $data);
    }
    public function getStudentsDataHistory(){
    	$fromDate = date("Y-m-d", strtotime($this->input->post('fromDate')));
    	$toDate = date("Y-m-d",strtotime($this->input->post('toDate')));
    	$testId = $this->input->post('testId');
    	$data['students'] = $this->practicetest_Model->getStudentsDataHistory($testId,$fromDate,$toDate);		      
		$this->load->view('ajaxStudents', $data);   


    }

	

       

}

