<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Author : Venkata Sudhakar 

 * Project : Brainwizz

 * Company : renegade it solutions

 * Version v1.0

 * Controller : questions

 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com

 * Phone :8686994774

 * Website : phpguidance.com

 */



class questions extends MY_Controller {



	public function __construct() {

            parent::__construct();

             if(!$this->authentication->checklogin()){

          redirect('login');

        }

        

            

            $this->load->library(array('template','form_validation'));

            $this->template->set_title('Welcome');

            $this->load->model(array('questions_model','practicetest_Model'));

            $this->load->language('practicetest');

        }

        //By this method and add quetion form

        public function addquetion($tid, $topicid){

            $breadcrumbarray = array('label'=> "Type Of Question",

                            'link' => base_url()."practicetest/gettests/"

                              );

            $link = breadcrumb($breadcrumbarray);

            $this->template->set_breadcrumb($link);

            $this->template->set_subpagetitle("Type Of Question");

            $data['topicid'] = $topicid;

            $data['testid'] = $tid;

          

           

            $this->template->load_view1('addq', $data); 

        }

        public function _qrules(){

            $rules = array(

                array('field' => 'qtype','label' => lang('type'),'rules' => 'trim|required|xss_clean') 

                 );

        return $rules;

        }

        public function getpasagetitles() {

            $topicid = $_POST['topicid'];

            $data['passages'] = $this->questions_model->getallpassages($topicid);

            header('Content-type: application/json');

            die( json_encode( $data['passages'] ) );

        }

         public function getsinglepassagedetails() {

            $pid = $_POST['selected'];

            $data['passagedetails'] = $this->questions_model->getpassagedetails($pid);

            header('Content-type: application/json');

            die( json_encode( $data['passagedetails'] ) );

       }       

        public function addtestquetion($tid,$topicId) {

            // $type = $this->input->post('qtype');

            // if($type == 'passage') {

            //    $passageid = $this->input->post('ptitle');

            //    $data['passagedetails'] = $this->questions_model->getpassagedetails($passageid);

            //    $data['pgs'] = '1';

            //    $data['pid'] = $passageid;

            // }else {

               $data['pgs'] = '0'; 

            //}

            $data['action'] = "add";

            $this->template->set_subpagetitle("Add Question");

            

            $data['topicid'] = $topicId;

            $data['testid'] = $tid;

            $this->template->load_view1('questions_setup', $data); 

      }

        public function edittestquetion($qid=null) {

           $data['questiondetails'] = $this->questions_model->getquestiondetails($qid);      

            $data['action'] = "edit";
            $this->template->set_subpagetitle("Edit Question");           

            $this->template->load_view1('question_edit', $data); 
      }
      public function updatequetiondetails() {

//echo "<pre>"; print_r($_POST);
          
            $qid = $this->input->post('qid');

                         
            

            $formvalues['topic_id'] = $this->input->post('topicid');

            $formvalues['test_id'] = $this->input->post('testid');



            $topicid = $this->input->post('topicid');

            $test_id = $this->input->post('testid');

         

            $qs1 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('question_name'));
            $qs = str_replace('../', '',$qs1);
            $formvalues['question_name'] = $qs;



            $op_1 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option1'));
            $op1 = str_replace('../', '',$op_1);
            $formvalues['option1'] = $op1;

            $op_2 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option2'));
            $op2 = str_replace('../', '',$op_2);
            $formvalues['option2'] = $op2;

            $op_3 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option3'));
            $op3 = str_replace('../', '',$op_3);
            $formvalues['option3'] = $op3;

            $op_4 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option4'));
            $op4 = str_replace('../', '',$op_4);
            $formvalues['option4'] = $op4;

            $op_5 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option5'));
            $op5 = str_replace('../', '',$op_5);
            $formvalues['option5'] = $op5;

            $exp = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('question_explanation'));
            $exp1 = str_replace('../', '',$exp);
            $formvalues['question_explanation'] = $exp1;



            $formvalues['question_answer'] = $this->input->post('question_answer');
            $formvalues['video_link'] = $this->input->post('video_link');

          //echo "<pre>"; print_r($formvalues);exit;

            $result = $this->questions_model->updatequestion($formvalues,$qid);  

            

                 $this->session->set_userdata('Question_success', "Successfully Question  Updated!!");

             redirect(base_url().'practicetest/questions/ptestquetions/'.$test_id.'/'.$topicid);     

            

            

         

         

      }

      public function addtestquetiondetails() {

          

            $passage =  $this->input->post('pgs');

            if($passage == '1') {

               $formvalues['passage_id'] = $this->input->post('pasageid');

               $pid = $this->input->post('pasageid');

            }

           
            //echo "<pre>"; print_r($this->input->post('question_name'));exit;

            $formvalues['topic_id'] = $this->input->post('topicid');

            $formvalues['test_id'] = $this->input->post('testid');



            $topicid = $this->input->post('topicid');

            $test_id = $this->input->post('testid');



            $qs1 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('question_name'));
            $qs = str_replace('../', '',$qs1);
            $formvalues['question_name'] = $qs;



            $op_1 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option1'));
            $op1 = str_replace('../', '',$op_1);
            $formvalues['option1'] = $op1;

            $op_2 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option2'));
            $op2 = str_replace('../', '',$op_2);
            $formvalues['option2'] = $op2;

            $op_3 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option3'));
            $op3 = str_replace('../', '',$op_3);
            $formvalues['option3'] = $op3;

            $op_4 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option4'));
            $op4 = str_replace('../', '',$op_4);
            $formvalues['option4'] = $op4;

            $op_5 = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('option5'));
            $op5 = str_replace('../', '',$op_5);
            $formvalues['option5'] = $op5;

            $exp = str_replace('../upload/', 'http://gobrainwiz.in/demo/upload/',$this->input->post('question_explanation'));
            $exp1 = str_replace('../', '',$exp);
            $formvalues['question_explanation'] = $exp1;

            $formvalues['question_answer'] = $this->input->post('question_answer');
            $formvalues['video_link'] = $this->input->post('video_link');

          //  echo "<pre>"; print_r($formvalues);exit;

            $result = $this->questions_model->addquestiondetails($formvalues);  

             if($passage == '1') {

                 $this->session->set_userdata('Question_success', "Successfully Question  Added!!");

             redirect(base_url().'practicetest/questions/addpassagequetion/'.$test_id.'/'.$topicid.'/'.$pid);

             }else {

                 $this->session->set_userdata('Question_success', "Successfully Question  Added!!");

             redirect(base_url().'practicetest/questions/ptestquetions/'.$test_id.'/'.$topicid);     

            

             }  

         

         

      }

       public function addpassagequetion($tid, $topicid,$pid) {

            $data['passagedetails'] = $this->questions_model->getpassagedetails($pid);

            $data['pgs'] = '1';

            $data['pid'] = $pid;

            $data['topicid'] =$topicid;

            $data['testid'] = $tid;

            $this->template->load_view1('questions_setup', $data); 

     }

     public function ptestquetions($tid, $topicid) {

         $data['topicid'] = $topicid;

         $data['tid'] = $tid; 

         $data['questions'] = $this->questions_model->getpttestquestion($tid);

        //echo "<pre>"; print_r($data['questions']); exit;

         $this->template->load_view1('questions', $data);

    }



        public function viewQuestions($sid=null)	{

            $breadcrumbarray = array('label'=> "Practicetest Questions",

                            'link' => base_url()."practicetest/questions/".$sid

                              );

           $link = breadcrumb($breadcrumbarray);

           $this->template->set_breadcrumb($link);

           $this->template->set_subpagetitle("Manage Questions");

           $data['questions'] = $this->questions_model->getquetions($sid);

           $data['sid'] = $sid;

          // echo "<pre>"; print_r($data);exit;

           $this->template->load_view1('questions', $data);

	}



	public function save($sid=null,$qid=null){

            

        $breadcrumbarray = array('label'=> "Add Question",

                           'link' => base_url()."placement/questions/save/".$sid

                           );

        $link = breadcrumb($breadcrumbarray);

        $this->template->set_breadcrumb($link);

        $data['companies'] = $this->company_Model->get();

        $data['csubcategorydetails'] = $this->company_Model->getcomapnyDetails($sid);

        $data['subcategories'] = $this->company_Model->getsubcategories($data['csubcategorydetails']->c_id);

        

        $validationRules = $this->_rules();

        foreach ($validationRules as $form_field)   {

        $rules[] = array(

        'name' => $form_field['field'],

        'display' => $form_field['label'],

        'rules' => $form_field['rules'],

        );

        }



      

$json_rules = json_encode($rules);

$script = <<< JS

<script>

var CIS = CIS || { Script: { queue: [] } };

CIS.Form.validation('question_from',{$json_rules});

</script>

JS;



           if($qid !=null){

            $data['action'] = "edit";

            $this->template->set_subpagetitle("Edit Question");

            $data['details'] = $this->questions_model->getquestiondetails($qid);

           }else {

            $data['action'] = "add";

            $this->template->set_subpagetitle("Add Question");

           }

         





          $this->form_validation->set_rules($validationRules);

          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

          

          if ($this->form_validation->run() == true)  {

          

            $formvalues['cat_id'] = $this->input->post('c_id');

            $formvalues['sub_id'] = $this->input->post('sub_id');

            $formvalues['question_name'] = $this->input->post('question_name');

            $formvalues['option1'] = $this->input->post('option1');

            $formvalues['option2'] = $this->input->post('option2');

            $formvalues['option3'] = $this->input->post('option3');

            $formvalues['option4'] = $this->input->post('option4');

            $formvalues['option5'] = $this->input->post('option5');

            $formvalues['question_explanation'] = $this->input->post('question_explanation');

            $formvalues['question_answer'] = $this->input->post('question_answer');

            

            

            $subid = $this->input->post('sub_id');

            if($qid !=null){

                $res = $this->questions_model->update($formvalues,$qid);

            }else {

                $res = $this->questions_model->save($formvalues);

            }

            redirect('placement/questions/viewQuestions/'.$subid);

          }



          

        }



      $data['script'] = $script;

      $this->template->load_view1('questions_setup',$data);

      }

        public function _rules() {

        $rules = array(

                array('field' => 'option1','label' => lang('option1'),'rules' => 'trim|required|xss_clean')

            

            

                 );

        return $rules;

    }

    

    

    public function viewQuestion($qid){

        $data['details'] = $this->questions_model->getquestiondetails($qid);

        //echo "<pre>"; print_r($data);exit;

         $this->template->load_view1('question_view',$data);

    }



    



    public function changequestionStatus(){

        $id = $_POST['id'];

        $staus = $_POST['status'];

        if($staus == 'y') {

            $formvalues['question_status'] = 'n';

            echo "0";

        } else {

            $formvalues['question_status'] = 'y';

            echo "1";

        }

        $this->questions_model->changequestionStatus($formvalues, $id);

    }

    public function delete(){

        $id =  $this->input->post('id');

        $this->questions_model->delete($id);



    }

    

      public function getsubcategories(){

       $catid = $_POST['selected'];

       $subcats = $this->questions_model->getsubcategories($catid);

        header('Content-type: application/json');

        die( json_encode( $subcats ) );  

   

    }

    



}

