<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Videos
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Videos extends MY_Controller {

      public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('videos_Model');
              $this->load->model('common_model');
            $this->load->language('videos');
      }
      public function index()	{
            $breadcrumbarray = array('label'=> "videos",
                            'link' => base_url()."videos"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Videos");
            $data['videos'] = $this->videos_Model->getallvideos();
            $this->template->load_view1('managevideos', $data);
      }

      public function edit($video_id){

            $breadcrumbarray = array('label'=> "videos/Edit",
                            'link' => base_url()."videos/edit/$video_id"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Videos");
            $this->videos_Model->updateVideoDetails($video_id);
            $data['video'] = $this->videos_Model->getvideodetails($video_id);
            $data['categories']=$this->videos_Model->getAllcategories();
            $this->template->load_view1('edit_video_setup',$data);
       
      }
      public function save($id=null){
            $data['categories'] = $this->videos_Model->getAllcategories();
            $breadcrumbarray = array('label'=> "Videos",
                           'link' => base_url()."videos"
                          );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);       
            $validationRules = $this->_rules();
            foreach ($validationRules as $form_field)   {
            $rules[] = array(
            'name' => $form_field['field'],
            'display' => $form_field['label'],
            'rules' => $form_field['rules'],
            );
            }      
            
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('video_from',{$json_rules});
</script>
JS;

            if($id !=''){
            $data['action'] = "edit";
            $data['video'] = $this->videos_Model->getvideodetails($id);
            $this->template->set_subpagetitle("Edit Video");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Vieos");
           }
           $this->form_validation->set_rules($validationRules);
           if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
           if ($this->form_validation->run() == true)  {
             $filename = '';
            if($_FILES['v_image']['name'] !='') {
           
            $filename = time()."_".$_FILES['v_image']['name'];
            $config['upload_path'] = '../public/videoimages/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 50;
            $config['file_name'] = $filename;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('v_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }  else {
            }
            $data = $this->upload->data();
           
            $timg = $data['file_name'];
            $formvalues['v_image'] = $timg;            
           }
           
            $formvalues['cid'] = $this->input->post('c_id');
            $formvalues['video_title'] = $this->input->post('video_title');
            $formvalues['video_format'] = $this->input->post('video_format');
            $formvalues['video_desc'] = $this->input->post('video_desc');
            $formvalues['video_show'] = $this->input->post('video_show');
            $formvalues['youtube_link']=$this->input->post('youtube_link');
            $formvalues['use_youtube']=$this->input->post('use_youtube');
            $cid = $this->input->post('c_id');
            $yurl = $this->input->post('youtube_link');

            
            if($id !=''){
             $res = $this->videos_Model->updatevideofile($id,$formvalues);
            }else {
              $ires = $this->videos_Model->insertvideofile($formvalues);
            }

            if($id !=''){
              $vid = $id;
            }else {
              $vid = $ires;
            }


$cuur_time = date('Y-m-d H:i:s');
                    // $ins_query = "INSERT INTO `gk_notifications` (`title`, `message`, `time_update`) VALUES ('Logged in', 'Logged in successfully', '".$cuur_time."')";

                    $notdata['title'] = "Video Uploaded";
                    $notdata['message'] = "New Video Released";
                    $notdata['notify_type'] = 2;
                    $notdata['catid'] = $cid;
                    $notdata['url'] = $yurl;
                    $notdata['time_update'] = $cuur_time;
                    
                    $nid = $this->common_model->insertSingle('gk_notifications',$notdata);

                    $stokens = array();
                    $studnetdata['students']= $this->videos_Model->getNotStudents();
                    foreach ($studnetdata['students'] as  $value) {
                      
                   
                     

                    $notlog['student_id'] = $value->examuser_id;
                    $notlog['notification_id'] =$nid;
                    $notlog['checked'] = '0';

                    $nlid = $this->common_model->insertSingle('gk_notifications_log',$notlog);
    // $result = $p_notify -> send_notification($tokens, $message,$title,0,false);
                    array_push($stokens, $value->notification_token);
                      }

                       $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
      //  $field1 -> message = 'New Video Released';
       // $field1 -> title = 'Video Uploaded';
       // $field1 -> image = false;

       $field1 -> message = $formvalues['video_desc'];
       $field1 -> title = $formvalues['video_title'];
       $field1 -> image = '';
if(!empty($filename))
{
  $field1 -> image = 'http://gobrainwiz.in/public/videoimages/'.$filename;
}
        $field1 -> type = '2';
        $field1 -> category = $cid;
        $field1 -> url = $yurl;
        $fields = array(
             'registration_ids' => $stokens,
             'data' => $field1
            );
    $headers = array(
     // 'Authorization:key = AIzaSyAFJlc5XeyL6vYCsXzTKP72DudKlKm64FU',
     'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',     
 'Content-Type: application/json'
      );
     $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       //return $result;


             redirect('videos');
          }          
        }

      $data['script'] = $script;
      $this->template->load_view1('video_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'c_id','label' => 'Category','rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'video_title','label' => "Title",'rules' => 'required'),
            array('field' => 'pdffile','label' => 'File','rules' => 'file_required')
                );
        return $rules;
    }

	 public function changetopperStatus(){
        $id = $_POST['voice_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['voice_status'] = 'n';
            echo "0";
        } else {
            $formvalues['voice_status'] = 'y';
            echo "1";
        }
        $this->videos_Model->changetopperStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->videos_Model->deletetopper($id);

    }

    public function deleteCat(){
     $cat=$this->input->post('id');
     $this->db->query("DELETE FROM gk_hvcategoriess WHERE c_id='$cat'");
    }

    public function deleteVideo($video_id)
    {
        $this->db->query("DELETE FROM gk_hvideos WHERE id='$video_id'");
        redirect('videos');
    }

}
