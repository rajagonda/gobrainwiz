<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : categories
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class categories extends MY_Controller {

       public function __construct(){
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('videos_Model');
            $this->load->language('videos');
       }	
       public function index()	{
            $breadcrumbarray = array('label'=> "categories",
                            'link' => base_url()."videos/categories"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Categories");
            $data['categories'] = $this->videos_Model->getAllcategories();
            $this->template->load_view1('categories', $data);
      }        
      public function changehomecatStatus() {
            $id = $_POST['cid'];
            $data['categories'] = $this->videos_Model->getAllcategories();
            foreach($data['categories'] as $value) {
            if($value->c_id !=$id){
             $formvalues['h_cat'] = '';
             $result = $this->videos_Model->updatehomecagoerystatus($value->c_id,$formvalues);
            }else {
             $formvalues['h_cat'] = '1';
             $result = $this->videos_Model->updatehomecagoerystatus($value->c_id,$formvalues);
            }
            }
            echo '1';
      }
      public function save($id=null){
            $breadcrumbarray = array('label'=> "categories",
                            'link' => base_url()."videos/categories"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $validationRules = $this->_rules();
            foreach ($validationRules as $form_field)   {
            $rules[] = array(
            'name' => $form_field['field'],
            'display' => $form_field['label'],
            'rules' => $form_field['rules'],
            );
            }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('category_from',{$json_rules});
</script>
JS;

            if($id !=''){
            $data['action'] = "edit";
            $data['catgeory'] = $this->videos_Model->getcategorydetails($id);
            $this->template->set_subpagetitle("Edit Catgeory");
            }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Category");
            }
            $this->form_validation->set_rules($validationRules);
            if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            if ($this->form_validation->run() == true)  {
            $form_values['cat_name'] = $this->input->post('cat_name');
            if($id !=''){
                
            $res = $this->videos_Model->updatecategorydetails($form_values,$id);
            }else {
            $res = $this->videos_Model->addcategorydetails($form_values);
            }
            redirect('videos/categories');
            }
            }
      $data['script'] = $script;
      $this->template->load_view1('category_setup',$data);
      }
      public function _rules() {
            $rules = array(
                array('field' => 'cat_name','label' => lang('cat_name'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
            return $rules;
     }
     public function changecategoryStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->videos_Model->changecategoryStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->company_Model->deletecompany($id);

    }    
}
