<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : videos
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class videos_Model extends CI_Model {
    public function __construct() {
        parent::__construct();    
    }
    public function getAllcategories() {
        $result = $this->db->get('gk_hvcategoriess');
        if($result->num_rows() > 0 ) {
            return $result->result();
        } else {
            return '';
        }
    }
    public function addcategorydetails($data) {
        $this->db->insert('gk_hvcategoriess', $data); 
        return $this->db->insert_id();
    }  
    public function getcategorydetails($id) {
        $this->db->where('c_id', $id);
        $query = $this->db->get('gk_hvcategoriess');
        if ($query->num_rows() > 0) {      
	    return $query->row();
        } else {
            return '';
        } 
     }
     public function updatecategorydetails($values, $id) {
        $this->db->where('c_id', $id);
        $this->db->update('gk_hvcategoriess', $values);
        return $this->db->affected_rows();       
    }
    public function deletecategory($id) {
        $this->db->where('c_id', $id);
        $this->db->delete('gk_hvcategoriess');
    }
    public function changecategoryStatus($values, $id) {
        $this->db->where('c_id', $id);
        $this->db->update('gk_hvcategoriess', $values);
        return $this->db->affected_rows();        
    }    
    public function updatehomecagoerystatus($id,$values) {
        $this->db->where('c_id', $id);
        $this->db->update('gk_hvcategoriess', $values);
        return $this->db->affected_rows(); 
    }
    public function getallvideos() {
        $sql = "SELECT  * FROM gk_hvideos INNER JOIN gk_hvcategoriess  
                on gk_hvcategoriess.c_id = gk_hvideos.cid"; 
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }    
    public function insertvideofile($data) {
        $this->db->insert('gk_hvideos', $data); 
        return $this->db->insert_id();
    }    
    public function getvideodetails($vid) {
        $this->db->where('id', $vid);
        $result = $this->db->get('gk_hvideos');
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }
    public function updatevideofile($vid, $data) {
        $this->db->where('id', $vid);
        $this->db->update('gk_hvideos', $data);
        return $this->db->affected_rows(); 
    }
    public function videodelete($vid) {
        $this->db->where('id', $vid);
        $this->db->delete('gk_hvideos');
    }
    public function changevdeoStatus($values, $id) {
        $this->db->where('id', $id);
        $this->db->update('gk_hvideos', $values);
        return $this->db->affected_rows();
    }

    public function updateVideoDetails($video_id){
        if($this->input->post('video_title')):
            $update=array();
            $update['cid']=$this->input->post('c_id');
            $update['video_title']=$this->input->post('video_title');
            $update['video_format']=$this->input->post('video_format');
            $update['video_desc']=$this->input->post('video_desc');
            $update['video_show']=$this->input->post('video_show');
            $update['youtube_link']=$this->input->post('youtube_link');
            $update['use_youtube']=$this->input->post('use_youtube');


            if($_FILES['pdffile']['name'] !='') {
            $filename = time()."_".$_FILES['pdffile']['name'];
            $config['upload_path'] = '../public/topicvideos/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 50;
            $config['file_name'] = $filename;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('pdffile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }  else {
            }
            $data = $this->upload->data();
            
            $timg = $data['file_name'];
            $update['video_name'] = $timg;
            }
            if($_FILES['v_image']['name'] !='') {
           
            $filename = time()."_".$_FILES['v_image']['name'];
            $config['upload_path'] = '../public/videoimages/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 50;
            $config['file_name'] = $filename;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('v_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }  else {
            }
            $data = $this->upload->data();
           
            $timg = $data['file_name'];
            $update['v_image'] = $timg;            
           }

            $this->db->where('id', $video_id);
            $this->db->update('gk_hvideos', $update);
        endif;    
    }
        public function getNotStudents(){
        $sql = "SELECT examuser_id,notification_token FROM gk_examuserslist WHERE notification_token !=''";
         $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }
}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */