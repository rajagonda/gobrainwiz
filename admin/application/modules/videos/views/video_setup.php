<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('video_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="video_from" id="video_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1"><?php echo lang('cat_name'); ?></label>
                                       <select name="c_id"  id="c_id" class="form-control">
                                       <option value="">Select Category</option>
                                       <?php
                                       foreach ($categories as $value)
                                       {?>                          
                                       <option value="<?php echo $value->c_id;?>" ><?php echo $value->cat_name;?></option>;
                                       <?php  
                                       }
                                       ?>
                                       </select>
                                   </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Video Title</label>
                                        <input type="text" value="<?php if($action=='edit') echo $catgeory->video_title; ?>" placeholder="Video Title" name="video_title" id="video_title"class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Video Format</label>
                                       <select class="form-control" name="video_format"  id="video_format">
                                       <option value="">Select Format</option>
                                       <option value="mp4" >MP4</option>;
                                        </select>
                                   </div>     
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Video File</label>
                                        <input type="file"  name="pdffile" >
                                    </div>
                                     <div class="form-group">
                                       <label for="exampleInputEmail1">Video Description</label>
                                        <textarea value="<?php if($action=='edit') echo $catgeory->cat_name; ?>" placeholder="Video Description" id="video_desc" cols="" name="video_desc"class="form-control"></textarea>
                                    </div>
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Video Image</label>
                                        <input type="file"  id="file" name="v_image" />
                                    </div>
                                       <div class="form-group">
                                             <label for="exampleInputEmail1">Video Show</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  value="y" checked="checked"   name="video_show" id="radio1">
                                                    All
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" value="n" name="video_show" id="radio2">
                                                   Paid
                                                </label>
                                            </div>

                                            <div class="form-group">
                                          <label for="exampleInputEmail1">Youtube Link</label>
                                          <input type="text" value="" placeholder="Video Title" name="youtube_link" id="youtube_link" class="form-control">
                                          </div>

                                          <label for="exampleInputEmail1">Video Player</label>
                                            
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" value="n"  name="use_youtube" id="radio2" checked="checked">
                                                     Customer Player
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  value="y"    name="use_youtube" id="radio1">
                                                      Youtube Player
                                                </label>
                                            </div>
                                         
                                        </div>
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                        <a href='/admin/videos' class="btn btn-info" >Back</a>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
