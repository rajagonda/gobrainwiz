   <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title">Manage Videos</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th align="center"><?php echo lang('cat_name');?></th>
                                        <th>Video Title</th>
                                        <th>Video Name</th>
                                        <th>Video Show</th>
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        <a class="blue" href="<?php echo base_url();?>videos/save">
                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        </th>
                                        </tr>
                                        <?php
                                        if($videos !=''){
                                        $i=1;
                                        $j=1;
                                        foreach ($videos as  $value) {
                                        ?>
                                        <tr id='com_<?php echo $value->c_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td ><?php echo $value->cat_name;?></td>
                                        
                                        <td align="center"><?php echo $value->video_title; ?></td>
                                        <td align="center"><?php echo $value->video_name; ?></td>
                                        <td class="actBtns" >
                                        <?php
                                        if($value->video_show == 'y') {
                                        ?>
                                        All users
                                        <?php } else { ?>
                                        Paid Users
                                        <?php } ?>
                                        </td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->c_id;?>', '<?php echo $value->status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->c_id;?>', '<?php echo $value->status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url();?>videos/edit/<?php echo $value->id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        |
                                        <a class="red trash" id="<?php echo $value->id;?>" onclick="if(window.confirm('Are you sure, you want to delete this video ?')){window.location='<?php echo base_url();?>videos/deleteVideo/<?php echo $value->id;?>'}" href="javascript:void(0)" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">
    /**
     * By using this method change role status
     * 
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */

function deleteVideo(video_id){
        var delete=window.confirm("Are you sure, you want to delete this video ?");
        if(delete)
        window.location="<?php echo site_url(); ?>";
}

 function changeStatus(id, tid,  status) { 
     var p_url= "<?php echo base_url(); ?>admin/homevideos/changevdeoStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id).html('<img src="<?php echo base_url();?>themes/admin/images/sp/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'tid='+tid+'&status='+status,
     success: function(data) {
        if(data == 1){
         $('#'+id).html('<a href="#" onclick="changeStatus(\''+id+'\', \''+tid+'\', \'y\')"><img src="<?php echo base_url();?>themes/admin/images/icons/color/tick.png" alt="" /></a>');
         $('#divSuccessMsg').show();
         $('#divSuccessMsg').html("<i class='icon-check'></i><span class='text-success'>Sucessfully Changed Status</span>")
            .fadeIn(1000).fadeOut(5000);
    
	 } else {
         $('#'+id).html('<a href="#" onclick="changeStatus(\''+id+'\', \''+tid+'\', \'n\')"><img src="<?php echo base_url();?>themes/admin/images/icons/color/cross.png" alt="" /></a>');
          $('#divSuccessMsg').show();
          $('#divSuccessMsg').html("<i class='icon-check'></i><span class='text-success'>Sucessfully Changed Status</span>")
            .fadeIn(1000).fadeOut(5000);
         }
	 ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>