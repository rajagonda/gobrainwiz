<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Sunil 
 * Project : Brainwiz
 * Version v1.0
 * Controller : Categories
 */
class Companies extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		
		if(!$this->authentication->checklogin()){
			redirect('login');
        }
		
	    $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('companies_model');
        $this->load->language('companies');
        $this->load->config('companies');
	}
	
	public function index() 
	{
            $breadcrumbarray = array('label'=> "Companies",
                           'link' => base_url()."companies"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Companies");
	    $data['companies'] = $this->companies_model->get();
		$this->template->load_view1('manage_companies',$data);

	}
	
	public function save($id=null)
	{
        $breadcrumbarray = array(
							'label'=> "companies",
                            'link' => base_url()."companies"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("companies");
        $validationRules = $this->_rules();
		
        foreach ($validationRules as $form_field)   {
			$rules[] = array(
				'name' => $form_field['field'],
				'display' => $form_field['label'],
				'rules' => $form_field['rules']
			);
        }

      
		$json_rules = json_encode($rules);
		$script = <<< JS
				<script>
				var CIS = CIS || { Script: { queue: [] } };
				CIS.Form.validation('companies_from',{$json_rules});
				</script>
JS;

            if($id !='')
		    {
				$data['action'] = "edit";
				$data['companies'] = $this->companies_model->getcompanyDetails($id);
            }
			else 
			{
				$data['action'] = "add";
            }

            $this->form_validation->set_rules($validationRules);
            
			if (isset($_POST) && is_array($_POST) && count($_POST) > 0)
			{
				$companies_values['cname'] = $this->input->post('cname');
				$companies_values['curl'] = $this->input->post('curl');
				if($_FILES['cimage']['name'] !=''){
					$filename = time()."_".$_FILES['cimage']['name'];
					$config['upload_path'] = '../upload/companies/';
					$config['allowed_types'] = '*';
					$config['max_size'] = 1024 * 8;
					$config['file_name'] = $filename;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
				
					if (!$this->upload->do_upload('cimage')) {
						$status = 'error';
						$msg = $this->upload->display_errors('', '');
					} 
					else 
					{
					}
					$this->data = $this->upload->data();
					$cimage = $this->data['file_name'];
                }
				else
				{
					if($id !='')
					{
						$cimage  = $data['companies']->cimage;
					}
					else
					{
						$cimage  = '';
					}
				}
				
				$companies_values['cimage'] = $cimage;
				
				           
				if($id !='')
				{
					$res = $this->companies_model->update($companies_values,$id);
				}
				else 
				{
					$res = $this->companies_model->save($companies_values);
				}
				redirect('companies');
        
			}

			$data['script'] = $script;
			$this->template->load_view1('companies_setup',$data);
	}

	public function _rules() 
	{
        $rules = array(
					array('field' => 'cname','label' => lang('cname'),'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }
	public function deleteCompany($cid)
    {
        $this->db->query("DELETE FROM gk_companies_placed WHERE cid='$cid'");
        redirect('companies');
    }
	
    public function changecompanyStatus()
	{
        $id = $_POST['cid'];
        $staus = $_POST['status'];
        if($staus == '1') {
            $formvalues['status'] = '0';
            echo "0";
        } else {
            $formvalues['status'] = '1';
            echo "1";
        }
        $this->companies_model->changecompanyStatus($formvalues, $id);
    }
   
}

/* End of file companies.php */
/* Location: ./application/controllers/companies.php */