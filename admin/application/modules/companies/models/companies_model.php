<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get()
	{
		$this->db->order_by('cid','desc');
		$query = $this->db->get(DB_PREFIX.'companies_placed');
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else 
		{
			return '';
		}
	}
	
	public function save($data)
	{
		$this->db->insert(DB_PREFIX.'companies_placed',$data);
		
		return $this->db->insert_id();

	}
	public function update($data, $id)
	{
		$this->db->where('cid', $id);
	    $this->db->update(DB_PREFIX.'companies_placed', $data);
		
		return $this->db->affected_rows();
	}

	public function getcompanyDetails($id)
	{
		$this->db->where('cid', $id);
		$query = $this->db->get(DB_PREFIX.'companies_placed');
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return '';
		}
	}
	
	public function changecategoryStatus($data, $id)
	{
		$this->db->where('cid', $id);
	    $this->db->update(DB_PREFIX.'companies_placed', $data);
	    return $this->db->affected_rows();
	}

}

/* End of file categories_model.php */
/* Location: ./application/models/categories_model.php */