<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| companies_lang.php
|--------------------------------------------------------------------------
*/

/**
*Companies 
**/
$lang['manage_companies'] = 'Manage Companies';
$lang['companies_add'] = 'Add company';
$lang['companies_edit'] = "Edit company";
$lang['companies_name'] = "company Name";
$lang['cname'] = 'Company Name';
$lang['curl'] = "Company Url";
$lang['cimage'] = "Company Image";

/* End of file volunteer_lang.php */
/* Location: ./application/module_core/Companies/language/english/blog_lang.php.php */
