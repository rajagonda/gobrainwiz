<div class="row">
	<div class="col-md-12">
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-6">
				<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><?php echo lang('companies_add');?></h3>
						</div><!-- /.box-header -->
						<!-- form start -->
						<?php
						$url = current_url();
						?>
						<form method="post"  action="<?php echo $url;?>" role="form" name="companies_from" id="companies_from" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1"><?php echo lang('cname'); ?></label>
									<input type="text" value="<?php if($action=='edit') echo $companies->cname; ?>" placeholder="<?php echo lang('cname'); ?>" name="cname" id="cname" class="form-control">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1"><?php echo lang('curl'); ?></label>
									<input type="text" value="<?php if($action=='edit') echo $companies->curl; ?>" placeholder="<?php echo lang('curl'); ?>" name="curl" id="curl" class="form-control">
								</div>
								<div class="form-group">
                                    <label for="exampleInputFile"><?php echo lang('cimage'); ?></label>
                                    <input type="file" name="cimage" id="cimage">
                                </div>
							</div><!-- /.box-body -->
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</form>
					</div><!-- /.box -->
                </div><!--/.col (left) -->
				
				<!-- right column -->
            </div>   <!-- /.row -->
		</section>
	</div>
</div>
<?php echo $script;?>