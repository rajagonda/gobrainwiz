<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| posts_lang.php
|--------------------------------------------------------------------------
*/

/**
*Posts 
**/
$lang['manage_posts'] = 'Manage Posts';
$lang['posts_add']    = 'Add Post';
$lang['posts_edit']   = "Edit Post";
$lang['cat_title']    = "Post Category";
$lang['post_title']   = "Post Title";
$lang['post_image']   = "Post Image";
$lang['meta_title']   = "Meta Title";
$lang['meta_keywords']  = "Meta Keywords";
$lang['meta_desc']    = "Meta Description";
$lang['post_desc']    = "Description";
$lang["disp_sidebar"] = "Diplay in Right Sidebar";

/* End of file volunteer_lang.php */
/* Location: ./application/module_core/Posts/language/english/posts_lang.php.php */
