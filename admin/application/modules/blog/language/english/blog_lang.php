<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| blog_lang.php
|--------------------------------------------------------------------------
*/

/**
*Categories 
**/
$lang['manage_categories'] = 'Manage Categories';
$lang['category_add'] = 'Add Category';
$lang['category_edit'] = "Edit Category";
$lang['category_name'] = "Category Name";

/* End of file volunteer_lang.php */
/* Location: ./application/module_core/Categories/language/english/blog_lang.php.php */
