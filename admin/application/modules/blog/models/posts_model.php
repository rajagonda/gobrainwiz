<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function postcount()
	{
		$sql = "SELECT * FROM gk_blog_posts WHERE status='1'"; 
	    $result = $this->db->query($sql);
           
		if($result->num_rows() > 0 ) 
		{
            return $result->num_rows();
        } 
		else 
		{
           return '';
        }
	}
		
	public function get($limit, $start){ 

		$sql = "SELECT * FROM gk_blog_posts WHERE status='1' ORDER BY post_id DESC LIMIT $start, $limit"; 
				
		$result = $this->db->query($sql);
		if($result->num_rows() > 0 ) 
		{
			return $result->result();
		} 
		else 
		{
			return '';
		}
	}
	
	
	public function getCategories()
	 {
        $this->db->where('status', 1);
		$query = $this->db->get(DB_PREFIX.'blog_categories');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}
        }
	
	public function save($data)
	{
		$this->db->insert(DB_PREFIX.'blog_posts',$data);
		
		return $this->db->insert_id();

	}
	public function update($data, $id)
	{
		$this->db->where('post_id', $id);
	    $this->db->update(DB_PREFIX.'blog_posts', $data);
		//echo $this->db->last_query();exit;
		return $this->db->affected_rows();
	}

	public function getPostDetails($id)
	{
		$this->db->where('post_id', $id);
		$query = $this->db->get(DB_PREFIX.'blog_posts');
		
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else
		{
			return '';
		}
	}
	
	public function changepostStatus($data, $id)
	{
		$this->db->where('post_id', $id);
	    $this->db->update(DB_PREFIX.'blog_posts', $data);
	    return $this->db->affected_rows();
	}
        
        public function checkPostSlug($urlSlug,$id='')
        {
            if(!empty($urlSlug)){
                $this->db->from('blog_posts');
                $this->db->where('post_slug',$urlSlug);
                $rowNum = $this->db->count_all_results();
                return ($rowNum>0) ? true : false;
            }
            else
            {
                return true;
            }
        }

}

/* End of file posts_model.php */
/* Location: ./application/models/posts_model.php */