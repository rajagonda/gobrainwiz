<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<section class="content">
			<div class="row">
				<!-- left column -->
				<div class="col-md-8">
				<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><?php echo lang('categories_add');?></h3>
						</div><!-- /.box-header -->
						<!-- form start -->
						<?php
						$url = current_url();
						?>
						<form method="post"  action="<?php echo $url;?>" role="form" name="categories_from" id="categories_from" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1"><?php echo lang('category_name'); ?></label>
									<input type="text" value="<?php if($action=='edit') echo $category->cat_name; ?>" placeholder="<?php echo lang('category_name'); ?>" name="cat_name" id="cat_name" class="form-control">
								</div>
							</div><!-- /.box-body -->
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</form>
					</div><!-- /.box -->
                </div><!--/.col (left) -->
				<!-- right column -->
            </div>   <!-- /.row -->
		</section>
	</div>
</div>
<?php echo $script;?>