<div class="row">
    <div class="col-md-12">
    <!-- col-md-offset-2 -->
        <section class="content">
            <div class="row">
                <?php $url = current_url();?>
                <form method="post"  action="<?php echo $url;?>" role="form" name="settings_from" id="settings_from" enctype="multipart/form-data">
                <!-- left column -->
                    <div class="col-md-6">
                    <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">
                                    <?php if($action=='edit')echo lang('posts_edit'); else echo lang('posts_add');?>
                                </h3>
                            </div><!-- /.box-header -->
                                <!-- form start -->
                                
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo lang('cat_title'); ?><span class="validate"> *</span> </label>
                                    <select id="cat_id" name="cat_id" class="form-control">
                                    <option value=''>Select Post Category</option>
                                    <?php foreach($categories as $category)	{?>
                                    <option value = "<?php echo $category->cat_id;?>" <?php if($action=='edit' && $category->cat_id == $post->cat_id){?>Selected<?php } ?>><?php echo $category->cat_name; ?></option>	
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo lang('post_title'); ?><span class="validate"> *</span></label>
                                    <input type="text" value="<?php if($action=='edit') echo $post->post_title; ?>" placeholder="<?php echo lang('post_title'); ?>" name="post_title" id="post_title" class="form-control">
                                </div>
								
                                <div class="form-group">
                                    <label for="exampleInputFile"><?php echo lang('post_image'); ?></label>
                                    <input type="file" name="post_image" id="post_image">
                                    <input type="hidden" name="post_thumb_image" value="<?php if($action=='edit') {echo $post->post_thumb_image;} ?>"/>
                                </div>
								
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('meta_title'); ?></label>
                                    <input type="text" value="<?php if($action=='edit') echo $post->meta_title; ?>" placeholder="<?php echo lang('meta_title'); ?>" name="meta_title" id="meta_title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('meta_keywords'); ?></label>
                                    <textarea id="meta_keywords" name="meta_keywords" rows="2" cols="68"><?php if($action=='edit') echo $post->meta_keywords; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('meta_desc'); ?></label>
                                    <textarea id="meta_desc" name="meta_desc" rows="3" cols="68"><?php if($action=='edit') echo $post->meta_desc; ?></textarea>
                                </div>  
                                       
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!--/.col (left) -->
                    <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('disp_sidebar'); ?></label>
                                    <input type="checkbox" id="sidebar_status" name="sidebar_status" value ="1" class="form_control" <?php if($action=='edit' && $post->sidebar_status == 1){?>checked<?php } ?>  />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('post_desc'); ?></label>
                                    <textarea id="post_desc" name="post_desc" rows="10" cols="80">
                                        <?php if($action=='edit') echo $post->post_description; ?>
                                    </textarea>
                                </div>
                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                               
                        </div><!-- /.box -->
                    </div><!--/.col (right) -->
                    <!-- right column -->
                 </form>        
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
<?php echo $script;?>
<script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
  <script type="text/javascript">
    $(function() {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('post_desc');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
</script>