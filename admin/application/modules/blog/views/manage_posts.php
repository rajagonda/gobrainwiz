<section class="content">
	<div class="row">
		<div class="col-md-12">
		   <div class="box">
				<div class="box-header">
					<h3 class="box-title"><?php echo lang('manage_posts');?></h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<th style="width: 10px"><?php echo lang('Sno');?></th>
								<th style="text-align:center !important;"><?php echo lang('post_title');?></th>
								<th style="text-align:center !important;"><?php echo lang('status');?></th>
								<th>
									<a class="blue" href="<?php echo base_url();?>blog/posts/save">
										<span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
									</a>
								</th>
							</tr>
							<?php
							if($posts !='')
							{
								$i=1;
								$j=1;
								foreach ($posts as  $value) 
								{
							?>
									<tr id='cat_<?php echo $value->post_id;?>'>
										<td><?php echo $i;?></td>
										<td align="center"><?php echo $value->post_title;?></td>
										<td align="center" id="status<?php echo $j;?>">
										 <?php
										if($value->status == '0') {
										?>
							
											<a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->post_id;?>', '<?php echo $value->status;?>')">
											<i class="ace-icon fa fa-close bigger-130"></i>
										</a>
										<?php } else { ?>
										<a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->post_id;?>', '<?php echo $value->status;?>')">
											<i class="ace-icon fa fa-check bigger-130"></i>
										</a>
										<?php } ?> 
										</td>
										<td>
											<div class="hidden-sm hidden-xs action-buttons">
												<a class="green" href="<?php echo base_url();?>blog/posts/save/<?php echo $value->post_id;?>" >
													<i class="ace-icon fa fa-pencil bigger-130"></i>
												</a>
											</div>
										</td>
									</tr>
								  <?php
									$i++;
									$j++;
								}
							}
						  ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
				<div class="box-footer clearfix">
					<?php echo $links; ?>
				</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</section>


<script type="text/javascript">
  /**
     * By using this method change role status
     * 
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */
function changeStatus(id, post_id,  status)
{ 
	var p_url= "<?php echo base_url(); ?>blog/posts/changepostStatus";
	
	var ajaxLoading = false;
	if(!ajaxLoading) 
	{
		var ajaxLoading = true;
		$('#'+id).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
		jQuery.ajax({
			type: "POST",             
			url: p_url,
			data: 'post_id='+post_id+'&status='+status,
			success: function(data) 
			{

				if(data == 1)
				{
					$('#'+id).html('<a class="green" href="#" onclick="changeStatus(\''+id+'\', \''+post_id+'\', \'1\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
				} 
				else 
				{
					$('#'+id).html('<a class="red" href="#" onclick="changeStatus(\''+id+'\', \''+post_id+'\', \'0\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
				 
				}
				ajaxLoading = false;
			}
	 
		});  

	}
}
</script>