<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Sunil 
 * Project : Brainwiz
 * Version v1.0
 * Controller : Categories
 */
class Categories extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		
		if(!$this->authentication->checklogin()){
			redirect('login');
        }
		
	    $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('categories_model');
        $this->load->language('blog');
        $this->load->config('categories');
	}
	
	public function index() 
	{
            $breadcrumbarray = array('label'=> "Categories",
                           'link' => base_url()."blog/categories"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Categories");
	    $data['categories'] = $this->categories_model->get();
	    $this->template->load_view1('manage_categories',$data);

	}
	
	public function save($id=null)
	{
        $breadcrumbarray = array(
							'label'=> "Categories",
                            'link' => base_url()."blog/categories"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Categories");
        $validationRules = $this->_rules();
		
        foreach ($validationRules as $form_field)   {
			$rules[] = array(
				'name' => $form_field['field'],
				'display' => $form_field['label'],
				'rules' => $form_field['rules']
			);
        }

      
		$json_rules = json_encode($rules);
		$script = <<< JS
				<script>
				var CIS = CIS || { Script: { queue: [] } };
				CIS.Form.validation('categories_from',{$json_rules});
				</script>
JS;

            if($id !='')
		    {
				$data['action'] = "edit";
				$data['category'] = $this->categories_model->getCategoryDetails($id);
            }
			else 
			{
				$data['action'] = "add";
            }

            $this->form_validation->set_rules($validationRules);
            
			if (isset($_POST) && is_array($_POST) && count($_POST) > 0)
			{
				$category_values['cat_name'] = $this->input->post('cat_name');
           
				if($id !='')
				{
					$res = $this->categories_model->update($category_values,$id);
				}
				else 
				{
					$res = $this->categories_model->save($category_values);
				}
				redirect('blog/categories');
        
			}

			$data['script'] = $script;
			$this->template->load_view1('categories_setup',$data);
	}

	public function _rules() 
	{
        $rules = array(
					array('field' => 'cat_name','label' => lang('category_name'),'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }
	
    public function changecategoryStatus()
	{
        $id = $_POST['cat_id'];
        $staus = $_POST['status'];
        if($staus == '1') {
            $formvalues['status'] = '0';
            echo "0";
        } else {
            $formvalues['status'] = '1';
            echo "1";
        }
        $this->categories_model->changecategoryStatus($formvalues, $id);
    }
   
}

/* End of file categories.php */
/* Location: ./application/controllers/categories.php */