<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Sunil 
 * Project : Brainwiz
 * Version v1.0
 * Controller : Posts
 */
class Posts extends MY_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        if(!$this->authentication->checklogin()){
            redirect('login');
        }
		
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('posts_model');
        $this->load->language('posts');
        $this->load->library('pagination');
        $this->load->config('posts');
        $slug_config = array(
            'field' => 'post_slug',
            'title' => 'title',
            'table' => 'blog_posts',
            'id' => 'post_id'
        );
        $this->load->library('slug', $slug_config);
    }
	
    public function index() 
    {
        $breadcrumbarray = array('label'=> "Posts",
                       'link' => base_url()."blog/posts"
                       );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Posts");

        $config['base_url'] = base_url().'/blog/posts/'; 
        $config["total_rows"] = $this->posts_model->postcount();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] =  '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';		

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["posts"] = $this->posts_model->get($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $this->template->load_view1('manage_posts',$data);

    }
	
    public function save($id=null)
    {
        $breadcrumbarray = array('label'=> "Posts",'link' => base_url()."blog/posts");
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Posts");
        $data['categories'] =  $this->posts_model->getCategories();
        $validationRules = $this->_rules();
		
        foreach ($validationRules as $form_field)   {
            $rules[] = array(
                    'name' => $form_field['field'],
                    'display' => $form_field['label'],
                    'rules' => $form_field['rules']
            );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
                <script>
                var CIS = CIS || { Script: { queue: [] } };
                CIS.Form.validation('settings_from',{$json_rules});
                </script>
JS;

        if($id !='')
        {
            $data['action'] = "edit";
            $data['post'] = $this->posts_model->getPostDetails($id);
            
            $post_values['modified_on'] = date('Y-m-d H:i:s'); 
            if(strcmp($data['post']->post_title,$this->input->post('post_title'))!=0)
                $post_values['post_slug'] = $this->slug->create_uri($this->input->post('post_title'));
        }
        else 
        {
            $data['action'] = "add";
            $post_values['created_on'] = date('Y-m-d H:i:s'); 
            $post_values['post_slug'] = $this->slug->create_uri($this->input->post('post_title'), $id);
        }
     
        $this->form_validation->set_rules($validationRules);
            
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0)
        {
            if($_FILES['post_image']['name'] !='')
            {
                $filename = time()."_".$_FILES['post_image']['name'];
                $config['upload_path'] = '../upload/post_images/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
            
                if (!$this->upload->do_upload('post_image')) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                } 
                else 
                {
                }
                $this->data = $this->upload->data();
                $post_image = $this->data['file_name'];
                $resized_image = $this->resizeImage($post_image);
                if($resized_image){
                    $split = explode('.', $resized_image);
                    $thumb_image =  $split[0] . '_thumb.' . $split[1];
                }
					
            }
            else 
            {
                if($id !='')
                {
                    $post_image  = $data['post']->post_image;
                    $thumb_image = $data['post']->post_thumb_image;
                }
                else{
                    $post_image  = '';
                    $thumb_image = '';
                }
            }

            $post_values['post_image'] = $post_image;
            $post_values['post_thumb_image'] = $thumb_image;
            
            $post_values['cat_id'] = $this->input->post('cat_id');
            $post_values['post_title'] = trim($this->input->post('post_title'));
            $post_values['meta_title'] = trim($this->input->post('meta_title'));
            $post_values['meta_keywords'] = trim($this->input->post('meta_keywords'));
            $post_values['meta_desc'] =  trim($this->input->post('meta_desc'));
            $post_values['sidebar_status'] = $this->input->post('sidebar_status'); 
            $post_values['post_description'] = trim($this->input->post('post_desc')); 
				
				
            if($id !='')
            {
                $res = $this->posts_model->update($post_values,$id);
            }
            else 
            {
                $res = $this->posts_model->save($post_values);
            }
            redirect('blog/posts');
        
        }

        $data['script'] = $script;
        $this->template->load_view1('posts_setup',$data);
    }

    public function _rules() 
    {
        $rules = array(
            
                array('field' => 'cat_id','label' => lang('cat_title'),'rules' => 'required'),
                array('field' => 'post_title','label' => lang('post_title'),'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }
	
    public function changepostStatus()
    {
        $id = $_POST['post_id'];
        $staus = $_POST['status'];
        if($staus == '1') {
            $formvalues['status'] = '0';
            echo "0";
        } else {
            $formvalues['status'] = '1';
            echo "1";
        }
        $this->posts_model->changepostStatus($formvalues, $id);
    }
	
    public function resizeImage($filename)
    {
        $source_path = '../upload/post_images/' . $filename;
        $target_path = '../upload/post_images/thumbnail/';

        $config_manip = array(
                'image_library' => 'gd2',
                'source_image' => $source_path,
                'new_image' => $target_path,
                'maintain_ratio' => TRUE,
                'create_thumb' => TRUE,
                'thumb_marker' => '_thumb',
                'width' => 360,
                'height' => 241
        );

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) 
        {
                echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();

        return $filename;
    }

   
}

/* End of file posts.php */
/* Location: ./application/controllers/posts.php */