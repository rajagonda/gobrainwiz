<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Cashback New Notification</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?=current_url()?>" role="form" name="job_form" id="job_form" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                <label>Title :</label>
                                <input type="text" placeholder="Name" value="" name="title" id="title" class="form-control">
                            </div>
                                
                            <div class="form-group">
                                <label>Message :</label>
                                <textarea placeholder="Description"  cols="" name="message" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Sent To :</label>
                                <select name="sendTo">
                                    <?php 
                                    foreach ($sendToOptions as $index => $opt) {
                                        ?>
                                            <option value="<?=$index?>" <?=$index=="0" ? "selected":""?>><?=$opt?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Image :</label>
                                <input type="file" name="image" id="image">
                            </div>
                            
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
