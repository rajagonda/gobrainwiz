<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#withdraw<?php echo $value->examuser_id; ?>">
    View
</button>

<!-- Modal -->
<div class="modal fade" id="withdraw<?php echo $value->examuser_id; ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Withdraw requests of <?php echo $value->examuser_name; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width: 10px"> Sno</th>
                        <th> Amount</th>
                        <th> Description</th>
                        <th> Paid Amount</th>
                        <th>Comments by Admin</th>
                        <th> Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count(getwithdrawrequestsByUser($value->examuser_id)) > 0) {
                        foreach (getwithdrawrequestsByUser($value->examuser_id) as $index => $record) {
                            ?>
                            <tr>
                                <td> <?= $index + 1 ?></td>
                                <td title="<?= $record->wr_requested_amount ?>"> <?= $record->wr_requested_amount ?></td>
                                <td title="<?= $record->wr_requested_user_description ?>"> <?= $record->wr_requested_user_description ?></td>
                                <td title="<?= $record->wr_paid_amount ?>"> <?= $record->wr_paid_amount ?></td>
                                <td title="<?= $record->wr_admin_description ?>"> <?= $record->wr_admin_description ?></td>
                                <td title="<?= $record->wr_status ?>"> <?= $record->wr_status ?></td>

                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="6" style="text-align:center">No Records Found</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--<a class="green" title="User Withdraw List"-->
<!--   href="--><?php //echo base_url(); ?><!--examusers/withdrawrequestsByUser/--><?php //echo $value->examuser_id; ?><!--">-->
<!--    View-->
<!--</a>-->