<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th><input type="checkbox" class="checkRow" name="checkRow1" id="checkAll"/></th>

            <th style="width: 10px"><?php echo lang('Sno'); ?></th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Role</th>
            <th>Gender</th>
            <th>Create</th>
            <th style="text-align:center !important;"><?php echo lang('status'); ?></th>
            <th>Wallet</th>
            <th>Withdraw requests</th>
            <th>
                <a class="blue" title="Add Test" href="<?php echo base_url(); ?>examusers/save">
                    <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                </a>
                <a class="blue" title="Upload Exam Users" href="<?php echo base_url(); ?>examusers/uploadusers/">
                    <span class="glyphicon glyphicon-upload " style="font-size:150%;color:#438EB9;;"></span>
                </a>
            </th>

        </tr>
        <?php
        if ($examusers != '') {
            $i = 1;
            $j = 1;
            foreach ($examusers as $value) {
                ?>

                <tr id='com_<?php echo $value->examuser_id; ?>'>
                    <td><input type="checkbox" id="titleCheck<?php echo $j; ?>"
                               value="<?php echo $value->examuser_id; ?>" class="checkRow1" name="checkRow"/></td>
                    <td><?php echo $i; ?></td>
                    <td align="center"><?php echo $value->examuser_name; ?></td>
                    <td align="center"><?php echo $value->examuser_mobile; ?></td>
                    <td align="center"><?php echo $value->examuser_email; ?></td>
                    <td align="center"><?php echo $value->examuser_role; ?></td>
                    <td align="center"><?php echo $value->examuser_gender; ?></td>
                    <td align="center"><?php echo $value->examuser_create; ?></td>
                    <td align="center" id="status<?php echo $j; ?>">
                        <?php
                        if ($value->examuser_status == 'n') {
                            ?>

                            <a class="red" href="#"
                               onclick="changeStatus('status<?php echo $j; ?>', '<?php echo $value->examuser_id; ?>', '<?php echo $value->examuser_status; ?>')">
                                <i class="ace-icon fa fa-close bigger-130"></i>
                            </a>
                        <?php } else { ?>
                            <a class="green" href="#"
                               onclick="changeStatus('status<?php echo $j; ?>', '<?php echo $value->examuser_id; ?>', '<?php echo $value->examuser_status; ?>')">
                                <i class="ace-icon fa fa-check bigger-130"></i>
                            </a>
                        <?php } ?>
                    </td>
                    <td>
                        <?php include('walletdiv.php'); ?>
                    </td>
                    <td>
                        <?php include('withdrawdiv.php'); ?>

                    </td>
                    <td>
                        <a class="green" title="Edit Question"
                           href="<?php echo base_url(); ?>examusers/save/<?php echo $value->examuser_id; ?>">
                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>
                        |
                        <a class="red trash" onclick="abcd('<?php echo $value->examuser_id; ?>')"
                           id="<?php echo $value->examuser_id; ?>" href="#">
                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                        </a>
                    </td>

                </tr>
                <?php
                $i++;
                $j++;
            }
        }
        ?>
        </tbody>
    </table>
</div><!-- /.table-responsive -->