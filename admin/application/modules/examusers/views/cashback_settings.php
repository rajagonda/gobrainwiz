<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<div class="row">
<?php
    if(!is_object($cashbackSettings)) {
?>
    <div class="col-md-12">
        <section class="content">
            <div class="row">
                <form method="post" action="<?=$_SERVER['PHP_SELF']?>" role="form" name="cashback_settings_form" id="cashback_settings_form" enctype="multipart/form-data">
                    <div class='col-md-4'>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">New Cashback Settings</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class='input-group date' id='datetimepicker6'>
                                        <input type='text' required value="" name="start_date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <div class='input-group date' id='datetimepicker7'>
                                        <input type='text' required value="" name="end_date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>    
    </div>
<?php
    }
?>
<?php
    if(is_object($cashbackSettings))
    {
        $paymentDate = date('Y-m-d H:i');
        $paymentDate=date('Y-m-d H:i', strtotime($paymentDate));
        $contractDateBegin = date('Y-m-d H:i', strtotime($cashbackSettings->start_date));
        $contractDateEnd = date('Y-m-d H:i', strtotime($cashbackSettings->end_date));

        if (($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd))
        {
?>
    <div class="col-md-12">
        <section class="content">
            <div class="row">
                <form method="post" action="<?=base_url() . "examusers/cashback"?>/edit/<?=$cashbackSettings->id?>" role="form" name="cashback_settings_form" id="cashback_settings_form" enctype="multipart/form-data">
                    <div class='col-md-4'>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Edit Current Active Cashback Settings</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class='input-group date' id='datetimepicker6'>
                                        <input type='text' required readonly value="<?=date("d/m/Y H:i", strtotime($cashbackSettings->start_date))?>" name="start_date" class="form-control" />
                                        <!-- <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <div class='input-group date' id='datetimepicker7'>
                                        <input type='text' required value="<?=date("d/m/Y H:i", strtotime($cashbackSettings->end_date))?>" name="end_date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>    
    </div>
<?php
        }
    }
?>
    <div class="col-md-12">
        <section class="content">
            <div class="row">
                <?php 
                    if(is_object($cashbackSettings)) {
                ?>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Current Active Cashback Settings</h3>
                        </div><!-- /.box-header -->
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td><?=date("d-m-Y h:i:s a", strtotime($cashbackSettings->start_date))?></td>
                                    <td><?=date("d-m-Y h:i:s a", strtotime($cashbackSettings->end_date))?></td>
                                    <td><?=date("d-m-Y h:i:s a", strtotime($cashbackSettings->created_at))?></td>
                                    <td>
                                        <a href="<?=current_url()."/removeCurrentCashbackSettings/".$cashbackSettings->id?>"> <button> Remove </button></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php 
                    }
                ?>
                <?php 
                    if(!is_object($cashbackSettings)) {
                ?>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title" style="color:red">Cashback Settings Currently Not Active</h3>
                        </div><!-- /.box-header -->
                    </div>
                <?php 
                    }
                ?>
            </div>
        </section>    
    </div>
    <div class="col-md-12">
        <section class="content">
            <div class="row">
                <?php 
                    if(count($prevCashbackSettings) > 0) {
                ?>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Removed/Expired Cashback Settings</h3>
                        </div><!-- /.box-header -->
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>S No</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Created At</th>
                                    <th>Deleted At</th>
                                </tr>
                                <?php 
                                foreach ($prevCashbackSettings as $index => $pcs) {
                                    ?>
                                    <tr>
                                        <td><?=$index+1?></td>
                                        <td><?=date("d-m-Y h:i:s a", strtotime($pcs->start_date))?></td>
                                        <td><?=date("d-m-Y h:i:s a", strtotime($pcs->end_date))?></td>
                                        <td><?=date("d-m-Y h:i:s a", strtotime($pcs->created_at))?></td>
                                        <td>
                                            <?=date("d-m-Y h:i:s a", strtotime($pcs->deleted_at))?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php 
                    } else {
                ?>
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Removed/Expired Cashback Settings Not Found</h3>
                        </div><!-- /.box-header -->
                    </div>
                <?php 
                    }
                ?>
            </div>
        </section>    
    </div>
</div>        <!-- bootstrap time picker -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
        });
        $('#datetimepicker7').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>

