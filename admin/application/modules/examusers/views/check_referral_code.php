<div class="row">
    <div class="col-md-12">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-10">
                    <!-- general form elements -->
                    <div class="box box-primary" style="width: 50%">
                        <!-- form start -->
                        <form method="get" action="<?=$_SERVER['PHP_SELF']?>" role="form" name="cashback_settings_form" id="cashback_settings_form" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Select Option</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="mobile" <?=($type == 'mobile')? 'checked="checked"':""?> <?=($type != 'code' && $type != 'mobile')? 'checked="checked"':""?> name="type">
                                            Mobile Number
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio"  value="code" <?=($type == 'code')? 'checked="checked"':""?>  name="type">
                                            Referral Code
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" value="<?=($referral_code != '')?$referral_code:""?>" name="value" class="form-control" style="width: 50%">
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div><!-- /.box -->
                    <?php 
                        if($referral_code != '' && is_object($examuser)) {
                    ?>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">User Details</h3>
                            </div><!-- /.box-header -->
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td><?=$examuser->examuser_name?></td>
                                    </tr>
                                    <tr>
                                        <th>Mobile</th>
                                        <td><?=$examuser->examuser_mobile?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?=$examuser->examuser_email?></td>
                                    </tr>
                                    <tr>
                                        <th>Share Count</th>
                                        <td><?=isset($data['referralData']) && is_object($data['referralData']) ? $data['referralData']->shareCount : "0"?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php 
                        }
                    ?>
                    <?php 
                        if($referral_code != '' && !is_object($examuser)) {
                    ?>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">No Records Found</h3>
                            </div><!-- /.box-header -->
                        </div>
                    <?php 
                        }
                    ?>

                    <?php 
                        if($referral_code != '' && isset($data['referredData']) && is_object($data['referredData']) && isset($data['cashbackByUserDetailsById'][(int)$data['referredData']->old_examuser_id])) {
                            $referredUser = $data['cashbackByUserDetailsById'][(int)$data['referredData']->old_examuser_id];
                    ?>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"> Referred User Details</h3>
                            </div><!-- /.box-header -->
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td><?=$referredUser->examuser_name?></td>
                                    </tr>
                                    <tr>
                                        <th>Mobile</th>
                                        <td><?=$referredUser->examuser_mobile?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?=$referredUser->examuser_email?></td>
                                    </tr>
                                    <tr>
                                        <th>Referral Code</th>
                                        <td><?=$data['referredData']->referral_code?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Cashback Referrals</th>
                                        <td><?=$data['referredData']->total_cashback_referrals?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php 
                        }
                    ?>
                    <?php 
                        if($referral_code != '' && isset($data['shareData'])) {
                            $totalShares = count($data['shareData']);
                            $totalJoins = 0;
                            foreach ($data['shareData'] as $row) {
                                if($row->joined == "1")
                                    $totalJoins++;
                            }
                    ?>
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Share Information</h3>
                            </div><!-- /.box-header -->
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Registrations</th>
                                        <th>Joins</th>
                                    </tr>
                                    <tr>
                                        <td><?=$totalShares?></td>
                                        <td><?=$totalJoins?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if(count($data['shareData']) > 0) {
                            ?>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Created At</th>
                                        <th>Joined At</th>
                                    </tr>
                                    <?php 
                                    foreach ($data['shareData'] as $row) {
                                        $referredUser = isset($data['cashbackByUserDetailsById'][(int)$row->new_examuser_id]) ? $data['cashbackByUserDetailsById'][(int)$row->new_examuser_id] : false;
                                        if(is_object($referredUser)) {
                                            ?>
                                            <tr>
                                                <td><?=$referredUser->examuser_name?></td>
                                                <td><?=$referredUser->examuser_mobile?></td>
                                                <td><?=$referredUser->examuser_email?></td>
                                                <td><?=date("d-m-Y h:i:s a", strtotime($row->createdAt))?></td>
                                                <td><?=$row->joined == "1" ? date("d-m-Y h:i:s a", strtotime($row->joinedAt)) : ""?></td>
                                            </tr>
                                            <?php
                                        } else {
                                        ?>
                                            <tr>
                                                <td colspan="5" style="text-align:center">Record Deleted</td>
                                            </tr>
                                        <?php 
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                                <?php }
                                ?>
                        </div>
                    <?php 
                        }
                    ?>
                </div><!--/.col (left) -->
                <!-- right column -->
                
            </div>   <!-- /.row -->
            
        </section>
    </div>
</div>        <!-- bootstrap time picker -->

