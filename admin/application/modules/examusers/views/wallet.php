<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('wallet_message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('wallet_message');
                                $this->session->set_userdata('wallet_message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Wallet

                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th> Coupon ID</th>
                                <th> Coupon Type</th>
                                <th> Price Type</th>
                                <th> Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($wallet) > 0) {
                                foreach ($wallet as $index => $record) {
                                    ?>
                                    <tr>
                                        <td> <?= $index + 1 ?></td>
                                        <td title="<?php if(isset($record->uw_coupon_id)){ echo getCouponCodeFromID($record->uw_coupon_id); }  ?>"> <?php if(isset($record->uw_coupon_id)){ echo getCouponCodeFromID($record->uw_coupon_id); } ?></td>

                                        <td title="<?= $record->uw_coupon_type ?>"> <?= $record->uw_coupon_type ?></td>
                                        <td title="<?= $record->uw_price_type ?>"> <?= $record->uw_price_type ?></td>
                                        <td title="<?= $record->uw_price ?>"> <?= $record->uw_price ?></td>

                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="5" style="text-align:center">No Records Found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
</section>