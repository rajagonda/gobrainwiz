<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal"
        data-target="#wallet<?php echo $value->examuser_id; ?>">
    View
</button>

<!-- Modal -->
<div class="modal fade" id="wallet<?php echo $value->examuser_id; ?>" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Wallet of <?php echo $value->examuser_name; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width: 10px"> Sno</th>
                        <th> Coupon ID</th>
                        <th> Coupon Type</th>
                        <th> Price Type</th>
                        <th> Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count(getWalletByUser($value->examuser_id)) > 0) {
                        foreach (getWalletByUser($value->examuser_id) as $index => $record) {
                            ?>
                            <tr>
                                <td> <?= $index + 1 ?></td>
                                <td title="<?php if (isset($record->uw_coupon_id)) {
                                    echo getCouponCodeFromID($record->uw_coupon_id);
                                } ?>"> <?php if (isset($record->uw_coupon_id)) {
                                        echo getCouponCodeFromID($record->uw_coupon_id);
                                    } ?></td>

                                <td title="<?= $record->uw_coupon_type ?>"> <?= $record->uw_coupon_type ?></td>
                                <td title="<?= $record->uw_price_type ?>"> <?= $record->uw_price_type ?></td>
                                <td title="<?= $record->uw_price ?>"> <?= $record->uw_price ?></td>

                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="5" style="text-align:center">No Records Found</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--<a class="green" title="User Wallet"-->
<!--   href="--><?php //echo base_url(); ?><!--examusers/wallet/--><?php //echo $value->examuser_id; ?><!--">-->
<!--    View-->
<!--</a>-->
