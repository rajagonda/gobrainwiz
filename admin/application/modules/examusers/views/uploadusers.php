<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Upload Exam Users List</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
							<?php 							
							if(!empty($error))				
							{							?>
							<div class="alert alert-danger"><?php echo $error;?></div>	
							<?php 							
							}	
							?>							
							<?php echo form_open_multipart('examusers/do_upload');?>															
							<div class="form-group">          
							<input type="file" name="userfile" size="20" class="form-control"/>										
							<input type="hidden" name="testId" value="" />							                    
							</div>						
							<br /><br />
							
								<div class="pull-center">
								
									<input type="submit" value="upload" class="btn btn-primary" />	
								</div>
							</form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
					
					
					
                </section>
            </div>
        </div>
