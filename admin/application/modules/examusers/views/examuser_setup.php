<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Add Exam User</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <?php
                        $url = current_url();
                        ?>
                        <form method="post" action="<?php echo $url; ?>" role="form" name="examuser_from"
                              id="examuser_from" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text"
                                           value="<?php if ($action == 'edit') echo $user->examuser_name; ?>"
                                           placeholder="Name" name="examuser_name" id="examuser_name"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mobile</label>
                                    <input type="text"
                                           value="<?php if ($action == 'edit') echo $user->examuser_mobile; ?>"
                                           placeholder="Mobile" name="examuser_mobile" id="examuser_mobile"
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="text"
                                           value="<?php if ($action == 'edit') echo $user->examuser_email; ?>"
                                           placeholder="<?php echo lang('com_name'); ?>" name="examuser_email"
                                           id="examuser_email" class="form-control">
                                </div>


                                <div class="form-group">
                                    <label for="exampleInputEmail1">Examuser Type</label>
                                    <select name="examuser_type" id="examuser_type" class="form-control">
                                        <option value="y">Brainwiz</option>
                                        <option value="n">Register</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Role</label>
                                    <select name="examuser_role" id="examuser_role" class="form-control">
                                        <option value="">Select Role</option>
                                        <?php

                                        foreach (getUserRoles() as $role) {
                                            ?>
                                            <option value="<?php echo $role; ?>" <?php if ($action == 'edit') echo ($user->examuser_role == $role) ? 'selected' : '' ?>><?php echo $role; ?></option>

                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gender</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="Male" checked name="examuser_gender"
                                                   id="examuser_gender">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="Female" name="examuser_gender"
                                                   id="examuser_gender" <?php if ($action == 'edit') echo ($user->examuser_gender == 'Female') ? 'checked' : '' ?>>
                                            Female
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Payment Options</label>

                                    <?php

                                    if ($action == 'edit' && $user->payment_options != '') {
                                        $paymentoptions = explode(',', $user->payment_options);
                                    }


                                    foreach (getUserPaymentOptions() as $type) {
                                        ?>

                                        <div class="radio">
                                            <label>
                                                <input type="checkbox" value="<?php echo $type; ?>"
                                                       name="payment_options[]"
                                                       id="<?php echo $type; ?>"

                                                    <?php if ($action == 'edit' && $user->payment_options != '') {
                                                        if (in_array($type, $paymentoptions)) {
                                                            echo "checked";

                                                        }
                                                    }

                                                    ?>>
                                                <?php echo $type; ?>
                                            </label>
                                        </div>

                                        <?php
                                    }
                                    ?>

                                </div>


                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div><!-- /.box -->


                </div><!--/.col (left) -->
                <!-- right column -->

            </div>   <!-- /.row -->
        </section>
    </div>
</div>        <!-- bootstrap time picker -->

<?php
echo $script;
?>
