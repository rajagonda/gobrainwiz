<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Manage Exam Users List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">


                    <select name="apiId" id="apiId">
                        <option value="">Select Option</option>
                        <option value="DEL">Delete All</option>
                    </select>
<!--                    <select name="searchrole" id="searchrole">-->
<!--                        <option value="">Select Role</option>-->
<!--                        --><?php
//                        foreach (getUserRoles() as $role) {
//                            ?>
<!--                            <option value="--><?php //echo $role; ?><!--">--><?php //echo $role; ?><!--</option>-->
<!--                            --><?php
//                        }
//                        ?>
<!--                    </select>-->

                    <span class="">
                              <input type="text" value="" name="searchitem" id="searchitem"
                                     placeholder="Name or Mobile or Role"/>
                              <button class="btn btn-primary" type="button" id="sitem">Search</button></span>
                    <div id="ajaxUsers">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <th><input type="checkbox" class="checkRow" name="checkRow1" id="checkAll"/></th>

                                    <th style="width: 10px"><?php echo lang('Sno'); ?></th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Gender</th>
                                    <th>Create</th>
                                    <th style="text-align:center !important;"><?php echo lang('status'); ?></th>
                                    <th>Wallet</th>
                                    <th>Withdraw requests</th>
                                    <th>
                                        <a class="blue" title="Add Test" href="<?php echo base_url(); ?>examusers/save">
                                            <span class="glyphicon glyphicon-plus "
                                                  style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        <a class="blue" title="Upload Exam Users"
                                           href="<?php echo base_url(); ?>examusers/uploadusers/">
                                            <span class="glyphicon glyphicon-upload "
                                                  style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                    </th>

                                </tr>
                                <?php
                                if ($examusers != '') {
                                    $i = 1;
                                    $j = 1;
                                    foreach ($examusers as $value) {
                                        ?>

                                        <tr id='com_<?php echo $value->examuser_id; ?>'>
                                            <td><input type="checkbox" id="titleCheck<?php echo $j; ?>"
                                                       value="<?php echo $value->examuser_id; ?>" class="checkRow1"
                                                       name="checkRow"/></td>
                                            <td><?php echo $i; ?></td>
                                            <td align="center"><?php echo $value->examuser_name; ?></td>
                                            <td align="center"><?php echo $value->examuser_mobile; ?></td>
                                            <td align="center"><?php echo $value->examuser_email; ?></td>
                                            <td align="center"><?php echo $value->examuser_role; ?></td>
                                            <td align="center"><?php echo $value->examuser_gender; ?></td>
                                            <td align="center"><?php echo $value->examuser_create; ?></td>
                                            <td align="center" id="status<?php echo $j; ?>">
                                                <?php
                                                if ($value->examuser_status == 'n') {
                                                    ?>

                                                    <a class="red" href="#"
                                                       onclick="changeStatus('status<?php echo $j; ?>', '<?php echo $value->examuser_id; ?>', '<?php echo $value->examuser_status; ?>')">
                                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="green" href="#"
                                                       onclick="changeStatus('status<?php echo $j; ?>', '<?php echo $value->examuser_id; ?>', '<?php echo $value->examuser_status; ?>')">
                                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>

                                            <td>
                                                <?php include('walletdiv.php'); ?>
                                            </td>
                                            <td>
                                                <?php include('withdrawdiv.php'); ?>


                                            </td>
                                            <td>
                                                <a class="green" title="Edit Question"
                                                   href="<?php echo base_url(); ?>examusers/save/<?php echo $value->examuser_id; ?>">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>
                                                |
                                                <a class="red trash" id="<?php echo $value->examuser_id; ?>" href="#">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                            </td>

                                        </tr>
                                        <?php
                                        $i++;
                                        $j++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->                            </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <?php echo $links; ?>
                    </div>
                </div>
            </div>


        </div><!-- /.col -->

    </div><!-- /.row -->

</section>
<script type="text/javascript">
    // <![CDATA[
    $(document).ready(function () {


        $("#sitem").on('click', function () {
            // var searchitem = document.getElementById('searchitem').value();
            var searchitem = $('#searchitem').val();
            var role = $('#searchrole').val();

            var type = 'reg_n';
            if (searchitem != '') {

                $.ajax({
                    type: "POST",
                    //  data: "item="+ searchitem,
                    data: "item=" + searchitem + '&type=' + type + '&role=' + role,
                    url: '<?php echo base_url();?>examusers/searchusers1',
                    success: function (response) {
                        $("#ajaxUsers").html(response);
                    }
                });

            } else {
                alert("Please Enter Name or Mobile Number");
                return false;
            }

        });

        $("#apiId").change(function () {
            var selectApiVal = $('#apiId :selected').val();
            if (selectApiVal == 'DEL') {


                var checked = $(".checkRow1:checked").length > 0;
                if (!checked) {
                    alert("Please check at least one checkbox");
                    return false;
                } else {
                    if (confirm("Are You Sure to Update!")) {
                        var data = new Array();
                        $("input[name='checkRow']:checked").each(function (i) {

                            data.push($(this).val());
                        });

                        $('#loading').html('<img src="<?php echo base_url(); ?>public/images/sp/loading_small.gif">');
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>examusers/deleteall",
                            data: 'updatedata=' + data + '&action_update=deleteall',
                            success: function (data) {
                                //alert(data);

                                document.location.reload();
                            }
                        });

                    } else {
                        return false;
                    }
                }
            }
        });


    });


    function saveAccessPages(apiId, providerId, imageId, api) {
        var p_url = "<?php echo base_url(); ?>providers/UpdateProviderApi";
        $('#' + imageId).html('<img src="<?php echo base_url(); ?>public/images/sp/loading_small.gif">');
        jQuery.ajax({
            type: "POST",
            url: p_url,
            data: 'apiId=' + apiId + '&providerId=' + providerId + '&api=' + api,
            success: function (data) {
                $('#' + imageId).html('');
                //setTimeout(function() { $('#'+imageId).hide(1000); }, 1000)

            }


        });


    }


    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


    //      $('input[type="checkbox"], input[type="radio"]').on('ifChanged', function (e) {

    // alert("fff");

    //     if(!this.changed) {
    //         this.changed=true;
    //         $('.checkRow1').iCheck('check');
    //     } else {
    //         this.changed=false;
    //         $('.checkRow1').iCheck('uncheck');
    //     }
    //     $('.checkRow1').iCheck('update');


    // });


    //     $('#checkAll tbody tr td:first-child').next('td').css('border-left-color', '#CBCBCB');
</script>

<script type="text/javascript">


    function chk1(url) {
        if (confirm('Are you sure you want to delete this row?')) {
            return true;
        } else {
            return false;
        }
    }

    // $(".trash ").click(function() {

    $(".trash").on("click", function () {

        if (chk1()) {
            var del_id = $(this).attr('id');
            $.ajax({
                type: "POST",
                data: "id=" + del_id,
                url: '<?php echo base_url();?>examusers/delete',
                success: function (msg) {
                    $("#com_" + del_id).hide();
                }
            });

        }
        return false;
    });


    function changeStatus(id1, id, status) {
        var p_url = "<?php echo base_url(); ?>braintest/changeTestStatus";
        var ajaxLoading = false;
        if (!ajaxLoading) {
            var ajaxLoading = true;
            $('#' + id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
            jQuery.ajax({
                type: "POST",
                url: p_url,
                data: 'id=' + id + '&status=' + status,
                success: function (data) {
                    if (data == 1) {
                        $('#' + id1).html('<a class="green" href="#" onclick="changeStatus(\'' + id1 + '\', \'' + id + '\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');

                    } else {
                        $('#' + id1).html('<a class="red" href="#" onclick="changeStatus(\'' + id1 + '\', \'' + id + '\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');

                    }
                    ajaxLoading = false;
                }

            });

        }
    }

    function abcd(id) {
        if (confirm('Are you sure you want to delete this row?')) {
            var del_id = id;
            $.ajax({
                type: "POST",
                data: "id=" + del_id,
                url: '<?php echo base_url();?>examusers/delete',
                success: function (msg) {
                    $("#com_" + del_id).hide();
                }
            });
        } else {
            return false;
        }
    }


</script>
