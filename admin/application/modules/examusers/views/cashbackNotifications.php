<section class="content">
    <div class="row">
        <div class="col-md-12">
        <?php
            if($this->session->userdata('Notification_deleted')){
        ?>
            <div class="box" style="border-top: #fff;">
                <div class="box-header">
                    <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                        <p style="margin:10px">
                            <strong>SUCCESS: </strong>
                            <?php 
                                echo $this->session->userdata('Notification_deleted');
                                $this->session->set_userdata('Notification_deleted', "");
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php 
            } 
        ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Cashback Notifications 
                        <a class="blue" title="Add New Notification" href="<?php echo base_url(); ?>examusers/cashback/AddNotification" style="float:right">
							<span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;"></span>
						</a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 10px"> Sno </th>
                                    <th> Title </th>
                                    <th> Message </th>
                                    <th> SendTo </th>
                                    <th> Created At </th>
                                    <th> Last Sent At </th>
                                    <th> Send Notification </th>
                                    <th> Edit </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(count($cbNotifications) > 0) {
                                foreach ($cbNotifications as $index => $cbNotification) {
                                ?>
                                    <tr>
                                        <td> <?=$index+1?></td>
                                        <td title="<?=$cbNotification->title?>"> <?=substr($cbNotification->title, 0, 150)?><?=strlen($cbNotification->title) > 150 ? "...": "" ?> </td>
                                        <td title="<?=$cbNotification->message?>"> <?=substr($cbNotification->message, 0, 150)?><?=strlen($cbNotification->message) > 150 ? "...": "" ?> </td>
                                        
                                        <td> <?=$sendToOptions[$cbNotification->sendTo]?> </td>
                                        <td style="width: 200px"> <?=date("d-m-Y h:i:s a", strtotime($cbNotification->createdAt))?> </td>
                                        <td style="width: 200px"> <?=$cbNotification->lastSendAt != "" ? date("d-m-Y h:i:s a", strtotime($cbNotification->lastSendAt)) : ""?> </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">
                                                <a href="<?php echo base_url(); ?>examusers/cashback/sendNotification/<?=$cbNotification->id?>" style="padding: 14px;">
                                                    <i class="ace-icon fa fa-paper-plane bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">                                            
                                                <a class="green" href="<?php echo base_url(); ?>examusers/cashback/EditNotification/<?=$cbNotification->id?>" style="padding: 14px;">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">                                            
                                                <a class="red trash" href="<?php echo base_url(); ?>examusers/cashback/DeleteNotification/<?=$cbNotification->id?>" style="padding: 14px;">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                    <tr><td colspan="9" style="text-align:center">No Records Found</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>