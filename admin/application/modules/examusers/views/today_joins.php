<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <form method="get" role="form" name="examuser_from" id="examuser_from" enctype="multipart/form-data">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="checkbox" <?=$show_cashback_eligible == "on" ? "checked": ""?> name="show_cashback_eligible" id="show_cashback_eligible">
                                    <label for="show_cashback_eligible">Show Only Cashback Eligible</label>
                                </div>
                            </div>    
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="show_only_joined">Search By</label>
                                    <select name="search_by" id="search_by_select">
                                        <option value="createdAt" <?=$search_by == "createdAt" ? "selected": ""?>>Registered But Not Joined</option>
                                        <option value="joinedAt" <?=$search_by == "joinedAt" ? "selected": ""?>>Joined</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <!-- <input type="text" id='datepicker' name='date' value="<?=$start_date?>" class="form-control" readonly=""/> -->
                                    <div class='input-group date' id='datetimepicker6'>
                                        <input type='text' required value="<?=$start_date?>" name="start_date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker7'>
                                        <input type='text' required value="<?=$end_date?>" name="end_date" class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="submit">
                                </div>
                            </div>
                        </form>
                        <?php 
                            if($cashbackIsActive && count($todayJoins) > 0 && $show_cashback_eligible == "on" && date('d/m/Y') == $start_date && date('d/m/Y') == $end_date) {
                        ?>
                            <div class="col-md-2" style="float: right">
                                <div class="form-group">
                                    <a href="<?php echo base_url(); ?>examusers/cashback/downloadPaytmCashbackFile?start_date=<?=$start_date?>&end_date=<?=$end_date?>" download><button> Download Cashback Eligible List </button></a>
                                </div>
                            </div>
                        <?php 
                            }
                        ?>        
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Referral Joinees List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th> Sno </th>
                                    <th> Name </th>
                                    <th> Mobile Number </th>
                                    <th> Referred By </th>
                                    <th> Referred Mobile Number </th>
                                    <th> Registration Date </th>
                                    <th> Joined Date </th>
                                    <th> Referral Code </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(count($todayJoins) > 0) {
                                foreach ($todayJoins as $index => $joinee) {
                                    if(isset($userDetailsById[$joinee->new_examuser_id])) {
                                        $joineeData = $userDetailsById[$joinee->new_examuser_id];
                                    } else {
                                        $joineeData = (object)['examuser_name' => 'Record Deleted', 'examuser_mobile' => ""];
                                    }
                                    if(isset($userDetailsById[$joinee->old_examuser_id])) {
                                        $referredData = $userDetailsById[$joinee->old_examuser_id];
                                    } else {
                                        $referredData = (object)['examuser_name' => 'Record Deleted', 'examuser_mobile' => ""];
                                    }
                                ?>
                                    <tr>
                                        <td> <?=$index+1?></td>
                                        <td> <?=$joineeData->examuser_name?> </td>
                                        <td><a href="<?php echo base_url(); ?>examusers/cashback/CheckReferralCode?type=mobile&value=<?=$joineeData->examuser_mobile?>"> <?=$joineeData->examuser_mobile?> </a> </td>
                                        <td> <?=$referredData->examuser_name?> </td>
                                        <td><a href="<?php echo base_url(); ?>examusers/cashback/CheckReferralCode?type=mobile&value=<?=$referredData->examuser_mobile?>"> <?=$referredData->examuser_mobile?> </a> </td>
                                        <td> <?=date("d-m-Y h:i:s a", strtotime($joinee->createdAt))?> </td>
                                        <td> <?=$joinee->joined == "1" ? date("d-m-Y h:i:s a", strtotime($joinee->joinedAt)) : "Not Joined Yet"?> </td>
                                        <td> <?=$joinee->referral_code?> </td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                    <tr><td colspan="8" style="text-align:center">No Records Found</td></tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php
                if(count($todayJoins) > 0) {
                ?>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a style="float:right" href="<?php echo base_url(); ?>examusers/cashback/referralJoinees?start_date=<?=$start_date?>&end_date=<?=$end_date?>&search_by=<?=$search_by?>&show_cashback_eligible=<?=$show_cashback_eligible?>&download=true" download><button> Download </button></a>
                        </div>
                    </div>
                </div>
                <?php 
                }
                ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(function () { 
    //$("#datepicker").datepicker({ format: 'yyyy-mm-dd' }); 
});
$(function () {
    $('#datetimepicker6').datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $('#datetimepicker7').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false //Important! See issue #1075
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
});
</script>
