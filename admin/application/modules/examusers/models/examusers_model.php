<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class examusers_Model extends CI_Model
{
    public $variable;

    public function __construct()
    {
        parent::__construct();
    }

    public function insertMany($tablename, $data)
    {
        $this->db->insert(DB_PREFIX . $tablename, $data);
        return $this->db->insert_id();
    }

    public function updateMany($tablename, $data, $fname, $value)
    {
        $this->db->where($fname, $value);
        $this->db->update(DB_PREFIX . $tablename, $data);
        return $this->db->affected_rows();
    }

    public function deleteMany($tablename, $fname, $value)
    {
        $this->db->where($fname, $value);
        $this->db->delete(DB_PREFIX . $tablename);
    }

    public function userscount($type = null)
    {
        if ($type == 'reg') {
            $sql = "SELECT * FROM gk_examuserslist WHERE examuser_type='n'";
        } else {
            $sql = "SELECT * FROM gk_examuserslist WHERE examuser_type='y'";
        }

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function get($type = null, $limit, $start)
    {

        if ($type == 'reg') {
            $sql = "SELECT * FROM gk_examuserslist WHERE examuser_type='n'
           		  ORDER BY examuser_id DESC LIMIT $start, $limit";
        } else {
            $sql = "SELECT * FROM gk_examuserslist WHERE examuser_type='y'
			        ORDER BY examuser_id DESC LIMIT $start, $limit";
        }
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }

    }

    public function getsingleuser($uid)
    {

        $sql = "SELECT * FROM gk_examuserslist
                WHERE examuser_id ='$uid'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }

    }

    public function searchUsers($item)
    {
        $sql = "SELECT * FROM gk_examuserslist
                WHERE (`examuser_name` LIKE '%$item%' OR `examuser_mobile`='$item')";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

    public function searchUsers1($item, $type)
    {
        if ($type == 'reg') {
            $sql = "SELECT * FROM gk_examuserslist
                WHERE (`examuser_name` LIKE '%$item%' OR `examuser_mobile`='$item' OR `examuser_role`='$item') and examuser_type='n'";

        } else {
            $sql = "SELECT * FROM gk_examuserslist
                WHERE (`examuser_name` LIKE '%$item%' OR `examuser_mobile`='$item' OR `examuser_role`='$item') and examuser_type='y'";
        }



        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

    public function getSinglestudent($mobile)
    {
        $this->db->where('examuser_mobile', $mobile);
        $query = $this->db->get(DB_PREFIX . 'examuserslist');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function addstudent($data)
    {
        $this->db->insert(DB_PREFIX . 'examuserslist', $data);
        return $this->db->insert_id();

    }

    public function updateexamuser($data, $id)
    {
        $this->db->where('examuser_id', $id);
        $this->db->update(DB_PREFIX . 'examuserslist', $data);
        return $this->db->affected_rows();
    }

    public function getstudents($id, $test_status = null)
    {
        if ($id != '') {
            $this->db->where('brainTestId', $id);
        }
        if ($test_status != null) {
            $this->db->where('test_status', $test_status);

        }
        $query = $this->db->get(DB_PREFIX . 'braintest_student');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getQuestion($id, $set_id)
    {
        if ($id != '' && $set_id != '') {
            $sql = "SELECT q.*, a.category_name as category FROM gk_braintestquestions as q INNER JOIN gk_braintestquestioncat as a ON a.id = q.cat_id where q.test_id=" . $id . " and set_id=$set_id order by q.cat_id";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function getQuestionset($id)
    {
        if ($id != '') {
            $this->db->where('test_id', $id);
        }
        $query = $this->db->get(DB_PREFIX . 'braintestquestionset');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getQuestionsetById($set_id)
    {
        if ($set_id != '') {
            $this->db->where('set_id', $set_id);
        }
        $query = $this->db->get(DB_PREFIX . 'braintestquestionset');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getTestCategoryIdByName($catname)
    {
        if ($catname != '') {
            $this->db->where('category_name', $catname);
        }
        $query = $this->db->get(DB_PREFIX . 'braintestquestioncat');
        if ($query->num_rows() > 0) {
            $category = $query->result();
            $catID = $category[0]->id;
            return $catID;
        } else {
            $data['category_name'] = $catname;
            $this->db->insert(DB_PREFIX . 'braintestquestioncat', $data);
            $catID = $this->db->insert_id();
            return $catID;
        }

    }

    public function getAllCategoryByTestId($test_id)
    {
        if ($test_id == '') {
            return "";
        } else {
            $sql = 'SELECT *			FROM `gk_braintestquestioncat`			WHERE id			IN (			SELECT cat_id			FROM `gk_braintestquestions`			WHERE `test_id` =' . $test_id . '			GROUP BY `cat_id`			)';
            $query = $this->db->query($sql);
            $lastId = 0;
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return "";
            }
        }
    }

    public function getstudent($id)
    {

        if ($id != '') {
            $this->db->where('studentId', $id);
        }
        $query = $this->db->get(DB_PREFIX . 'braintest_student');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function save($data)
    {
        $this->db->insert(DB_PREFIX . 'braintests', $data);
        $testID = $this->db->insert_id();
        $this->createStudent($data, $testID);
        return $testID;
    }

    public function addquestion($data)
    {
        $this->db->insert(DB_PREFIX . 'braintestquestions', $data);
        $testID = $this->db->insert_id();
        return $testID;
    }

    public function addquestionSet($data)
    {
        $this->db->insert(DB_PREFIX . 'braintestquestionset', $data);
        $setID = $this->db->insert_id();
        return $setID;
    }

    public function laststudentID($testID)
    {
        $query = "SELECT `studentId` FROM " . DB_PREFIX . 'braintest_student' . "	WHERE `brainTestId` =$testID ORDER BY `studentId` DESC LIMIT 0 , 1";
        $student = $this->db->query($query);
        $lastId = 0;
        if ($student->num_rows() > 0) {
            $lastId = $student->result();
            $lastId = substr($lastId[0]->studentId, -3);
        } else {
            $lastId = 0;
        }
        return $lastId;
    }

    public function savestudent($data, $testID)
    {
        $lastId = $this->laststudentID($testID);
        $studentId = str_pad($lastId + 1, 3, '0', STR_PAD_LEFT);
        $data["studentId"] = "BW" . $testID . $studentId;
        $data["password"] = "br@inT3st" . $testID;
        $data["brainTestId"] = $testID;
        $this->db->insert(DB_PREFIX . 'braintest_student', $data);
        $query = "UPDATE " . DB_PREFIX . 'braintests' . " SET no_of_student = no_of_student + 1   WHERE brain_test_id =" . $testID;
        $query = $this->db->query($query);
    }

    public function createStudent($testdata, $testID)
    {
        $studentCount = $testdata['no_of_student'];
        $testcode = strtoupper(substr($testdata['test_name'], 0, 3));

        for ($i = 1; $i <= $studentCount; $i++) {
            $data = array();
            $studentId = str_pad($i, 3, '0', STR_PAD_LEFT);
            $data["studentId"] = "BW" . $testID . $studentId;
            $data["password"] = $testdata['student_password'];
            $data["brainTestId"] = $testID;
            $this->db->insert(DB_PREFIX . 'braintest_student', $data);
        }
    }

    public function update($data, $id)
    {
        $this->db->where('brain_test_id', $id);
        $this->db->update(DB_PREFIX . 'braintests', $data);
        return $this->db->affected_rows();
    }

    public function updatestudent($data, $id)
    {
        $this->db->where('studentId', $id);
        $this->db->update(DB_PREFIX . 'braintest_student', $data);
        return $this->db->affected_rows();
    }

    public function getTestDetails($id)
    {

        $this->db->where('brain_test_id', $id);
        $query = $this->db->get(DB_PREFIX . 'braintests');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function changeTestStatus($data, $id)
    {
        $this->db->where('brain_test_id', $id);
        $this->db->update(DB_PREFIX . 'braintests', $data);
        return $this->db->affected_rows();
    }

    public function changeStudentStatus($data, $id)
    {
        $this->db->where('studentId', $id);
        $this->db->update(DB_PREFIX . 'braintest_student', $data);
        return $this->db->affected_rows();
    }

    public function changequestionsetStatus($data, $id)
    {
        $this->db->where('set_id', $id);
        $this->db->update(DB_PREFIX . 'braintestquestionset', $data);
        return $this->db->affected_rows();
    }

    public function deleteTest($id)
    {
        $this->db->where('brain_test_id', $id);
        $this->db->delete(DB_PREFIX . 'braintests');
        $this->deleteStudentByTest($id);
        $allQusSet = $this->getQuestionset($id);
        foreach ($allQusSet as $qset) {
            $this->deleteQuestionSet($qset->set_id);
        }
    }

    public function deleteQuestionSet($id)
    {
        $qusSet = $this->getQuestionsetById($id);
        $file_name = $qusSet[0]->file_name;
        $path = './assets_new/uploads/' . $file_name;
        if (file_exists($path)) {
            unlink($path) or die('failed deleting: ' . $path);
        }
        $this->db->where('set_id', $id);
        $this->db->delete(DB_PREFIX . 'braintestquestionset');
        $this->deleteQuestionByset($id);
    }

    public function deleteuser($id)
    {
        $this->db->where('examuser_id', $id);
        $this->db->delete(DB_PREFIX . 'examuserslist');
    }

    public function deleteStudentByTest($id)
    {
        $this->db->where('brainTestId', $id);
        $this->db->delete(DB_PREFIX . 'braintest_student');
    }

    public function deleteStudent($id)
    {

        $row = $this->getstudent($id);
        $query = "UPDATE " . DB_PREFIX . 'braintests' . " SET no_of_student = no_of_student - 1   WHERE brain_test_id =" . $row->brainTestId;
        $query = $this->db->query($query);
        $this->db->where('studentId', $id);
        $this->db->delete(DB_PREFIX . 'braintest_student');
    }

    public function gettopics()
    {
        $sql = "SELECT gk_ptopics.*, a.category_name as category FROM gk_ptopics INNER JOIN gk_pcategories as a ON a.id = gk_ptopics.subcat_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function gettopicDetails($tpid)
    {
        $sql = "SELECT * FROM gk_ptopics WHERE  topic_id = $tpid";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function getPassages()
    {
        $sql = "SELECT c.*,gk_ptpassages.*, a.category_name as category FROM gk_ptpassages INNER JOIN gk_pcategories as a ON a.id = gk_ptpassages.subcat_id INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptpassages.topic_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getcattopics($catid)
    {
        $sql = "SELECT * FROM gk_ptopics WHERE gk_ptopics.subcat_id = $catid";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getpassageDetails($passageid)
    {
        $sql = "SELECT * FROM gk_ptpassages WHERE gk_ptpassages.id=$passageid";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return '';
        }
    }

    public function getTests()
    {

        $sql = "SELECT c.*,gk_ptests.*, a.category_name as category FROM gk_ptests INNER JOIN gk_pcategories as a ON a.id = gk_ptests.subcat_id INNER JOIN gk_ptopics as c ON c.topic_id = gk_ptests.topic_id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function get_student_test_result($uid)
    {

        $sql = "SELECT c.category_name, count(q_id) AS total_q ,
					count(if(attempet=0,1,NULL))AS notattempetd_q,
					count(if(attempet>0,1,NULL))AS attempet,
					count(if(correct=0,1,NULL))AS wrong,
					count(if(correct=1,1,NULL))AS correct_q,
					SUM(if(correct=1, 1, 0)) AS score
					  FROM `gk_braintest_answer` AS a join gk_braintestquestioncat AS c  on a.cat_id=c.id  WHERE student_id='$uid' group by cat_id order by cat_id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return '';

        }

    }

    public function userWalletCountByUser($id)
    {

        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id=" . $id;


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function getWalletByUser($id, $limit, $start)
    {

        $sql = "SELECT * FROM gk_user_wallet WHERE uw_user_id='$id'
           		  ORDER BY uw_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }

    }

    public function withdrawRequestByUser($id)
    {
        $sql = "SELECT * FROM gk_withdraw_requests where wr_user_id=" . $id;


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }
    public function getwithdrawrequestsByUser($id,$limit, $start)
    {

        $sql = "SELECT * FROM gk_withdraw_requests where  wr_user_id='$id'
			        ORDER BY wr_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }


}
/* End of file braintest_model.php *//* Location: ./application/models/braintest_model.php */
