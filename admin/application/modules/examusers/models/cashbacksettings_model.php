<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class cashbackSettings_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function isCashbackActive()
    {
        $sql = "SELECT status FROM gk_cashback_settings WHERE deleted='0'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->status === "1" ? true : false;
        } else {
            return false;
        }
    }
    public function getCurrentCashbackSettings()
    {
        $sql = "SELECT * FROM gk_cashback_settings WHERE deleted='0'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function getRemovedCashbackSettings()
    {
        $sql = "SELECT * FROM gk_cashback_settings WHERE deleted='1'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function updateSettings($data) 
    {
        $this->db->where('deleted', '0');
        $this->db->update('gk_cashback_settings', ['deleted' => '1', 'deleted_at' => date("Y-m-d H:i:s")]);

        $this->db->insert('gk_cashback_settings', ['start_date' => $data['start_date'], 'end_date' => $data['end_date']]);
        return $this->getCurrentCashbackSettings();
    }
    public function updateCashbackSettings($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('gk_cashback_settings', ['end_date' => $data['end_date']]);
        return $this->getCurrentCashbackSettings();
    }
    public function removeSettings($id) 
    {
        $this->db->where(['deleted' => '0', 'id' => $id]);
        $this->db->update('gk_cashback_settings', ['deleted' => '1', 'deleted_at' => date("Y-m-d H:i:s")]);
    }
    public function getExamuserByReferral($code)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE referral_code ='$code' and active='1' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getExamuserByReferralEvenInactive($code)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE referral_code ='$code' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function deactivateReferralCodes() {
        $this->db->where('active', '1');
        $this->db->update('gk_examuser_referrals', ['active' => '0', 'deletedAt' => date('Y-m-d H:i:s')]);
    }
    public function getExamuserReferralById($id)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE examuser_id ='$id' and active='1' limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function updateReferralCode($examuserId)
    {
        $this->deactivateReferralCodeById($examuserId);        
        $creatNew = true;
        $prev = $this->getExamuserLastReferralById($examuserId);
        if(is_object($prev)) {
            $creatNew = ((int)$prev->shareCount) > 0 ? false : true;
        }
        if($creatNew) {
            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            $res = "";
            for ($i = 0; $i < 6; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            return $this->db->insert('gk_examuser_referrals', ['referral_code' => $res, 'examuser_id' => $examuserId]);
        } else {
            return true;
        }
    }
    public function deactivateReferralCodeById($examuserId) {
        $this->db->where(['examuser_id' => $examuserId]);
        $this->db->update('gk_examuser_referrals', ['active' => '0', 'deletedAt' => date('Y-m-d H:i:s')]);
    }
    public function getReferralJoin($old, $new) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE new_examuser_id='$new'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function updateShareCountIfZero($examuserId) {
        $this->db->where(['examuser_id' => $examuserId, 'shareCount' => '0']);
        $this->db->update('gk_examuser_referrals', ['shareCount' => '1']);
    }
    
    public function getCurrentUserReferredData($id) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE new_examuser_id='$id'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $rData = $result->row();
            $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$rData->old_examuser_id' and cashback = '1'";
            $result = $this->db->query($sql);
            if ($result->num_rows() > 0) {
                $rData->total_cashback_referrals = $result->num_rows();
            } else {
                $rData->total_cashback_referrals = 0;
            }
            return $rData;
        } else {
            return false;
        }
    }
    public function getNumberOfReferralJoinsByoldId($old) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old'";
        $result = $this->db->query($sql);
        return $result->num_rows();
    }
    public function saveNewCashbackReferral($data) {
        if ($data != null) {
            $this->db->insert("gk_examuser_referral_joins", $data);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function getReferralJoinsByoldId($old)
    {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function getCashbackReferralJoinsByoldId($old)
    {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old' and cashback='1'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function isOldUserCashbackEligibleOnNewJoineeReferral($old, $referral_code) {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$old' and referral_code='$referral_code' and joined='1' and cashback='1' ORDER BY `joinedAt` ASC limit 5";
        $result = $this->db->query($sql);
        return $result->num_rows() < 5 ? true : false;
    }
    public function getFirstFiveReferrals($rid)
    {
        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE old_examuser_id='$rid' and joined='1' ORDER BY `joinedAt` ASC limit 5";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function getCashbackReferralJoinsByDate($start_date, $end_date, $search_by)
    {
        $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $start_date)));
        $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $end_date)));

        $sql = "SELECT * FROM gk_examuser_referral_joins WHERE $search_by  between '$start_date 00:00:00' and '$end_date 23:59:00' ORDER BY $search_by DESC";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function getExamuserLastReferralById($old)
    {
        $sql = "SELECT * FROM gk_examuser_referrals
                WHERE examuser_id ='$old' ORDER BY `createdAt` DESC limit 1";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->row();
        } else {
            return false;
        }
    }
    public function getUserDetailsByIds($ids=[])
    {
        $this->db->where_in('examuser_id',$ids);
        $res = $this->db->get('gk_examuserslist');
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return [];
        }
    }
    public function updateCashbackReferralJoin($where, $data = []) {
        $this->db->where($where);
        $this->db->update("gk_examuser_referral_joins", $data);
        return $this->db->affected_rows();
    }
}