<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class cashbackNotifications_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getAllCbNotifications() {
        $sql = "SELECT * FROM gk_cashback_notifications WHERE deleted='0'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }
    public function deleteNotificationById($id) {
        $this->db->where('id', "$id");
        $this->db->update('gk_cashback_notifications', ['deleted' => '1', 'deletedAt' => date('Y-m-d H:i:s')]);
    }
    public function addNotification($form) {
        return $this->db->insert('gk_cashback_notifications', $form);
    }
    public function getCbNotificationById($id) {
        $this->db->where('id', $id);
        return $this->db->get('gk_cashback_notifications')->row();
    }
    public function updateNotification($form, $id) {
        $this->db->where('id', $id);
        $this->db->update('gk_cashback_notifications', $form);
    }
    public function getStudentsBySendTo($v){
        $sql = "SELECT examuser_id,notification_token FROM gk_examuserslist WHERE notification_token !=''";
        if($v == "1") {
            $sql .= " and examuser_type = 'n'";
        } else if($v == "2") {
            $sql .= " and examuser_type = 'y'";
        }
        $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return [];
        }
    }
    public function updateNotificationSentDate($id) {
        $this->db->where('id', "$id");
        $this->db->update('gk_cashback_notifications', ['lastSendAt' => date('Y-m-d H:i:s')]);
    }
}