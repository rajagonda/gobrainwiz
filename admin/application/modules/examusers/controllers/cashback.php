<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class cashback extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->authentication->checklogin()) {
            redirect('login');
        }
        $this->load->library(array('template', 'form_validation'));
        $this->template->set_title('Cashback');
        $this->load->model('examusers_Model');
        $this->load->model('cashbackSettings_Model');
        $this->load->model('cashbackNotifications_Model');
        $this->load->language('examusers');
        $this->load->helper('text');
        $this->load->library('pagination');
        $this->sendToOptions = ["0" => "All Students", "1" => "Only Non Brainwiz Students", "2" => "Only Brainwiz Students"];
    }
    public function index()
    {
        $breadcrumbarray = [
            'label' => "Cashback Settings",
            'link' => base_url() . "examusers/cashback",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Cashback Settings");

        $cashbackSettings = $this->cashbackSettings_Model->getCurrentCashbackSettings();

        $prevCashbackSettings = $this->cashbackSettings_Model->getRemovedCashbackSettings();

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $form['start_date'] = $this->input->post('start_date');
            $form['end_date'] = $this->input->post('end_date');

            $var = $form['start_date'];
            $date = str_replace('/', '-', $var);
            $form['start_date'] = date('Y-m-d H:i:s', strtotime($date));

            $var = $form['end_date'];
            $date = str_replace('/', '-', $var);
            $form['end_date'] = date('Y-m-d H:i:s', strtotime($date));

            $res = $this->cashbackSettings_Model->updateSettings($form);
            redirect('examusers/cashback');
        }
        $this->template->load_view1('cashback_settings', ['action' => 'edit', 'prevCashbackSettings' => $prevCashbackSettings, 'cashbackSettings' => $cashbackSettings, 'page' => "Cashback Settings"]);
    }
    public function edit($id) {
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $form['start_date'] = $this->input->post('start_date');
            $form['end_date'] = $this->input->post('end_date');

            $var = $form['start_date'];
            $date = str_replace('/', '-', $var);
            $form['start_date'] = date('Y-m-d H:i:s', strtotime($date));

            $var = $form['end_date'];
            $date = str_replace('/', '-', $var);
            $form['end_date'] = date('Y-m-d H:i:s', strtotime($date));

            $this->cashbackSettings_Model->updateCashbackSettings($form, $id);
        }
        redirect('examusers/cashback');
    }
    public function notifications()
    {
        $breadcrumbarray = [
            'label' => "Cashback Notifications",
            'link' => base_url() . "examusers/cashback/notifications",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Cashback Notifications");
        $cbNotifications = $this->cashbackNotifications_Model->getAllCbNotifications();
        $this->template->load_view1('cashbackNotifications', ['cbNotifications' => $cbNotifications, 'sendToOptions' => $this->sendToOptions]);
    }
    public function DeleteNotification($id)
    {
        $this->cashbackNotifications_Model->deleteNotificationById($id);
        $this->session->set_userdata('Notification_deleted', "Successfully Deleted Notification!!");
        redirect('examusers/cashback/notifications');
    }
    public function AddNotification()
    {
        $breadcrumbarray = [
            'label' => "Cashback Add Notification",
            'link' => base_url() . "examusers/cashback/AddNotification",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Cashback Add Notification");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $form['title'] = $this->input->post('title');
            $form['message'] = $this->input->post('message');
            $form['sendTo'] = $this->input->post('sendTo');

            if ($_FILES['image']['name'] != '') {
                $filename = time() . "_" . $_FILES['image']['name'];
                $config['upload_path'] = '../upload/notification/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                } else {
                }
                $this->data = $this->upload->data();
                $update_image = $this->data['file_name'];
                $form['image'] = $update_image;
            }

            $this->cashbackNotifications_Model->addNotification($form);
            $this->session->set_userdata('Notification_deleted', "Successfully Added New Notification!!");
            redirect('examusers/cashback/notifications');
        }
        $this->template->load_view1('cashback_new_notification', ['sendToOptions' => $this->sendToOptions]);
    }
    public function EditNotification($id)
    {
        $breadcrumbarray = [
            'label' => "Cashback Add Notification",
            'link' => base_url() . "examusers/cashback/AddNotification",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Cashback Add Notification");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $form['title'] = $this->input->post('title');
            $form['message'] = $this->input->post('message');
            $form['sendTo'] = $this->input->post('sendTo');

            if ($_FILES['image']['name'] != '') {
                $filename = time() . "_" . $_FILES['image']['name'];
                $config['upload_path'] = '../upload/notification/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                } else {
                }
                $this->data = $this->upload->data();
                $update_image = $this->data['file_name'];
                $form['image'] = $update_image;
            }

            $this->cashbackNotifications_Model->updateNotification($form, $id);
            $this->session->set_userdata('Notification_deleted', "Successfully Updated Notification!!");
            redirect('examusers/cashback/notifications');
        }
        $notification = $this->cashbackNotifications_Model->getCbNotificationById($id);
        $this->template->load_view1('cashback_edit_notification', ['sendToOptions' => $this->sendToOptions, 'notification' => $notification]);
    }
    public function sendNotification($id)
    {
        $notification = $this->cashbackNotifications_Model->getCbNotificationById($id);
        if (is_object($notification)) {
            $cuur_time = date('Y-m-d H:i:s');
            $notdata['title'] = $notification->title;
            $notdata['message'] = $notification->message;
            $notdata['notify_type'] = 8;
            $notdata['time_update'] = $cuur_time;

            $nid = $this->common_model->insertSingle('gk_notifications', $notdata);

            $stokens = array();
            $studnetdata['students'] = $this->cashbackNotifications_Model->getStudentsBySendTo($notification->sendTo);
            foreach ($studnetdata['students'] as $value) {
                $notlog['student_id'] = $value->examuser_id;
                $notlog['notification_id'] = $nid;
                $notlog['checked'] = '0';
                $nlid = $this->common_model->insertSingle('gk_notifications_log', $notlog);
                array_push($stokens, $value->notification_token);
            }

            $url = 'https://fcm.googleapis.com/fcm/send';
            $field1 = new \stdClass;
            $field1->message = $notification->message;
            $field1->title = $notification->title;
            $field1->image = '';
            if (!empty($notification->image)) {
                $field1->image = base_url().'../upload/notification/' . $notification->image;
            }
            $field1->type = '8';
            $fields = array(
                'registration_ids' => $stokens,
                'data' => $field1,
            );
            
            $headers = array(
                'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',
                'Content-Type: application/json',
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === false) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);

            $this->session->set_userdata('Notification_deleted', "Successfully Sent Notification!!");
            $this->cashbackNotifications_Model->updateNotificationSentDate($id);
        }
        redirect('examusers/cashback/notifications');
    }
    public function removeCurrentCashbackSettings($id)
    {
        try {
            $this->cashbackSettings_Model->removeSettings($id);
            $this->cashbackSettings_Model->deactivateReferralCodes();
            redirect('examusers/cashback');
        } catch (\Exception $e) {
            redirect('examusers/cashback');
        }
    }
    public function CheckReferralCode()
    {
        $breadcrumbarray = [
            'label' => "Check Referral Code",
            'link' => base_url() . "examusers/cashback/CheckReferralCode",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Check Referral Code");

        $examuser = false;
        $data = [];
        $code = $this->input->get('value', "");
        $type = $this->input->get('type', "mobile");

        if (trim($code) != "" && !is_null($code)) {
            if ($type == "mobile") {
                $examuser = $this->examusers_Model->getSinglestudent($code);
            } else {
                $examuser = $this->cashbackSettings_Model->getExamuserByReferral(trim($code));
                if (!is_object($examuser)) {
                    $examuser = $this->cashbackSettings_Model->getExamuserByReferralEvenInactive(trim($code));
                }
            }

            if (is_object($examuser)) {
                $examuser = $this->cashbackSettings_Model->getUserDetailsByIds([$examuser->examuser_id])[0];
                $currentUserReferredData = $this->cashbackSettings_Model->getCurrentUserReferredData($examuser->examuser_id);
                $isActive = $this->cashbackSettings_Model->isCashbackActive();
                if ($examuser->examuser_type == "y") {
                    $currentUserReferral = $this->cashbackSettings_Model->getExamuserReferralById($examuser->examuser_id);
                    if (!is_object($currentUserReferral)) {
                        $currentUserReferralJoins = $this->cashbackSettings_Model->getNumberOfReferralJoinsByoldId($examuser->examuser_id);
                        $currentUserReferral = $this->cashbackSettings_Model->getExamuserLastReferralById($examuser->examuser_id);
                    }
                }
                $currentUserReferralJoinData = $this->cashbackSettings_Model->getReferralJoinsByoldId($examuser->examuser_id);
                $currentUsercashbackReferralJoinData = $this->cashbackSettings_Model->getCashbackReferralJoinsByoldId($examuser->examuser_id);
                $ids = [];
                foreach ($currentUserReferralJoinData as $row) {
                    $ids[] = $row->new_examuser_id;
                }

                if (is_object($currentUserReferredData)) {
                    $ids[] = $currentUserReferredData->old_examuser_id;
                }

                $cashbackByUserDetails = count($ids) > 0 ? $this->cashbackSettings_Model->getUserDetailsByIds($ids) : [];
                $cashbackByUserDetailsById = [];
                foreach ($cashbackByUserDetails as $row) {
                    $cashbackByUserDetailsById[$row->examuser_id] = $row;
                }
                $data = [
                    'isCashbackActive' => $isActive,
                    'referredData' => is_object($currentUserReferredData) ? $currentUserReferredData : false,
                    'referralData' => isset($currentUserReferral) ? $currentUserReferral : false,
                    'shareData' => $currentUserReferralJoinData,
                    'cashbackByUsersData' => $currentUsercashbackReferralJoinData,
                    'cashbackByUserDetailsById' => $cashbackByUserDetailsById,
                ];
            }
        }
        // echo "<pre>";
        // print_r([$data, $examuser]);
        // die();
        $this->template->load_view1('check_referral_code', ['action' => 'edit', 'data' => $data, 'type' => $type, 'referral_code' => $code, 'examuser' => $examuser, 'page' => "CheckReferralCode"]);
    }
    public function referralJoinees()
    {
        $download = $this->input->get('download', "false");
        if ($download != 'true') {
            $breadcrumbarray = [
                'label' => "Referral Joinees",
                'link' => base_url() . "examusers/cashback/referralJoinees",
            ];
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Referral Joinees");
        }

        $search_by = $this->input->get('search_by', "createdAt");
        $search_by = in_array($search_by, ['createdAt', 'joinedAt']) ? $search_by : "joinedAt";
        $show_cashback_eligible = $this->input->get('show_cashback_eligible', "off");
        $start_date = $this->input->get('start_date', "");
        if ($start_date == "") {
            $start_date = date("d/m/Y");
        }

        $end_date = $this->input->get('end_date', "");
        if ($end_date == "") {
            $end_date = date("d/m/Y");
        }

        $cashbackIsActive = $this->cashbackSettings_Model->isCashbackActive();
        $todayJoins = $this->cashbackSettings_Model->getCashbackReferralJoinsByDate($start_date, $end_date, $search_by);
        $filterTodayJoins = [];
        $ids = [];
        foreach ($todayJoins as $row) {
            if ($show_cashback_eligible == "on" && $row->cashback == "0") {
                continue;
            }
            if ($search_by == "createdAt" && $row->joined == "1") {
                continue;
            }
            $filterTodayJoins[] = $row;
            $ids[] = $row->old_examuser_id;
            $ids[] = $row->new_examuser_id;
        }

        $todayJoins = $filterTodayJoins;
        $userDetails = count($ids) > 0 ? $this->cashbackSettings_Model->getUserDetailsByIds($ids) : [];
        $userDetailsById = [];
        foreach ($userDetails as $row) {
            $userDetailsById[$row->examuser_id] = $row;
        }

        $dataV = [
            'userDetailsById' => $userDetailsById,
            'todayJoins' => $todayJoins,
            'search_by' => $search_by,
            'show_cashback_eligible' => $show_cashback_eligible,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'cashbackIsActive' => $cashbackIsActive,
        ];

        if ($download != 'true') {
            $this->template->load_view1('today_joins', $dataV);
        } else {
            $this->downloadList($dataV);
        }
    }
    public function downloadList($data)
    {
        $todayJoins = $data['todayJoins'];
        $userDetailsById = $data['userDetailsById'];
        $list = [];
        foreach ($todayJoins as $index => $joinee) {
            if (isset($userDetailsById[$joinee->new_examuser_id])) {
                $joineeData = $userDetailsById[$joinee->new_examuser_id];
            } else {
                $joineeData = (object) ['examuser_name' => 'Record Deleted', 'examuser_mobile' => ""];
            }
            if (isset($userDetailsById[$joinee->old_examuser_id])) {
                $referredData = $userDetailsById[$joinee->old_examuser_id];
            } else {
                $referredData = (object) ['examuser_name' => 'Record Deleted', 'examuser_mobile' => ""];
            }

            $v = [];
            $v['Sno'] = $index + 1;
            $v['Name'] = $joineeData->examuser_name;
            $v['Mobile Number'] = $joineeData->examuser_mobile;
            $v['Referred By'] = $referredData->examuser_name;
            $v['Referred Mobile Number'] = $referredData->examuser_mobile;
            $v['Registration Date'] = date("d-m-Y h:i:s a", strtotime($joinee->createdAt));
            $v['Joined Date'] = $joinee->joined == "1" ? date("d-m-Y h:i:s a", strtotime($joinee->joinedAt)) : "Not Joined Yet";
            $v['Referral Code'] = $joinee->referral_code;

            $list[] = $v;
        }
        $this->array_to_csv_download($list, "Report(" . $data['start_date'] . "-" . $data['end_date'] . ").csv");
    }
    public function downloadPaytmCashbackFile()
    {
        // $cashbackIsActive = $this->cashbackSettings_Model->isCashbackActive();
        // if(!$cashbackIsActive) return;

        $start_date = $this->input->get('start_date', "");
        if ($start_date == "") {
            $start_date = date("d/m/Y");
        }

        $end_date = $this->input->get('end_date', "");
        if ($end_date == "") {
            $end_date = date("d/m/Y");
        }

        $cashbackIsActive = $this->cashbackSettings_Model->isCashbackActive();
        $todayJoins = $this->cashbackSettings_Model->getCashbackReferralJoinsByDate($start_date, $end_date, "joinedAt");
        $filterTodayJoins = [];
        $ids = [];
        foreach ($todayJoins as $row) {
            if ($row->cashback == "0") {
                continue;
            }
            $filterTodayJoins[] = $row;
            $ids[] = $row->old_examuser_id;
            $ids[] = $row->new_examuser_id;
        }

        $todayJoins = $filterTodayJoins;
        $userDetails = count($ids) > 0 ? $this->cashbackSettings_Model->getUserDetailsByIds($ids) : [];
        $userDetailsById = [];
        foreach ($userDetails as $row) {
            $userDetailsById[$row->examuser_id] = $row;
        }

        $paytmData = [];

        foreach ($todayJoins as $joinee) {
            if (isset($userDetailsById[$joinee->new_examuser_id])) {
                $joineeData = $userDetailsById[$joinee->new_examuser_id];
            } else {
                $joineeData = (object) ['examuser_name' => 'Record Removed', 'examuser_mobile' => ""];
            }
            if (!isset($userDetailsById[$joinee->old_examuser_id])) {
                continue;
            }
            $referredData = $userDetailsById[$joinee->old_examuser_id];
            $temp = [
                'Order Id' => 'Referred To ' . $joineeData->examuser_name,
                "User's Mobile Number/Email" => $referredData->examuser_mobile,
                "Amount" => 200,
                "Comment" => 'Referred To ' . $joineeData->examuser_name . '(' . $joineeData->examuser_mobile . ')',
            ];
            $paytmData[] = $temp;
        }

        $this->array_to_csv_download($paytmData, "paytmCashbackEligibleList(" . $start_date . "-" . $end_date . ").csv");
    }
    public function array_to_csv_download($array, $filename = "export.csv", $delimiter = ",")
    {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');

        fputcsv($f, array_keys($array[0]), $delimiter);

        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }
}
