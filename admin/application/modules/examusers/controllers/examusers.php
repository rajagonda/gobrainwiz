<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class examusers extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        if (!$this->authentication->checklogin()) {
            redirect('login');
        }
        $this->load->library(array('template', 'form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('examusers_Model');
        $this->load->model('cashbackSettings_Model');
        $this->load->language('examusers');
        $this->load->helper('text');
        $this->load->helper('basic');
        $this->load->library('pagination');
    }

    public function index()
    {
        $breadcrumbarray = array('label' => "Exam Users List",
            'link' => base_url() . "examusers",
        );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Exam Users");

        $config['base_url'] = base_url() . '/examusers/';
        $config["total_rows"] = $this->examusers_Model->userscount('');
        $config["per_page"] = 100;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["examusers"] = $this->examusers_Model->get('', $config["per_page"], $page);
//var_dump($data["examusers"]);die();
        $data["links"] = $this->pagination->create_links();
        //$data['examusers'] = $this->examusers_Model->get();
        $this->template->load_view1('examusers', $data);

    }

    public function register()
    {
        $breadcrumbarray = array('label' => "Exam Users List",
            'link' => base_url() . "examusers",
        );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Exam Users");

        $config['base_url'] = base_url() . '/examusers/register/';
        $config["total_rows"] = $this->examusers_Model->userscount('reg');
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["examusers"] = $this->examusers_Model->get('reg', $config["per_page"], $page);
//var_dump($data["examusers"]);die();
        $data["links"] = $this->pagination->create_links();
        //$data['examusers'] = $this->examusers_Model->get();
        $this->template->load_view1('regexamusers', $data);
    }

    public function uploadusers()
    {

        $breadcrumbarray = array('label' => "Exam Users List",
            'link' => base_url() . "examusers/uploadusers",
        );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Exam Users Upload");
        $this->template->load_view1('uploadusers');
    }

    public function do_upload()
    {

        $config['upload_path'] = '../upload/uploads/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '*';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            $data['error'] = $this->upload->display_errors();
            $this->template->load_view1('uploadusers', $data);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $fileName = $data['upload_data']['file_name'];
            $this->load->library('excel');
            $inputFileType = 'Excel2007';
            $inputFileName = $data['upload_data']['full_path'];

            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            //$objReader = new PHPExcel_Reader_Excel2007();

            $objReader->setLoadAllSheets();

            $objPHPExcel = $objReader->load($inputFileName);
            // echo "hi";exit;
            /*            echo $objPHPExcel->getSheetCount(),' worksheet',(($objPHPExcel->getSheetCount() == 1) ? '' : 's'),' loaded<br /><br />';            */
            //$SetId=$this->braintest_Model->addquestionSet(array('file_name'=>$fileName,                                                                'test_id'=>$brainTestId                                                                ));
            $loadedSheetNames = $objPHPExcel->getSheetNames();

            foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                //$cat_id= $this->braintest_Model->getTestCategoryIdByName($loadedSheetName);
                $sheetData = $objPHPExcel->getSheet($sheetIndex)->toArray(null, true, true, true);
                // echo "<pre>"; print_r($sheetData);
                foreach ($sheetData as $key => $value) {
                    //echo $key;exit;
                    if ($key > 1) {

                        if ($value['B'] != "" && $value['C'] != "") {
                            $qdata['examuser_name'] = $value['B'];
                            $qdata['examuser_mobile'] = $value['C'];
                            $qdata['examuser_email'] = $value['E'];
                            $qdata['examuser_gender'] = $value['D'];

                            // echo "<pre>"; print_r($qdata);exit;

                            $sres = $this->examusers_Model->getSinglestudent($value['C']);
                            if (!$sres) {
                                $res = $this->examusers_Model->addstudent($qdata);
                                $this->cashbackSettings_Model->updateReferralCode($res);
                            } else {
                                if ($sres->examuser_type == 'n') {
                                    $updateValue = [];
                                    $updateValue['examuser_type'] = 'y';
                                    if ($this->examusers_Model->updateexamuser($updateValue, $sres->examuser_id)) {
                                        $this->cashbackSettings_Model->updateReferralCode($sres->examuser_id);
                                    }

                                    $hasReferralCode = $this->cashbackSettings_Model->getCurrentUserReferredData($sres->examuser_id);

                                    if (is_object($hasReferralCode)) {
                                        $this->cashbackSettings_Model->updateCashbackReferralJoin([
                                            'new_examuser_id' => $sres->examuser_id,
                                            'old_examuser_id' => $hasReferralCode->old_examuser_id,
                                            'referral_code' => $hasReferralCode->referral_code,
                                        ], [
                                            'joined' => '1',
                                            'joinedAt' => date('Y-m-d H:i:s'),
                                        ]);
                                        $isCashbackActive = $this->cashbackSettings_Model->isCashbackActive();
                                        if ($isCashbackActive) {
                                            $isReferralCodeActive = $this->cashbackSettings_Model->getExamuserReferralById($hasReferralCode->old_examuser_id);
                                            if (is_object($isReferralCodeActive)) {
                                                $valid = $this->cashbackSettings_Model->isOldUserCashbackEligibleOnNewJoineeReferral($hasReferralCode->old_examuser_id, $hasReferralCode->referral_code);
                                                if ($valid) {
                                                    $this->cashbackSettings_Model->updateCashbackReferralJoin([
                                                        'new_examuser_id' => $sres->examuser_id,
                                                        'old_examuser_id' => $hasReferralCode->old_examuser_id,
                                                        'referral_code' => $hasReferralCode->referral_code,
                                                    ], [
                                                        'questions' => '1',
                                                        'cashbackAt' => date('Y-m-d H:i:s'),
                                                    ]);
                                                    $this->sendReferralJoinedNotification($hasReferralCode->old_examuser_id, $sres->examuser_id);
                                                    $this->sendNewReferralJoinedNotification($hasReferralCode->old_examuser_id, $sres->examuser_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            redirect('examusers');
        }
    }

    public function save($id = null)
    {

        $breadcrumbarray = array('label' => "Braintest Test", 'link' => base_url() . "examusers");
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field) {
            $rules[] = array(
                'name' => $form_field['field'],
                'display' => $form_field['label'],
                'rules' => $form_field['rules'],
            );
        }

        $json_rules = json_encode($rules);
        $script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('examuser_from',{$json_rules});
</script>
JS;

        if ($id != '') {
            $data['action'] = "edit";
            $data['user'] = $this->examusers_Model->getsingleuser($id);
            $this->template->set_subpagetitle("Edit Brain Test");
        } else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Brain Test");
        }
        $this->form_validation->set_rules($validationRules);
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            if ($this->form_validation->run() == true) {
                $form_values['examuser_name'] = $this->input->post('examuser_name');
                $form_values['examuser_mobile'] = $this->input->post('examuser_mobile');
                $form_values['examuser_email'] = $this->input->post('examuser_email');
                $form_values['examuser_gender'] = $this->input->post('examuser_gender');
                $form_values['examuser_role'] = $this->input->post('examuser_role');
                $form_values['examuser_type'] = $this->input->post('examuser_type');
                if (count($this->input->post('payment_options')) > 0) {
                    $form_values['payment_options'] = implode(',', $this->input->post('payment_options'));
                } else {
                    $form_values['payment_options'] = null;
                }
                if ($id != '') {

                    if ($data['user']->examuser_mobile != $this->input->post('examuser_mobile')) {
                        $res = $this->examusers_Model->getSinglestudent($this->input->post('examuser_mobile'));
                        if ($res) {
                            redirect('examusers');
                        }
                    }
                }

                if ($id != '') {
                    $res = $this->examusers_Model->updateexamuser($form_values, $id);
                } else {

                    $sres = $this->examusers_Model->getSinglestudent($this->input->post('examuser_mobile'));
                    if (!$sres) {
                        $res = $this->examusers_Model->addstudent($form_values);
                        $this->cashbackSettings_Model->updateReferralCode($res);
                    } else {
                        if ($sres->examuser_type == 'n') {
                            $updateValue['examuser_type'] = 'y';
                            if ($this->examusers_Model->updateexamuser($updateValue, $sres->examuser_id)) {
                                $this->cashbackSettings_Model->updateReferralCode($sres->examuser_id);
                            }

                            $hasReferralCode = $this->cashbackSettings_Model->getCurrentUserReferredData($sres->examuser_id);

                            if (is_object($hasReferralCode)) {
                                $this->cashbackSettings_Model->updateCashbackReferralJoin([
                                    'new_examuser_id' => $sres->examuser_id,
                                    'old_examuser_id' => $hasReferralCode->old_examuser_id,
                                    'referral_code' => $hasReferralCode->referral_code,
                                ], [
                                    'joined' => '1',
                                    'joinedAt' => date('Y-m-d H:i:s'),
                                ]);

                                $this->cashbackSettings_Model->updateShareCountIfZero($hasReferralCode->old_examuser_id);

                                $isCashbackActive = $this->cashbackSettings_Model->isCashbackActive();
                                if ($isCashbackActive) {
                                    $isReferralCodeActive = $this->cashbackSettings_Model->getExamuserReferralById($hasReferralCode->old_examuser_id);
                                    if (is_object($isReferralCodeActive)) {
                                        $valid = $this->cashbackSettings_Model->isOldUserCashbackEligibleOnNewJoineeReferral($hasReferralCode->old_examuser_id, $hasReferralCode->referral_code);
                                        if ($valid) {
                                            $this->cashbackSettings_Model->updateCashbackReferralJoin([
                                                'new_examuser_id' => $sres->examuser_id,
                                                'old_examuser_id' => $hasReferralCode->old_examuser_id,
                                                'referral_code' => $hasReferralCode->referral_code,
                                                'joined' => '1'
                                            ], [
                                                'questions' => '1',
                                                'cashbackAt' => date('Y-m-d H:i:s'),
                                            ]);
                                            $this->sendReferralJoinedNotification($hasReferralCode->old_examuser_id, $sres->examuser_id);
                                            $this->sendNewReferralJoinedNotification($hasReferralCode->old_examuser_id, $sres->examuser_id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                redirect('examusers');
            }
        }
        $data['script'] = $script;
        $this->template->load_view1('examuser_setup', $data);
    }

    public function sendNewReferralJoinedNotification($old, $new)
    {
        $oldObject = $this->examusers_Model->getsingleuser($old);
        $newObject = $this->examusers_Model->getsingleuser($new);

        if (!is_object($oldObject))
            return;
        if (!is_object($newObject))
            return;

        if (is_object($newObject) && empty(trim($newObject->notification_token)))
            return;

        $notdata = [];
        $notdata['title'] = "Congratulations";
        $notdata['message'] = "Congratulations !!! You have received Rs.100 discount, Happy Learning !!!";
        $notdata['notify_type'] = 7;
        $notdata['time_update'] = date('Y-m-d H:i:s');

        $nid = $this->common_model->insertSingle('gk_notifications', $notdata);

        $notlog = [];
        $notlog['student_id'] = $newObject->examuser_id;
        $notlog['notification_id'] = $nid;
        $notlog['checked'] = '0';

        $nlid = $this->common_model->insertSingle('gk_notifications_log', $notlog);

        $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
        $field1->message = $notdata['message'];
        $field1->title = $notdata['title'];
        $field1->image = '';
        $field1->type = '7';
        $field1->category = '';
        $field1->url = '';
        $fields = array(
            'registration_ids' => [$newObject->notification_token],
            'data' => $field1,
        );
        $headers = array(
            // 'Authorization:key = AIzaSyAFJlc5XeyL6vYCsXzTKP72DudKlKm64FU',
            'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',
            'Content-Type: application/json',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function sendReferralJoinedNotification($old, $new)
    {
        $oldObject = $this->examusers_Model->getsingleuser($old);
        $newObject = $this->examusers_Model->getsingleuser($new);

        if (!is_object($oldObject))
            return;
        if (!is_object($newObject))
            return;

        if (is_object($oldObject) && empty(trim($oldObject->notification_token)))
            return;

        $notdata = [];
        $notdata['title'] = "Congratulations";
        $notdata['message'] = "Congratulations !!! Your friend " . $newObject->examuser_name . " has joined BRAINWIZ and now you will receive Rs.200 cashback within 24 hours.";
        $notdata['notify_type'] = 6;
        $notdata['time_update'] = date('Y-m-d H:i:s');

        $nid = $this->common_model->insertSingle('gk_notifications', $notdata);

        $notlog = [];
        $notlog['student_id'] = $oldObject->examuser_id;
        $notlog['notification_id'] = $nid;
        $notlog['checked'] = '0';

        $nlid = $this->common_model->insertSingle('gk_notifications_log', $notlog);

        $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
        $field1->message = $notdata['message'];
        $field1->title = $notdata['title'];
        $field1->image = '';
        $field1->type = '6';
        $field1->category = '';
        $field1->url = '';
        $fields = array(
            'registration_ids' => [$oldObject->notification_token],
            'data' => $field1,
        );
        $headers = array(
            // 'Authorization:key = AIzaSyAFJlc5XeyL6vYCsXzTKP72DudKlKm64FU',
            'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',
            'Content-Type: application/json',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function _rules()
    {
        $rules = array(
            array('field' => 'examuser_name', 'label' => 'Name', 'rules' => 'trim|required|xss_clean|max_length[250]'),
            array('field' => 'examuser_mobile', 'label' => 'Mobile', 'rules' => 'trim|required|xss_clean|numeric'),
            array('field' => 'examuser_email', 'label' => 'Email', 'rules' => 'trim|required|xss_clean'),
            array('field' => 'examuser_role', 'label' => 'Role', 'rules' => 'trim|required|xss_clean'),

        );
        return $rules;
    }

    public function _rules2()
    {
        $rules = array(
            array('field' => 'name', 'label' => lang('student_name'), 'rules' => 'trim|xss_clean|max_length[250]'), array('field' => 'email', 'label' => lang('student_email'), 'rules' => 'trim|xss_clean|valid_email'),
            array('field' => 'phone', 'label' => lang('student_phone'), 'rules' => 'trim|xss_clean|min_length[10]|integer'),
        );
        return $rules;
    }

    public function changeTestStatus()
    {
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if ($staus == 'y') {
            $formvalues['test_status'] = 'n';
            echo "0";
        } else {
            $formvalues['test_status'] = 'y';
            echo "1";
        }
        $this->braintest_Model->changeTestStatus($formvalues, $id);
    }

    public function changeStudentStatus()
    {
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if ($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->braintest_Model->changeStudentStatus($formvalues, $id);
    }

    public function changequestionsetStatus()
    {
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if ($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->braintest_Model->changequestionsetStatus($formvalues, $id);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->examusers_Model->deleteuser($id);

    }

    public function deleteall()
    {
        $idsdata = $this->input->post('updatedata');

        $ids = explode(',', $idsdata);
        // echo "<pre>"; print_r($ids)    ;exit;

        for ($i = 0; $i < count($ids); $i++) {
            $del_id = $ids[$i];
            $this->examusers_Model->deleteuser($del_id);
        }
    }

    public function searchusers()
    {
        $item = $_POST['item'];
        if ($item != '') {
            $data["examusers"] = $this->examusers_Model->searchUsers($item);
            //echo $this->db->last_query();
            //echo "<pre>"; print_r($data);exit;

            $this->load->view('ajax_users', $data);
        }
    }

    public function searchusers1()
    {
        $item = $_POST['item'];
        $type = $_POST['type'];
        if ($item != '') {
            $data["examusers"] = $this->examusers_Model->searchUsers1($item, $type);

//            echo $this->db->last_query();
//            echo "<pre>"; print_r($data);exit;

            $this->load->view('ajax_users', $data);
        }
    }

    public function deletestudent()
    {
        $id = $this->input->post('id');
        $this->braintest_Model->deleteStudent($id);
    }

    public function questionsetdelete()
    {
        $id = $this->input->post('id');
        $this->braintest_Model->deleteQuestionSet($id);
    }

    public function exportstudents($id = null)
    {
        $braintest = $this->braintest_Model->getTestDetails($id);
        $title = "Manage " . $braintest->test_name . " Test students";
        $students = $this->braintest_Model->getstudents($id);
        $heading = array('No', 'student Id', 'Password');
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle($braintest->test_name);
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        $totn = count($students);
        $maxrow = $totn + 1;
        $row = 2;
        $no = 1;
        foreach ($students as $student) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $no);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $student->studentId);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $student->password);
            $row++;
            $no++;
        }

        $objPHPExcel->getActiveSheet()->freezePane('A2');
        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
        $objPHPExcel->getActiveSheet()->getStyle('A1:C' . $maxrow)->applyFromArray($styleArray);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="BWTest-' . $braintest->test_name . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        redirect('braintest/students/' . $id);
    }

    public function viewresult($test_id, $studentId = null)
    {

        $breadcrumbarray = array('label' => "Braintest Test students", 'link' => base_url() . "braintest/students/" . $test_id);
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);

        $data['tresult'] = $this->braintest_Model->get_student_test_result($studentId, $test_id);
        $data['testdetails'] = $this->braintest_Model->getTestDetails($test_id);

        $data['student'] = $this->braintest_Model->getstudent($studentId);

        $this->template->load_view1('testresult', $data);
    }

    public function studentresult($id = null)
    {
        $braintest = $this->braintest_Model->getTestDetails($id);
        $title = "Manage " . $braintest->test_name . " Test students";
        $students = $this->braintest_Model->getstudents($id, 3);

        if (count($students) > 0 && isset($students[0]->studentId)) {
            $allCategory = $this->braintest_Model->get_student_test_result($students[0]->studentId);

            $heading = array('No', 'User ID', 'Name', 'Phone', 'Email');
            $totalMark = 0;
            foreach ($allCategory as $cat) {
                $heading[] = $cat->category_name . ' (' . $cat->total_q . ')';
                $totalMark += $cat->total_q;
            }
            $heading[] = 'Total(' . $totalMark . ')';
            $heading[] = 'Percentage(%)';

            $this->load->library('excel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setTitle($braintest->test_name);
            $rowNumberH = 1;
            $colH = 'A';
            foreach ($heading as $h) {
                $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
                $colH++;
            }

            $totn = count($students);
            $maxrow = $totn + 1;
            $row = 2;
            $no = 1;

            //print_r($students);

            $x = '';

            foreach ($students as $student) {
                $result = $this->braintest_Model->get_student_test_result($student->studentId);

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $no);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $student->studentId);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $student->name);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $student->phone);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $student->email);

                $x = 'E';
                $per = "";
                $total = 0;
                foreach ($result as $keys => $testresult) {
                    $per += ($testresult->correct_q / $testresult->total_q) * 100;
                    $total += $testresult->correct_q;

                    $x++;

                    $objPHPExcel->getActiveSheet()->setCellValue($x . $row, $testresult->correct_q);

                }
                $per = round($per / count($result), 2);

                $x++;
                $objPHPExcel->getActiveSheet()->setCellValue($x . $row, $total);

                $x++;
                $objPHPExcel->getActiveSheet()->setCellValue($x . $row, $per);

                $row++;
                $no++;
            }

            $objPHPExcel->getActiveSheet()->freezePane('A2');
            $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
            $objPHPExcel->getActiveSheet()->getStyle("A1:$x" . $maxrow)->applyFromArray($styleArray);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BWTest-' . $braintest->test_name . '-' . $braintest->test_date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            redirect('braintest/students/' . $id);
        } else {
            redirect('braintest/students/' . $id);
        }
    }

    public function wallet($id)
    {
        $this->load->helper('basic');
        $breadcrumbarray = [
            'label' => "User Wallet",
            'link' => base_url() . "examusers/wallet/" . $id,
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("User Wallet");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/examusers/wallet/' . $id;
        $config["total_rows"] = $this->examusers_Model->userWalletCountByUser($id);
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if ($config["total_rows"] > 0) {
            $data["wallet"] = $this->examusers_Model->getWalletByUser($id, $config["per_page"], $page);
        } else {
            $data["wallet"] = array();
        }
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('wallet', $data);
    }

    public function withdrawrequestsByUser($id)
    {
        if (!$this->session->userdata('user_login') == '2') {
            redirect(base_url() . "onlinetest/login");
        }
        $breadcrumbarray = [
            'label' => "Coupons",
            'link' => base_url() . "examusers/withdrawrequestsByUser/" . $id,
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Withdraw Requests List");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/examusers/withdrawrequestsByUser/' . $id;
        $config["total_rows"] = $this->examusers_Model->withdrawRequestByUser($id);
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["records"] = $this->examusers_Model->getwithdrawrequestsByUser($id, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $this->template->load_view1('withdraw_request_list', $data);
    }
}
