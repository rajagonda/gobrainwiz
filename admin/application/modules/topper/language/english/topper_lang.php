<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Toppers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Toppers
|--------------------------------------------------------------------------
*/


$lang['manage_toppers'] = 'Manage Toppers';
$lang['topper_add'] = 'Add Topper';
$lang['topper_edit'] = "Edit Subject";
$lang['student_name'] = "Student  Name";
$lang['voice_description'] = "Description";
$lang['voice_image'] = "Image";
$lang['company'] = "Company  Name";

/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
