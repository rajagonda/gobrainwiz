<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('topper_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topper_from" id="topper_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('student_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $topper->student_name; ?>" placeholder="<?php echo lang('student_name'); ?>" name="student_name" id="student_name" class="form-control">
                                        </div>

										<div class="form-group">
                                            <label for="exampleInputEmail12">College</label>
                                            <input type="text" value="<?php if($action=='edit') echo $topper->college; ?>" placeholder="college" name="college"  class="form-control">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail12">Native Place</label>
                                            <input type="text" value="<?php if($action=='edit') echo $topper->student_native_place; ?>" placeholder="student native place" name="student_native_place"  class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('voice_description'); ?></label>
                                            <textarea placeholder="Enter ..." rows="3" name="voice_description" id="voice_description" class="form-control"><?php if($action=='edit') echo $topper->voice_description; ?></textarea>
                                        </div>
										<div class="form-group">
                                            <label>Video URL</label>
                                            <input type="text" value="<?php if($action=='edit') echo $topper->student_video_url; ?>" placeholder="student video URL" name="student_video_url"  class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('company'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $topper->company; ?>" placeholder="<?php echo lang('company'); ?>" id="company" name="company" class="form-control">
                                        </div>
                                         <div class="form-group">
                                              <?php if($action=='edit'){ ?>
                                            <?php if($topper->voice_image !=''){   ?>
                                    <img src="http://brainwizz.in/upload/topper/<?php echo $topper->voice_image;?>" height="90" width="75" class="pull-right img-rounded" alt=""/>
                                    <?php }else { ?>
                                    <img src="http://brainwizz.in/upload/topper/images.jpg" class="pull-right img-rounded" alt=""/>
                                     <?php  } ?>
                                            <?php } ?>
                                            <label for="exampleInputFile"><?php echo lang('voice_image'); ?></label>
                                            <input type="file" name="voice_image" id="voice_image">
                                           
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
