<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Language : aptitude
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Aptitude
|--------------------------------------------------------------------------
*/


$lang['manage_categories'] = 'Manage Categories';
$lang['category_add'] = 'Add Category';
$lang['category_edit'] = "Edit Company";
$lang['com_name'] = "Category Name";
$lang['c_id'] = "Parent";




$lang['manage_subcat'] = 'Manage Subcategories';
$lang['subcat_add'] = 'Add Sub Category';
$lang['subcat_edit'] = "Edit ub Category";
$lang['sub_cat'] = "SubCategory";
$lang['c_id'] = "Company Name ";
$lang['add'] = "Add";
$lang['view'] = 'view';
$lang['add_question'] = 'Add Question';
$lang['view_question'] = 'View Questions';


$lang['manage_topics'] = 'Manage Topics';
$lang['topic_add'] = 'Add Topic';
$lang['topic_edit'] = "Edit Topic";
$lang['topic_name'] = "Topic Title";


$lang['manage_questions'] = 'Manage Questions';
$lang['question_add'] = 'Add Question';
$lang['question_edit'] = "Edit Question";
$lang['question_details'] = "Question Details";
$lang['option1'] = "option1";
$lang['question_name'] = "Question";




/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
