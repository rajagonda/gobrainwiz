<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : aptitude
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class aptitude_Model extends CI_Model {

	public $variable;

	public function __construct() {
	    parent::__construct();		
	}
	public function getAllcompanaies() {
            $this->db->where('c_id','0');
            $result = $this->db->get('gk_aptitude');
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }
       }
       public function addaptitudedetails($data) {
            $this->db->insert('gk_aptitude', $data); 
            return $this->db->insert_id();
       }
       public function getcompanydetails($id) {
            $this->db->where('id', $id);
            $query = $this->db->get('gk_aptitude');
            if ($query->num_rows() > 0) {      
            return $query->row();
            }
            else {
            return '';
            } 
       }
       public function updatecompanydetails($id, $values) {
            $this->db->where('id', $id);
            $this->db->update('gk_aptitude', $values);
            return $this->db->affected_rows();
       }      
       public function deleteaptitude($id) {
            $this->db->where('id', $id);
            $this->db->delete('gk_aptitude');
       }
       public function changeaptitudeStatus($values, $id) {
            $this->db->where('id', $id);
            $this->db->update('gk_aptitude', $values);
            return $this->db->affected_rows();
       }
       public function getAllcsubcategories() {
            $sql = "SELECT  cat.com_name , subcat.id as SubID ,subcat.status, subcat.com_name as SubCategory
                  FROM gk_aptitude as cat inner join gk_aptitude as subcat
                  on cat.id = subcat.c_id where cat.c_id=0";              
            $query = $this->db->query($sql);
            if($query->num_rows()>0) {
            return $query->result();
            } else {
            return '';
            }
       }
       public function getsubcategories($cid) {
            $this->db->where('c_id', $cid);
            $query = $this->db->get('gk_aptitude');
            if ($query->num_rows() > 0) {      
            return $query->result();
            }
            else {
            return '';
            } 
       }
       public function getallsubcattopics($sid) {
            $this->db->where('subcat_id', $sid);
            $query = $this->db->get('gk_atopics');
            if ($query->num_rows() > 0) {      
            return $query->result();
            }
            else {
            return '';
            } 
     }    
     public function addquestiondetails($data) {
            $this->db->insert('gk_aptitudeqas', $data); 
            return $this->db->insert_id();
    }
    public function updatequestionchoices($data, $qid) {
            $this->db->where('id', $qid);
            $this->db->update('gk_aptitudeqas', $data);
            return $this->db->affected_rows();
    }
    public function getsubcatquerion($id){
            $this->db->where('topic_id', $id);
            $query = $this->db->get('gk_aptitudeqas');
            if ($query->num_rows() > 0) {      
            return $query->result();
            }
            else {
            return '';
            } 
    }
    public function getsquestiondetails($id) {
            $sql = "SELECT  * FROM gk_aptitudeqas where id ='$id'";
            $query = $this->db->query($sql);
            if($query->num_rows()>0) {
            return $query->row();
            } else {
            return '';
            }
    }
    public function updatequestiondetails($id, $data) {
            $this->db->where('id', $id);
            $this->db->update('gk_aptitudeqas', $data);
            return $this->db->affected_rows();
    }
    public function getalltopics() {
            $sql = "SELECT gk_atopics.*, b.com_name as category, a.com_name as subcategory FROM gk_atopics
                   INNER JOIN gk_aptitude as b ON b.id = gk_atopics.cat_id
                   INNER JOIN gk_aptitude as a ON a.id = gk_atopics.subcat_id";
            $query = $this->db->query($sql);
            if($query->num_rows()>0) {
                return $query->result();
            } else {
                return '';
            }
    }
    public function addtopicdetails($data) {
            $this->db->insert('gk_atopics', $data); 
            return $this->db->insert_id();
    }
    public function gettopicdetails($id) {
            $this->db->where('topic_id', $id);
            $result = $this->db->get('gk_atopics');
            if ($result->num_rows() > 0) {
                return $result->row();
            } else {
                return '';
            }
    }
    public function updatetopicdetails($values,$id) {
            $this->db->where('topic_id', $id);
            $this->db->update('gk_atopics', $values);
            return $this->db->affected_rows();
    }
    /**
    * 
    * @param integer $id
    */
    public function deletetopic($id) {
            $this->db->where('topic_id', $id);
            $this->db->delete('gk_atopics');
    }
    public function changetopicStatus($values, $id) {
            $this->db->where('topic_id', $id);
            $this->db->update('gk_atopics', $values);
            return $this->db->affected_rows();
    }}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */