<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Company
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class questions_model extends CI_Model {

	public $variable;

	public function __construct() {
	    parent::__construct();
		
	}
	public function getquetions($sid){
            
            $sql = "SELECT gk_companyqas.*,cat.com_name as catname,subcat.com_name as subcatname FROM gk_companyqas
                    INNER JOIN gk_companies as cat ON cat.id = gk_companyqas.cat_id
                    INNER JOIN gk_companies as subcat ON subcat.id = gk_companyqas.sub_id
                     WHERE gk_companyqas.sub_id='$sid'";
             
            
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
        }
        
        
        public function getquestiondetails($qid){
            
            $sql = "SELECT gk_companyqas.*,cat.com_name as catname,subcat.com_name as subcatname FROM gk_companyqas
                    INNER JOIN gk_companies as cat ON cat.id = gk_companyqas.cat_id
                    INNER JOIN gk_companies as subcat ON subcat.id = gk_companyqas.sub_id
                     WHERE gk_companyqas.id='$qid'";
             
            
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
        }
        
         public function getsubcategories($cid) {
         $this->db->where('c_id', $cid);
         $query = $this->db->get(DB_PREFIX.'companies');
	  if ($query->num_rows() > 0) {      
	    return $query->result();
         }
         else {
            return '';
	 } 
    }
	public function save($data){
		$this->db->insert(DB_PREFIX.'companyqas',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas', $data);
	    return $this->db->affected_rows();
	}
	public function getcomapnyDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'companypdf');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changequestionStatus($data, $id){
	    $this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas', $data);
            return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'companyqas');
	}
        
        public function getPapers(){
             $sql = "SELECT  cat.com_name , subcat.id as SubID ,subcat.status, subcat.com_name as SubCategory
              FROM gk_companies as cat inner join gk_companies as subcat
              on cat.id = subcat.c_id where cat.c_id =0";  
             //left outer join
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
        }


}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */