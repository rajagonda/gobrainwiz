<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="#" role="form" name="question_from" id="question_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                   <div class="box-body">
                                  
                                    <div class="form-group">
                                    <label for="exampleInputPassword1">Category</label>
                                    <select name="c_id"  id="c_id" class="form-control">
                                    <option value="">Select Company</option>
                                    <?php
                                    foreach ($catgeories as $value)
                                    {?>                          
                                    <option value="<?php echo $value->id;?>" <?php if($value->id == $topicdetails->cat_id) echo "selected";  ?>><?php echo $value->com_name;?></option>;
                                    <?php  
                                    }
                                    ?>
                                    </select>       
                                    </div>
                                    
                                    <div class="form-group">
                                    <label for="exampleInputPassword1">Subcategory</label>
                                    <select name="sub_id" id="sub_id" class="form-control" >
                                    <option value="">Select Sub category</option><?php
                                    foreach ($subcategories as $value)
                                    {?>                          
                                    <option value="<?php echo $value->id;?>" <?php if($value->id == $topicdetails->subcat_id) echo "selected";  ?>><?php echo $value->com_name;?></option>;
                                    <?php  
                                    }
                                    ?>
                                    </select>     
                                    </div>
                                         <div class="form-group">
                                    <label for="exampleInputPassword1">Topic</label>
                                    <select name="topic_id" id="topic_id" class="form-control" >
                                    <option value="">Select Topic</option><?php
                                    foreach ($topics as $value)
                                    {?>                          
                                    <option value="<?php echo $value->topic_id;?>" <?php if($value->topic_id == $topicdetails->topic_id) echo "selected";  ?>><?php echo $value->topic_name;?></option>;
                                    <?php  
                                    }
                                    ?>
                                    </select>     
                                    </div>
                                        <div class="form-group">
                                            <label>Question</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_name" id="question_name" class="form-control"><?php if($action == 'edit') echo $details->question_name; ?></textarea>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 1</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option1" id="option1" class="form-control"><?php if($action == 'edit') echo $details->option1; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 2</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option2" id="option2" class="form-control"><?php if($action == 'edit') echo $details->option2; ?></textarea>
                                        </div>
                                       <div class="form-group">
                                            <label>Choice 3</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option3" id="option3" class="form-control"><?php if($action == 'edit') echo $details->option3; ?></textarea>
                                        </div>
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                               
                                    <div class="box-body">
                                        
                                        
                                        <div class="form-group">
                                            <label>Choice 4</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option4" id="option4" class="form-control"><?php if($action == 'edit') echo $details->option4; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 5</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option5" id="option5" class="form-control"><?php if($action == 'edit') echo $details->option5; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Explanation</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_explanation" id="question_explanation" class="form-control"><?php if($action == 'edit') echo $details->question_explanation; ?></textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Answer</label>
                         <select name="question_answer" id="question_answer"  class="form-control">
                            <option value="">Select Answer</option>
                            <option value="1">Choice 1</option>
                            <option value="2">Choice 2</option>
                            <option value="3">Choice 3</option>
                            <option value="4">Choice 4</option>
                            <option value="5">Choice 5</option>
                         </select>           
                                
                        <div class="clear"></div>
                    </div> 
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
<?php

echo $script;
?>
 <script type="text/javascript">
            $(function() {


                 CKEDITOR.replace( 'question_name', {
                extraPlugins: 'mathjax'
                });

                  CKEDITOR.replace( 'option1', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option2', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option3', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option4', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option5', {
                extraPlugins: 'mathjax'
                });
                  CKEDITOR.replace( 'question_explanation', {
                extraPlugins: 'mathjax'
                });
                  
               
                


               
               
            });
        </script>
        
 <script type="text/javascript"> 
 $(document).ready(function () {
 $("#c_id").change(function (){
     var selectVal = $('#c_id :selected').val();
     $.ajax({
       url:'<?php echo base_url(); ?>placement/questions/getsubcategories',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
       var htmlString ='';
        htmlString+="<option value=''>Select Subcategory</option>"
       $.each(data,function(i){
             htmlString+="<option value="+data[i]['id']+">"+data[i]['com_name']+"</option>"
           });
       $("#sub_id").html(htmlString);
       }
});

 });
 
 
  
        
       
 
    });
 </script>