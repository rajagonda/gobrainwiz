<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : aptitude
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class aptitude extends MY_Controller {
     
    public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('aptitude_Model');
            $this->load->language('aptitude');
     }
     public function index()	{
            $breadcrumbarray = array('label'=> "Categories",
                            'link' => base_url()."aptitude"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Categories");
            $data['categories'] = $this->aptitude_Model->getAllcompanaies();
            $this->template->load_view1('categories', $data);            
     }
     public function save($id=null){        
            $breadcrumbarray = array('label'=> "Categories",
                           'link' => base_url()."aptitude"
                           );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $validationRules = $this->_rules();
            foreach ($validationRules as $form_field)   {
            $rules[] = array(
            'name' => $form_field['field'],
            'display' => $form_field['label'],
            'rules' => $form_field['rules'],
            );
            }
            
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('catgeory_from',{$json_rules});
</script>
JS;
           if($id !=''){
           $data['action'] = "edit";
           $data['category'] = $this->aptitude_Model->getcompanydetails($id);
           $this->template->set_subpagetitle("Edit Company");
           }else {
           $data['action'] = "add";
           $this->template->set_subpagetitle("Add Category");
           }
           $this->form_validation->set_rules($validationRules);
           if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
           if ($this->form_validation->run() == true)  {
           $form_values['com_name'] = $this->input->post('com_name');
           if($id !=''){
           $res = $this->aptitude_Model->updatecompanydetails($id,$form_values);
           }else {
           $form_values['c_id'] = '0';
           $res = $this->aptitude_Model->addaptitudedetails($form_values);
           }
           redirect('aptitude');
           }         
           }
           $data['script'] = $script;
           $this->template->load_view1('category_setup',$data);
     }
     public function _rules() {
           $rules = array(
                array('field' => 'com_name','label' => lang('com_name'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
           return $rules;
     }
     public function changecategoryStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->aptitude_Model->updatecompanydetails($id, $formvalues);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->aptitude_Model->deleteaptitude($id);
    }    
    public function subcategories(){
        $breadcrumbarray = array('label'=> "Subcategories",
                            'link' => base_url()."aptitude/subcategories"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Sub Categories");
        $data['subcategories'] = $this->aptitude_Model->getAllcsubcategories();
        //  echo "<pre>"; print_r($data);exit;
        $this->template->load_view1('subcategories', $data);
    }
    public function subcategoriesssave($id=null){
        $breadcrumbarray = array('label'=> "Subcategories",
                           'link' => base_url()."aptitude/subcategories"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $data['categories'] = $this->aptitude_Model->getAllcompanaies();
        $validationRules = $this->_rules1();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('subcat_from',{$json_rules});
</script>
JS;

         if($id !=''){
         $data['action'] = "edit";
         $data['subcatgeory'] = $this->aptitude_Model->getcompanydetails($id);
         $this->template->set_subpagetitle("Edit Subcategory");
         }else {
         $data['action'] = "add";
         $this->template->set_subpagetitle("Add Subcategory");
         }
         $this->form_validation->set_rules($validationRules);
         if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
         if ($this->form_validation->run() == true)  {
         
         $form_values['com_name'] = $this->input->post('com_name');
         $form_values['c_id'] =  $this->input->post('c_id');
         
         if($id !=''){
         $res = $this->aptitude_Model->updatecompanydetails($id,$form_values);
         }else {
         $res = $this->aptitude_Model->addaptitudedetails($form_values);
         }
         redirect('aptitude/subcategories');
         }  
         }
         $data['script'] = $script;
         $this->template->load_view1('subcategories_setup',$data);
    }
    public function _rules1() {
         $rules = array(
                array('field' => 'com_name','label' => lang('com_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'c_id','label' => lang('c_id'),'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }
     public function getalltopics() {
         $breadcrumbarray = array('label'=> "Topics",
                            'link' => base_url()."aptitude/getalltopics"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Topics");
        $data['topicslist'] = $this->aptitude_Model->getalltopics();          
        $this->template->load_view1('topics', $data);        
     }
     public function topicsssave($id=null){
        $breadcrumbarray = array('label'=> "Subcategories",
                           'link' => base_url()."aptitude/subcategories"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $data['categories'] = $this->aptitude_Model->getAllcompanaies();
        $validationRules = $this->_rules2();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('topic_from',{$json_rules});
</script>
JS;

         if($id !=''){
         $data['action'] = "edit";
         $data['subcatgeory'] = $this->aptitude_Model->getcompanydetails($id);
         $this->template->set_subpagetitle("Edit Topic");
         }else {
         $data['action'] = "add";
         $this->template->set_subpagetitle("Add Topic");
         }
         $this->form_validation->set_rules($validationRules);
         if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
         if ($this->form_validation->run() == true)  {
         
         $formvalues['cat_id'] = $this->input->post('c_id');
         $formvalues['subcat_id'] = $this->input->post('subcat_id');
         $formvalues['topic_name'] = $this->input->post('topic_name');
         
         if($id !=''){
         $res = $this->aptitude_Model->updatetopicdetails($formvalues, $id);
         }else {
         $res = $this->aptitude_Model->addtopicdetails($formvalues);
         }
         redirect('aptitude/getalltopics');
         }  
         }
         $data['script'] = $script;
         $this->template->load_view1('topic_setup',$data);
    }
    public function _rules2() {
         $rules = array(
                array('field' => 'subcat_id','label' => lang('sub_cat'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'c_id','label' => lang('c_id'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'topic_name','label' => lang('topic_name'),'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }
    public function getsubcategory(){
         $catid = $_POST['selected'];
         $subcategories= $this->aptitude_Model->getsubcategories($catid);
         header('Content-type: application/json');
         die( json_encode( $subcategories ) );  
    }

}
