<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : questions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class questions extends MY_Controller {

     public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model(array('aptitude_Model','aptitude_Model'));
            $this->load->language('aptitude');
     }
     public function index($sid=null)	{
           
     }
     public function viewQuestions($sid=null)	{
            $breadcrumbarray = array('label'=> "Question View",
                            'link' => base_url()."aptitude/questions/viewQuestions/".$sid
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Questions");
            $data['questions'] = $this->aptitude_Model->getsubcatquerion($sid);
            $data['sid'] = $sid;
            // echo "<pre>"; print_r($data);exit;
            $this->template->load_view1('questions', $data);
     }
     public function save($tid=null,$qid=null){
            $breadcrumbarray = array('label'=> "Add Question",
                           'link' => base_url()."aptitude/questions/save/".$tid
                           );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $data['topicdetails'] = $this->aptitude_Model->gettopicdetails($tid);
            $data['catgeories'] = $this->aptitude_Model->getAllcompanaies();
            $data['topics'] = $this->aptitude_Model->getallsubcattopics($data['topicdetails']->subcat_id);
            $data['subcategories'] = $this->aptitude_Model->getsubcategories($data['topicdetails']->cat_id);
            //echo "<pre>"; print_r($data);exit;

            $validationRules = $this->_rules();
            foreach ($validationRules as $form_field)   {
            $rules[] = array(
            'name' => $form_field['field'],
            'display' => $form_field['label'],
            'rules' => $form_field['rules'],
            );
            }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('question_from',{$json_rules});
</script>
JS;

           if($qid !=null){
            $data['action'] = "edit";
            $this->template->set_subpagetitle("Edit Question");
            $data['details'] = $this->aptitude_Model->getquestiondetails($qid);
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Question");
           }
         


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          if ($this->form_validation->run() == true)  {
          
            $formvalues['cat_id'] = $this->input->post('c_id');
            $formvalues['sub_id'] = $this->input->post('sub_id');
            $formvalues['topic_id'] = $this->input->post('topic_id');
            $formvalues['question_name'] = $this->input->post('question_name');
            $formvalues['option1'] = $this->input->post('option1');
            $formvalues['option2'] = $this->input->post('option2');
            $formvalues['option3'] = $this->input->post('option3');
            $formvalues['option4'] = $this->input->post('option4');
            $formvalues['option5'] = $this->input->post('option5');
            $formvalues['question_explanation'] = $this->input->post('question_explanation');
            $formvalues['question_answer'] = $this->input->post('question_answer');
            
           // echo "<pre>"; print_r($formvalues);exit;
            $subid = $this->input->post('sub_id');
            if($qid !=null){
                $res = $this->aptitude_Model->updatequestionchoices($formvalues,$qid);
            }else {
                $res = $this->aptitude_Model->addquestiondetails($formvalues);
            }
            redirect('aptitude/questions/viewQuestions/'.$tid);
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('questions_setup',$data);
      }
        public function _rules() {
        $rules = array(
                array('field' => 'option1','label' => lang('option1'),'rules' => 'trim|required|xss_clean')
            
            
                 );
        return $rules;
    }
    
    
    public function viewQuestion($qid){
        $data['details'] = $this->aptitude_Model->getsquestiondetails($qid);
        $this->template->load_view1('question_view',$data);
    }

    

    public function changequestionStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['question_status'] = 'n';
            echo "0";
        } else {
            $formvalues['question_status'] = 'y';
            echo "1";
        }
        $this->aptitude_Model->changequestionStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->aptitude_Model->delete($id);

    }
    
      public function getsubcategories(){
       $catid = $_POST['selected'];
       $subcats = $this->aptitude_Model->getsubcategories($catid);
        header('Content-type: application/json');
        die( json_encode( $subcats ) );  
   
    }
    

}
