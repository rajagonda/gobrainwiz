<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : videos
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class emailsubjects_Model extends CI_Model {
    public function __construct() {
        parent::__construct();    
    }
    public function getAllsubject() {
        $result = $this->db->get('gk_email_subjects');
        if($result->num_rows() > 0 ) {
            return $result->result();
        } else {
            return '';
        }
    }
    public function addSubject($data) {
        $this->db->insert('gk_email_subjects', $data); 
        return $this->db->insert_id();
    }  
    
    public function getSubjectdetails($id) {
        $this->db->where('subject_id', $id);
        $query = $this->db->get('gk_email_subjects');
        if ($query->num_rows() > 0) {      
	    return $query->row();
        } else {
            return '';
        } 
     }

     public function updateSubjectdetails($values, $id) {
        $this->db->where('subject_id', $id);
        $this->db->update('gk_email_subjects', $values);
        return $this->db->affected_rows();       
    }
  
  
   
}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */