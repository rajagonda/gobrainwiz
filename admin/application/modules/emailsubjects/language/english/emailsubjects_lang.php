<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Video categories
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Catgeories
|--------------------------------------------------------------------------
*/


$lang['manage_subjects'] = 'Manage Subjects';
$lang['subject_add'] = 'Add Subject';
$lang['subject_edit'] = "Edit Subject";
$lang['subject_title'] = "Subject";







/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
