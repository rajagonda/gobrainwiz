<style type="text/css">
  .green {
    color: #69aa46 !important;
}
.red{
  color:red !important;
}
.errorClass { border:  1px solid red !important; }

</style>
<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_subjects');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th ><?php echo lang('subject_title');?></th>                                        
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        <a class="blue" href="<?php echo base_url();?>emailsubjects/save">
                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        </th>
                                        </tr>
                                      <?php
                                      if($details !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($details as  $value) {
                                      ?>
                                        <tr id='com_<?php echo $value->subject_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td ><?php echo $value->subject_title;?></td>                                      
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->subject_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changecate('<?php echo $value->subject_id;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url();?>emailsubjects/save/<?php echo $value->subject_id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                       <!--  |
                                        <a class="red trash" id="<?php echo $value->c_id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a> -->
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>




<script type="text/javascript">
    /**
     * By using this method change role status
     * 
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */
 function changecate(cid) { 
     var p_url= "<?php echo base_url(); ?>emailsubjects/changesubjectStatus";
   
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'cid='+cid,
     success: function(data) {
        alert("Successfully Changed");
       location.reload();
     }
         
     });  
   
     }
     
  
</script>