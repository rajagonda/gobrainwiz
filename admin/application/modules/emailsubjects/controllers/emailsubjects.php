<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : emailsubjects
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class emailsubjects extends MY_Controller {

       public function __construct(){
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('emailsubjects_Model');
            $this->load->language('emailsubjects');
       }	
       public function index()	{
            $breadcrumbarray = array('label'=> "emailsubjects",
                            'link' => base_url()."emailsubjects"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Categories");
            $data['details'] = $this->emailsubjects_Model->getAllsubject();
            $this->template->load_view1('emailsubjects', $data);
      }        
      public function changesubjectStatus() {
            $id = $_POST['cid'];
            $data['details'] = $this->emailsubjects_Model->getAllsubject();
            foreach($data['details'] as $value) {
            if($value->subject_id !=$id){
             $formvalues['subject_status'] = 'n';
             $result = $this->emailsubjects_Model->updateSubjectdetails($formvalues,$value->subject_id);
            }else {
             $formvalues['subject_status'] = 'y';
             $result = $this->emailsubjects_Model->updateSubjectdetails($formvalues,$value->subject_id);
            }
            }
            echo '1';
      }
      public function save($id=null){
            $breadcrumbarray = array('label'=> "emailsubjects",
                            'link' => base_url()."emailsubjects"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $validationRules = $this->_rules();
            foreach ($validationRules as $form_field)   {
            $rules[] = array(
            'name' => $form_field['field'],
            'display' => $form_field['label'],
            'rules' => $form_field['rules'],
            );
            }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('subject_from',{$json_rules});
</script>
JS;

            if($id !=''){
            $data['action'] = "edit";
            $data['subject'] = $this->emailsubjects_Model->getSubjectdetails($id);
            $this->template->set_subpagetitle("Edit Subject");
            }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Subject");
            }
            $this->form_validation->set_rules($validationRules);
            if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            if ($this->form_validation->run() == true)  {
            $form_values['subject_title'] = $this->input->post('subject_title');
            if($id !=''){
                
            $res = $this->emailsubjects_Model->updateSubjectdetails($form_values,$id);
            }else {
            $res = $this->emailsubjects_Model->addSubject($form_values);
            }
            redirect('emailsubjects');
            }
            }
      $data['script'] = $script;
      $this->template->load_view1('emailsubjects_setup',$data);
      }
      public function _rules() {
            $rules = array(
                array('field' => 'subject_title','label' => lang('subject_title'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
            return $rules;
     }
    
}
