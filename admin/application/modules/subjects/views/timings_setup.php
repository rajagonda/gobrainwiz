<div class="row">

            <div class="col-md-8 col-md-offset-2">

<section class="content">

                    <div class="row">

                        <!-- left column -->

                        <div class="col-md-8">

                            <!-- general form elements -->

                            <div class="box box-primary">

                                <div class="box-header">

                                    <h3 class="box-title"><?php echo lang('batch_add');?></h3>

                                </div><!-- /.box-header -->

                                <!-- form start -->

                                <?php

                                $url = current_url();

                                ?>

                                <form method="post"  action="<?php echo $url;?>" role="form" name="timing_from" id="timing_from" enctype="multipart/form-data">

                                    <div class="box-body">

                                        <div class="form-group">

                                            <label for="exampleInputEmail1"><?php echo lang('batch_name'); ?></label>

                                            <input type="text" value="<?php if($action=='edit') echo $batch->batch_name; ?>" placeholder="<?php echo lang('batch_name'); ?>" name="batch_name" id="batch_name" class="form-control">

                                        </div>

                                        <div class="form-group">

                                            <label for="exampleInputEmail1"><?php echo lang('faculty_name'); ?></label>

                                            <input type="text" value="<?php if($action=='edit') echo $batch->faculty_name; ?>" placeholder="<?php echo lang('faculty_name'); ?>" name="faculty_name" id="faculty_name" class="form-control">

                                        </div>

                                          <div class="form-group">

                                            <label><?php echo lang('timing_id'); ?>:</label>

                                            <select id="timing_id" name="timing_id" class="form-control">

                                                <option value=''>Please Select Timing</option>

                                                <?php foreach($timings as $value) { ?>

                                                <option value="<?php echo $value->timing_id; ?>" <?php if($action=='edit')  if($batch->timing_id == $value->timing_id) echo "selected";  ?>><?php echo $value->timing; ?></option>

                                                <?php } ?>

                                            </select>

                                        </div>

                                        <div class="form-group">

                                            <label><?php echo lang('subject_id'); ?>:</label>

                                            <select id="subject_id" name="subject_id" class="form-control">

                                                <option value=''>Please Select Subject</option>

                                                <?php foreach($subjects as $value) { ?>

                                                <option value="<?php echo $value->subject_id; ?>" <?php if($action=='edit')  if($batch->subject_id == $value->subject_id) echo "selected";  ?>><?php echo $value->subject_name; ?></option>

                                                <?php } ?>

                                            </select>

                                        </div>
										
                                        <div class="form-group">

                                        <label><?php echo lang('start_date'); ?>:</label>

                                        <div class="input-group">

                                            <div class="input-group-addon">

                                                <i class="fa fa-calendar"></i>

                                            </div>

                                            <input type="text" id='datepicker' name='start_date' value="<?php if($action=='edit') echo $batch->start_date; ?>" class="form-control" readonly=""  />

                                        </div><!-- /.input group -->

                                    </div><!-- /.form group -->

                                         <div class="bootstrap-timepicker">

                                        <div class="form-group">

                                            <label><?php echo lang('from'); ?>:</label>

                                            <div class="input-group">

                                                <input type="text" name="from" id="from" value="<?php if($action=='edit') echo $batch->from; ?>" class="form-control timepicker"/>

                                                <div class="input-group-addon">

                                                    <i class="fa fa-clock-o"></i>

                                                </div>

                                            </div><!-- /.input group -->

                                        </div><!-- /.form group -->

                                    </div>

                                    <div class="bootstrap-timepicker">

                                        <div class="form-group">

                                            <label><?php echo lang('to'); ?>:</label>

                                            <div class="input-group">

                                                <input type="text" name="to" id="to" value="<?php if($action=='edit') echo $batch->to; ?>" class="form-control timepicker"/>

                                                <div class="input-group-addon">

                                                    <i class="fa fa-clock-o"></i>

                                                </div>

                                            </div><!-- /.input group -->

                                        </div><!-- /.form group -->

                                    </div>

                                        

                                    </div><!-- /.box-body -->



                                    <div class="box-footer">

                                        <button class="btn btn-primary" type="submit">Submit</button>

                                    </div>

                                </form>

                            </div><!-- /.box -->





                        </div><!--/.col (left) -->

                        <!-- right column -->

                        

                    </div>   <!-- /.row -->

                </section>

            </div>

        </div>



       <?php 

echo $script;

?>

 <!-- InputMask -->

        <script src="<?php echo base_url();?>assets_new/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets_new/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets_new/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>

       

        

        <!-- bootstrap time picker -->

        <script src="<?php echo base_url();?>assets_new/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

        

        <!-- Page script -->

        <script type="text/javascript">

            $(function() {

                

                //Money Euro

                $("[data-mask]").inputmask();



               

//Timepicker

                $(".timepicker").timepicker({

                    showInputs: false

                });

                

               



               

            });

        </script>

        

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script>

$(function() {

$( "#datepicker" ).datepicker();

});

</script>