<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('subject_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="subject_from" id="subject_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('subject_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $subject->subject_name; ?>" placeholder="<?php echo lang('subject_name'); ?>" name="subject_name" id="subject_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('subject_desc'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $subject->subject_desc; ?>" placeholder="<?php echo lang('subject_desc'); ?>" id="subject_desc" name="subject_desc" class="form-control">
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
