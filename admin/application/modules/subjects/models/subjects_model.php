<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Subject
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Subjects_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'subjects');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'subjects',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('subject_id', $id);
	    $this->db->update(DB_PREFIX.'subjects', $data);
	    return $this->db->affected_rows();
	}
	public function getsubjectDetails($id){
		$this->db->where('subject_id', $id);
		$query = $this->db->get(DB_PREFIX.'subjects');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changesubjectStatus($data, $id){
		$this->db->where('subject_id', $id);
	    $this->db->update(DB_PREFIX.'subjects', $data);
            //echo $this->db->last_query();
	    return $this->db->affected_rows();
	}
	public function deleterole($id){
		$this->db->where('subject_id', $id);
        $this->db->delete(DB_PREFIX.'subjects');
	}


}

/* End of file subjects_model.php */
/* Location: ./application/models/subjects_model.php */