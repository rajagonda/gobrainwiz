<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Timings
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Timings_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
            
            $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id ORDER BY batch_id DESC";

                 $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }

	}
        public function getTimings(){
            $query = $this->db->get(DB_PREFIX.'timings');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}
        }

        public function save($data){
		$this->db->insert(DB_PREFIX.'batches',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('batch_id', $id);
	    $this->db->update(DB_PREFIX.'batches', $data);
	    return $this->db->affected_rows();
	}
	public function getbatchDetails($id){
		  $sql = "SELECT * FROM gk_batches 
                INNER JOIN gk_subjects ON gk_subjects.subject_id = gk_batches.subject_id
                INNER JOIN gk_timings ON gk_timings.timing_id = gk_batches.timing_id
                WHERE gk_batches.batch_id ='$id'";

                 $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->row();
            } else {
            return '';
            }
	}
	public function changebatchStatus($data, $id){
		$this->db->where('batch_id', $id);
	    $this->db->update(DB_PREFIX.'batches', $data);
            
	    return $this->db->affected_rows();
	}
	public function deletebatch($id){
		$this->db->where('batch_id', $id);
        $this->db->delete(DB_PREFIX.'batches');
	}


}

/* End of file subjects_model.php */
/* Location: ./application/models/subjects_model.php */