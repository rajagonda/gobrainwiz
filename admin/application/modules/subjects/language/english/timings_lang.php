<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Timings
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Timings
|--------------------------------------------------------------------------
*/


$lang['manage_batches'] = 'Manage Batches';
$lang['batch_add'] = 'Add Batch';
$lang['batch_edit'] = "Edit Batch";
$lang['batch_name'] = "Batch Name";
$lang['timing_id'] = "Timing";
$lang['subject_id'] = "Subject";
$lang['from'] = "From";
$lang['to'] = "To";
$lang['faculty_name'] = "Faculty Name";
$lang['start_date'] = "Start Date";



/* End of file Timings_lang.php */
/* Location: ./application/module_core/Timings_lang/language/english/Timings_lang.php */
