<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Subject
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Subjects extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('Subjects_Model');
        $this->load->language('subjects');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Subjects",
                            'link' => base_url()."subjects"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Subjects");
           $data['subjects'] = $this->Subjects_Model->get();
           $this->template->load_view1('subjects', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Subjects",
                           'link' => base_url()."subjects"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('subject_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['subject'] = $this->Subjects_Model->getsubjectDetails($id);
            $this->template->set_subpagetitle("Edit Subject");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Subject");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['subject_name'] = $this->input->post('subject_name');
            $form_values['subject_desc'] = $this->input->post('subject_desc');    

            if($id !=''){
             $res = $this->Subjects_Model->update($form_values,$id);
            }else {
              $res = $this->Subjects_Model->save($form_values);

            }
             redirect('subjects');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('subjects_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'subject_name','label' => lang('subject_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'subject_desc','label' => lang('subject_desc'),'rules' => 'required'));
        return $rules;
    }

	 public function changesubjectStatus(){
        $id = $_POST['subject_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['subject_status'] = 'n';
            echo "0";
        } else {
            $formvalues['subject_status'] = 'y';
            echo "1";
        }
        $this->Subjects_Model->changesubjectStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Subjects_Model->deleterole($id);

    }

}
