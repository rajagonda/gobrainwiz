<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Timings
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Timings extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('timings_model','subjects_model'));
        $this->load->language(array('subjects','timings'));
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Timings",
                            'link' => base_url()."subjects/timings"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Timings");
           $data['timings'] = $this->timings_model->get();
           
           $this->template->load_view1('timings', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Timings",
                           'link' => base_url()."subjects/timings"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        

        $data['timings'] = $this->timings_model->getTimings();
        $data['subjects'] = $this->subjects_model->get();
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('timing_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['batch'] = $this->timings_model->getbatchDetails($id);
            $this->template->set_subpagetitle("Edit Batch");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Batch");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
              if ($this->form_validation->run() == true)  {
          
            $form_values['batch_name'] = $this->input->post('batch_name');
            $form_values['timing_id'] = $this->input->post('timing_id');    
            $form_values['subject_id'] = $this->input->post('subject_id');    
            $form_values['from'] = $this->input->post('from');    
            $form_values['to'] = $this->input->post('to'); 
					
            $form_values['start_date'] = date('Y-m-d',  strtotime($this->input->post('start_date')));    
            $form_values['faculty_name'] = $this->input->post('faculty_name');    
            

            if($id !=''){
             $res = $this->timings_model->update($form_values,$id);
            }else {
              $res = $this->timings_model->save($form_values);

            }
             redirect('subjects/timings');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('timings_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'batch_name','label' => lang('batch_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'timing_id','label' => lang('timing_id'),'rules' => 'required'),
                array('field' => 'subject_id','label' => lang('subject_id'),'rules' => 'required'),
                array('field' => 'from','label' => lang('from'),'rules' => 'required'),
                array('field' => 'to','label' => lang('to'),'rules' => 'required'),
				
                array('field' => 'faculty_name','label' => lang('faculty_name'),'rules' => 'required'),
                array('field' => 'start_date','label' => lang('start_date'),'rules' => 'required')
            );
        return $rules;
    }

	 public function changebatchStatus(){
        $id = $_POST['batch_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['batch_status'] = 'n';
            echo "0";
        } else {
            $formvalues['batch_status'] = 'y';
            echo "1";
        }
        $this->timings_model->changebatchStatus($formvalues, $id);
    }
    public function deletebatch(){
        $id =  $this->input->post('id');
        $this->timings_model->deletebatch($id);

    }

}
