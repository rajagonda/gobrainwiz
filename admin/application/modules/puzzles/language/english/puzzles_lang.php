<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| puzzles
|--------------------------------------------------------------------------
*/


$lang['manage_puzzles'] = 'Manage Puzzles';
$lang['puzzle_add'] = 'Add Puzzle';
$lang['puzzle_edit'] = "Edit Puzzle";
$lang['puzzle_topic'] = "Puzzle Topic";
$lang['puzzle_date'] = "Puzzle Date";

$lang['manage_questions'] = "Manage Questions";
$lang['question'] = "Quastion";
$lang['add_question'] = " Add Quastion";



/* End of file puzzles_lang.php */
/* Location: ./application/module_core/Puzzles/language/english/puzzles_lang.php */
