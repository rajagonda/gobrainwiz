<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Groups
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class puzzles extends MY_Controller {

	public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
        
            
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('puzzles_model');
            $this->load->language('puzzles');
       }
       public function index() {
	    $breadcrumbarray = array('label'=> "Puzzles",
                               'link' => base_url()."puzzles"
                               );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Puzzles");
            $data['puzzles'] = $this->puzzles_model->get();
            $this->template->load_view1('puzzles', $data);
	}

	public function save($id=null){
            $breadcrumbarray = array('label'=> "Puzzles",
                           'link' => base_url()."puzzles"
                           );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('puzzle_from',{$json_rules});
</script>
JS;

           if($id !=''){
            $data['action'] = "edit";
            $data['puzzle'] = $this->puzzles_model->getpuzzleDetails($id);
//            echo "<pre>"; print_r($data);exit;
            $this->template->set_subpagetitle("Edit Puzzle Topic");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Puzzle Topic");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['puzzle_topic'] = $this->input->post('puzzle_topic');
            if($id !=''){
             $res = $this->puzzles_model->update($form_values,$id);
            }else {
              $res = $this->puzzles_model->save($form_values);

            }
             redirect('puzzles');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('puzzles_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'puzzle_topic','label' => lang('puzzle_topic'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
        return $rules;
    }

	 public function changeStatus(){
        $id = $_POST['puzzle_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['puzzle_status'] = 'n';
            echo "0";
        } else {
            $formvalues['puzzle_status'] = 'y';
            echo "1";
        }
        $this->puzzles_model->changeStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->puzzles_model->delete($id);

    }

}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */