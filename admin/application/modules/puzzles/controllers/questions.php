<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : questions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class questions extends MY_Controller {
      public function __construct() {
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        
        
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('questions_model','puzzles_Model'));
        $this->load->language('puzzles');
     }
     public function index()	{
        $data['puzzles'] = $this->questions_model->get();
        $this->template->load_view1('questions', $data);
     }
    public function save($id=NULL){
        
        $breadcrumbarray = array('label'=> "Add Question",
                           'link' => base_url()."puzzles/questions/save"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $data['puzzles'] = $this->puzzles_Model->get();
        //echo "<pre>"; print_r($data);exit;
        
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('question_from',{$json_rules});
</script>
JS;

           if($id !=null){
            $data['action'] = "edit";
            $this->template->set_subpagetitle("Edit Question");
            $data['details'] = $this->questions_model->getquestiondetails($id);
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Question");
           }
         
         // echo "<pre>"; print_r( $data['details']);exit;

          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          if ($this->form_validation->run() == true)  {
              
              if($id !=''){
                if($this->input->post('puzzle_date') != $data['details']->puzzle_date){
                $date = $this->input->post('puzzle_date');
                $res = $this->questions_model->checkpuzzledate($date);
                if($res !=''){
                $this->session->set_userdata('puz_filure', "Sorry Puzzle Date Alredy Exist!!");
                redirect(base_url().'puzzles/questions/save');
                }
                }
              }else {
                $date = $this->input->post('puzzle_date');
                $res = $this->questions_model->checkpuzzledate($date);

                if($res !=''){
                $this->session->set_userdata('puz_filure', "Sorry Puzzle Date Alredy Exist!!");
                redirect(base_url().'puzzles/questions/save');
                }
              }
              
              
             
             
            $formvalues['puzzle_id'] = $this->input->post('puzzle_id');
            $formvalues['puzzle_date'] = $this->input->post('puzzle_date');
            $formvalues['question_name'] = $this->input->post('question_name');
            $formvalues['option1'] = $this->input->post('option1');
            $formvalues['option2'] = $this->input->post('option2');
            $formvalues['option3'] = $this->input->post('option3');
            $formvalues['option4'] = $this->input->post('option4');
            $formvalues['option5'] = $this->input->post('option5');
            $formvalues['question_explantion'] = $this->input->post('question_explanation');
            $formvalues['question_answer'] = $this->input->post('question_answer');
            if($id !=''){
                $res = $this->questions_model->update($formvalues,$id);
            }else {
            $res = $this->questions_model->save($formvalues);
            }
            
            redirect('puzzles/questions/');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('questions_setup',$data);
      }
        public function _rules() {
        $rules = array(
                array('field' => 'option1','label' => lang('option1'),'rules' => 'trim|required|xss_clean')
            
            
                 );
        return $rules;
    }
    
    
    public function viewQuestion($qid){
        $data['details'] = $this->questions_model->getquestiondetails($qid);
        //echo "<pre>"; print_r($data);exit;
         $this->template->load_view1('question_view',$data);
    }

    

    public function changequestionStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['question_status'] = 'n';
            echo "0";
        } else {
            $formvalues['question_status'] = 'y';
            echo "1";
        }
        $this->questions_model->changequestionStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->questions_model->delete($id);

    }
    

}
