<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class puzzles_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'puzzles');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'puzzles',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('puzzle_id', $id);
	    $this->db->update(DB_PREFIX.'puzzles', $data);
	    return $this->db->affected_rows();
	}
	public function getpuzzleDetails($id){
		$this->db->where('puzzle_id', $id);
		$query = $this->db->get(DB_PREFIX.'puzzles');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changeStatus($data, $id){
		$this->db->where('puzzle_id', $id);
	    $this->db->update(DB_PREFIX.'puzzles', $data);
	    return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('puzzle_id', $id);
        $this->db->delete(DB_PREFIX.'puzzles');
	}


}

/* End of file puzzles_model.php */
/* Location: ./application/models/puzzles_model.php */