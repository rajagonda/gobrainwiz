<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Company
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class questions_model extends CI_Model {
       public $variable;
       public function __construct() {
	    parent::__construct();
		
	}
	public function get(){
           $sql = "SELECT * FROM gk_puzzlequestions
                   INNER JOIN gk_puzzles  ON gk_puzzles.puzzle_id = gk_puzzlequestions.puzzle_id";
          $query = $this->db->query($sql);
          if($query->num_rows()>0) {
            return $query->result();
          } else {
            return '';
          }
        }
        public function getquestiondetails($qid){
            $sql = "SELECT * FROM gk_puzzlequestions
                   INNER JOIN gk_puzzles  ON gk_puzzles.puzzle_id = gk_puzzlequestions.puzzle_id
                   WHERE gk_puzzlequestions.question_id = '$qid'";
          $query = $this->db->query($sql);
          if($query->num_rows()>0) {
            return $query->row();
          } else {
            return '';
          }
        }
        
       
	public function save($data){
		$this->db->insert(DB_PREFIX.'puzzlequestions',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('question_id', $id);
	    $this->db->update(DB_PREFIX.'puzzlequestions', $data);
	    return $this->db->affected_rows();
	}
	
	public function changequestionStatus($data, $id){
	    $this->db->where('question_id', $id);
	    $this->db->update(DB_PREFIX.'puzzlequestions', $data);
            return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('question_id', $id);
        $this->db->delete(DB_PREFIX.'puzzlequestions');
	}
         public function checkpuzzledate($data) {
         $this->db->where('puzzle_date', $data);
         $result = $this->db->get(DB_PREFIX.'puzzlequestions');
        if($result->num_rows() > 0 ) {
            return $result->row();
        } else {
            return '';
        }
    }
       
}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */