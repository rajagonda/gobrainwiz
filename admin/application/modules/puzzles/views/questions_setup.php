<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="#" role="form" name="question_from" id="question_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('add_question');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><?php echo lang('puzzle_topic'); ?></label>
                                   <select name="puzzle_id"  id="puzzle_id" class="form-control">
                                <option value="">Select Puzzle Topic</option>
                                <?php
                                foreach ($puzzles as $value)
                                {?>                          
                                <option value="<?php echo $value->puzzle_id;?>" <?php  if($action=='edit') if($value->puzzle_id == $details->puzzle_id) echo "selected";  ?>><?php echo $value->puzzle_topic;?></option>;
                                <?php  
                                }
                                ?>
                                </select>       
                                </div>
                                <div class="form-group">
                                   <label for="exampleInputEmail1"><?php echo lang('puzzle_date'); ?></label>
                                   <input type="text" id='datepicker'  value="<?php if($action=='edit') echo $details->puzzle_date; ?>" placeholder="<?php echo lang('puzzle_date'); ?>" name="puzzle_date"   class="form-control">
                               </div>

                                        <div class="form-group">
                                            <label>Question</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_name" id="question_name" class="form-control"><?php if($action == 'edit') echo $details->question_name; ?></textarea>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 1</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option1" id="option1" class="form-control"><?php if($action == 'edit') echo $details->option1; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 2</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option2" id="option2" class="form-control"><?php if($action == 'edit') echo $details->option2; ?></textarea>
                                        </div>
                                       <div class="form-group">
                                            <label>Choice 3</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option3" id="option3" class="form-control"><?php if($action == 'edit') echo $details->option3; ?></textarea>
                                        </div>
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                               
                                    <div class="box-body">
                                        
                                        
                                        <div class="form-group">
                                            <label>Choice 4</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option4" id="option4" class="form-control"><?php if($action == 'edit') echo $details->option4; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 5</label>
                                            <textarea placeholder="Enter ..." rows="3" name="option5" id="option5" class="form-control"><?php if($action == 'edit') echo $details->option5; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Explanation</label>
                                            <textarea placeholder="Enter ..." rows="3" name="question_explanation" id="question_explanation" class="form-control"><?php if($action == 'edit') echo $details->question_explantion; ?></textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Answer</label>
                         <select name="question_answer" id="question_answer"  class="form-control">
                            <option value="">Select Answer</option>
                            <option value="1">Choice 1</option>
                            <option value="2">Choice 2</option>
                            <option value="3">Choice 3</option>
                            <option value="4">Choice 4</option>
                            <option value="5">Choice 5</option>
                         </select>           
                                
                        <div class="clear"></div>
                    </div> 
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
<?php

//echo $script;
?>



 <script type="text/javascript">



            $(function() {


                 CKEDITOR.replace( 'question_name', {
                extraPlugins: 'mathjax'
                });

                  CKEDITOR.replace( 'option1', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option2', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option3', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option4', {
                extraPlugins: 'mathjax'
                });
                
                  CKEDITOR.replace( 'option5', {
                extraPlugins: 'mathjax'
                });
                  CKEDITOR.replace( 'question_explanation', {
                extraPlugins: 'mathjax'
                });
                  
               
                


               
               
            });
        </script>
      

<script>
$(function() {
$( "#datepicker" ).datepicker();
});
</script>
 <script type="text/javascript"> 
 $(document).ready(function () {
     
     $("#question_from").validate({
                ignore: [], 
        rules: {
            
            
                    question_name: {
                    required: function() 
                    {
                    CKEDITOR.instances.question_name.updateElement();
                    },
                    maxlength: 3000
                    },
                    puzzle_id : {
                        required:true
                    },
                    puzzle_date : {
                        required:true
                    },
                        question_answer :{
                            required: true
                        },
                       
                        option1: {
                            required: true
                        },
                        option2: {
                            required: true
                        },
                        option3: {
                            required: true
                        },
        },
        
    });
     });

    </script>