<!-- Main content -->
<section class="content">
    <!-- right column -->
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Search For Short Url
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-3">
                        Image Name Or Text
                        <input type="text" class="form-control" id="imgaeText" name="imgaeText"
                               placeholder="Image name Or Text">
                        <span id="urlerror" style="color: Red; display: none"></span>
                    </div>
                    <div class="col-xs-3">
                        <label></label>
                        <button type="button" id="search" class="btn btn-info pull-right">Search</button>
                    </div>

                </div>


            </div>

        </div>


    </div>
    <!--/.col (right) -->
    <div id="ajaxdata">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Images</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th style="width: 10px"><?php echo lang('Sno'); ?></th>
                                <th>Image Text</th>
                                <th>Image</th>
                                <th>Url</th>
                                <th>
                                    <a class="blue" href="<?php echo base_url(); ?>fileUpload/save">
                                        <span class="glyphicon glyphicon-plus "
                                              style="font-size:150%;color:#438EB9;;"></span>
                                    </a>
                                </th>
                            </tr>
                            <?php
                            if ($images != ''){
                            $i = 1;
                            $j = 1;
                            foreach ($images

                            as  $value) {
                            ?>
                            <tr id='com_<?php echo $value->imageId; ?>'>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value->imageText; ?></td>
                                <td><img src="../upload/images/<?php echo $value->imageName; ?>"></td>
                                <td align="center">
                                    http://gobrainwiz.in/upload/images/<?php echo $value->imageName; ?></td>
                                <td>
                                    <!-- <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url(); ?>videos/edit/<?php echo $value->imageId; ?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        | -->
                                    <!--  <a class="red trash" id="<?php echo $value->id; ?>" onclick="if(window.confirm('Are you sure, you want to delete this video ?')){window.location='<?php echo base_url(); ?>videos/deleteVideo/<?php echo $value->id; ?>'}" href="javascript:void(0)" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a> -->
                    </div>
                    </td>
                    </tr>
                    <?php
                    $i++;
                    $j++;
                    }
                    }
                    ?>
                    </tbody></table>
                </div><!-- /.box-body -->
                <!-- <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>-->


            </div><!-- /.col -->

        </div><!-- /.row -->
    </div>

</section>


<script type="text/javascript">
    /**
     * By using this method change role status
     *
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */

    function deleteVideo(video_id) {
        var delete
        = window.confirm("Are you sure, you want to delete this video ?");
        if (delete)
            window.location = "<?php echo site_url(); ?>";
    }

    function changeStatus(id, tid, status) {
        var p_url = "<?php echo base_url(); ?>admin/homevideos/changevdeoStatus";
        var ajaxLoading = false;
        if (!ajaxLoading) {
            var ajaxLoading = true;
            $('#' + id).html('<img src="<?php echo base_url();?>themes/admin/images/sp/loading_small.gif">');
            jQuery.ajax({
                type: "POST",
                url: p_url,
                data: 'tid=' + tid + '&status=' + status,
                success: function (data) {
                    if (data == 1) {
                        $('#' + id).html('<a href="#" onclick="changeStatus(\'' + id + '\', \'' + tid + '\', \'y\')"><img src="<?php echo base_url();?>themes/admin/images/icons/color/tick.png" alt="" /></a>');
                        $('#divSuccessMsg').show();
                        $('#divSuccessMsg').html("<i class='icon-check'></i><span class='text-success'>Sucessfully Changed Status</span>")
                            .fadeIn(1000).fadeOut(5000);

                    } else {
                        $('#' + id).html('<a href="#" onclick="changeStatus(\'' + id + '\', \'' + tid + '\', \'n\')"><img src="<?php echo base_url();?>themes/admin/images/icons/color/cross.png" alt="" /></a>');
                        $('#divSuccessMsg').show();
                        $('#divSuccessMsg').html("<i class='icon-check'></i><span class='text-success'>Sucessfully Changed Status</span>")
                            .fadeIn(1000).fadeOut(5000);
                    }
                    ajaxLoading = false;
                }

            });

        }
    }


</script>


<script>
    $(document).ready(function () {

        $("#search").on('click', function () {

            var imgaeText;
            imgaeText = $("#imgaeText").val();
            if (imgaeText == '') {
                $("#urlerror").css('display', 'block');
                $("#urlerror").html('Image Text Or Name Required');
                return false;
            }


            var p_url = "<?php echo base_url(); ?>fileUpload/getImageData";
            var ajaxLoading = false;
            if (!ajaxLoading) {
                var ajaxLoading = true;
                $('#ajaxdata').html('<img src="<?php echo base_url(); ?>assets/images/loading_small.gif">');
                $.ajax({
                    type: "POST",
                    url: p_url,
                    data: 'imgaeText=' + imgaeText,
                    success: function (data) {
                        $("#ajaxdata").html(data);
                        ajaxLoading = false;
                    }

                });
            }
            return false;

        });


    });

</script>