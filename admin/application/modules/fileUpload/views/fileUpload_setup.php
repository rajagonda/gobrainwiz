<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Add Image</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="video_from" id="video_from" enctype="multipart/form-data">
                                    <div class="box-body">     
                                     <div class="form-group">

                                            <label for="exampleInputEmail1">Image Text</label>

                                            <input type="text" value="<?php if($action=='edit') echo $banner->imageText; ?>" placeholder="Enter Image Text" name="imageText" id="imageText" class="form-control">

                                        </div>                             
                                  
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Image File</label>
                                        <input type="file"  name="imagefile" id="imagefile" >
                                    </div>                                       
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                    <input type="hidden" name="uname" value="x">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                        <a href='/admin/fileUpload' class="btn btn-info" >Back</a>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>