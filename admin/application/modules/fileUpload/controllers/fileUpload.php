<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : fileUpload
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Videos
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class fileUpload extends MY_Controller {

      public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('fileupload_Model');
            $this->load->language('fileUpload');
      }
      public function index()	{
            $breadcrumbarray = array('label'=> "fileUpload",
                            'link' => base_url()."fileUpload"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage fileUpload");
            $data['images'] = $this->fileupload_Model->getAllImages();
            $this->template->load_view1('manageImages', $data);
      }

      public function edit($video_id){

            $breadcrumbarray = array('label'=> "videos/Edit",
                            'link' => base_url()."videos/edit/$video_id"
                              );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage fileUpload");
            $this->videos_Model->updateVideoDetails($video_id);
            $data['video'] = $this->videos_Model->getvideodetails($video_id);
            $data['categories']=$this->videos_Model->getAllcategories();
            $this->template->load_view1('edit_video_setup',$data);
       
      }
      public function save($id=null){
            $breadcrumbarray = array('label'=> "fileUpload",
                           'link' => base_url()."fileUpload"
                          );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);       
         

           
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Images");
            
           if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
         
             
            if($_FILES['imagefile']['name'] !='') {
           
            $filename = time()."_".$_FILES['imagefile']['name'];
            $config['upload_path'] = '../upload/images/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 50;
            $config['file_name'] = $filename;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('imagefile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }  else {
            }
            $data = $this->upload->data();
           
            $timg = $data['file_name'];
            $formvalues['imageName'] = $timg;    
            $formvalues['imageText'] =  $this->input->post('imageText');            

            $res = $this->fileupload_Model->insertImages($formvalues);
            redirect(base_url().'fileUpload');        

           }else {            
            redirect(base_url().'fileUpload/save');    
           }          
                
        }

      
      $this->template->load_view1('fileUpload_setup',$data);
	}


	 public function changetopperStatus(){
        $id = $_POST['voice_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['voice_status'] = 'n';
            echo "0";
        } else {
            $formvalues['voice_status'] = 'y';
            echo "1";
        }
        $this->videos_Model->changetopperStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->videos_Model->deletetopper($id);

    }

    public function deleteCat(){
     $cat=$this->input->post('id');
     $this->db->query("DELETE FROM gk_hvcategoriess WHERE c_id='$cat'");
    }

    public function deleteVideo($video_id)
    {
        $this->db->query("DELETE FROM gk_hvideos WHERE id='$video_id'");
        redirect('videos');
    }
     public function getImageData(){
      if (isset($_POST) && is_array($_POST) && count($_POST) > 0) { 
        $imgaeText = $this->input->post('imgaeText');

        
        if($imgaeText !=''){
          $data['images'] = $this->fileupload_Model->getImageData($imgaeText);
          $this->load->view('ajaxdata',$data);
        }

      }
    }

    

}
