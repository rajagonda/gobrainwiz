<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : placement
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| practicetest
|--------------------------------------------------------------------------
*/


$lang['manage_braintest'] = 'Manage Brain Test';
$lang['braintest_add'] = 'Add Test';
$lang['braintest_edit'] = "Edit Test";
$lang['com_name'] = "Test Name";
$lang['no_of_student'] = "No of student";
$lang['test_date'] = "Test Date";

$lang['c_id'] = "Parent";
$lang['company_add'] = "Brain Test";



$lang['manage_student'] = 'Manage Brain Test students';
$lang['student_update'] = 'Manage student Profile';

$lang['student_id'] = 'ID';
$lang['student_password'] = "Password";
$lang['student_name'] = "Name";
$lang['student_phone'] = "Phone";
$lang['student_address'] = "Address";
$lang['student_email'] = "Email";


$lang['add'] = "Add";
$lang['view'] = 'view';
$lang['question'] = 'Question';
$lang['questions'] = 'Questions';


$lang['manage_passages'] = 'Manage Passages';
$lang['passage_add'] = 'Add Passage';
$lang['passage_edit'] = "Edit Passage";
$lang['passage_title'] = "Passage Title";
$lang['passage'] = "Passage";


$lang['manage_test'] = 'Manage Test';
$lang['test_add'] = 'Add Test';
$lang['test_edit'] = "Edit Test";
$lang['test_name'] = "Test Name";
$lang['test_time'] = "Time";
$lang['test_show'] = "Test Show";

$lang['question_set'] = 'Question Set';
$lang['question_set_upload'] = 'Upload Question Set';

$lang['question_set_name'] = 'Set file name';
$lang['question_set_time'] = 'Upload Time';
$lang['question_name'] = 'Question Name';
$lang['question_answer'] = 'Question Answer';





$lang['manage_questions'] = 'Manage Questions';
$lang['question_add'] = 'Add Question';
$lang['question_edit'] = "Edit Question";
$lang['question_details'] = "Question Details";
$lang['option1'] = "option1";
$lang['type_question'] = "Type Of Question";
$lang['type'] = "Type";
$lang['category'] = "Section";

$lang['duration'] = "Exam Duration in minutes";
$lang['break_time'] = "Break time in minutes";





/* End of file practicetest_lang.php */
/* Location: ./application/module_core/practicetest/language/english/practicetest_lang.php */
