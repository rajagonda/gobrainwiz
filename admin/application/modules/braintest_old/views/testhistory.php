<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title">Test History</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">								<div class="table-responsive">
									<table class="table table-bordered table-striped">
                                    <tbody>
										<tr>
											<th align="center"><?php echo lang('Sno');?></th>
											<th align="center" style="text-align: center !important;" ><?php echo lang('com_name');?></th>
                      <th  align="center" style="text-align: center !important;">Attend Students</th>
											<th align="center" style="text-align: center !important;">
											Actions
											</th>
                                        </tr>
                                      <?php
                                      if($braintest !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($braintest as  $value) {
                                      ?>
                                        <tr id='com_<?php echo $value->brain_test_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->test_name;?></td> 
                                        <td align="center">
                                        <?php echo $countstudents[$value->brain_test_id]; ?>
                                        </td>
                                      
                                        <td align="center">
                                        <div class="action-buttons">
                                        
                                        <a class="" alt="View Result" title="Question set"  href="<?php echo base_url();?>braintest/testhistory/viewstudents/<?php echo $value->brain_test_id;?>" >
                                          <i class="ace-icon fa fa-arrows-alt bigger-130"></i>
                                        </a>   

                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.table-responsive -->                            </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>braintest/delete',
          success: function(msg) {
            $("#com_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id1, id,  status) { 
     var p_url= "<?php echo base_url(); ?>braintest/changeTestStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'id='+id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>