<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_braintest');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">								<div class="table-responsive">
									<table class="table table-bordered table-striped">
                                    <tbody>
										<tr>
											<th style="width: 10px"><?php echo lang('Sno');?></th>
											<th ><?php echo lang('com_name');?></th>
											                                       <th ><?php echo lang('test_date');?></th>
											<th ><?php echo lang('test_time');?></th>
											<th style="text-align:center !important;"><?php echo lang('status');?></th>
											<th>
											<a class="blue" title="Add Test" href="<?php echo base_url();?>braintest/save">
											<span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
											</a>
											</th>
                                        </tr>
                                      <?php
                                      if($braintest !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($braintest as  $value) {
                                      ?>
                                        <tr id='com_<?php echo $value->brain_test_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->test_name;?></td> 
                                                                                                                    <td align="center"><?php echo $value->test_date;?></td>                                        <td align="center"><?php echo $value->test_time;?></td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->test_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->brain_test_id;?>', '<?php echo $value->test_status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->brain_test_id;?>', '<?php echo $value->test_status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="action-buttons">
                                        <a class="green" title="Edit" href="<?php echo base_url();?>braintest/save/<?php echo $value->brain_test_id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>										|                                        <a class="blue" alt="students" title="students"  href="<?php echo base_url();?>braintest/students/<?php echo $value->brain_test_id;?>" >											<i class="glyphicon glyphicon-user"></i>                                        </a>
                                        |										<a class="" alt="questionset" title="Question set"  href="<?php echo base_url();?>braintest/questionset/<?php echo $value->brain_test_id;?>" >											<i class="glyphicon glyphicon-question-sign"></i>                                        </a>                                        |                                        <a class="red trash" title="trash" id="<?php echo $value->brain_test_id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.table-responsive -->                            </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>braintest/delete',
          success: function(msg) {
            $("#com_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id1, id,  status) { 
     var p_url= "<?php echo base_url(); ?>braintest/changeTestStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'id='+id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>