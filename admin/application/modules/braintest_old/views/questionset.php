<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('question_set');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">								<div class="table-responsive">
									<table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th ><?php echo lang('Sno');?></th>
                                         <th  ><?php echo lang('question_set_name');?></th>	
										 <th  ><?php echo lang('question_set_time');?></th>
                                        <th ><?php echo "Total No Of Quetions";?></th>

                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        <a class="blue" title="upload quetion set" href="<?php echo base_url();?>braintest/uploadquetion/<?php echo $testId;?>">
                                        <span class="glyphicon glyphicon-upload " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        </th>
                                        </tr>
                                      <?php
                                      if($questionset !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($questionset as  $value) {
                                      ?>
                                        <tr id='question_<?php echo $value->set_id;?>'>
                                        <td><?php echo $i;?></td>
                                        
                                        <td ><?php echo $value->file_name;?></td>	
										<td ><?php echo $value->uploaded_on;?></td>
										<td ><?php echo $value->qus_count;?></td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->set_id;?>', '<?php echo $value->status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->set_id;?>', '<?php echo $value->status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="action-buttons">
                                        <a class="green"  title="view questions" href="<?php echo base_url().'braintest/viewquestion/'.$value->test_id.'/'.$value->set_id;?>" >
                                        <i class="ace-icon fa fa-arrows-alt bigger-130"></i>
                                        </a>
<!--                                        |
                                        <a class="green" href="<?php echo base_url();?>braintest/questions/save/<?php echo $value->sub_id;?>/<?php echo $value->set_id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        -->										|                                        <a class="red trash" title="trash set" id="<?php echo $value->set_id;?>" href="#" >                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.responsive -->                            </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>braintest/questionsetdelete',
          success: function(msg) {
            $("#question_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id1, id,  status) { 
     var p_url= "<?php echo base_url(); ?>braintest/changequestionsetStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'id='+id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
</script>