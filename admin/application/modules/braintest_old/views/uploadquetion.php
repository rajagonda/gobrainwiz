<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_set_upload');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
							<?php 							
							if(!empty($error))				
							{							?>
							<div class="alert alert-danger"><?php echo $error;?></div>	
							<?php 							
							}	
							?>							
							<?php echo form_open_multipart('braintest/do_upload');?>															
							<div class="form-group">          
							<input type="file" name="userfile" size="20" class="form-control"/>										
							<input type="hidden" name="testId" value="<?php echo $testId;?>" />
							<label>*Allowed File Type .xlsx</label>                        
							</div>						
							<br /><br />
							
								<div class="pull-center">
								
									<input type="submit" value="upload" class="btn btn-primary" />	
								</div>
							</form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
					<div class="row">
						<div class="col-md-4 block col-md-offset-2">
							<button type="button" class="btn btn-primary btn-lg btn-block " disabled>OR</button>	
							<p></p>
						</div>
					</div>
					
					<div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Import Questions From Test</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
														
							<?php echo form_open_multipart('braintest/import_questions');?>	
															
							<div class="form-group">  
								<label>Select a test</label>                        
								<select name="import_questions" id="import_questions" class="form-control">
									<?php 
										foreach($braintest as $testkey => $test)
										{
											if($test->brain_test_id!=$testId)
											{
									?>
									
										<option value="<?php echo $test->brain_test_id;?>">
											<?php echo $test->test_name." - ".$test->test_date." at ".$test->test_time ;?>
										</option>
										<?php 
											}
										}
									?>
								</select>
								
								<input type="hidden" name="testId" value="<?php echo $testId;?>" />
							</div>						
							<br /><br />				
							<input type="submit" value="Import" class="btn btn-primary" />	
							</form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
