<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('student_update');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topper_from" id="topper_from" enctype="multipart/form-data">
                                    <div class="box-body">										<div class="form-group">                                            <label for="exampleInputEmail1"><?php echo lang('student_id'); ?></label>                                            <input type="text" readonly value="<?php if($action=='edit') echo $student->studentId; ?>" placeholder="<?php echo lang('student_id'); ?>" name="studentId" id="studentId" class="form-control">                                        </div>										<div class="form-group">                                            <label for="exampleInputEmail1"><?php echo lang('student_password'); ?></label>                                            <input type="text" readonly value="<?php if($action=='edit') echo $student->password; ?>" placeholder="<?php echo lang('student_password'); ?>" name="password" id="password" class="form-control">                                        </div>										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('student_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $student->name; ?>" placeholder="<?php echo lang('student_name'); ?>" name="name" id="name" class="form-control">
                                        </div>
                                        <div class="form-group">                                            <label for="exampleInputEmail1"><?php echo lang('student_email'); ?></label>                                            <input type="text" value="<?php if($action=='edit') echo $student->email; ?>" placeholder="<?php echo lang('student_email'); ?>" name="email" id="email" class="form-control">                                        </div>																				<div class="form-group">                                            <label for="exampleInputEmail1"><?php echo lang('student_phone'); ?></label>                                            <input type="text" value="<?php if($action=='edit') echo $student->phone; ?>" placeholder="<?php echo lang('student_phone'); ?>" name="phone" id="phone" class="form-control">                                        </div>																				<div class="form-group">                                            <label for="exampleInputEmail1"><?php echo lang('student_address'); ?></label>											<textarea name="address" id="address" class="form-control"> <?php if($action=='edit') echo $student->address; ?></textarea>                                                                                   </div>																			
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>               
       <?php 
echo $script;
?>
