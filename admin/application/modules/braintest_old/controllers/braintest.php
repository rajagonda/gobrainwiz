<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : practicetest
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class braintest extends MY_Controller {

     public function __construct(){
            
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('braintest_Model');
        $this->load->language('braintest');
		$this->load->helper('text');
     }
     public function index()	{
        $breadcrumbarray = array('label'=> "Braintest Test",
                            'link' => base_url()."braintest"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Brain Test");
        $data['braintest'] = $this->braintest_Model->get();
        $this->template->load_view1('braintest', $data);
     }	 
	 public function students($id=null)	 
	 {        
	 $breadcrumbarray = array('label'=> "Braintest Test",'link' => base_url()."braintest");
	 $link = breadcrumb($breadcrumbarray);
	 $this->template->set_breadcrumb($link);
	 $braintest = $this->braintest_Model->getTestDetails($id);
	 $title ="Manage ". $braintest->test_name." Test students";
	 $this->template->set_subpagetitle($title); 
	 $data['students'] = $this->braintest_Model->getstudents($id);	
	 $data['testId']=$id;		        
	 $this->template->load_view1('students', $data);     
	 }
	 
	 public function viewquestion($id=null,$set_id=null)	 {
	 $breadcrumbarray = array('label'=> "Question Set",'link' => base_url()."braintest/questionset/".$id); 
	 $link = breadcrumb($breadcrumbarray); 
	 $this->template->set_breadcrumb($link);
	 $braintest = $this->braintest_Model->getTestDetails($id);
	 $title ="Manage ". $braintest->test_name." Test Question";
	 $this->template->set_subpagetitle($title);
	 $data['questions'] = $this->braintest_Model->getQuestion($id,$set_id);
	 $data['testId']=$id;
	 $data['set_id']=$set_id;
	 $this->template->load_view1('questions', $data);
     }
	 
	 
	 public function questionset($id=null)	 
	 {    
	 $breadcrumbarray = array('label'=> "Braintest Test",'link' => base_url()."braintest");
	 $link = breadcrumb($breadcrumbarray); 
	 $this->template->set_breadcrumb($link);
	 $braintest = $this->braintest_Model->getTestDetails($id);	
	 $title ="Manage ". $braintest->test_name." Test Question";
	 $this->template->set_subpagetitle($title);
	 $data['questionset'] = $this->braintest_Model->getQuestionset($id);
	 if($data['questionset']!="")
	 {
		foreach($data['questionset'] as $questionsetKey => $questionset)
		 {
			$qus = $this->braintest_Model->getQuestion($id,$questionset->set_id);
			$data['questionset'][$questionsetKey]->qus_count = count($qus);
		 }
	 }
	 
	 
	 $data['testId']=$id;
	 $this->template->load_view1('questionset', $data); 

	 }

	 public function uploadquetion($id=null)		
	 {	
		 $breadcrumbarray = array('label'=> "Braintest Test",'link' => base_url()."braintest");
		 $braintest = $this->braintest_Model->getTestDetails($id);
		 $data['testId']=$id;	
		 $data['error']='';	
		 $title ="Upload ". $braintest->test_name." Question File";	
		 $this->template->set_subpagetitle($title);
		 $data['braintest'] = $this->braintest_Model->get();
		 $this->template->load_view1('uploadquetion',$data);
	 }

	function import_questions()
	{
		if($_POST)
		{
		
		    $testId				=			$this->input->post('testId');
            $import_test_id		=			$this->input->post('import_questions');
			
			$questionset = $this->braintest_Model->getQuestionset($import_test_id);
								


			foreach($questionset as $set_key => $set)
			{
				$originalsetId=$set->set_id;
				 $SetId=$this->braintest_Model->addquestionSet(array('file_name'=>$set->file_name,																'test_id'=>$testId																));
				$Original_questions = $this->braintest_Model->getQuestion($import_test_id,$originalsetId);

				foreach($Original_questions as $org_question)
				{
						 $qdata['test_id']=$testId;
						 $qdata['cat_id']=$org_question->cat_id;								
						 
						 $qdata['set_id']=$SetId;								
						 $qdata['question_name']=$org_question->question_name;
						 $qdata['option1']=$org_question->option1;	
						 $qdata['option2']=$org_question->option2;								 
						 $qdata['option3']=$org_question->option3;								
						 $qdata['option4']=$org_question->option4;	
						 $qdata['option5']=$org_question->option4;	
						 $qdata['question_answer']=$org_question->question_answer;	
						 $this->braintest_Model->addquestion($qdata);
				}

			}
			
		redirect('braintest/questionset/'.$testId);			

		}
		else
		{
			redirect('braintest');			

		}
	}
	
	
	function do_upload()	
	 {	
	 $config['upload_path'] = './assets_new/uploads/';
	 $config['allowed_types'] = '*';
	 $config['max_size']	= '1024';
	 $data['testId']=$this->input->post('testId');
	 $brainTestId=$data['testId'];
	 $this->load->library('upload', $config);
	 
	 if ( ! $this->upload->do_upload())		
	 {	
		 $data['error']=$this->upload->display_errors();
		 $this->template->load_view1('uploadquetion',$data);
	 }		
	 else
	 {
		 $data = array('upload_data' => $this->upload->data());
		 $fileName= $data['upload_data']['file_name'];	
		 $this->load->library('excel');
		 $inputFileType='Excel2007';
		 $inputFileName = $data['upload_data']['full_path'];
		 $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		 $objReader->setLoadAllSheets();
		 $objPHPExcel = $objReader->load($inputFileName);
		 /*			echo $objPHPExcel->getSheetCount(),' worksheet',(($objPHPExcel->getSheetCount() == 1) ? '' : 's'),' loaded<br /><br />';			*/
		 $SetId=$this->braintest_Model->addquestionSet(array('file_name'=>$fileName,																'test_id'=>$brainTestId																));																			
		 $loadedSheetNames = $objPHPExcel->getSheetNames();
		 foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) 
		 {
			 $cat_id= $this->braintest_Model->getTestCategoryIdByName($loadedSheetName);
			 $sheetData = $objPHPExcel->getSheet($sheetIndex)->toArray(null,true,true,true);
			 foreach($sheetData as $key => $value)					
			 {	
				 if($key>1)		
				 {				
				 
					 if($value['B']!="" && $value['C']!="" && $value['D']!="" && $value['E']!="" && $value['F']!="")							
					 {
						 $qdata['test_id']=$brainTestId;
						 $qdata['cat_id']=$cat_id;								
						 
						 $qdata['set_id']=$SetId;								
						 $qdata['question_name']=$value['B'];
						 $qdata['option1']=$value['C'];								
						 $qdata['option2']=$value['D'];								 
						 $qdata['option3']=$value['E'];								
						 $qdata['option4']=$value['F'];	
						 if($value['G']!="")
							$qdata['option5']=$value['G'];	
						 $qdata['question_answer']=$value['H'];	


						 $this->braintest_Model->addquestion($qdata);							
					 }
				 }				
			 }			
		 }					        
		 
		 redirect('braintest/questionset/'.$brainTestId);			
	   }
	}

	 
     public function save($id=null)
	 {
	 
       $breadcrumbarray = array('label'=> "Braintest Test",'link' => base_url()."braintest");							  
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }
      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('topper_from',{$json_rules});
</script>
JS;

        if($id !='')
		{
			$data['action'] = "edit";
			$data['braintest'] = $this->braintest_Model->getTestDetails($id);
			$this->template->set_subpagetitle("Edit Brain Test");
        }
		else
		{
			$data['action'] = "add";
			$this->template->set_subpagetitle("Add Brain Test");
        }
        $this->form_validation->set_rules($validationRules);
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) 
		{
        if ($this->form_validation->run() == true)  
		{
			$form_values['test_name'] = $this->input->post('test_name');
			// $form_values['no_of_student'] = $this->input->post('no_of_student');
			$form_values['test_date'] = $this->input->post('test_date');
			$form_values['test_time'] = $this->input->post('test_time');
			$form_values['duration'] = $this->input->post('duration');
			$form_values['break_time'] = $this->input->post('break_time');
			// $form_values['student_password'] = $this->input->post('student_password');

			if($id !='')
			{
				$res = $this->braintest_Model->update($form_values,$id);
			}
			else 
			{

				$res = $this->braintest_Model->save($form_values);
			}
			redirect('braintest');
        }         
      }
        $data['script'] = $script;
        $this->template->load_view1('braintest_setup',$data);
    }  	
	
	public function studentsave($brainTestId,$id=null)
	{
	
       $breadcrumbarray = array('label'=> "Braintest Test students",'link' => base_url()."braintest/students/".$brainTestId);							  
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $validationRules = $this->_rules2();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }
      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('topper_from',{$json_rules});
</script>
JS;

        if($id !='')
		{
			$data['action'] = "edit";
			$data['student'] = $this->braintest_Model->getstudent($id);
			$this->template->set_subpagetitle("Edit student");
        }
		else 
		{
			$data['action'] = "add";
			$this->template->set_subpagetitle("Add student");
        }
        $this->form_validation->set_rules($validationRules);
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) 
		{
			if ($this->form_validation->run() == true)  
			{
				$form_values['name'] = $this->input->post('name');
				$form_values['email'] = $this->input->post('email');
				$form_values['phone'] = $this->input->post('phone'); 
				$form_values['address'] = $this->input->post('address');
				

				if($id !='')
				{
					$res = $this->braintest_Model->updatestudent($form_values,$id);
				}
				else 
				{
				
					$res = $this->braintest_Model->savestudent($form_values,$brainTestId);
				}
				redirect('braintest/students/'.$brainTestId);
			}         
        }
        $data['script'] = $script;
        $this->template->load_view1('student_setup',$data);
    }
    public function _rules() 
	{
        $rules = array(
                array('field' => 'test_name','label' => lang('com_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),	
				array('field' => 'duration','label' => lang('duration'),'rules' => 'trim|required|xss_clean|numeric'),
				array('field' => 'break_time','label' => lang('break_time'),'rules' => 'trim|required|xss_clean|numeric'),
				array('field' => 'test_time','label' => lang('test_date'),'rules' =>'trim|required|xss_clean|max_length[250]'),
				array('field' => 'test_time','label' => lang('test_time'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
        return $rules;
    }	
	
	public function _rules2() 
	{ 
		$rules = array(
					array('field' => 'name','label' => lang('student_name'),'rules' => 'trim|xss_clean|max_length[250]'),				array('field' => 'email','label' => lang('student_email'),'rules' => 'trim|xss_clean|valid_email'),
					array('field' => 'phone','label' => lang('student_phone'),'rules' => 'trim|xss_clean|min_length[10]|integer')                 
					);
		return $rules;
	}
					
	public function changeTestStatus()	
	{
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
        $formvalues['test_status'] = 'n';
        echo "0";
        } else {
        $formvalues['test_status'] = 'y';
        echo "1";
        }
        $this->braintest_Model->changeTestStatus($formvalues, $id);
    }
	public function changeStudentStatus()	
	{    
		$id = $_POST['id']; 
		$staus = $_POST['status'];
		if($staus == 'y')
		{  
		$formvalues['status'] = 'n'; 
		echo "0"; 
		}
		else {
		$formvalues['status'] = 'y'; 
		echo "1";
		} 
		$this->braintest_Model->changeStudentStatus($formvalues, $id);
    }
	public function changequestionsetStatus()	
	{
		$id = $_POST['id']; 
		$staus = $_POST['status'];
		if($staus == 'y')
		{
		$formvalues['status'] = 'n';
		echo "0"; 
		} 
		else { 
		$formvalues['status'] = 'y';
		echo "1";   
		} 
		$this->braintest_Model->changequestionsetStatus($formvalues, $id);
    }	
    public function delete()
	{
        $id =  $this->input->post('id');
        $this->braintest_Model->deleteTest($id);
    } 
    public function deletestudent()
	{
	$id =  $this->input->post('id'); 
	$this->braintest_Model->deleteStudent($id); 
	}	
 
 public function questionsetdelete()
 { 
	 $id =  $this->input->post('id');
	 $this->braintest_Model->deleteQuestionSet($id);
 }
 
 public function exportstudents($id=null)
 {
	 $braintest = $this->braintest_Model->getTestDetails($id);
	 $title ="Manage ". $braintest->test_name." Test students";
	 $students = $this->braintest_Model->getstudents($id);
	 $heading=array('No','student Id','Password');
	 $this->load->library('excel');
	 $objPHPExcel = new PHPExcel();
	 $objPHPExcel->getActiveSheet()->setTitle($braintest->test_name);
	 $rowNumberH = 1;
	 $colH = 'A';
	 foreach($heading as $h)
	 {
	 $objPHPExcel->getActiveSheet()->setCellValue($colH.$rowNumberH,$h);
	 $colH++;       
	 }
	 $totn=count($students);
	 $maxrow=$totn+1;   
	 $row = 2;   
	 $no = 1;  
	 foreach($students as $student)
	 {		
		 $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$no); 
		 $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$student->studentId);
		 $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$student->password);	
		 $row++; 
		 $no++;   
	 }   
	 
	 $objPHPExcel->getActiveSheet()->freezePane('A2'); 
	 $styleArray = array( 'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )));    $objPHPExcel->getActiveSheet()->getStyle('A1:C'.$maxrow)->applyFromArray($styleArray);
	 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	 header('Content-Type: application/vnd.ms-excel');
	 header('Content-Disposition: attachment;filename="BWTest-'.$braintest->test_name.'.xls"');
	 header('Cache-Control: max-age=0');
	 $objWriter->save('php://output');
	 redirect('braintest/students/'.$id);
 }
 
 
 public function viewresult($test_id,$studentId=null)
	{
	
       $breadcrumbarray = array('label'=> "Braintest Test students",'link' => base_url()."braintest/students/".$test_id);							  
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);

		$data['tresult'] = $this->braintest_Model->get_student_test_result($studentId,$test_id);
        $data['testdetails'] = $this->braintest_Model->getTestDetails($test_id);
        
		$data['student'] = $this->braintest_Model->getstudent($studentId);
			 
        
        $this->template->load_view1('testresult',$data);
    }
	
  public function studentresult($id=null)
 {
	 $braintest = $this->braintest_Model->getTestDetails($id);
	 $title ="Manage ". $braintest->test_name." Test students";
	 $students = $this->braintest_Model->getstudents($id,3);

	 if(count($students)>0 && isset($students[0]->studentId))
	 {
			$allCategory= $this->braintest_Model->get_student_test_result($students[0]->studentId);
			 
			 $heading=array('No','User ID','Name','Phone','Email');
			 $totalMark=0;
			 foreach ($allCategory as $cat)
			 {
				$heading[]=$cat->category_name.' ('.$cat->total_q.')';
				$totalMark+=$cat->total_q;
			 }
			 $heading[]='Total('.$totalMark.')';
			 $heading[]='Percentage(%)';
			 
			 $this->load->library('excel');
			 $objPHPExcel = new PHPExcel();
			 $objPHPExcel->getActiveSheet()->setTitle($braintest->test_name);
			 $rowNumberH = 1;
			 $colH = 'A';
			 foreach($heading as $h)
			 {
				 $objPHPExcel->getActiveSheet()->setCellValue($colH.$rowNumberH,$h);
				 $colH++;       
			 }
			 
			 $totn=count($students);
			 $maxrow=$totn+1;   
			 $row = 2;   
			 $no = 1;
			 
					 //print_r($students);

			 $x = '';

			 foreach($students as $student)
			 {		
				 $result= $this->braintest_Model->get_student_test_result($student->studentId);
				
				 
				 $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$no); 
				 $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$student->studentId);
				 $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$student->name);	
				 $objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$student->phone);	
				 $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,$student->email);	

				 $x = 'E';
				 $per="";
				 $total = 0;
				foreach($result as $keys => $testresult)
				{
					$per += ($testresult->correct_q/$testresult->total_q)*100;
					$total+=$testresult->correct_q;
					
					$x++;
					
					$objPHPExcel->getActiveSheet()->setCellValue($x.$row,$testresult->correct_q);	

				}
				$per=round($per/count($result),2);
				
				$x++;
 				$objPHPExcel->getActiveSheet()->setCellValue($x.$row,$total);

				$x++;				
				$objPHPExcel->getActiveSheet()->setCellValue($x.$row,$per);	

				 
				 $row++; 
				 $no++;   
			 }   
			 
			 $objPHPExcel->getActiveSheet()->freezePane('A2'); 
			 $styleArray = array( 'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )));    $objPHPExcel->getActiveSheet()->getStyle("A1:$x".$maxrow)->applyFromArray($styleArray);
			 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
			 header('Content-Type: application/vnd.ms-excel');
			 header('Content-Disposition: attachment;filename="BWTest-'.$braintest->test_name.'-'.$braintest->test_date.'.xls"');
			 header('Cache-Control: max-age=0');
			 $objWriter->save('php://output');
			 redirect('braintest/students/'.$id);
	 }
	 else
	 {
			 redirect('braintest/students/'.$id);

	 }
	 
 }
       
}
