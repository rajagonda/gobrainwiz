<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_members');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                         <th style="text-align:center !important;" ><?php echo lang('member_name');?></th>
                                        <th style="text-align:center !important;"><?php echo lang('member_email');?></th>
                                       
                                        <th style="text-align:center !important;" ><?php echo lang('member_phone');?></th>
                                        
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
<!--                                        <a class="blue" href="<?php echo base_url();?>placement/pdfs/save">
                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>-->
                                        </th>
                                        </tr>
                                      <?php
                                      if($members !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($members as  $value) {
                                      ?>
                                        <tr id='member_<?php echo $value->member_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->member_name;?></td>
                                        <td align="center"><?php echo $value->member_email;?></td>
                                        <td align="center"><?php echo $value->member_phone;?></td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->member_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->member_id;?>', '<?php echo $value->member_status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->member_id;?>', '<?php echo $value->member_status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
<!--                                        <a class="green" href="<?php echo base_url();?>placement/pdfs/save/<?php echo $value->id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        |-->
                                        <a class="red trash" id="<?php echo $value->member_id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

    
function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>placement/pdfs/delete',
          success: function(msg) {
            $("#member_"+del_id).hide();
          }
        });
       
        }
        return false;
      });

    
 function changeStatus(id1, member_id,  status) { 
     var p_url= "<?php echo base_url(); ?>members/changememberStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'member_id='+member_id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+member_id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+member_id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>