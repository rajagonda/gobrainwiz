<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : members
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class members_model extends CI_Model {

	public $variable;

	public function __construct() {
	    parent::__construct();
		
	}
	public function get(){
            
         $query = $this->db->get(DB_PREFIX.'members');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}
        }
        
	public function changememberStatus($data, $id){
	    $this->db->where('member_id', $id);
	    $this->db->update(DB_PREFIX.'members', $data);
            return $this->db->affected_rows();
	}
	public function deletemember($id){
		$this->db->where('member_id', $id);
        $this->db->delete(DB_PREFIX.'members');
	}
        


}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */