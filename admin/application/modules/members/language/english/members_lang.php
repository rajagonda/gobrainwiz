<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : members
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| members
|--------------------------------------------------------------------------
*/


$lang['manage_members'] = 'Manage Members';
$lang['member_name'] = "Member  Name";
$lang['member_email'] = "Member Email";
$lang['member_phone'] = "Member Phone";







/* End of file members_lang.php */
/* Location: ./application/module_core/members/language/english/members_lang.php */
