<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : members
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class members extends MY_Controller {

     public function __construct() {
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('members_model'));
        $this->load->language('members');
     }
     public function index()	{
        $breadcrumbarray = array('label'=> "Manage Members",
                            'link' => base_url()."members/"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Members");
        $data['members'] = $this->members_model->get();
        $this->template->load_view1('members', $data);
     }
     public function changememberStatus(){
        $id = $_POST['member_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['member_status'] = 'n';
            echo "0";
        } else {
            $formvalues['member_status'] = 'y';
            echo "1";
        }
       $this->members_model->changememberStatus($formvalues, $id);
     }
     public function delete(){
        $id =  $this->input->post('id');
        $this->members_model->deletemember($id);

     }
    

}
