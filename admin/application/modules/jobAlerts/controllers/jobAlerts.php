<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Subject
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class jobAlerts extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('Job_Model');
        //$this->load->language('subjects');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "JobAlerts",
                            'link' => base_url()."jobAlerts"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Job Alerts");
           $data['jobs'] = $this->Job_Model->get();
           $this->template->load_view1('jobs', $data);
	}

public function _testrules(){

         $rules = array(

                 array('field' => 'title','label' => 'title','rules' => 'trim|required|xss_clean|max_length[250]')
                 );

        return $rules;

    }

  public function jobsave($id=null){        

        $breadcrumbarray = array('label'=> "JobAlerts",

                           'link' => base_url()."jobAlerts"

                           );

        $link = breadcrumb($breadcrumbarray);

        $this->template->set_breadcrumb($link);

        

       // print_r($data);exit;

        $validationRules = $this->_testrules();

        foreach ($validationRules as $form_field)   {

        $rules[] = array(

        'name' => $form_field['field'],

        'display' => $form_field['label'],

        'rules' => $form_field['rules'],

        );

        }



      

$json_rules = json_encode($rules);

$script = <<< JS

<script>

var CIS = CIS || { Script: { queue: [] } };

CIS.Form.validation('job_form',{$json_rules});

</script>

JS;



        if($id !=''){

            $data['action'] = "edit";

            $data['job'] = $this->Job_Model->getjobDetails($id);

            // $data['topics'] = $this->practicetest_Model->getcattopics($data['test']->subcat_id);

            // $this->template->set_subpagetitle("Edit Passage");

        }else {

            $data['action'] = "add";

           // $this->template->set_subpagetitle("Add Passage");

        }

        $this->form_validation->set_rules($validationRules);

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

        if ($this->form_validation->run() == true)  {



        $form_values['drive_date'] =  date('Y-m-d',  strtotime($this->input->post('drive_date')));
        $form_values['last_date'] =  date('Y-m-d',  strtotime($this->input->post('last_date')));

        $form_values['title'] =  $this->input->post('title');

        $form_values['message'] = $this->input->post('message');

        $form_values['link'] = $this->input->post('link');



       // echo "<pre>"; print_r($form_values);exit;
        $filename = '';
        if($_FILES['image']['name'] !='') {
            
            $filename = time()."_".$_FILES['image']['name'];
            $config['upload_path'] = '../upload/notification/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
             $form_values['image'] = $update_image;
           }

        

        

        if($id !=''){

        $res = $this->Job_Model->update($form_values,$id);
        $testId = $id;

        }else {

        $res = $this->Job_Model->save($form_values);
       

        }
//echo $this->input->post('topic_id');exit;
          

      

                    

        redirect('jobAlerts');

        }         

        }

        $data['script'] = $script;

        $this->template->load_view1('job_setup',$data);

    }

    public function changeTestStatus(){

        $id = $_POST['id'];

        $staus = $_POST['status'];

        if($staus == 'y') {

        $formvalues['job_status'] = 'n';

        echo "0";

        } else {

        $formvalues['job_status'] = 'y';

        echo "1";

        }
if($formvalues['job_status'] == 'y')
        {

        $jobDetails = $this->Job_Model->getjobDetails($id);


        $cuur_time = date('Y-m-d H:i:s');

        $notdata['title'] = "Job Alert Uploaded";
        $notdata['message'] = "Job Alert Released";
        $notdata['notify_type'] = 4;
       // $notdata['catid'] = '';                 
      //  $notdata['topicid'] = $testDetails->topic_id;
      //  $notdata['subcatid'] = $id;
        $notdata['time_update'] = $cuur_time;

        $nid = $this->common_model->insertSingle('gk_notifications',$notdata);



        $stokens = array();
        $studnetdata['students']= $this->Job_Model->getNotStudents();
        foreach ($studnetdata['students'] as  $value) {

        $notlog['student_id'] = $value->examuser_id;
        $notlog['notification_id'] =$nid;
        $notlog['checked'] = '0';

        $nlid = $this->common_model->insertSingle('gk_notifications_log',$notlog);

        array_push($stokens, $value->notification_token);
        }

       // $testRes = $this->practicetest_Model->getTopicAndCatName($testDetails->topic_id);

        $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
        $field1 -> message = $jobDetails->message;
        $field1 -> title = $jobDetails->title;
        $field1 -> image = '';
        if(!empty($jobDetails->image))
        {
             $field1 -> image = 'http://gobrainwiz.in/upload/notification/'.$jobDetails->image;
        }
        $field1 -> type =  '4';
        $field1 -> drive_date = $jobDetails->drive_date; 
        $field1 -> last_date = $jobDetails->last_date;
        $field1 -> link = $jobDetails->link;
         //var_dump($field1);die();
         
        $fields = array(
             'registration_ids' => $stokens,
             'data' => $field1
            );
    $headers = array(
     // 'Authorization:key = AIzaSyAFJlc5XeyL6vYCsXzTKP72DudKlKm64FU',
    'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',     
 'Content-Type: application/json'
      );
     $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
}
        $this->Job_Model->changeTestStatus($formvalues, $id);

    }

public function deletejob(){

        $id =  $this->input->post('id');

        $this->Job_Model->deletejob($id);

    } 









	/*public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Subjects",
                           'link' => base_url()."subjects"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('subject_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['subject'] = $this->Subjects_Model->getsubjectDetails($id);
            $this->template->set_subpagetitle("Edit Subject");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Subject");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['subject_name'] = $this->input->post('subject_name');
            $form_values['subject_desc'] = $this->input->post('subject_desc');    

            if($id !=''){
             $res = $this->Subjects_Model->update($form_values,$id);
            }else {
              $res = $this->Subjects_Model->save($form_values);

            }
             redirect('subjects');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('subjects_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'subject_name','label' => lang('subject_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'subject_desc','label' => lang('subject_desc'),'rules' => 'required'));
        return $rules;
    }

	 public function changesubjectStatus(){
        $id = $_POST['subject_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['subject_status'] = 'n';
            echo "0";
        } else {
            $formvalues['subject_status'] = 'y';
            echo "1";
        }
        $this->Subjects_Model->changesubjectStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Subjects_Model->deleterole($id);

    }*/

}

