<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Job Alert</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="job_form" id="job_form" enctype="multipart/form-data">
                                    <div class="box-body">
                                         
                                        
                                         <div class="form-group">
                                            <label>Name :</label>
                                            <input type="text" placeholder="Name" value="<?php if($action == 'edit') echo $job->title; ?>" name="title" id="title" class="form-control">
                                        </div>
                                        

                                        <div class="form-group">
                                       <label>Description :</label>
                                        <textarea placeholder="Description"  cols="" name="message"class="form-control"><?php if($action=='edit') echo $job->message; ?></textarea>

                                      
                                        </div>

                                        <div class="form-group">

                                        <label>Drive Date:</label>

                                        <div class="input-group">

                                            <div class="input-group-addon">

                                                <i class="fa fa-calendar"></i>

                                            </div>

                                            <input type="text" id='datepicker' name='drive_date' value="<?php if($action=='edit') echo date('m-d-Y',  strtotime($job->drive_date)); ?>" class="form-control" readonly=""  />

                                        </div><!-- /.input group -->

                                    </div>

                                    <div class="form-group">

                                        <label>Last Date:</label>

                                        <div class="input-group">

                                            <div class="input-group-addon">

                                                <i class="fa fa-calendar"></i>

                                            </div>

                                            <input type="text" id='datepicker1' name='last_date' value="<?php if($action=='edit') echo date('m-d-Y',  strtotime($job->last_date)); ?>" class="form-control" readonly=""  />

                                        </div><!-- /.input group -->

                                    </div>
                                    <div class="form-group">
                                            <label>Link :</label>
                                            <input type="text" placeholder="Link" value="<?php if($action == 'edit') echo $job->link; ?>" name="link" id="link" class="form-control">
                                        </div>
                                        <div class="form-group">
                                       <label>Image :</label>
                                        <input type="file" name="image" id="image">
                                      

                                      
                                        </div>
                                        
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>

<script>

$(function() {

$( "#datepicker" ).datepicker();
$( "#datepicker1" ).datepicker();

});

</script>
