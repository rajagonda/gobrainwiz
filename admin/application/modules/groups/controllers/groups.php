<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Groups
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Groups extends MY_Controller {

	public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model('Groups_model');
            $this->load->language('groups');
       }
       public function index() {
	    $breadcrumbarray = array('label'=> "Colleges",
                               'link' => base_url()."groups"
                               );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
            $this->template->set_subpagetitle("Manage Colleges");
            $data['groups'] = $this->Groups_model->get();
            $this->template->load_view1('groups', $data);
	}

	public function save($id=null){
            $breadcrumbarray = array('label'=> "Colleges",
                           'link' => base_url()."groups"
                           );
            $link = breadcrumb($breadcrumbarray);
            $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('group_from',{$json_rules});
</script>
JS;

           if($id !=''){
            $data['action'] = "edit";
            $data['group'] = $this->Groups_model->getgroupDetails($id);
//            echo "<pre>"; print_r($data);exit;
            $this->template->set_subpagetitle("Edit College");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add College");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $group_values['group_name'] = $this->input->post('group_name');
            $group_values['group_description'] = $this->input->post('group_description');    

            if($id !=''){
             $res = $this->Groups_model->update($group_values,$id);
            }else {
              $res = $this->Groups_model->save($group_values);

            }
             redirect('groups');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('group_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'group_name','label' => lang('group_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'group_description','label' => lang('group_description'),'rules' => 'required'));
        return $rules;
    }

	 public function changegroupStatus(){
        $id = $_POST['group_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['group_status'] = 'n';
            echo "0";
        } else {
            $formvalues['group_status'] = 'y';
            echo "1";
        }
        $this->Groups_model->changegroupStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Groups_model->deletegroup($id);
    }
    public function mailForm($id){

       

        $breadcrumbarray = array('label'=> "Colleges",
                           'link' => base_url()."groups"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);



        $validationRules = $this->_rules1();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('mail_from',{$json_rules});
</script>
JS;
        
          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
         
          
          if ($this->form_validation->run() == true)  {

            $data['students'] = $this->Groups_model->getCollegeStudents($id);

            if($data['students']){

              $mailbody= $this->input->post('mailbody');
              foreach ($data['students'] as  $value) {

              $data['body'] = $mailbody;
              $data['name'] = $value->name;

              //$this->load->view('emailtemplate',$data,true);

              $message = $this->load->view('emailtemplate',$data,true);
              $subject = "Brainwiz";
              $emails = $value->email;


              $headers = "MIME-Version: 1.0" . "\r\n";
              $headers .= "Content-Type: text/html; charset=UTF-8\r\n";                
              $headers.='From:  Brainwiz <gobrainwiz@gmail.com>'."\r\n".'X-Mailer: PHP/' . phpversion();                 
              
              if(mail($emails,$subject, $message,$headers)){
                
              }else {
                
              }    
                //exit;
              }
              $this->session->set_userdata('sucess','Successfully Sent');
              redirect('groups');
           

            }else {
              $this->session->set_userdata('sucess','Please Add Emails To This College');
              redirect('groups');
            }
          
           


          }

          
        }else {
          $data['script'] = $script;
          $this->template->load_view1('mail_setup',$data);
        }


      
    }
     public function _rules1() {
        $rules = array(
                array('field' => 'mailbody','label' => "Mail Body",'rules' => 'trim|required|xss_clean|max_length[250]')
                );
        return $rules;
    }


}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */