<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Groups
|--------------------------------------------------------------------------
*/


$lang['manage_groups'] = 'Manage Colleges';
$lang['group_add'] = 'Add College';
$lang['group_edit'] = "Edit College";
$lang['group_name'] = "College Name";
$lang['group_description'] = "Description";
$lang['send_mail'] = "Send Mail";


/* End of file group_lang.php */
/* Location: ./application/module_core/groups/language/english/group_lang.php */
