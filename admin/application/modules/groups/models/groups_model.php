<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class groups_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){

        $sql = "SELECT * FROM gk_groups ORDER BY group_id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'groups',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('group_id', $id);
	    $this->db->update(DB_PREFIX.'groups', $data);
	    return $this->db->affected_rows();
	}
	public function getgroupDetails($id){
		$this->db->where('group_id', $id);
		$query = $this->db->get(DB_PREFIX.'groups');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changegroupStatus($data, $id){
		$this->db->where('group_id', $id);
	    $this->db->update(DB_PREFIX.'groups', $data);
	    return $this->db->affected_rows();
	}
	public function deletegroup($id){
		$this->db->where('group_id', $id);
        $this->db->delete(DB_PREFIX.'groups');
	}
	public function getCollegeStudents($id){

		$sql = "SELECT * FROM gk_emails WHERE group_id='$id'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
           return $query->result();
		}else {
           return false;
		}


	}


}

/* End of file groups_model.php */
/* Location: ./application/models/groups_model.php */