<div class="row">
            <div class="col-md-10 col-md-offset-3">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Mail Form</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="mail_from" id="mail_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                      
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Mail Body</label>
                                            <textarea id="mailbody" name="mailbody" rows="10" cols="80"> </textarea>
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
 <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
  <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('mailbody');
              
            });
        </script>