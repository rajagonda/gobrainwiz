<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_groups');?></h3>
                      <p class="text-red">
                           <?php

                           if($this->session->userdata('sucess') !='') {
                            echo $this->session->userdata('sucess');
                             $this->session->set_userdata('sucess','');
                           }

                           ?>
                           </p>
                            
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th style="width: 100px" style="text-align:center !important;"><?php echo lang('group_name');?></th>
                                        <th ><?php echo lang('group_description');?></th>
                                        <th ><?php echo lang('send_mail');?></th>                                        
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        <a class="blue" href="<?php echo base_url();?>groups/save">
                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        </th>
                                        </tr>
                                      <?php
                                      if($groups !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($groups as  $value) {
                                      ?>
                                        <tr id='role_<?php echo $value->group_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->group_name;?></td>
                                        <td><?php echo $value->group_description;?></td>

                                        <td><a href="<?php echo base_url(); ?>groups/mailForm/<?php echo $value->group_id;?>"><button type="button" class="btn btn-block btn-success btn-xs">Send Mail</button></a></td>

                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->group_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->group_id;?>', '<?php echo $value->group_status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->group_id;?>', '<?php echo $value->group_status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url();?>groups/save/<?php echo $value->group_id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        |
                                        <a class="red trash" id="<?php echo $value->group_id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>groups/delete',
          success: function(msg) {
            $("#role_"+del_id).hide();
          }
        });
       
        }
        return false;
      });


    /**
     * By using this method change role status
     * 
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */
 function changeStatus(id, group_id,  status) { 
     var p_url= "<?php echo base_url(); ?>groups/changegroupStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'group_id='+group_id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id).html('<a class="green" href="#" onclick="changeStatus(\''+id+'\', \''+group_id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id).html('<a class="red" href="#" onclick="changeStatus(\''+id+'\', \''+group_id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>