<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse !important;}
    </style>
    <![endif]-->
    <style>
        @media only screen and (max-width: 615px) {

            .new-color-head {
                color: #4E4E4E !important;
            }

            .new-color-subHead {
                color: #696969 !important;
            }

			.innerHeading{
				padding-bottom: 20px !!important;
				padding-left: 15px !important;
				padding-right: 15px !important;
			}
      .contentsBody{
        padding-top: 20px !important;
      }
			.contentsBody,
			.left .main{
				padding-left: 20px !important;
				padding-right: 20px !important;
			}

			.border-margin{
				margin-left: 20px !important;
				margin-right: 20px !important;
			}

			.contents-footer{
				padding-left: 20px !important;
				padding-right: 20px !important;
			}

			.footer{
				padding-bottom: 5px !important;
				padding-left: 20px !important;
				padding-right: 20px !important;
			}

			.contents-footer .h1{
				font-size: 27px !important;
			}

			.mobile_max{
				max-width: 360px !important;
			}
        }
    </style>
</head>
<body style="margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#ffffff;" >
<center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;" >
    <div class="webkit" style="max-width:600px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;" >
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" style="border-spacing:0;font-family:'Arial Regular';color:#333333;" >
            <tr>
                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
        <![endif]-->
        <table class="outer" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;" >
            <tr>
                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#C0C0C0">
                    <table class="outer" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;">
                        <tr>
                            <td style="padding-top: 12px;padding-bottom: 12px;padding-right: 12px;padding-left: 12px;">
                                <a href="http://www.gobrainwiz.in/" target="_blank" style="color: #fff; font-size: 30px;text-decoration: none;"><img src="http://gobrainwiz.in/assets/images/logomini.png" width="100%" height="90px"></a><br><span style="font-size: 16px; color: #eeaac1;padding-top: 5px;display: block;"></span></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                    <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                        <tbody>
							<tr>
                            <td  width="600" bgcolor="#ECEFF8" style="background-repeat: no-repeat; padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                <!--[if gte mso 9]>
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" id="theImage"  src="https://scontent-sea1-1.xx.fbcdn.net/t39.2365-6/13717843_641491789334787_1120254879_n.png" style="behavior:url(#default#VML);display:inline-block;height:309px;width:600px;border-width:0; background-repeat-y:no-repeat;" bgcolor="#ECEFF8" />
                                <v:shape xmlns:v="urn:schemas-microsoft-com:vml" id="theText" fill="true" style="behavior:url(#default#VML);display:inline-block;position:absolute;height:1800px;width:620px;top:-5;left:-10;border-width:0; background-color: #ECEFF8;" >
                                <div>
                                <![endif]-->
                                <!-- This is where you nest a table with the content that will float over the image -->
                                <!--[if (gte mso 9)|(IE)]>
                                <table width="600" align="center" style="border-spacing:0;font-family:'Arial Regular';color:#333333;" >
                                    <tr>
                                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                <![endif]-->
                                <table class="outer" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;">
                                    <tbody><tr>
                                        <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;">
                                            <!--[if (gte mso 9)|(IE)]>
                                            <table width="100%" style="border-spacing:0;font-family:'Arial Regular';color:#333333;" >
                                                <tr>
                                                    <td width="50%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                            <![endif]-->
                                            <div class="column" style="width:100%;max-width:350px;display:inline-block;vertical-align:top;">
                                                <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                    <tbody><tr>
                                                        <td style="padding:10px;"><img src="http://gobrainwiz.in/b_assets/business-mail/profile.png" alt="" style="margin:0 !important;border-width:0;width:100%;"></td>
                                                        <!-- <td class="inner" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;">
                                                            <table class="contents" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;width:100%;font-size:14px;text-align:left;">
                                                                <tbody><tr>
                                                                    <td class="text" style="padding-bottom:0;padding-right:0;padding-left:0;">
                                                                    <img src="profile.png" alt="" style="margin:0 !important;border-width:0;width:100%;">
                                                                    </td>
                                                                </tr>
                                                                
                                                                </tbody></table>
                                                        </td> -->
                                                    </tr>
                                                    </tbody>
												</table>
                                            </div>
                                            <!--[if (gte mso 9)|(IE)]>
                                            </td><td width="50%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                            <![endif]-->
                                            <div class="column mobile_max" style="width:100%;max-width:250px;display:inline-block;vertical-align:top;">
                                                <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                    <tbody>
													<tr>
                                                        <td class="innerHeading" style="padding-top: 18px;padding-bottom:10px;padding-right: 20px;padding-left:0px;">
                                                            <table class="contents" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;width:100%;font-size:14px;text-align:left;">
                                                                <tbody>
																<tr>
                                                                    <td class="text" style="padding-bottom:0;padding-right:0;padding-left:0;padding-top:10px; text-align: center;">
                                                                        <p class="new-color-head" style="margin:0; color:#8d001f;font-weight:bold;font-size:28px;margin-bottom:3px;font-family: Arial, Helvetica, sans-serif;">Learn Aptitude <br/>to crack <br>Interviews.<br></p>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                        <div align="center" class="button_table" style="padding-bottom:1px;padding-top:0px;">
                                                                            <a href="https://www.facebook.com/business/a/facebook-video-ads-creative-ideas" style="color:#ffffff;display:inline-block;border-width:0;text-decoration:underline;">
                                                                                <table cellspacing="0" cellpadding="15" bgcolor="#4380FC" style="border-width:1px;border-style:solid;border-color:#4380FC;background-color:#4380FC;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;width:auto;border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                    <tbody><tr>
                                                                                        <td bgcolor="#4380FC" style="color:#ffffff;display:block;background-color:#4380FC;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top:12.5px;padding-bottom:12.5px;padding-right:20px;padding-left:20px;font-family: Arial, Helvetica, sans-serif;">
                                                                                            <div align="center">
                                                                                                <a href="http://gobrainwiz.in/videos/homevideos" target="_blank" style="font-weight:bold;color:#ffffff;font-size: 23px;line-height:1.2em;font-family: Arial, Helvetica, sans-serif;text-decoration:none;width:100%;display:inline-block;border-width:0;letter-spacing: 0.6px">Watch Videos</a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody></table>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
															</table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>
                                            <!--[if (gte mso 9)|(IE)]>
                                            </td>
                                            </tr>
                                            </table>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                    </tbody>
								</table>
                                <!--[if (gte mso 9)|(IE)]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
									 <!-- This ends the nested table content -->
                                <!--[if gte mso 9]>
                                </div>
                                </v:shape>
                                <![endif]-->
                            </td>
                        </tr>
                        </tbody>
					</table>
					<!--[if (gte mso 9)|(IE)]>
					<table width="600" align="center" style=" border-collapse:collapse; border-spacing:0;font-family:'';color:#333333;" >
					<tr>
					<td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#ECEFF8" >
					<![endif]-->
					<table style="max-width:600px; border-collapse:collapse;" width="100%">
						<tr>
							<td bgcolor="#ECEFF8">
                                <!--[if (gte mso 9)|(IE)]>
                                <table width="550" align="center" style="border-spacing:0;font-family:'Arial Regular';color:#333333;" >
                                    <tr>
                                        <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                <![endif]-->
                                <table class="outer" align="center" style="border-spacing:0;font-family:'Arial Regular';color:#333333;Margin:0 auto;width:95%;max-width:560px;">
                                    <tbody><tr>
                                        <td class="one-column" bgcolor="#FFFFFF" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                            <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                <tbody>
                                                <tr><a href="#"><img src="http://gobrainwiz.in/b_assets/business-mail/strip.png"></a></tr>
												<tr>
													<td class="inner contentsBody" style="padding-top: 24px;padding-bottom: 0px;padding-right: 44px;padding-left: 44px;width:100%;text-align:left;">
														<p style="Margin:0;font-size: 16px;Margin-bottom: 0px;color:#8C8C8C;font-family: Arial, Helvetica, sans-serif">Hi, <?php echo $name;?>,
															<br/><br/>
														</p>
														<p style="margin:0;font-size: 16px;color:#8C8C8C;font-family: Arial, Helvetica, sans-serif">

                                                        <?php echo $body; ?>
														</p>

													</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="left-sidebar" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;font-size:0;">
                                                        <div class="column left" style="width:100%;display:inline-block;vertical-align:middle;max-width:435px;">
															<!--[if (gte mso 9)|(IE)]>
															<table width="435" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
															<![endif]-->
                                                            <table class="main" align="center" width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
                                                                <tbody><tr>
                                                                    <td class="inner" style="padding-top: 25px;padding-bottom: 20px;padding-right: 0;vertical-align:top;">
                                                                        <img src="http://gobrainwiz.in/b_assets/business-mail/1.jpg" alt="" style="margin:0 !important;border-width:0;width:100px">
                                                                    </td>
                                                                    <td style="padding-top: 24px;">
                                                                        <div class="column right" style="width:100%;display:inline-block;vertical-align:middle;max-width:400px;">
																			<!--[if (gte mso 9)|(IE)]>
																			<table width="350" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
																			<![endif]-->
                                                                            <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                <tbody><tr>
                                                                                    <td class="inner contents" style="padding-top: 3px;padding-bottom:20px;padding-left: 13px;width:100%;font-size:14px;text-align:left;">
                                                                                        <span class="heading" style="font-weight:bold;color:#4266B0; font-size: 20px;line-height: 1.2;">Arithmetic & Logical Reasoning</span><br/><br>
                                                                                        <span class="sub-heading" style="color:#333; text-algn:left; font-size: 16px; line-height:1.6;">34+ Aptitude Topics will be covered with short-cuts helping you to manage time in live competitive
exams.</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
																			                            </table>
																			<!--[if (gte mso 9)|(IE)]>
																			</table>
																			<![endif]-->
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
															</table>
															<!--[if (gte mso 9)|(IE)]>
															</table>
															<![endif]-->

                                                        </div>
                                                        <div class="border-margin" style="border-bottom-width:1px;border-bottom-style:dashed;border-bottom-color:#CCCCCC;margin-right: 53px;margin-left: 55px;"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="left-sidebar" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;font-size:0;">
                                                        <div class="column left" style="width:100%;display:inline-block;vertical-align:middle;max-width:435px;">
															<!--[if (gte mso 9)|(IE)]>
															<table width="435" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
															<![endif]-->
                                                            <table class="main" width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
                                                                <tbody><tr>
                                                                    <td class="inner" style="padding-top: 20px;padding-bottom: 20px;padding-right:0;vertical-align:top;">
                                                                        <img src="http://gobrainwiz.in/b_assets/business-mail/2.jpg" alt="" style="margin:0 !important;border-width:0;width:100px">
                                                                    </td>
                                                                    <td>
                                                                        <div class="column right" style="width:100%;display:inline-block;vertical-align:middle;max-width:400px;">
																			<!--[if (gte mso 9)|(IE)]>
																			<table width="350" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
																			<![endif]-->
                                                                            <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                <tbody><tr>
                                                                                    <td class="inner contents" style="padding-top: 20px;padding-bottom:20px;padding-right: 0px;padding-left: 13px;width:100%;font-size:14px;text-align:left;">
                                                                                        <span class="heading" style="font-weight:bold;color:#4266B0; font-size: 20px;text-align:left;line-height: 1;">Verbal Ability</span><br/><br>
                                                                                        <span class="sub-heading" style="color:#333; text-algn:left; font-size: 16px; line-height:1.6;">Almost every competitive exam has Verbal Section. Our Verbal classes cover basic grammar to high level Vocabulary, Sentence Completion + Passages + Jumbled sentences.</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
																			</table>
																			<!--[if (gte mso 9)|(IE)]>
																			</table>
																			<![endif]-->
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
															</table>
															<!--[if (gte mso 9)|(IE)]>
															</table>
															<![endif]-->
                                                        </div>
                                                        <div class="border-margin" style="border-bottom-width:1px;border-bottom-style:dashed;border-bottom-color:#CCCCCC;margin-right: 53px;margin-left: 55px;"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="left-sidebar" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;font-size:0;">
                                                        <div class="column left" style="width:100%;display:inline-block;vertical-align:middle;max-width:435px;">
															<!--[if (gte mso 9)|(IE)]>
															<table width="435" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
															<![endif]-->
                                                            <table class="main" width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
                                                                <tbody><tr>
                                                                    <td class="inner" style="padding-top: 20px;padding-bottom: 20px;padding-right:0;vertical-align:top;">
                                                                        <img src="http://gobrainwiz.in/b_assets/business-mail/3.jpg" width="100%" alt="" style="margin:0 !important;border-width:0;width:100px">
                                                                    </td>
                                                                    <td>
                                                                        <div class="column right" style="width:100%;display:inline-block;vertical-align:middle;max-width:400px;">
																			<!--[if (gte mso 9)|(IE)]>
																			<table width="350" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
																			<![endif]-->
                                                                            <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                <tbody><tr>
                                                                                    <td class="inner contents" style="padding-top: 20px;padding-bottom:20px;padding-right:10px;padding-left: 13px;width:100%;font-size:14px;text-align:left;">
                                                                                        <span class="heading" style="font-weight:bold;color:#4266B0; font-size: 20px;line-height: 1;">Soft Skills</span><br/><br>
                                                                                        <span class="sub-heading" style="color:#333; text-algn:left; font-size: 16px; line-height:1.6;"> Email- Writing + Essay Writing + Group Discussion + Resume Building + Personality Development + MOCK INTERVIEWS.</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
																			</table>
																			<!--[if (gte mso 9)|(IE)]>
																			</table>
																			<![endif]-->
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
															</table>
															<!--[if (gte mso 9)|(IE)]>
															</table>
															<![endif]-->
                                                        </div>
                                                        <div class="border-margin" style="border-bottom-width:1px;border-bottom-style:dashed;border-bottom-color:#CCCCCC;margin-right: 53px;margin-left: 55px;"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="left-sidebar" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;font-size:0;">
                                                        <div class="column left" style="width:100%;display:inline-block;vertical-align:middle;max-width:435px;">
															<!--[if (gte mso 9)|(IE)]>
															<table width="435" align="center" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
															<![endif]-->
                                                            <table class="main" width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;padding-left:0;padding-right:0;">
                                                                <tbody><tr>
                                                                    <td class="inner" style="padding-top: 20px;padding-bottom: 20px;padding-right:0;vertical-align:top;">
                                                                        <img src="4.jpg" width="100%" alt="" style="margin:0 !important;border-width:0;width:100px">
                                                                    </td>
                                                                    <td>
                                                                        <div class="column right" style="width:100%;display:inline-block;vertical-align:middle;max-width:400px;">
																			<!--[if (gte mso 9)|(IE)]>
																			<table width="350" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
																			<![endif]-->
                                                                            <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                <tbody><tr>
                                                                                    <td class="inner contents" style="padding-top: 20px;padding-bottom:25px;padding-right:10px;padding-left: 13px;width:100%;font-size:14px;text-align:left;">
                                                                                    <span class="heading" style="font-weight:bold;color:#4266B0; font-size: 20px;line-height: 1;">Mock Online Tests + Videos</span><br/><br>
																					<span class="sub-heading" style="color:#333; text-algn:left; font-size: 16px; line-height:1.6;">MNC based MOCK online tests also boost your confidence and performance. Our Online tests can enhance your speed in live tests.</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
																			</table>
																			<!--[if (gte mso 9)|(IE)]>
																			</table>
																			<![endif]-->
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
															</table>
															<!--[if (gte mso 9)|(IE)]>
															</table>
															<![endif]-->
                                                        </div>
                                                        <div class="border-margin" style="border-bottom-width:1px;border-bottom-style:dashed;border-bottom-color:#CCCCCC;margin-right: 53px;margin-left: 55px;"></div>
                                                    </td>
                                                </tr>
												<tr>
													<td class="inner contentsBody" style="padding-top: 24px;padding-bottom: 0px;padding-right: 44px;padding-left: 44px;width:100%;text-align:left;">
														<p style="Margin:0;font-size: 16px;Margin-bottom: 0px;color:#8C8C8C;font-family: Arial, Helvetica, sans-serif">
														Talk to you soon,
														</p><br>

														<p style="margin:0;font-size: 18px;font-weight:bold;color:#04079d;font-family: Arial, Helvetica, sans-serif;"> BRAINWIZ TEAM
														</p>
														<br>

													</td>
                                                </tr>
                                                <tr>
                                                    <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#c2d8ff">
                                                        <table width="100%" style="border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#626262;">
                                                            <tbody><tr>
                                                                <td class="inner contents-footer" align="center" style="padding-top:1.50em;padding-bottom: 1.50em;padding-right:2.75em;padding-left: 2.75em;width:100%;text-align:center;">
                                                                    <p class="h1" style="margin:0 !important;font-size: 28px;font-weight:bold;Margin-bottom: 6px;color: #626262;"><!--Aliquam erat volpat.--></p>
                                                                    <p style="margin:0 !important;color: #626262;font-size: 17px;"><!--Donec sodales pulvinar velit, quis porttitor massa.--></p>
                                                                    <div align="center" class="button_table" style="padding-bottom:1px;padding-top:10px;">
                                                                        <a href="https://www.facebook.com/business/a/facebook-video-ads-creative-ideas" style="color:#ffffff;display:inline-block;border-width:0;text-decoration:underline;">
                                                                            <table cellspacing="0" cellpadding="15" bgcolor="#4380fc" style="border-width:1px;border-style:solid;border-color:#4380fc;background-color:#4380fc;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;width:auto;min-width: 260px;border-spacing:0;font-family: Arial, Helvetica, sans-serif;color:#333333;">
                                                                                <tbody><tr>
                                                                                    <td bgcolor="#4380fc" style="color:#ffffff;display:block;background-color:#4380fc;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top: 10.5px;padding-bottom: 10.5px;padding-right: 25px;padding-left: 25px;font-family: Arial, Helvetica, sans-serif;">
                                                                                        <strong align="center">
                                                                                            <strong><a href="http://www.gobrainwiz.in/" class="font24" style="font-weight:bold;color:#ffffff;font-size: 23px;line-height:1.2em;font-family: Arial, Helvetica, sans-serif;text-decoration:none;width:100%;display:inline-block;border-width:0;">REGISTER NOW</a>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
														</table>
                                                    </td>
                                                </tr>
                                                </tbody>
											</table>
											</td>
                                        </tr>
                                        </tbody>
								</table>
								<!--[if (gte mso 9)|(IE)]>
								</td>
								</tr>
								</table>
								<![endif]-->

                                <!--[if (gte mso 9)|(IE)]>
                                <table width="550" align="center" style="border-spacing:0;font-family:'Arial Regular';color:#333333;" >
                                <tr>
								<td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                <![endif]-->

                                   <table width="100%" style="border-spacing:0;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#333333;font-size:11px" >
										<tr>
											<td class="inner contents footer100" style="text-align:left;color:#B9B8B8;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;width:100%;" >

											</td>
										</tr>
									</table>
	
                                <!--[if (gte mso 9)|(IE)]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
							</td>
						</tr>
					</table>
					<!--[if (gte mso 9)|(IE)]>
					</td>
					</tr>
					</table>
					<![endif]-->
				</td>
			</tr>
		</table>
		<!--[if (gte mso 9)|(IE)]>
		</td>
		</tr>
		</table>
		<![endif]-->
    </div>
</center>
</body>
</html>