<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class Coupons_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllCoupons()
    {
        $sql = "SELECT * FROM gk_coupons";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }

    public function getAllUsers()
    {
        $sql = "SELECT * FROM gk_examuserslist";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }

    public function deleteCouponById($id)
    {
        $this->db->where('coupon_id', "$id");
        $this->db->delete('gk_coupons');
    }

    public function addCoupon($form)
    {
        return $this->db->insert('gk_coupons', $form);
    }

    public function updateCoupon($form, $id)
    {
        $this->db->where('coupon_id', $id);
        $this->db->update('gk_coupons', $form);
    }

    public function getCouponById($id)
    {
        $this->db->where('coupon_id', $id);
        return $this->db->get('gk_coupons')->row();
    }

    public function get($limit, $start)
    {

        $sql = "SELECT * FROM gk_coupons
			        ORDER BY coupon_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }

    }

    public function couponscount()
    {
        $sql = "SELECT * FROM gk_coupons";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function assignedCouponscount()
    {
        $sql = "SELECT * FROM gk_assign_coupons_users";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function getAssignedCoupons($limit, $start, $search)
    {

        $sql = "SELECT uc.acu_id,c.coupon_code,u.examuser_name, u.examuser_mobile,au.examuser_name as assignee_name, au.examuser_mobile as assignee_mobile FROM gk_assign_coupons_users uc 
INNER JOIN gk_coupons c ON c.coupon_id = uc.acu_coupon_id
INNER JOIN gk_examuserslist u ON u.examuser_id = uc.acu_user_id
INNER JOIN gk_examuserslist au ON au.examuser_id = uc.acu_assignee_id ";

        $where = '';
        if (!empty($search) && isset($search['coupon_code'])) {
            $where .= "c.coupon_code='" . $search['coupon_code'] . "' ";
        }

        if (!empty($search) && isset($search['username'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(u.examuser_name='" . $search['username'] . "') ";
        }

        if (!empty($search) && isset($search['mobile_no'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(u.examuser_mobile='" . $search['mobile_no'] . "') ";
        }

        if (!empty($search) && isset($search['user'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(uc.acu_assignee_id=" . $search['user'] . ") ";
        }


        if ($where) {
            $sql .= "WHERE  " . $where;
        }


        $sql .= "ORDER BY uc.acu_id DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }

    public function deleteAssignedCouponById($id)
    {
        $this->db->where('acu_id', "$id");
        $this->db->delete('gk_assign_coupons_users');
    }

    public function addAssignedCoupon($form)
    {
        return $this->db->insert('gk_assign_coupons_users', $form);
    }

    public function updateAssignedCoupon($form, $id)
    {
        $this->db->where('acu_id', $id);
        $this->db->update('gk_assign_coupons_users', $form);
    }

    public function getAssignCouponById($id)
    {
        $this->db->where('acu_id', $id);
        return $this->db->get('gk_assign_coupons_users')->row();
    }

    public function getAssignCouponByCouponID($id)
    {
        $this->db->where('acu_coupon_id', $id);
        return $this->db->get('gk_assign_coupons_users')->row();
    }

    public function CouponUseagecount()
    {
        $sql = "SELECT * FROM gk_coupon_usages";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function getCouponsUseages($limit, $start, $search)
    {

        $sql = "SELECT 
cu.cu_used_date,cu.cu_id,
c.coupon_code,
u.examuser_name, u.examuser_mobile,
v.examuser_name as provider_user, v.examuser_mobile as provider_user_phone
FROM gk_coupon_usages cu 
INNER JOIN gk_coupons c ON c.coupon_id = cu.cu_coupon_id
INNER JOIN gk_examuserslist u ON u.examuser_id = cu.cu_used_user_id
INNER JOIN gk_examuserslist v ON v.examuser_id = cu.cu_provider_user_id ";

        $where = '';
        if (!empty($search) && isset($search['coupon_code'])) {
            $where .= "c.coupon_code='" . $search['coupon_code'] . "' ";
        }

        if (!empty($search) && isset($search['username'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(u.examuser_name='" . $search['username'] . "' OR v.examuser_name='" . $search['username'] . "') ";
        }

        if (!empty($search) && isset($search['mobile_no'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(u.examuser_mobile='" . $search['mobile_no'] . "' OR v.examuser_mobile='" . $search['mobile_no'] . "') ";
        }

        if ($where) {
            $sql .= "WHERE  " . $where;
        }


        $sql .= "ORDER BY cu.cu_id DESC LIMIT $start, $limit";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }

    public function deleteCouponUseage($id)
    {
        $this->db->where('cu_id', "$id");
        $this->db->delete('gk_coupon_usages');
    }

    public function addCouponUseage($form)
    {
        return $this->db->insert('gk_coupon_usages', $form);
    }

    public function addWallet($form)
    {
        return $this->db->insert('gk_user_wallet', $form);
    }

    public function getAllAssignedCoupons()
    {
        $sql = "SELECT c.*,cu.* FROM gk_coupons c
INNER JOIN  gk_assign_coupons_users cu ON c.coupon_id = cu.acu_coupon_id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }

    public function withdrawRequestByUser()
    {
        $sql = "SELECT * FROM gk_withdraw_requests";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function getwithdrawrequestsByUser($limit, $start, $search)
    {

        $sql = "SELECT * FROM gk_withdraw_requests wr
INNER JOIN gk_examuserslist u ON u.examuser_id = wr.wr_user_id ";

        $where = '';
        if (!empty($search) && isset($search['username'])) {
            $where .= "(u.examuser_name='" . $search['username'] . "') ";
        }

        if (!empty($search) && isset($search['mobile_no'])) {
            if ($where) {
                $where .= "AND ";
            }
            $where .= "(u.examuser_mobile='" . $search['mobile_no'] . "' ) ";
        }

        if ($where) {
            $sql .= "WHERE  " . $where;
        }

        $sql .= "ORDER BY wr_id DESC LIMIT $start, $limit";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        }

    }

    public function getWithdrawID($id)
    {

        $this->db->where('wr_id', $id);
        return $this->db->get('gk_withdraw_requests')->row();

    }

    public function addWithdrawRequest($form)
    {
        return $this->db->insert('gk_withdraw_requests', $form);
    }

    public function getAllCouponsExceptAssigned()
    {
        $sql = "SELECT * 
FROM gk_coupons 
WHERE NOT EXISTS 
    (SELECT * 
     FROM gk_assign_coupons_users 
     WHERE gk_assign_coupons_users.acu_coupon_id = gk_coupons.coupon_id)";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return [];
        }
    }


}