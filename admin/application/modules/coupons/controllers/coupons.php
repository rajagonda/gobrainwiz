<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class coupons extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->authentication->checklogin()) {
            redirect('login');
        }
        $this->load->library(array('template', 'form_validation'));
        $this->template->set_title('coupons');
        $this->load->model(array('examusers/examusers_Model', 'coupons_model'));
        $this->load->language('coupons');
        $this->load->helper(array('text', 'basic'));
        $this->load->library('pagination');
        $this->activeInactiveOptions = ["active" => "Active", "inactive" => "Inactive"];

    }

    public function index($param = false)
    {
        $breadcrumbarray = [
            'label' => "Coupons",
            'link' => base_url() . "coupons",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Coupons");


        // Pagination code starts here


        $config['base_url'] = base_url() . '/coupons/';
        $config["total_rows"] = $this->coupons_model->couponscount();
        $config["per_page"] = 100;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["coupons"] = $this->coupons_model->get($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('coupons', $data);
    }

    public function DeleteCoupon($id)
    {
        $this->coupons_model->deleteCouponById($id);
        $this->session->set_userdata('coupon_message', "Successfully Deleted Coupon!!");
        redirect('coupons');
    }

    public function AddCoupons()
    {
        $breadcrumbarray = [
            'label' => "Add New Coupons",
            'link' => base_url() . "coupons/AddCoupons",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Add New Coupons");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required|is_unique[gk_coupons.coupon_code]');
            $this->form_validation->set_rules('coupon_quantity', 'Coupon Qunatity', 'required');
            $this->form_validation->set_rules('coupon_referral_price', 'Coupon Referral Price', 'required');
            $this->form_validation->set_rules('coupon_earning_price', 'Coupon Earning Price', 'required');

            if ($this->form_validation->run() == TRUE) {
                $form['coupon_code'] = $this->input->post('coupon_code');
                $form['coupon_quantity'] = $this->input->post('coupon_quantity');
                $form['coupon_referral_price'] = $this->input->post('coupon_referral_price');
                $form['coupon_earning_price'] = $this->input->post('coupon_earning_price');
                $form['coupon_status'] = $this->input->post('coupon_status');
                $form['coupon_description'] = $this->input->post('coupon_description');

                $this->coupons_model->addCoupon($form);
                $this->session->set_userdata('coupon_message', "Successfully Added New Coupon!!");
                redirect('coupons');
            }
        }
        $this->template->load_view1('add_coupons', ['activeInactiveOptions' => $this->activeInactiveOptions]);
    }

    public function EditCoupon($id)
    {
        $breadcrumbarray = [
            'label' => "Update Coupons",
            'link' => base_url() . "coupons/EditCoupon/" . $id,
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Update Coupons");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $original_value = $this->db->query("select coupon_code from gk_coupons where coupon_id ='" . $this->input->post("coupon_id") . "' ")->row()->coupon_code;

            if (isset($original_value) && $this->input->post('coupon_code') != $original_value) {
                $is_unique = '|is_unique[coupons.coupon_code]';
            } else {
                $is_unique = '';
            }

            $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required' . $is_unique);
            $this->form_validation->set_rules('coupon_quantity', 'Coupon Qunatity', 'required');
            $this->form_validation->set_rules('coupon_referral_price', 'Coupon Referral Price', 'required');
            $this->form_validation->set_rules('coupon_earning_price', 'Coupon Earning Price', 'required');

            if ($this->form_validation->run() == TRUE) {

                $form['coupon_code'] = $this->input->post('coupon_code');
                $form['coupon_quantity'] = $this->input->post('coupon_quantity');
                $form['coupon_referral_price'] = $this->input->post('coupon_referral_price');
                $form['coupon_earning_price'] = $this->input->post('coupon_earning_price');
                $form['coupon_status'] = $this->input->post('coupon_status');
                $form['coupon_description'] = $this->input->post('coupon_description');

                $this->coupons_model->updateCoupon($form, $id);
                $this->session->set_userdata('coupon_message', "Successfully Updated Coupon!!");
                redirect('coupons');
            }
        }
        $coupon = $this->coupons_model->getCouponById($id);
        $this->template->load_view1('edit_coupons', ['activeInactiveOptions' => $this->activeInactiveOptions, 'coupon' => $coupon]);
    }

    public function assignCouponsToUsers($param = false)
    {
        $breadcrumbarray = [
            'label' => "Coupons",
            'link' => base_url() . "assignCouponsToUsers",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Assigned Coupons List");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/assignCouponsToUsers/';
        $config["total_rows"] = $this->coupons_model->assignedCouponscount();
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $search = array();
        if ($this->input->get('search')) {
            if ($this->input->get('search_user')) {
                $search['user'] = $this->input->get('search_user');
            }
            if ($this->input->get('coupon_code')) {
                $search['coupon_code'] = $this->input->get('coupon_code');
            }
            if ($this->input->get('mobile_no')) {
                $search['mobile_no'] = $this->input->get('mobile_no');
            }
            if ($this->input->get('username')) {
                $search['username'] = $this->input->get('username');
            }

        }
        $data["coupons"] = $this->coupons_model->getAssignedCoupons($config["per_page"], $page, $search);

        $data['users'] = $this->coupons_model->getAllUsers();
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('assigned_coupons', $data);
    }

    public function DeleteAssignedCoupons($id)
    {
        $this->coupons_model->deleteAssignedCouponById($id);
        $this->session->set_userdata('coupon_message', "Successfully Deleted Coupon!!");
        redirect('coupons/assignCouponsToUsers');
    }


    public function AddCouponsToUsers()
    {
        $breadcrumbarray = [
            'label' => "Assign Coupons to user",
            'link' => base_url() . "coupons/AddCouponsToUsers",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Assign Coupons to user");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('acu_user_id', 'User', 'required');
            $this->form_validation->set_rules('acu_coupon_id', 'Coupon', 'required|is_unique[gk_assign_coupons_users.acu_coupon_id]');

            if ($this->form_validation->run() == TRUE) {
                $form['acu_user_id'] = $this->input->post('acu_user_id');
                $form['acu_coupon_id'] = $this->input->post('acu_coupon_id');
                $form['acu_assignee_id'] = $this->session->userdata('appadminuser_id');

                $this->coupons_model->addAssignedCoupon($form);
                $this->session->set_userdata('coupon_message', "Successfully Assigned User To Coupon!");
                redirect('coupons/assignCouponsToUsers');
            }
        }
        $data['coupons'] = $this->coupons_model->getAllCouponsExceptAssigned();
        $data['users'] = $this->coupons_model->getAllUsers();
        $this->template->load_view1('assign_coupons_users', $data);
    }

    public function EditAssignedCouponsToUsers($id)
    {
        $breadcrumbarray = [
            'label' => "Update Assigned Coupons",
            'link' => base_url() . "coupons/EditAssignedCouponsToUsers/" . $id,
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Update Coupons");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $original_value = $this->db->query("select acu_coupon_id from gk_assign_coupons_users where acu_id ='" . $this->input->post("acu_id") . "' ")->row()->acu_coupon_id;

            if (isset($original_value) && $this->input->post('acu_coupon_id') != $original_value) {
                $is_unique = '|is_unique[gk_assign_coupons_users.acu_coupon_id]';
            } else {
                $is_unique = '';
            }

            $this->form_validation->set_rules('acu_coupon_id', 'Coupon', 'required' . $is_unique);
            $this->form_validation->set_rules('acu_user_id', 'User', 'required');

            if ($this->form_validation->run() == TRUE) {

                $form['acu_user_id'] = $this->input->post('acu_user_id');
                $form['acu_coupon_id'] = $this->input->post('acu_coupon_id');

                $this->coupons_model->updateAssignedCoupon($form, $id);
                $this->session->set_userdata('coupon_message', "Successfully Updated Coupon!!");
                redirect('coupons/assignCouponsToUsers');
            }
        }
        $data['coupons'] = $this->coupons_model->getAllCouponsExceptAssigned();
        $data['users'] = $this->coupons_model->getAllUsers();
        $data['coupon'] = $this->coupons_model->getAssignCouponById($id);
        $this->template->load_view1('edit_assign_coupons_users', $data);
    }


    public function couponUseages($param = false)
    {
        $breadcrumbarray = [
            'label' => "Coupons",
            'link' => base_url() . "couponUseages",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Coupon Useages List");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/couponUseages/';
        $config["total_rows"] = $this->coupons_model->CouponUseagecount();
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $search = array();
        if ($this->input->get('search')) {
            if ($this->input->get('coupon_code')) {
                $search['coupon_code'] = $this->input->get('coupon_code');
            }
            if ($this->input->get('mobile_no')) {
                $search['mobile_no'] = $this->input->get('mobile_no');
            }
            if ($this->input->get('username')) {
                $search['username'] = $this->input->get('username');
            }

        }
        $data["coupons"] = $this->coupons_model->getCouponsUseages($config["per_page"], $page, $search);
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('coupon_useages', $data);
    }

    public function AddCouponUseages()
    {
        $breadcrumbarray = [
            'label' => "Add Coupon Useages",
            'link' => base_url() . "coupons/AddCouponUseages",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Add Coupon Useages");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

//            $this->form_validation->set_rules('cu_provider_user_id', 'User', 'required');
            $this->form_validation->set_rules('cu_used_user_id', 'User', 'required');
            $this->form_validation->set_rules('cu_used_date', 'Date', 'required');
            $this->form_validation->set_rules('cu_coupon_id', 'Coupon', 'required');

            if ($this->form_validation->run() == TRUE) {

                $assigncouponinfo = $this->coupons_model->getAssignCouponByCouponID($this->input->post('cu_coupon_id'));

                $form['cu_provider_user_id'] = $assigncouponinfo->acu_user_id;
                $form['cu_used_user_id'] = $this->input->post('cu_used_user_id');
                $form['cu_used_date'] = date("Y-m-d", strtotime($this->input->post('cu_used_date')));
                $form['cu_coupon_id'] = $this->input->post('cu_coupon_id');
                $form['cu_status'] = 'applied';

                $couponinfo = $this->coupons_model->getCouponById($this->input->post('cu_coupon_id'));

                $wallet = array();
                $wallet['uw_user_id'] = $this->input->post('cu_used_user_id');
                $wallet['uw_coupon_id'] = $this->input->post('cu_coupon_id');
                $wallet['uw_coupon_type'] = "referral";
                $wallet['uw_price_type'] = "credit";
                $wallet['uw_price'] = $couponinfo->coupon_referral_price;
                $this->coupons_model->addWallet($wallet);

//                $input = array();
//                $input['wr_requested_amount'] = $couponinfo->coupon_referral_price;
//                $input['wr_user_id'] = $this->input->post('cu_used_user_id');
//                $this->coupons_model->addWithdrawRequest($input);
//
//
//                $userinfo = $this->examusers_Model->getsingleuser($this->input->post('cu_used_user_id'));
//
//                $updatedamount = ($userinfo->examuser_wallet) + $couponinfo->coupon_referral_price;
//
//                $updatedata = array();
//                $updatedata['examuser_wallet'] = $updatedamount;
//                $this->db->where('examuser_id', $this->input->post('cu_used_user_id'));
//                $this->db->update('gk_examuserslist', $updatedata);

                $wallet = array();
                $wallet['uw_user_id'] = $assigncouponinfo->acu_user_id;
                $wallet['uw_coupon_id'] = $this->input->post('cu_coupon_id');
                $wallet['uw_coupon_type'] = "provider";
                $wallet['uw_price_type'] = "credit";
                $wallet['uw_scratch_status'] = 1;
                $wallet['uw_price'] = $couponinfo->coupon_earning_price;
                $this->coupons_model->addWallet($wallet);

                $input = array();
                $input['wr_requested_amount'] = $couponinfo->coupon_earning_price;
                $input['wr_requested_amount_type'] = "provider";
                $input['wr_user_id'] = $assigncouponinfo->acu_user_id;
                $this->coupons_model->addWithdrawRequest($input);

                $userinfo = $this->examusers_Model->getsingleuser($assigncouponinfo->acu_user_id);

                $updatedamount = ($userinfo->examuser_wallet) + $couponinfo->coupon_earning_price;

                $updatedata = array();
                $updatedata['examuser_wallet'] = $updatedamount;
                $this->db->where('examuser_id', $assigncouponinfo->acu_user_id);
                $this->db->update('gk_examuserslist', $updatedata);

                $this->coupons_model->addCouponUseage($form);
                $this->session->set_userdata('coupon_message', "Successfully added to coupon useage list!");
                redirect('coupons/couponUseages');
            }
        }
        $data['coupons'] = $this->coupons_model->getAllAssignedCoupons();
        $data['users'] = $this->coupons_model->getAllUsers();
        $this->template->load_view1('add_coupon_useage', $data);
    }

    public function withdrawrequests()
    {

        $breadcrumbarray = [
            'label' => "Withdraw Requests",
            'link' => base_url() . "onlinetest/withdrawrequests",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Withdraw Requests List");


        // Pagination code starts here

        $config['base_url'] = base_url() . '/coupons/withdrawrequests/';
        $config["total_rows"] = $this->coupons_model->withdrawRequestByUser();
        $config["per_page"] = 100;
        $config["uri_segment"] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $search = array();
        if ($this->input->get('search')) {
            if ($this->input->get('mobile_no')) {
                $search['mobile_no'] = $this->input->get('mobile_no');
            }
            if ($this->input->get('username')) {
                $search['username'] = $this->input->get('username');
            }

        }
        $data["records"] = $this->coupons_model->getwithdrawrequestsByUser($config["per_page"], $page, $search);
        $data["links"] = $this->pagination->create_links();

        $this->template->load_view1('withdraw_request_list', $data);
    }

    public function pay($id)
    {
        $breadcrumbarray = [
            'label' => "Pay",
            'link' => base_url() . "coupons/pay",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Pay");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('wr_paid_amount', 'Amount', 'required');
            $this->form_validation->set_rules('wr_payment_type', 'Payment Type', 'required');
            $this->form_validation->set_rules('wr_admin_description', 'Description', 'required');

            if ($this->form_validation->run() == TRUE) {

                $recordInfo = $this->coupons_model->getWithdrawID($id);

                $wallet = array();
                $wallet['uw_user_id'] = $recordInfo->wr_user_id;
                $wallet['uw_price'] = $this->input->post('wr_paid_amount');
                $wallet['uw_price_type'] = "debit";
                $this->coupons_model->addWallet($wallet);

                $userinfo = $this->examusers_Model->getsingleuser($recordInfo->wr_user_id);

                $updatedamount = ($userinfo->examuser_wallet) - $this->input->post('wr_paid_amount');

                $updatedata = array();
                $updatedata['examuser_wallet'] = $updatedamount;
                $this->db->where('examuser_id', $recordInfo->wr_user_id);
                $this->db->update('gk_examuserslist', $updatedata);


                $updatedata = array();
                $updatedata['wr_paid_amount'] = $this->input->post('wr_paid_amount');
                $updatedata['wr_payment_type'] = $this->input->post('wr_payment_type');
                $updatedata['wr_admin_description'] = $this->input->post('wr_admin_description');
                $updatedata['wr_status'] = "Paid";
                $this->db->where('wr_id', $id);
                $this->db->update('gk_withdraw_requests', $updatedata);


                $this->session->set_userdata('request_message', "Successfully paid!");
                redirect('coupons/withdrawrequests');
            }
        }
        $data['record'] = $this->coupons_model->getWithdrawID($id);
        $this->template->load_view1('pay', $data);
    }
}
