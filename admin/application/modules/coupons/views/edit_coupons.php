<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Edit Coupons</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?= current_url() ?>" role="form" name="coupon_form"
                              id="coupon_form" enctype="multipart/form-data">
                            <input type="hidden" name="coupon_id" value="<?= $coupon->coupon_id ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Coupon Code :</label>
                                    <input type="text" placeholder="Coupon Code"
                                           value="<?= $coupon->coupon_code ?>" name="coupon_code" id="coupon_code"
                                           class="form-control">
                                    <?php echo form_error('coupon_code', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Coupon Referral Amount :</label>
                                    <input type="text" placeholder="Coupon Referral Amount" name="coupon_referral_price"
                                           id="coupon_referral_price" class="form-control" value="<?= $coupon->coupon_referral_price ?>">
                                    <?php echo form_error('coupon_referral_price', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Coupon Earning Amount :</label>
                                    <input type="text" placeholder="Coupon Earning Amount" name="coupon_earning_price"
                                           id="coupon_earning_price" class="form-control" value="<?= $coupon->coupon_earning_price ?>">
                                    <?php echo form_error('coupon_earning_price', '<div class="error">', '</div>'); ?>
                                </div>
                                    <div class="form-group">
                                        <label>Coupon Qunatity :</label>
                                        <input type="text" placeholder="Coupon Qunatity"
                                               value="<?= $coupon->coupon_quantity ?>" name="coupon_quantity"
                                               id="coupon_quantity" class="form-control">
                                        <?php echo form_error('coupon_quantity', '<div class="error">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Description :</label>
                                        <textarea placeholder="Description" cols="" name="coupon_description"
                                                  class="form-control"><?= $coupon->coupon_description ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Sent To :</label>
                                        <select name="coupon_status">
                                            <?php
                                            foreach ($activeInactiveOptions as $index => $opt) {
                                                ?>
                                                <option value="<?=$index?>" <?=$coupon->coupon_status==$index ? "selected":""?>><?=$opt?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
