<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Edit Assigned Coupons</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?= current_url() ?>" role="form" name="coupon_form"
                              id="coupon_form" enctype="multipart/form-data">
                            <input type="hidden" name="acu_id" value="<?= $coupon->acu_id ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Coupon:</label>
                                    <select id="acu_coupon_id" name="acu_coupon_id" class="form-control">
                                        <option value=''>Please Select Coupon</option>
                                        <?php foreach ($coupons as $coup) { ?>
                                            <option value="<?php echo $coup->coupon_id; ?>" <?=$coupon->acu_coupon_id==$coup->coupon_id ? "selected":""?>> <?php echo $coup->coupon_code; ?></option>

                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('acu_coupon_id', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>User:</label>
                                    <select id="acu_user_id" name="acu_user_id" class="form-control">
                                        <option value=''>Please Select User</option>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="<?php echo $user->examuser_id; ?>" <?=$coupon->acu_user_id==$user->examuser_id ? "selected":""?>> <?php echo $user->examuser_name; ?></option>

                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('acu_user_id', '<div class="error">', '</div>'); ?>
                                </div>
                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
