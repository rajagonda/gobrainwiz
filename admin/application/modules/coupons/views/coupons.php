<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('coupon_message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('coupon_message');
                                $this->session->set_userdata('coupon_message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Coupons
                        <a class="blue" title="Add New Coupons" href="<?php echo base_url(); ?>coupons/AddCoupons"
                           style="float:right">
                            <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;"></span>
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th> Coupon Code</th>
                                <th> Coupon Referral Amount</th>
                                <th> Coupon Earning Amount</th>
                                <th> Coupon Quantity</th>
                                <th> Coupon Description</th>
                                <th> Coupon Status</th>
                                <th> Created At</th>
                                <th> Edit</th>
                                <th> Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($coupons) > 0) {
                                foreach ($coupons as $index => $coupon) {
                                    ?>
                                    <tr>
                                        <td> <?= $index+1 ?></td>
                                        <td title="<?= $coupon->coupon_code ?>">
                                            <p>
                                                <?php
//                                                echo qRCodeCreate($coupon->coupon_code)
                                                ?>
                                            </p>


                                            <img alt="Embedded Image" src="<?php echo  qRCodeCreate($coupon->coupon_code) ?>"  />

                                            <?= $coupon->coupon_code ?>
                                            <a href="<?php echo  qRCodeCreate($coupon->coupon_code) ?>" download="<?= 'coupon_code_'.$coupon->coupon_code?>">
                                                <span class="fa fa-download"></span>
                                            </a>


                                        </td>
                                        <td title="<?= $coupon->coupon_referral_price ?>"> <?= $coupon->coupon_referral_price ?></td>
                                        <td title="<?= $coupon->coupon_earning_price ?>"> <?= $coupon->coupon_earning_price ?></td>
                                        <td title="<?= $coupon->coupon_quantity ?>"> <?= $coupon->coupon_quantity ?></td>
                                        <td title="<?= $coupon->coupon_description ?>"> <?= $coupon->coupon_description ?></td>
                                        <td title="<?= $coupon->coupon_status ?>"> <?= $coupon->coupon_status ?></td>
                                        <td style="width: 200px"> <?= date("d-m-Y h:i:s a", strtotime($coupon->createdAt)) ?> </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">
                                                <a class="green"
                                                   href="<?php echo base_url(); ?>coupons/EditCoupon/<?= $coupon->coupon_id ?>"
                                                   style="padding: 14px;">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">
                                                <a class="red trash"
                                                   href="<?php echo base_url(); ?>coupons/DeleteCoupon/<?= $coupon->coupon_id ?>"
                                                   style="padding: 14px;">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="10" style="text-align:center">No Records Found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
</section>