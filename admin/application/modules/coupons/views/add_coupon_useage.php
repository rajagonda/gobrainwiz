<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Add Coupon Useage</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?= current_url() ?>" role="form" name="coupon_form"
                              id="coupon_form" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Coupon:</label>
                                    <select id="cu_coupon_id" name="cu_coupon_id" class="form-control">
                                        <option value=''>Please Select Coupon</option>
                                        <?php foreach ($coupons as $coupon) { ?>
                                            <option value="<?php echo $coupon->coupon_id; ?>"> <?php echo $coupon->coupon_code; ?></option>

                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('cu_coupon_id', '<div class="error">', '</div>'); ?>
                                </div>
<!--                                -->
<!--                                <input type="hidden">-->
<!--                                -->
<!--                                <div class="form-group">-->
<!--                                    <label>Provider User:</label>-->
<!--                                    <select id="cu_provider_user_id" name="cu_provider_user_id" class="form-control">-->
<!--                                        <option value=''>Please Select User</option>-->
<!--                                        --><?php //foreach ($users as $user) { ?>
<!--                                            <option value="--><?php //echo $user->examuser_id; ?><!--"> --><?php //echo $user->examuser_name; ?><!--</option>-->
<!---->
<!--                                        --><?php //} ?>
<!--                                    </select>-->
<!--                                    --><?php //echo form_error('cu_provider_user_id', '<div class="error">', '</div>'); ?>
<!--                                </div>-->
                              <div class="form-group">
                                    <label>Used User:</label>
                                    <select id="cu_used_user_id" name="cu_used_user_id" class="form-control">
                                        <option value=''>Please Select User</option>
                                        <?php foreach ($users as $user) { ?>
                                            <option value="<?php echo $user->examuser_id; ?>"> <?php echo $user->examuser_name; ?></option>

                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('cu_used_user_id', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Used Date :</label>
                                    <input type="text" placeholder="Coupon Used date" value="" name="cu_used_date"
                                           id="cu_used_date" class="form-control datepicker">
                                    <?php echo form_error('cu_used_date', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '-3d'
        });
    });
</script>