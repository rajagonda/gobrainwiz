<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('coupon_message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('coupon_message');
                                $this->session->set_userdata('coupon_message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Coupon Useages List
                        <a class="blue" title="Add Coupon Useage"
                           href="<?php echo base_url(); ?>coupons/AddCouponUseages"
                           style="float:right">
                            <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;"></span>
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <form method="get">
                        <input type="hidden" name="search" value="search"/>

                        <input type="text" placeholder="Coupon Code" name="coupon_code"
                               value="<?php echo $this->input->get('coupon_code'); ?>"/>
                        <input type="text" placeholder="Mobile No" name="mobile_no"
                               value="<?php echo $this->input->get('mobile_no'); ?>"/>
                        <input type="text" placeholder="Username" name="username"
                               value="<?php echo $this->input->get('username'); ?>"/>

                        <span class="">

                              <button class="btn btn-primary" type="submit">Search</button></span>
                    </form>


                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th> Coupon</th>
                                <th> Provider User</th>
                                <th> Used User</th>
                                <th> Used Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($coupons) > 0) {
                                foreach ($coupons as $index => $coupon) {
                                    ?>
                                    <tr>
                                        <td> <?= $index + 1 ?></td>
                                        <td title="<?= $coupon->coupon_code ?>"> <?= $coupon->coupon_code ?></td>
                                        <td title="<?= $coupon->provider_user ?>"> <?= $coupon->provider_user ?>
                                            (<?= $coupon->provider_user_phone ?>)
                                        </td>
                                        <td title="<?= $coupon->examuser_name ?>"> <?= $coupon->examuser_name ?>
                                            (<?= $coupon->examuser_mobile ?>)
                                        </td>
                                        <td title="<?= date("d-m-Y", strtotime($coupon->cu_used_date)); ?>"> <?= date("d-m-Y", strtotime($coupon->cu_used_date)); ?></td>

                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="6" style="text-align:center">No Records Found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>


        </div>
    </div>
</section>