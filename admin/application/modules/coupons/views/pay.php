<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Pay</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?= current_url() ?>" role="form" name="coupon_form"
                              id="coupon_form" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Amount :</label>
                                    <input type="text" placeholder="Amount"
                                           value="<?php echo $record->wr_requested_amount; ?>" name="wr_paid_amount"
                                           id="wr_paid_amount" class="form-control">
                                    <?php echo form_error('wr_paid_amount', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Comments :</label>
                                    <input type="text" placeholder="Comments" name="wr_admin_description"
                                           id="wr_admin_description" class="form-control">
                                    <?php echo form_error('wr_admin_description', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
