<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('coupon_message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('coupon_message');
                                $this->session->set_userdata('coupon_message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Assigned Coupons
                        <a class="blue" title="Add New Coupons"
                           href="<?php echo base_url(); ?>coupons/AddCouponsToUsers"
                           style="float:right">
                            <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;"></span>
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <form method="get">
                        <input type="hidden" name="search" value="search"/>
                        <select name="search_user" id="search_user" class="select2">
                            <option value="">Select Assignee User</option>
                            <?php foreach ($users as $user) { ?>
                                <option value="<?php echo $user->examuser_id; ?>"
<!--                                    --><?php
//                                    if ($this->input->get('search_user') == $user->examuser_id) {
//                                        ?>
<!--                                        selected-->
<!---->
<!--                                    --><?php //} ?>

                                 <?php echo $user->examuser_name; ?> (<?= $user->examuser_mobile ?>)</option>

                            <?php } ?>
                        </select>

                        <input type="text" placeholder="Coupon Code" name="coupon_code">
<!--                               value="--><?php //echo $this->input->get('coupon_code'); ?><!--"/>-->
                        <input type="text" placeholder="Mobile No" name="mobile_no">
<!--                               value="--><?php //echo $this->input->get('mobile_no'); ?><!--"/>-->
                        <input type="text" placeholder="Username" name="username">
<!--                               value="--><?php //echo $this->input->get('username'); ?><!--"/>-->


                        <span class="">

                              <button class="btn btn-primary" type="submit">Search</button></span>
                    </form>


                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th> Coupon</th>
                                <th> User</th>
                                <th> Assigned User</th>
                                <th> Edit</th>
                                <th> Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($coupons) > 0) {
                                foreach ($coupons as $index => $coupon) {
                                    ?>
                                    <tr>

                                        <?php
//                                        echo '<pre>';
//                                        print_r($coupon);
//                                        echo '</pre>';

                                        ?>

                                        <td> <?= $index + 1 ?></td>
                                        <td title="<?= $coupon->coupon_code ?>"> <?= $coupon->coupon_code ?></td>
                                        <td title="<?= $coupon->examuser_name ?>"> <?= $coupon->examuser_name ?> (<?= $coupon->examuser_mobile ?>) </td>
                                        <td title="<?= $coupon->assignee_name ?>"> <?= $coupon->assignee_name ?> (<?= $coupon->examuser_mobile ?>) </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">
                                                <a class="green"
                                                   href="<?php echo base_url(); ?>coupons/EditAssignedCouponsToUsers/<?= $coupon->acu_id ?>"
                                                   style="padding: 14px;">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="hidden-sm hidden-xs action-buttons">
                                                <a class="red trash"
                                                   href="<?php echo base_url(); ?>coupons/DeleteAssignedCoupons/<?= $coupon->acu_id ?>"
                                                   style="padding: 14px;">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="5" style="text-align:center">No Records Found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
</section>