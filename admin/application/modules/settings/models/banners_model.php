<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$this->db->order_by('banner_position','desc');
		$query = $this->db->get(DB_PREFIX.'mobile_banners');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}
	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'mobile_banners',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'mobile_banners', $data);
	    return $this->db->affected_rows();
	}


	public function getBannerDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'mobile_banners');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changebannerStatus($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'mobile_banners', $data);
	    return $this->db->affected_rows();
	}
	public function deletebanner($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'mobile_banners');
	}
}

/* End of file banners_model.php */
/* Location: ./application/models/banners_model.php */