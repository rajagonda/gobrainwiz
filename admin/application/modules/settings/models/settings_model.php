<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'settings');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function getSeoSettinigs(){
		$query = $this->db->get(DB_PREFIX.'seo');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	function update($tableName, $data, $whereId =NULL, $id=NULL){
		if($id !=NULL) {
	        $this->db->where($whereId, $id);
	        $this->db->update(DB_PREFIX.$tableName, $data);
	        return $this->db->affected_rows();
		}else {
			$this->db->insert($tableName,$data);
			return $this->db->insert_id();
		}

	}

}

/* End of file settings_model.php */
/* Location: ./application/models/settings_model.php */