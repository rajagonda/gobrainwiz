<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>/saveConfig" role="form" name="settings_from" id="settings_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('banner_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('site_name'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('site_name'); ?>" placeholder="<?php echo lang('site_name'); ?>" name="site_name" id="site_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('contact_email'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Contact_Mail'); ?>" placeholder="<?php echo lang('contact_email'); ?>" id="Contact_Mail" name="Contact_Mail" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('contact_number'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('contact_number'); ?>" placeholder="<?php echo lang('contact_number'); ?>" id="contact_number" name="contact_number" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Home_Banners'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Home_banners'); ?>" placeholder="<?php echo lang('Home_Banners'); ?>" id="Home_banners" name="Home_banners" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Fax'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Fax'); ?>" placeholder="<?php echo lang('Fax'); ?>" id="Fax" name="Fax" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Powered_by'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Powered_by'); ?>" placeholder="<?php echo lang('Powered_by'); ?>" id="Powered_by" name="Powered_by" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Rights_Reserved'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Rights_Reserved'); ?>" placeholder="<?php echo lang('Rights_Reserved'); ?>" id="Rights_Reserved" name="Rights_Reserved" class="form-control">
                                        </div>
                                       
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('banner_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                               
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('address'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Address'); ?>" placeholder="<?php echo lang('address'); ?>" name="Address" id="Address" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('City'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('City'); ?>" placeholder="<?php echo lang('City'); ?>" id="City" name="City" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('State'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('State'); ?>" placeholder="<?php echo lang('State'); ?>" id="State" name="State" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Country'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Country'); ?>" placeholder="<?php echo lang('Country'); ?>" id="Country" name="Country" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('Zip_Code'); ?></label>
                                            <input type="text" value="<?php echo $this->config->item('Zip_Code'); ?>" placeholder="<?php echo lang('Zip_Code'); ?>" id="Zip_Code" name="Zip_Code" class="form-control">
                                        </div>
                                      
                                       
                                       
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
