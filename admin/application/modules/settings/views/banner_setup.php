<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('banner_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="banner_from" id="banner_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                          <div class="form-group">
                                            <label>Banner Type:</label>
                                            <select id="banner_type" name="banner_type" class="form-control">
                                                <option value=''>Please Select Banner Type</option>
                                               
                                                <option value="h" <?php if($action=='edit')  if($banner->banner_type == 'h') echo "selected";  ?>>Header Slider</option>
                                                <option value="p" <?php if($action=='edit')  if($banner->banner_type == 'p') echo "selected";  ?>>Pramotional Ads</option>
                                               
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('banner_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->banner_name; ?>" placeholder="<?php echo lang('banner_name'); ?>" name="banner_name" id="banner_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('banner_position'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->banner_position; ?>" placeholder="<?php echo lang('banner_position'); ?>" id="banner_position" name="banner_position" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile"><?php echo lang('banner_image'); ?></label>
                                            <input type="file" name="banner_image" id="banner_image">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail1">Banner Title</label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->banner_title; ?>" placeholder="Banner Title" name="banner_title" id="banner_title" class="form-control">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail1">Banner Description</label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->banner_desc; ?>" placeholder="Banner Description" name="banner_desc" id="banner_desc" class="form-control">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail1">Banner Link</label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->banner_link; ?>" placeholder="Banner Link" name="banner_link" id="banner_link" class="form-control">
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail1">Button Title</label>
                                            <input type="text" value="<?php if($action=='edit') echo $banner->btn_title; ?>" placeholder="Banner Title" name="btn_title" id="btn_title" class="form-control">
                                        </div>
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
