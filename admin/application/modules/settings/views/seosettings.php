<div class="row">
            <div class="col-md-8 col-md-offset-2">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                        <?php
                        $url = current_url();
                        ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="settings_from" id="settings_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('seo_edit');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('seo_name'); ?></label>
                                            <input type="text" value="<?php echo $seosettings->seo_name; ?>" placeholder="<?php echo lang('seo_name'); ?>" name="seo_name" id="seo_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><?php echo lang('seo_title'); ?></label>
                                            <input type="text" value="<?php echo $seosettings->seo_title; ?>" placeholder="<?php echo lang('seo_title'); ?>" id="seo_title" name="seo_title" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('seo_desc'); ?></label>
                                            <textarea placeholder="Enter ..." rows="3" name="seo_description" id="seo_description" class="form-control"><?php echo $seosettings->seo_description; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('seo_keywords'); ?></label>
                                            <textarea placeholder="Enter ..." rows="3" name="seo_keywords" id="seo_keywords" class="form-control"><?php echo $seosettings->seo_keywords; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('seo_analytics'); ?></label>
                                            <textarea placeholder="Enter ..." rows="3" name="analytics" id="analytics" class="form-control"><?php echo $seosettings->analytics; ?></textarea>
                                        </div>
                                        
                                        
                                         <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                       
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
