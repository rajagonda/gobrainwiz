<section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_banners');?></h3>
                            
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th style="text-align:center !important;"><?php echo lang('banner_name');?></th>
                                          <th style="width: 40px">Banner Type</th>
                                        <th style="width: 40px"><?php echo lang('banner_position');?></th>
                                      
                                        <th style="text-align:center !important;"><?php echo lang('status');?></th>
                                        <th>
                                        <a class="blue" href="<?php echo base_url();?>settings/banners/save">
                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>
                                        </a>
                                        </th>
                                        </tr>
                                      <?php
                                      if($banners !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($banners as  $value) {
                                      ?>
                                        <tr id='banner_<?php echo $value->id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->banner_name;?></td>
                                         <td>

                                            <?php if($value->banner_type == 'h') { 
                                                echo "Header";
                                            } else { 
                                                echo "Promotional";}?></td>
                                        <td><?php echo $value->banner_position;?></td>
                                        <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->banner_status == 'n') {
                                        ?>
                                        
                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->id;?>', '<?php echo $value->banner_status;?>')">
                                        <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                        <?php } else { ?>
                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->id;?>', '<?php echo $value->banner_status;?>')">
                                        <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                        <?php } ?> 
                                        </td>
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url();?>settings/banners/save/<?php echo $value->id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        |
                                        <a class="red trash" id="<?php echo $value->id;?>" href="#" >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>


<script type="text/javascript">

function chk1( url )  {
    if( confirm('Are you sure you want to delete this row?') ) {
    return true;
    }else {
        return false;
    }
}

 $(".trash ").click(function() {
        if(chk1()){
         var del_id= $(this).attr('id');
          $.ajax({
          type: "POST",
          data: "id="+ del_id,
          url: '<?php echo base_url();?>settings/banners/delete',
          success: function(msg) {
            $("#banner_"+del_id).hide();
          }
        });
       
        }
        return false;
      });


    /**
     * By using this method change role status
     * 
     * @param {integer} id
     * @param {integer} roleId
     * @param {string} status
     * @returns string
     */
 function changeStatus(id, banner_id,  status) { 
     var p_url= "<?php echo base_url(); ?>settings/banners/changebannerStatus";
     var ajaxLoading = false;
     if(!ajaxLoading) {
     var ajaxLoading = true;
     $('#'+id).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');
     jQuery.ajax({
     type: "POST",             
     url: p_url,
     data: 'banner_id='+banner_id+'&status='+status,
     success: function(data) {
      if(data == 1){
        $('#'+id).html('<a class="green" href="#" onclick="changeStatus(\''+id+'\', \''+banner_id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');
       
      } else {
        $('#'+id).html('<a class="red" href="#" onclick="changeStatus(\''+id+'\', \''+banner_id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');
         
      }
     ajaxLoading = false;
         }
         
     });  
        
    }
     }
     
  
</script>