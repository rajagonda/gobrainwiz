<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| settings_lang.php
|--------------------------------------------------------------------------
*/

/**
*Banners 
**/
$lang['manage_banners'] = 'Manage Banners';
$lang['banner_add'] = 'Add Banner';
$lang['banner_edit'] = "Edit Banner";
$lang['banner_name'] = "Banner Name";
$lang['banner_position'] = "Position";
$lang['banner_image'] = "Banner Image";

/**
*
* Seo config
*
**/

$lang['manage_seo'] = 'Manage Seo';
$lang['seo_edit'] = "Edit Seo Settings";
$lang['seo_name'] = "Seo Name";
$lang['seo_title'] = "Seo Title";
$lang['seo_desc'] = "Seo Descritpion";
$lang['seo_keywords'] = "Seo Keywords";
$lang['seo_analytics'] = "Analytics ";


/* End of file volunteer_lang.php */
/* Location: ./application/module_core/banners/language/english/settings_lang.php.php */
