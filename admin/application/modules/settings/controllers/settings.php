<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Settings
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Settings extends  MY_Controller {

	public function __construct()
	{
		parent::__construct();
     if(!$this->authentication->checklogin()){
          redirect('login');
        }
		$this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('Siteconfig','settings_model'));
        $this->load->language('settings');
      
	}

	public function index() {
		$breadcrumbarray = array('label'=> "Settings",
                           'link' => base_url()."settings"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Settings");
	  $this->template->load_view1('settings');
	}
	public function saveConfig(){
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          $update_data=array(
          'site_name'=>$this->input->post('site_name'),
          'Contact_Mail'=>$this->input->post('Contact_Mail'),
          'contact_number'=>$this->input->post('contact_number'),
          'Home_banners'=>$this->input->post('Home_banners') ,
          'Address'=>$this->input->post('Address') ,
          'City'=>$this->input->post('City') ,
          'State'=>$this->input->post('State') ,
          'Country'=>$this->input->post('Country') ,
          'Zip_Code'=>$this->input->post('Zip_Code') ,
          'Fax'=>$this->input->post('Fax') ,
          'Powered_by'=>$this->input->post('Powered_by') ,
          'Rights_Reserved'=>$this->input->post('Rights_Reserved') 
           );
          $success = $this->Siteconfig->update_config($update_data);
          redirect(base_url()."settings");
        }
  }

  public function seoupdate(){
    $breadcrumbarray = array('label'=> "Seo Settinigs",
                           'link' => base_url()."settings/seoupdate"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Seo Settings");
         
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
           $update_data=array(
          'seo_title'=>$this->input->post('seo_title'),
          'seo_name'=>$this->input->post('seo_name'),
          'seo_description'=>$this->input->post('seo_description'),
          'seo_keywords'=>$this->input->post('seo_keywords') ,
          'analytics'=>$this->input->post('analytics')
          );
          $success = $this->settings_model->update('seo',$update_data,'seo_id','1');
          redirect(base_url()."settings/seoupdate");
        }

        $data['seosettings'] = $this->settings_model->getSeoSettinigs();
        $this->template->load_view1('seosettings.php',$data);
  }



}

/* End of file settings.php */
/* Location: ./application/controllers/settings.php */