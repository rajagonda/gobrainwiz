<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Banners
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Banners extends MY_Controller {

	public function __construct() {
		parent::__construct();
     if(!$this->authentication->checklogin()){
          redirect('login');
        }
	    $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('banners_model');
        $this->load->language('settings');
        $this->load->config('banners');
	}
	public function index() {
            $breadcrumbarray = array('label'=> "Banners",
                           'link' => base_url()."settings/banners"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Banners");
	    $data['banners'] = $this->banners_model->get();
	    $this->template->load_view1('manage_banners',$data);

	}
	public function save($id=null){
        $breadcrumbarray = array('label'=> "Banners",
                           'link' => base_url()."settings/banners"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Banners");
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('banner_from',{$json_rules});
</script>
JS;

           if($id !=''){
            $data['action'] = "edit";
            $data['banner'] = $this->banners_model->getBannerDetails($id);
//            echo "<pre>"; print_r($data);exit;
           }else {
            $data['action'] = "add";
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          if ($this->form_validation->run() == false)  {
            
            if($_FILES['banner_image']['name'] !='') {
            
            $filename = time()."_".$_FILES['banner_image']['name'];
            $config['upload_path'] = '../upload/banners/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('banner_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            $banner_image = $this->data['file_name'];
          }else {
            $banner_image = '';
            }
            $banner_values['banner_name'] = $this->input->post('banner_name');
            $banner_values['banner_type'] = $this->input->post('banner_type');
            if($this->input->post('banner_position') !=''){
            $banner_values['banner_position'] = $this->input->post('banner_position');    
            }else {
            $banner_values['banner_position'] = '0';
            }
            $banner_values['banner_location'] = $banner_image;
			$banner_values['banner_title'] = $this->input->post('banner_title');
			$banner_values['banner_desc'] = $this->input->post('banner_desc');
			$banner_values['banner_link'] = $this->input->post('banner_link');
			$banner_values['btn_title'] = $this->input->post('btn_title');
			
            
            if($id !=''){
             $res = $this->banners_model->update($banner_values,$id);
            }else {
              $res = $this->banners_model->save($banner_values);

            }
             redirect('settings/banners');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('banner_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'banner_name','label' => lang('banner_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                 array('field' => 'banner_type','label' => "Banner Type",'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'banner_image','label' => lang('banner_image'),'rules' => 'required'));
        return $rules;
    }
    public function changebannerStatus(){
        $id = $_POST['banner_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['banner_status'] = 'n';
            echo "0";
        } else {
            $formvalues['banner_status'] = 'y';
            echo "1";
        }
        $this->banners_model->changebannerStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->banners_model->deletebanner($id);

    }

}

/* End of file banners.php */
/* Location: ./application/controllers/banners.php */