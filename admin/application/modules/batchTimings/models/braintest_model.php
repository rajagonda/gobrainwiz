<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/** * Author : Avijit 
* Project : Brainwizz 
* Company : renegade it solutions 
* Version v1.0 * Model : practicetest 
* mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com 
* Phone : 9591510490 
* Website : phpguidance.com 
*/
class braintest_Model extends CI_Model {
	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'batch_timings');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}

	public function getBatchDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'batch_timings');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'batch_timings',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'batch_timings', $data);
	    return $this->db->affected_rows();
	}
	public function changeTestStatus($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'batch_timings', $data);
            //echo $this->db->last_query();
	    return $this->db->affected_rows();
	}

	public function getNotStudents(){
        $sql = "SELECT examuser_id,notification_token FROM gk_examuserslist WHERE notification_token !=''";
         $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }

	public function deleteBatch($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'batch_timings');
	}
	 
	
}
	/* End of file braintest_model.php *//* Location: ./application/models/braintest_model.php */
