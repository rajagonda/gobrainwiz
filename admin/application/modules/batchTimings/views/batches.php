<style type="text/css">

.green {
    color: #69aa46 !important;
}
.red{
  color:red !important;
}  
.bootstrap-timepicker-widget table td input{
  width: 100% !important;
}
</style>
<section class="content">

                    <div class="row">

                        <div class="col-md-12">

                           <div class="box">

                           <div class="box-header">

                           <h3 class="box-title">Batch Timings</h3>

                            </div><!-- /.box-header -->

                            <div class="box-body">

                            <table class="table table-bordered table-striped" id="tableList">
                                    <thead>
                                      <tr>

                                        <th style="width: 10px">Sno</th>

                                        <th >Title</th>

                                        <th >Description</th>

                                        <th >Date</th>
                                        <th >Time</th>

                                        <th >Status</th>


                                        <th>

                                        <a class="blue" href="<?php echo base_url();?>batchTimings/batchsave">

                                        <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;;"></span>

                                        </a>

                                        </th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                      <?php

                                      if($batches !=''){

                                        $i=1;

                                        $j=1;

                                      foreach ($batches as  $value) {

                                      ?>

                                        <tr class="com_<?php echo $value->id;?>" id='<?php echo $value->id;?>'>

                                        <td><?php echo $i;?></td>

                                        <td ><?php echo $value->title;?></td>

                                        <td ><?php echo $value->message;?></td>

                                       
                                        <td><?php if(!empty($value->batch_date)){ ?>
                                            <?php echo date('Y-m-d',  strtotime($value->batch_date)); ?>
                                       <?php }else{ ?>
                                            -
                                       <?php } ?></td>
                                       <td><?php if(!empty($value->batch_time)){ ?>
                                            <?php echo $value->batch_time; ?>
                                       <?php }else{ ?>
                                            -
                                       <?php } ?></td>
                                        

                                        

                                        <td align="center" id="status<?php echo $j;?>">

                                         <?php

                                        if($value->batch_status == 'n') {

                                        ?>

                                        

                                        <a class="red" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->id;?>', '<?php echo $value->batch_status;?>')">

                                        <i class="ace-icon fa fa-close bigger-130"></i>

                                        </a>


                                        <?php } else { ?>

                                        <a class="green" href="#" onclick="changeStatus('status<?php echo $j;?>', '<?php echo $value->id;?>', '<?php echo $value->batch_status;?>')">

                                        <i class="ace-icon fa fa-check bigger-130"></i>

                                        </a>

                                        <?php } ?> 
                                        
                                        </td>

                                        <td>

                                        <div class="hidden-sm hidden-xs action-buttons">

                                        <a class="green" href="<?php echo base_url();?>batchTimings/batchsave/<?php echo $value->id;?>" >

                                        <i class="ace-icon fa fa-pencil bigger-130"></i>

                                        </a>

                                        |

                                        <a class="red trash" id="<?php echo $value->id;?>" href="#" >

                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>

                                        </a>

                                        </div>

                                        </td>
                                        
                                        </tr>

                                      <?php

                                        $i++;

                                        $j++;

                                       }

                                       }

                                      ?>

                                    </tbody></table>

                                </div><!-- /.box-body -->

                                <!-- <div class="box-footer clearfix">

                                    <ul class="pagination pagination-sm no-margin pull-right">

                                        <li><a href="#">«</a></li>

                                        <li><a href="#">1</a></li>

                                        <li><a href="#">2</a></li>

                                        <li><a href="#">3</a></li>

                                        <li><a href="#">»</a></li>

                                    </ul>

                                </div>

                            </div>--> 



                           

                        </div><!-- /.col -->

                       

                    </div><!-- /.row -->

                    

                </section>

<script type="text/javascript">
	
	function changeStatus(id1, id,  status) { 

     var p_url= "<?php echo base_url(); ?>batchTimings/changeTestStatus";

     var ajaxLoading = false;

     if(!ajaxLoading) {

     var ajaxLoading = true;

     $('#'+id1).html('<img src="<?php echo base_url();?>assets_new/img/loading_small.gif">');

     jQuery.ajax({

     type: "POST",             

     url: p_url,

     data: 'id='+id+'&status='+status,

     success: function(data) {

      if(data == 1){

        $('#'+id1).html('<a class="green" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'y\')"> <i class="ace-icon fa fa-check bigger-130"></i></a>');

       

      } else {

        $('#'+id1).html('<a class="red" href="#" onclick="changeStatus(\''+id1+'\', \''+id+'\', \'n\')"><i class="ace-icon fa fa-close bigger-130"></i></a>');

         

      }

     ajaxLoading = false;

         }

         

     });  

        

    }

     }

     function chk1( url )  {

    if( confirm('Are you sure you want to delete this row?') ) {

    return true;

    }else {

        return false;

    }

}



 $(".trash ").click(function() {

        if(chk1()){

         var del_id= $(this).attr('id');

          $.ajax({

          type: "POST",

          data: "id="+ del_id,

          url: '<?php echo base_url();?>batchTimings/deletebatch',

          success: function(msg) {

            $(".com_"+del_id).hide();

          }

        });

       

        }

        return false;

      });

</script>

