<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Batch Timings</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="batch_form" id="batch_form" enctype="multipart/form-data">
                                    <div class="box-body">
                                         
                                        
                                         <div class="form-group">
                                            <label>Name :</label>
                                            <input type="text" placeholder="Name" value="<?php if($action == 'edit') echo $batch->title; ?>" name="title" id="title" class="form-control">
                                        </div>
                                        

                                        <div class="form-group">
                                       <label>Description :</label>
                                        <textarea placeholder="Description"  cols="" name="message"class="form-control"><?php if($action=='edit') echo $batch->message; ?></textarea>

                                      
                                        </div>

                                        <div class="form-group">

                                        <label>Batch Date:</label>

                                        <div class="input-group">

                                            <div class="input-group-addon">

                                                <i class="fa fa-calendar"></i>

                                            </div>

                                            <input type="text" id='datepicker' name='batch_date' value="<?php if($action=='edit') echo date('m-d-Y',  strtotime($batch->batch_date)); ?>" class="form-control" readonly=""  />

                                        </div><!-- /.input group -->

                                    </div>

                                    <div class="bootstrap-timepicker">

                                        <div class="form-group">

                                            <label>Batch Timing:</label>

                                            <div class="input-group">

                                                <input type="text" name="batch_time" id="from" value="<?php if($action=='edit') echo $batch->batch_time; ?>" class="form-control timepicker"/>

                                                <div class="input-group-addon">

                                                    <i class="fa fa-clock-o"></i>

                                                </div>

                                            </div><!-- /.input group -->

                                        </div><!-- /.form group -->

                                    </div>
                                    
                                    
                                        <div class="form-group">
                                       <label>Notification Image :</label>
                                        <input type="file" name="image" id="image">
                                      

                                      
                                        </div>
					<div class="form-group">
                                       <label>Background Image :</label>
                                        <input type="file" name="image2" id="image2">
                                      

                                      
                                        </div>                                        
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>


<script src="<?php echo base_url();?>assets_new/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script>


$(function() {

    $(".timepicker").timepicker({

                    showInputs: false

                });

$( "#datepicker" ).datepicker();
$( "#datepicker1" ).datepicker();

});

</script>
