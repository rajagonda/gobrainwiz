<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Subject
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class batchTimings extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('braintest_Model');
        //$this->load->language('subjects');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "BatchTimings",
                            'link' => base_url()."batchTimings"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Batch");
           $data['batches'] = $this->braintest_Model->get();
           $this->template->load_view1('batches', $data);
	}

public function _testrules(){

         $rules = array(

                 array('field' => 'title','label' => 'title','rules' => 'trim|required|xss_clean|max_length[250]')
                 );

        return $rules;

    }

  public function batchsave($id=null){        

        $breadcrumbarray = array('label'=> "BatchTimings",

                           'link' => base_url()."batchTimings"

                           );

        $link = breadcrumb($breadcrumbarray);

        $this->template->set_breadcrumb($link);

        

       // print_r($data);exit;

        $validationRules = $this->_testrules();

        foreach ($validationRules as $form_field)   {

        $rules[] = array(

        'name' => $form_field['field'],

        'display' => $form_field['label'],

        'rules' => $form_field['rules'],

        );

        }



      

$json_rules = json_encode($rules);

$script = <<< JS

<script>

var CIS = CIS || { Script: { queue: [] } };

CIS.Form.validation('batch_form',{$json_rules});

</script>

JS;



        if($id !=''){

            $data['action'] = "edit";

            $data['batch'] = $this->braintest_Model->getBatchDetails($id);

            // $data['topics'] = $this->practicetest_Model->getcattopics($data['test']->subcat_id);

            // $this->template->set_subpagetitle("Edit Passage");

        }else {

            $data['action'] = "add";

           // $this->template->set_subpagetitle("Add Passage");

        }

        $this->form_validation->set_rules($validationRules);

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

        if ($this->form_validation->run() == true)  {



        $form_values['batch_date'] =  date('Y-m-d',  strtotime($this->input->post('batch_date')));

        $form_values['title'] =  $this->input->post('title');
        $form_values['batch_time'] =  $this->input->post('batch_time');

        $form_values['message'] = $this->input->post('message');


       // echo "<pre>"; print_r($form_values);exit;
        $filename = '';
	$bfilename = '';
        if($_FILES['image']['name'] !='') {
            
            $filename = time()."_".$_FILES['image']['name'];
            $config['upload_path'] = '../upload/notification/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
             $form_values['image'] = $update_image;
           }

	if($_FILES['image2']['name'] !='') {
            
            $bfilename = time()."_".$_FILES['image2']['name'];
            $config['upload_path'] = '../upload/notification/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $bfilename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('image2')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image1 = $this->data['file_name'];
             $form_values['backgroundimage'] = $update_image1;
           }        

        

        if($id !=''){

        $res = $this->braintest_Model->update($form_values,$id);
        $testId = $id;

        }else {

        $res = $this->braintest_Model->save($form_values);
       

        }
//echo $this->input->post('topic_id');exit;
          

      

                    

        redirect('batchTimings');

        }         

        }

        $data['script'] = $script;

        $this->template->load_view1('batch_setup',$data);

    }

    public function changeTestStatus(){

        $id = $_POST['id'];

        $staus = $_POST['status'];

        if($staus == 'y') {

        $formvalues['batch_status'] = 'n';

        echo "0";

        } else {

        $formvalues['batch_status'] = 'y';

        echo "1";

        }

   if($formvalues['batch_status'] == 'y')
        {
        $batchDetails = $this->braintest_Model->getBatchDetails($id);


        $cuur_time = date('Y-m-d H:i:s');

        $notdata['title'] = "New Batch Uploaded";
        $notdata['message'] = "New Batch Released";
        $notdata['notify_type'] = 5;
       // $notdata['catid'] = '';                 
      //  $notdata['topicid'] = $testDetails->topic_id;
      //  $notdata['subcatid'] = $id;
        $notdata['time_update'] = $cuur_time;

        $nid = $this->common_model->insertSingle('gk_notifications',$notdata);



        $stokens = array();
        $studnetdata['students']= $this->braintest_Model->getNotStudents();
        foreach ($studnetdata['students'] as  $value) {

        $notlog['student_id'] = $value->examuser_id;
        $notlog['notification_id'] =$nid;
        $notlog['checked'] = '0';

        $nlid = $this->common_model->insertSingle('gk_notifications_log',$notlog);

        array_push($stokens, $value->notification_token);
        }

       // $testRes = $this->practicetest_Model->getTopicAndCatName($testDetails->topic_id);

        $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
        $field1 -> message = $batchDetails->message;
        $field1 -> title = $batchDetails->title;
        $field1 -> image = '';
        if(!empty($batchDetails->image))
        {
             $field1 -> image = 'http://gobrainwiz.in/upload/notification/'.$batchDetails->image;
        }

	$field1 -> background_image = '';
        if(!empty($batchDetails->backgroundimage))
        {
             $field1 -> background_image = 'http://gobrainwiz.in/upload/notification/'.$batchDetails->backgroundimage;
        }

        $field1 -> type =  '5';
        $field1 -> batch_date = $batchDetails->batch_date; 
        $field1 -> batch_time = $batchDetails->batch_time; 
         //var_dump($field1);die();
         
        $fields = array(
             'registration_ids' => $stokens,
             'data' => $field1
            );
    $headers = array(
      'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',
      'Content-Type: application/json'
      );
     $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
     }
        $this->braintest_Model->changeTestStatus($formvalues, $id);

    }

public function deletebatch(){

        $id =  $this->input->post('id');

        $this->braintest_Model->deleteBatch($id);

    } 









	/*public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Subjects",
                           'link' => base_url()."subjects"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('subject_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['subject'] = $this->Subjects_Model->getsubjectDetails($id);
            $this->template->set_subpagetitle("Edit Subject");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Subject");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['subject_name'] = $this->input->post('subject_name');
            $form_values['subject_desc'] = $this->input->post('subject_desc');    

            if($id !=''){
             $res = $this->Subjects_Model->update($form_values,$id);
            }else {
              $res = $this->Subjects_Model->save($form_values);

            }
             redirect('subjects');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('subjects_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'subject_name','label' => lang('subject_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'subject_desc','label' => lang('subject_desc'),'rules' => 'required'));
        return $rules;
    }

	 public function changesubjectStatus(){
        $id = $_POST['subject_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['subject_status'] = 'n';
            echo "0";
        } else {
            $formvalues['subject_status'] = 'y';
            echo "1";
        }
        $this->Subjects_Model->changesubjectStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Subjects_Model->deleterole($id);

    }*/

}

