<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : userquestions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| userquestions
|--------------------------------------------------------------------------
*/


$lang['manage_questions'] = 'Manage User Questions';
$lang['member_name'] = "Member  Name";
$lang['question'] = 'Question';
$lang['date'] = "Date";
$lang['view'] = "View";
$lang['replay_status'] = "Replay Status";
$lang['home_page'] = "Home Page";
$lang['actions'] = 'Actions';









/* End of file members_lang.php */
/* Location: ./application/module_core/members/language/english/members_lang.php */
