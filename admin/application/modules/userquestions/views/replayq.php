<div class="wrapper">
    <?php
        if($this->session->userdata('replay_success')){
        ?>
       
        <div class="nNote nSuccess hideit">
            <p><strong>SUCCESS: </strong>
                <?php 
                echo $this->session->userdata('replay_success');
                $this->session->set_userdata('replay_success', "");
                ?></p>
        </div> 
        
        <?php 
        } 
        ?>
        
        	<fieldset>
                <div class="widget">
                    <div class="title">
                        <h6>Replay  Question Details</h6></div>
                    
                    <div class="formRow">
                        <label>Question Explanation:<span class="req">*</span></label>
                        <div class="formRight"><?php echo $questionsview->question_explanation; ?></div><div class="clear"></div>
                    </div>
                  
                     <div class="formSubmit">
                      
                        <button class="greenB" id="showreplaybox"  type="button">Replay</button>
                    </div>
                    <div class="clear"></div>
                </div>
                
            </fieldset>
       
           </div>
<div id="replay" style="display:none">
<div class="wrapper">
<form id="regusterform" class="form" method="post" action="<?php echo base_url(); ?>admin/userquestions/submitreplay">
        	<fieldset>
                <div class="widget">
                    <div class="title">
                        <h6>Question Replay Form</h6></div>
                    
                    <div class="formRow">
                        <label>Question Replay:</label>
                        <div class="formRight"><textarea  rows="20" cols="" name="question_replay"></textarea></div>
                        <div class="clear"></div>
                    </div>
                  
                    

                <div class="formSubmit">
                    <input type="hidden" name="qid" value="<?php echo $questionsview->question_id;?>">
                   <input type="submit" value="submit" class="greenB" /><button class="redB" id="cancel" type="button">Back</button>
                    </div>
                    <div class="clear"></div>
                </div>
                
            </fieldset>
        </form>
           </div>
</div>
     <!-- END Main Content -->
    <script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    
    $('#showreplaybox').click(function() {
        $("#replay").show();
    });

});
</script>
     <!-- END Main Content -->
    <script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
$('#cancel').click(function() {
   
   if(confirm("Are you sure you want to navigate away from this page?"))
   {
      history.go(-1);
   }        
   return false;
});
});
</script>