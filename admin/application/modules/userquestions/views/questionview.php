<div class="row">
<div class="col-md-8 col-md-offset-2">
            <!-- col-md-offset-2 -->
<section class="content">
 <div class="row">
                               
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">View Question Details</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Member Name :</label>
                                           <?php echo $questionsview->member_name; ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Member Email :</label>
                                          <?php echo $questionsview->member_email; ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Question :</label>
                                        <?php echo $questionsview->question_name; ?>
                                        </div>
                                         <div class="form-group">
                                            <label>Question Post Date :</label>
                                        <?php echo $questionsview->question_date; ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Question Ask By :</label>
                                          <?php echo $questionsview->question_ask; ?>
                                        </div>
                                       <div class="form-group">
                                            <label>Question Explanation :</label>
                                         <?php echo $questionsview->question_explanation; ?>
                                        </div>
                                         <div class="form-group">
                                            <label>Question Replay:</label>
                                            <?php
                                        if($questionsview->question_replay == 'n') {
                                        ?>
                                        <i class="red ace-icon fa fa-close bigger-130"></i>
                                        
                                        <?php } else { ?>
                                        
                                        <i class="green ace-icon fa fa-check bigger-130"></i>
                                        
                                        <?php } ?> 
                                        </div>
                                         
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                       
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>