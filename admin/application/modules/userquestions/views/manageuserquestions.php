 <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                           <div class="box">
                           <div class="box-header">
                           <h3 class="box-title"><?php echo lang('manage_questions');?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <th style="width: 10px"><?php echo lang('Sno');?></th>
                                        <th style="text-align:center !important;" ><?php echo lang('member_name');?></th>
                                        <th style="text-align:center !important;"><?php echo lang('question');?></th>
                                        <th style="text-align:center !important;" ><?php echo lang('date');?></th>
                                        <th style="text-align:center !important;" ><?php echo lang('view');?></th>
                                        <th style="text-align:center !important;" ><?php echo lang('replay_status');?></th>
                                        <th style="text-align:center !important;" ><?php echo lang('actions');?></th>
                                        </tr>
                                      <?php
                                      if($questionslist !=''){
                                        $i=1;
                                        $j=1;
                                      foreach ($questionslist as  $value) {
                                      ?>
                                        <tr id='question_<?php echo $value->question_id;?>'>
                                        <td><?php echo $i;?></td>
                                        <td align="center"><?php echo $value->member_name;?></td>
                                        <td align="center"><?php echo $value->question_name;?></td>
                                        <td align="center"><?php echo $value->question_date;?></td>
                                        <td>
                                        <a href="<?php echo base_url()?>userquestions/viewquetion/<?php echo $value->question_id;?>">
                                        <img src="<?php echo base_url();?>assets_new/img/zoom.png" alt="" />
                                        </a>
                                        </td>
                                        
                                        
                                        
                                          <td align="center" id="status<?php echo $j;?>">
                                         <?php
                                        if($value->question_replay == 'n') {
                                        ?>
                                        <i class="red ace-icon fa fa-close bigger-130"></i>
                                        
                                        <?php } else { ?>
                                        
                                        <i class="green ace-icon fa fa-check bigger-130"></i>
                                        
                                        <?php } ?> 
                                        </td>
                                        
                                      
                                       
                                        <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green" href="<?php echo base_url(); ?>userquestions/editquestion/<?php echo $value->question_id;?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                        |
                                        <a class="red"  onclick="return confirm('Are you sure delete?')" href="<?php echo base_url()?>userquestions/deletequestion/<?php echo $value->question_id; ?>"  >
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                        |
                                        <a class="green" href="<?php echo base_url()?>userquestions/replayquetion/<?php echo $value->question_id;?>" >
                                        <i class="ace-icon fa fa-reply bigger-130"></i>
                                        </a>
                                        </div>
                                        </td>
                                        </tr>
                                      <?php
                                        $i++;
                                        $j++;
                                       }
                                       }
                                      ?>
                                    </tbody></table>
                                </div><!-- /.box-body -->
                                <!-- <div class="box-footer clearfix">
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>--> 

                           
                        </div><!-- /.col -->
                       
                    </div><!-- /.row -->
                    
                </section>