<?php
class userquestions_Model extends CI_Model {
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();    
    }
    
    
    public function getAllquestions() {
        $this->db->select('gk_userquestions.*,gk_members.member_name,gk_members.member_email');
        $this->db->from('gk_userquestions', 'gk_members');
        $this->db->join('gk_members', 'gk_userquestions.member_id = gk_members.member_id');
       // $this->db->where('gk_members.member_paid', 'y');
        $result = $this->db->get();
        if($result->num_rows() > 0 ) {
            return $result->result();
        } else {
            return '';
        }
    }
    
     
    public function getquestiondetails($id) {
        $this->db->select('gk_userquestions.*,gk_members.member_name,gk_members.member_email');
        $this->db->from('gk_userquestions', 'gk_members');
        $this->db->join('gk_members', 'gk_userquestions.member_id = gk_members.member_id');
        $this->db->where('gk_userquestions.question_id', $id);
        $result = $this->db->get();
        if($result->num_rows() > 0 ) {
            return $result->row();
        } else {
            return '';
        }
    }

    
    
      public function updatequetion($id, $values) {
         $this->db->where('question_id', $id);
        $this->db->update('gk_userquestions', $values);
        return $this->db->affected_rows();
        
    }
     /**
     * 
     * @param integer $id
     */
    function deletequestion($id) {
        $this->db->where('question_id', $id);
        $this->db->delete('gk_userquestions');
    }
    
    
   function changequestionStatus($values, $id) {
        $this->db->where('question_id`', $id);
        $this->db->update('gk_userquestions', $values);
        return $this->db->affected_rows();
    }
    
    
    
}
   

?>
