<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : userquestions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class userquestions extends MY_Controller {
   
     public function __construct() {
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('userquestions_Model');
        $this->load->language('userquestions');        
     }
    
    /**
     * index
     */
     public function index() {
        $breadcrumbarray = array('label'=> "Manage User Questions",
                            'link' => base_url()."userquestions/"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage User Questions");
        $data['questionslist'] = $this->userquestions_Model->getAllquestions();
        $this->template->load_view1('manageuserquestions', $data);
    }
    /*
     * User add method
     */
    public function viewquetion($id) {
      $data['questionsview'] = $this->userquestions_Model->getquestiondetails($id);
      $this->template->load_view1('questionview', $data);
    }
   

     
     public function editquestion($id) {
          $data['questiondetails'] = $this->userquestions_Model->getquestiondetails($id);
          $this->template->load_view1('editquestion', $data);
     }
     
     public function updatequetion() {
        $formvalues['question_explanation'] = $this->input->post('question_explanation');
        
         $id = $this->input->post('qid');
         $lid = $this->userquestions_Model->updatequetion($id,$formvalues);
          if($lid !='') {
           $this->session->set_userdata('question_success', "Successfully Updated!!");
           redirect(base_url().'userquestions');
        } else {
             $this->session->set_userdata('question_filure', "Nothing To Process");
            redirect(base_url().'userquestions/editquestion/'.$id);
        }  
     }
     
       public function deletequestion($id) {
          $this->userquestions_Model->deletequestion($id);
          $this->session->set_userdata('question_success', "Successfully Deleted!!");
          redirect(base_url().'userquestions');
     }


    public function changequestionStatus() {
        $id = $_POST['planid'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['home_page'] = 'n';
            echo "0";
        } else {
            $formvalues['home_page'] = 'y';
            echo "1";
        }
        $this->userquestions_Model->changequestionStatus($formvalues, $id);
    }
    public function replayquetion($id) {
        $data['questionsview'] = $this->userquestions_Model->getquestiondetails($id);
      $this->template->load_view1('replayq', $data);
    }
    
    public function submitreplay() {
         $config = array();
            $config['useragent']           = "CodeIgniter";
            $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol']            = "smtp";
            $config['smtp_host']           = "localhost";
            $config['smtp_port']           = "25";
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;
            $this->load->library('email');
            $this->email->initialize($config);
            $qid = $this->input->post('qid');
            $replaydata = $this->input->post('question_replay');
            $qdetails = $this->userquestions_Model->getquestiondetails($qid);
            ob_start();	 
            ?>

 <!--mail template stsrt here-->
		<table border="0" cellspacing="2"  cellpadding="2" width="100%" bgcolor="#323232">
                    <tr>
                    <td bgcolor='#323232' colspan='6'><h3 style='color: #fff; font-family: ubuntuc; padding-left:15px;'>Quetion Replay</h3></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Question</td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $qdetails->question_explanation;?></td>
                    </tr>
                    <tr>
                    <td width='204' class='heading' bgcolor='#FFFFFF' >Answer </td>
                    <td width='342' class='heading' bgcolor='#FFFFFF'><?php echo $replaydata;?></td>
                    </tr>
                    
                </table>
                  <!--mail template end here-->
				    


	<?php	
	 $sendermessage  = ob_get_contents();
	 ob_end_clean(); 
            
            
            
          $email  = $qdetails->member_email;
          $this->email->from('admin@brainwizz.in','BRAINWIZZ');
          $this->email->to($email); 
          $this->email->subject('Quetion Replay Details');
          $this->email->message($sendermessage);	
          if($this->email->send()){
             // echo "ok"; exit;
          } else {
              //echo $this->email->print_debugger();exit;
          }
          
          //update replay status
          $formvalues['question_replay'] = 'y';
          $result = $this->userquestions_Model->updatequetion($qid,$formvalues);
          
          $this->session->set_userdata('replay_success', "Sucessfully Reply submitted!");
          redirect(base_url()."userquestions/replayquetion/".$qid);
        
    }
    
   
    
    



    
}
?>