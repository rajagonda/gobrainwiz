<?php
class resumes_Model extends CI_Model {
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();    
    }
    
    
    public function getAllresumes() {
        
         $sql = "SELECT gk_resumes.*, gk_members.member_name AS membername FROM gk_resumes INNER JOIN
               gk_members ON gk_members.member_id = gk_resumes.member_id  ORDER BY resume_id DESC";

        $query  =   $this->db->query($sql);                
        if($query->num_rows() > 0) {
            return $query->result();
        } else {
            return '';
        }
     
    }
    
    public function getresumedetails($id) {
       
        $this->db->where('resume_id', $id);
         $query = $this->db->get('gk_resumes');
	  if ($query->num_rows() > 0) {      
	    return $query->row();
         }
         else {
            return '';
	 } 
   
    }
            
    function deleteresume($id) {
        $this->db->where('resume_id', $id);
        $this->db->delete('gk_resumes');
    }
    
    
    
}
   

?>
