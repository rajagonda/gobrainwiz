<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : resumes
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class resumes extends MY_Controller {
    /**
     * Constructor
     */
       public function __construct() {
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->helper('download');
        $this->load->model('resumes_Model');
         $this->load->language('resumes');
    }
    
    /**
     * index
     */
    public function index() {
        $breadcrumbarray = array('label'=> "Manage Resumes",
                            'link' => base_url()."resumes/"
                              );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Manage Resumes");
        $data['resumeslist'] = $this->resumes_Model->getAllresumes();
        $this->template->load_view1('manageresumes', $data);
    }
    
    public function resumedownload($id) {
        $result  = $this->resumes_Model->getresumedetails($id);
        if($result !='') {
         $file_name =  $result->resume_path;  
         $path = "../upload/resumes/".$result->resume_path;     
         $data = file_get_contents($path); // Read the file's contents
	 $name = $file_name;
 	 force_download($name, $data);      
        }
    }

    



    public function deleteresume($id) {
          $result  = $this->resumes_Model->getresumedetails($id);
          if($result !='') {
            $path =  $path = "../upload/resumes/".$result->resume_path;  
            if (file_exists($path)) {
               unlink($path);
            }
              
             
          }
          $this->resumes_Model->deleteresume($id);
          $this->session->set_userdata('resume_success', "Successfully Deleted!!");
          redirect(base_url().'resumes');
     }





    
}
?>