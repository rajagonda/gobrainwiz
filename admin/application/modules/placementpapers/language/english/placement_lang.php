<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : placement
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| placement
|--------------------------------------------------------------------------
*/


$lang['manage_companies'] = 'Manage Companies';
$lang['company_add'] = 'Add Company';
$lang['company_edit'] = "Edit Company";
$lang['com_name'] = "Company  Name";
$lang['c_id'] = "Parent";



$lang['manage_papers'] = 'Manage papers';
$lang['paper_add'] = 'Add Paper';
$lang['paper_edit'] = "Edit Paper";
$lang['com_name1'] = "Paper Name";
$lang['c_id'] = "Company Name ";
$lang['add'] = "Add";
$lang['view'] = 'view';
$lang['question'] = 'Question';
$lang['questions'] = 'Questions';


$lang['manage_pdfs'] = 'Manage Pdfs';
$lang['pdf_add'] = 'Add Pdf';
$lang['pdf_edit'] = "Edit Pdf";
$lang['path'] = "Pdf File";
$lang['title'] = "Pdf Title";


$lang['manage_questions'] = 'Manage Questions';
$lang['question_add'] = 'Add Question';
$lang['question_edit'] = "Edit Question";
$lang['question_details'] = "Question Details";
$lang['option1'] = "option1";




/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
