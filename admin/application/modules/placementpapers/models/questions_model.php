<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Company
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class questions_model extends CI_Model {

	public $variable;

	public function __construct() {
	    parent::__construct();
		
	}
	public function getquetions($cid){
            
            $sql = "SELECT gk_companyqas_placement.*,cat.com_name as catname FROM gk_companyqas_placement
                    INNER JOIN gk_placement_comp as cat ON cat.comp_id = gk_companyqas_placement.cat_id                   
                     WHERE gk_companyqas_placement.cat_id='$cid'";
             
            
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
        }
        
        
        public function getquestiondetails($qid){
            
            $sql = "SELECT gk_companyqas_placement.*,cat.com_name as catname FROM gk_companyqas_placement
                    INNER JOIN gk_placement_comp as cat ON cat.comp_id = gk_companyqas_placement.cat_id
                    WHERE gk_companyqas_placement.id='$qid'";
             
            
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
        }
        
         public function getsubcategories($cid) {
         $this->db->where('c_id', $cid);
         $query = $this->db->get(DB_PREFIX.'companies');
	  if ($query->num_rows() > 0) {      
	    return $query->result();
         }
         else {
            return '';
	 } 
    }
	public function save($data){
		$this->db->insert(DB_PREFIX.'companyqas_placement',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas_placement', $data);
	    return $this->db->affected_rows();
	}
	public function getcomapnyDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'companypdf');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changequestionStatus($data, $id){
	    $this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'companyqas_placement', $data);
            return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'companyqas_placement');
	}
        
        public function getPapers(){
             $sql = "SELECT  cat.com_name , subcat.id as SubID ,subcat.status, subcat.com_name as SubCategory
              FROM gk_companies as cat inner join gk_companies as subcat
              on cat.id = subcat.c_id where cat.c_id =0";  
             //left outer join
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
        }


}

/* End of file companies_model.php */
/* Location: ./application/models/companies_model.php */