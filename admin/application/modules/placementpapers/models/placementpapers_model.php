<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Topper
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Topper_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'voice_toper');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'voice_toper',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('voice_id', $id);
	    $this->db->update(DB_PREFIX.'voice_toper', $data);
	    return $this->db->affected_rows();
	}
	public function gettopperDetails($id){
		$this->db->where('voice_id', $id);
		$query = $this->db->get(DB_PREFIX.'voice_toper');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changetopperStatus($data, $id){
	    $this->db->where('voice_id', $id);
	    $this->db->update(DB_PREFIX.'voice_toper', $data);
            return $this->db->affected_rows();
	}
	public function deletetopper($id){
		$this->db->where('voice_id', $id);
        $this->db->delete(DB_PREFIX.'voice_toper');
	}


}

/* End of file voice_toper_model.php */
/* Location: ./application/models/voice_toper_model.php */