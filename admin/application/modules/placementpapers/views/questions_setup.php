<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="#" role="form" name="question_from" id="question_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Category</label>
                                           <select name="c_id"  id="c_id" class="form-control">
                                <option value="">Select Company</option>
                              <?php
                                  foreach ($companies as $value)
                                  {?>                          
                                  <option value="<?php echo $value->comp_id;?>" <?php if($value->comp_id == $cid) echo "selected";  ?>><?php echo $value->com_name;?></option>;
                                  <?php  
                                  }
                                  ?>
                    </select>       
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Question</label>
                                            <textarea id="example" class="wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" name="question_name"><?php if($action != 'edit') { ?><p></p><?php } ?><?php if($action == 'edit') echo $details->question_name; ?></textarea>
                                        </div>
                                        
                                        
                                       
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                               
                                    <div class="box-body">
                                        
                                        
                                        
                                        
                                        <div class="form-group">
                                            <label>Explanation</label>
                                            
                                 <textarea id="example6" class="wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" name="question_explanation"><?php if($action != 'edit') { ?><p></p><?php } ?><?php if($action == 'edit') echo $details->question_explanation; ?></textarea>
                                        </div>
                                         
                                        
                                         
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>
<?php
//echo $script;
?><link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/tinymce4/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd5.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd4.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd3.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd2.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd1.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/abcd.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/js/wirislib.js"></script>

        
 <script type="text/javascript"> 
 $(document).ready(function () {
 $("#c_id").change(function (){
     var selectVal = $('#c_id :selected').val();
     $.ajax({
       url:'<?php echo base_url(); ?>placement/questions/getsubcategories',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
       var htmlString ='';
        htmlString+="<option value=''>Select Subcategory</option>"
       $.each(data,function(i){
             htmlString+="<option value="+data[i]['id']+">"+data[i]['com_name']+"</option>"
           });
       $("#sub_id").html(htmlString);
       }
});

 });
 
 
  
        
       
 
    });
 </script>
 
 <script type="text/javascript"> 
 $(document).ready(function () {
     
     $("#question_from").validate({
                ignore: [], 
        rules: {
            
            
                    question_name: {
                    required: function() 
                    {
                    CKEDITOR.instances.question_name.updateElement();
                    },
                    maxlength: 3000
                    },
                    c_id : {
                        required:true
                    },
                    sub_id : {
                        required:true
                    },
                        question_answer :{
                            required: true
                        },
                       
                        option1: {
                            required: true
                        },
                        option2: {
                            required: true
                        },
                        option3: {
                            required: true
                        },
        },
        
    });
     });

    </script>