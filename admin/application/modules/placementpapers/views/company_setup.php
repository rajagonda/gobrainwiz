<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('company_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topper_from" id="topper_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('com_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $company->com_name; ?>" placeholder="<?php echo lang('com_name'); ?>" name="com_name" id="com_name" class="form-control">
                                        </div>


                                        <div class="form-group">
                                            <label>Description Template</label>
                                            <textarea placeholder="Enter ..." rows="50" cols="50" name="description" id="description" class="form-control"><?php if($action=='edit') echo $company->com_desc; ?></textarea>
                                        </div>
                                        
                                    </div><!-- /.box-body -->


                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
 <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
  <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('description');
                              
                
            });
        </script>