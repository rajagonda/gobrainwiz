<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : topper
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class placementpapers extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('Topper_Model');
        $this->load->language('topper');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Toppers",
                            'link' => base_url()."topper"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Toppers");
           $data['toppers'] = $this->Topper_Model->get();
           $this->template->load_view1('topper', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Toppers",
                           'link' => base_url()."topper"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('topper_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['topper'] = $this->Topper_Model->gettopperDetails($id);
            $this->template->set_subpagetitle("Edit Topper");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Topper");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['student_name'] = $this->input->post('student_name');
            $form_values['voice_description'] = $this->input->post('voice_description');
            $form_values['company'] = $this->input->post('company');
            
            
            
            if($_FILES['voice_image']['name'] !='') {
            
            $filename = time()."_".$_FILES['voice_image']['name'];
            $config['upload_path'] = '../upload/topper/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('voice_image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            $voice_image = $this->data['file_name'];
          }else {
            $voice_image = '';
            }
           $form_values['voice_image'] = $voice_image;
            if($id !=''){
             $res = $this->Topper_Model->update($form_values,$id);
            }else {
              $res = $this->Topper_Model->save($form_values);

            }
             redirect('topper');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('topper_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'student_name','label' => lang('student_name'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'voice_description','label' => lang('voice_description'),'rules' => 'required'),
            array('field' => 'company','label' => lang('company'),'rules' => 'required')
                );
        return $rules;
    }

	 public function changetopperStatus(){
        $id = $_POST['voice_id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['voice_status'] = 'n';
            echo "0";
        } else {
            $formvalues['voice_status'] = 'y';
            echo "1";
        }
        $this->Topper_Model->changetopperStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->Topper_Model->deletetopper($id);

    }

}
