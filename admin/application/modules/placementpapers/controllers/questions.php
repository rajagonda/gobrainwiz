<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : questions
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class questions extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('questions_model','company_Model'));
        $this->load->language('placement');
     }

	public function index($cid=null)	{
           
	}
        public function viewQuestions($cid=null)	{
            $breadcrumbarray = array('label'=> "Companies Questions",
                            'link' => base_url()."placementpapers/questions/".$cid
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Questions");
           $data['questions'] = $this->questions_model->getquetions($cid);
           $data['cid'] = $cid;
          // echo "<pre>"; print_r($data);exit;
           $this->template->load_view1('questions', $data);
	}

	public function save($cid=null,$qid=null){
            
        $breadcrumbarray = array('label'=> "Add Question",
                           'link' => base_url()."placementpapers/questions/save/".$cid
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $data['companies'] = $this->company_Model->get();
      
        $data['cid'] = $cid;
        //echo "<pre>"; print_r($data); exit;
        //$data['subcategories'] = $this->company_Model->getsubcategories($cid);
        
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('question_from',{$json_rules});
</script>
JS;

           if($qid !=null){
            $data['action'] = "edit";
            $this->template->set_subpagetitle("Edit Question");
            $data['details'] = $this->questions_model->getquestiondetails($qid);
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Question");
           }
         


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          if ($this->form_validation->run() == true)  {
          
            $formvalues['cat_id'] = $this->input->post('c_id');
            $formvalues['question_name'] = $this->input->post('question_name');
            $formvalues['question_explanation'] = $this->input->post('question_explanation');
    
            
            //echo "<pre>"; print_r($formvalues);exit;
            $subid = $this->input->post('c_id');
            if($qid !=null){
                $res = $this->questions_model->update($formvalues,$qid);
            }else {
                $res = $this->questions_model->save($formvalues);
            }
            redirect('placementpapers/questions/viewQuestions/'.$subid);
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('questions_setup',$data);
      }
        public function _rules() {
        $rules = array(
                array('field' => 'question_name','label' => "Question",'rules' => 'trim|required|xss_clean')
            
            
                 );
        return $rules;
    }
    
    
    public function viewQuestion($qid){

        $data['details'] = $this->questions_model->getquestiondetails($qid);
        //echo "<pre>"; print_r($data);exit;
         $this->template->load_view1('question_view',$data);
    }

    

    public function changequestionStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['question_status'] = 'n';
            echo "0";
        } else {
            $formvalues['question_status'] = 'y';
            echo "1";
        }
        $this->questions_model->changequestionStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->questions_model->delete($id);

    }
    
      public function getsubcategories(){
       $catid = $_POST['selected'];
       $subcats = $this->questions_model->getsubcategories($catid);
        header('Content-type: application/json');
        die( json_encode( $subcats ) );  
   
    }
    

}
