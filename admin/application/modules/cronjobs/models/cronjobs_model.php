<?php
class cronjobs_Model extends CI_Model {
    /*
     * Constructor
     */
    public function __construct() {
        parent::__construct();    
    }
    public function getRegisterUSers(){
      $sql = "SELECT * FROM gk_examuserslist WHERE examuser_type ='n' ORDER BY    examuser_id ASC";
      $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }

    }

     public function getEmails(){

      $sql = "SELECT * FROM gk_emails WHERE sent_status ='n' ORDER BY id ASC LIMIT 150";
      $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
    }
     public function updateEmail($id){

      $sql = "UPDATE gk_emails SET  `sent_status` = 'y' WHERE id ='$id'";
      $this->db->query($sql);
      
    }
    public function updateEmails(){

      $sql = "UPDATE gk_emails SET  `sent_status` = 'n' WHERE id !=''";
      $this->db->query($sql);

    }
    public function getSubject(){
      $sql = "SELECT * FROM gk_email_subjects WHERE subject_status ='y' ";
       $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return false;
        }
    }
    
    
   public function gettodaypuzzle($d) {
        $sql = "SELECT  * FROM gk_puzzles INNER JOIN gk_puzzlequestions 
              on gk_puzzlequestions.puzzle_id = gk_puzzles.puzzle_id WHERE gk_puzzlequestions.puzzle_date ='07/31/2015' ORDER BY gk_puzzles.puzzle_id desc LIMIT 1";  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
    
     
    public function getallusers() {
         $sql = "SELECT  * FROM gk_members WHERE member_status ='y'";  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
    }
    public function getallcontactusers() {
        $sql = "SELECT  * FROM gk_contacts WHERE status ='y'";  
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return '';
        }
    }

    public function getpreviouspuzzle($d){
         $sql = "SELECT  * FROM gk_puzzles INNER JOIN gk_puzzlequestions 
              on gk_puzzlequestions.puzzle_id = gk_puzzles.puzzle_id WHERE gk_puzzlequestions.puzzle_date ='07/30/2015' ORDER BY gk_puzzles.puzzle_id desc LIMIT 1";
          $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->row();
        } else {
            return '';
        }
     }
}
   

?>
