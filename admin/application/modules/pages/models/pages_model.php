<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
            $query = $this->db->get(DB_PREFIX.'pages');
            if($query->num_rows()>0){
                    return $query->result();
            }else {
                    return '';
            }

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'pages',$data);
                echo $this->db->last_query();exit;
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('page_id', $id);
	    $this->db->update(DB_PREFIX.'pages', $data);
	    return $this->db->affected_rows();
	}
	public function getPageDetails($id){
		$this->db->where('page_id', $id);
		$query = $this->db->get(DB_PREFIX.'pages');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}


}

/* End of file module_model.php */
/* Location: ./application/models/module_model.php */