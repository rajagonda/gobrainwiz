<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {

    public function __construct(){
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('Pages_Model');
        $this->load->language('pages');
     }

    public function index()
    {
            $breadcrumbarray = array('label'=> "Pages",
                       'link' => base_url()."pages"
                       );
    $link = breadcrumb($breadcrumbarray);
    $this->template->set_breadcrumb($link);
    $this->template->set_subpagetitle("Manage Pages");

    $data['pages'] = $this->Pages_Model->get();
    $this->template->load_view1('pages', $data);
    }

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Pages",
                       'link' => base_url()."pages"
                       );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('package_from',{$json_rules});
</script>
JS;

           if($id !=''){
            $data['action'] = "edit";
            $data['page'] = $this->Pages_Model->getPageDetails($id);
//            echo "<pre>"; print_r($data);exit;
            $this->template->set_subpagetitle("Edit Page");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add page");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $page_values['page_title'] = $this->input->post('page_title');
            $page_values['page_short_desc'] = $this->input->post('page_short_desc');    
            $page_values['page_short_desc1'] = $this->input->post('page_short_desc1');    
            $page_values['page_desc'] = $this->input->post('page_desc');    
            $page_values['page_metatitle'] = $this->input->post('page_metatitle');    
            $page_values['page_metadesc'] = $this->input->post('page_metadesc');    
            $page_values['page_metawords'] = $this->input->post('page_metawords');    

            if($id !=''){
             $res = $this->Pages_Model->update($page_values,$id);
            }else {
              $res = $this->Pages_Model->save($page_values);

            }
             redirect('pages');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('page_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'page_title','label' => lang('page_title'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'page_desc','label' => lang('page_desc'),'rules' => 'required')
               
               );
        return $rules;
    }

	

}

/* End of file module.php */
/* Location: ./application/controllers/module.php */