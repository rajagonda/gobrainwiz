<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| pages
|--------------------------------------------------------------------------
*/


$lang['manage_page'] = 'Manage Pages';
$lang['page_add'] = 'Add Page';
$lang['page_edit'] = "Edit Page";
$lang['page_title'] = "Page Title";
$lang['page_short_desc'] = "Short Desc1";
$lang['page_short_desc1'] = "Short Desc2";
$lang['page_desc'] = "Description";
$lang['page_image'] = "Page Image";
$lang['page_metatitle'] = "Meta Title";
$lang['page_metadesc'] = "Meta Desc";
$lang['page_metawords'] = "Meta Keywords";


/* End of file pages_lang.php */
/* Location: ./application/module_core/Packages/language/english/pages_lang.php */
