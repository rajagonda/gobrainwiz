<div class="row">
            <div class="col-md-12">
            <!-- col-md-offset-2 -->
<section class="content">
                    <div class="row">
                    <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="settings_from" id="settings_from" enctype="multipart/form-data">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('page_edit');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('page_title'); ?></label>
                                            <input type="text" value="<?php echo $page->page_title; ?>" placeholder="<?php echo lang('page_title'); ?>" name="page_title" id="page_title" class="form-control">
                                        </div>
                                        <div class="form-group">
                                         <label for="exampleInputPassword1"><?php echo lang('page_short_desc'); ?></label>
                                         <textarea id="page_short_desc" name="page_short_desc" rows="10" cols="80"><?php echo $page->page_short_desc; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                         <label for="exampleInputPassword1"><?php echo lang('page_short_desc1'); ?></label>
                                        <textarea id="page_short_desc1" name="page_short_desc1" rows="10" cols="80"><?php echo $page->page_short_desc1; ?></textarea>
                                        </div>
                                          
                                        
                                       
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                
                               
                                    <div class="box-body">
                                        <div class="form-group">
                                        <label for="exampleInputPassword1"><?php echo lang('page_desc'); ?></label>
                                        <textarea id="page_desc" name="page_desc" rows="10" cols="80"><?php echo $page->page_desc; ?></textarea>
                                       </div>
                                        <div class="form-group">
                                            <label><?php echo lang('page_metatitle'); ?></label>
                                            <textarea class="form-control" id="page_metatitle" name="page_metatitle" rows="3" placeholder="Enter ..."><?php echo $page->page_metatitle; ?></textarea>
                                        </div>
                                       <div class="form-group">
                                            <label><?php echo lang('page_metadesc'); ?></label>
                                            <textarea class="form-control" id="page_metadesc" name="page_metadesc" rows="3" placeholder="Enter ..."><?php echo $page->page_metadesc; ?></textarea>
                                        </div>
                                       <div class="form-group">
                                            <label><?php echo lang('page_metawords'); ?></label>
                                            <textarea class="form-control" id="page_metawords" name="page_metawords" rows="3" placeholder="Enter ..."><?php echo $page->page_metawords; ?></textarea>
                                        </div>
                                        
                                      
                                       
                                       
                                       
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

  <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
  <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('page_short_desc');
                CKEDITOR.replace('page_short_desc1');
                CKEDITOR.replace('page_desc');
                
                
                //bootstrap WYSIHTML5 - text editor
                $(".textarea").wysihtml5();
            });
        </script>