<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : emails
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Emails extends MY_Controller {

	public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
             //echo $this->uri->segment(2);exit;
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model(array('emails_Model','groups/groups_model'));
            $this->load->language(array('emails'));
              $this->load->library('pagination');
       }
       public function index()	{
          
            $breadcrumbarray = array('label'=> "Emails",
                            'link' => base_url()."emails"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage EMails");
           //$data['emails'] = $this->emails_Model->get();
           
           
           
                $config['base_url'] = base_url().'/emails/';
		$config["total_rows"] = $this->emails_Model->emailscount();
		$config["per_page"] = 100;
		$config["uri_segment"] = 2;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = true;
		$config['last_link'] = true;
                $config['last_link'] = 'last';
                $config['first_link'] = 'first';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] =  '<li><a   class="active" href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data["emails"] = $this->emails_Model->get($config["per_page"], $page);
                $data["links"] = $this->pagination->create_links();
              $this->template->load_view1('emails', $data);
           
           
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Emails",
                           'link' => base_url()."emails"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        

        $data['groups'] = $this->groups_model->get();
        
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('emails_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['email'] = $this->emails_Model->getemailDetails($id);
            $this->template->set_subpagetitle("Edit Email");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Email");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
              if ($this->form_validation->run() == true)  {
          
            $form_values['group_id'] = $this->input->post('group_id');
            $form_values['name'] = $this->input->post('name');    
            $form_values['email'] = $this->input->post('email');    
            $form_values['phone'] = $this->input->post('phone');    
           

            if($id !=''){
             $res = $this->emails_Model->update($form_values,$id);
            }else {
              $res = $this->emails_Model->save($form_values);

            }
             redirect('emails');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('emails_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'group_id','label' => lang('group_id'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'email','label' => lang('email'),'rules' => 'required|valid_email'),
                array('field' => 'name','label' => lang('name'),'rules' => 'required')
               );
        return $rules;
    }

	 public function changeemailStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->emails_Model->changeemailStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->emails_Model->delete($id);

    }
    public function excelimport(){
        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
            $this->load->library('excel'); 
            $file =   $_FILES["excel_file"]["tmp_name"];
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $formvalues['group_id'] = $this->input->post('group_id');
            
            
            $arr_data = array();
            $inputFileType = PHPExcel_IOFactory::identify($file);  
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);  
            $objReader->setReadDataOnly(true);  
            /**  Load $inputFileName to a PHPExcel Object  **/  
            $objPHPExcel = $objReader->load($file);  
            $total_sheets=$objPHPExcel->getSheetCount(); // here 4  
            $allSheetName=$objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')  
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0); // first sheet  
            $highestRow = $objWorksheet->getHighestRow(); // here 5  
            $highestColumn = $objWorksheet->getHighestColumn(); // here 'E'  
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5  
            for ($row = 2; $row <= $highestRow; ++$row) {  
                for ($col = 0; $col <= $highestColumnIndex; ++$col) {  
                $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();  
                    if(is_array($arr_data) ) { $arr_data[$row-1][$col]=$value; }  
                }  
            }
        
           foreach($arr_data as $val) {
               if($val['0'] !=''){
                   $formvalues['name'] = $val['0'];
               }else {
                   $formvalues['name'] = '';
               }
            
            if($val['2'] !=''){
            $formvalues['phone'] = $val['2'];
            }else {
                $formvalues['phone'] = '';
            }
            $formvalues['email'] = $val['1'];

            $res = $this->emails_Model->save($formvalues);
           
          }
           redirect('emails');
        }else {
              $validationRules = $this->_rules1();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('emails_from',{$json_rules});
</script>
JS;
  $data['script'] = $script;
  $data['groups'] = $this->groups_model->get();
          $this->template->load_view1('emails_export',$data);
        }
    }
    public function _rules1(){
         $rules = array(
                array('field' => 'group_id','label' => lang('group_id'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'excel_file','label' => lang('excel_file'),'rules' => 'required')
                
               );
        return $rules;
    }

}
