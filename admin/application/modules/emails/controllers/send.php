<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : send
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class send extends MY_Controller {

	public function __construct() {
            parent::__construct();
             if(!$this->authentication->checklogin()){
          redirect('login');
        }
            $this->load->library(array('template','form_validation'));
            $this->template->set_title('Welcome');
            $this->load->model(array('emails_Model','groups/groups_model'));
            $this->load->language(array('emails'));
       }
       public function index()	{
            $breadcrumbarray = array('label'=> "Emails",
                            'link' => base_url()."emails"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Send Mails");
           $data['groups'] = $this->groups_model->get();
           $data['email_template'] = $this->emails_Model->getemailtemplate();
           //echo  $data['email_template']->template;exit;
           $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('emails_from',{$json_rules});
</script>
JS;
           
           
           
           
           $data['script'] = $script;
           $this->template->load_view1('sendmails', $data);
	}

	
	 public function _rules() {
        $rules = array(
                array('field' => 'group_id','label' => lang('group_id'),'rules' => 'trim|required|xss_clean|max_length[250]')
                
               );
        return $rules;
    }
    
    public function sendmails(){
        $group_id = $this->input->post('group_id');
        $emails = $this->emails_Model->getemail($group_id);
        $body = $this->input->post('mail_body');
        if($emails !=''){
        foreach ($emails as $value){
        
         $config['useragent']           = "CodeIgniter";
            $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol']            = "smtp";
            $config['smtp_host']           = "localhost";
            $config['smtp_port']           = "25";
        $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;
        $this->load->library('email');
        $this->email->initialize($config);
        ob_start();	 
        ?>
        <?php echo $body;  ?>
        
        <?php 
	 $sendermessage  = ob_get_contents();
	 ob_end_clean(); 
         $this->email->from('admin@brainwizz.in','BRAINWIZZ');
         $this->email->to($value->email);
         $this->email->subject('Email  Details');
         $this->email->message($sendermessage);	
         $this->email->send();
       }
        }
         redirect('emails/send');
          

    }
        

        public function email(){
         $this->load->view('emailtemplate');
    }

	

}
