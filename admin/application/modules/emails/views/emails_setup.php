<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('email_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="emails_from" id="emails_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('group_id'); ?>:</label>
                                            <select id="group_id" name="group_id" class="form-control">
                                                <option value=''>Please Select Group</option>
                                                <?php foreach($groups as $value) { ?>
                                                <option value="<?php echo $value->group_id; ?>" <?php if($action=='edit')  if($email->group_id == $value->group_id) echo "selected";  ?>><?php echo $value->group_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $email->name; ?>" placeholder="<?php echo lang('name'); ?>" name="name" id="name" class="form-control">
                                        </div>
                                        
                                          <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $email->email; ?>" placeholder="<?php echo lang('email'); ?>" name="email" id="email" class="form-control">
                                        </div>
                                          <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $email->phone; ?>" placeholder="<?php echo lang('phone'); ?>" name="phone" id="phone" class="form-control">
                                        </div>
                                       
                                         
                                      
                                   
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>