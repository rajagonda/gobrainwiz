<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : Emails
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class emails_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get($limit, $start){
            
            $sql = "SELECT * FROM gk_emails 
                INNER JOIN gk_groups ON gk_groups.group_id = gk_emails.group_id ORDER BY id DESC LIMIT $start, $limit";
            $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }

        }
        public function emailscount(){
        
            $sql = "SELECT * FROM gk_emails 
               INNER JOIN gk_groups ON gk_groups.group_id = gk_emails.group_id";
           $result = $this->db->query($sql);
           if($result->num_rows() > 0 ) {
            return $result->num_rows();
           } else {
           return '';
           }
            
         
        }
            

                public function save($data){
		$this->db->insert(DB_PREFIX.'emails',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'emails', $data);
	    return $this->db->affected_rows();
	}
	public function getemailDetails($id){
		 $sql = "SELECT * FROM gk_emails 
                INNER JOIN gk_groups ON gk_groups.group_id = gk_emails.group_id
                WHERE gk_emails.id ='$id'";

                 $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->row();
            } else {
            return '';
            }
	}
	public function changeemailStatus($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'emails', $data);
            
	    return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'emails');
	}
        
        public function getemailtemplate(){
             $sql = "SELECT * FROM gk_emailtemplates 
                                WHERE template_id =1";
             $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->row();
            } else {
            return '';
            }
             
        }
        
        public function getemail($gid){
            $sql = "SELECT * FROM gk_emails WHERE group_id = '$gid'";
            $result = $this->db->query($sql);
            if($result->num_rows() > 0 ) {
            return $result->result();
            } else {
            return '';
            }

	}
            


}

/* End of file subjects_model.php */
/* Location: ./application/models/subjects_model.php */