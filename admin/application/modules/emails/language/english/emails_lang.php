<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Emails
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Emails
|--------------------------------------------------------------------------
*/


$lang['manage_emails'] = 'Manage Emails';
$lang['email_add'] = 'Add Emails';
$lang['email_edit'] = "Edit Emails";
$lang['group_id'] = "Group Name";
$lang['email'] = "Email";
$lang['name'] = "Name";
$lang['phone'] = "Phone Number";
$lang['excel_file'] = "Excel File";
$lan['emails_send'] = "Emails Send";



/* End of file Emails_lang.php */
/* Location: ./application/module_core/Emails_lang/language/english/Emails_lang.php */
