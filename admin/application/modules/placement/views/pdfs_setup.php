<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('pdf_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form onsubmit="return Checkfiles(this);" method="post"  action="<?php echo $url;?>" role="form" name="pdf_from" id="pdf_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label><?php echo lang('c_id'); ?>:</label>
                                            <select id="cat_id" name="cat_id" class="form-control">
                                                <option value=''>Please Select Company</option>
                                                <?php foreach($companies as $value) { ?>
                                                <option value="<?php echo $value->id; ?>" <?php if($action=='edit')  if($company->cat_id == $value->id) echo "selected";  ?>><?php echo $value->com_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label><?php echo lang('com_name1'); ?>:</label>
                                            <select id="sub_id" name="sub_id" class="form-control">
                                                <option value=''>Please Select Paper</option>
                                                 <?php if($action=='edit') { ?>
                                               <?php foreach($papers as $value) { ?>
                                                <option value="<?php echo $value->id; ?>" <?php if($action=='edit')  if($company->sub_id == $value->id) echo "selected";  ?>><?php echo $value->com_name; ?></option>
                                                 <?php }  } ?>
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputFile"><?php echo lang('title'); ?></label>
                                            <input type="text" name="title"  value="<?php if($action=='edit')  echo $company->title;?>" class="form-control" id="title">
                                            
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputFile"><?php echo lang('path'); ?></label>
                                            <input type="file" name="path" id="path">
                                            
                                        </div>
                                        
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>


 <script type="text/javascript"> 
     
       function Checkfiles()
        {
        var fup = document.getElementById('path');
        var fileName = fup.value;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
        if(ext == "pdf" )
        {
        return true;
        } 
        else
        {
        alert("Upload pdf files only");
        fup.focus();
        return false;
        }
        }
     
     
 $(document).ready(function () {
 $("#cat_id").change(function (){
     var selectVal = $('#cat_id :selected').val();
     $.ajax({
       url:'<?php echo base_url(); ?>placement/pdfs/getsubcategories',
       type: 'POST',
       datatype:'application/json',
       data: {
            'selected' : selectVal
       },
       success:function(data){
       var htmlString ='';
        htmlString+="<option value=''>Please Select Paper</option>"
       $.each(data,function(i){
             htmlString+="<option value="+data[i]['id']+">"+data[i]['com_name']+"</option>"
           });
       $("#sub_id").html(htmlString);
       }
});

 });
 
 
  
        
       
 
    });
 </script>