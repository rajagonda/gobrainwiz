<div class="row">
<div class="col-md-8 col-md-offset-2">
            <!-- col-md-offset-2 -->
<section class="content">
 <div class="row">
                               
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('question_details');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Category :</label>
                                            <?php echo $details->catname;?>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Question :</label>
                                           <?php echo $details->question_name;?>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 1:</label>
                                          <?php echo $details->option1;?>
                                        </div>
                                        <div class="form-group">
                                            <label>Choice 2:</label>
                                           <?php echo $details->option2;?>
                                        </div>
                                       <div class="form-group">
                                            <label>Choice 3:</label>
                                          <?php echo $details->option3;?>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 4:</label>
                                          <?php echo $details->option4;?>
                                        </div>
                                         <div class="form-group">
                                            <label>Choice 5:</label>
                                           <?php echo $details->option5;?>
                                        </div>
                                         <div class="form-group">
                                            <label>Answer:</label>
                                           <?php echo $details->question_answer;?>
                                        </div>
                                         <div class="form-group">
                                            <label>Explanation:</label>
                                           <?php echo $details->question_explanation;?>
                                        </div>
                                    </div><!-- /.box-body -->

                                   
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                       
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>