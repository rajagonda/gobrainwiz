<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('company_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="topper_from" id="topper_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('com_name'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $company->com_name; ?>" placeholder="<?php echo lang('com_name'); ?>" name="com_name" id="com_name" class="form-control">
                                        </div>


                                        <div class="form-group">
                                            <label>Description Template</label>
                                            <textarea placeholder="Enter ..." rows="50" cols="50" name="description" id="description" class="form-control"><?php if($action=='edit') echo $company->description; ?></textarea>
                                        </div>
                                        
                                    </div><!-- /.box-body -->


                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>


<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets_new/tiny_mce/jscripts/tiny_mce/tiny_mce.js"></script>

    <script language="javascript" type="text/javascript">
        tinyMCE.init({
            mode : "exact",
            elements : "catdesc,section_description,Information,longdesc,shortdesc,description,title_description,mail_body,contact_desc1,contact_desc2,contact_desc3,value",
            theme : "advanced",
            plugins : "advimage,advlink,media,contextmenu",
            theme_advanced_buttons1_add_before : "newdocument,separator",
            theme_advanced_buttons1_add : "fontselect,fontsizeselect",
            theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
            theme_advanced_buttons2_add_before: "cut,copy,separator,",
            theme_advanced_buttons3_add_before : "",
            theme_advanced_buttons3_add : "media",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            extended_valid_elements : "hr[class|width|size|noshade]",
            file_browser_callback : "ajaxfilemanager",
            paste_use_dialog : false,
            theme_advanced_resizing : true,
            theme_advanced_resize_horizontal : true,
            apply_source_formatting : true,
            force_br_newlines : true,
            force_p_newlines : false,   
            relative_urls : false,
                        remove_script_host : false,
                        convert_urls : true
        });

        function ajaxfilemanager(field_name, url, type, win) {
            var ajaxfilemanagerurl = "<?php echo base_url();?>assets_new/tiny_mce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
            var view = 'detail';
            switch (type) {
                case "image":
                view = 'thumbnail';
                    break;
                case "media":
                    break;
                case "flash": 
                    break;
                case "file":
                    break;
                default:
                    return false;
            }
            tinyMCE.activeEditor.windowManager.open({
                url: "<?php echo base_url();?>assets_new/tiny_mce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;         
            var fileBrowserWindow = new Array();
            fileBrowserWindow["file"] = ajaxfilemanagerurl;
            fileBrowserWindow["title"] = "Ajax File Manager";
            fileBrowserWindow["width"] = "782";
            fileBrowserWindow["height"] = "440";
            fileBrowserWindow["close_previous"] = "no";
            tinyMCE.openWindow(fileBrowserWindow, {
              window : win,
              input : field_name,
              resizable : "yes",
              inline : "yes",
              editor_id : tinyMCE.getWindowArg("editor_id")
            });
            
            return false;*/
        }
    </script>