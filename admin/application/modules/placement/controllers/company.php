<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : company
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Company extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('company_Model');
        $this->load->language('placement');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Companies",
                            'link' => base_url()."placement/company"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Companies");
           $data['companies'] = $this->company_Model->get();
           //echo "<pre>"; print_r($data);exit;
           $this->template->load_view1('company', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Companies",
                           'link' => base_url()."placement/company"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('topper_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['company'] = $this->company_Model->getcomapnyDetails($id);
            $this->template->set_subpagetitle("Edit Company");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Company");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['com_name'] = $this->input->post('com_name');
            $form_values['description'] = $this->input->post('description');
            
            
            
            
            
            if($id !=''){
             $res = $this->company_Model->update($form_values,$id);
            }else {
                $form_values['c_id'] = '0';
              $res = $this->company_Model->save($form_values);

            }
             redirect('placement/company');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('company_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'com_name','label' => lang('com_name'),'rules' => 'trim|required|xss_clean|max_length[250]')
                 );
        return $rules;
    }

	 public function changecompanyStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->company_Model->changecompanyStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->company_Model->deletecompany($id);

    }
    
    public function papers(){
         $breadcrumbarray = array('label'=> "Papers",
                            'link' => base_url()."placement/company/papers"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Papers");
           $data['papers'] = $this->company_Model->get();
           //echo "<pre>"; print_r($data);exit;
           $this->template->load_view1('papers', $data);
    }
    public function paperssave($id=null){
        
        $breadcrumbarray = array('label'=> "Papers",
                           'link' => base_url()."placement/company/papers"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        
       $data['companies'] = $this->company_Model->get();

        $validationRules = $this->_rules1();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('paper_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['company'] = $this->company_Model->getcomapnyDetails($id);
            $this->template->set_subpagetitle("Edit Company");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Paper");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['com_name'] = $this->input->post('com_name');
             $form_values['c_id'] =  $this->input->post('c_id');
            
            
            
            if($id !=''){
             $res = $this->company_Model->update($form_values,$id);
            }else {
               
              $res = $this->company_Model->save($form_values);

            }
             redirect('placement/company/papers');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('paper_setup',$data);
	}

	 public function _rules1() {
        $rules = array(
                array('field' => 'com_name','label' => lang('com_name1'),'rules' => 'trim|required|xss_clean|max_length[250]'),
            array('field' => 'c_id','label' => lang('c_id'),'rules' => 'trim|required|xss_clean|max_length[250]')
            
                 );
        return $rules;
    }

}
