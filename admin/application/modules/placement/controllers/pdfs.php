<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : pdfs
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class Pdfs extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model(array('pdfs_model','company_Model'));
        $this->load->language('placement');
     }

	public function index()	{
            $breadcrumbarray = array('label'=> "Companies Pdfs",
                            'link' => base_url()."placement/"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Pdfs");
           $data['pdfs'] = $this->pdfs_model->get();
          // echo "<pre>"; print_r($data);exit;
           $this->template->load_view1('pdfs', $data);
	}

	public function save($id=null){
        
        $breadcrumbarray = array('label'=> "Companies Pdfs",
                           'link' => base_url()."placement/pdfs"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        

        $data['companies'] = $this->company_Model->get();
        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('pdf_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['company'] = $this->pdfs_model->getcomapnyDetails($id);
             $data['papers'] = $this->pdfs_model->getsubcategories($data['company']->cat_id);
            $this->template->set_subpagetitle("Edit Pdf");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Pdf");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          //var_dump($this->form_validation->run());
          //echo validation_errors(); exit;
          if ($this->form_validation->run() == true)  {
          
            $form_values['cat_id'] = $this->input->post('cat_id');
            $form_values['sub_id'] = $this->input->post('sub_id');
            $form_values['title'] = $this->input->post('title');
            
             if($_FILES['path']['name'] !='') {
            
            $filename = time()."_".$_FILES['path']['name'];
            $config['upload_path'] = '../upload/company_pdfs/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('path')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            $pdfpath = $this->data['file_name'];
          }else {
            $pdfpath = '';
            }
            $form_values['path'] = $pdfpath;
            
            
            if($id !=''){
             $res = $this->pdfs_model->update($form_values,$id);
            }else {
                
              $res = $this->pdfs_model->save($form_values);

            }
             redirect('placement/pdfs');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('pdfs_setup',$data);
	}

	 public function _rules() {
        $rules = array(
                array('field' => 'cat_id','label' => lang('com_name'),'rules' => 'trim|required|xss_clean'),
            array('field' => 'sub_id','label' => lang('com_name1'),'rules' => 'trim|required|xss_clean'),
            array('field' => 'title','label' => lang('title'),'rules' => 'trim|required|xss_clean')
            
                 );
        return $rules;
    }
    
      public function getsubcategories(){
        $catid = $_POST['selected'];
        $companies= $this->pdfs_model->getsubcategories($catid);
        header('Content-type: application/json');
        die( json_encode( $companies ) );  
   
    }

	 public function changepdfStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->pdfs_model->changepdfStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->pdfs_model->deletepdf($id);

    }
    
    public function papers(){
         $breadcrumbarray = array('label'=> "Papers",
                            'link' => base_url()."placement/company/papers"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Papers");
           $data['papers'] = $this->pdfs_model->getPapers();
           //echo "<pre>"; print_r($data);exit;
           $this->template->load_view1('papers', $data);
    }
    public function paperssave($id=null){
        
        $breadcrumbarray = array('label'=> "Papers",
                           'link' => base_url()."placement/company/papers"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        
       $data['companies'] = $this->pdfs_model->get();

        $validationRules = $this->_rules1();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('paper_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['company'] = $this->pdfs_model->getcomapnyDetails($id);
            $this->template->set_subpagetitle("Edit Company");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add Paper");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          
          if ($this->form_validation->run() == true)  {
          
            $form_values['com_name'] = $this->input->post('com_name');
             $form_values['c_id'] =  $this->input->post('c_id');
            
            
            
            if($id !=''){
             $res = $this->pdfs_model->update($form_values,$id);
            }else {
               
              $res = $this->pdfs_model->save($form_values);

            }
             redirect('placement/company/papers');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('paper_setup',$data);
	}

	 public function _rules1() {
        $rules = array(
                array('field' => 'com_name','label' => lang('com_name1'),'rules' => 'trim|required|xss_clean|max_length[250]'),
            array('field' => 'c_id','label' => lang('c_id'),'rules' => 'trim|required|xss_clean|max_length[250]')
            
                 );
        return $rules;
    }

}
