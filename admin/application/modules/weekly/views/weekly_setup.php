<div class="row">
            <div class="col-md-8 col-md-offset-2">
<section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo lang('weekly_add');?></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <?php
                                $url = current_url();
                                ?>
                                <form method="post"  action="<?php echo $url;?>" role="form" name="weekly_from" id="weekly_from" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('title'); ?></label>
                                            <input type="text" value="<?php if($action=='edit') echo $update->title; ?>" placeholder="<?php echo lang('title'); ?>" name="title" id="title" class="form-control">
                                        </div>
                                      	
					<div class="form-group">
                                            <label for="exampleInputEmail12">Description</label>
                                            <textarea placeholder="Description"  cols="" name="weekly_desc"class="form-control"><?php if($action=='edit') echo $update->description; ?></textarea>
                                            
                                        </div>



                                         <div class="form-group">
                                              <?php if($action=='edit'){ ?>
                                            <?php if($update->image !=''){   ?>
                                    <img src="../upload/weekly/<?php echo $update->image;?>" height="90" width="75" class="pull-right img-rounded" alt=""/>
                                    <?php }else { ?>
                                    <img src="../upload/weekly/images.jpg" class="pull-right img-rounded" alt=""/>
                                     <?php  } ?>
                                            <?php } ?>
                                            <label for="exampleInputFile"><?php echo lang('image'); ?></label>
                                            <input type="file" name="image" id="image">
                                           
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->


                        </div><!--/.col (left) -->
                        <!-- right column -->
                        
                    </div>   <!-- /.row -->
                </section>
            </div>
        </div>

       <?php 
echo $script;
?>
