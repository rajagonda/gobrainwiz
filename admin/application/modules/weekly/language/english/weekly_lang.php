<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : Toppers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

/*
|--------------------------------------------------------------------------
| Toppers
|--------------------------------------------------------------------------
*/


$lang['manage_weekly'] = 'Manage Weekly Schedule';
$lang['weekly_add'] = 'Add Weekly';
$lang['weekly_edit'] = "Edit Weekly";
$lang['title'] = "Title";
$lang['image'] = "Image";


/* End of file roles_lang.php */
/* Location: ./application/module_core/roles/language/english/roles_lang.php */
