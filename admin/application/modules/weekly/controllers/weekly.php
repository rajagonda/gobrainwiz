<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : weekly
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class weekly extends MY_Controller {

  public function __construct()
  {
        parent::__construct();
         if(!$this->authentication->checklogin()){
          redirect('login');
        }
        $this->load->library(array('template','form_validation'));
        $this->template->set_title('Welcome');
        $this->load->model('weekly_Model');
        $this->load->model('common_model');
        $this->load->language('weekly');
     }

  public function index() {
            $breadcrumbarray = array('label'=> "Weekly Schudle",
                            'link' => base_url()."weekly"
                              );
           $link = breadcrumb($breadcrumbarray);
           $this->template->set_breadcrumb($link);
           $this->template->set_subpagetitle("Manage Weekly");
           $data['weekly'] = $this->weekly_Model->get();
           $this->template->load_view1('weekly', $data);
  }

  public function save($id=null){
        
        $breadcrumbarray = array('label'=> "weekly",
                           'link' => base_url()."weekly"
                           );
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        


        $validationRules = $this->_rules();
        foreach ($validationRules as $form_field)   {
        $rules[] = array(
        'name' => $form_field['field'],
        'display' => $form_field['label'],
        'rules' => $form_field['rules'],
        );
        }

      
$json_rules = json_encode($rules);
$script = <<< JS
<script>
var CIS = CIS || { Script: { queue: [] } };
CIS.Form.validation('weekly_from',{$json_rules});
</script>
JS;

         if($id !=''){
            $data['action'] = "edit";
            $data['update'] = $this->weekly_Model->gettopperDetails($id);
            $this->template->set_subpagetitle("Edit weekly");
           }else {
            $data['action'] = "add";
            $this->template->set_subpagetitle("Add weekly");
           }


          $this->form_validation->set_rules($validationRules);
          if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {
          
          //echo "<pre>"; var_dump($this->form_validation->run());exit;
          if ($this->form_validation->run() == false)  {
          
            $form_values['title'] = $this->input->post('title');            
            $form_values['description'] = $this->input->post('weekly_desc');
            
            if($id !=''){
            if($_FILES['image']['name'] !='') {
            
            $filename = time()."_".$_FILES['image']['name'];
            $config['upload_path'] = '../upload/weekly/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
             $form_values['image'] = $update_image;
           }
          }else {
             if($_FILES['image']['name'] !='') {
            
            $filename = time()."_".$_FILES['image']['name'];
            $config['upload_path'] = '../upload/weekly/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['file_name'] = $filename;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('image')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
            }
            $this->data = $this->upload->data();
            //echo "<pre>"; print_r($this->data); exit;
            $update_image = $this->data['file_name'];
          }else {
            $update_image = '';
            }
             $form_values['image'] = $update_image;
          }
          //echo "<pre>"; print_r($form_values);exit;
            if($id !=''){
             $res = $this->weekly_Model->update($form_values,$id);
            }else {
              $res = $this->weekly_Model->save($form_values);


            }
             $image  ="http://gobrainwiz.in/upload/weekly/".$update_image;

              $cuur_time = date('Y-m-d H:i:s');

              $notdata['title'] = "Weekly Schedule";
                    $notdata['message'] = "Weekly Schedule released";
                    $notdata['notify_type'] = 1;                   
                    $notdata['image'] = $image;
                    $notdata['time_update'] = $cuur_time;
                    
                    $nid = $this->common_model->insertSingle('gk_notifications',$notdata);

                    $stokens = array();
                    $studnetdata['students']= $this->weekly_Model->getNotStudents();
                    foreach ($studnetdata['students'] as  $value) {
                      
                   
                    
                    // $ins_query = "INSERT INTO `gk_notifications` (`title`, `message`, `time_update`) VALUES ('Logged in', 'Logged in successfully', '".$cuur_time."')";

                    

                    $notlog['student_id'] = $value->examuser_id;
                    $notlog['notification_id'] =$nid;
                    $notlog['checked'] = '0';

                    $nlid = $this->common_model->insertSingle('gk_notifications_log',$notlog);
    // $result = $p_notify -> send_notification($tokens, $message,$title,0,false);
                    array_push($stokens, $value->notification_token);
                      }
                   

                    


          //$tokens = array('ffPCoYna1XU:APA91bHxf1g-WL0Q9gmFlU53uiQFZOTxIwiQpe19jI5kZkBZ7wOfj7L9ggNEkZ0wa-kchxcqHo7YhvzACfYOSjaDTdPRd574mFBN6h1dA4fidqRXDkS_hw6TVGTGOLLhrn_De3tyDuCE');
                    $url = 'https://fcm.googleapis.com/fcm/send';
        $field1 = new \stdClass;
       // $field1 -> message = 'Weekly Schudle released';
       // $field1 -> title = 'Weekly Schudle';
       // $field1 -> image = $image;


$field1 -> message = $form_values['description'];
$field1 -> title =  $form_values['title'];
$field1 -> image = $image;
        $field1 -> type = '1';
        $fields = array(
             'registration_ids' => $stokens,
             'data' => $field1
            );
    $headers = array(
     // 'Authorization:key = AIzaSyAFJlc5XeyL6vYCsXzTKP72DudKlKm64FU',
     'Authorization:key = AIzaSyAN9f0vleLFggOxzE2BFv4X8ui6SbIQZKg',     
 'Content-Type: application/json'
      );
     $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       //return $result;
                       


             redirect('weekly');
          }

          
        }

      $data['script'] = $script;
      $this->template->load_view1('weekly_setup',$data);
  }

   public function _rules() {
        $rules = array(
                array('field' => 'title','label' => lang('title'),'rules' => 'trim|required|xss_clean|max_length[250]'),
                array('field' => 'image','label' => lang('image'),'rules' => 'required')
                );
        return $rules;
    }

   public function changetopperStatus(){
        $id = $_POST['id'];
        $staus = $_POST['status'];
        if($staus == 'y') {
            $formvalues['status'] = 'n';
            echo "0";
        } else {
            $formvalues['status'] = 'y';
            echo "1";
        }
        $this->weekly_Model->changetopperStatus($formvalues, $id);
    }
    public function delete(){
        $id =  $this->input->post('id');
        $this->weekly_Model->deletetopper($id);

    }

}
