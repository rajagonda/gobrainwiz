<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Venkata Sudhakar 
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Model : updates
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */

class weekly_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get(){
		$query = $this->db->get(DB_PREFIX.'weekly`');
		if($query->num_rows()>0){
			return $query->result();
		}else {
			return '';
		}

	}
	public function save($data){
		$this->db->insert(DB_PREFIX.'weekly',$data);
		return $this->db->insert_id();

	}
	public function update($data, $id){
		$this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'weekly', $data);
	    return $this->db->affected_rows();
	}
	public function gettopperDetails($id){
		$this->db->where('id', $id);
		$query = $this->db->get(DB_PREFIX.'weekly');
		if($query->num_rows()>0){
			return $query->row();
		}else {
			return '';
		}
	}
	public function changetopperStatus($data, $id){
	    $this->db->where('id', $id);
	    $this->db->update(DB_PREFIX.'weekly', $data);
            return $this->db->affected_rows();
	}
	public function deletetopper($id){
		$this->db->where('id', $id);
        $this->db->delete(DB_PREFIX.'weekly');
	}
	public function getNotStudents(){
		$sql = "SELECT examuser_id,notification_token FROM gk_examuserslist WHERE notification_token !=''";
		 $query = $this->db->query($sql);
        if($query->num_rows()>0) {
            return $query->result();
        } else {
            return false;
        }
	}


}

/* End of file voice_toper_model.php */
/* Location: ./application/models/voice_toper_model.php */