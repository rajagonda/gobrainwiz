<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/** * Author : Avijit
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0 * Model : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone : 9591510490
 * Website : phpguidance.com
 */
class Questions_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function questionscount()
    {
        $sql = "SELECT * FROM gk_questions";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function deleteQuestionById($id)
    {
        // Delete answers
        $this->db->where('qa_question_id', "$id");
        $this->db->delete('gk_question_answers');

        // Delete question result
        $this->db->where('qr_question_id', "$id");
        $this->db->delete('gk_question_results');

        // Delete questions
        $this->db->where('question_id', "$id");
        $this->db->delete('gk_questions');
    }


    public function addQuestions($form)
    {
        $this->db->insert('gk_questions', $form);
        return $this->db->insert_id();
    }

    public function addQuestionsAnswer($form)
    {
        return $this->db->insert('gk_question_answers', $form);
    }


    public function updateQuestions($form, $id)
    {
        $this->db->where('question_id', $id);
        $this->db->update('gk_questions', $form);
    }

   public function updateQuestionAnswers($form, $id)
    {
        $this->db->where('qa_id', $id);
        $this->db->update('gk_question_answers', $form);
    }

    public function getQuestionById($id)
    {
        $sql = "SELECT q.*,qa.* FROM gk_questions as q
                JOIN gk_question_answers as qa ON(q.question_id = qa.qa_question_id)
			        where q.question_id =" . $id;

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $resultdata = $result->result();

            $newdata = array();
            foreach ($resultdata as $record) {
                if (!array_key_exists($record->question_id, $newdata)) {
                    $newdata[$record->question_id] = array();
                    $newdata[$record->question_id] = array('question_id' => $record->question_id, 'question' => $record->question, 'question_status' => $record->question_status);
                    $newdata[$record->question_id]['answers'] = array();
                }
                $newdata[$record->question_id]['answers'][] = array('qa_id' => $record->qa_id, 'qa_question_id' => $record->qa_question_id, 'qa_answer' => $record->qa_answer, 'qa_answer_sequence' => $record->qa_answer_sequence, 'qa_is_correct' => $record->qa_is_correct);
            }

            return $newdata[$id];


        } else {
            return '';
        }
    }


    public function deleteAnswersByQuestionId($id)
    {
        // Delete answers
        $this->db->where('qa_question_id', "$id");
        $this->db->delete('gk_question_answers');
    }


    public function get($limit, $start)
    {

        $sql = "SELECT q.*,qa.* FROM gk_questions as q
                JOIN gk_question_answers as qa ON(q.question_id = qa.qa_question_id)
			        ORDER BY q.question_id  DESC LIMIT $start, $limit";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $resultdata = $result->result();

            $newdata = array();
            foreach ($resultdata as $record) {
                if (!array_key_exists($record->question_id, $newdata)) {
                    $newdata[$record->question_id] = array();
                    $newdata[$record->question_id] = array('question_id' => $record->question_id, 'question' => $record->question, 'question_status' => $record->question_status);
                    $newdata[$record->question_id]['answers'] = array();
                }
                $newdata[$record->question_id]['answers'][] = array('qa_id' => $record->qa_id, 'qa_question_id' => $record->qa_question_id, 'qa_answer' => $record->qa_answer, 'qa_answer_sequence' => $record->qa_answer_sequence, 'qa_is_correct' => $record->qa_is_correct);
            }

            return $newdata;


        } else {
            return '';
        }

    }

    public function questionAnswerscount($id)
    {
        $sql = "SELECT gqr.* FROM gk_question_results gqr
                JOIN gk_question_answers as qa ON(gqr.qr_answer_id = qa.qa_id)
                JOIN gk_questions as q ON(gqr.qr_question_id = q.question_id)
			        where gqr.qr_question_id =" . $id;


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        } else {
            return '';
        }
    }

    public function getQuestionAnswers($id, $limit, $start)
    {
        $sql = "SELECT gqr.*,qa.*,q.*,u.examuser_name FROM gk_question_results gqr
                JOIN gk_examuserslist as u ON(gqr.qr_user_id = u.examuser_id)
                JOIN gk_question_answers as qa ON(gqr.qr_answer_id = qa.qa_id)
                JOIN gk_questions as q ON(gqr.qr_question_id = q.question_id)
			        where gqr.qr_question_id=$id ORDER BY gqr.submitted_at  DESC LIMIT $start, $limit";


        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }
    public function getQuestionDetails($id){

        $this->db->select('*');
        $this->db->where('question_id', $id); // Produces: WHERE name = 'Joe'

        $this->db->from('gk_questions');;

        $query=$this->db->get();

        $row=$query->row();

        return $row;
    }


}