<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('message');
                                $this->session->set_userdata('message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">
                        Questions
                        <a class="blue" title="Add New Question" href="<?php echo base_url(); ?>questions/AddQuestions"
                           style="float:right">
                            <span class="glyphicon glyphicon-plus " style="font-size:150%;color:#438EB9;"></span>
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th>Question</th>
                                <th>Answer 1</th>
                                <th>Answer 2</th>
                                <th>Answer 3</th>
                                <th>Answer 4</th>
                                <th>Status</th>
                                <th>Results</th>
                                <th> Edit</th>
                                <th> Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                            foreach ($records as $index => $record) {

                            ?>
                            <tr>
                                <td> <?= $index+1 ?></td>
                                <td title="<?= $record['question'] ?>"><?= $record['question'] ?></td>
                                <td><?= $record['answers'][0]['qa_answer']; ?>
                                    <?php
                                    if ($record['answers'][0]['qa_is_correct'] == 1){
                                        ?>
                                        (<b>Correct</b>)
                                        <?php
                                    }
                                    ?>

                                </td>
                                <td><?= $record['answers'][1]['qa_answer']; ?>
                                    <?php
                                    if ($record['answers'][1]['qa_is_correct'] == 1){
                                        ?>
                                        (<b>Correct</b>)
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><?= $record['answers'][2]['qa_answer']; ?>
                                    <?php
                                    if ($record['answers'][2]['qa_is_correct'] == 1){
                                        ?>
                                        (<b>Correct</b>)
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><?= $record['answers'][3]['qa_answer']; ?>
                                    <?php
                                    if ($record['answers'][3]['qa_is_correct'] == 1){
                                        ?>
                                        (<b>Correct</b>)
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td title="<?= $record['question_status'] ?>"> <?= $record['question_status'] ?></td>
                                <td>
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        <a target="_blank" class="green"
                                           href="<?php echo base_url(); ?>questions/viewResults/<?= $record['question_id'] ?>"
                                           style="padding: 14px;">
                                            <i class="ace-icon fa fa-external-link bigger-130"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="green"
                                           href="<?php echo base_url(); ?>questions/EditQuestions/<?= $record['question_id'] ?>"
                                           style="padding: 14px;">
                                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="hidden-sm hidden-xs action-buttons">
                                        <a class="red trash"
                                           href="<?php echo base_url(); ?>questions/DeleteQuestion/<?= $record['question_id'] ?>"
                                           style="padding: 14px;">
                                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                                }
                            } else {
                                ?>
                            <tr>
                                <td colspan="10" style="text-align:center">No Records Found</td>
                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
</section>