<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Edit Questions</h3>
                        </div><!-- /.box-header -->
                        <form method="post" action="<?= current_url() ?>" role="form" name="coupon_form"
                              id="coupon_form" enctype="multipart/form-data">
                            <input type="hidden" name="question_id" value="<?= $record['question_id'] ?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Question :</label>
                                    <input type="text" placeholder="Question" name="question"
                                           id="question"
                                           class="form-control" value="<?= $record['question'] ?>">
                                    <?php echo form_error('question', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Answer1 :</label>
                                    <input type="text" placeholder="Answer1" value="<?= $record['answers'][0]['qa_answer'] ?>" name="answer1"
                                           id="answer1" class="form-control">
                                    <?php echo form_error('answer1', '<div class="error">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="answer1_id" value="<?= $record['answers'][0]['qa_id'] ?>"/>

                                <div class="form-group">
                                    <label>Answer2 :</label>
                                    <input type="text" placeholder="answer2" value="<?= $record['answers'][1]['qa_answer'] ?>" name="answer2"
                                           id="answer2" class="form-control">
                                    <?php echo form_error('answer2', '<div class="error">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="answer2_id" value="<?= $record['answers'][1]['qa_id'] ?>"/>

                                <div class="form-group">
                                    <label>Answer3 :</label>
                                    <input type="text" placeholder="Answer3" value="<?= $record['answers'][2]['qa_answer'] ?>" name="answer3"
                                           id="answer3" class="form-control">
                                    <?php echo form_error('answer3', '<div class="error">', '</div>'); ?>
                                </div>
                                <input type="hidden" name="answer3_id" value="<?= $record['answers'][2]['qa_id'] ?>"/>

                                <div class="form-group">
                                    <label>Answer4 :</label>
                                    <input type="text" placeholder="Answer4" value="<?= $record['answers'][3]['qa_answer'] ?>" name="answer4"
                                           id="answer4" class="form-control">
                                    <?php echo form_error('answer4', '<div class="error">', '</div>'); ?>
                                </div>

                                <input type="hidden" name="answer4_id" value="<?= $record['answers'][3]['qa_id'] ?>"/>


                                <div class="form-group">
                                    <label>Correct Answer ? :</label>
                                    <input type="radio" name="qa_is_correct" value="answer1" checked
                                        <?php
                                    if ($record['answers'][0]['qa_is_correct'] == 1){
                                    ?>
                                        <?php echo "checked"; ?>

                                    <?php } ?>
                                    >
                                    Answer1
                                    <input type="radio" name="qa_is_correct" value="answer2"
                                        <?php
                                        if ($record['answers'][1]['qa_is_correct'] == 1){
                                        ?>
                                            <?php echo "checked"; ?>

                                    <?php } ?>

                                    >Answer2
                                    <input type="radio" name="qa_is_correct" value="answer3"

                                        <?php
                                        if ($record['answers'][2]['qa_is_correct'] == 1){
                                        ?>
                                            <?php echo "checked"; ?>

                                    <?php } ?>

                                    >Answer3
                                    <input type="radio" name="qa_is_correct" value="answer4"

                                        <?php
                                        if ($record['answers'][3]['qa_is_correct'] == 1){
                                        ?>
                                            <?php echo "checked"; ?>

                                    <?php } ?>

                                    >Answer4
                                    <?php echo form_error('qa_is_correct', '<div class="error">', '</div>'); ?>
                                </div>

                                <div class="form-group">
                                        <label>Status :</label>
                                        <select name="question_status">
                                            <?php
                                            foreach ($activeInactiveOptions as $index => $opt) {
                                                ?>
                                                <option value="<?=$index?>" <?=$record['question_status']==$index ? "selected":""?>><?=$opt?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                        </form>
                    </div><!-- /.box -->
                </div><!--/.col (left) -->
            </div>   <!-- /.row -->
        </section>
    </div>
</div>
