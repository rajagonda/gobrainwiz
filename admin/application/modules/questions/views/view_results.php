<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->userdata('message')) {
                ?>
                <div class="box" style="border-top: #fff;">
                    <div class="box-header">
                        <div class="nNote nSuccess hideit" style="color: green;text-align: center;font-size: 18px;">
                            <p style="margin:10px">
                                <strong>SUCCESS: </strong>
                                <?php
                                echo $this->session->userdata('message');
                                $this->session->set_userdata('message', "");
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="width:100%">

                        <a class="blue" title="Add New Question" href="<?php echo base_url(); ?>questions"
                           style="float:right">
                            Back
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <h3>Question : <?php echo $question->question; ?></h3>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 10px"> Sno</th>
                                <th>Answer</th>
                                <th>User</th>
                                <th>Submitted date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($records) > 0) {
                                foreach ($records as $index => $record) {
                                    ?>
                                    <tr>
                                        <td> <?= $index + 1 ?></td>
                                        <td title="<?= $record->qa_answer ?>"><?= $record->qa_answer ?></td>
                                        <td title="<?= $record->examuser_name ?>"><?= $record->examuser_name ?></td>
                                        <td title="<?= $record->submitted_at ?>"><?php echo date("d-m-Y H:i:s", strtotime($record->submitted_at));?></td>

                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="3" style="text-align:center">No Records Found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
</section>