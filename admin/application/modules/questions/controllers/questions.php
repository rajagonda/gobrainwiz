<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Author : Venkata Sudhakar
 * Project : Brainwizz
 * Company : renegade it solutions
 * Version v1.0
 * Controller : examusers
 * mail id: ch.v.sudhakar9@gmail.com,phpguidance@gmail.com
 * Phone :8686994774
 * Website : phpguidance.com
 */
class Questions extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->authentication->checklogin()) {
            redirect('login');
        }
        $this->load->library(array('template', 'form_validation'));
        $this->template->set_title('questions');
        $this->load->model(array('questions_model'));
        $this->load->language('questions');
        $this->load->helper(array('text', 'basic'));
        $this->load->library('pagination');
        $this->activeInactiveOptions = ["active" => "Active", "inactive" => "Inactive"];

    }

    public function index($param = false)
    {
        $breadcrumbarray = [
            'label' => "Questions",
            'link' => base_url() . "questions",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Questions");


        // Pagination code starts here


        $config['base_url'] = base_url() . '/questions/';
        $config["total_rows"] = $this->questions_model->questionscount();
        $config["per_page"] = 100;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        if ($config["total_rows"] > 0) {
            $data["records"] = $this->questions_model->get($config["per_page"], $page);
        } else {
            $data["records"] = array();
        }
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('questions', $data);
    }

    public function DeleteQuestion($id)
    {
        $this->questions_model->deleteQuestionById($id);
        $this->session->set_userdata('message', "Successfully Deleted Question!!");
        redirect('questions');
    }

    public function AddQuestions()
    {
        $breadcrumbarray = [
            'label' => "Add New Question",
            'link' => base_url() . "questions/AddQuestions",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Add New Question");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('question', 'Question', 'required');
            $this->form_validation->set_rules('answer1', 'Answer', 'required');
            $this->form_validation->set_rules('answer2', 'Answer', 'required');
            $this->form_validation->set_rules('answer3', 'Answer', 'required');
            $this->form_validation->set_rules('answer4', 'Answer', 'required');
            $this->form_validation->set_rules('qa_is_correct', 'Correct Answer', 'required');

            if ($this->form_validation->run() == TRUE) {

                $form['question'] = $this->input->post('question');
                $form['question_status'] = $this->input->post('question_status');
                $qid = $this->questions_model->addQuestions($form);

                // First Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer1');
                if ($this->input->post('qa_is_correct') == 'answer1') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 1;
                $this->questions_model->addQuestionsAnswer($form);

                // Second Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer2');
                if ($this->input->post('qa_is_correct') == 'answer2') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 2;
                $this->questions_model->addQuestionsAnswer($form);

                // 3rd Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer3');
                if ($this->input->post('qa_is_correct') == 'answer3') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 3;
                $this->questions_model->addQuestionsAnswer($form);


                //4th Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer4');
                if ($this->input->post('qa_is_correct') == 'answer4') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 4;
                $this->questions_model->addQuestionsAnswer($form);


                $this->session->set_userdata('message', "Successfully Added Question!!");
                redirect('questions');
            }
        }
        $this->template->load_view1('add_questions', ['activeInactiveOptions' => $this->activeInactiveOptions]);
    }

    public function EditQuestions($id)
    {
        $breadcrumbarray = [
            'label' => "Update Questions",
            'link' => base_url() . "questions/EditQuestion/" . $id,
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("Update Questions");

        if (isset($_POST) && is_array($_POST) && count($_POST) > 0) {

            $this->form_validation->set_rules('question', 'Question', 'required');
            $this->form_validation->set_rules('answer1', 'Answer', 'required');
            $this->form_validation->set_rules('answer2', 'Answer', 'required');
            $this->form_validation->set_rules('answer3', 'Answer', 'required');
            $this->form_validation->set_rules('answer4', 'Answer', 'required');
            $this->form_validation->set_rules('qa_is_correct', 'Correct Answer', 'required');

            if ($this->form_validation->run() == TRUE) {

                $form['question'] = $this->input->post('question');
                $form['question_status'] = $this->input->post('question_status');

                $this->questions_model->updateQuestions($form, $id);

                // Delete All Answers Before inserting

                $qid = $id;
                // First Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer1');
                if ($this->input->post('qa_is_correct') == 'answer1') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 1;
                $qaid = $this->input->post('answer1_id');
                $this->questions_model->updateQuestionAnswers($form, $qaid);

                // Second Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer2');
                if ($this->input->post('qa_is_correct') == 'answer2') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 2;
                $qaid = $this->input->post('answer2_id');
                $this->questions_model->updateQuestionAnswers($form, $qaid);
                // 3rd Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer3');
                if ($this->input->post('qa_is_correct') == 'answer3') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 3;
                $qaid = $this->input->post('answer3_id');
                $this->questions_model->updateQuestionAnswers($form, $qaid);

                //4th Answer
                $form = array();
                $form['qa_question_id'] = $qid;
                $form['qa_answer'] = $this->input->post('answer4');
                if ($this->input->post('qa_is_correct') == 'answer4') {
                    $correct = 1;
                } else {
                    $correct = 0;
                }
                $form['qa_is_correct'] = $correct;
                $form['qa_answer_sequence'] = 4;
                $qaid = $this->input->post('answer4_id');
                $this->questions_model->updateQuestionAnswers($form, $qaid);

                $this->session->set_userdata('message', "Successfully Updated Question!!");
                redirect('questions');
            }
        }
        $question = $this->questions_model->getQuestionById($id);

        $this->template->load_view1('edit_questions', ['activeInactiveOptions' => $this->activeInactiveOptions, 'record' => $question]);
    }

    public function viewResults($id)
    {

        $breadcrumbarray = [
            'label' => "View Results For Question",
            'link' => base_url() . "View Results For Question",
        ];
        $link = breadcrumb($breadcrumbarray);
        $this->template->set_breadcrumb($link);
        $this->template->set_subpagetitle("View Results For Question");


        // Pagination code starts here


        $config['base_url'] = base_url() . '/questions/viewResults/' . $id;
        $config["total_rows"] = $this->questions_model->questionAnswerscount($id);
        $config["per_page"] = 100;
        $config["uri_segment"] = 2;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['last_link'] = 'last';
        $config['first_link'] = 'first';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active" ><a   href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if ($config["total_rows"] > 0) {
            $data["records"] = $this->questions_model->getQuestionAnswers($id, $config["per_page"], $page);
        } else {
            $data["records"] = array();
        }
        $data['question'] = $this->questions_model->getQuestionDetails($id);
        $data["links"] = $this->pagination->create_links();
        $this->template->load_view1('view_results', $data);

    }

}
