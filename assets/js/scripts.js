var base_url = 'http://gobrainwiz.in/';
jQuery.validator.addMethod("phoneno", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 && 
	phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, "<br />Please specify a valid phone number");
  
$(document).ready(function () {  
	$('#frmHome').validate({ // initialize the plugin
		rules: {
			yourname: {required: true},
			mobile:{required:true,phoneno:true},
			message:{required:true}
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
		$('#ajax_loader').show();
		var dataString = 'yourname='+ yourname +'&mobile='+mobile+'&message='+message;
			$.ajax({
				type: "POST",
				url: base_url+'pages/sendenquiry',
				data: $(form).serialize(),
				success : function(response){
					if(response.id == 1){ 
						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#frmHome').trigger('reset');
					}
					else{
						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#frmHome').trigger('reset');
					}
				}
			});
		}
	});
});


$(document).ready(function () {  
	$('#frmDemo').validate({ // initialize the plugin
		rules: {
			yourname: {required: true},
			mobile:{required:true,phoneno:true},
			message:{required:true}
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
			$('#ajax_loader').show();
			var dataString = 'yourname='+ yourname +'&mobile='+mobile+'&message='+message;
			$.ajax({
				type: "POST",
				url: base_url+'pages/senddemo',
				data: $(form).serialize(),
				success : function(response){
					if(response.id == 1){ 
						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#frmDemo').trigger('reset');
					}
					else
					{
						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#frmDemo').trigger('reset');
					}
				}
			});
		}
	});
});


$(document).ready(function () {
    $('#contactFrm').validate({ // initialize the plugin
        rules: {
            yourname: {required: true},
			email:{required:true,email:true},
			mobile:{required:true,phoneno:true},
			message:{required:true}
		},
        errorPlacement: function(){
            return false;
        },
		
        submitHandler: function (form) { // for demo
		$('#ajax_loader').show();
		var dataString = 'yourname='+ yourname +'&mobile='+mobile+'&email='+email+'&message='+message;
			$.ajax({
				type: "POST",
				url: base_url+'pages/sendenquiry',
				data: $(form).serialize(),
				success : function(response){
					
					if(response.id == 1)
					{
						$("html,#message").animate({ scrollTop:$('#message').prop("scrollHeight")}, "slow");

						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#contactFrm').trigger('reset');
					}
					else
					{
						$('#ajax_loader').hide();
						$("#modal-confirm").modal();
						$("#modal-confirm p").html(response.message);
						$('#frmHome').trigger('reset');
					}
				}
			});
        }
    });
});


$(document).ready(function () {  
	$('#reg_from').validate({ // initialize the plugin
		rules: {
			uname: {required: true},
			mobile:{required:true,phoneno:true},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
		var dataString = 'uname='+ uname +'&mobile='+mobile;
			$.ajax({
				type: "POST",
				datatype:'application/json',
				data: dataString,
				url: base_url+'onlinetest/userRegistration',
				data: $(form).serialize(),
				success : function(response){
					if(response.id == "1")
					{   
						$('#after_otp_form').show();
						$('#after_otp_form').addClass('bounceInRight');
						$('#cd-login,#cd-signup').hide();       
              
						document.getElementById("otpmsg").style.display = "block";
						document.getElementById("otpmsg").innerHTML= response.message;
						setTimeout(function(){ 
							document.getElementById("otpmsg").style.display = "none";
						}, 10000);   
					}else if(response.id == 0){          
            
						document.getElementById("loginfailuremsg").style.display = "block";
						document.getElementById("loginfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("loginfailuremsg").style.display = "none";
						}, 10000);          
            
					}else {                      
						document.getElementById("loginfailuremsg").style.display = "block";
						document.getElementById("loginfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("loginfailuremsg").style.display = "none";
						}, 10000);          
            
					}
				}
			});
		}
	});
});



$(document).ready(function () {  

	$('#login_form').validate({ // initialize the plugin
		rules: {
			loginid: {required: true,phoneno:true},
			pwd:{required:true},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
		
			var dataString = 'loginid='+ $("#loginid").val() +'&pwd='+$('#pwd').val();
			
			$.ajax({
				url:base_url+'onlinetest/loginsubmit',
				type: 'POST',
				datatype:'application/json',
				data: dataString,
				success:function(response)
				{  
					//alert(response);
					if(response.id == "1")
					{         

					
						var ur ='';
						
					   if(ur !=''){   
						  window.location.replace(base_url+ur);
						}else {
						  window.location.replace(base_url+"onlinetest/dashboard");
						}
	  
					}else   if(response.id == 0){          
					
						document.getElementById("loginfailuremsg").style.display = "block";
						document.getElementById("loginfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("loginfailuremsg").style.display = "none";
						}, 10000);          
					
					}else {                      
						document.getElementById("loginfailuremsg").style.display = "block";
						document.getElementById("loginfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("loginfailuremsg").style.display = "none";
						}, 10000);          
					
					}
				}
			});
		}
	});
});



$(document).ready(function () {
	$('#getotp_form1').validate({
		rules: {
			phone: {required: true,phoneno:true},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
			var dataString = 'phone='+ $("#phone").val();
			$.ajax({
				url:base_url+'onlinetest/generateotp',
				type: 'POST',
				datatype:'application/json',
				data: dataString,
				success:function(response)
				{  
					if(response.id == "1")
					{   
						$('#after_otp_form').show();
						$('#after_otp_form').addClass('bounceInRight');
						$('#cd-login,#cd-signup,#cd-reset-password').hide();       
						  
						document.getElementById("otpmsg").style.display = "block";
						document.getElementById("otpmsg").innerHTML= response.message;
						setTimeout(function(){ 
							document.getElementById("otpmsg").style.display = "none";
						}, 10000);   
				  
					}
					else if(response.id == 0)
					{          
						document.getElementById("getotpfailuremsg1").style.display = "block";
						document.getElementById("getotpfailuremsg1").innerHTML= response.message;
						setTimeout(function(){ 
							document.getElementById("getotpfailuremsg1").style.display = "none";
						}, 10000);          
					}
					else
					{                      
						document.getElementById("getotpfailuremsg1").style.display = "block";
						document.getElementById("getotpfailuremsg1").innerHTML= response.message;
						setTimeout(function(){ 
							document.getElementById("getotpfailuremsg1").style.display = "none";
						}, 10000);          
					}
				}
			});
		}
		
	});
});


$(document).ready(function () {
	$('#otp_from').validate({
		rules: {
			otp: {required: true},
			optpwd:{required:true},
			cpwd:{equalTo: "#optpwd"}
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
			var dataString = 'otp='+ $("#otp").val() +'&optpwd='+$("#optpwd").val()+'&cpwd='+$('#cpwd').val();
			$.ajax({
				url:base_url+'onlinetest/regcheckotp',
				type: 'POST',
				datatype:'application/json',
				data: dataString,
				success:function(response){  
					if(response.id == "1"){   
						document.getElementById("otpmsg").style.display = "block";
						document.getElementById("otpmsg").innerHTML= response.message;
						setTimeout(function(){ 
					
							$('#after_otp_form').hide();
							$('#cd-login').addClass('bounceInRight');
							$('#cd-login').show();       
						}, 2000);   
					} else   if(response.id == 0){          
            
						document.getElementById("otpfailuremsg").style.display = "block";
						document.getElementById("otpfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("otpfailuremsg").style.display = "none";
						}, 2000);          
            
					}else {                      
						document.getElementById("otpfailuremsg").style.display = "block";
						document.getElementById("otpfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("otpfailuremsg").style.display = "none";
						}, 2000);          
						
					}
        		}
			});
		}
		
	});
});



$(document).ready(function () {
	$('#getotp_form').validate({
		rules: {
			mobilenumber: {required: true,phoneno:true},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function (form) { // for demo
			var dataString = 'mobilenumber='+ $("#mobilenumber").val();
			$.ajax({
				url:base_url+'onlinetest/generateotp',
				type: 'POST',
				datatype:'application/json',
				data: dataString,
				success:function(response)
				{  
					if(response.id == "1")
					{   
						$('#after_otp_form').show();
						$('#after_otp_form').addClass('bounceInRight');
						$('#cd-login,#cd-signup').hide();       
              
						document.getElementById("otpmsg").style.display = "block";
						document.getElementById("otpmsg").innerHTML= response.message;
						setTimeout(function(){ 
							document.getElementById("otpmsg").style.display = "none";
						}, 10000);   
					}
					else   if(response.id == 0)
					{          
            
						document.getElementById("getotpfailuremsg").style.display = "block";
						document.getElementById("getotpfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("getotpfailuremsg").style.display = "none";
						}, 10000);          
            
					}else 
					{                      
						document.getElementById("getotpfailuremsg").style.display = "block";
						document.getElementById("getotpfailuremsg").innerHTML= response.message;
						setTimeout(function(){ 
						document.getElementById("getotpfailuremsg").style.display = "none";
						}, 10000);          
            
					}
        		}
			});
		}
		
	});
});